Source: cds-crtools
Section: misc
Priority: optional
Maintainer: Erik von Reis <evonreis@caltech.edu>
Build-Depends:
 cmake (>= 3.13), dh-make, libcurl4-gnutls-dev, libexpat1-dev, libfftw3-dev,
 libfreetype6-dev, liblzma-dev, libmetaio-dev, libncurses5-dev,
 libpcre3-dev, libpng-dev, libreadline-dev, libsasl2-dev, libssl-dev,
 libtinfo-dev, zlib1g-dev, python, python-dev, python3, python3-dev,
 gds-root-extensions-base,
 gds-root-extensions-crtools,
 gds-base-crtools,
 gds-base-dev,
 gds-dmt-dev,
Standards-Version: 4.5.0
Homepage: https://git.ligo.org/cds/dtt
Rules-Requires-Root: no

Package: cds-crtools
Architecture: any
Depends:
 dtt-diag, dtt-diagd, dtt-diaggui,
 dtt-awggui, dtt-awgstream, dtt-multiawgstream,
 dtt-tpcmd, dtt-xml-tools,
 chndump,  foton, lidax, dmtviewer,
 python-awg, python3-awg, python-foton, python3-foton
Description: A collection of tools for gathering data from, and
 inducing excitations on LIGO data channels. diag, diagd, diaggui,
 awggui, tpcmd, chndump are included along with foton, the filter
 design program.

Package: dtt-diag
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description: Command line program for running LIGO's Diagnostics Test Tools.

Package: dtt-diagd
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description: Daemon for running LIGO's Diagnostics Test Tools remotely.

Package: dtt-diaggui
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 dtt-diag,
 crtools-gui-libraries-dev,
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description: Graphical program for running LIGO's Diagnostics Test Tools.

Package: dtt-awggui
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 crtools-gui-libraries-dev,
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description: Graphical program for running excitations using LIGO's awgtpman.

Package: dtt-awgstream
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description: Stream arbitrary data as an excitation using LIGO's awgtpman.
 Includes awgexec_run program for streaming excitations from Matlab.

Package: dtt-multiawgstream
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description: Stream multiple arbitrary data sets as excitations using LIGO's awgtpman.

Package: dtt-tpcmd
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description: Command line tool for manipulating test points using LIGO's awgtpman.

Package: dtt-xml-tools
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description:  A handful of command line tools for inspecting and manipulating XML files produced using DTT, including
 xmlconv, xmldata, and xmldir.

Package: dtt-headers
Architecture: all
Description: Private headers needed form some of the DTT library development packages.

Package: chndump
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description: Older program for listing channels for a LIGO NDS server.

Package: foton
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Replaces: gds-gui-crtools
Breaks: gds-gui-crtools
Conflicts: gds-gui-crtools
Description: Graphical program for designing and generating filters, as well as manipulating LIGO-style filter files.
 A variety of design strategies are supported.

Package: lidax
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Replaces: gds-gui-crtools
Breaks: gds-gui-crtools
Conflicts: gds-gui-crtools
Description: Graphical program for retrieving LIGO data.

Package: dmtviewer
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Replaces: gds-gui-crtools
Breaks: gds-gui-crtools
Conflicts: gds-gui-crtools
Description: Graphical program for viewing output of DMT monitors.

Package: fantom
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Replaces: gds-gui-crtools
Breaks: gds-gui-crtools
Conflicts: gds-gui-crtools
Description: Command-line program for tranlation of LIGO frame and NDS data.

Package: python-awg
Architecture: all
Depends:
 ${misc:Depends},
 python, python-numpy,
 libawg-dev,
 libsistr-dev,
 libtestpoint-dev,
Replaces: py-gds-dtt
Breaks: py-gds-dtt
Conflicts: py-gds-dtt
Description: Python 2 bindings to libawg. Create excitations using LIGO's awgtpman using Python.

Package: python3-awg
Architecture: all
Depends:
 ${misc:Depends},
 python3, python3-numpy,
 libawg-dev,
 libsistr-dev,
 libtestpoint-dev,
Description: Python 3 bindings to libawg. Create excitations using LIGO's awgtpman using Python.

Package: python-foton
Architecture: all
Depends:
 ${misc:Depends},
 python,
 python-numpy,
 python-scipy,
 gds-root-extensions-crtools,
 crtools-root-libraries,
 crtools-gui-libraries-dev,
Replaces: gds-gui-crtools
Breaks: gds-gui-crtools
Conflicts: gds-gui-crtools
Description: Python 2 bindings for foton functions.  Library for design, inspection and manipulation of filters and
 LIGO style filter files.

Package: python3-foton
Architecture: all
Depends:
 ${misc:Depends},
 python3,
 python3-numpy,
 python3-scipy,
 gds-root-extensions-crtools,
 crtools-root-libraries,
 crtools-gui-libraries-dev,
Description: Python 3 bindings for foton functions.  Library for design, inspection and manipulation of filters and
 LIGO style filter files.

Package: libdtt
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description: Library containing the functionality of LIGO's Diagnostics Test Tools.

Package: libdtt-dev
Architecture: any
Depends:
 ${misc:Depends},
 libdtt, libtestpoint-dev, libfantom-dev, libdfm-dev
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description: Files needed for development using libdtt.

Package: libawg
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description: Library used to interface with awgtpman, the waveform excitation program used at LIGO.

Package: libawg-dev
Architecture: any
Depends:
 ${misc:Depends},
 libawg, libtestpoint-dev, libdtt-dev
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description: Files needed for development with libawg.

Package: libtestpoint
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description: Library for manipulating LIGO test points.

Package: libtestpoint-dev
Architecture: any
Depends:
 ${misc:Depends},
 libtestpoint,
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description: Files needed for development with libtestpoint.

Package: libsistr
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description: Library for sending arbitrary data as excitations using LIGO's awgtpman.

Package: libsistr-dev
Architecture: any
Depends:
 ${misc:Depends},
 libsistr,
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description: Files needed to develop with libsistr.

Package: libfantom
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description: Library for translation of LIGO data frames and LIGO NDS data.

Package: libfantom-dev
Architecture: any
Depends:
 ${misc:Depends},
 libsistr,
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description: Files needed to develop with libfantom.

Package: libdfm
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description: Library for interfacing with LIGO's data flow manager.

Package: libdfm-dev
Architecture: any
Depends:
 ${misc:Depends},
 libsistr, libfantom-dev
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description: Files needed to develop with libdfm.

Package: libfilterfile
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends}
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description: C++ library for manipulating foton filter files.

Package: libfilterfile-dev
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends}
Replaces: gds-dtt-crtools, gds-crtools
Breaks: gds-dtt-crtools, gds-crtools
Conflicts: gds-dtt-crtools, gds-crtools
Description: Files needed to develop with libfilterfile.

Package: crtools-gui-libraries
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Replaces: gds-gui-crtools
Breaks: gds-gui-crtools
Conflicts: gds-gui-crtools
Description: Common libraries used by LIGO control room graphical programs.

Package: crtools-gui-libraries-dev
Architecture: any
Depends:
 ${misc:Depends},
 crtools-gui-libraries,
 libfantom-dev,
 libdfm-dev
Replaces: gds-gui-crtools, gds-gui-headers
Breaks: gds-gui-crtools, gds-gui-headers
Conflicts: gds-gui-crtools, gds-gui-headers
Description: Files needed for development with crtools-gui-libraries

Package: crtools-root-libraries
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Replaces: gds-gui-crtools
Breaks: gds-gui-crtools
Conflicts: gds-gui-crtools
Description: Common ROOT dictionary libraries used by LIGO control room programs.


