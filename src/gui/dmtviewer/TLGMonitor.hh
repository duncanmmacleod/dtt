#ifndef _LIGO_TLGMONITOR_H
#define _LIGO_TLGMONITOR_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGMonitor						*/
/*                                                         		*/
/* Module Description: monitor client API				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 03Jun00  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGMonitor.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include "monitorapi.hh"
#include "TLGFrame.hh"
#include <TGFrame.h>
#include <TGButton.h>
#include <TGListBox.h>
#include <TGComboBox.h>
#include <TGLabel.h>
#include <TGTab.h>
#include <string>

#if defined(__SUNPRO_CC) && defined(G__DICTIONARY)
   namespace ligogui {}
   using namespace ligogui;
#endif

   class ParameterDescriptor;
   class PlotDescriptor;

namespace calibration {
   class Descriptor;
}
namespace ligogui {

   class TLGNumericControlBox;


/** @name TLGMonitor
    This header exports class used in the GUI of the dmtviewer.

    @memo Data monitor interface
    @author Written June 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

/** Monitor selection frame. Contains a pad with a list of available
    monitors and data objects, and a pad with a list of the selected
    objects.
   
    @memo Monitor selection frame
    @author Written June 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGMonitorSelection : public TGHorizontalFrame, public TGWidget {
   
      friend class TLGMonitorSelectionDialog;
   
   protected:
      /// Wait cursor
      static Cursor_t fWaitCursor;
      /// Monitor name manager
      monapi::TLGMonitorMgr*	fMgr;
      /// List of currently active objects
      monapi::TLGMonitorDatumList*	fDobjs;
      /// Currently selected service
      std::string	fCurService;
      /// Currently selected data object
      std::string	fCurDObject;
      /// Currently selected  active data object
      monapi::TLGMonitorDatum::name_t	fActiveDObject;
   
      /// Group frames
      TGGroupFrame*	fAvail;
      /// Group frames
      TGGroupFrame*	fActive;
      /// Button frame
      TGCompositeFrame*	fButton;
      /// Helper frames
      TGCompositeFrame*	fF[10];
      /// Labels
      TGLabel*		fLabel[10];
      /// Numereous layout hints
      TGLayoutHints*	fL[10];
      /// List of monitors
      TGListBox*	fMonitors;
      /// List of data objects
      TGListBox*	fDObjects;
      /// List of selected objects
      TGListBox*	fSel;
      /// Option tab
      TGTab*		fOptions;
      /// Buttons (add, delete, update)
      TGButton*		fAction[3];
      /// Tab for update
      TGCompositeFrame*	fTabUpdate;
      /// Tab for type
      TGCompositeFrame*	fTabType;
      /// Tab for calibration
      TGCompositeFrame*	fTabCal;
      /// Update selection
      TGRadioButton*	fUpdateSel[4];
      /// Update time
      TLGNumericControlBox* fUpdateTime;
   
   public:
   
      /** Constructs a monitor selection frame.
          @memo Constructor.
          @param p Parent window
   	  @param mgr Monitor name manager
   	  @param list List of currently active objects
   	  @param id Widget id
        ******************************************************************/
      TLGMonitorSelection (const TGWindow* parent, 
                        monapi::TLGMonitorMgr& mgr,
                        monapi::TLGMonitorDatumList& list, Int_t id = -1);
      /// Destructor
      virtual ~TLGMonitorSelection ();
      /// Build list of available monitors/data objects
      virtual Bool_t BuildAvailList (Int_t level);
      /// Build list of active data objects
      virtual Bool_t BuildActiveList (Int_t level);
      /// Transfer options
      virtual Bool_t TransferOpt (Bool_t toGUI);
   
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };



/** Dialog box with monitor selection frame.
    objects.
   
    @memo Monitor selection dialog box
    @author Written June 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGMonitorSelectionDialog : public TLGTransientFrame {
   protected:
      /// Monitor selction window
      TLGMonitorSelection*	fMon;
      /// Numereous layout hints
      TGLayoutHints*	fL[10];
      /// Ok button
      TGButton*		fOkButton;
   
   public:
      /// Constructor
      TLGMonitorSelectionDialog (const TGWindow *p, const TGWindow *main,
                        monapi::TLGMonitorMgr& mgr, 
                        monapi::TLGMonitorDatumList& list);
      /// Destructor
      virtual ~TLGMonitorSelectionDialog ();
      /// Close windows
      virtual void CloseWindow();
   
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };



//@}
}

#endif
