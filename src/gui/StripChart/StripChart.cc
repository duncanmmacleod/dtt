//
//    Strip chart class
//
#ifndef __CINT__
#include "TGaxis.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TPolyLine.h"
#include "TText.h"
#include <iostream>
#endif
#include "StripChart.hh"
#include "StripPen.hh"
#include <time.h>

//--------------------------------------  Constructor
StripChart::StripChart(int nPen, const char* Title) {
    TPad* canvas = new TCanvas("cStripChart", Title);
    mDelCanvas = true;
    InitChart(nPen, canvas);
}

//--------------------------------------  Constructor
StripChart::StripChart(int nPen, TPad* canvas) {
    mDelCanvas = false;
    InitChart(nPen, canvas);
}

//--------------------------------------  Constructor
void
StripChart::InitChart(int nPen, TPad* canvas) {
    mNPen   = 0;
    mXData  = 0;
    mLogY   = false;
    mNPosn  = 0;
    mXmin   = 0;
    mX0     = 0;
    mXmax   = 10.0;
    mAutoScroll = true;
    mScroll = 0.1;
    mYmin   = 0.0;
    mYmax   = 1.0;
    setNPen(nPen);
    setN(1000);
    mCanv   = canvas;
    mPlot   = 0;
    mPenNames = true;
    mAxisType = kNumber;
    mAxisOK = false;
    mPlotYAxis = false;
}

//--------------------------------------  Destructor
//
//    Canvases and Pads tend to take things with them when they are deleted,
//    leaving invalid pointers lying around. To prevent an error when this 
//    happens, delete plotted objects first then delete the TPad and finally 
//    the TCanvas.
StripChart::~StripChart() {
    setNPen(0);

    if (mDelCanvas) {
	if (mPlot) {
	    delete mPlot;
	    mPlot = 0;
	}
	if (mCanv) {
	    delete mCanv;
	    mCanv = 0;
	}
    }
}

//--------------------------------------  Set Logarithmic axes
bool
StripChart::getLogY(int p) const {
    if (p<0 || p >= mNPen) return mLogY;
    return mPen[p]->getLogY();
}

//--------------------------------------  Set Color
void 
StripChart::setColor(int p, int color) {
    if (p<0 || p >= mNPen) return;
    mPen[p]->setColor(color);
}

void 
StripChart::setColor(int p, const char* color) {
    if (p >= mNPen) return;
}

//--------------------------------------  Set Scaling factor
void 
StripChart::setScale(int p, float Scale) {
    if (p < 0 || p >= mNPen) return;
    mPen[p]->setScale(Scale);
}

//--------------------------------------  Set Bias
void 
StripChart::setBias(int p, float Bias) {
    if (p < 0 || p >= mNPen) return;
    mPen[p]->setBias(Bias);
}

//--------------------------------------  Set Stripe
void 
StripChart::setStripe(int p, float Center, float hWidth) {
    if (p < 0 || p >= mNPen) return;
    if (mPen[p]->getLogY()) {
        hWidth = log10(hWidth);
	Center = log10(Center);
    }
    float Scale = 0.5/(float(mNPen)*hWidth);
    mPen[p]->setScale(Scale);
    mPen[p]->setBias((p + 0.5)/float(mNPen) - Center*Scale);
}

//--------------------------------------  Set minimum x
void 
StripChart::setMinX(XAxis_t xmin) {
    mXmin = xmin;
    mX0   = xmin;
    mAxisOK = false;
}

//--------------------------------------  Set Maximum X
void 
StripChart::setMaxX(XAxis_t xmax) {
    mXmax = xmax;
    mAxisOK = false;
}

//--------------------------------------  Set minimum y
void 
StripChart::setMinY(float ymin) {
    if (mLogY) {
        if (ymin > 0) mYmin = log10(ymin);
	else          mYmin = 0;
    } else {
        mYmin = ymin;
    }
    mAxisOK = false;
}

//--------------------------------------  Set maximum y
void 
StripChart::setMaxY(float ymax) { 
    if (mLogY) {
        if (ymax > 0) mYmax = log10(ymax);
	else          mYmax = 0;
    } else {
        mYmax = ymax;
    }
    mAxisOK = false;
}


//--------------------------------------  Scroll display region
void StripChart::scroll(float dx) {
    XAxis_t minScroll = mScroll*(mXmax - mXmin);
    if (dx<minScroll) dx = minScroll;
    mXmin += dx;
    mXmax += dx;
    mAxisOK = false;
}

//--------------------------------------  Select linear/log mode
void 
StripChart::setLogY(bool log) {
    mLogY = log;
    for (int p=0 ; p<mNPen ; p++) mPen[p]->setLogY(log);
}

void 
StripChart::setLogY(int p, bool log) {
    if (p<0 || p>=mNPen) return;
    mPen[p]->setLogY(log);
}

//--------------------------------------  Set the number of polyline slots
void 
StripChart::setN(int N) {
    if (N<0) return;
    mNPosn = N;
    mInx.setN(N);
    delete mXData;
    mXData = 0;
    if (N) {
        mXData = new XAxis_t[N];
	for (int i=0 ; i<N ; i++) mXData[i] = 0;
	for (int p=0 ; p<mNPen ; p++) {
	    mPen[p]->setN(N);
	    resetPen(p);
	}
    } else {
        for (int p=0 ; p<mNPen ; p++) delete mPen[p];
    }
    mHighWater = 0;
}

//--------------------------------------  Set the pen title
void 
StripChart::setTitle(int p, const char* Title) {
  if (p<0 || p>=mNPen) return;
  mPen[p]->setTitle(Title);
}

//--------------------------------------  Set the number of Pens
void 
StripChart::resetPen(int p) {
    if (mNPosn) {
	mPen[p]->clear(0);
	mPen[p]->setColor(p+2);
	mPen[p]->setLogY(mLogY);
	if (mLogY) setStripe(p, 1.0, 10.);
	else       setStripe(p, 0.0, 1.0);
    }
}

//--------------------------------------  Set the number of Pens
void 
StripChart::redraw(void) {
    drawAxis();
    update();
}

//--------------------------------------  Set the number of Pens
void 
StripChart::setNPen(int N) {
    if (N < 0) return;
    StripPen** Psave = mPen;
    if (N) {
        mPen   = new StripPen*[N];
	for (int p=0 ; p<mNPen ; p++) mPen[p] = Psave[p];
	for (int p=mNPen ; p<N ; p++) {
	    mPen[p] = new StripPen(mNPosn, "");
	    resetPen(p);
	}
    } else {
        mPen   = 0;
    }
    if (mNPen) {
        for (int p=N ; p<mNPen ; p++) delete Psave[p];
	delete Psave;
    }
    mNPen = N;
}

//--------------------------------------  Add a point to the plot
void 
StripChart::plot(XAxis_t x, const float* y) {
    int inx = mInx.add(1);
    mXData[inx] = x - mX0;
    for (int p=0 ; p<mNPen ; p++) {
	mPen[p]->setData(inx, y[p]);
    }

    if ((x > mXmax) && mAutoScroll) {
        scroll(x - mXmax);
	update();
    } else {
        update(inx);
    }
}

//--------------------------------------  Update the display
void 
StripChart::update(int inxmin) {
    int   start, lp;
    XAxis_t xlast, thisx;
    float thisy;

    if (!mNPen) return;
    if (!mAxisOK) drawAxis();

    //----------------------------------  Build polyline lists
    int ip = mInx.prev(inxmin);
    if (ip < 0 ) {
        start = mInx.first();
        xlast = mXmin - mX0;
    } else {
        start = inxmin;
        xlast = mXData[ip];
    }

    //-----------------------------------  Find the start point
    TPolyLine* PL = mPen[0]->getPLine();
    if (!mHighWater) {
        lp = 0;
    } else if (PL->GetX()[mHighWater-1] == xlast) {
        lp = mHighWater-1;
    } else {
        for (lp=0 ; (lp<mHighWater) && xlast>(PL->GetX())[lp] ; lp+=2);
    }

    //-----------------------------------  Loop over points to be plotted
    for (int inx=start ; inx >= 0 ; inx = mInx.next(inx)) {
        thisx = mXData[inx];
 	if ((thisx > mXmin-mX0) && (thisx <= mXmax-mX0)) {
	    for (int p=0 ; p<mNPen ; p++) {
	        PL    = mPen[p]->getPLine();
	        thisy = mPen[p]->getData(inx);
	        PL->SetPoint(lp  , xlast, thisy);
	        PL->SetPoint(lp+1, thisx, thisy);
	    }
	    xlast = thisx;
	    lp += 2;
	}
    }
    for (int p=0 ; p<mNPen ; p++) {
        PL    = mPen[p]->getPLine();
        PL->SetPoint(lp, xlast, 0.0);
	mPen[p]->clear(lp+1);
    }
    mHighWater = lp+1;
    mPlot->Modified();
    mPlot->Update();
}

//--------------------------------------  Draw the frame.
void 
StripChart::drawAxis(void) {
    XAxis_t pxmin = 0.15;
    XAxis_t pxmax = 0.95;
    float pymin = 0.10;
    float pymax = 0.95;
    if (!mPenNames && mAxisType == kNone) {
        pxmin = 0.01;
	pxmax = 0.99;
        pymin = 0.01;
	pymax = 0.99;
    } else if (mAxisType == kNone) {
        pymin = 0.05;
	pymax = 0.95;
    } else if (!mPenNames) {
        pxmin = 0.05;
	pxmax = 0.95;
    }

    //----------------------------------  Clear off the canvas, create a pad.
    if (mPlot) delete mPlot;
    mPlot = 0;
    mCanv->Clear("");
    mPlot = new TPad("cStripPlot", "Plot", pxmin, pymin, pxmax, pymax, 1);
    mPlot->Range(mXmin-mX0, mYmin, mXmax-mX0, mYmax);
    mCanv->cd();
    mPlot->Draw("");

    //----------------------------------  Plot the axes.
    int dtinc  = 1;
    enum {
        kSecs,
	kMins,
	kHour,
	kDays} dtunit;
    char datstr[64];
    const char* months[12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
			      "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    float   yaxis;
    TGaxis* pAxis;

    switch (mAxisType) {
    case kNone:
        break;

    case kNumber:
        yaxis = pymin;
	pAxis = new TGaxis(pxmin, yaxis, pxmax, yaxis, mXmin, mXmax, 
			   510, "=-");
	pAxis->SetBit(kCanDelete);
	pAxis->Draw();
	break;

    case kLocalTime:
    case kGMTime:
        struct tm* date;

        //------------------------------  Print the start date
        time_t tSec = time_t(mXmin);
	if (mAxisType == kLocalTime) date = localtime(&tSec);
	else                         date = gmtime(&tSec);
	sprintf(datstr, "Start Date: %s %i, %i %i:%02i:%02i", 
		months[date->tm_mon], date->tm_mday, 1900+date->tm_year, 
		date->tm_hour, date->tm_min, date->tm_sec);
	TText* Text = new TText(pxmin, pymin-0.05, datstr);
	Text->SetTextAlign(13);
	Text->SetTextSize(0.040);
	Text->SetBit(kCanDelete);
	Text->Draw("");

        //------------------------------  Calculate the axis units
        double dtime = mXmax - mXmin;
	if (dtime < 10 ) {
	    dtinc = 1;
	    dtunit = kSecs;
	} else if (dtime < 45) {
	    dtinc = 5;
	    dtunit = kSecs;
	} else if (dtime < 150) {
	    dtinc = 15;
	    dtunit = kSecs;
	} else if (dtime < 600) {
	    dtinc = 60;
	    dtunit = kMins;
	} else if (dtime < 1800) {
	    dtinc = 300;
	    dtunit = kMins;
	} else if (dtime < 7200) {
	    dtinc = 900;
	    dtunit = kMins;
	} else {
	    dtinc = 3600;
	    dtunit = kHour;
	}

	//------------------------------  Offset to first label position
	if (tSec%dtinc) tSec += dtinc - tSec%dtinc;

	for (XAxis_t x=tSec ; x <= mXmax ; x += XAxis_t(dtinc)) {
	     tSec = time_t(x);
	     if (mAxisType == kLocalTime) date = localtime(&tSec);
	     else                         date = gmtime(&tSec);
	     switch (dtunit) {
	     case kSecs:
	         sprintf(datstr, "%i:%02i:%02i", date->tm_hour, date->tm_min, 
			 date->tm_sec);
	         break;
	     case kMins:
	         sprintf(datstr, "%i:%02i", date->tm_hour, date->tm_min);
	         break;
	     default:
	         sprintf(datstr, "%i:00", date->tm_hour);
	     }
	     XAxis_t xpos = pxmin + (x-mXmin)*(pxmax-pxmin)/(mXmax-mXmin);
	     Text = new TText(xpos, pymin, datstr);
	     Text->SetTextAlign(23);
	     Text->SetTextSize(0.035);
	     Text->SetBit(kCanDelete);
	     Text->Draw("");
	}
    }

    //----------------------------------  Title the traces.
    // XAxis_t xtext = pxmin - 0.01;
    float pdy   = (pymax - pymin)/mNPen;
    for (int p=0 ; p<mNPen ; p++) {
	if (mPlotYAxis) {
	    double Ymin = pymin + pdy*p;
	    double Ymax = Ymin + pdy;
	    pAxis = new TGaxis(pxmin, Ymin, pxmin, Ymax, 
			       mPen[p]->transFromDisplay(double(p)/mNPen),
			       mPen[p]->transFromDisplay(double(p+1)/mNPen),
			       510, "=-");
	    pAxis->SetBit(kCanDelete);
	    pAxis->Draw();
	}
   }

    mPlot->cd();
    for (int p=0 ; p<mNPen ; p++) {
        double Yc = double(p+1)/mNPen-0.025;
        mPen[p]->putTitle(mXmax - mX0, Yc, "R");
	if (p) {
	    mPen[p]->clear(0);
	    (mPen[p]->getPLine())->Draw("");
	    double Yc = double(p)/double(mNPen);
	    TLine* cLine = new TLine(mXmin-mX0, Yc, mXmax-mX0, Yc);
	    cLine->SetLineColor(0);
	    cLine->SetBit(kCanDelete);
	    cLine->Draw("");
	}
    }
    mHighWater = 0;
    mCanv->Modified();
    mCanv->Update();
    mAxisOK = true;
}


//--------------------------------------  Pen


//--------------------------------------  Constructor
StripPen::StripPen(int N, const char* Title) 
  : mPLine(0), mText(0), mBias(0.0), mScale(1.0), mLogY(false), mYData(0)
{
    setN(N);
    setTitle(Title);
}

//--------------------------------------  Destructor
StripPen::~StripPen() {
  delete mPLine;
  delete mYData;
  delete mText;
}

//--------------------------------------  Set Color
void 
StripPen::setColor(int color) {
  mPLine->SetLineColor(color);
}

//--------------------------------------  Set Scaling factor
void 
StripPen::setScale(float Scale) {
  mScale = Scale;
}

//--------------------------------------  Set Bias
void 
StripPen::setBias(float Bias) {
  mBias = Bias;
}

//--------------------------------------  Select linear/log mode
void 
StripPen::setLogY(bool log) {
  mLogY = log;
}

//--------------------------------------  Set the number of polyline slots
void 
StripPen::setN(int N) {
    if (N<0) return;
    delete mYData;
    mYData = 0;
    delete mPLine;
    mPLine = 0;
    if (N) {
        mYData  = new float[N];
	int npl = 2*N + 1;
	mPLine  = new TPolyLine(npl);
	clear(0);
    }
}

//--------------------------------------  Add a point to the plot
void 
StripPen::setData(int inx, float y) {
    float yVal = y;
    if (mLogY) {
        if (yVal > 0) yVal = log10(yVal);
	else          yVal = 0;
    }
    mYData[inx] = transToDisplay(yVal);
}

//--------------------------------------  Add a point to the plot
void 
StripPen::setTitle(const char* Title) {
    if (!Title) {
        mTitle = "";
    } else {
        mTitle = Title;
    }
}

//--------------------------------------  Add a point to the plot
void 
StripPen::putTitle(float xpos, float ypos, const char* opts) {
    if (!*mTitle.c_str()) return;
    mText = new TText(xpos, ypos, mTitle.c_str());
    mText->SetTextColor(mPLine->GetLineColor());
    mText->SetTextAlign(32);
    mText->SetTextSize(0.035);
    mText->Draw(opts);
}

//--------------------------------------  Clear a pen polyline
void 
StripPen::clear(int min, int max) {
    if (min <  0) min = 0;
    if (max <= 0 || max > mPLine->GetN()) max = mPLine->GetN();
    Double_t* xptr = mPLine->GetX() + min;
    Double_t* yptr = mPLine->GetY() + min;
    for (int i=min ; i<max ; i++) {
        *xptr++ = 0.0;
	*yptr++ = 0.0;
    }
}

