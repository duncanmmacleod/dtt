Version 2.16.18, January 2015
* Fix Bugzilla 778, make filter module lists iterable in Python.

Version 2.16.17 November 2014
* Bugzilla 761, graph line width control changed to integer values
  greater than 0.

* Bugzilla 760, Foton now warns the user if a filter design with too 
  many poles/zeros is pasted into the design window, either with a
  warning popup, or a warning when the file is written.

* Bugzilla 754, graph legend background is red by default using
  root version 5.34.21, correct by explicitly settig the color to white.

* Bugzilla 755, file filter for PNG files is incorrect in print
  Save As dialog box.

Version 2.16.15 October 2014
* Fix Bugzilla 687, add copy/paste to Calibration dialog in graphics window.

Version 2.16.12.2, July 2014
* Implement patch listed in Bugzilla 659 to add foton scripting capability
  using python.

* Fix Bugzilla 685, add a "-o <outfile>" option to the foton command line
  to be used with the -c option, allowing the file written by foton to have
  a different name than the input file.

* Fix Bugzilla 684, to get foton to use the correct path when the -c option
  is used and the "-p <directory path>" option is used.

* Remove leading spaces from the design string when a filter file is saved.
  
Version 2.16.12.1, May 2014
* Bugzilla 655 -Remove legacy write option and menu item, option is no 
  longer needed.

* Fix Bugzilla 656, foton sometimes crashes when invoked from diaggui.

Version 2.16.9.1, February 2014

* Change format of floating point numbers in design strings from %g 
  to %.16g which prints more significant digits.  Affects cases where
  foton creates a design string from a set of SOS coefficients.
  Increasing the number of significant digits in the design string allows
  foton to regenerate the SOS coefficients with more accuracy.

Version 2.16.9, January 2014

* Add a non-interactive correction mode to foton.  This is invoked with
  a -c option, also specifying a filter file.  Foton will read the filter
  file, then write it as if the user had saved the file in the interactive
  mode.  The effect is to create design strings for filters that don't 
  have them, and to strip out filters not associated with the module list
  at the beginning of the file.  This option can be somewhat destructive 
  if not used with care.

Version 2.16.8, November 2013

* Change warning message on file read when no design string is present for
  filter module.  New message is "Missing design string, a new string will
  be generated."  (bugzilla #511)

* Add option to allow selection of filter module and section on invocation.
  The sytax is "foton -s <module>:<section> <filter file>", where module
  is the name of a module in the filter file, and section is in the range
  1 through 10 for the filter section.  The section may be omitted, in 
  which case the module is selected. (bugzilla #497) 

* Updates to compile with gcc version 4.7.x

Version 2.16.5, October 2012

* Change matlab merge batch mode functionality to skip displaying any ROOT 
  graphics.  Foton will return 0 for successful batch merge operations, or 
  -1 (255) for failure.  Errors reading the filter file and merge file will 
  now be written to stdout if any are encountered.

* Errors found on reading a filter file are reported to the user in a popup 
  dialog box when the file is read.  Although foton still discards filters 
  it doesn't understand at least you have fair warning.  The filter "File:"
  label has been changed to a button which turns red if the file has errors.
  This button may be clicked to display file errors.

* Added the ability to read release notes from the Help menu in foton.

* If a filter has an output ramp set with 0 ramp time, foton will issue a 
  warning and change the output switching to immediate.
  
Version 2.16.3, October 2011

* Filter files which are symbolic links will now preserve the symbolic link 
  by following the links until the original file (if any) is found. This 
  requires that the directory in which the actual file resides be writable by 
  the user, as a temporary file is still generated when writing.

Version 2.15.2, July 2011

* Foton files can be saved even if no changes have been made.

* Popup dialog windows now appear within the frame of foton instead of some 
  random spot on the screen.

* Ported to Mac OS X (10.6.5).  

Version 2.15.0, May 2011

* Filter modules are now sorted before creating the module selection menu.

* Add matlab merge file capability to foton, documented in the foton man page.

* Fixed bugzilla #324, making sure that plot option channel A and B list boxes 
  have a minimum size to make them useable.  


$Id: foton_changes 7275 2015-01-09 17:24:07Z james.batch@LIGO.ORG $
