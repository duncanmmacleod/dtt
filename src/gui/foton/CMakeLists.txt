
add_library(foton_obj OBJECT
        foton.cc)


target_include_directories(foton_obj PUBLIC
        ${EXTERNAL_INCLUDE_DIRECTORIES}
        )
#turned into an executable by a parent directory


#handle python installation
configure_file(foton.py.in foton.py @ONLY )


if(NOT NO_PYTHON_INSTALL)
    MESSAGE(STATUS "Creating install target for foton python files")
    INSTALL(FILES ${CMAKE_CURRENT_BINARY_DIR}/foton.py
            DESTINATION ${Python2_SITELIB}
            )

    INSTALL(FILES ${CMAKE_CURRENT_BINARY_DIR}/foton.py
            DESTINATION ${Python3_SITELIB}
            )
endif()

