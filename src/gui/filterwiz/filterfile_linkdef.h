#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;

#pragma link C++ class filterwiz::FilterFile;
#pragma link C++ class filterwiz::FilterModule;
#pragma link C++ class filterwiz::FilterSection;

#endif