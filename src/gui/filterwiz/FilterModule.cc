/* version $Id: FilterModule.cc 8034 2018-07-12 18:51:43Z john.zweizig@LIGO.ORG $ */
#include <ctype.h>
#include <string.h>
#include <strings.h>
//#include "FilterModule.hh"
#include "FilterFile.hh" // Includes FilterModule.hh - JCB
#include "IIRFilter.hh"
#include "iirutil.hh"
#include <string>
#include <iostream>
#include <cstdarg>


   using namespace std;
   using namespace ligogui;

namespace filterwiz {


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// FilterModule							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   FilterModule::FilterModule () : fSample (1)
   {
      // This simply adds kMaxFilterSections FilterSections to the
      // vector of FilterSections fSect.
      for (int i = 0; i < kMaxFilterSections; ++i) {
         fSect.push_back (FilterSection (fSample, i));
      }
      // Clear the vector of error messages.
      clearErrors() ;
   }

//______________________________________________________________________________
   FilterModule::FilterModule (const char* name, double fsample)
   : fName (name ? name : ""), fSample (fsample)
   {
    // Add kMaxFilterSections FilterSections to the vector of
    // FilterSections fSect.
      for (int i = 0; i < kMaxFilterSections; ++i) {
         fSect.push_back (FilterSection (fSample, i));
      }
      // Clear the vector of error messages.
      clearErrors() ;
   }

//______________________________________________________________________________
   void FilterModule::checkDesign()
   {
      int split = 0;
      bool noDesign = false ;	// Flag to indicate that no design string was provided.
   
      for (int i = kMaxFilterSections - 1; i >= 0; --i) 
      {
	 noDesign = false ;

	 if ( fSect[i].refDesign() == "split") 
	 {
            ++split;
            continue;
         }
         // get filter from design string
         IIRFilter iir_design;
	 // Create a new filter with the sample rate.
         FilterDesign design (fSample);
	 // Make a filter from the design string, add it to the filter.
	 // getDesign() gets the design string from the filter section.
	 if (fSect[i].getDesign() != (char *) NULL && fSect[i].getDesign()[0] == '\0')
	 {
	    noDesign = true ;
	    // errorMessage("Warning: Filter module %s section %d design not specified. \n  A new design will be generated from coefficients.", getName(), i) ;
	    // If there's no design and the filter is marked as gain-only, we need to create a design string anyway.
	    if (fSect[i].getGainOnly())
	    {
	       // A design string of the form zpk([],[],<gain>,"n") will do.
	       string s = "zpk([],[]," + fSect[i].getGainOnlyGain() + ",\"n\")" ;
	       cerr << "No design string for gain only filter "
		    << fSect[i].getName() << ", creating one: " << s << endl;
	       errorMessage("Module %s section %d: Missing design string, "
			    "a new string will be generated.", getName(), i);
	       fSect[i].setDesign(s) ;
	    }
	 }
         bool ok = design.filter (fSect[i].getDesign());
         if (ok) 
	 {
            try {
	    	// design.get() gets the filter as a Pipe &.
		// iir2iir() at it's simplest will cast the Pipe& to an IIRFilter and return.
               iir_design = iir2iir (design.get());
            } 
	    catch (...) 
	    {
	       ok = false;
	       errorMessage("Filter module %s section %d: Failed to create "
			    "filter from design string %s",
			    getName(), i, fSect[i].getDesign()) ;
#if 0
	       // Add the error messages from the FilterDesign, if any.
	       if (!design.errorsEmpty())
	       {
		  std::vector<std::string> *fdErrors = design.getErrors() ;
		  for (std::vector<std::string>::iterator k = fdErrors->begin(); k != fdErrors->end(); ++k)
		  {
		     errorMessage(k->c_str()) ;
		  }
	       }
#endif
	    }
         }
         // get filter from coefficients
         IIRFilter iir_coeff;
         if (ok) 
	 {
            for (int j = i; j <= i + split; ++j) {
               try {
                  IIRFilter iir = iir2iir (fSect[j].filter().get());
                  iir_coeff *= iir;
               } 
	       catch (...) 
	       {
		  ok = false;
		  errorMessage("Filter module %s section %d: Failed to create filter from coefficients.", getName(), i) ;
#if 0
		  // Add the error messages from the IIRFilter class, if any.
		  if (!iir_coeff.errorsEmpty())
		  {
		     std::vector<std::string> *iirErrors = iir_coeff.getErrors() ;
		     for (std::vector<std::string>::iterator k = iirErrors->begin(); k != iirErrors->end(); ++k)
		     {
			errorMessage(k->c_str()) ;
		     }
		  }
#endif
		  break;
	       }
            }
         }
         // now compare the two
         if (ok) {
	    // iircmp() is found in SignalProcessing/IIRFilter/iirutil.cc
            ok = iircmp (iir_design, iir_coeff);
	    if (!ok)
	    {
	       if (noDesign)
		  errorMessage("Module %s section %d: Missing design string, a new string will be generated.", getName(), i) ;
	       else
		  errorMessage("Module %s section %d: Mismatch between design and coefficients.", getName(), i) ;
	    }
         }
         // if not ok, patch in some names
         if (!ok) {
            cerr << "Filter module " << getName() << " section " << i <<
               ": mismatch between design and coefficients" << endl;
            for (int j = i; j <= i + split; ++j) {
               string cmd;
               if (iir2zpk (fSect[j].filter().get(), cmd, "n") ||
                  iir2z (fSect[j].filter().get(), cmd, "s")) 
	       {
                  fSect[j].setDesign (cmd.c_str());
               }
               else 
	       {
                  fSect[j].setDesign ("");
                  fSect[j].filter().reset();
               }
            }
         }
         split = 0;
      }
   }

//______________________________________________________________________________
   void FilterModule::changeSampleRate(double newSample)
   {
      int split = 0;
   
      for (int i = kMaxFilterSections - 1; i >= 0; --i) 
      {
	 if ( fSect[i].refDesign() == "split") 
	 {
            ++split;
            continue;
         }
         // get filter from design string
         IIRFilter iir_design;
	 // Create a new filter with the new sample rate.
         FilterDesign design (newSample);

	 // Make a filter from the design string, add it to the filter.
	 // getDesign() gets the design string from the filter section.
	 if (fSect[i].getDesign() != (char*)NULL && fSect[i].getDesign()[0] == '\0')
	 {
	    // If there's no design and the filter is marked as gain-only,
	    // we need to create a design string anyway.
	    if (fSect[i].getGainOnly())
	    {
	       // A design string of the form zpk([],[],<gain>,"n") will do.
	       string s = "zpk([],[]," + fSect[i].getGainOnlyGain() + ",\"n\")";
	       cerr << "No design string for gain only filter "
		    << fSect[i].getName() << ", creating one: " << s << endl ;
	       errorMessage("Module %s section %d: Missing design string, "
			    "a new string will be generated.", getName(), i) ;
	       fSect[i].setDesign(s) ;
	    }
	 }
         bool ok = design.filter (fSect[i].getDesign());
         if (ok) 
	 {
            try {
	    	// design.get() gets the filter as a Pipe&.
		// iir2iir() at it's simplest will cast the Pipe& to an
	        //           IIRFilter and return.
               iir_design = iir2iir(design.get());
            } 
	    catch (...) 
	    {
	       ok = false;
	       errorMessage("Filter module %s section %d: Failed to create "
			    "filter from design string %s",
			    getName(), i, fSect[i].getDesign());
	    }
         }

	 // Replace the old filter with the new filter.
	 fSect[i].filter() = design;

	 fSect[i].update();
      }
   }

//______________________________________________________________________________
   void FilterModule::setName (const char* p) 
   {
      fName = p;
      string::size_type pos;
      while ((pos = fName.find_first_of (" \n\f\r\t\v")) != string::npos) {
         fName.erase (pos, 1);
      }
   }

//______________________________________________________________________________
   void FilterModule::setFSample (double sample) 
   {
      fSample = sample; 
      for (int i = 0; i < kMaxFilterSections; ++i) {
         fSect[i].filter().setFSample (fSample);
      }
   }

//______________________________________________________________________________
// This should be removed...
   void FilterModule::defaultFSample (void) 
   {
      setFSample (16384);
   }

//______________________________________________________________________________
   bool FilterModule::operator<(const FilterModule &b) const
   {
      return (strcmp(fName.c_str(), b.getName()) < 0);
   }

//______________________________________________________________________________
   bool FilterModule::operator==(const FilterModule &b) const
   {
      return (!strcmp(fName.c_str(), b.getName()));
   }


// JCB - start
//______________________________________________________________________________
    // Copy the design data for each filter section in the module before paste.
    void FilterModule::SaveSections()
    {
        // Clear out the old data.
        fRevertSect.clear() ;

        // Copy each section, even if it's empty.
        for (int i = 0; i < kMaxFilterSections; i++)
        {
            fRevertSect.push_back(SectCopy(&(fSect[i]))) ;
        }
    }

//______________________________________________________________________________
    // Restore the saved design data for each filter section, used in case an
    // error was made in the paste operation (user error)
    // If successful, an Update (1, 1) should be called next to finish the job.
    bool FilterModule::RestoreSections()
    {
        if (fRevertSect.empty())
        {
            return 0 ;
        }
        else
        {
            for (int i = 0; i < kMaxFilterSections; i++)
            {
                // Paste the section design data into the filter section.
                fRevertSect[i].PasteSection(&(fSect[i])) ;
            }
	    // Undo has put the sections back, clear the vector.
	    fRevertSect.clear() ;
	
        }
        return 1 ;
    }

//______________________________________________________________________________
   // Indicate if the saved design data set is empty.  Returns kTRUE if empty.
   bool FilterModule::RestoreSectionsEmpty()
   {
      return fRevertSect.empty() ;
   }

// JCB - end
//______________________________________________________________________________
  void FilterModule::errorMessage(const char *msg, ...)
  {
      char              msgbuf[1024] ;
      va_list           argp ;

      // Textbook case of handling variable arguments being passed to another
      // function that takes variable arguments.
      va_start(argp, msg) ;
      vsprintf(msgbuf, msg, argp) ;
      va_end(argp) ;

      string errstr(msgbuf) ;
      errors.push_back(errstr) ;
  }
}
