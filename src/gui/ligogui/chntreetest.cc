VCSID("$(#)$Id: chntreetest.cc 7338 2015-04-14 23:24:47Z ed.maros@LIGO.ORG $");
/*---------------------------------------------------------------------------*/
/*                                                                           */
/* Module Name:  mainmenu						     */
/*                                                                           */
/* Module Description:  ROOT gui for main diagnostics test menu		     */
/*                                                                           */
/*---------------------------------------------------------------------------*/

/* Header File List */

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include <unistd.h>
#include <iostream>

#include <TROOT.h>
#include <TApplication.h>
#include <TControlBar.h>
#include <TVirtualX.h>

#include "TLGChannelBox.hh"
#include <TGClient.h>
#include <TGString.h>
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGButton.h>
#include <TGTextEntry.h>
#include <TGMsgBox.h>
#include <TGMenu.h>
#include <TGComboBox.h>
#include <TGFileDialog.h>
#include <TSystem.h>
#include <TString.h>
#include <TEnv.h>


   using namespace ligogui;
   using namespace std;


   const char* names = "H0:PEM-xxx 1 H1:SUS-DDD 32 H0:PEM-ss 2 H1:LSC-eee 1024 "
   "H2:ASC-tt1212334434324 4 H1:SUS-yeyeye 4 H1:SUS-22222 4 H1:test_1 4 "
   "H1:SUS-uuuuuu 32 H1:SUS-u1 32 H1:SUS-u2 32 H1:SUS-u3 32 H1:SUS-u4 32 H1:SUS-u5 32 "
   "H1:SUS-u6 4 H1:SUS-u7 4 H1:SUS-u8 4 H1:SUS-u9 4 H1:SUS-u10 4 H1:SUS-u11 4 "
   "H1:SUS-u13 32 H1:SUS-u14 32 H1:SUS-u15 32 H1:SUS-u16 32 H1:SUS-u17 32 H1:SUS-u18 32 "
   "H1:LSC-x11 64 H1:LSC-x12 64 H1:LSC-x13 64 H1:LSC-x14 64 H1:LSC-x15 64 H1:LSC-x16 64 "
   "H1:LSC-x17 64 H1:LSC-x8 64 H1:LSC-x1 64 H19:LSC-x1 64 H10:LSC-x1 64 H1:LSC-x1 64 "
   "H1:LSC-yy11 64 H1:LSC-yy12 64 H1:LSC-yy13 64 H1:LSC-yy14 64 H1:LSC-yy15 64 H1:LSC-yy16 64 "
   "H1:LSC-yy17 64 H1:LSC-yy8 64 H1:LSC-yy1 64 H19:LSC-yy1 64 H10:LSC-yy1 64 H1:LSC-yy1 64 "
   "H1:ASC-x11 64 H1:ASC-x12 64 H1:ASC-x13 64 H1:ASC-x14 64 H1:ASC-x15 64 H1:ASC-x16 64 "
   "H1:ASC-x17 64 H1:ASC-x8 64 H1:ASC-x1 64 H19:ASC-x1 64 H10:ASC-x1 64 H1:ASC-x1 64 "
   "H1:ASC-yy11 64 H1:ASC-yy12 64 H1:ASC-yy13 64 H1:ASC-yy14 64 H1:ASC-yy15 64 H1:ASC-yy16 64 "
   "H1:ASC-yy17 64 H1:ASC-yy8 64 H1:ASC-yy1 16 H2:ASC-yy1 64 H10:ASC-yy1 64 H1:ASC-yy1 ";


   class MainMainFrame : public TGMainFrame {
   private:
      TLGChannelListbox*	fChn1;
      TLGChannelCombobox* 	fChn2;
      TGCompositeFrame*	 	fTextFrame;
      TGLayoutHints*     	fL1;
      TGButton*			fExitButton;
   
   public:
      MainMainFrame (const TGWindow *p, UInt_t w, UInt_t h);
      virtual ~MainMainFrame();
   
      virtual void CloseWindow();
      virtual Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t);
   };


   MainMainFrame::MainMainFrame (const TGWindow *p, UInt_t w, UInt_t h)
   : TGMainFrame (p, w, h)
   {
   /*------------------------------------------------------------------------*/
   /* Set font for label text.                                               */
   /*------------------------------------------------------------------------*/
   
      FontStruct_t labelfont;
      labelfont = gClient->GetFontByName 
         (gEnv->GetValue
         ("Gui.NormalFont",
         "-adobe-helvetica-medium-r-*-*-12-*-*-*-*-*-iso8859-1"));
   
   /* Define new graphics context. Only the fields specified in
      fMask will be used. */
   
      GCValues_t   gval;
      gval.fMask = kGCForeground | kGCFont;
      gval.fFont = gVirtualX->GetFontHandle(labelfont);
   
      // Channel Tree
      fTextFrame = new TGCompositeFrame(this, 320, 220, kHorizontalFrame |
                           kSunkenFrame);
      fChn1 = new TLGChannelListbox (fTextFrame, 10, names);
      fChn2 = new TLGChannelCombobox (fTextFrame, 11, names, 
                           kChannelTreeShowRate | kChannelTreeSeparateSlow, kTRUE);
   
      fChn1->Resize (300, 200);
      fChn2->SetPopupHeight (200);
      fChn2->Resize(300, 22);
      fChn1->Associate(this);
      fChn2->Associate(this);
      fChn1->SelectChannel ("H1:LSC-eee");
      fChn2->Select (0);
   
      fL1 = new TGLayoutHints (kLHintsCenterX | kLHintsTop, 2, 2, 5, 5);
      fTextFrame->AddFrame (fChn1, fL1); 
      fTextFrame->AddFrame (fChn2, fL1); 
      AddFrame (fTextFrame, fL1);
   
      fExitButton = new TGTextButton (this, "E&xit", 1);
      fExitButton->Associate (this);
      AddFrame (fExitButton, fL1);
   
      SetWindowName ("Diagnostics");
      SetIconName ("Diagnostics");
      SetClassHints ("Diagnostics", "Diagnostics");
      SetWMPosition (0,0);
      MapSubwindows ();
   
      // we need to use GetDefault...() to initialize the layout algorithm...
      Resize (GetDefaultSize());
      MapWindow ();
   }


   MainMainFrame::~MainMainFrame()
   {
      delete fChn1;
      delete fChn2;
      delete fTextFrame;
      delete fExitButton;
      delete fL1;
   }


   Bool_t MainMainFrame::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      switch (GET_MSG (msg)) {
         case kC_COMMAND:
            {
               switch (GET_SUBMSG (msg)) {
                  case kCM_BUTTON:
                     {
                        switch (parm1) {
                           case 1:
                              const char* chn = fChn1->GetSelectedChannel();
                              cout << "selection 1 = " << 
                                 (chn ? chn : "--none--") << endl;
                              chn = fChn2->GetChannel();
                              cout << "selection 2 = " << 
                                 (chn ? chn : "--none--") << endl;
                              CloseWindow();
                              break;
                        }
                     }
                  case kCM_MENU: 
                     {
                     }
               }
               break;
            }
      }
      return kTRUE;
   }


   void MainMainFrame::CloseWindow()
   {
   /* Got close message for this MainFrame. Calls parent CloseWindow()
    (which destroys the window) and terminate the application.
    The close message is generated by the window manager when its close
    window menu item is selected. */
   
      TGMainFrame::CloseWindow();
      gApplication->Terminate(0);
   }


   TROOT root("GUI", "AWG GUI");



   int main(int argc, char **argv)
   {
      TApplication theApp ("Diagnostics tests", &argc, argv);
      MainMainFrame mainWindow (gClient->GetRoot(), 600, 240);
      theApp.Run();
   
      return 0;
   }
