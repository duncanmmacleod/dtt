/* Version $Id: TLGTextEditor.hh 6319 2010-09-17 18:06:52Z james.batch@LIGO.ORG $ */
/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef _LIGO_TLGTEXTEDITOR_H
#define _LIGO_TLGTEXTEDITOR_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGTextEditor						*/
/*                                                         		*/
/* Module Description: Simple text editor				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 1.0	 16Apr02  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGTextEditor.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include "TLGFrame.hh"
#include <TGFrame.h>
#include <TGWindow.h>
#include <TGWidget.h>
#include <TGMenu.h>
#include <TGText.h>
#include <TGTextEdit.h>
#include <TGButton.h>
#include <string>

namespace ligogui {


#ifndef _kTE_TEXTUPDATED
#define _kTE_TEXTUPDATED
   /// Submessage identification number for an updated text entry field
   const int kTE_TEXTUPDATED = 3;
#endif


/** @name TLGTextEdit
    A text editor similar to TGTextEdit but with the added capbility to 
    become inactive.
    The text editor can generate the following additional message:
    \begin{verbatim}
    kC_TEXTENTRY, kTE_TEXTUPDATED, widget id, 0
    \end{verbatim}
    A kTE_TEXTUPDATED message is generated when the text is changed.

    @memo Text Editor
    @author Written April 2002 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGTextEdit : public TGTextEdit {
   protected:
      /// State
      Bool_t		fState;
      /// Draw cursor
      virtual void DrawCursor (Int_t mode);
   
   public:
      /// Create TLGTextEdit widget
      TLGTextEdit (const TGWindow* parent, UInt_t w, UInt_t h, 
                  Int_t id = -1, UInt_t sboptions = 0, 
                  ULong_t back = GetWhitePixel());
      /// Create TLGTextEdit widget
      TLGTextEdit (const TGWindow* parent, UInt_t w, UInt_t h, 
                  TGText* text, Int_t id = -1, UInt_t sboptions = 0, 
                  ULong_t back = GetWhitePixel());
      /// Create TLGTextEdit widget
      TLGTextEdit (const TGWindow* parent, UInt_t w, UInt_t h, 
                  const char* string, Int_t id = -1, 
                  UInt_t sboptions = 0, ULong_t back = GetWhitePixel());
      /// Set state of widget
      void SetState (Bool_t state = kTRUE);
      /// Send update message
      void SendUpdateMsg();
      /// Handle input keys differently if inactive
      virtual Bool_t HandleKey (Event_t* event);
      /// Get clipboard
      TGText* GetClipboard() {
         return fClipText; }
      /// Get the complete text from the edit-box
      std::string GetString(char cmt=0) const;

      /// Insert text at current position
      virtual Bool_t InsertText (TGText* text, Bool_t mark = kFALSE);
      /// Get filename
      const char* GetFileName() const {
         return fText->GetFileName(); }
   };


/** @name TLGTextEditor
    As simple text editor for entering and changing multiline
    text. Comes up as a transient frame with menu if modeless is
    false. If modeless is true it comes up as a stand-alone
    window.

    The text editor can generate the following messages in
    modeless operations:
    \begin{verbatim}
    kC_TEXTENTRY, kTE_TEXTUPDATED, widget id, 0
    \end{verbatim}
    A kTE_TEXTUPDATED message is generated when the update or ok
    button is pressed.

    @memo Text Editor
    @author Written April 2002 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGTextEditor : public TLGTransientFrame, public TGWidget {
   public:
      /// Constructor
      TLGTextEditor (const TGWindow *p, const TGWindow *main,
                    const char* title, UInt_t w, UInt_t h, 
                    TGText& text, Bool_t& ret, Bool_t modeless = kFALSE, 
                    Int_t id = 0, Bool_t* done = 0);
      /// Desctructor
      ~TLGTextEditor();
      /// Close window method
      virtual void CloseWindow();
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   
   protected:
      /// Title
      TString			fTitle;
      /// Return argument
      Bool_t*			fRet;
      /// true if done
      Bool_t*			fDone;
      /// Return string
      TGText*			fText;
      /// Maim menu
      TGMenuBar*		fMenu;
      /// File menu
      TGPopupMenu*		fMenuFile;
      /// Edit menu
      TGPopupMenu*		fMenuEdit;
      /// Search menu
      TGPopupMenu*		fMenuSearch;
      /// Text edit widget
      TLGTextEdit*		fTextEdit;
      /// Text search widget
      TGTextEdit*		fTextSearch;
      /// Text edit widget
      TGButton*			fButton[3];
   };



//@}
}

#endif // _LIGO_TLGTEXTEDITOR_H
