
/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "TLGEntry.hh"
#include <math.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <KeySymbols.h>
#include <TMath.h>
#include <TTimer.h>
#include <TSystem.h>
#include <TGToolTip.h>
#include <TGMsgBox.h>
#include <TGLabel.h>	// JCB
#include <TGButton.h>	// JCB
#include <TGTextEntry.h>	// JCB
#include <TGComboBox.h>	// JCB
#include <TGListView.h> // JCB
#include <TGFSContainer.h>	// JCB
#include <TGFSComboBox.h>	// JCB
#include <TGInputDialog.h>	// JCB
#include <TObjString.h>	//
#include <TVirtualX.h>

namespace ligogui {
   using namespace std;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGTextEntry                                                         //
//                                                                      //
// Modified text entry field (new update text notification message      //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGTextEntry::TLGTextEntry (const TGWindow *p, TGTextBuffer *text, 
   	          Int_t id, GContext_t norm, FontStruct_t font,
                  UInt_t option, ULong_t back)
   : TGTextEntry (p, text, id, norm, font, option, back), 
   fOldText (text->GetString()) {
   }

//______________________________________________________________________________
   Bool_t TLGTextEntry::HandleFocusChange (Event_t* event)
   {
      Bool_t status = TGTextEntry::HandleFocusChange (event);
      if (!IsEnabled()) {
         return status;
      }
   
   // ADDED UPDATE NOTIFICATION
      if ((event->fCode == kNotifyNormal) && 
         (event->fState != kNotifyPointer)) {
         TString text = GetText();
         if ((event->fType == kFocusOut) && (fOldText != text)) {
            TextUpdated();
         }
         fOldText = text;
      }
   
      return status;
   }

//______________________________________________________________________________
   void TLGTextEntry::ReturnPressed () 
   {
      TGTextEntry::ReturnPressed();
   // ADDED UPDATE NOTIFICATION
      TString text = GetText();
      if (fOldText != text) {
         TextUpdated();
         fOldText = text;
      }
   }

//______________________________________________________________________________
   void TLGTextEntry::TextUpdated () 
   {
   // ADDED MESSAGE kTE_TEXTUPDATED
      SendMessage (fMsgWindow, MK_MSG (kC_TEXTENTRY, 
                                    (EWidgetMessageTypes) kTE_TEXTUPDATED), 
                  fWidgetId, 0);
   }

//______________________________________________________________________________
   void TLGTextEntry::UpdateOffset ()
   {
   // Updates start text offset according GetAlignment() mode,
   // if cursor is out of frame => scroll the text.
   // See also SetAlignment() and ScrollByChar().
   
      TString dt = GetDisplayText();
      Int_t textWidth = gVirtualX->TextWidth(fFontStruct, dt.Data() , dt.Length());
      Int_t offset = IsFrameDrawn() ? 4 : 0;
   // SUBTRACT BORDER
      Int_t w = GetWidth() - 2 * offset;
   
      if (textWidth > w) {                          // may need to scroll.
         if (IsCursorOutOfFrame()) ScrollByChar();
      }
      // SUBTRACT 1
      else if (fAlignment == kTextRight)   fOffset = w - textWidth - 1;
      else if (fAlignment == kTextCenterX) fOffset = (w - textWidth)/2;
      else if (fAlignment == kTextLeft)    fOffset = 0;
   }

//______________________________________________________________________________
   void TLGTextEntry::SetCursorPosition (Int_t newPos)
   {
   // Set the cursor position to newPos.
   // See also NewMark().
   
      Int_t offset =  IsFrameDrawn() ? 4 : 0;
      if (GetEchoMode() == kNoEcho) { fCursorX = offset; 
         return; }
   
      TString dt =  GetDisplayText();
   // ADDED OFFSET UPDATE
      UpdateOffset();
   
      Int_t x = fOffset + offset;
      Int_t len = dt.Length();
   
      Int_t pos = newPos < len ? newPos : len;
      fCursorIX = pos < 0 ? 0 : pos;
   
      fCursorX = x + gVirtualX->TextWidth(fFontStruct, dt.Data() , fCursorIX);
   
      if (!fSelectionOn){
         fStartIX = fCursorIX;
         fStartX  = fCursorX;
      }
   }

//______________________________________________________________________________
   void TLGTextEntry::Layout ()
   {
   // ADDED UPDATE OFFSET
      UpdateOffset();
      TGTextEntry::Layout();
   }

//______________________________________________________________________________
#if ROOT_VERSION_CODE < ROOT_VERSION(5,22,0)
   void TLGTextEntry::SetText (const char* text)
   {
   // ADDED OLD TEXT
      fOldText = text;
      TGTextEntry::SetText (text);
   }
#else
  void TLGTextEntry::SetText (const char* text, bool emit)
   {
   // ADDED OLD TEXT
      fOldText = text;
      TGTextEntry::SetText (text, emit);
   }
#endif

#if ROOT_VERSION_CODE < ROOT_VERSION(5,22,0)
   // JCB added SetText(const char *, Bool_t)
   void TLGTextEntry::SetText (const char *text, Bool_t emit)
   {
      // Provide selective emit capability in older versions of root.
      // Sets text entry to text, clears the selection and moves
      // the cursor to the end of the line.
      // If necessary the text is truncated to fit MaxLength().

      TString oldText(GetText()) ;

      fOldText = text ;
      fText->Clear() ;
      fText->AddText(0, text) ;

      Int_t dif = fText->GetTextLength() - fMaxLen ;
      if (dif > 0) fText->RemoveText(fMaxLen, dif) ;

      End(kFALSE) ;
      if (oldText != GetText())
      {
	 if (emit)
	    TextChanged() ;	// emit signal
	 fClient->NeedRedraw(this) ;
      }
   }
#endif
//______________________________________________________________________________
   void TLGTextEntry::SetState (Bool_t state)
   {
   // REMOVE CURSOR ON STATE CHANGE
      Event_t event;
      event.fCode = kNotifyNormal;
      event.fState = 0;
      event.fType = kFocusOut;
      HandleFocusChange (&event);
      TGTextEntry::SetState (state);
   }


//______________________________________________________________________________
   void TLGTextEntry::SetMaxLength (Int_t maxlen)
   {
      fMaxLen = (maxlen < 0) ? 0 : maxlen;
      Int_t diff = fText->GetTextLength() - fMaxLen;
      if (diff > 0) fText->RemoveText (fMaxLen, diff);
      SetCursorPosition (0);
      Deselect();
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Miscellanous routines for handling numeric values <-> strings        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////


   /// Style of real
   enum RealStyle {
   /// Integer
   kRSInt = 0,
   /// Fraction only
   kRSFrac = 1,
   /// Exponent only
   kRSExpo = 2,
   /// Fraction and Exponent
   kRSFracExpo = 3
   };

   struct RealInfo_t {
      /// Style of real
      RealStyle	fStyle;
      /// Number of fractional digits
      Int_t	fFracDigits;
      /// Base of fractional digits
      Int_t	fFracBase;
      /// Integer number
      Int_t	fIntNum;
      /// Fraction
      Int_t	fFracNum;
      /// Exponent
      Int_t	fExpoNum;
      /// Sign
      Int_t	fSign;
   };

   const Double_t kEpsilon = 1E-12;

   const Int_t mDays[13] = 
   {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

//______________________________________________________________________________
   Long_t Round (Double_t x)
   {
      if (x > 0) {
         return (Long_t) (x + 0.5);
      }
      else if (x < 0) {
         return (Long_t) (x - 0.5);
      }
      else {
         return 0;
      }
   }

//______________________________________________________________________________
   Long_t Truncate (Double_t x)
   {
      if (x > 0) {
         return (Long_t) (x + kEpsilon);
      }
      else if (x < 0) {
         return (Long_t) (x - kEpsilon);
      }
      else {
         return 0;
      }
   }

//______________________________________________________________________________
   Bool_t IsLeapYear (Int_t year) 
   {
      return ((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0)));
   }

//______________________________________________________________________________
   Bool_t IsGoodChar (char c, ENumericEntryStyle style,
                     ENumericEntryAttributes attr) 
   {
      if (isdigit (c)) {
         return kTRUE;
      }
      if (isxdigit (c) && (style == kNESHex)) {
         return kTRUE;
      }
      if ((c == '-') && (style == kNESInteger) &&
         (attr == kNEAAnyNumber)) {
         return kTRUE;
      }
      if ((c == '-') && 
         ((style == kNESRealOne) || (style == kNESRealTwo) || 
         (style == kNESRealThree) || (style == kNESRealFour) || 
         (style == kNESReal) || (style == kNESDegree) || 
         (style == kNESMinSec)) && (attr == kNEAAnyNumber)) {
         return kTRUE;
      }
      if ((c == '-') && (style == kNESReal)) {
         return kTRUE;
      }
      if (((c == '.') || (c == ',')) && 
         ((style == kNESRealOne) || (style == kNESRealTwo) || 
         (style == kNESRealThree) || (style == kNESRealFour) || 
         (style == kNESReal) || (style == kNESDegree) || 
         (style == kNESMinSec) || (style == kNESHourMin) ||
         (style == kNESHourMinSec) || (style == kNESDayMYear) ||
         (style == kNESMDayYear))) {
         return kTRUE;
      }
      if ((c == ':') && 
         ((style == kNESDegree) || 
         (style == kNESMinSec) || (style == kNESHourMin) ||
         (style == kNESHourMinSec) || (style == kNESDayMYear) ||
         (style == kNESMDayYear))) {
         return kTRUE;
      }
      if ((c == '/') && 
         ((style == kNESDayMYear) || (style == kNESMDayYear))) {
         return kTRUE;
      }
      if (((c == 'e') || (c == 'E')) && (style == kNESReal)) {
         return kTRUE;
      }
      return kFALSE;
   }

//______________________________________________________________________________
   char* EliminateGarbage (char* text, ENumericEntryStyle style,
                     ENumericEntryAttributes attr) 
   {
      if (text != 0) {
	 Int_t j = 0;
	 for (Int_t i = 0; text[i]; i++) {
	    if (IsGoodChar (text[i], style, attr)) {
	       if (i != j) text[j] = text[i];
	       j++;
	    }
	 }
	 text[j] = 0;
      }
      return text;
   }

//______________________________________________________________________________
   Long_t IntStr (const char* text)
   {
      Long_t	l = 0;
      Int_t	Sign = 1;
      for (UInt_t i = 0; i < strlen (text); i++) {
         if (text[i] == '-') {
            Sign = -1;
         }
         else if ((isdigit (text[i])) && (l < 100000000)) {
            l = 10 * l + (text[i] - '0');
         }
      }
      return Sign * l;
   }

//______________________________________________________________________________
   char* StrInt (char* text, Long_t i, Int_t Digits) 
   {
      sprintf (text, "%li", TMath::Abs (i));
      TString s = text;
      while (Digits > s.Length()) {
         s = "0" + s;
      }
      if (i < 0) {
         s = "-" + s;
      }
      strcpy (text, (const char*) s);
      return text;
   }

//______________________________________________________________________________
   TString StringInt (Long_t i, Int_t Digits)
   {
      char text [256];
      StrInt (text, i, Digits);
      return TString (text);
   }

//______________________________________________________________________________
   char* RealToStr (char* text, const RealInfo_t& ri)
   {
      char* p = text;
      if (text == 0) {
         return 0;
      }
      strcpy (p, "");
      if (ri.fSign < 0) {
         strcpy (p, "-");
         p++;
      }
      StrInt (p, TMath::Abs (ri.fIntNum), 0);
      p += strlen (p);
      if ((ri.fStyle == kRSFrac) || (ri.fStyle == kRSFracExpo)) {
         strcpy (p, ".");
         p++;
         StrInt (p, TMath::Abs (ri.fFracNum), ri.fFracDigits);
         p += strlen (p);
      }
      if ((ri.fStyle == kRSExpo) || (ri.fStyle == kRSFracExpo)) {
         strcpy (p, "e");
         p++;
         StrInt (p, ri.fExpoNum, 0);
         p += strlen (p);
      }
      return text;
   }

//______________________________________________________________________________
   Double_t StrToReal (const char* text, RealInfo_t& ri)
   {
      char*	s;
      char*	Frac;
      char*	Expo;
      char*	Minus;
      char	buf[256];
   
      if ((text == 0) || (strlen (text) == 0)) {
         ri.fStyle = kRSInt;
         ri.fIntNum = 0;
         ri.fSign = 1;
         return 0.0;
      }
      strncpy (buf, text, sizeof (buf) - 1);
      buf[sizeof (buf) - 1] = 0;
      s = buf;
      Frac = strchr (s, '.');
      if (Frac == 0) {
         Frac = strchr (s, ',');
      }
      Expo = strchr (s, 'e');
      Minus = strchr (s, '-');
      if (Expo == 0) {
         Expo = strchr (s, 'E');
      }
      if ((Frac != 0) && (Expo != 0) && (Frac > Expo)) {
         Frac = 0;
      }
      if ((Minus != 0) && ((Expo == 0) || (Minus < Expo))) {
         ri.fSign = -1;
      }
      else {
         ri.fSign = 1;
      }
      if ((Frac == 0) && (Expo == 0)) {
         ri.fStyle = kRSInt;
      }
      else if (Frac == 0) {
         ri.fStyle = kRSExpo;
      }
      else if (Expo == 0) {
         ri.fStyle = kRSFrac;
      }
      else {
         ri.fStyle = kRSFracExpo;
      }
      if (Frac != 0) {
         *Frac = 0;
         Frac++;
      }
      if (Expo != 0) {
         *Expo = 0;
         Expo++;
      }
      ri.fIntNum = TMath::Abs (IntStr (s));
      if (Expo != 0) {
         ri.fExpoNum = IntStr (Expo);
      }
      else {
         ri.fExpoNum = 0;
      }
      if (ri.fExpoNum > 999) {
         ri.fExpoNum = 999;
      }
      if (ri.fExpoNum < -999) {
         ri.fExpoNum = -999;
      }
      ri.fFracDigits = 0;
      ri.fFracBase = 1;
      ri.fFracNum = 0;
      if (Frac != 0) {
         for (UInt_t i = 0; i < strlen (Frac); i++) {
            if (isdigit (Frac[i])) {
               if (ri.fFracNum < 100000000) {
                  ri.fFracNum = 10 * ri.fFracNum + (Frac[i] - '0');
                  ri.fFracDigits++;
                  ri.fFracBase *= 10;
               }
            }
         }
      }
      if ((ri.fFracDigits == 0) && (ri.fStyle == kRSFrac)) {
         ri.fStyle = kRSInt;
      }
      if ((ri.fFracDigits == 0) && (ri.fStyle == kRSFracExpo)) {
         ri.fStyle = kRSExpo;
      }
      // if (ri.fIntNum < 0) {
         // ri.fSign = -1;
      // }
      // else if (ri.fIntNum > 0) {
         // ri.fSign = 1;
      // }
      // else if (((ri.fStyle == kRSInt) || (ri.fStyle == kRSExpo)) ||
              // (((ri.fStyle == kRSFrac) || (ri.fStyle == kRSFracExpo)) &&
              // (ri.fFracNum == 0))) {
         // ri.fSign = 1;
      // }
      switch (ri.fStyle) {
         case kRSInt:
            return ri.fSign* ri.fIntNum;
         case kRSFrac:
            return ri.fSign * 
               (ri.fIntNum + (Double_t) ri.fFracNum / ri.fFracBase);
         case kRSExpo:
            return ri.fSign * (ri.fIntNum * TMath::Power (10, ri.fExpoNum));
         case kRSFracExpo:
            return ri.fSign * (ri.fIntNum + 
                              (Double_t) ri.fFracNum / ri.fFracBase) *
               TMath::Power (10, ri.fExpoNum);
      }
      return 0;
   }

//______________________________________________________________________________
   ULong_t HexStrToInt (const char* s) 
   {
      ULong_t	w = 0;
      for (UInt_t i = 0; i < strlen (s); i++) {
         if ((s[i] >= '0') && (s[i] <= '9')) {
            w = 16 * w + (s[i] - '0');
         }
         else if ((toupper (s[i]) >= 'A') && (toupper (s[i]) <= 'F')) {
            w = 16 * w + (toupper (s[i]) - 'A' + 10);
         }
      }
      return w;
   }

//______________________________________________________________________________
   char* IntToHexStr (char* text, ULong_t l) 
   {
      const char* const Digits = "0123456789ABCDEF";
      char buf[64];
      char* p = buf + 62;
      strcpy (p, "");
      while (l > 0) {
         *(--p) = Digits[l % 16];
         l /= 16;
      }
      if (strlen (p) == 0) {
         strcpy (text, "0");
      }
      else {
         strcpy (text, p);
      }
      return text;
   }

//______________________________________________________________________________
   char* mIntToStr (char* text, Long_t l, Int_t Digits)
   {
      TString	s;
      Int_t	Base;
      switch (Digits) {
         case 0: 
            Base = 1;
            break;
         case 1: 
            Base = 10;
            break;
         case 2: 
            Base = 100;
            break;
         case 3: 
            Base = 1000;
            break;
         default:
         case 4: 
            Base = 10000;
            break;
      }
      s = StringInt (TMath::Abs (l) / Base, 0) + "." + 
         StringInt (TMath::Abs (l) % Base, Digits);
      if (l < 0) {
         s = "-" + s;
      }
      strcpy (text, (const char*) s);
      return text;
   }

//______________________________________________________________________________
   char* dIntToStr (char* text, Long_t l, Bool_t Sec, char Del)
   {
      TString	s;
      if (Sec) {
         s = StringInt (TMath::Abs (l) / 3600, 0) + Del +
            StringInt ((TMath::Abs (l) % 3600) / 60, 2) + Del +
            StringInt (TMath::Abs (l) % 60, 2);
      }
      else {
         s = StringInt (TMath::Abs (l) / 60, 0) + Del +
            StringInt (TMath::Abs (l) % 60, 2);
      }
      if (l < 0) {
         s = "-" + s;
      }
      strcpy (text, (const char*) s);
      return text;
   }

//  JCB bugzilla 359
// Obviously, this function is incomplete.  And for some reason, it wasn't designed
// to convert numbers greater than 999 999 999, even though MAXINT is 2 147 483 647
// and MAXLONG is 9 223 372 036 854 775 807.  Why limit to 9 digits when it uses longs?
// A quick look will reveal that most calls to this function have a maxd1 value of 12,
// which would never be reached with the if (n1 < 100000000) condition.
//______________________________________________________________________________
   void GetNumbers (const char* s, Int_t& Sign, 
                   Long_t& n1, Int_t maxd1, 
                   Long_t& n2, Int_t maxd2, 
                   Long_t& n3, Int_t maxd3, 
                   const char* Delimiters)
   {
      Long_t	n;
      Long_t	d = 0;
      Sign = +1;
      n1 = 0;
      n2 = 0;
      n3 = 0;
      if (*s == '-') {
         Sign = -1;
         s++;
      }
      if (!isdigit (*s)) {
         return;
      }
      while ((*s != 0) && ((strchr (Delimiters, *s) == 0) || (maxd2 == 0))) {
         if (isdigit (*s) && (d < maxd1)) {
// JCB bugzilla 359            if (n1 < 100000000) {
               n1 = 10 * n1 + (*s - '0');
// JCB bugzilla 359             }
            d++;
         }
         s++;
      }
      if (strcspn (s, Delimiters) == strlen (s)) {
         return;
      }
      Int_t dummy = 0;
      GetNumbers (s + 1, dummy, n2, maxd2, n3, maxd3, n, d, Delimiters);
   }

//______________________________________________________________________________
   Long_t GetSignificant (Long_t l, Int_t Max)
   {
      while (TMath::Abs (l) >= Max) {
         l /= 10;
      }
      return l;
   }

//______________________________________________________________________________
   void AppendFracZero (char* text, Int_t Digits)
   {
      char* 	p;
      Int_t	Found = 0;
      p = strchr (text, '.');
      if (p == 0) {
         p = strchr (text, ',');
      }
      if (p == 0) {
         return;
      }
      p++;
      for (UInt_t i = 0; i < strlen (p); i++) {
         if (isdigit (*p)) {
            Found++;
         }
      }
      while (Found < Digits) {
         strcpy (p + strlen (p), "0");
         Found++;
      }
   }

//______________________________________________________________________________
   Long_t MakeDateNumber (const char* text, Long_t Day, 
                     Long_t Month, Long_t Year) 
   {
      Day = TMath::Abs (Day);
      Month = TMath::Abs (Month);
      Year = TMath::Abs (Year);
      time_t now = time (0);
      tm date;
      Int_t y = localtime_r (&now, &date)->tm_year + 1900;
      if ((y >= 2000) && (Year < 100)) {
         Year += 2000;
      }
      if ((y >= 1900) && (Year < 100)) {
         Year += 1900;
      }
      Month = GetSignificant (Month, 100);
      if (Month > 12) Month = 12;
      if (Month == 0) Month = 1;
      Day = GetSignificant (Day, 100);
      if (Day == 0) Day = 1;
      if (Day > mDays[Month]) Day = mDays[Month];
      if ((Month == 2) && (Day > 28) && !IsLeapYear (Year)) Day = 28;
      return 10000 * Year + 100 * Month + Day;
   }

//______________________________________________________________________________
   Long_t TranslateToNum (const char* text, ENumericEntryStyle style,
                     RealInfo_t& ri)
   {
      Long_t	n1;
      Long_t	n2;
      Long_t	n3;
      Int_t	sign;
//      cerr << "TranslateToNum line " << __LINE__ << " - text = " << text << endl ; // JCB
      switch (style) {
         case kNESInteger:
            GetNumbers (text, sign, n1, 12, n2, 0, n3, 0, "");
            return sign * n1;
         case kNESRealOne:
            GetNumbers (text, sign, n1, 12, n2, 1, n3, 0, ".,");
            return sign * (10 * n1 + GetSignificant (n2, 10));
         case kNESRealTwo:
            {
               char	buf[256];
               strncpy (buf, text, sizeof (buf) - 1);
               buf[sizeof (buf) - 1] = 0;
               AppendFracZero (buf, 2);
               GetNumbers (buf, sign, n1, 12, n2, 2, n3, 0, ".,");
               return sign * (100 * n1 + GetSignificant (n2, 100));
            }
         case kNESRealThree:
            {
               char	buf[256];
               strncpy (buf, text, sizeof (buf) - 1);
               buf[sizeof (buf) - 1] = 0;
               AppendFracZero (buf, 3);
               GetNumbers (buf, sign, n1, 12, n2, 3, n3, 0, ".,");
               return sign * (1000 * n1 + GetSignificant (n2, 1000));
            }
         case kNESRealFour:
            {
               char	buf[256];
               strncpy (buf, text, sizeof (buf) - 1);
               buf[sizeof (buf) - 1] = 0;
               AppendFracZero (buf, 4);
               GetNumbers (buf, sign, n1, 12, n2, 4, n3, 0, ".,");
               return sign * (10000 * n1 + GetSignificant (n2, 10000));
            }
         case kNESReal:
            return (Long_t) StrToReal (text, ri);
         case kNESDegree:
            GetNumbers (text, sign, n1, 12, n2, 2, n3, 2, ".,:");
            return sign * (3600 * n1 + 60 * GetSignificant (n2, 60) +
                          GetSignificant (n3, 60));
         case kNESHourMinSec:
            GetNumbers (text, sign, n1, 12, n2, 2, n3, 2, ".,:");
            return 3600 * n1 + 60 * GetSignificant (n2, 60) +
               GetSignificant (n3, 60);
         case kNESMinSec:
            GetNumbers (text, sign, n1, 12, n2, 2, n3, 0, ".,:");
            return sign * (60 * n1 + GetSignificant (n2, 60));
         case kNESHourMin:
            GetNumbers (text, sign, n1, 12, n2, 2, n3, 0, ".,:");
            return 60 * n1 + GetSignificant (n2, 60);
         case kNESDayMYear:
            GetNumbers (text, sign, n1, 2, n2, 2, n3, 4, ".,/");
            return MakeDateNumber (text, n1, n2, n3);
         case kNESMDayYear:
            GetNumbers (text, sign, n2, 2, n1, 2, n3, 4, ".,/");
            return MakeDateNumber (text, n1, n2, n3);
         case kNESHex:
            return HexStrToInt (text);
      }
      return 0;
   }

//______________________________________________________________________________
   char* TranslateToStr (char* text, Long_t l, ENumericEntryStyle style,
                     const RealInfo_t& ri)
   {
      switch (style) {
         case kNESInteger:
            return StrInt (text, l, 0);
         case kNESRealOne:
            return mIntToStr (text, l, 1);
         case kNESRealTwo:
            return mIntToStr (text, l, 2);
         case kNESRealThree:
            return mIntToStr (text, l, 3);
         case kNESRealFour:
            return mIntToStr (text, l, 4);
         case kNESReal:
            return RealToStr (text, ri);
         case kNESDegree:
            return dIntToStr (text, l, kTRUE, '.');
         case kNESHourMinSec:
            return dIntToStr (text, l % (24 * 3600), kTRUE, ':');
         case kNESMinSec:
            return dIntToStr (text, l, kFALSE, ':');
         case kNESHourMin:
            return dIntToStr (text, l % (24 * 60), kFALSE, ':');
         case kNESDayMYear:
            {
               TString date = StringInt (TMath::Abs (l) % 100, 0) + "/" +
                  StringInt ((TMath::Abs (l) / 100) % 100, 0) + "/" +
                  StringInt (TMath::Abs (l) / 10000, 0);
               return strcpy (text, (const char*) date);
            }
         case kNESMDayYear:
            {
               TString date = StringInt ((TMath::Abs (l) / 100) % 100, 0) + "/" +
                  StringInt (TMath::Abs (l) % 100, 0) + "/" +
                  StringInt (TMath::Abs (l) / 10000, 0);
               return strcpy (text, (const char*) date);
            }
         case kNESHex:
            return IntToHexStr (text, (ULong_t) l);
      }
      return 0;
   }

//______________________________________________________________________________
   Double_t RealToDouble (const RealInfo_t ri)
   {
      // Convert to double format;
      switch (ri.fStyle) {
         // Integer type real
         case kRSInt:
            return (Double_t) ri.fSign * ri.fIntNum;
         // Fraction type real
         case kRSFrac:
            return (Double_t) ri.fSign * (fabs (double(ri.fIntNum)) + 
                                 (Double_t) ri.fFracNum / ri.fFracBase);
         // Exponent only
         case kRSExpo:
            return (Double_t) ri.fSign * ri.fIntNum * TMath::Power (10, ri.fExpoNum);
         // Fraction and exponent
         case kRSFracExpo:
            return (Double_t) ri.fSign * (fabs (double(ri.fIntNum)) + 
                                 (Double_t) ri.fFracNum / ri.fFracBase) *
               TMath::Power (10, ri.fExpoNum);
      }
      return 0;
   }

//______________________________________________________________________________
   void CheckMinMax (Long_t& l, ENumericEntryStyle style, 
                    ENumericEntryLimits limits, Double_t min, Double_t max)
   {
      if ((limits == kNELNoLimits) || (style == kNESReal)) {
         return;
      }
      // check min
      if ((limits == kNELLimitMin) || (limits == kNELLimitMinMax)) {
         Long_t	lower;
         switch (style) {
            case kNESRealOne:
               lower = Round (10.0 * min);
               break;
            case kNESRealTwo:
               lower = Round (100.0 * min);
               break;
            case kNESRealThree:
               lower = Round (1000.0 * min);
               break;
            case kNESRealFour:
               lower = Round (10000.0 * min);
               break;
            case kNESHex:
               lower = (ULong_t) Round (min);
               break;
            default:
               lower = Round (min);
               break;
         }
         if (style != kNESHex) {
            if (l < lower) l = lower;
         }
         else {
            if (lower < 0) lower = 0;
            if ((ULong_t) l < (ULong_t) lower) l = lower;
         }
      }
      // check max
      if ((limits == kNELLimitMax) || (limits == kNELLimitMinMax)) {
         Long_t	upper;
         switch (style) {
            case kNESRealOne:
               upper = Round (10.0 * max);
               break;
            case kNESRealTwo:
               upper = Round (100.0 * max);
               break;
            case kNESRealThree:
               upper = Round (1000.0 * max);
               break;
            case kNESRealFour:
               upper = Round (10000.0 * max);
               break;
            case kNESHex:
               upper = (ULong_t) Round (max);
               break;
            default:
               upper = Round (max);
               break;
         }
         if (style != kNESHex) {
            if (l > upper) l = upper;
         }
         else {
            if (upper < 0) upper = 0;
            if ((ULong_t) l > (ULong_t) upper) l = upper;
         }
      }
   }

//______________________________________________________________________________
   void IncreaseReal (RealInfo_t& ri, Double_t mag, Bool_t logstep,
                     ENumericEntryLimits limits = kNELNoLimits, 
                     Double_t min = 0, Double_t max = 1)
   {
      // Convert to double format;
      Double_t x = RealToDouble (ri);
   
      // apply step
      if (logstep) {
         x *= mag;
      }
      else {
         switch (ri.fStyle) {
            case kRSInt:
               x = x + mag;
               break;
            case kRSFrac:
               x = x + mag / ri.fFracBase;
               break;
            case kRSExpo:
               x = x + mag * TMath::Power (10, ri.fExpoNum);
               break;
            case kRSFracExpo:
               x = x + (mag / ri.fFracBase) * TMath::Power (10, ri.fExpoNum);
               break;
         }
      }
      // check min
      if ((limits == kNELLimitMin) || (limits == kNELLimitMinMax)) {
         if (x < min) x = min;
      }
      // check max
      if ((limits == kNELLimitMax) || (limits == kNELLimitMinMax)) {
         if (x > max) x = max;
      }
   
      // check format after log step
      if ((x != 0) && logstep && (fabs (mag) > kEpsilon)) {
         for (int j = 0; j < 10; j++) {
            // Integer: special case
            if ((ri.fStyle == kRSInt) && (fabs (x) < 1) &&
               (fabs (x) > kEpsilon)) {
               ri.fStyle = kRSFrac;
               ri.fFracDigits = 1;
               ri.fFracBase = 10;
               continue;
            }
            if ((ri.fStyle == kRSInt) && (fabs (x) > 10000)) {
               ri.fStyle = kRSFracExpo;
               ri.fExpoNum = 4;
               ri.fFracDigits = 4;
               ri.fFracBase = 10000;
               Long_t rest = Round (fabs (x)) % 10000;
               for (int k = 0; k < 4; k++) {
                  if (rest % 10 != 0) {
                     break;
                  }
                  ri.fFracDigits--;
                  ri.fFracBase /= 10;
                  rest /= 10;
               }
               if (ri.fFracDigits == 0) {
                  ri.fStyle = kRSExpo;
               }
               continue;
            }
            if (ri.fStyle == kRSInt)
               break;
         
            // caluclate first digit
            Double_t y;
            if ((ri.fStyle == kRSExpo) || (ri.fStyle == kRSFracExpo)) {
               y = fabs (x) * TMath::Power (10, -ri.fExpoNum);
            }
            else {
               y = fabs (x);
            }
            // adjust exponent if num < 1
            if ((Truncate (y) == 0) && (y > 0.001)) {
               if ((ri.fStyle == kRSExpo) ||
                  (ri.fStyle == kRSFracExpo)) {
                  ri.fExpoNum--;
               }
               else  {
                  ri.fStyle = kRSFracExpo;
                  ri.fExpoNum = -1;
               }
               continue;
            }
            // adjust exponent if num > 10
            if (Truncate (y) >= 10) {
               if ((ri.fStyle == kRSExpo) ||
                  (ri.fStyle == kRSFracExpo)) {
                  ri.fExpoNum++;
               }
               else {
                  ri.fStyle = kRSFracExpo;
                  ri.fExpoNum = 1;
               }
               continue;
            }
            break;
         }
      }
   
      // convert back to RealInfo_t
      switch (ri.fStyle) {
         // Integer type real
         case kRSInt: 
            {
               ri.fSign = (x < 0) ? -1 : 1;
               ri.fIntNum = Round (fabs (x));
               break;
            }
         // Fraction type real
         case kRSFrac:
            {
               ri.fSign = (x < 0) ? -1 : 1;
               ri.fIntNum = Truncate (fabs (x));
               ri.fFracNum = 
                  Round ((fabs (x) - fabs (double(ri.fIntNum))) * ri.fFracBase);
               break;
            }
         // Exponent only
         case kRSExpo:
            {
               ri.fSign = (x < 0) ? -1 : 1;
               ri.fIntNum = Round (fabs (x) * TMath::Power (10, -ri.fExpoNum));
               if (ri.fIntNum == 0) {
                  ri.fStyle = kRSInt;
               }
               break;
            }
         // Fraction and exponent
         case kRSFracExpo:
            {
               ri.fSign = (x < 0) ? -1 : 1;
               Double_t y = fabs (x) * TMath::Power (10, -ri.fExpoNum);
               ri.fIntNum = Truncate (y);
               ri.fFracNum = 
                  Round ((y - fabs (double(ri.fIntNum))) * ri.fFracBase);
               if ((ri.fIntNum == 0) && (ri.fFracNum == 0)) {
                  ri.fStyle = kRSFrac;
               }
               break;
            }
      }
   
      // check if the back conversion violated limits
      if (limits != kNELNoLimits) {
         x = RealToDouble (ri);
         // check min
         if ((limits == kNELLimitMin) || (limits == kNELLimitMinMax)) {
            if (x < min) {
               char	text[256];
               sprintf (text, "%g", min);
               StrToReal (text, ri);
            }
         }
         // check max
         if ((limits == kNELLimitMax) || (limits == kNELLimitMinMax)) {
            if (x > max) {
               char	text[256];
               sprintf (text, "%g", max);
               StrToReal (text, ri);
            }
         }
      }
   }

//______________________________________________________________________________
   void IncreaseDate (Long_t& l, ENumericEntryStepSize step, Int_t sign)
   {
      Long_t	year;
      Long_t	month;
      Long_t	day;
   
      // get year/month/day format
      year = l / 10000;
      month = (TMath::Abs (l) / 100) % 100;
      if (month > 12) month = 12;
      if (month == 0) month = 1;
      day = TMath::Abs (l) % 100;
      if (day > mDays[month]) day = mDays[month];
      if ((month == 2) && (day > 28) && !IsLeapYear (year)) {
         day = 28;
      }
      if (day == 0) day = 0;
   
      // apply step
      if (step == kNSSHuge) {
         year += sign * 10;
      }
      else if (step == kNSSLarge) {
         year += sign;
      }
      else if (step == kNSSMedium) {
         month += sign;
         if (month > 12) {
            month = 1;
            year++;
         }
         if (month < 1) {
            month = 12;
            year--;
         }
      }
      else if (step == kNSSSmall) {
         day += sign;
         if ((sign > 0) &&
            ((day > mDays [month]) || 
            ((month == 2) && (day > 28) && !IsLeapYear (year)))) {
            day = 1;
            month++;
            if (month > 12) {
               month = 1;
               year++;
            }
         }
         if ((sign < 0) && (day == 0)) {
            month--;
            if (month < 1) {
               month = 12;
               year--;
            }
            day = mDays[month];
         }
      }
      // check again for valid date
      if (year < 0) year = 0;
      if (day > mDays[month]) day = mDays[month];
      if ((month == 2) && (day > 28) && !IsLeapYear (year)) {
         day = 28;
      }
      l = 10000 * year + 100 * month + day;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGNumericEntry                                                      //
//                                                                      //
// Entry field for numeric values			                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGNumericEntry::TLGNumericEntry (const TGWindow *p, Int_t id, 
                     Double_t val, GContext_t norm, FontStruct_t font,
                     UInt_t option, ULong_t back)
   : TLGTextEntry (p, new TGTextBuffer (), id, norm, font, option, back),
   fNumStyle (kNESReal), fNumAttr (kNEAAnyNumber), 
   fNumLimits (kNELNoLimits) 
   {
      fStepLog = kFALSE;
      SetAlignment (kTextRight);
      SetNumber (val, kFALSE);
      UpdateOffset ();
   }

//______________________________________________________________________________
   TLGNumericEntry::TLGNumericEntry (const TGWindow *parent, 
                     Double_t val,  Int_t id, ENumericEntryStyle style,
                     ENumericEntryAttributes attr, 
                     ENumericEntryLimits limits, Double_t min, Double_t max)
   : TLGTextEntry (parent, "", id), fNumStyle (style), 
   fNumAttr (attr), fNumLimits (limits), fNumMin (min), fNumMax (max) 
   {
      fStepLog = kFALSE;
      SetAlignment (kTextRight);
      SetNumber (val, kFALSE);
      UpdateOffset ();
   }

//______________________________________________________________________________
   void TLGNumericEntry::SetNumber (Double_t val)
   {
      SetNumber(val, kTRUE) ;
   }
   void TLGNumericEntry::SetNumber (Double_t val, Bool_t emit)
   {
      switch (fNumStyle) {
         case kNESInteger:
            SetIntNumber (Round (val), emit);
            break;
         case kNESRealOne:
            SetIntNumber (Round (10.0 * val), emit);
            break;
         case kNESRealTwo:
            SetIntNumber (Round (100.0 * val), emit);
            break;
         case kNESRealThree:
            SetIntNumber (Round (1000.0 * val), emit);
            break;
         case kNESRealFour:
            SetIntNumber (Round (10000.0 * val), emit);
            break;
         case kNESReal: 
            {
               char	text[256];
               sprintf (text, "%g", val);
               SetText (text, emit);
               break;
            }
         case kNESDegree:
            SetIntNumber (Round (val), emit);
            break;
         case kNESHourMinSec:
            SetIntNumber (Round (val), emit);
            break;
         case kNESMinSec:
            SetIntNumber (Round (val), emit);
            break;
         case kNESHourMin:
            SetIntNumber (Round (val), emit);
            break;
         case kNESDayMYear:
            SetIntNumber (Round (val), emit);
            break;
         case kNESMDayYear:
            SetIntNumber (Round (val), emit);
            break;
         case kNESHex:
            SetIntNumber ((UInt_t) (fabs (val) + 0.5), emit);
            break;
      }
   }

//______________________________________________________________________________
   void TLGNumericEntry::SetIntNumber (Long_t val)
   {
      SetIntNumber(val, kTRUE) ;
   }
   void TLGNumericEntry::SetIntNumber (Long_t val, Bool_t emit)
   {
      char	text[256];
      RealInfo_t ri;
//      cerr << "TLGNumericEntry::SetIntNumber - val = " << val << endl ; // JCB
      if (fNumStyle == kNESReal) {
         TranslateToStr (text, val, kNESInteger, ri);
      }
      else {
         TranslateToStr (text, val, fNumStyle, ri);
      }
//      cerr << "TLGNumericEntry::SetIntNumber - text = " << text << endl ; // JCB
      SetText (text, emit);
   }

//______________________________________________________________________________
   void TLGNumericEntry::SetTime (Int_t hour, Int_t min, Int_t sec)
   {
      SetTime(hour, min, sec, kTRUE) ;
   }
   void TLGNumericEntry::SetTime (Int_t hour, Int_t min, Int_t sec, Bool_t emit)
   {
      switch (fNumStyle) {
         case kNESHourMinSec:
            SetIntNumber (3600 * TMath::Abs (hour) + 60 * TMath::Abs (min) + TMath::Abs (sec), emit);
            break;
         case kNESMinSec:
            {
               SetIntNumber (60 *min + sec, emit);
               break;
            }
         case kNESHourMin:
            SetIntNumber (60 * TMath::Abs (hour) + TMath::Abs (min), emit);
            break;
         default:
            break;
      }
   }

//______________________________________________________________________________
   void TLGNumericEntry::SetDate (Int_t year, Int_t month, Int_t day)
   {
      SetDate(year, month, day, kTRUE) ;
   }
   void TLGNumericEntry::SetDate (Int_t year, Int_t month, Int_t day, Bool_t emit)
   {
      switch (fNumStyle) {
         case kNESDayMYear:
         case kNESMDayYear:
            {
               SetIntNumber (10000 * TMath::Abs (year) + 100 * TMath::Abs (month) + 
                            TMath::Abs (day), emit);
            }
         default:
            {
               break;
            }
      }
   }

//______________________________________________________________________________
#if ROOT_VERSION_CODE < ROOT_VERSION(5,22,0)
   void TLGNumericEntry::SetText (const char* text)
   {
      char	buf[256];
      strncpy (buf, text, sizeof (buf) - 1);
      buf[sizeof (buf) - 1] = 0;
      EliminateGarbage (buf, fNumStyle, fNumAttr);
      TLGTextEntry::SetText (buf);
   }

  // JCB added SetText(const char *, Bool_t)
  void TLGNumericEntry::SetText (const char* text, bool emit)
   {
      char      buf[256];
      strncpy (buf, text, sizeof (buf) - 1);
      buf[sizeof (buf) - 1] = 0;
      EliminateGarbage (buf, fNumStyle, fNumAttr);
      TLGTextEntry::SetText (buf, emit);
   }
#else
  void TLGNumericEntry::SetText (const char* text, bool emit)
   {
      char	buf[256];
//      cerr << "TLGNumericEntry::SetText - text = " << text << endl ; // JCB
      strncpy (buf, text, sizeof (buf) - 1);
      buf[sizeof (buf) - 1] = 0;
//      cerr << "TLGNumericEntry::SetText - buf = " << buf << endl ; // JCB
      EliminateGarbage (buf, fNumStyle, fNumAttr);
//      cerr << "TLGNumericEntry::SetText - buf = " << buf << endl ; // JCB
      TLGTextEntry::SetText (buf, emit);
   }
#endif

//______________________________________________________________________________
   Double_t TLGNumericEntry::GetNumber () const
   {
      switch (fNumStyle) {
         case kNESInteger:
            return (Double_t) GetIntNumber ();
         case kNESRealOne:
            return (Double_t) GetIntNumber () / 10.0;
         case kNESRealTwo:
            return (Double_t) GetIntNumber () / 100.0;
         case kNESRealThree:
            return (Double_t) GetIntNumber () / 1000.0;
         case kNESRealFour:
            return (Double_t) GetIntNumber () / 10000.0;
         case kNESReal: 
            {
               char	text[256];
               RealInfo_t ri;
               strcpy (text, GetText ());
               return  StrToReal (text, ri);
            }
         case kNESDegree:
            return (Double_t) GetIntNumber ();
         case kNESHourMinSec:
            return (Double_t) GetIntNumber ();
         case kNESMinSec:
            return (Double_t) GetIntNumber ();
         case kNESHourMin:
            return (Double_t) GetIntNumber ();
         case kNESDayMYear:
            return (Double_t) GetIntNumber ();
         case kNESMDayYear:
            return (Double_t) GetIntNumber ();
         case kNESHex:
            return (Double_t) (ULong_t) GetIntNumber ();
      }
      return 0;
   }

//______________________________________________________________________________
   Long_t TLGNumericEntry::GetIntNumber () const
   {
      RealInfo_t ri;
      return TranslateToNum (GetText(), fNumStyle, ri);
   }

//______________________________________________________________________________
   void TLGNumericEntry::GetTime (Int_t& hour, Int_t& min, 
                     Int_t& sec) const
   {
      switch (fNumStyle) {
         case kNESHourMinSec:
            {
               Long_t l = GetIntNumber();
               hour = TMath::Abs (l) / 3600;
               min = (TMath::Abs (l) % 3600) / 60;
               sec = TMath::Abs (l) % 60;
               break;
            }
         case kNESMinSec:
            {
               Long_t l = GetIntNumber();
               hour = 0;
               min = TMath::Abs (l) / 60;
               sec = TMath::Abs (l) % 60;
               if (l < 0) {
                  min *= -1;
                  sec *= -1;
               }
               break;
            }
         case kNESHourMin:
            {
               Long_t l = GetIntNumber();
               hour = TMath::Abs (l) / 60;
               min = TMath::Abs (l) % 60;
               sec = 0;
               break;
            }
         default:
            {
               hour = 0;
               min = 0;
               sec = 0;
               break;
            }
      }
   }

//______________________________________________________________________________
   void TLGNumericEntry::GetDate (Int_t& year, Int_t& month, 
                     Int_t& day) const
   {
      switch (fNumStyle) {
         case kNESDayMYear:
         case kNESMDayYear:
            {
               Long_t l = GetIntNumber();
               year = l / 10000;
               month = (l % 10000) / 100;
               day = l % 100;
               break;
            }
         default:
            {
               year = 0;
               month =0;
               day = 0;
               break;
            }
      }
   }

//______________________________________________________________________________
   Int_t TLGNumericEntry::GetCharWidth (const char* text) const
   {
      return gVirtualX->TextWidth (fFontStruct, text, strlen (text));
   }

//______________________________________________________________________________
   void TLGNumericEntry::IncreaseNumber (ENumericEntryStepSize step,
                     Int_t stepsign, Bool_t logstep)
   {
      Long_t		l;
      RealInfo_t	ri;
      Long_t		mag = 0;
      Double_t		rmag = 0.0;
      Int_t		sign = stepsign;
   
      // svae old text field
      TString oldtext = GetText();
//      cerr << "TLGNumericEntry::IncreaseNumber line " << __LINE__ << " - text field is " << oldtext << endl ; // JCB
      // Get number
      if (fNumStyle != kNESReal) {
         l = GetIntNumber();
//	 cerr << "TLGNumericEntry::IncreaseNumber line " << __LINE__ << " - GetIntNumber() returned " << l << endl ; // JCB
      }
      else {
         StrToReal (oldtext, ri);
      }
   
      // magnitude of step
      if ((fNumStyle == kNESDegree) || (fNumStyle == kNESHourMinSec) ||
         (fNumStyle == kNESMinSec) || (fNumStyle == kNESHourMin) ||
         (fNumStyle == kNESDayMYear) || (fNumStyle == kNESMDayYear) ||
         (fNumStyle == kNESHex)) {
         logstep = kFALSE;
         switch (step) {
            case kNSSSmall:
               mag = 1;
               break;
            case kNSSMedium:
               mag = 10;
               break;
            case kNSSLarge:
               mag = 100;
               break;
            case kNSSHuge:
               mag = 1000;
               break;
         }
      }
      else {
         Int_t msd = TMath::Abs ((fNumStyle == kNESReal) ? ri.fIntNum : l);
         while (msd >= 10) msd /= 10;
         Bool_t odd = (msd < 3);
         if (sign < 0) odd = !odd;
         switch (step) {
            case kNSSSmall:
               rmag = (!logstep) ? 1. : (odd ? 3. : 10./3.);
               break;
            case kNSSMedium:
               rmag = (!logstep) ? 10. : 10.;
               break;
            case kNSSLarge:
               rmag = (!logstep) ? 100. : (odd ? 30. : 100./3.);
               break;
            case kNSSHuge:
               rmag = (!logstep) ? 1000. : 100.;
               break;
         }
         if (sign < 0) rmag = logstep ? 1. / rmag : -rmag;
      }
//      { // JCB
//	 char str[128] ;
//	 sprintf(str, "TLGNumericEntry::IncreaseNumber - mag = %d, rmag = %f\n", mag, rmag) ;
//	 cerr << str ;
//      }
   
      // sign of step
      if (sign == 0) {
         logstep = kFALSE;
         rmag = 0;
         mag = 0;
      }
      else {
         sign = (sign > 0) ? 1 : -1;
      }
      // add/multiply step
      switch (fNumStyle) {
         case kNESInteger:
         case kNESRealOne:
         case kNESRealTwo:
         case kNESRealThree:
         case kNESRealFour:
            {
               l = logstep ? Round (l * rmag) : Round (l + rmag);
               CheckMinMax (l, fNumStyle, fNumLimits, fNumMin, fNumMax);
               if ((l < 0) && (fNumAttr == kNEANonNegative)) l = 0;
               if ((l <= 0) && (fNumAttr == kNEAPositive)) l = 1;
               break;
            }
         case kNESReal:
            {
               IncreaseReal (ri, rmag, logstep, 
                            fNumLimits, fNumMin, fNumMax);
               if (((fNumAttr == kNEANonNegative) || 
                   (fNumAttr == kNEAPositive)) && (ri.fSign < 0)) {
                  ri.fIntNum = 0;
                  ri.fFracNum = 0;
                  ri.fExpoNum = 0;
                  ri.fSign = 1;
               }
               break;
            }
         case kNESDegree:
            {
               if (mag > 60) l += sign * 36 * mag;
               else if (mag > 6) l += sign * 6 * mag;
               else l += sign * mag;
               CheckMinMax (l, fNumStyle, fNumLimits, fNumMin, fNumMax);
               if ((l < 0) && (fNumAttr == kNEANonNegative)) l = 0;
               if ((l <= 0) && (fNumAttr == kNEAPositive)) l = 1;
               break;
            }
         case kNESHourMinSec:
            {
               if (mag > 60) l += sign * 36 * mag;
               else if (mag > 6) l += sign * 6 * mag;
               else l += sign * mag;
               CheckMinMax (l, fNumStyle, fNumLimits, fNumMin, fNumMax);
               if (l < 0) l = (24 * 3600) - ((-l) % (24 * 3600));
               if (l > 0) l = l % (24 * 3600);
               break;
            }
         case kNESMinSec:
            {
               if (mag > 6) l += sign * 6 * mag;
               else l += sign * mag;
               CheckMinMax (l, fNumStyle, fNumLimits, fNumMin, fNumMax);
               if ((l < 0) && (fNumAttr == kNEANonNegative)) l = 0;
               if ((l <= 0) && (fNumAttr == kNEAPositive)) l = 1;
               break;
            }
         case kNESHourMin:
            {
               if (mag > 6) l += sign * 6 * mag;
               else l += sign * mag;
               CheckMinMax (l, fNumStyle, fNumLimits, fNumMin, fNumMax);
               if (l < 0) l = (24 * 60) - ((-l) % (24 * 60));
               if (l > 0) l = l % (24 * 60);
               break;
            }
         case kNESDayMYear:
         case kNESMDayYear:
            {
               IncreaseDate (l, step, sign);
               CheckMinMax (l, fNumStyle, fNumLimits, fNumMin, fNumMax);
               break;
            }
         case kNESHex:
            {
               ULong_t ll = (ULong_t) l;
               if (mag > 500) ll += sign * 4096 * mag / 1000;
               else if (mag > 50) ll += sign * 256 * mag / 100;
               else if (mag > 5) ll += sign * 16 * mag / 10;
               else ll += sign * mag;
               l = (Long_t) ll;
               CheckMinMax (l, fNumStyle, fNumLimits, fNumMin, fNumMax);
               break;
            }
      }
      if (fNumStyle != kNESReal) {
//	 cerr << "TLGNumericEntry::IncreaseNumber - SetIntNumber( " << l << " )" << endl ; // JCB
         SetIntNumber (l, kTRUE);
      }
      else {
         char	buf[256];
         RealToStr (buf, ri);
//	 cerr << "TLGNumericEntry::IncreaseNumber - SetText( " << buf << " )" << endl ; // JCB
         SetText (buf, kTRUE);
      }
      // send update message
      if ((stepsign != 0) && (oldtext != GetText())) {
         TLGTextEntry::TextUpdated();
      } 
   }

//______________________________________________________________________________
   void TLGNumericEntry::SetFormat (ENumericEntryStyle style,
                     ENumericEntryAttributes attr)
   {
      Double_t val = GetNumber();
      fNumStyle = style;
      fNumAttr = attr;
      if ((fNumAttr != kNEAAnyNumber) && (val < 0)) val = 0; 
      SetNumber (val);
      // make sure we have a valid number by increasaing it by 0
      IncreaseNumber (kNSSSmall, 0, kFALSE);
   }

//______________________________________________________________________________
   void TLGNumericEntry::SetLimits (
                     ENumericEntryLimits limits,
                     Double_t min, Double_t max)
   {
      Double_t val = GetNumber();
      fNumLimits = limits;
      fNumMin = min;
      fNumMax = max;
      SetNumber (val);
      // make sure we have a valid number by increasaing it by 0
      IncreaseNumber (kNSSSmall, 0, kFALSE);
   }

//______________________________________________________________________________
   void TLGNumericEntry::TextUpdated () 
   {
      // make sure we have a valid number by increasaing it by 0
      IncreaseNumber (kNSSSmall, 0, kFALSE);
      TLGTextEntry::TextUpdated ();
   }

//______________________________________________________________________________
   Bool_t TLGNumericEntry::HandleKey (Event_t* event)
   {
      if (!IsEnabled()) {
         return TLGTextEntry::HandleKey (event);
      }
   
      Int_t	n;
      char	tmp[10];
      UInt_t	keysym;
      gVirtualX->LookupString (event, tmp, sizeof(tmp), keysym);
      n = strlen (tmp);
//      { // JCB
//	 char str[256] ;
//	 sprintf(str, "TLGNumericEntry::HandleKey() line %d: tmp = %s\n", __LINE__, tmp) ;
//	 cerr << str;
//      }
   
      // intercept up key
      if ((EKeySym) keysym == kKey_Up) {
         // Get log step / alt key
         Bool_t logstep = fStepLog;
         if (event->fState & kKeyMod1Mask) logstep = !logstep;
         // shift-cntrl-up
         if ((event->fState & kKeyShiftMask) && 
            (event->fState & kKeyControlMask)) {
            IncreaseNumber (kNSSHuge, 1, logstep);
         }
         // cntrl-up
         else if (event->fState & kKeyControlMask) {
            IncreaseNumber (kNSSLarge, 1, logstep);
         }
         // shift-up
         else if (event->fState & kKeyShiftMask) {
            IncreaseNumber (kNSSMedium, 1, logstep);
         }
         // up
         else {
            IncreaseNumber (kNSSSmall, 1, logstep);
         }
         return kTRUE;
      }
      // intercept down key
      else if ((EKeySym) keysym == kKey_Down) {
         // Get log step / alt key
         Bool_t logstep = fStepLog;
         if (event->fState & kKeyMod1Mask) logstep = !logstep;
         // shift-cntrl-down
         if ((event->fState & kKeyShiftMask) && 
            (event->fState & kKeyControlMask)) {
            IncreaseNumber (kNSSHuge, -1, logstep);
         }
         // cntrl-down
         else if (event->fState & kKeyControlMask) {
            IncreaseNumber (kNSSLarge, -1, logstep);
         }
         // shift-down
         else if (event->fState & kKeyShiftMask) {
            IncreaseNumber (kNSSMedium, -1, logstep);
         }
         // down
         else {
            IncreaseNumber (kNSSSmall, -1, logstep);
         }
         return kTRUE;
      }
      // intercept printable characters
      else if (n && (keysym < 127) && (keysym >= 32) &&
              ((EKeySym) keysym != kKey_Delete) && 
              ((EKeySym) keysym != kKey_Backspace) &&
              ((event->fState & kKeyControlMask) == 0)) {
         if (IsGoodChar (tmp[0], fNumStyle, fNumAttr)) {
            return TLGTextEntry::HandleKey (event);
         }
         else {
            // cout << "bad character" << endl;
            return kTRUE;
         }
      }
      // otherwise use default behaviour
      else {
         return TLGTextEntry::HandleKey (event);
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGNumericControlBoxLayout                                           //
//                                                                      //
// Layout manager for numeric control  			                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   class TLGNumericControlBoxLayout : public TGLayoutManager {
   protected:
      // pointer to numeric control box
      TLGNumericControlBox*	fBox;
   
   public:
      TLGNumericControlBoxLayout (TLGNumericControlBox* box) {
         fBox = box; }
      virtual void Layout ();
      virtual TGDimension GetDefaultSize () const;
   };

//______________________________________________________________________________
   void TLGNumericControlBoxLayout::Layout ()
   {
      if (fBox == 0) {
         return;
      }
      UInt_t w = fBox->GetWidth();
      UInt_t h = fBox->GetHeight();
      UInt_t upw = 2 * h / 3;
      UInt_t uph = h / 2;
      Int_t upx = (w > h) ? (Int_t)w - (Int_t)upw : -1000;
      Int_t upy = 0;
      Int_t downx = (w > h) ? (Int_t)w - (Int_t)upw : -1000;
      Int_t downy = h / 2;
      UInt_t downw = upw;
      UInt_t downh = h - downy;
      UInt_t numw = (w > h) ? w - upw : w;
      UInt_t numh = h;
      fBox->fNumericEntry->MoveResize (0, 0, numw, numh);
      fBox->fButtonUp->MoveResize (upx, upy, upw, uph);
      fBox->fButtonDown->MoveResize (downx, downy, downw, downh);
   }

//______________________________________________________________________________
   TGDimension TLGNumericControlBoxLayout::GetDefaultSize () const
   {
      return fBox->GetSize();
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TRepeatTimer                                                         //
//                                                                      //
// timer for numeric control box buttons		                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   class TLGRepeatFireButton;

   class TRepeatTimer : public TTimer {
   private:
      TLGRepeatFireButton* fButton;
   public:
      TRepeatTimer (TLGRepeatFireButton* button, Long_t ms) 
      : TTimer (ms, kTRUE), fButton (button) {
      }
      virtual Bool_t Notify();
   };



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TRepeatFireButton                                                    //
//                                                                      //
// Picture button which fires repeatly as long as the button is pressed //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   class TLGRepeatFireButton : public TGPictureButton {
   protected:
      TRepeatTimer*	fTimer;
      Int_t		fIgnoreNextFire;
      ENumericEntryStepSize fStep;
      Bool_t		fStepLog;
      Bool_t 		dologstep;   
   
   public:
      TLGRepeatFireButton (const TGWindow* p, const TGPicture* pic, 
                        Int_t id, Bool_t logstep) 
      : TGPictureButton (p, pic, id), fTimer (0), fIgnoreNextFire (0), 
      fStep (kNSSSmall), fStepLog (logstep) {
      }
      virtual ~TLGRepeatFireButton () {
         delete fTimer; }
   
      virtual Bool_t HandleButton (Event_t* event);
      void FireButton ();
      virtual void SetLogStep (Bool_t on = kTRUE) {
         fStepLog = on; }
   };

//______________________________________________________________________________
   Bool_t TLGRepeatFireButton::HandleButton (Event_t* event)
   {
      if (fTip) fTip->Hide();
   
      if (fState == kButtonDisabled) 
         return kTRUE;
   
      if (event->fType == kButtonPress) {
         // Get log step / alt key
         dologstep = fStepLog;
         if (event->fState & kKeyMod1Mask) dologstep = !dologstep;
         if ((event->fState & kKeyShiftMask) &&
            (event->fState & kKeyControlMask)) {
            fStep = kNSSHuge;
         }
         else if (event->fState & kKeyControlMask) {
            fStep = kNSSLarge;
         }
         else if (event->fState & kKeyShiftMask) {
            fStep = kNSSMedium;
         }
         else {
            fStep = kNSSSmall;
         }
         SetState (kButtonDown);
         fIgnoreNextFire = 0;
         FireButton();
         fIgnoreNextFire = 2;
         if (fTimer == 0) {
            fTimer = new TRepeatTimer (this, 330);
         }
         fTimer->Reset();
         gSystem->AddTimer (fTimer);
      }
      else {
         SetState (kButtonUp);
         if (fTimer != 0) {
            fTimer->Remove();
         }
      }
   
      return kTRUE;
   }

//______________________________________________________________________________
   void TLGRepeatFireButton::FireButton () {
      if (fIgnoreNextFire <= 0) {
         SendMessage (fMsgWindow, MK_MSG (kC_COMMAND, kCM_BUTTON), 
                     fWidgetId, (Long_t) fStep + (dologstep ? 100 : 0)); 
      }
      else {
         fIgnoreNextFire--;
      }
   }

//______________________________________________________________________________
   Bool_t TRepeatTimer::Notify() 
   {
      fButton->FireButton(); 
      Reset();
      return kFALSE;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGNumericControlBox                                                 //
//                                                                      //
// Control box for numeric entry field 			                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGNumericControlBox::TLGNumericControlBox (const TGWindow *parent,
                     Double_t val, Int_t wdigits, Int_t id, 
                     ENumericEntryStyle style,
                     ENumericEntryAttributes attr, 
                     ENumericEntryLimits limits, 
                     Double_t min, Double_t max)
   : TGCompositeFrame (parent, 10 * wdigits, 25), TGWidget (id), 
   fButtonToNum (kTRUE) 
   {
      // get button pictures
      fPicUp = fClient->GetPicture ("arrow_up.xpm");
      if (!fPicUp) 
         Error ("TLGNumericControlBox", "arrow_up.xpm not found");
      fPicDown = fClient->GetPicture ("arrow_down.xpm");
      if (!fPicDown) 
         Error ("TLGNumericControlBox", "arrow_down.xpm not found");
   
      // create gui elements
      fNumericEntry = new TLGNumericEntry (this, val, 3, style, attr, 
                           limits, min, max);
      fNumericEntry->Associate (this);
      AddFrame (fNumericEntry, 0);
      fButtonUp = new TLGRepeatFireButton (this, fPicUp, 1, 
                           fNumericEntry->IsLogStep());
      fButtonUp->Associate (this);
      AddFrame (fButtonUp, 0);
      fButtonDown = new TLGRepeatFireButton (this, fPicDown, 2,
                           fNumericEntry->IsLogStep());
      fButtonDown->Associate (this);
      AddFrame (fButtonDown, 0);
   
      // resize
      Int_t h = fNumericEntry->GetDefaultHeight();
      Int_t charw = fNumericEntry->GetCharWidth ("0123456789");
      Int_t w = charw * TMath::Abs (wdigits) / 10 + 8 + 2 * h / 3;
      SetLayoutManager (new TLGNumericControlBoxLayout (this));
      Resize (w, h);
   }

//______________________________________________________________________________
   TLGNumericControlBox::~TLGNumericControlBox ()
   {
      delete fButtonUp;
      delete fButtonDown;
      delete fNumericEntry;
   }

//______________________________________________________________________________
   void TLGNumericControlBox::SetLogStep (Bool_t on) 
   {
      fNumericEntry->SetLogStep (on);
      ((TLGRepeatFireButton*)fButtonUp)->SetLogStep (
                           fNumericEntry->IsLogStep());
      ((TLGRepeatFireButton*)fButtonDown)->SetLogStep (
                           fNumericEntry->IsLogStep());
   }

//______________________________________________________________________________
   void TLGNumericControlBox::SetState (Bool_t enable)
   {
      if (enable) {
         fButtonUp->SetState (kButtonUp);
         fButtonDown->SetState (kButtonUp);
         fNumericEntry->SetState (kTRUE);
      }
      else {
         fButtonUp->SetState (kButtonDisabled);
         fButtonDown->SetState (kButtonDisabled);
         fNumericEntry->SetState (kFALSE);
      }
   }

//______________________________________________________________________________
   Bool_t TLGNumericControlBox::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      switch (GET_MSG (msg)) {
         case kC_COMMAND:
            {
               if ((GET_SUBMSG (msg) == kCM_BUTTON) &&
                  (parm1 >= 1) && (parm1 <= 2)) {
                  if (fButtonToNum) {
                     Int_t sign = (parm1 == 1) ? 1 : -1;
                     ENumericEntryStepSize step = 
                        (ENumericEntryStepSize) (parm2 % 100);
                     Bool_t logstep = (parm2 >= 100);
                     fNumericEntry->IncreaseNumber (step, sign, logstep);
                  }
                  else {
                     SendMessage (fMsgWindow, msg, fWidgetId, 
                                 10000 * (parm1-1) + parm2);
                  }
               }
               break;
            }
         
         case kC_TEXTENTRY:
            {
               SendMessage (fMsgWindow, msg, fWidgetId, 0);
               break;
            }
      }
      return kTRUE;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGLineStyleComboBox                                                 //
//                                                                      //
// Combobox for line style selection          		                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGLineStyleComboBox::TLGLineStyleComboBox (TGWindow* p, Int_t id)
   : TGComboBox (p, id)
   {
      Resize (110, 22);
      AddEntry ("solid", 1);
      AddEntry ("dash", 2);
      AddEntry ("dot-dot", 3);
      AddEntry ("dash-dot", 4);
      Select (1);
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGMarkerStyleComboBox                                               //
//                                                                      //
// Combobox for marker style selection          		        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGMarkerStyleComboBox::TLGMarkerStyleComboBox (TGWindow* p, Int_t id)
   : TGComboBox (p, id)
   {
      Resize (110, 22);
      AddEntry ("circle", 20);
      AddEntry ("triangle up", 22);
      AddEntry ("square", 21);
      AddEntry ("triangle down", 23);
      AddEntry ("star", 29);
      AddEntry ("open diamond", 27);
      AddEntry ("cross", 2);
      AddEntry ("angle cross", 5);
      AddEntry ("open circle", 24);
      AddEntry ("open triangle", 26);
      AddEntry ("open square", 25);
      AddEntry ("open cross", 28);
      AddEntry ("open star", 30);
      AddEntry ("dot", 1);
      Select (20);
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGFillStyleComboBox                                                 //
//                                                                      //
// Combobox for fill style selection            		        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGFillStyleComboBox::TLGFillStyleComboBox (TGWindow* p, Int_t id)
   : TGComboBox (p, id)
   {
      Resize (110, 22);
      AddEntry ("solid", 1001);
      AddEntry ("hollow", 0);
      AddEntry ("diagonal up", 3004);
      AddEntry ("diagonal down", 3005);
      AddEntry ("vertical", 3006);
      AddEntry ("horizontal", 3007);
      AddEntry ("cross hatch", 3013);
      AddEntry ("brick", 3010);
      AddEntry ("funny", 3014);
      AddEntry ("circle", 3012);
      Select (1001);
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGFontSelection                                                     //
//                                                                      //
// Multiple (font name, style, size) comboboxes for font selection      //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGFontSelection::TLGFontSelection (TGWindow* p, Int_t id, 
                     Bool_t size)
   : TGHorizontalFrame (p, 200, 22), TGWidget (id), fUseSize (size)
   {
      fL1 = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 0, 2, 2);
      fL2 = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 3, 0, 2, 2);
      // font name
      fFontName = new TGComboBox (this, 1);
      fFontName->Associate (this);
      fFontName->Resize (100, 22);
      fFontName->AddEntry ("Times", 0);
      fFontName->AddEntry ("Helvetica", 1);
      fFontName->AddEntry ("Courier", 2);
      fFontName->AddEntry ("Symbol", 3);
      fFontName->Select (0);
      AddFrame (fFontName, fL1);
      // font weight
      fFontWeight = new TGComboBox (this, 2);
      fFontWeight->Associate (this);
      fFontWeight->Resize (85, 22);
      fFontWeight->AddEntry ("normal", 0);
      fFontWeight->AddEntry ("bold", 2);
      fFontWeight->AddEntry ("italic", 1);
      fFontWeight->AddEntry ("bold-italic", 3);
      fFontWeight->Select (0);
      AddFrame (fFontWeight, fL2);
      // font size
      if (fUseSize) {
         fFontSize = new TLGNumericControlBox (this, 0.04, 5, 3, 
                              kNESRealThree, kNEANonNegative);
         fFontSize->Associate (this);
         AddFrame (fFontSize, fL2);
      }
      else {
         fFontSize = 0;
      }
   }

//______________________________________________________________________________
   TLGFontSelection::~TLGFontSelection ()
   {
      delete fFontSize;
      delete fFontWeight;
      delete fFontName;
      delete fL1;
      delete fL2;
   }

//______________________________________________________________________________
   void TLGFontSelection::SetFont (Font_t font)
   {
      switch (font / 10) {
         case 13:
         case 1:
         case 2:
         case 3:
         default:
            {
               fFontName->Select (0);
               break;
            }
         case 4:
         case 5:
         case 6:
         case 7:
            {
               fFontName->Select (1);
               break;
            }
         case 8:
         case 9:
         case 10:
         case 11:
            {
               fFontName->Select (2);
               break;
            }
         case 12:
            {
               fFontName->Select (3);
               break;
            }
      }
      switch (font / 10) {
         case 13:
         case 4:
         case 8:
         case 12:
         default:
            {
               fFontWeight->Select (0);
               break;
            }
         case 1:
         case 5:
         case 9:
            {
               fFontWeight->Select (1);
               break;
            }
         case 2:
         case 6:
         case 10:
            {
               fFontWeight->Select (2);
               break;
            }
         case 3:
         case 7:
         case 11:
            {
               fFontWeight->Select (3);
               break;
            }
      }
   }

//______________________________________________________________________________
   Font_t TLGFontSelection::GetFont () const
   {
      Font_t font = 0;
      Int_t w = fFontWeight->GetSelected();
      switch (fFontName->GetSelected()) {
         case 0:
            {
               font = (w == 0) ? 132 : 10 * w + 2;
               break;
            }
         case 1:
            {   
               font = 10 * (4 + w) + 2;
               break;
            }
         case 2:
            {   
               font = 10 * (8 + w) + 2;
               break;
            }
         case 3:
            {   
               font = 122;
               break;
            }
      }
      return font;
   }

//______________________________________________________________________________
   Bool_t TLGFontSelection::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      Bool_t nsel = (GET_MSG (msg) == kC_COMMAND) && 
         (GET_SUBMSG (msg) == kCM_COMBOBOX) && (parm1 == 1);
      Bool_t wsel = (GET_MSG (msg) == kC_COMMAND) && 
         (GET_SUBMSG (msg) == kCM_COMBOBOX) && (parm1 == 2);
      Bool_t ssel = (GET_MSG (msg) == kC_TEXTENTRY) && 
         (GET_SUBMSG (msg) == kTE_TEXTUPDATED) && (parm1 == 3);
      if (nsel || wsel) {
         SendMessage (fMsgWindow, msg, fWidgetId, GetFont());
      }
      else if ((ssel) && (fUseSize)) {
         // cout << "send new font size message" << endl;
         SendMessage (fMsgWindow, msg, fWidgetId, fFontSize->GetIntNumber());
      }
      return kTRUE;
   }


#if 1
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGFileDialog class                                                  //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   // Events emitted by widgets
   enum EFileDialog {
      kIDF_CDUP,
      kIDF_NEW_FOLDER,
      kIDF_LIST,
      kIDF_DETAILS,
      kIDF_CHECKB,
      kIDF_FSLB,
      kIDF_FTYPESLB,
      kIDF_OK,
      kIDF_CANCEL
   };

   static const char *gDefTypes[] = { "All files",    "*",
				      "ROOT files",   "*.root",
				      "ROOT macros",  "*.C",
				      0, 0} ;
   static TGFileInfo gInfo ;


   TLGFileDialog::TLGFileDialog(const TGWindow *main, TGFileInfo *file_info,
		  EFileDialogMode dlg_type)
	       : TGTransientFrame(gClient->GetRoot(), main, 10, 10, kVerticalFrame)
   {
	 // Create a file selection dialog. Depending on the dlg_type it can be
	 // used for opening or saving a file.

	 SetCleanup(kDeepCleanup);
	 Connect("CloseWindow()", "TGFileDialog", this, "CloseWindow()");
	 DontCallClose();

	 int i;

	 if (!main) {
	    MakeZombie();
	    return;
	 }
	 if (!file_info) {
	    Error("TGFileDialog", "file_info argument not set");
	    fFileInfo = &gInfo;
	    if (fFileInfo->fIniDir) {
	       delete [] fFileInfo->fIniDir;
	       fFileInfo->fIniDir = 0;
	    }
	    if (fFileInfo->fFilename) {
	       delete [] fFileInfo->fFilename;
	       fFileInfo->fFilename = 0;
	    }
	    fFileInfo->fFileTypeIdx = 0;
	 } else
	    fFileInfo = file_info;

	 if (!fFileInfo->fFileTypes)
	    fFileInfo->fFileTypes = gDefTypes;

	 if (!fFileInfo->fIniDir)
	    fFileInfo->fIniDir = StrDup(".");

	 TGHorizontalFrame *fHtop = new TGHorizontalFrame(this, 10, 10);
	 
	 //--- top toolbar elements
	 TGLabel *fLookin = new TGLabel(fHtop, new TGHotString((dlg_type == kFDSave)
							? "S&ave in:" : "&Look in:"));
	 fTreeLB = new TGFSComboBox(fHtop, kIDF_FSLB);
	 fTreeLB->Associate(this);

	 fPcdup = fClient->GetPicture("tb_uplevel.xpm");
	 fPnewf = fClient->GetPicture("tb_newfolder.xpm");
	 fPlist = fClient->GetPicture("tb_list.xpm");
	 fPdetails = fClient->GetPicture("tb_details.xpm");

	 if (!(fPcdup && fPnewf && fPlist && fPdetails))
	    Error("TGFileDialog", "missing toolbar pixmap(s).\n");

	 fCdup    = new TGPictureButton(fHtop, fPcdup, kIDF_CDUP);
	 fNewf    = new TGPictureButton(fHtop, fPnewf, kIDF_NEW_FOLDER);
	 fList    = new TGPictureButton(fHtop, fPlist, kIDF_LIST);
	 fDetails = new TGPictureButton(fHtop, fPdetails, kIDF_DETAILS);

	 fCdup->SetToolTipText("Up One Level");
	 fNewf->SetToolTipText("Create New Folder");
	 fList->SetToolTipText("List");
	 fDetails->SetToolTipText("Details");

	 fCdup->Associate(this);
	 fNewf->Associate(this);
	 fList->Associate(this);
	 fDetails->Associate(this);

	 fList->AllowStayDown(kTRUE);
	 fDetails->AllowStayDown(kTRUE);

	 fTreeLB->Resize(200, fTreeLB->GetDefaultHeight());

	 fHtop->AddFrame(fLookin, new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 2, 5, 2, 2));
	 fHtop->AddFrame(fTreeLB, new TGLayoutHints(kLHintsLeft | kLHintsExpandY, 3, 0, 2, 2));
	 fHtop->AddFrame(fCdup, new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 3, 0, 2, 2));
	 fHtop->AddFrame(fNewf, new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 3, 0, 2, 2));
	 fHtop->AddFrame(fList, new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 3, 0, 2, 2));
	 fHtop->AddFrame(fDetails, new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 0, 8, 2, 2));

	 if (dlg_type == kFDSave) {
	    fCheckB = new TGCheckButton(fHtop, "&Overwrite", kIDF_CHECKB);
	    fCheckB->SetToolTipText("Overwrite a file without displaying a message if selected");
	 } else {
	    fCheckB = new TGCheckButton(fHtop, "&Multiple files", kIDF_CHECKB);
	    fCheckB->SetToolTipText("Allows multiple file selection when SHIFT is pressed");
	    fCheckB->Connect("Toggled(Bool_t)","TGFileInfo",fFileInfo,"SetMultipleSelection(Bool_t)");
	 }
	 fHtop->AddFrame(fCheckB, new TGLayoutHints(kLHintsLeft | kLHintsCenterY));
	 fCheckB->SetOn(fFileInfo->fMultipleSelection);
	 AddFrame(fHtop, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 4, 4, 3, 1));

	 //--- file view

	 fFv = new TGListView(this, 400, 161);

	 fFc = new TGFileContainer(fFv->GetViewPort(),
				   10, 10, kHorizontalFrame, fgWhitePixel);
	 fFc->Associate(this);

	 fFv->GetViewPort()->SetBackgroundColor(fgWhitePixel);
	 fFv->SetContainer(fFc);
	 fFv->SetViewMode(kLVList);
	 fFv->SetIncrements(1, 19); // set vertical scroll one line height at a time

	 TGTextButton** buttons = fFv->GetHeaderButtons();
	 if (buttons) {
	    buttons[0]->Connect("Clicked()", "TGFileContainer", fFc, "Sort(=kSortByName)");
	    buttons[1]->Connect("Clicked()", "TGFileContainer", fFc, "Sort(=kSortByType)");
	    buttons[2]->Connect("Clicked()", "TGFileContainer", fFc, "Sort(=kSortBySize)");
	    buttons[3]->Connect("Clicked()", "TGFileContainer", fFc, "Sort(=kSortByOwner)");
	    buttons[4]->Connect("Clicked()", "TGFileContainer", fFc, "Sort(=kSortByGroup)");
	    buttons[5]->Connect("Clicked()", "TGFileContainer", fFc, "Sort(=kSortByDate)");
	 }

	  fFc->SetFilter(fFileInfo->fFileTypes[fFileInfo->fFileTypeIdx+1]);
	 fFc->Sort(kSortByName);
	 fFc->ChangeDirectory(fFileInfo->fIniDir);
	 fFc->SetMultipleSelection(fFileInfo->fMultipleSelection);
	 fTreeLB->Update(fFc->GetDirectory());

	 fList->SetState(kButtonEngaged);

	 AddFrame(fFv, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 4, 4, 3, 1));

	 if (dlg_type == kFDOpen) {
	    fCheckB->Connect("Toggled(Bool_t)","TGFileContainer",fFc,"SetMultipleSelection(Bool_t)");
	    fCheckB->Connect("Toggled(Bool_t)","TGFileContainer",fFc,"UnSelectAll()");
	 }
	 
	 //--- file name and types

	 TGHorizontalFrame *fHf = new TGHorizontalFrame(this, 10, 10);

	 TGVerticalFrame *fVf = new TGVerticalFrame(fHf, 10, 10);

	 TGHorizontalFrame *fHfname = new TGHorizontalFrame(fVf, 10, 10);

	 TGLabel *fLfname = new TGLabel(fHfname, new TGHotString("File &name:"));
	 fTbfname = new TGTextBuffer(1034);
	 fName = new TGTextEntry(fHfname, fTbfname);
	 fName->Resize(230, fName->GetDefaultHeight());
	 fName->Associate(this);

	 fHfname->AddFrame(fLfname, new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 2, 5, 2, 2));
	 fHfname->AddFrame(fName, new TGLayoutHints(kLHintsRight | kLHintsCenterY, 0, 20, 2, 2));

	 fVf->AddFrame(fHfname, new TGLayoutHints(kLHintsLeft | kLHintsCenterY | kLHintsExpandX));

	 TGHorizontalFrame *fHftype = new TGHorizontalFrame(fVf, 10, 10);

	 TGLabel *fLftypes = new TGLabel(fHftype, new TGHotString("Files of &type:"));
	 fTypes = new TGComboBox(fHftype, kIDF_FTYPESLB);
	 fTypes->Associate(this);
	 fTypes->Resize(230, fName->GetDefaultHeight());

	 TString s;
	 for (i = 0; fFileInfo->fFileTypes[i] != 0; i += 2) {
	    s.Form("%s (%s)", fFileInfo->fFileTypes[i], fFileInfo->fFileTypes[i+1]);
	    fTypes->AddEntry(s.Data(), i);
	 }
	 fTypes->Select(fFileInfo->fFileTypeIdx);

	 // Show all items in combobox without scrollbar
	 //TGDimension fw = fTypes->GetListBox()->GetContainer()->GetDefaultSize();
	 //fTypes->GetListBox()->Resize(fw.fWidth, fw.fHeight);

	 if (fFileInfo->fFilename && fFileInfo->fFilename[0])
	    fTbfname->AddText(0, fFileInfo->fFilename);
	 else
	    fTbfname->Clear();

	 fHftype->AddFrame(fLftypes, new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 2, 5, 2, 2));
	 fHftype->AddFrame(fTypes, new TGLayoutHints(kLHintsRight | kLHintsCenterY, 0, 20, 2, 2));

	 fVf->AddFrame(fHftype, new TGLayoutHints(kLHintsLeft | kLHintsCenterY | kLHintsExpandX));

	 fHf->AddFrame(fVf, new TGLayoutHints(kLHintsLeft | kLHintsCenterY | kLHintsExpandX));

	 //--- Open/Save and Cancel buttons

	 TGVerticalFrame *fVbf = new TGVerticalFrame(fHf, 10, 10, kFixedWidth);

	 fOk = new TGTextButton(fVbf, new TGHotString((dlg_type == kFDSave)
						       ? "&Save" : "&Open"), kIDF_OK);
	 fCancel = new TGTextButton(fVbf, new TGHotString("Cancel"), kIDF_CANCEL);

	 fOk->Associate(this);
	 fCancel->Associate(this);

	 fVbf->AddFrame(fOk, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 0, 0, 2, 2));
	 fVbf->AddFrame(fCancel, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 0, 0, 2, 2));

	 UInt_t width = TMath::Max(fOk->GetDefaultWidth(), fCancel->GetDefaultWidth()) + 20;
	 fVbf->Resize(width + 20, fVbf->GetDefaultHeight());

	 fHf->AddFrame(fVbf, new TGLayoutHints(kLHintsLeft | kLHintsCenterY));

	 AddFrame(fHf, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 4, 4, 3, 1));
	 SetEditDisabled(kEditDisable);

	 MapSubwindows();

	 TGDimension size = GetDefaultSize();

	 Resize(size);

	 //---- position relative to the parent's window

	 CenterOnParent();

	 //---- make the message box non-resizable

	 SetWMSize(size.fWidth, size.fHeight);
	 SetWMSizeHints(size.fWidth, size.fHeight, size.fWidth, size.fHeight, 0, 0);

	 const char *wname = (dlg_type == kFDSave) ? "Save As..." : "Open";
	 SetWindowName(wname);
	 SetIconName(wname);
	 SetClassHints("FileDialog", "FileDialog");

	 SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
				    kMWMDecorMinimize | kMWMDecorMenu,
		     kMWMFuncAll |  kMWMFuncResize    | kMWMFuncMaximize |
				    kMWMFuncMinimize,
		     kMWMInputModeless);

	 MapWindow();
	 fFc->DisplayDirectory();
	 fClient->WaitFor(this);
   }

   TLGFileDialog::~TLGFileDialog()
   {
      if (IsZombie()) return ;
      TString str = fCheckB->GetString() ;
      if (str.Contains("Multiple"))
	 fCheckB->Disconnect("Toggled(Bool_t)") ;
      fClient->FreePicture(fPcdup) ;
      fClient->FreePicture(fPnewf) ;
      fClient->FreePicture(fPlist) ;
      fClient->FreePicture(fPdetails) ;
      delete fFc ;
   }

   void TLGFileDialog::CloseWindow()
   {
      // Close the file dialog.
      if (fFileInfo->fFilename)
	 delete [] fFileInfo->fFilename ;
      // The TGFileDialog destructor gets called next.
   }

   Bool_t TLGFileDialog::ProcessMessage(Long_t msg, Long_t parm1, Long_t)
   {
      // Process messages generated by the user input in the file dialog.
      if (!fFc->GetDisplayStat()) return kTRUE; // Cancel button was pressed

      TGTreeLBEntry *e;
      TGTextLBEntry *te;
      TGFileItem *f;
      void *p = 0;
      TString txt;
      TString sdir = gSystem->WorkingDirectory();

      switch (GET_MSG(msg)) {
	 case kC_COMMAND:
	    switch (GET_SUBMSG(msg)) {
	       case kCM_BUTTON:
		  switch (parm1) {
		     case kIDF_OK:
			// same code as under kTE_ENTER
			if (fTbfname->GetTextLength() == 0) {
			   txt = "Please provide file name or use \"Cancel\"";
			   new TGMsgBox(fClient->GetRoot(), GetMainFrame(),
					"Missing File Name", txt, kMBIconExclamation,
					kMBOk);
			   return kTRUE;
			} else if (!gSystem->AccessPathName(fTbfname->GetString(), kFileExists) &&
				   !strcmp(fOk->GetTitle(), "Save") &&
				   (!(fCheckB->GetState() == kButtonDown))) {
			   Int_t ret;
			   txt = TString::Format("File name %s already exists, OK to overwrite it?",
						 fTbfname->GetString());
			   new TGMsgBox(fClient->GetRoot(), GetMainFrame(),
					"File Name Exist", txt.Data(), kMBIconExclamation,
					kMBYes | kMBNo, &ret);
			   if (ret == kMBNo)
			      return kTRUE;
			}
			if (fFileInfo->fMultipleSelection) {
			   if (fFileInfo->fFilename)
			      delete [] fFileInfo->fFilename;
			   fFileInfo->fFilename = 0;
			}
			else {
			   if (fFileInfo->fFilename)
			      delete [] fFileInfo->fFilename;
			   if (gSystem->IsAbsoluteFileName(fTbfname->GetString()))
			      fFileInfo->fFilename = StrDup(fTbfname->GetString());
			   else
			      fFileInfo->fFilename = gSystem->ConcatFileName(fFc->GetDirectory(),
									     fTbfname->GetString());
			   fFileInfo->fFilename = StrDup(gSystem->UnixPathName(fFileInfo->fFilename));
			}
			if (fCheckB && (fCheckB->GetState() == kButtonDown))
			   fFileInfo->fOverwrite = kTRUE;
			else
			   fFileInfo->fOverwrite = kFALSE;
			DeleteWindow();
			break;

		     case kIDF_CANCEL:
			if (fFileInfo->fFilename)
			   delete [] fFileInfo->fFilename;
			fFileInfo->fFilename = 0;
			if (fFc->GetDisplayStat()) 
			   fFc->SetDisplayStat(kFALSE);
			if (fFileInfo->fFileNamesList != 0) {
			   fFileInfo->fFileNamesList->Delete();
			   fFileInfo->fFileNamesList = 0;
			}
			DeleteWindow();
			return kTRUE;   //no need to redraw fFc
			break;

		     case kIDF_CDUP:
			 fFc->ChangeDirectory("..");
			fTreeLB->Update(fFc->GetDirectory());
			if (fFileInfo->fIniDir) delete [] fFileInfo->fIniDir;
			fFileInfo->fIniDir = StrDup(fFc->GetDirectory());
			if (strcmp(gSystem->WorkingDirectory(),fFc->GetDirectory())) {
			   gSystem->cd(fFc->GetDirectory());
			}
			break;

		     case kIDF_NEW_FOLDER: {
			char answer[128];
			strcpy(answer, "(empty)");
			new TGInputDialog(gClient->GetRoot(), GetMainFrame(),
					  "Enter directory name:",
					  answer/*"(empty)"*/, answer);

			while ( strcmp(answer, "(empty)") == 0 ) {
			   new TGMsgBox(gClient->GetRoot(), GetMainFrame(), "Error",
					"Please enter a valid directory name.",
					kMBIconStop, kMBOk);
			   new TGInputDialog(gClient->GetRoot(), GetMainFrame(),
					     "Enter directory name:",
					     answer, answer);
			}
			if ( strcmp(answer, "") == 0 )  // Cancel button was pressed
			   break;
			
			if (strcmp(gSystem->WorkingDirectory(),fFc->GetDirectory())) {
			   gSystem->cd(fFc->GetDirectory());
			}
			if ( gSystem->MakeDirectory(answer) != 0 )
			   new TGMsgBox(gClient->GetRoot(), GetMainFrame(), "Error", 
					TString::Format("Directory name \'%s\' already exists!", answer),
					kMBIconStop, kMBOk);
			else {
			   fFc->DisplayDirectory();
			}
			gSystem->ChangeDirectory(sdir.Data());
			break;
		     }

		     case kIDF_LIST:
			fFv->SetViewMode(kLVList);
			fDetails->SetState(kButtonUp);
			break;

		     case kIDF_DETAILS:
			fFv->SetViewMode(kLVDetails);
			fList->SetState(kButtonUp);
			break;
		  }
		  break;

	       case kCM_COMBOBOX:
		  switch (parm1) {
		     case kIDF_FSLB:
			e = (TGTreeLBEntry *) fTreeLB->GetSelectedEntry();
			if (e) {
			   fFc->ChangeDirectory(e->GetPath()->GetString());
			   fTreeLB->Update(fFc->GetDirectory());
			   if (fFileInfo->fIniDir) delete [] fFileInfo->fIniDir;
			   fFileInfo->fIniDir = StrDup(fFc->GetDirectory());
			   if (strcmp(gSystem->WorkingDirectory(),fFc->GetDirectory())) {
			      gSystem->cd(fFc->GetDirectory());
			   }
			}
			break;

		     case kIDF_FTYPESLB:
			 te = (TGTextLBEntry *) fTypes->GetSelectedEntry();
			if (te) {
			   //fTbfname->Clear();
			   //fTbfname->AddText(0, fFileInfo->fFileTypes[te->EntryId()+1]);
			   fFileInfo->fFileTypeIdx = te->EntryId();
			   fFc->SetFilter(fFileInfo->fFileTypes[fFileInfo->fFileTypeIdx+1]);
			   fFc->DisplayDirectory();
			   fClient->NeedRedraw(fName);
			}
			break;
		  }
		  break;

	       default:
		  break;
	    } // switch(GET_SUBMSG(msg))
	    break;

	 case kC_CONTAINER:
	     switch (GET_SUBMSG(msg)) {
	       case kCT_ITEMCLICK:
		  if (parm1 == kButton1) {
		     if (fFc->NumSelected() > 0) {
			if ( fFileInfo->fMultipleSelection == kFALSE ) {
			   TGLVEntry *e2 = (TGLVEntry *) fFc->GetNextSelected(&p);
			   fTbfname->Clear();
			   fTbfname->AddText(0, e2->GetItemName()->GetString());
			   fClient->NeedRedraw(fName);
			}
			else {
			   TString tmpString;
			   TList *tmp = fFc->GetSelectedItems();
			   TObjString *el;
			   TIter next(tmp);
			   if ( fFileInfo->fFileNamesList != 0 ) {
			      fFileInfo->fFileNamesList->Delete();
			   }
			   else {
			      fFileInfo->fFileNamesList = new TList();
			   }
			   while ((el = (TObjString *) next())) {
			      tmpString += "\"" + el->GetString() + "\" ";
			      fFileInfo->fFileNamesList->Add(new TObjString(
				 gSystem->ConcatFileName(fFc->GetDirectory(),
							 el->GetString())));
			   }
			   fTbfname->Clear();
			   fTbfname->AddText(0, tmpString);
			   fClient->NeedRedraw(fName);
			}
		     }
		  }
		  break;

	       case kCT_ITEMDBLCLICK:

		   if (parm1 == kButton1) {
		     if (fFc->NumSelected() == 1) {
			f = (TGFileItem *) fFc->GetNextSelected(&p);
			if (R_ISDIR(f->GetType())) {
			   fFc->ChangeDirectory(f->GetItemName()->GetString());
			   fTreeLB->Update(fFc->GetDirectory());
			   if (fFileInfo->fIniDir) delete [] fFileInfo->fIniDir;
			   fFileInfo->fIniDir = StrDup(fFc->GetDirectory());
			   if (strcmp(gSystem->WorkingDirectory(),fFc->GetDirectory())) {
			      gSystem->cd(fFc->GetDirectory());
			   }
			} else {
			   if (!strcmp(fOk->GetTitle(), "Save") &&
			       (!(fCheckB->GetState() == kButtonDown))) {

			      Int_t ret;
			      txt = TString::Format("File name %s already exists, OK to overwrite it?",
						    fTbfname->GetString());
			      new TGMsgBox(fClient->GetRoot(), GetMainFrame(),
					   "File Name Exist", txt.Data(), kMBIconExclamation,
					   kMBYes | kMBNo, &ret);
			      if (ret == kMBNo)
				 return kTRUE;
			   }
			   if (fFileInfo->fFilename)
			      delete [] fFileInfo->fFilename;
			   if (gSystem->IsAbsoluteFileName(fTbfname->GetString()))
			      fFileInfo->fFilename = StrDup(fTbfname->GetString());
			   else
			      fFileInfo->fFilename = gSystem->ConcatFileName(fFc->GetDirectory(),
									     fTbfname->GetString());
			   fFileInfo->fFilename = StrDup(gSystem->UnixPathName(fFileInfo->fFilename));
			   if (fCheckB && (fCheckB->GetState() == kButtonDown))
			      fFileInfo->fOverwrite = kTRUE;
			   else
			      fFileInfo->fOverwrite = kFALSE;

			   DeleteWindow();
			}
		     }
		  }

		  break;

	       default:
		  break;

	    } // switch(GET_SUBMSG(msg))
	    break;

	 case kC_TEXTENTRY:
	     switch (GET_SUBMSG(msg)) {
	       case kTE_ENTER:
		  // same code as under kIDF_OK
		  if (fTbfname->GetTextLength() == 0) {
		     const char *txt2 = "Please provide file name or use \"Cancel\"";
		     new TGMsgBox(fClient->GetRoot(), GetMainFrame(),
				  "Missing File Name", txt2, kMBIconExclamation,
				  kMBOk);
		     return kTRUE;
		  } else if (!gSystem->AccessPathName(fTbfname->GetString(), kFileExists)) {
		     FileStat_t buf;
		     gSystem->GetPathInfo(fTbfname->GetString(), buf);
		     if (R_ISDIR(buf.fMode)) {
			fFc->ChangeDirectory(fTbfname->GetString());
			fTreeLB->Update(fFc->GetDirectory());
			if (strcmp(gSystem->WorkingDirectory(), fFc->GetDirectory())) {
			   gSystem->cd(fFc->GetDirectory());
			}
			// fName->SetText("", kFALSE); Can't use the emit arg with 5.20.00
			fName->SetText("");
			return kTRUE;
		     }
		     else if (!strcmp(fOk->GetTitle(), "Save") && 
			     (!(fCheckB->GetState() == kButtonDown))) {
			Int_t ret;
			txt = TString::Format("File name %s already exists, OK to overwrite it?",
					      fTbfname->GetString());
			new TGMsgBox(fClient->GetRoot(), GetMainFrame(),
				     "File Name Exist", txt.Data(), kMBIconExclamation,
				     kMBYes | kMBNo, &ret);
			if (ret == kMBNo)
			   return kTRUE;
		     }
		  }
		  if (fFileInfo->fFilename)
		     delete [] fFileInfo->fFilename;
		  fFileInfo->fFilename = gSystem->ConcatFileName(fFc->GetDirectory(),
								 fTbfname->GetString());
		  fFileInfo->fFilename = StrDup(gSystem->UnixPathName(fFileInfo->fFilename));
		  if (fCheckB && (fCheckB->GetState() == kButtonDown))
		     fFileInfo->fOverwrite = kTRUE;
		  else
		     fFileInfo->fOverwrite = kFALSE;
		  DeleteWindow();
		  break;

	       default:
		  break;
	    }
	    break;

	 default:
	    break;

      } // switch(GET_MSG(msg))

      fClient->NeedRedraw(fFc);
      return kTRUE;
   }




#else
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGFileDialog                                                        //
//                                                                      //
// File dialog function                          		        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   Bool_t TLGFileDialog (const TGWindow* main, TGFileInfo& info, 
                     EFileDialogMode dlg_type, const char* defext,
                     Bool_t ask)
   {
      const int maxpath = 1024;
      static char fdlgPath[maxpath+1] = "";
      char savePath[maxpath+1];
   
      // set path 
      if (strlen (fdlgPath) == 0) {
         strncpy (fdlgPath, gSystem->WorkingDirectory(), maxpath);
         fdlgPath[maxpath] = 0;
      }
      strncpy (savePath, gSystem->WorkingDirectory(), maxpath);
      savePath[maxpath] = 0;
      if (info.fIniDir != 0) {
         if (!gSystem->ChangeDirectory (info.fIniDir)) {
            gSystem->ChangeDirectory (fdlgPath);
         }
      }
      else {
         gSystem->ChangeDirectory (fdlgPath);
      }
   
      // ask for filename: file save as dialog
      new TGFileDialog (gClient->GetRoot(), main, dlg_type, &info);
      gSystem->ChangeDirectory (savePath);
      if (info.fFilename == 0) {
         return kFALSE;
      }
      // add default file extension
      if ((dlg_type == kFDSave) && (defext != 0) && 
         (strchr (info.fFilename, '.') == 0)) {
         TString filename (info.fFilename);
         filename += defext;
         delete [] info.fFilename;
         info.fFilename = new char [filename.Length() + 1];
         if (info.fFilename == 0) {
            return kFALSE;
         }
         strcpy (info.fFilename, filename);
      }
      // overwrite?
      if (ask && (dlg_type == kFDSave)) {
         ifstream inp (info.fFilename);
         bool exists = inp;
         inp.close();
         if (exists) {
            Int_t ret;
            TString msg = TString ("File ") + info.fFilename + 
               " exists.\nOverwrite it?";
            new TGMsgBox (gClient->GetRoot(), main, "Replace", 
                         msg, kMBIconQuestion, kMBYes | kMBNo, &ret);
            if (ret != kMBYes) {
               delete [] info.fFilename;
               info.fFilename = 0;
               return kFALSE;
            }
         }
      }
      // keep track of path
      strncpy (fdlgPath, gSystem->DirName (info.fFilename), maxpath); 
      fdlgPath[maxpath] = 0;
      return kTRUE;
   }
#endif

}
