
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGComboTree, TLGComboTreePopup                                      //
//                                                                      //
// A combobox (also known as a drop down listbox) allows the selection  //
// of one item out of a list of items. The selected item is visible in  //
// a little window. To view the list of possible items one has to click //
// on a button on the right of the little window. This will drop down   //
// a listbox. After selecting an item from the listbox the box will     //
// disappear and the newly selected item will be shown in the little    //
// window.                                                              //
//                                                                      //
// The TLGComboTree is user callable. The TLGComboTreePopup is a service//
// class of the combobox.                                               //
//                                                                      //
// Selecting an item in the combobox will generate the event:           //
// kC_COMMAND, kCM_COMBOBOX, combobox id, item id.                      //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TLGComboTree.hh"
#include "TLGComboEditBox.hh"
#include <TGScrollBar.h>
#include <TGPicture.h>
#include <TGTextEntry.h>
#include <TVirtualX.h>

// remove after testing
//#include <iostream>


namespace ligogui {
   using namespace std;

   static const int my_debug = 0 ;

   Cursor_t TLGComboTreePopup::fgDefaultCursor = (Cursor_t)-1;


   // ClassImp(TLGComboTreePopup)
   // ClassImp(TLGComboTree)

//______________________________________________________________________________
   TLGComboTreePopup::TLGComboTreePopup(const TGWindow *p, TLGComboTree* combo,
                     UInt_t w, UInt_t h, UInt_t options, ULong_t back) :
   TGCompositeFrame (p, w, h, options, back), fTreebox (combo)
   {
      if (fgDefaultCursor == (Cursor_t)-1) {
         fgDefaultCursor = gVirtualX->CreateCursor (kArrowRight);
      }
   // Create a combo box popup frame.
   
      SetWindowAttributes_t wattr;
   
      wattr.fMask = kWAOverrideRedirect | kWASaveUnder |
         kWABorderPixel      | kWABorderWidth;
      wattr.fOverrideRedirect = kTRUE;
      wattr.fSaveUnder = kTRUE;
      wattr.fBorderPixel = fgBlackPixel;
      wattr.fBorderWidth = 1;
      gVirtualX->ChangeWindowAttributes(fId, &wattr);
   
      gVirtualX->SelectInput(fId, kStructureNotifyMask);
   }

//______________________________________________________________________________
   Bool_t TLGComboTreePopup::HandleButton(Event_t *event)
   {
   // Handle mouse button event in combo box popup.
   
      if (event->fType == kButtonRelease) {
         EndPopup();
      }
      return kTRUE;
   }

//______________________________________________________________________________
   void TLGComboTreePopup::EndPopup()
   {
   // Ungrab pointer and unmap popup window.
   
      gVirtualX->GrabPointer(0, 0, 0, 0, kFALSE);
      UnmapWindow();
   }

//______________________________________________________________________________
   void TLGComboTreePopup::PlacePopup(Int_t x, Int_t y, UInt_t w, UInt_t h)
   {
   // Popup combo box popup window at the specified place.
   
      Int_t  rx, ry;
      UInt_t rw, rh;
   
   // Parent is root window for the popup:
      gVirtualX->GetWindowSize(fParent->GetId(), rx, ry, rw, rh);
   
      if (x < 0) x = 0;
      if (x + fWidth > rw) x = rw - fWidth;
      if (y < 0) y = 0;
      if (y + fHeight > rh) y = rh - fHeight;
   
      MoveResize(x, y, w, h);
      MapSubwindows();
      Layout();
      MapRaised();
   // ((TLGLBTree*)(fTreebox->GetLBTree()))->Layout();
   //    gClient->NeedRedraw ((TGWindow*) (fTreebox->GetLBTree()));
   
      gVirtualX->GrabPointer(fId, kButtonPressMask | kButtonReleaseMask |
                           kPointerMotionMask, kNone, fgDefaultCursor);
   
      fClient->WaitForUnmap(this);
      EndPopup();
   }

//______________________________________________________________________________
   class ComboScrollBarElement : public TGScrollBarElement {
   public:
      ComboScrollBarElement (const TGWindow *p, const TGPicture* pic,
                        UInt_t w, UInt_t h, UInt_t options)
      : TGScrollBarElement (p, pic, w, h, options) {
      }
      virtual Bool_t HandleButton(Event_t *event) {         
         return ((TLGComboTree*)fParent)->HandleButton(event); }
   };

//______________________________________________________________________________
   TLGComboTree::TLGComboTree(const TGWindow *p, Int_t id, Bool_t editable,
                     UInt_t options, ULong_t back) :
   TGCompositeFrame (p, 10, 10, options, back), fEditable (editable)
#ifdef TEST_LOCALRATE
      ,fRate(0)
#endif
   {
   // Create a combo box widget.
   
      fActive = kTRUE;
      fComboBoxId = id;
      fPopupHeight = 100;
      fMsgWindow = p;
      fBpic = fClient->GetPicture("arrow_down.xpm");
   
      if (!fBpic)
         Error("TLGComboTree", "arrow_down.xpm not found");
   
      if (fEditable) {
         fSelEntry = new TGTextEntry (this, "", 0);
         fSelEntry->ChangeOptions (0);
      }
      else {
         fSelEntry = new TLGTextLBEntry(this, new TGString(""), 0);
      }
      fDDButton = new ComboScrollBarElement(this, fBpic, kDefaultScrollBarWidth,
                           kDefaultScrollBarWidth, kRaisedFrame);
   
      AddFrame(fSelEntry, fLhs = new TGLayoutHints(kLHintsLeft |
                                kLHintsExpandY | kLHintsExpandX));
                                                //0, 0, 1, 0));
      AddFrame(fDDButton, fLhb = new TGLayoutHints(kLHintsRight |
                                kLHintsExpandY));
   
      // pop up listbox
      fComboFrame = new TLGComboTreePopup(fClient->GetRoot(), this, 100, 
                           fPopupHeight, kVerticalFrame);
   
      fListBox = new TLGLBTree (fComboFrame, fComboBoxId, kChildFrame);
      fListBox->Resize(100, fPopupHeight);
      fListBox->Associate(this);
      fLhdd = new TGLayoutHints(kLHintsExpandX | kLHintsExpandY);
      fComboFrame->AddFrame(fListBox, fLhdd);
      fComboFrame->MapSubwindows();
      fComboFrame->Resize(fComboFrame->GetDefaultSize());
   
      if (fEditable) {
         gVirtualX->GrabButton(fDDButton->GetId(), kButton1, kAnyModifier, 
                              kButtonPressMask | kButtonReleaseMask, 
                              kNone, kNone);
      }
      else {
         gVirtualX->GrabButton(fId, kButton1, kAnyModifier, kButtonPressMask |
                              kButtonReleaseMask, kNone, kNone);
      }
   
   // Drop down listbox of combo box should react to pointer motion
   // so it will be able to Activate() (i.e. highlight) the different
   // items when the mouse crosses.
      gVirtualX->SelectInput(fListBox->GetContainer()->GetId(), kButtonPressMask |
                           kButtonReleaseMask | kPointerMotionMask);
   }

//______________________________________________________________________________
   TLGComboTree::~TLGComboTree()
   {
   // Delete a combo box widget.
      delete fDDButton;
      delete fSelEntry;
      delete fListBox;
      delete fComboFrame;
      delete fLhs;
      delete fLhb;
      delete fLhdd;
      fClient->FreePicture (fBpic);
   }

//______________________________________________________________________________
   const char* TLGComboTree::SelGetText () const
   {
   
      if (fEditable) {
         return ((TGTextEntry*)fSelEntry)->GetText();
      }
      else {
         return ((TLGTextLBEntry*)fSelEntry)->GetText()->GetString();
      }
   }

//______________________________________________________________________________
   void TLGComboTree::SelSetText (const char* txt)
   {
   
      if (fEditable) {
         ((TGTextEntry*)fSelEntry)->SetText (txt);
      }
      else {
         ((TLGTextLBEntry*)fSelEntry)->SetText (new TGString(txt));
      }
   }

//______________________________________________________________________________
   void TLGComboTree::DrawBorder()
   {
   // Draw border of combo box widget.
   
#if ROOT_VERSION_CODE > ROOT_VERSION(3,5,5)
      GContext_t shadow = GetShadowGC()();
      GContext_t black = GetBlackGC()();
      GContext_t hilight = GetHilightGC()();
      GContext_t bckgnd = GetBckgndGC()();
#else
      GContext_t shadow = fgShadowGC();
      GContext_t black = fgBlackGC();
      GContext_t hilight = fgHilightGC();
      GContext_t bckgnd = fgBckgndGC();
#endif
      switch (fOptions & (kSunkenFrame | kRaisedFrame | kDoubleBorder)) {
         case kSunkenFrame | kDoubleBorder:
            gVirtualX->DrawLine(fId, shadow, 0, 0, fWidth-2, 0);
            gVirtualX->DrawLine(fId, shadow, 0, 0, 0, fHeight-2);
            gVirtualX->DrawLine(fId, black, 1, 1, fWidth-3, 1);
            gVirtualX->DrawLine(fId, black, 1, 1, 1, fHeight-3);
         
            gVirtualX->DrawLine(fId, hilight, 0, fHeight-1, fWidth-1, fHeight-1);
            gVirtualX->DrawLine(fId, hilight, fWidth-1, fHeight-1, fWidth-1, 0);
            gVirtualX->DrawLine(fId, bckgnd,  1, fHeight-2, fWidth-2, fHeight-2);
            gVirtualX->DrawLine(fId, bckgnd,  fWidth-2, 1, fWidth-2, fHeight-2);
            break;
      
         default:
            TGCompositeFrame::DrawBorder();
            break;
      }
   }

//______________________________________________________________________________
   Bool_t TLGComboTree::HandleButton(Event_t *event)
   {
   // Handle mouse button events in the combo box.
      if (!fActive) {
         return kTRUE; 
      }
      if (event->fType == kButtonPress) {
         if ((Window_t)event->fUser[0] == fDDButton->GetId()) 
         		// fUser[0] = child window
            fDDButton->SetState(kButtonDown);
      } 
      else {
         int      ax, ay;
         Window_t wdummy;
      
         fDDButton->SetState(kButtonUp);
         gVirtualX->TranslateCoordinates(fId, (fComboFrame->GetParent())->GetId(),
                              0, fHeight, ax, ay, wdummy);
      
         fComboFrame->PlacePopup(ax, ay, fWidth-2, fPopupHeight);
         //fComboFrame->GetDefaultHeight());
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGComboTree::ProcessMessage(Long_t msg, Long_t, Long_t parm2)
   {
   // Process messages generated by the listbox and forward
   // messages to the combobox message handling window. Parm2 contains
   // the id of the selected listbox entry.
   
      switch (GET_MSG(msg)) {
         case kC_LISTTREE:
            {
               switch (GET_SUBMSG(msg)) {
                  case kCT_ITEMCLICK:
                     {
                        TLGLBTreeEntry *e = fListBox->GetSelectedEntry();
#ifdef TEST_LOCALRATE
			fRate = 0 ; // Set the rate to 0 in case this isn't a channel
#endif
                        // only accept if end of branch
                        if (e->GetFirstChild() == 0) {
#ifdef TEST_LOCALRATE
			   // Set the data rate.
			   fRate = (long) fListBox->GetSelectedUserData() ;
			   if (my_debug) cerr << "TLGComboTree::ProcessMessage() - setting fRate for " << e->GetFullname() << " to " << fRate << endl ;
#endif
                           SelSetText (e->GetFullname());
                           Layout();
                           fComboFrame->EndPopup();
                           SendMessage(fMsgWindow, MK_MSG(kC_COMMAND, kCM_COMBOBOX), 
                                      fComboBoxId, parm2);
                        }
                        break;
                     }
               }
               break;
            }
         case kC_TEXTENTRY:
            {
               switch (GET_SUBMSG(msg)) {
                  case kTE_TEXTCHANGED:
                  case kTE_ENTER:
                     {
                        SendMessage (fMsgWindow, msg, fComboBoxId, 0);
                        break;
                     }
               }
               break;
            }
         default:
            {
               break;
            }
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGComboTree::Select (void* userData)
   {
      TLGLBTreeEntry *e = fListBox->Select(userData);
      if (!e) {
         SelSetText ("");
         return kFALSE;
      }
      else {
         SelSetText (e->GetFullname());
         return kTRUE; 
      }
   }

//______________________________________________________________________________
   Bool_t TLGComboTree::SelectByName (const char* fullname, const int rate)
   { 
#ifdef TEST_LOCALRATE
      fRate = rate ;
#endif
      // The fListBox is a TLGLBTree pointer.  Call TLGLBTree::SelectByName()
      TLGLBTreeEntry *e = fListBox->SelectByName (fullname, kTRUE, rate);
      if (!e) {
         SelSetText ("");
         return kFALSE;
      }
      else {
         SelSetText (e->GetFullname());
         return kTRUE; 
      }
   }


//______________________________________________________________________________
   Bool_t TLGComboTree::SetByName (const char* fullname, const int rate)
   { 
      TLGLBTreeEntry *e = fListBox->SelectByName (fullname, kTRUE, rate);
#ifdef TEST_LOCALRATE
      fRate = rate ;
#endif
      if (!e) {
         SelSetText (fullname);
         return kFALSE;
      }
      else {
         SelSetText (e->GetFullname());
         return kTRUE; 
      }
   }


//______________________________________________________________________________
   const char* TLGComboTree::GetDisplayedName () const
   { 
      return SelGetText();
   }


}

