/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "TLGTextEditor.hh"
#include <KeySymbols.h>
#include <TGMsgBox.h>
#include <iostream>
#include <TGFileDialog.h>
#include "TLGEntry.hh"
#include <TVirtualX.h>

namespace ligogui {
   using namespace std;

   static const char* const gSaveAsTypes[] = { 
   "Text file", "*.txt",
   "All files", "*",
   0, 0 };


   static const char* const gOpenTypes[] = { 
   "Text file", "*.txt",
   "All files", "*",
   0, 0 };


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGTextEdit		                                                //
//                                                                      //
// Text editor							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGTextEdit::TLGTextEdit (const TGWindow* parent, UInt_t w, UInt_t h, 
                     Int_t id, UInt_t sboptions, ULong_t back)
   : TGTextEdit (parent, w, h, id, sboptions, back), fState (kTRUE)
   {
   }
//______________________________________________________________________________
   TLGTextEdit::TLGTextEdit (const TGWindow* parent, UInt_t w, UInt_t h, 
                     TGText* text, Int_t id, UInt_t sboptions, 
                     ULong_t back)
   : TGTextEdit (parent, w, h, text, id, sboptions, back), fState (kTRUE)
   {
   }
//______________________________________________________________________________
   TLGTextEdit::TLGTextEdit (const TGWindow* parent, UInt_t w, UInt_t h, 
                     const char* string, Int_t id, UInt_t sboptions, 
                     ULong_t back)
   : TGTextEdit (parent, w, h, string, id, sboptions, back), 
   fState (kTRUE)
   {
   }

//______________________________________________________________________________
   void TLGTextEdit::SetState (Bool_t state)
   {
      fState = state;
      CursorOff();
      TGTextEdit::DrawCursor (fCursorState);
   }

//______________________________________________________________________________
   void TLGTextEdit::DrawCursor (Int_t mode)
   {
      if (fState) TGTextEdit::DrawCursor (mode);
   }

//______________________________________________________________________________
   void TLGTextEdit::SendUpdateMsg()
   {
      SendMessage(fMsgWindow, 
		  MK_MSG(kC_TEXTENTRY, EWidgetMessageTypes(kTE_TEXTUPDATED)), 
		  fWidgetId, kTRUE);
   }

//______________________________________________________________________________
   std::string TLGTextEdit::GetString(char cmt) const
   {
      const char dle(16);
      //   Note that this function starts with a root TString containing 
      //   the entire text of the TextEdit object (lines sparated by 
      //   newline characters), removes the funky tab padding characters
      //   and reformats it into a STL string.
      //

      // work-around GetText()->AsString(), which is buggy.

      auto text_block = GetText();

      // AsString() is broken.  The only public access is
      // TGText::GetChar()

      TGLongPosition pos(0,0);
      char c;
      std::string txt = "";
      while((c = text_block->GetChar(pos)) >= 0)
      {
        do {
          if(c != cmt) {
            txt += c;
          }
          pos.fX += 1;
        } while((c = text_block->GetChar(pos)) >= 0 && c != cmt);
        pos.fX = 0;
        pos.fY += 1;
        txt += '\n';
      }


      int N = txt.length();

      std::string cmd;
      cmd.reserve(N);

      bool incmt = false;
      for (int i=0; i<N; ++i) {
	 char txti = txt[i];
	 incmt = (incmt && txti != '\n') || (txti == cmt);
	 if (!incmt && txti != dle) cmd += txti;
      }
      return cmd;
   }

//______________________________________________________________________________
   Bool_t TLGTextEdit::HandleKey (Event_t* event)
   {
   // The key press event handler converts a key press to some line editor
   // action.
   
      Bool_t mark_ok = kFALSE;
      char   input[10];
      Int_t  n;
      UInt_t keysym;
   
      if (event->fType == kGKeyPress) {
         gVirtualX->LookupString(event, input, sizeof(input), keysym);
         n = strlen(input);

         AdjustPos();
      
         switch ((EKeySym)keysym) {   // ignore these keys
            case kKey_Shift:
            case kKey_Control:
            case kKey_Meta:
            case kKey_Alt:
            case kKey_CapsLock:
            case kKey_NumLock:
            case kKey_ScrollLock:
               return kTRUE;
            default:
               break;
         }
         if (event->fState & kKeyControlMask) {   // Cntrl key modifier pressed
            switch((EKeySym)keysym & ~0x20) {   // treat upper and lower the same
               case kKey_A:
                  mark_ok = kTRUE;
                  Home();
                  break;
               case kKey_B:
                  mark_ok = kTRUE;
                  PrevChar();
                  break;
               case kKey_C:
                  Copy();
                  return kTRUE;
               case kKey_D:
                  if (!fState) 
                     break;
                  if (fIsMarked)
                     Cut();
                  else {
                     Long_t len = fText->GetLineLength(fCurrent.fY);
                     if (fCurrent.fY == fText->RowCount()-1 && 
			 fCurrent.fX == len) {
                        gVirtualX->Bell(0);
                        return kTRUE;
                     }
                     NextChar();
                     DelChar();
                  }
                  SendUpdateMsg();
                  break;
               case kKey_E:
                  mark_ok = kTRUE;
                  End();
                  break;
               case kKey_F:
                  mark_ok = kTRUE;
                  NextChar();
                  break;
               case kKey_H:
                  if (!fState) 
                     break;
                  DelChar();
                  SendUpdateMsg();
                  break;
               case kKey_K:
                  if (!fState) 
                     break;
                  End();
                  fIsMarked = kTRUE;
                  Mark(fCurrent.fX, fCurrent.fY);
                  Cut();
                  SendUpdateMsg();
                  break;
               case kKey_U:
                  if (!fState) 
                     break;
                  Home();
                  UnMark();
                  fMarkedStart.fY = fMarkedEnd.fY = fCurrent.fY;
                  fMarkedStart.fX = fMarkedEnd.fX = fCurrent.fX;
                  End();
                  fIsMarked = kTRUE;
                  Mark(fCurrent.fX, fCurrent.fY);
                  Cut();
                  SendUpdateMsg();
                  break;
               case kKey_V:
               case kKey_Y:
                  if (!fState) 
                     break;
                  Paste();
                  SendUpdateMsg();
                  return kTRUE;
               case kKey_X:
                  if (!fState) 
                     break;
                  Cut();
                  SendUpdateMsg();
                  return kTRUE;
               default:
                  return kTRUE;
            }
         }
         if (n && keysym >= 32 && keysym < 127 &&     // printable keys
            !(event->fState & kKeyControlMask) &&
            (EKeySym)keysym != kKey_Delete &&
            (EKeySym)keysym != kKey_Backspace) {
         
            if (fState) {
               if (fIsMarked)
                  Cut();
               InsChar(input[0]);
               SendUpdateMsg();
            }
         } 
         else {
         
            switch ((EKeySym)keysym) {
               case kKey_F3:
               // typically FindAgain action
                  SendMessage(fMsgWindow, MK_MSG(kC_TEXTVIEW, kTXT_F3), fWidgetId,
                             kTRUE);
                  SetMenuState();
                  if (fMenu->IsEntryEnabled(kM_SEARCH_FINDAGAIN)) {
                     SendMessage(this, MK_MSG(kC_COMMAND, kCM_MENU),
                                kM_SEARCH_FINDAGAIN, 0);
                     FindAgain();
                  }
                  break;
               case kKey_Delete:
                  if (!fState) 
                     break;
                  if (fIsMarked)
                     Cut();
                  else {
                     Long_t len = fText->GetLineLength(fCurrent.fY);
                     if (fCurrent.fY == fText->RowCount()-1 && fCurrent.fX == len) {
                        gVirtualX->Bell(0);
                        return kTRUE;
                     }
                     NextChar();
                     DelChar();
                  }
                  SendUpdateMsg();
                  break;
               case kKey_Return:
               case kKey_Enter:
                  if (!fState) 
                     break;
                  BreakLine();
                  SendUpdateMsg();
                  break;
               case kKey_Tab:
                  if (!fState) 
                     break;
                  InsChar('\t');
		  Update();
                  SendUpdateMsg();
                  break;
               case kKey_Backspace:
                  if (!fState) 
                     break;
                  if (fIsMarked)
                     Cut();
                  else
                     DelChar();
                  SendUpdateMsg();
                  break;
               case kKey_Left:
                  mark_ok = kTRUE;
                  PrevChar();
                  break;
               case kKey_Right:
                  mark_ok = kTRUE;
                  NextChar();
                  break;
               case kKey_Up:
                  mark_ok = kTRUE;
                  LineUp();
                  break;
               case kKey_Down:
                  mark_ok = kTRUE;
                  LineDown();
                  break;
               case kKey_PageUp:
                  mark_ok = kTRUE;
                  ScreenUp();
                  break;
               case kKey_PageDown:
                  mark_ok = kTRUE;
                  ScreenDown();
                  break;
               case kKey_Home:
                  mark_ok = kTRUE;
                  Home();
                  break;
               case kKey_End:
                  mark_ok = kTRUE;
                  End();
                  break;
               case kKey_Insert:           // switch on/off insert mode
                  SetInsertMode(GetInsertMode() == kInsert ? kReplace : kInsert);
                  break;
               default:
                  break;
            }
         }
         if ((event->fState & kKeyShiftMask) && mark_ok) {
            fIsMarked = kTRUE;
            Mark(fCurrent.fX, fCurrent.fY);
            Copy();
            SendMessage(fMsgWindow, MK_MSG(kC_TEXTVIEW, kTXT_ISMARKED), fWidgetId,
                       kTRUE);
            Marked(kTRUE);
         } 
         else {
            if (fIsMarked) {
               fIsMarked = kFALSE;
               UnMark();
               SendMessage(fMsgWindow, MK_MSG(kC_TEXTVIEW, kTXT_ISMARKED),
                          fWidgetId, kFALSE);
               Marked(kFALSE);
            }
            fMarkedStart.fY = fMarkedEnd.fY = fCurrent.fY;
            fMarkedStart.fX = fMarkedEnd.fX = fCurrent.fX;
         }
      }
      return kTRUE;      
   }

//______________________________________________________________________________
   Bool_t TLGTextEdit::InsertText (TGText* text, Bool_t mark)
   {
      if (!text) {
         return kFALSE;
      }
   
      TGLongPosition start_src, end_src, pos;
      pos.fX = pos.fY = 0;
      start_src.fY = start_src.fX = 0;
      end_src.fY = text->RowCount()-1;
      end_src.fX = text->GetLineLength(end_src.fY)-1;
   
      if (end_src.fX < 0) end_src.fX = 0;
      fText->InsText(fCurrent, text, start_src, end_src);
      if (mark) {
         fIsMarked = kFALSE;
         UnMark();
         fIsMarked = kTRUE;
         fIsMarking = kTRUE;
         fMarkedStart = fMarkedEnd = fCurrent;
         Mark (fCurrent.fX + end_src.fX + 1, fCurrent.fY + end_src.fY);
         SendMessage (fMsgWindow, MK_MSG(kC_TEXTVIEW, kTXT_ISMARKED),
                     fWidgetId, kTRUE);
         Marked (kTRUE);
         fIsMarking = kFALSE;
      }
      return kTRUE;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGTextEditorLayout                                                  //
//                                                                      //
// Layout manager for simple text editor in transient window	        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class TLGTextEditorLayout : public TGLayoutManager {
   public:
      TLGTextEditorLayout (TGCompositeFrame* p, UInt_t wdef, UInt_t hdef) 
      : fDefW (wdef), fDefH (hdef), fMain (p) {
         fList = fMain->GetList(); }
      virtual void Layout ();
      virtual TGDimension GetDefaultSize() const {
         return TGDimension (fDefW, fDefH);  }
   
   protected:
      UInt_t		fDefW;
      UInt_t		fDefH;
      TGCompositeFrame*	fMain;
      TList*		fList;
   };

//______________________________________________________________________________
   void TLGTextEditorLayout::Layout ()
   {
   
      Int_t w = fMain->GetWidth();
      Int_t h = fMain->GetHeight();
      TGFrameElement* ptr;
      TIter next (fList);
      int num = 0;
      while ((ptr = (TGFrameElement*) next())) {
         switch (num) {
            // menu
            case 0:
               {
                  ptr->fFrame->MoveResize (0, 0, w, 24);
                  break;
               }
            // text edit
            case 1:
               {
                  ptr->fFrame->MoveResize (0, 24, w, h-56);
                  break;
               }
            // ok button
            case 2:
               {
                  ptr->fFrame->MoveResize (10, h-28, 80, 24);
                  break;
               }
            // cancel or update button
            case 3:
               {
                  ptr->fFrame->MoveResize (100, h-28, 80, 24);
                  break;
               }
            // cancel button
            case 4:
               {
                  ptr->fFrame->MoveResize (190, h-28, 80, 24);
                  break;
               }
         }
         ++num;
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGTextEditor                                                        //
//                                                                      //
// Simple text editor in transient window			        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGTextEditor::TLGTextEditor (const TGWindow *p, 
                     const TGWindow *main, const char* title,
                     UInt_t w, UInt_t h, TGText& text, Bool_t& ret, 
                     Bool_t modeless, Int_t id, Bool_t* done)
   : TLGTransientFrame (p, main, 200, 300), TGWidget (id),
   fRet (&ret), fDone (done), fText (&text)
   {
      SetLayoutManager (new TLGTextEditorLayout (this, 6*w, 12*h));
      fMsgWindow = 0;
      fWidgetFlags = 0;
      // Menu
      TGLayoutHints* fMenuItemLayout = 
         new TGLayoutHints (kLHintsTop | kLHintsLeft, 0, 4, 0, 0);
      fMenu = new TGMenuBar (this, 1, 1, kHorizontalFrame | kRaisedFrame);
      AddFrame (fMenu, 0);
      fMenuFile = new TGPopupMenu (fClient->GetRoot());
      fMenuFile->Associate (this);
      fMenuFile->AddEntry ("&New...", TGTextEdit::kM_FILE_NEW);
      fMenuFile->AddEntry ("&Open...", TGTextEdit::kM_FILE_OPEN);
      fMenuFile->AddSeparator();
      fMenuFile->AddEntry ("&Save", TGTextEdit::kM_FILE_SAVE);
      fMenuFile->AddEntry ("Save &As...", TGTextEdit::kM_FILE_SAVEAS);
      fMenuFile->AddSeparator();
      fMenuFile->AddEntry ("&Print...", TGTextEdit::kM_FILE_PRINT);
      fMenuFile->AddSeparator();
      fMenuFile->AddEntry ("&Close", TGTextEdit::kM_FILE_CLOSE);
      fMenu->AddPopup ("&File", fMenuFile, fMenuItemLayout);
      fMenuEdit = new TGPopupMenu (fClient->GetRoot());
      fMenuEdit->Associate (this);
      fMenuEdit->AddEntry ("C&ut         Ctrl-x", TGTextEdit::kM_EDIT_CUT);
      fMenuEdit->AddEntry ("&Copy        Ctrl-c", TGTextEdit::kM_EDIT_COPY);
      fMenuEdit->AddEntry ("&Paste       Ctrl-v", TGTextEdit::kM_EDIT_PASTE);
      fMenuEdit->AddEntry ("&Select All  Ctrl-a", TGTextEdit::kM_EDIT_SELECTALL);
      fMenu->AddPopup ("&Edit", fMenuEdit, fMenuItemLayout);
      fMenuSearch = new TGPopupMenu (fClient->GetRoot());
      fMenuSearch->Associate (this);
      fMenuSearch->AddEntry ("&Find...", TGTextEdit::kM_SEARCH_FIND);
      fMenuSearch->AddEntry ("Find &Again", TGTextEdit::kM_SEARCH_FINDAGAIN);
      fMenuSearch->AddEntry ("&Goto...", TGTextEdit::kM_SEARCH_GOTO);
      fMenu->AddPopup ("&Search", fMenuSearch, fMenuItemLayout);
   
      // Edit box
      fTextEdit = new TLGTextEdit (this, 6*w, 12*h, &text, 10);
      fTextEdit->Associate (this);
      AddFrame (fTextEdit, 0);
      // buttons
      fButton[0] = new TGTextButton (this, 
                           new TGHotString ("       &Ok       "), 1);
      fButton[0]->Associate (this);
      AddFrame (fButton[0], 0);
      if (modeless) {
         fButton[1] = new TGTextButton (this, 
                              new TGHotString ("     &Update     "), 2);
         fButton[1]->Associate (this);
         AddFrame (fButton[1], 0);
      }
      else {
         fButton[1] = 0;
      }
      fButton[2] = new TGTextButton (this, 
                           new TGHotString ("     &Cancel     "), 0);
      fButton[2]->Associate (this);
      AddFrame (fButton[2], 0);
   
      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates 
            (main->GetId(), GetParent()->GetId(),
            (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
            (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
            ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
   
      // make the message box non-resizable
      SetWMSize(width, height);
      SetWMSizeHints(0, 0, 10000, 10000, 1, 1);
   
      // set dialog box title
      fTitle = title ? title : "Simple Text Editor";
      SetWindowName (fTitle);
      SetIconName (fTitle);
      SetClassHints (fTitle, fTitle);
   
      SetMWMHints (kMWMDecorAll, kMWMFuncAll, kMWMInputModeless);
   
      MapWindow();
      if (!modeless) {
         fClient->WaitFor (this);
      }
   }

//______________________________________________________________________________
   TLGTextEditor::~TLGTextEditor()
   {
      delete fTextEdit;
      delete fButton[0];
      delete fButton[1];
      delete fMenuSearch;
      delete fMenuEdit;
      delete fMenuFile;
      delete fMenu;
      if (fDone) *fDone = kTRUE;
   }

//______________________________________________________________________________
   void TLGTextEditor::CloseWindow()
   {
      fTextEdit->GetText()->Clear();
      if (fRet) *fRet = kFALSE;
      DeleteWindow();
   }

//______________________________________________________________________________
   Bool_t TLGTextEditor::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         switch (parm1) {
            // Ok
            case 1:
               {
                  fText->Clear();
                  fText->AddText (fTextEdit->GetText());
                  fTextEdit->GetText()->Clear();
                  if (fRet) *fRet = kTRUE;
                  if (fMsgWindow) {
                     Long_t m = MK_MSG (kC_TEXTENTRY, 
                                       (EWidgetMessageTypes)kTE_TEXTUPDATED);
                     SendMessage (fMsgWindow, m, fWidgetId, 0);
                  }
                  DeleteWindow();
                  break;
               }
            // Cancel
            case 0:
               {
                  fTextEdit->GetText()->Clear();
                  if (fRet) *fRet = kFALSE;
                  DeleteWindow();
                  break;
               }
            // Update
            case 2:
               {
                  fText->Clear();
                  fText->AddText (fTextEdit->GetText());
                  if (fMsgWindow) {
                     Long_t m = MK_MSG (kC_TEXTENTRY, 
                                       (EWidgetMessageTypes)kTE_TEXTUPDATED);
                     SendMessage (fMsgWindow, m, fWidgetId, 0);
                  }
                  break;
               }
         }
      }
      // menus
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_MENU)) {
         switch (parm1) {
            // New
            case TGTextEdit::kM_FILE_NEW:
               {
                  fTextEdit->Clear();
                  break;
               }
            // File open
            case TGTextEdit::kM_FILE_OPEN:
               {
                  TGFileInfo	info;
                  info.fFilename = (char *) 0;
                  info.fIniDir = (char *) 0;
               #if ROOT_VERSION_CODE >= ROOT_VERSION(3,1,6)
                  info.fFileTypes = const_cast<const char**>(gOpenTypes);
               #else
                  info.fFileTypes = const_cast<char**>(gOpenTypes);
               #endif
	       #if 1
		  info.fFileTypeIdx = 2 ;  // Point at "all files"
		  {
		     // If the user presses cancel or doesn't choose
		     // a file, info.fFilename will be null
		     new TLGFileDialog(this, &info, kFDOpen) ;
		  }
		  if (!info.fFilename)
	       #else
                  if (!TLGFileDialog (this, info, kFDOpen, 0))
	       #endif
		  {
                     return kFALSE;
                  }
                  fTextEdit->Clear();
                  if (!fTextEdit->LoadFile (info.fFilename)) {
                     TString error = "Unable to open file:\n";
                     error += info.fFilename;
                     new TGMsgBox (gClient->GetRoot(), this, "Error", 
                                  error, kMBIconStop, kMBOk);
                  }
                  else {
                     SetWindowName (fTitle + " - " + info.fFilename);
                  }
               #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
                  delete [] info.fFilename;
               #endif
                  break;
               }
            // save
            case TGTextEdit::kM_FILE_SAVE:
               {
                  string filename = fTextEdit->GetFileName();
                  if (!filename.empty()) {
                     if (!fTextEdit->SaveFile (filename.c_str(), kFALSE)) {
                        TString error = "Unable to save file:\n";
                        error += filename.c_str();
                        new TGMsgBox (gClient->GetRoot(), this, "Error", 
                                     error, kMBIconStop, kMBOk);
                     
                     }
                     return kTRUE;
                  }
               // fall through!
               }  
            // save as
            case TGTextEdit::kM_FILE_SAVEAS:
               {
                  // file save as dialog
                  TGFileInfo info;
                  info.fFilename = (char *) 0;
                  info.fIniDir = (char *) 0;
               #if ROOT_VERSION_CODE >= ROOT_VERSION(3,1,6)
                  info.fFileTypes = const_cast<const char**>(gSaveAsTypes);
               #else
                  info.fFileTypes = const_cast<char**>(gSaveAsTypes);
               #endif
	       #if 1
		  info.fFileTypeIdx = 0 ; // Point at .txt
		  {
		     // If the user presses cancel or doesn't choose
		     // a file, info.fFilename will be null
		     new TLGFileDialog(this, &info, kFDSave) ;
		  }
		  if (!info.fFilename)
	       #else
                  if (!TLGFileDialog (this, info, kFDSave, ".txt"))
	       #endif
		  {
                     return kTRUE;
                  }
                  if (!fTextEdit->SaveFile (info.fFilename, kFALSE)) {
                     TString error = "Unable to save file:\n";
                     error += info.fFilename;
                     new TGMsgBox (gClient->GetRoot(), this, "Error", 
                                  error, kMBIconStop, kMBOk);
                  }
                  else {
                     SetWindowName (fTitle + " - " + info.fFilename);
                  }
               #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
                  delete [] info.fFilename;
               #endif
                  break;
               }
            // print, cut, copy, paste, selectall, find, findagain, goto
            case TGTextEdit::kM_FILE_PRINT:
            case TGTextEdit::kM_EDIT_CUT:
            case TGTextEdit::kM_EDIT_COPY:
            case TGTextEdit::kM_EDIT_PASTE:
            case TGTextEdit::kM_EDIT_SELECTALL:
            case TGTextEdit::kM_SEARCH_FIND:
            case TGTextEdit::kM_SEARCH_FINDAGAIN:
            case TGTextEdit::kM_SEARCH_GOTO:
               {
                  return fTextEdit->ProcessMessage (msg, parm1, parm2);
                  break;
               }
            // Close
            case TGTextEdit::kM_FILE_CLOSE:
               {
                  CloseWindow();
                  break;
               }
         }
      }
      return kTRUE;
   }

}
