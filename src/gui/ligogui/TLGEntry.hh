/* version $Id: TLGEntry.hh 6319 2010-09-17 18:06:52Z james.batch@LIGO.ORG $ */
/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef _LIGO_TLGENTRY_H
#define _LIGO_TLGENTRY_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGEntry						*/
/*                                                         		*/
/* Module Description: Entry fields	 				*/
/*		       modified test entry and numeric field entry	*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 29Oct99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGEntry.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include <TGFrame.h>
#include <TGTextEntry.h>
#include <TGButton.h>
#include <TGComboBox.h>
#include <TGFileDialog.h>


#if defined(__SUNPRO_CC) && defined(G__DICTIONARY)
   namespace ligogui {}
   using namespace ligogui;
#endif

namespace ligogui {


/** @name TLGEntry
    This header exports entry fields for numerical entries, for
    color selection, for font selection, for fill and line style
    selection and for marker style selection. 

    A numerical entry field supports the following formats:
    \begin{verbatim}
    kNESInteger:        integer number
    kNESRealOne:        real number with one digit (no exponent)
    kNESRealTwo:        real number with two digits (no exponent)
    kNESRealThree:      real number with three digits (no exponent)
    kNESRealFour:       real number with four digits (no exponent)
    kNESReal:           arbitrary real number
    kNESDegree:         angle in degree:minutes:seconds format
    kNESMinSec:         time in minutes:seconds format
    kNESHourMin:        time in hour:minutes format
    kNESHourMinSec:     time in hour:minutes:seconds format
    kNESDayMYear:       date in day/month/year format
    kNESMDayYear:       date in month/day/year format
    kNESHex:            hex number
    \end{verbatim}

    Numerical entry fields support an attribute specifying:
    \begin{verbatim}
    kNEAAnyNumber:      any number is allowed
    kNEANonNegative:	only non-negative numbers are allowed
    kNEAPositive:	only positive numbers are allowed
    \end{verbatim}

    Numerical entry fields also support limits on the input values:
    \begin{verbatim}
    kNELNoLimits:       no limits
    kNELLimitMin:       lower limit only
    kNELLimitMax        upper limit only
    kNELLimitMinMax     both lower and upper limits
    \end{verbatim}

    A numerical control box additionally has two buttons to its right
    which allow to increase or decrease the value in the field.
    Numerical entry fields also support cursor up and down keys
    within the field to achieve the same effect. The step size can be 
    selected with the control and shift keys:
    \begin{verbatim}
    --                  small step (1 unit/factor of 3)
    shift               medium step (10 units/factor of 10)
    control             large step (100 units/factor of 30)
    shift-control       huge step (1000 units/factor of 100)
    \end{verbatim}
    The steps are either linear or logarithmic. The default behaviour
    is set when the entry field is created, but it can be changed by 
    pressing the alt key at the same time.

    A numerical entry field can generate the following messages
    \begin{verbatim}
    kC_TEXTENTRY, kTE_TEXTCHANGED, widget id, 0
    kC_TEXTENTRY, kTE_TEXTENTER,   widget id, 0
    kC_TEXTENTRY, kTE_TEXTUPDATED, widget id, 0
    \end{verbatim}
    A kTE_TEXTCHANGED message is generated every time the user makes
    a modification to the text in the field, whereas kTE_TEXTUPDATED
    is only sent when the user leaves the field or acknowlegdes the
    entry by pressing 'enter'. The kTE_TEXTENTER message is sent when
    the user presses 'enter'.

    @memo Numeric entry fields
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{
/** @name Constants and enumerated types
    Constants to control the appearance of a numeric input field.
   
    @memo Option type constants
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{
#ifndef _kTE_TEXTUPDATED
#define _kTE_TEXTUPDATED
   /// Submessage identification number for an updated text entry field
   const int kTE_TEXTUPDATED = 3;
#endif

   /// Style of numeric entry field
   enum ENumericEntryStyle {
   /// Integer
   kNESInteger = 0,
   /// Fixed fraction real, one digit
   kNESRealOne = 1,
   /// Fixed fraction real, two digit
   kNESRealTwo = 2,
   /// Fixed fraction real, three digit
   kNESRealThree = 3,
   /// Fixed fraction real, four digit
   kNESRealFour = 4,
   /// Real number
   kNESReal = 5,
   /// Degree
   kNESDegree = 6,
   /// Minute:seconds
   kNESMinSec = 7,
   /// Hour:minutes
   kNESHourMin = 8,
   /// Hour:minute:seconds
   kNESHourMinSec = 9,
   /// Day/month/year
   kNESDayMYear = 10,
   /// Month/day/year
   kNESMDayYear = 11,
   /// Hex
   kNESHex = 12
   };

   /// Attributes of numeric entry field
   enum ENumericEntryAttributes {
   /// Any number
   kNEAAnyNumber = 0,
   /// Non-negative number
   kNEANonNegative = 1,
   /// Positive number
   kNEAPositive = 2
   };


   /// Limit selection of numeric entry field
   enum ENumericEntryLimits {
   /// No limits
   kNELNoLimits = 0,
   /// Lower limit only
   kNELLimitMin = 1,
   /// Upper limit only
   kNELLimitMax = 2,
   /// Both lower and upper limits
   kNELLimitMinMax = 3
   };


   /// Step sizes for numeric entry field automatic increase
   enum ENumericEntryStepSize {
   /// Small step
   kNSSSmall = 0,
   /// Medium step
   kNSSMedium = 1,
   /// Large step
   kNSSLarge = 2,
   /// Huge step
   kNSSHuge = 3
   };

//@}



/** A text entry field which adds the kTE_TEXTUPDATED message.
    (Also corrects ROOT bugs when right adjusting the text.)
   
    @memo Modified text entry field
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGTextEntry : public TGTextEntry {
   
   protected:
      /// value at begin of edit
      TString		fOldText;
      /// Update offset
      virtual void UpdateOffset ();
      /// Set cursor position
      virtual void SetCursorPosition (Int_t newPos); 
   
   public:
      /// Create a text entry field
#if ROOT_VERSION_CODE > 197893
      TLGTextEntry(const TGWindow *p, TGTextBuffer *text, Int_t id = -1,
                  GContext_t norm = GetDefaultGC()(),
                  FontStruct_t font = GetDefaultFontStruct(),
                  UInt_t option = kSunkenFrame | kDoubleBorder,
                  ULong_t back = GetWhitePixel());
#else
      TLGTextEntry(const TGWindow *p, TGTextBuffer *text, Int_t id = -1,
                  GContext_t norm = fgDefaultGC(),
                  FontStruct_t font = fgDefaultFontStruct,
                  UInt_t option = kSunkenFrame | kDoubleBorder,
                  ULong_t back = fgWhitePixel);
#endif
      /// Create a text entry field
      TLGTextEntry (const TGWindow *parent, const char *text,  
                   Int_t id = -1)
      : TGTextEntry (parent, text, id), fOldText (text) {
      }
      /// Create a text entry field
      TLGTextEntry (const TString &contents, const TGWindow *parent,  
                   Int_t id = -1)
      : TGTextEntry (contents, parent, id) {
      }
      /// Handle focus change
      virtual Bool_t HandleFocusChange (Event_t* event);
      /// Called when return is pressed
      virtual void ReturnPressed ();
      /// Called to send a test update message
      virtual void TextUpdated ();
      /// Layout
      virtual void Layout();
      /// Set text
// #if ROOT_VERSION_CODE < ROOT_VERSION(5,22,0)
#if ROOT_VERSION_CODE < 333312
      virtual void SetText (const char* text);
      virtual void SetText (const char *text, Bool_t emit) ; // JCB
#else
      virtual void SetText (const char* text, bool emit=true);
#endif
      /// Set state
      virtual void SetState (Bool_t state);
      /// Set maximum length
      virtual void SetMaxLength (Int_t maxlen);
   };


/** A numeric entry field. Supportes diverse formats, limits and 
    automatic number increase and decrease.
   
    @memo Numeric entry field
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGNumericEntry : public TLGTextEntry {
   
   protected:
      /// Numeric style
      ENumericEntryStyle 	fNumStyle;
      /// Numeric attribute
      ENumericEntryAttributes 	fNumAttr;
      /// Limit attributes
      ENumericEntryLimits 	fNumLimits;
      /// Lower limit
      Double_t			fNumMin;
      /// Upper limit
      Double_t			fNumMax;
      /// Logarithmic steps for automatic increase?
      Bool_t			fStepLog;
   
   public:
      /** Constructs a numeric entry field.
          @memo Constructor.
          @param p Parent window
   	  @param id Widget id
   	  @param val Initial value
   	  @param norm Graphics context
   	  @param option Widget option
   	  @param back Background color
       ******************************************************************/
#if ROOT_VERSION_CODE > 197893
      TLGNumericEntry (const TGWindow *p, Int_t id = -1, 
                      Double_t val = 0, 
		      GContext_t norm = GetDefaultGC()(),
                      FontStruct_t font = GetDefaultFontStruct(),
                      UInt_t option = kSunkenFrame | kDoubleBorder,
                      ULong_t back = GetWhitePixel());
#else
      TLGNumericEntry (const TGWindow *p, Int_t id = -1, 
                      Double_t val = 0, GContext_t norm = fgDefaultGC(),
                      FontStruct_t font = fgDefaultFontStruct,
                      UInt_t option = kSunkenFrame | kDoubleBorder,
                      ULong_t back = fgWhitePixel);
#endif
      /** Constructs a numeric entry field.
          @memo Constructor.
          @param p Parent window
   	  @param val Initial value
   	  @param id Widget id
   	  @param style Numeric style
   	  @param attr Numeric attribute
   	  @param limits Numeric limits attribute
   	  @param min Lower limit
   	  @param max Upper limit
       ******************************************************************/
      TLGNumericEntry (const TGWindow *parent, Double_t val,  
                      Int_t id = -1, ENumericEntryStyle style = kNESReal,
                      ENumericEntryAttributes attr = kNEAAnyNumber, 
                      ENumericEntryLimits limits = kNELNoLimits, 
                      Double_t min = 0, Double_t max = 1);
   
      /** Set the numeric value.
          @memo Set the value.
          @param val Value
          @return void
       ******************************************************************/
      virtual void SetNumber (Double_t val);
      /** Set the numeric value (integer format).
          @memo Set the value.
          @param val Value
          @return void
       ******************************************************************/
      virtual void SetIntNumber (Long_t val);
      /** Set the time value.
          @memo Set the time.
          @param hour Hour
          @param min Minute
          @param sec Second
          @return void
       ******************************************************************/
      virtual void SetTime (Int_t hour, Int_t min, Int_t sec);
      /** Set the date value.
          @memo Set the date.
          @param year Year
          @param month Month
          @param day Day
          @return void
       ******************************************************************/
      virtual void SetDate (Int_t year, Int_t month, Int_t day);
      /** Set the hex value.
          @memo Set the hex value.
          @param val Value
          @return void
       ******************************************************************/
      virtual void SetHexNumber (ULong_t val) {
         SetIntNumber ((Long_t) val); }
      /** Set the value (text).
          @memo Set the value.
          @param text Text value
          @return void
       ******************************************************************/
// Additional versions of Set* to control signals. JCB
      virtual void SetNumber(Double_t val, Bool_t emit) ;
      virtual void SetIntNumber(Long_t val, Bool_t emit) ;
      virtual void SetTime(Int_t hour, Int_t min, Int_t sec, Bool_t emit) ;
      virtual void SetDate(Int_t year, Int_t month, Int_t day, Bool_t emit) ;
      virtual void SetHexNumber(ULong_t val, Bool_t emit) 
	 {SetIntNumber((Long_t) val, emit); }
// #if ROOT_VERSION_CODE < ROOT_VERSION(5,22,0)
#if ROOT_VERSION_CODE < 333312
      virtual void SetText (const char* text);
      virtual void SetText (const char* text, Bool_t emit) ; // JCB
#else
      virtual void SetText (const char* text, bool emit=true);
#endif

      /** Get the numeric value.
          @memo Get the value.
          @param val Hex value
          @return void
       ******************************************************************/
      virtual Double_t GetNumber() const;
      /** Get the numeric value (integer format).
          @memo Get the value.
          @return Value
       ******************************************************************/
      virtual Long_t GetIntNumber() const;
      /** Get the time value.
          @memo Get the time.
          @param hour Hour
          @param min Minute
          @param sec Second
          @return void
       ******************************************************************/
      virtual void GetTime (Int_t& hour, Int_t& min, Int_t& sec) const;
      /** Get the date value.
          @memo Get the date.
          @param year Year
          @param month Month
          @param day Day
          @return void
       ******************************************************************/
      virtual void GetDate (Int_t& year, Int_t& month, Int_t& day) const;
      /** Get the hex value.
          @memo Get the hex value.
          @return Hex value
       ******************************************************************/
      virtual ULong_t GetHexNumber() const {
         return (ULong_t) GetIntNumber(); }
      /// Get the text width in pixels
      virtual Int_t GetCharWidth (const char* text = "0") const; 
   
      /** Increase the number value.
          @memo Increase the number value.
          @param step Type of step (small, medium, large, huge)
          @param sign Sign of step (>0 add, <0 subtract, 0: unchanged)
          @param logstep Make a log step if true
          @return void
       ******************************************************************/
      virtual void IncreaseNumber (ENumericEntryStepSize step = kNSSSmall,
                        Int_t sign = 1, Bool_t logstep = kFALSE);
   
      /** Set the numeric format.
          @memo Set the numeric format.
          @param style Numeric style
          @param attr Numeric attribute
          @return void
       ******************************************************************/
      virtual void SetFormat (ENumericEntryStyle style,
                        ENumericEntryAttributes attr = kNEAAnyNumber);
      /** Set the numeric limits.
          @memo Set the numeric limits.
   	  @param limits Numeric limits attribute
   	  @param min Lower limit
   	  @param max Upper limit
          @return void
       ******************************************************************/
      virtual void SetLimits (ENumericEntryLimits limits = kNELNoLimits,
                        Double_t min = 0, Double_t max = 1);
      /// Set logarithmic steps
      virtual void SetLogStep (Bool_t on = kTRUE) {
         fStepLog = on; }
      /// Get the numeric style
      virtual ENumericEntryStyle GetNumStyle() const {
         return fNumStyle; } 
      /// Get the numeric attributes
      virtual ENumericEntryAttributes GetNumAttr() const {
         return fNumAttr; } 
      /// Get the numeric limits attribute
      virtual ENumericEntryLimits GetNumLimits() const {
         return fNumLimits; }
      /// Get the lower limit
      virtual Double_t GetNumMin() const {
         return fNumMin; } 
      /// Get the upper limit
      virtual Double_t GetNumMax() const {
         return fNumMax; } 
   
      /// Is log step enabled?
      virtual Bool_t IsLogStep() const {
         return fStepLog; }
   
      /// Text update
      virtual void TextUpdated();
      /// Handle keys
      virtual Bool_t HandleKey (Event_t* event);
   };



/** A numeric control box. Implements a numeric entry field together
    with two small buttons for increasing or decreasing the value.
   
    @memo Numeric control box
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGNumericControlBox : public TGCompositeFrame, public TGWidget {
      /// Layout manager is a friend
      friend class TLGNumericControlBoxLayout;
   
   private:
      // Up arrow
      const TGPicture*	fPicUp;
      // Down arrow
      const TGPicture*	fPicDown;
   
   protected:
      /// Numeric text entry field
      TLGNumericEntry*	fNumericEntry;
      /// Button for increasing value
      TGButton*		fButtonUp;
      /// Button for decreasing value
      TGButton*		fButtonDown;
      /// Send button messages to parent or numeric entry (true)
      Bool_t 		fButtonToNum;
   
   public:
      /** Constructs a numeric control box.
          @memo Constructor.
          @param p Parent window
   	  @param val Initial value
   	  @param id Widget id
   	  @param style Numeric style
   	  @param attr Numeric attribute
   	  @param limits Numeric limits attribute
   	  @param min Lower limit
   	  @param max Upper limit
       ******************************************************************/
      TLGNumericControlBox (const TGWindow *parent, Double_t val = 0,  
                        Int_t digitwidth = 5, Int_t id = -1, 
                        ENumericEntryStyle style = kNESReal,
                        ENumericEntryAttributes attr = kNEAAnyNumber, 
                        ENumericEntryLimits limits = kNELNoLimits, 
                        Double_t min = 0, Double_t max = 1);
      /** Destructs the numeric control box
          @memo Destructor.
          @return void
       ******************************************************************/
      virtual ~TLGNumericControlBox();
   
      /** Set the numeric value.
          @memo Set the value.
          @param val Value
          @return void
       ******************************************************************/
      virtual void SetNumber (Double_t val) {
         fNumericEntry->SetNumber (val); }
      /** Set the numeric value (integer format).
          @memo Set the value.
          @param val Value
          @return void
       ******************************************************************/
      virtual void SetIntNumber (Long_t val) {
         fNumericEntry->SetIntNumber (val); }
      /** Set the time value.
          @memo Set the time.
          @param hour Hour
          @param min Minute
          @param sec Second
          @return void
       ******************************************************************/
      virtual void SetTime (Int_t hour, Int_t min, Int_t sec) {
         fNumericEntry->SetTime (hour, min, sec); }
      /** Set the date value.
          @memo Set the date.
          @param year Year
          @param month Month
          @param day Day
          @return void
       ******************************************************************/
      virtual void SetDate (Int_t year, Int_t month, Int_t day) {
         fNumericEntry->SetDate (year, month, day); }
      /** Set the hex value.
          @memo Set the hex value.
          @param val Value
          @return void
       ******************************************************************/
      virtual void SetHexNumber (ULong_t val) {
         fNumericEntry->SetHexNumber (val); }
      /** Set the value (text).
          @memo Set the value.
          @param text Text value
          @return void
       ******************************************************************/
      virtual void SetText (const char* text) {
         fNumericEntry->SetText (text); }
      /** Set the field state (enable/disable).
          @memo enable True if enabling.
          @return void
       ******************************************************************/
      virtual void SetState (Bool_t enable = kTRUE);
   
      /** Get the numeric value.
          @memo Get the value.
          @param val Hex value
          @return void
       ******************************************************************/
      virtual Double_t GetNumber () const {
         return fNumericEntry->GetNumber (); }
      /** Get the numeric value (integer format).
          @memo Get the value.
          @return Value
       ******************************************************************/
      virtual Long_t GetIntNumber () const {
         return fNumericEntry->GetIntNumber (); }
      /** Get the time value.
          @memo Get the time.
          @param hour Hour
          @param min Minute
          @param sec Second
          @return void
       ******************************************************************/
      virtual void GetTime (Int_t& hour, Int_t& min, Int_t& sec) const {
         fNumericEntry->GetTime (hour, min, sec); }
      /** Get the date value.
          @memo Get the date.
          @param year Year
          @param month Month
          @param day Day
          @return void
       ******************************************************************/
      virtual void GetDate (Int_t& year, Int_t& month, Int_t& day) const {
         fNumericEntry->GetDate (year, month, day); }
      /** Get the hex value.
          @memo Get the hex value.
          @return Hex value
       ******************************************************************/
      virtual ULong_t GetHexNumber () const {
         return fNumericEntry->GetHexNumber (); }
   
      /** Increase the number value.
          @memo Increase the number value.
          @param step Type of step (small, medium, large, huge)
          @param sign Sign of step (>0 add, <0 subtract, 0: unchanged)
          @param logstep Make a log step if true
          @return void
       ******************************************************************/
      virtual void IncreaseNumber (ENumericEntryStepSize step = kNSSSmall,
                        Int_t sign = 1, Bool_t logstep = kFALSE) {
         fNumericEntry->IncreaseNumber (step, sign, logstep); }
   
      /** Set the numeric format.
          @memo Set the numeric format.
          @param style Numeric style
          @param attr Numeric attribute
          @return void
       ******************************************************************/
      virtual void SetFormat (ENumericEntryStyle style,
                        ENumericEntryAttributes attr = kNEAAnyNumber) {
         fNumericEntry->SetFormat (style, attr); }
      /** Set the numeric limits.
          @memo Set the numeric limits.
   	  @param limits Numeric limits attribute
   	  @param min Lower limit
   	  @param max Upper limit
          @return void
       ******************************************************************/
      virtual void SetLimits (ENumericEntryLimits limits = kNELNoLimits,
                        Double_t min = 0, Double_t max = 1) {
         fNumericEntry->SetLimits (limits, min, max); }
      /// Set logarithmic steps
      virtual void SetLogStep (Bool_t on = kTRUE);
   
      /// Get the numeric style
      virtual ENumericEntryStyle GetNumStyle() const {
         return fNumericEntry->GetNumStyle(); } 
      /// Get the numeric attributes
      virtual ENumericEntryAttributes GetNumAttr() const {
         return fNumericEntry->GetNumAttr(); } 
      /// Get the numeric limits attribute
      virtual ENumericEntryLimits GetNumLimits() const {
         return fNumericEntry->GetNumLimits(); } 
      /// Get the lower limit
      virtual Double_t GetNumMin() const {
         return fNumericEntry->GetNumMin(); } 
      /// Get the upper limit
      virtual Double_t GetNumMax() const {
         return fNumericEntry->GetNumMax(); } 
      /// Is log step enabled?
      virtual Bool_t IsLogStep () const {
         return fNumericEntry->IsLogStep (); }
      /** Send button messages to numeric field or parent.
          The following message is sent:
      	  \begin{verbatim}
          kC_COMMAND, kCM_BUTTON, widget id, param
    	  param % 100 is the step size
    	  param % 10000 / 100 != 0 indicates log step
          param / 10000 != 0 indicated button down
          \end{verbatim}
          
       */
      virtual void SetButtonToNum (Bool_t state) {
         fButtonToNum = state; }
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };



/** Line style selection combobox.
   
    @memo Line style selection combobox
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGLineStyleComboBox : public TGComboBox {
   public:
      /// Create line style selection combobox
      TLGLineStyleComboBox (TGWindow* p, Int_t id);
   };



/** Marker style selection combobox.
   
    @memo Marker style selection combobox
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGMarkerStyleComboBox : public TGComboBox {
   public:
      /// Create marker style selection combobox
      TLGMarkerStyleComboBox (TGWindow* p, Int_t id);
   };



/** Fill style selection combobox.
   
    @memo Fill style selection combobox
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGFillStyleComboBox : public TGComboBox {
   public:
      /// Create fill style selection combobox
      TLGFillStyleComboBox (TGWindow* p, Int_t id);
   };



/** Font selection combobox. By default it will display twi comboboxes
    for selecting the font name and the font weight. Optionally, a
    numeric control box can be added to select the font height.
   
    @memo Font selection combobox
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGFontSelection : public TGHorizontalFrame, public TGWidget {
   protected:
      /// Use font size?
      Bool_t		fUseSize;
      /// Font name combobox
      TGComboBox*	fFontName;
      /// Font weight combobox
      TGComboBox*	fFontWeight;
      /// Font size numeric control entry
      TLGNumericControlBox* fFontSize;
      /// Layout hints
      TGLayoutHints*	fL1;
      /// Layout hints
      TGLayoutHints*	fL2;
   
   public:
      /// Create font selection combobox
      TLGFontSelection (TGWindow* p, Int_t id, 
                       Bool_t size = kFALSE);
   
      /// Destroy the font selection combobox
      virtual ~TLGFontSelection ();
      /// Set the selected font
      virtual void SetFont (Font_t font);
      /// Get the selected font
      virtual Font_t GetFont () const;
      /// Set the font size
      virtual void SetFontSize (Float_t size) {
         if (fUseSize) fFontSize->SetNumber (size); }
      /// Gets the selected font size
      virtual Float_t GetFontSize () const {
         return fUseSize ? fFontSize->GetNumber() : 0.04; }
      /// Relays messages from the comboboxes and the numeric entry
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };


#if 1
/* Implemented as a class to overload ProcessMessage() which has
 * and error in all root versions at least prior to 5.27
 */
/** SaveAs/Open file dialog box routine. This routine opens a file
    dialog box and returns a selected filename. This routine will
    ask for overwrite permission if the file exists. It will also
    keep track of pathnames and reads the fIniDir field from the
    file info structure. This routine will return a filename
    which has to be deleted by the user upon success.
   
    @memo File dialog routine
    @author Written November 1999 by Daniel Sigg
    @version 1.0
    @param main Parent window
    @param info File info strcuture
    @param dlg_type Open/SaveAs dialog
    @param defext Default file extension
    @param ask If true and file exists, asks for overwrire permission
    @return kTRUE upon success, kFALSE otherwise
 ************************************************************************/
   class TLGFileDialog : public TGTransientFrame {

   protected:
      TGTextBuffer      *fTbfname;  // text buffer of file name
      TGTextEntry       *fName;     // file name text entry
      TGComboBox        *fTypes;    // file type combo box
      TGFSComboBox      *fTreeLB;   // file system path combo box
      TGPictureButton   *fCdup;     // top toolbar button
      TGPictureButton   *fNewf;     // top toolbar button
      TGPictureButton   *fList;     // top toolbar button
      TGPictureButton   *fDetails;  // top toolbar button
      TGCheckButton     *fCheckB;   // set on/off file overwriting for Open dialog
				    // OR set on/off multiple file selection for SaveAs dialog
      const TGPicture   *fPcdup;    // picture for fCdup
      const TGPicture   *fPnewf;    // picture for fNewf
      const TGPicture   *fPlist;    // picture for fList
      const TGPicture   *fPdetails; // picture for fDetails
      TGTextButton      *fOk;       // ok button
      TGTextButton      *fCancel;   // cancel button
      TGListView        *fFv;       // file list view
      TGFileContainer   *fFc;       // file list view container (containing the files)
      TGFileInfo        *fFileInfo; // file info passed to this dialog

   private:
      TLGFileDialog(const TGFileDialog&);              // not implemented
      TLGFileDialog& operator=(const TGFileDialog&);   // not implemented

   public:
      // Constructor
      TLGFileDialog(const TGWindow *main, TGFileInfo *info,
			EFileDialogMode dlg_type) ;
      /*TLGFileDialog(const TGWindow *main, TGFileInfo &info,
			EFileDialogMode dlg_type, const char *defext = 0,
			Bool_t ask = kTRUE) ;
      */

      virtual ~TLGFileDialog() ;
      virtual Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2) ;
      virtual void CloseWindow() ;
   } ;

#else
/** SaveAs/Open file dialog box routine. This routine opens a file
    dialog box and returns a selected filename. This routine will
    ask for overwrite permission if the file exists. It will also
    keep track of pathnames and reads the fIniDir field from the
    file info structure. This routine will return a filename
    which has to be deleted by the user upon success.
   
    @memo File dialog routine
    @author Written November 1999 by Daniel Sigg
    @version 1.0
    @param main Parent window
    @param info File info strcuture
    @param dlg_type Open/SaveAs dialog
    @param defext Default file extension
    @param ask If true and file exists, asks for overwrire permission
    @return kTRUE upon success, kFALSE otherwise
 ************************************************************************/
   Bool_t TLGFileDialog (const TGWindow* main, TGFileInfo& info, 
                     EFileDialogMode dlg_type, const char* defext = 0,
                     Bool_t ask = kTRUE);
#endif

//@}
}

#endif
