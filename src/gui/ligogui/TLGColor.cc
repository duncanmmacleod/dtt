/* -*- mode: c++; c-basic-offset: 3; -*- */
#include "TLGColor.hh"
#include "TLGEntry.hh"
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <TGMsgBox.h>
#include <TColor.h>
#include <TROOT.h>
#if ROOT_VERSION_CODE >= ROOT_VERSION(3,4,2)
#include <TGColorDialog.h>
#include <TVirtualX.h>
#endif

namespace ligogui {
   using namespace std;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// gPlotColorLookup                                                     //
//                                                                      //
// Global color table   				                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TPlotColorLookup*	gColorTable = 0;

//______________________________________________________________________________
   TPlotColorLookup& gPlotColorLookup()
   {
      if (!gColorTable) {
         gColorTable = new TPlotColorLookup();
      }
      return *gColorTable;
   }

//______________________________________________________________________________
   const int kNumColors = 13;

//______________________________________________________________________________
   const float kColorTable[kNumColors][3] = {
   { 1.000, 0.698, 0.102 }, // Orange
   { 0.804, 0.030, 0.954 }, // Purple
   { 0.000, 0.450, 0.000 }, // Dark Green
   { 0.000, 0.894, 0.561 }, // Medium Green
   { 0.690, 0.718, 0.682 }, // Slate
   { 0.820, 0.431, 0.000 }, // Cedar
   { 0.737, 0.702, 0.220 }, // Gold
   { 0.686, 0.004, 0.945 }, // Indigo
   { 0.259, 0.373, 0.584 }, // Navy
   { 0.027, 0.937, 0.686 }, // Sea Green
   { 0.835, 0.000, 0.000 }, // Crimson
   { 0.443, 0.000, 0.443 }, // Plum
   { 0.976, 0.125, 0.612 }  // Pink
   };

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TPlotColorLookup                                                     //
//                                                                      //
// Lookup tabel for standard plot colors		                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TPlotColorLookup::ColorType::ColorType (Int_t index)
   : fIndex (0), fRGB (0)
   {
      SetColor (index);
   }

//______________________________________________________________________________
   TPlotColorLookup::ColorType::ColorType (Int_t r, Int_t g, Int_t b)
   : fIndex (0), fRGB (0)
   {
      SetRGB (r, g, b);
   }

//______________________________________________________________________________
   void TPlotColorLookup::ColorType::SetColor (Int_t col)
   {
      if (col <= 0) {
         SetRGB (-col);
         return;
      }
      TColor* color = gROOT->GetColor (col);
      if (!color) {
         SetRGB (0); // black
         return;
      }
      fIndex = col;
      fRGB = 256*256*(int)(255.*color->GetRed()) + 
         256*(int)(255.*color->GetGreen()) + 
         (int)(255.*color->GetBlue());
   }

//______________________________________________________________________________
   void TPlotColorLookup::ColorType::SetRGB (Int_t r, Int_t g, Int_t b)
   {
      fRGB = 256*256*r + 256*g + b;
      fIndex = TColor::GetColor (r, g, b);
   }

//______________________________________________________________________________
   void TPlotColorLookup::ColorType::SetRGB (Int_t rgb)
   {
      int r = rgb / (256*256) % 256;
      int g = rgb / 256 % 256;
      int b = rgb % 256;
      SetRGB (r, g, b);
   }

//______________________________________________________________________________
   Bool_t TPlotColorLookup::ColorType::GuiColor (ULong_t& color) const
   {         
      int r = fRGB / (256*256) % 256;
      int g = fRGB / 256 % 256;
      int b = fRGB % 256;
      char cname[16];
      sprintf (cname, "#%02x%02x%02x", r, g, b);
      return gClient->GetColorByName (cname, color);
   }

//______________________________________________________________________________
   Int_t TPlotColorLookup::ColorType::Compatibility() const
   {
      if (fIndex < 30) {
         return fIndex;
      }
      else {
         return -fRGB;
      }
   }

//______________________________________________________________________________
   TPlotColorLookup::TPlotColorLookup ()
   : fVersion (1) {
      fCTable.resize (8 + kNumColors);
      fCTable[0] = ColorType (1);
      fCTable[1] = ColorType (2);
      fCTable[2] = ColorType (4);
      fCTable[3] = ColorType (3);
      fCTable[4] = ColorType (28);
      fCTable[5] = ColorType (6);
      fCTable[6] = ColorType (7);
      fCTable[7] = ColorType (5);
      for (int i = 0; i < kNumColors; ++i) {
         fCTable[8+i] = ColorType ((int)(255*kColorTable[i][0]),
                              (int)(255*kColorTable[i][1]),
                              (int)(255*kColorTable[i][2]));
      }
   }

//______________________________________________________________________________
   TPlotColorLookup::ColorType& TPlotColorLookup::operator[] (
                     Int_t index) 
   {
      if ((index < 0) || (index >= (int)fCTable.size())) index = 0;
      return fCTable[index];
   }

//______________________________________________________________________________
   const TPlotColorLookup::ColorType& TPlotColorLookup::operator[] (
                     Int_t index) const 
   {
      if ((index < 0) || (index >= (int)fCTable.size())) index = 0;
      return fCTable[index];
   }

//______________________________________________________________________________
   int TPlotColorLookup::Add (Int_t col)
   {
      ColorType ct (col);
      for (int i = 0; i < (int)fCTable.size(); ++i) {
         if (fCTable[i] == ct) {
            return ct.Index();
         }
      }
      fCTable.push_back (ct);
      ++fVersion;
      return ct.Index();
   }

//______________________________________________________________________________
   int TPlotColorLookup::Remove (Int_t col)
   {
      ColorType ct (col);
      for (int i = 8 + kNumColors; i < (int)fCTable.size(); ++i) {
         if (fCTable[i] == ct) {
            fCTable.erase (fCTable.begin() + i);
            ++fVersion;
            return ct.Index();
         }
      }
      return -1;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGColorLBEntry                                                      //
//                                                                      //
// Listbox entry for color selection      		                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGColorLBEntry::TLGColorLBEntry (TGWindow* p, Int_t id) 
   : TGTextLBEntry (p, new TGString (id != 1000000000 ? "    " : ">"), id) 
   {
      SetColor (id);
   }

//______________________________________________________________________________
   Bool_t TLGColorLBEntry::SetColor (Int_t cid) 
   {
      fColIndex = cid;
      if (cid != 1000000000) {
         TPlotColorLookup::ColorType ct (cid);
         fEntryId = ct.Index();
         return ct.GuiColor (fColor);
      }
      else {
//         fColor = (ULong_t)-1;
	 fColor = 0xffffff ;
         return kTRUE;
      }
   }

//______________________________________________________________________________
   void TLGColorLBEntry::Update (TGLBEntry* entry) 
   {
      SetColor (((TLGColorLBEntry*)entry)->GetColor());
      TGTextLBEntry::Update (entry);
   }

//______________________________________________________________________________
   void TLGColorLBEntry::DoRedraw() 
   {
      // The color entry is a hex value, 0xrrggbb.
      // Since 0xffffff is not (ULong_t) -1, use 0xffffff.
//      if (fColor != (ULong_t)-1) {
      if (fColor != 0xffffff) {
         SetBackgroundColor (fColor);
	 gVirtualX->ClearWindow (fId);
      }
      else {
	 // Change this to SetBackgroundColor(0xffffff), 
	 // because moving the mouse over the ">" entry
	 // was turning the background black.
//         SetBackgroundColor (0);
	 SetBackgroundColor(0xffffff) ;
	 gVirtualX->ClearWindow (fId);
	 TGTextLBEntry::DoRedraw();
      }
   }

//______________________________________________________________________________
// Intercept the Activate() call to always be false.  This prevents the
// mouse from changing the colors to the Gui.Select*Color from system.rootrc,
// which obliterates the color you might want to choose. 
// This change causes no action to be taken when the mouse moves over a color
// entry in the TLGColorComboBox.
   void TLGColorLBEntry::Activate(Bool_t a)
   {
      TGLBEntry::Activate(kFALSE) ;
   }

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGColorComboBox                                                     //
//                                                                      //
// Combobox for color selection          		                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGColorComboBox::TLGColorComboBox (TGWindow* p, Int_t id, 
                     Bool_t allownew) 
   : TGComboBox (p, id) , fAllowNew (allownew), fVersion (0),
   fLastSel (1)
   {
      Resize (35, 20);
      Build();
      // Top entry
      TGTextLBEntry* entry = new TLGColorLBEntry (this, 1);
      SetTopEntry (entry, 
                  new TGLayoutHints (kLHintsExpandX | kLHintsTop));
      // Select black
      Select (1);
   }

//______________________________________________________________________________
#if ROOT_VERSION_CODE < ROOT_VERSION(5,12,0)
   void TLGColorComboBox::Select (Int_t id)
#else
   void TLGColorComboBox::Select (Int_t id, Bool_t emit)
#endif
   {
      if (fVersion != gPlotColorLookup().GetVersion()) {
         Build();
      }
      if (id == 1000000000) {
         TLGColorAllocDialog::DialogBox (this);
#if ROOT_VERSION_CODE < ROOT_VERSION(5,12,0)
         Select (fLastSel);
#else
         Select (fLastSel, emit);
#endif
      }
      else {
#if ROOT_VERSION_CODE < ROOT_VERSION(5,12,0)
	 TGComboBox::Select (id);
#else
	 TGComboBox::Select (id, emit);
#endif
         fLastSel = id;
      }
   }

//______________________________________________________________________________
   Bool_t TLGColorComboBox::HandleButton (Event_t* event)
   {
      if (fVersion != gPlotColorLookup().GetVersion()) {
         Build();
      }
      return TGComboBox::HandleButton (event);
   }

//______________________________________________________________________________
   Bool_t TLGColorComboBox::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      // intercept new message
      if ((GET_MSG (msg) == kC_COMMAND) && 
         (GET_SUBMSG (msg) == kCM_LISTBOX) &&
         (parm2 == 1000000000)) {
         fComboFrame->EndPopup();
         TLGColorAllocDialog::DialogBox (this);
         Select (fLastSel);
         return kTRUE;
      }
      else {
         if ((GET_MSG (msg) == kC_COMMAND) && 
            (GET_SUBMSG (msg) == kCM_LISTBOX)) {
            fLastSel = parm2;
         }
         return TGComboBox::ProcessMessage (msg, parm1, parm2);
      }
   }

//______________________________________________________________________________
   void TLGColorComboBox::Build()
   {
      RemoveEntries (0, 1000000001);
      // New color entry
      TGTextLBEntry* entry;
      if (fAllowNew) {
         entry = new TLGColorLBEntry(GetListBox()->GetContainer(), 1000000000);
         AddEntry(entry, new TGLayoutHints(kLHintsExpandX | kLHintsTop));
      }
      // Color list
      for (int j = 0; j < gPlotColorLookup().Size(); j++) {
         entry = new TLGColorLBEntry (GetListBox()->GetContainer(), 
				      gPlotColorLookup()[j].Index());
         AddEntry(entry, new TGLayoutHints (kLHintsExpandX | kLHintsTop));
	 entry->Update(entry);
      }
      fVersion = gPlotColorLookup().GetVersion();
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGColorAllocDialog                                                  //
//                                                                      //
// Dialog box for color allocation          		                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGColorAllocDialog* TLGColorAllocDialog::gColorDlg = 0;

//______________________________________________________________________________
   TLGColorAllocDialog::TLGColorAllocDialog (const TGWindow *p, 
                     const TGWindow *main)
   : TLGTransientFrame (p, main, 10, 10)
   {
      // layout hints
      fL[0] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsTop, 8, 8, 2, 2);
      // fL[0] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           // kLHintsTop, 2, 2, 2, 2);
      fL[1] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2);
      fL[2] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsCenterY, 2, 2, 2, 2);
      fL[3] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 6, 6, 12, 4);
      // frames
      fG[0] = new TGGroupFrame (this, "Color");
      AddFrame (fG[0], fL[0]);
      for (int i = 0; i < 3; i++) {
         fF[i] = new TGHorizontalFrame (fG[0], 10, 10);
         fG[0]->AddFrame (fF[i], fL[0]);
      }
      fF[3] = new TGHorizontalFrame (this, 10, 10);
      AddFrame (fF[3], fL[0]);
      // line 1
      fLabel[0] = new TGLabel (fF[0], "C:");
      fF[0]->AddFrame (fLabel[0], fL[1]);
      fColor = new TLGColorComboBox (fF[0], 11, kFALSE);
      fColor->Associate (this);
      fF[0]->AddFrame (fColor, fL[2]);
   
      // line 2
      fLabel[1] = new TGLabel (fF[1], "R:");
      fF[1]->AddFrame (fLabel[1], fL[1]);
      fNum[0] = new TLGNumericControlBox (fF[1], 0, 5, 21,
                           kNESInteger, kNEANonNegative, 
                           kNELLimitMax, 0, 255);
      fNum[0]->Associate (this);
      fF[1]->AddFrame (fNum[0], fL[2]);
      fLabel[2] = new TGLabel (fF[1], "G:");
      fF[1]->AddFrame (fLabel[2], fL[1]);
      fNum[1] = new TLGNumericControlBox (fF[1], 0, 5, 22,
                           kNESInteger, kNEANonNegative, 
                           kNELLimitMax, 0, 255);
      fNum[1]->Associate (this);
      fF[1]->AddFrame (fNum[1], fL[2]);
      fLabel[3] = new TGLabel (fF[1], "B:");
      fF[1]->AddFrame (fLabel[3], fL[1]);
      fNum[2] = new TLGNumericControlBox (fF[1], 0, 5, 23,
                           kNESInteger, kNEANonNegative, 
                           kNELLimitMax, 0, 255);
      fNum[2]->Associate (this);
      fF[1]->AddFrame (fNum[2], fL[2]);
   
      // line 3
      fLabel[4] = new TGLabel (fF[2], "H:");
      fF[2]->AddFrame (fLabel[4], fL[1]);
      fNum[3] = new TLGNumericControlBox (fF[2], 0, 5, 31,
                           kNESInteger, kNEANonNegative, 
                           kNELLimitMax, 0, 360);
      fNum[3]->Associate (this);
      fF[2]->AddFrame (fNum[3], fL[2]);
      fLabel[5] = new TGLabel (fF[2], "L:");
      fF[2]->AddFrame (fLabel[5], fL[1]);
      fNum[4] = new TLGNumericControlBox (fF[2], 0, 5, 32,
                           kNESRealTwo, kNEANonNegative, 
                           kNELLimitMax, 0.0, 1.);
      fNum[4]->Associate (this);
      fF[2]->AddFrame (fNum[4], fL[2]);
      fLabel[6] = new TGLabel (fF[2], "S:");
      fF[2]->AddFrame (fLabel[6], fL[1]);
      fNum[5] = new TLGNumericControlBox (fF[2], 0, 5, 33,
                           kNESRealTwo, kNEANonNegative,
                           kNELLimitMax, 0.0, 1.);
      fNum[5]->Associate (this);
      fF[2]->AddFrame (fNum[5], fL[2]);
   
      // buttons
      fButton[0] = new TGTextButton (fF[3],
                           new TGHotString ("       &Add       "), 1);
      fButton[0]->Associate (this);
      fF[3]->AddFrame (fButton[0], fL[3]);
      fButton[1] = new TGTextButton (fF[3], 
                           new TGHotString ("    &Remove    "), 2);
      fButton[1]->Associate (this);
      fF[3]->AddFrame (fButton[1], fL[3]);
   #if ROOT_VERSION_CODE >= ROOT_VERSION(3,4,2)
      if (gVirtualX->GetDepth() > 8) {
         fButton[2] = new TGTextButton (fF[3], 
                              new TGHotString ("     &Pick...     "), 3);
         fButton[2]->Associate (this);
         fF[3]->AddFrame (fButton[2], fL[3]);
      }
      else {
         fButton[2] = 0;
      }
   #else
      fButton[2] = 0;
   #endif
      fButton[3] = new TGTextButton (fF[3], 
                           new TGHotString ("      &Quit      "), 0);
      fButton[3]->Associate (this);
      fF[3]->AddFrame (fButton[3], fL[3]);
   
      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates 
            (main->GetId(), GetParent()->GetId(),
            (((TGFrame*)main)->GetWidth() - fWidth), 
            0*(((TGFrame*)main)->GetHeight() - fHeight),
            ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth);
         ay = (root_h - fHeight);
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
   
      // make the message box non-resizable
      SetWMSize(width, height);
      SetWMSizeHints(width, height, width, height, 0, 0);
   
      // set dialog box title
      SetWindowName ("Color Selection");
      SetIconName ("Color Selection");
      SetClassHints ("ColorAllocDlg", "ColorAllocDlg");
      SetMWMHints (kMWMDecorAll, kMWMFuncAll, kMWMInputModeless);
   
      MapWindow();
   }

//______________________________________________________________________________
   TLGColorAllocDialog::~TLGColorAllocDialog ()
   {
      for (int i = 0; i < 7; i++) {
         delete fLabel[i];
      }
      delete fColor;
      for (int i = 0; i < 6; i++) {
         delete fNum[i];
      }
      for (int i = 0; i < 4; i++) {
         delete fButton[i];
      }
      for (int i = 0; i < 4; i++) {
         delete fF[i];
      }
      delete fG[0];
      for (int i = 0; i < 4; i++) {
         delete fL[i];
      }
      gColorDlg = 0;
   }

//______________________________________________________________________________
   void TLGColorAllocDialog::CloseWindow()
   {
      DeleteWindow();
   }

//______________________________________________________________________________
   Bool_t TLGColorAllocDialog::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         switch (parm1) {
            // Add
            case 1:
               {
                  int r = fNum[0]->GetIntNumber();
                  int g = fNum[1]->GetIntNumber();
                  int b = fNum[2]->GetIntNumber();
                  if (gPlotColorLookup().Add (-(256*256*r+256*g+b)) < 0) {
                     Int_t ret;
                     new TGMsgBox (gClient->GetRoot(), this, 
                                  "Warning", "Unable to allocate color", 
                                  kMBIconExclamation, kMBOk, &ret);
                  }
                  break;
               }
            // Remove
            case 2:
               {
                  int r = fNum[0]->GetIntNumber();
                  int g = fNum[1]->GetIntNumber();
                  int b = fNum[2]->GetIntNumber();
                  int cindex = 
                     gPlotColorLookup().Remove (-(256*256*r+256*g+b));
                  if (cindex == fColor->GetSelected()) {
                     fColor->Select (1);
                  }
                  break;
               }
            // Quit
            case 0:
               {
                  DeleteWindow();
                  break;
               }
            // Pick
            case 3:
               {
               #if ROOT_VERSION_CODE >= ROOT_VERSION(3,4,2)
                  if (gVirtualX->GetDepth() <= 8) {
                     break;
                  }
                  Int_t ret;
                  TPlotColorLookup::ColorType c (fColor->GetSelected());
                  ULong_t color = 0;
                  c.GuiColor (color);
                  new TGColorDialog (gClient->GetRoot(), this, &ret, &color);
                  if (ret == kMBOk) {
                     // set the RGB & HLS values
                     Float_t r, g, b, h, l, s;
                     TColor::Pixel2RGB (color, r, g, b);
                     TColor::RGB2HLS (r, g, b, h, l, s);
                     fNum[0]->SetIntNumber (long(255.0 * r));
                     fNum[1]->SetIntNumber (long(255.0 * g));
                     fNum[2]->SetIntNumber (long(255.0 * b));
                     fNum[3]->SetIntNumber (long(h));
                     fNum[4]->SetNumber (l);
                     fNum[5]->SetNumber (s);
                  }
               #endif
                  break;
               }
         }
      }
      // Combobox
      else if ((GET_MSG (msg) == kC_COMMAND) &&
              (GET_SUBMSG (msg) == kCM_COMBOBOX)) {
         if (parm1 == 11) {
            // set the RGB & HLS values
            TColor* col = gROOT->GetColor (parm2);
            if (col) {
               fNum[0]->SetIntNumber (long(255.0 * col->GetRed()));
               fNum[1]->SetIntNumber (long(255.0 * col->GetGreen()));
               fNum[2]->SetIntNumber (long(255.0 * col->GetBlue()));
               fNum[3]->SetIntNumber (long(col->GetHue()));
               fNum[4]->SetNumber (col->GetLight());
               fNum[5]->SetNumber (col->GetSaturation());
            }
         }
      }
      // Numbers
      else if ((GET_MSG (msg) == kC_TEXTENTRY) &&
              (GET_SUBMSG (msg) == kTE_TEXTUPDATED)) {
      	 // RGB has changed
         if ((parm1 >= 20) && (parm1 < 30)) {
            int r = fNum[0]->GetIntNumber();
            int g = fNum[1]->GetIntNumber();
            int b = fNum[2]->GetIntNumber();
            float h, l, s;
            TColor::RGBtoHLS ((float)r/255.0, (float)g/255.0, 
                             (float)b/255.0, h, l, s);
            fNum[3]->SetIntNumber (long(h));
            fNum[4]->SetNumber (l);
            fNum[5]->SetNumber (s);
         }
         // HLS has changed
         else if ((parm1 >= 30) && (parm1 < 40)) {
            float h = fNum[3]->GetIntNumber();
            float l = fNum[4]->GetNumber();
            float s = fNum[5]->GetNumber();
            float r, g, b;
            TColor::HLStoRGB (h, l, s, r, g, b);
            fNum[0]->SetIntNumber (long(255.0 * r));
            fNum[1]->SetIntNumber (long(255.0 * g));
            fNum[2]->SetIntNumber (long(255.0 * b));
         }
      }
      return kTRUE;
   }

//______________________________________________________________________________
   void TLGColorAllocDialog::DialogBox (const TGWindow* p)
   {
      if (gColorDlg) {
         gColorDlg->RaiseWindow();
      }
      else {
         const TGWindow* main = p;
         while (main && (main != gClient->GetRoot()) &&
               (main->GetParent() != gClient->GetRoot())) {
            main = main->GetParent();
         }
         gColorDlg = new TLGColorAllocDialog (gClient->GetRoot(), main);
      
      }
   }



}
