#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;

#pragma link C++ class ligogui::TLGLBTreeEntry;
#pragma link C++ class ligogui::TLGLBTreeContainer;
#pragma link C++ class ligogui::TLGLBTree;
#pragma link C++ class ligogui::TLGTextLBEntry;
#pragma link C++ class ligogui::TLGComboTreePopup;
#pragma link C++ class ligogui::TLGComboTree;
#pragma link C++ class ligogui::ChannelEntry;
#pragma link C++ class ligogui::ChannelTree;
#pragma link C++ class ligogui::TLGChannelListbox;
#pragma link C++ class ligogui::TLGChannelCombobox;
#pragma link C++ enum ligogui::ENumericEntryStyle;
#pragma link C++ enum ligogui::ENumericEntryAttributes;
#pragma link C++ enum ligogui::ENumericEntryLimits;
#pragma link C++ enum ligogui::ENumericEntryStepSize;
#pragma link C++ class ligogui::TLGTextEntry;
#pragma link C++ class ligogui::TLGNumericEntry;
#pragma link C++ class ligogui::TLGNumericControlBox;
#pragma link C++ class ligogui::TPlotColorLookup;
#pragma link C++ class ligogui::TLGColorLBEntry;
#pragma link C++ class ligogui::TLGColorComboBox;
#pragma link C++ class ligogui::TLGLineStyleComboBox;
#pragma link C++ class ligogui::TLGMarkerStyleComboBox;
#pragma link C++ class ligogui::TLGFillStyleComboBox;
#pragma link C++ class ligogui::TLGFontSelection;
#pragma link C++ class ligogui::TLGMultiTabElement;
#pragma link C++ class ligogui::TLGMultiTabLayout;
#pragma link C++ class ligogui::TLGMultiTab;
#pragma link C++ class ligogui::TLGProgressBar;
#pragma link C++ class ligogui;

#endif

