/* Version $Id: TLGMultiTab.hh 6319 2010-09-17 18:06:52Z james.batch@LIGO.ORG $ */
#ifndef _LIGO_TLGMultiTab
#define _LIGO_TLGMultiTab

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGMultiTab, TLGMultiTabElement, TLGMultiTabLayout                   //
//                                                                      //
// A tab widget contains a set of composite frames each with a little   //
// tab with a name (like a set of folders with tabs). This multi tab    //
// support multi line tabs				                //
//                                                                      //
// The TLGMultiTab is user callable. The TLGMultiTabElement and 	//
// TLGMultiTabLayout are a service classes of the tab widget.           //
//                                                                      //
// Clicking on a tab will bring the associated composite frame to the   //
// front and generate the following event:                              //
// kC_COMMAND, kCM_TAB, tab id, 0.                                      //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include <TGFrame.h>
#include <TGTab.h>

namespace ligogui {

   class TLGMultiTab;

/** @name TLGMultiTab
    A tab widget contains a set of composite frames each with a little
    tab with a name (like a set of folders with tabs). This multi tab 
    widget supports tabs arranged on multiple lines. This is useful for
    tab widgets with many options which would otherwise run out of 
    space when displaying the tabs. The multi tab widget is a derived
    class of the standard tab widget.
   
    @memo Multi tab widget
    @author Written January 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


/** Multi tab element. For internal use only.
   
    @memo Multi tab element.
    @author Written January 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGMultiTabElement : public TGTabElement {
   
   protected:
      /// Width adjust
      Int_t		fWAdjust;
   
   public:
      /// Constructor
#if ROOT_VERSION_CODE > 197893
      TLGMultiTabElement (const TGWindow *p, TGString *text, 
                        UInt_t w, UInt_t h,
                        GContext_t norm, FontStruct_t font,
                        UInt_t options = kRaisedFrame,
                        ULong_t back = GetDefaultFrameBackground());
#else
      TLGMultiTabElement (const TGWindow *p, TGString *text, 
                        UInt_t w, UInt_t h,
                        GContext_t norm, FontStruct_t font,
                        UInt_t options = kRaisedFrame,
                        ULong_t back = fgDefaultFrameBackground);
#endif
      /// Destructor
      virtual ~TLGMultiTabElement();
   
      /// Get the preferred width of this element
      virtual Int_t GetPreferredWidth() const;
      /// Get the default size
      virtual TGDimension GetDefaultSize() const;
      /// Set the width adjustment
      virtual void SetWidthAdjust (Int_t adjust) {
         fWAdjust = adjust; }
   };


/** Multi tab layout manager. For internal use only.
   
    @memo Multi tab layout manager.
    @author Written January 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGMultiTabLayout : public TGLayoutManager {
   
   protected:
      /// Container frame
      TLGMultiTab*	fMain;
      /// List of frames to arrange
      TList*		fList;
   
   public:
      /// Constructor
      TLGMultiTabLayout (TLGMultiTab *main);
      /// Layout
      virtual void Layout ();
      /// Get the default size
      virtual TGDimension GetDefaultSize () const;
   };


/** Multi tab widget. Use this widget the same way you would use
    a standard tab widget. The only difference is that one has to
    specify the number of lines used for the tabs during the 
    construction. The behaviour of this widget is identical to the
    standard tab widget.
   
    @memo Multi tab layout manager.
    @author Written January 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGMultiTab : public TGTab {
      /// friend
      //friend class TGClient;

   protected:
      /// Number of lines used for tabs
      Int_t		fLines;

      /// Change to a different tab
      //  Since ChangeTab isn't virtual, we can use the post-5.14 
      //  signature for all root versions
      void ChangeTab (Int_t tabIndex, Bool_t emit = kTRUE);

   public:
      /** Constructs a multi tab widget.
          @memo Constructor.
          @param p Parent window
          @param w Width of widget
          @param h Height of widget
          @param tablines Number of lines used for tabs
          @param norm Default graphics context
          @param font Default font
          @param options Options
          @param back Default background
       ******************************************************************/
#if ROOT_VERSION_CODE > 197893
      TLGMultiTab (const TGWindow *p, UInt_t w, UInt_t h,
                  Int_t tablines, GContext_t norm = GetDefaultGC()(),
                  FontStruct_t font = GetDefaultFontStruct(),
                  UInt_t options = kChildFrame,
                  ULong_t back = GetDefaultFrameBackground());
#else
      TLGMultiTab (const TGWindow *p, UInt_t w, UInt_t h,
                  Int_t tablines, GContext_t norm = fgDefaultGC(), 
                  FontStruct_t font = fgDefaultFontStruct,
                  UInt_t options = kChildFrame,
                  ULong_t back = fgDefaultFrameBackground);
#endif
      /** Destructs a multi tab widget.
          @memo Destructor.
       ******************************************************************/
      virtual ~TLGMultiTab();
   
      /** Add a tab. The returned frame is owned by the widget.
          @memo Add a tab.
          @param text Text of tab
          @return Frame of new tab
       ******************************************************************/
      virtual TGCompositeFrame* AddTab (TGString* text);
      /** Add a tab. The returned frame is owned by the widget.
          @memo Add a tab.
          @param text Text of tab
          @return Frame of new tab
       ******************************************************************/
      virtual TGCompositeFrame* AddTab (const char* text);
   
// #if ROOT_VERSION_CODE >= ROOT_VERSION(5,18,0)
#if ROOT_VERSION_CODE >= 332288
      /** Add a tab. The returned frame is owned by the widget.
          @memo Add a tab.
          @param text Text of tab
          @return Frame of new tab
       ******************************************************************/
     virtual void AddTab (TGString* text, TGCompositeFrame* cf);

      /** Add a tab. The returned frame is owned by the widget.
          @memo Add a tab.
          @param text Text of tab
          @return Frame of new tab
       ******************************************************************/
     virtual void AddTab (const char* text, TGCompositeFrame* cf);
#endif

      /** Set the tab which is on top.
          @memo Set tab.
          @param tabIndex index of new tab
          @return true if successful
       ******************************************************************/
#if ROOT_VERSION_CODE < 331264
      using TGTab::SetTab;
      virtual Bool_t SetTab(Int_t tabIndex);
      //syntax changed with version 5.14-0 = 331264
#else
      virtual Bool_t SetTab(Int_t tabIndex, Bool_t emit = kTRUE);
      virtual Bool_t SetTab(const char* name, Bool_t emit = kTRUE);
#endif

// #if ROOT_VERSION_CODE < ROOT_VERSION(4,4,0)
#if ROOT_VERSION_CODE < 263168
      /** Handle button event.
          @memo Handle button event.
          @param event Event
          @return true if successful
       ******************************************************************/
      virtual Bool_t HandleButton(Event_t *event);
      //  The button handler was moved to TGTabElement in v4.04-0 where it 
      //  uses SetTab (virtualized by TLGMultiTabElement!)
#endif
   
      /** Get the number of tab lines.
          @memo Get tab lines.
          @return Number of lines
       ******************************************************************/
      Int_t GetTabLines () const {
         return fLines; }
      /** Get the width of a set of tabs.
          @memo Get width of tabs
          @param tabIndex Index of first tab
          @param tabs Number of tabs
          @return Width of tabs
       ******************************************************************/
      Int_t GetWidthOfTabs (Int_t tabIndex, Int_t tabs) const;
      /** Get the line number of the current tab.
          @memo Get line of current tab.
          @return Line number
       ******************************************************************/
      Int_t GetLineOfCurrentTab () const;
   };

//@}

}

#endif //_LIGO_TLGMultiTab
