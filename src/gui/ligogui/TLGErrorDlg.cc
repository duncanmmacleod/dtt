/* -*- mode: c++; c-basic-offset: 3; -*- */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <TGMsgBox.h>
//#include <TGTableLayout.h>
#include <TPad.h>
#include <TCanvas.h>
//#include <TGFSComboBox.h>
//#include <TGListView.h>
//#include <TGFSContainer.h>
#include "TLGErrorDlg.hh"
//#include <TSystem.h>
#include <iostream>
#include <fstream>
#include <TVirtualX.h>

   using namespace std;
//   using namespace ligogui;

   const int kOkId = 1;
   const int kCancelId = 0;
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGGridLayout                                                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class TLGGridLayout : public TGLayoutManager {
   public:
      TLGGridLayout (TGCompositeFrame* p, const Int_t* coord)
      : fMain (p), fCoord (coord) {
         fList = fMain->GetList(); }
      virtual void Layout ();
      virtual TGDimension GetDefaultSize() const {
         return TGDimension (fCoord[0], fCoord[1]); }

   protected:
      TGCompositeFrame* fMain;
      TList*            fList;
      const Int_t*      fCoord;
   };

//______________________________________________________________________________
   void TLGGridLayout::Layout ()
   {
      const Int_t* i = fCoord + 2;
      TGFrameElement* ptr;
      TIter next (fList);
      while ((ptr = (TGFrameElement*) next())) {
         ptr->fFrame->MoveResize (i[0], i[1], i[2], i[3]);
         i += 4;
      }
   }


// Constructor for error reporting dialog box, takes a name of a text
// file which contains the messages, and waits for the OK button to be
// clicked or the window to close.  If the file doesn't exist, returns
// null.
   TLGErrorDialog::TLGErrorDialog(const TGWindow *p, const TGWindow *main, const char *fname, TGString title) : TGTransientFrame (p, main, 10, 10)
   {
      // This stuff is done for each constructor.
      CommonSetup(p, main, title) ;

      // Fill in the text box.
      fText->LoadFile(fname) ;

      fClient->WaitFor(this) ;
   }

// Constructor for error reporting dialog box.  Takes a vector of strings,
// display the messages, and wait for the OK button to be clicked or the 
// window to close.
   TLGErrorDialog::TLGErrorDialog(const TGWindow *p, const TGWindow *main, vector<string> *errors, TGString title) : TGTransientFrame (p, main, 10, 10)
   {
      // This stuff is done for each constructor.
      CommonSetup(p, main, title) ;

      // Fill in the text box
      for (std::vector<std::string>::iterator i = errors->begin(); i != errors->end(); ++i)
      {
	 fText->AddLine(i->c_str()) ;
      }

      // Wait for user to close the window.
      fClient->WaitFor(this) ;
   }

// Constructor for error reporting dialog box.  Takes an array of char *,
// display the messages, and wait for the OK button to be clicked or the 
// window to close.
   TLGErrorDialog::TLGErrorDialog(const TGWindow *p, const TGWindow *main, const char *errors[], TGString title) : TGTransientFrame (p, main, 10, 10)
   {
      int i = 0 ;

      // This stuff is done for each constructor.
      CommonSetup(p, main, title) ;

      // Fill in the text box
      while (errors[i] != (char *) NULL)
      {
	 fText->AddLine(errors[i]) ;
	 ++i ;
      }

      // Wait for user to close the window.
      fClient->WaitFor(this) ;
   }

#define BOX_WIDTH 600
#define BOX_HEIGHT 700
#define BUTTON_WIDTH 80
#define BUTTON_HEIGHT 24

   // This is a special version of the error dialog which has two buttons at the bottom, and 
   // returns a boolean value depending on which button is used to close the window.
   TLGErrorDialog::TLGErrorDialog(const TGWindow *p, const TGWindow *main, 
   				std::vector<std::string> *errors, 
				const std::string button1, const std::string but1_tooltip,
				const std::string button2, const std::string but2_tooltip,
				bool *ret, TGString title)
   {
      UInt_t box_width = BOX_WIDTH ;
      UInt_t box_height = BOX_HEIGHT ;
      UInt_t button_width = BUTTON_WIDTH ;
      UInt_t button_height = BUTTON_HEIGHT ;

      // Place two buttons centered in the frame with a total gap of 20 between them,
      // 10 on each side of the center.
      Int_t kButtonGrid[] = {
      BOX_WIDTH, BUTTON_HEIGHT,			// Total width, height of frame.
      (BOX_WIDTH/2 - BUTTON_WIDTH - 10), 0, BUTTON_WIDTH, BUTTON_HEIGHT,	// button1 x,y,w,h
      (BOX_WIDTH/2 + 10), 0, BUTTON_WIDTH, BUTTON_HEIGHT    // button2 x,y,w,h
      } ;

      // Set the return value to 0 in case the window is closed not using the buttons.
      fRet = ret ;
      if (fRet) *fRet = 0 ;

      // Set the width and height for the overall error dialog.
      SetWidth(box_width) ;
      SetHeight(box_height) ;

      // Layout hints.
      // kLHintsLeft places the frame to the left of the container frame after other frames with the same hint
      // into the list.
      // kLHintsTop places the frame position to the top of the container frame, below any laid out frames
      // with the same hint in the list.
      // kLHintsCenterX places the frame position centered hoizontally (in veritical containers).
      // kLHintsExpandX specifies the frame to be expanded to the width of the container frame.
      // kLHintsExpandY specifies the frame to be expanded to the height of the container frame.
      //
      // This window should have two frames in a vertical arrangement.  The bottom one, containing the
      // OK button, should have a fixed height of button_height+4 (2 pixels on top and bottom).  The width
      // of the frame should be expanded to the width of the window, but the button width is fixed at 
      // 65 pixels.
      //
      fL[0] = new TGLayoutHints(kLHintsLeft | kLHintsTop | kLHintsCenterX | kLHintsExpandY, 2, 2, 2, 2) ; // Group frame layout
      fL[1] = new TGLayoutHints(kLHintsLeft | kLHintsBottom | kLHintsCenterX ,2, 2, 2, 2) ; // Ok button layout
      fL[2] = new TGLayoutHints(kLHintsLeft | kLHintsTop | kLHintsExpandX | kLHintsExpandY ,2, 2, 2, 2) ; // TextView layout

      // Horizontal frame to hold the OK button.
      fFButton = new TGHorizontalFrame(this, box_width, button_height) ;
      // Set a layout for the fFButton frame, use a grid layout to place the buttons where we want them.
      fFButton->SetLayoutManager(new TLGGridLayout(fFButton, kButtonGrid)) ;

      // This is the TGTransientFrame's AddFrame().  Add the new frame to the transient frame.
      AddFrame(fFButton, fL[1]) ;

      // button1
      fCancel = new TGTextButton(fFButton, button1.c_str(), kCancelId) ;
      if (but1_tooltip.size())
	 fCancel->SetToolTipText(but1_tooltip.c_str()) ;
      fCancel->Associate(this) ;
      // Add the button to the fFButton frame, with no layout hints.
      fFButton->AddFrame(fCancel, 0) ;

      // button2
      fOk = new TGTextButton(fFButton, button2.c_str(), kOkId) ;
      if (but2_tooltip.size())
	 fOk->SetToolTipText(but2_tooltip.c_str()) ;
      fOk->Associate(this) ;
      // Add the button to the fFButton frame, with no layout hints.
      fFButton->AddFrame(fOk, 0) ;

      // Create a group frame to put a nice titled border around the text box for the errors.
      fG = new TGGroupFrame(this, title) ;
      // This is the TGTransientFrame's AddFrame().  Add the new TGGroupFrame to the transient frame.
      AddFrame(fG, fL[0]) ;
      
      // Create a text object to display the errors.
      fText = new TGTextView(fG, box_width - 8, box_height - 8 - button_height) ;
      fText->Associate(this) ;  
      fG->AddFrame(fText, fL[2]) ;
      fText->Clear() ;


      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                              (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
                              (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                              ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
   
      // make the message box non-resizable
      SetWMSize(width, height);
      SetWMSizeHints(width, height, width, height, 0, 0);
   
      // Set the dialog box title
      SetWindowName(title.GetString()) ;
      SetIconName("Errors") ;
      //SetClassHints("MergeDlg`", "MergeDlg`") ;
      SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
	      kMWMDecorMinimize | kMWMDecorMenu,
	      kMWMFuncAll  | kMWMFuncResize    | kMWMFuncMaximize |
	      kMWMFuncMinimize,
	      kMWMInputModeless);

      MapWindow() ;

      // Fill in the text box
      for (std::vector<std::string>::iterator i = errors->begin(); i != errors->end(); ++i)
      {
	 fText->AddLine(i->c_str()) ;
      }

      // Wait for user to close the window.
      fClient->WaitFor(this) ;
   }

   void
   TLGErrorDialog::CommonSetup(const TGWindow *p, const TGWindow *main, TGString title)
   {
      UInt_t box_width = 600 ;
      UInt_t box_height = 700 ;
      UInt_t button_width = 65 ;
      UInt_t button_height = 24 ;

      fRet = (bool *) NULL ;

      // Set the width and height for the overall error dialog.
      SetWidth(box_width) ;
      SetHeight(box_height) ;

      // Layout hints.
      // kLHintsLeft places the frame to the left of the container frame after other frames with the same hint
      // into the list.
      // kLHintsTop places the frame position to the top of the container frame, below any laid out frames
      // with the same hint in the list.
      // kLHintsCenterX places the frame position centered hoizontally (in veritical containers).
      // kLHintsExpandX specifies the frame to be expanded to the width of the container frame.
      // kLHintsExpandY specifies the frame to be expanded to the height of the container frame.
      //
      // This window should have two frames in a vertical arrangement.  The bottom one, containing the
      // OK button, should have a fixed height of button_height+4 (2 pixels on top and bottom).  The width
      // of the frame should be expanded to the width of the window, but the button width is fixed at 
      // 65 pixels.
      //
      fL[0] = new TGLayoutHints(kLHintsLeft | kLHintsTop | kLHintsCenterX | kLHintsExpandY, 2, 2, 2, 2) ; // Group frame layout
      fL[1] = new TGLayoutHints(kLHintsLeft | kLHintsBottom | kLHintsCenterX ,2, 2, 2, 2) ; // Ok button layout
      fL[2] = new TGLayoutHints(kLHintsLeft | kLHintsTop | kLHintsExpandX | kLHintsExpandY ,2, 2, 2, 2) ; // TextView layout

      // Horizontal frame to hold the OK button.
      fFButton = new TGHorizontalFrame(this, box_width, button_height) ;
      // This is the TGTransientFrame's AddFrame().  Add the new frame to the transient frame.
      AddFrame(fFButton, fL[1]) ;

      // fCancel button
      fCancel = 0 ;

      // Ok button
      fOk = new TGTextButton(fFButton, "OK", kOkId) ;
      fOk->Associate(this) ;
      // Add the button to the fFButton frame, with no layout hints.
      fFButton->AddFrame(fOk, 0) ;
      // Move the button and resize it.  X, Y, width, height.
      fOk->MoveResize(box_width/2 - button_width/2, box_height, button_width, button_height) ;

      // Create a group frame to put a nice titled border around the text box for the errors.
      fG = new TGGroupFrame(this, title) ;
      // This is the TGTransientFrame's AddFrame().  Add the new TGGroupFrame to the transient frame.
      AddFrame(fG, fL[0]) ;
      
      // Create a text object to display the errors.
      fText = new TGTextView(fG, box_width - 8, box_height - 8 - button_height) ;
      fText->Associate(this) ;  
      fG->AddFrame(fText, fL[2]) ;
      fText->Clear() ;


      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                              (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
                              (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                              ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
   
      // make the message box non-resizable
      SetWMSize(width, height);
      SetWMSizeHints(width, height, width, height, 0, 0);
   
      // Set the dialog box title
      SetWindowName(title.GetString()) ;
      SetIconName("Errors") ;
      SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
	      kMWMDecorMinimize | kMWMDecorMenu,
	      kMWMFuncAll  | kMWMFuncResize    | kMWMFuncMaximize |
	      kMWMFuncMinimize,
	      kMWMInputModeless);

      MapWindow() ;
   }

   // Destructor for error reporting dialog box.
   TLGErrorDialog::~TLGErrorDialog()
   {
      if (fOk)
	 delete fOk ;
      if (fCancel)
	 delete fCancel ;
      if (fText)
	 delete fText ;
      if (fFButton)
	 delete fFButton ;
      if (fG)
	 delete fG ;
      for (int i = 0; i < 3; i++)
	 if (fL[i])
	    delete fL[i] ;

   }

   // Method to call if the window needs to be closed from the application.
   void TLGErrorDialog::CloseWindow()
   {
      DeleteWindow() ; // From the sub classes.
   }

   Bool_t TLGErrorDialog::ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2)
   {
      // Only one button.
      if ((GET_MSG (msg) == kC_COMMAND) && (GET_SUBMSG (msg) == kCM_BUTTON))
      {
	 switch (parm1)
	 {
	    // Ok.
	    case kOkId:
	    {
	       // Set the return value. 
	       if (fRet) *fRet = 1 ;
	       // Nothing to do, really.
	       DeleteWindow() ;
	       break ;
	    }
	    default:
	    case kCancelId:
	    {
	       // Set the return value. 
	       if (fRet) *fRet = 0 ;
	       DeleteWindow() ;
	       break ;
	    }
	 }
      }
      return kTRUE ;
   }
