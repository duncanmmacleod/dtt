/* Version $Id: TLGProgressBar.hh 6319 2010-09-17 18:06:52Z james.batch@LIGO.ORG $ */
#ifndef _LIGO_TLGPROGRESSBAR_H
#define _LIGO_TLGPROGRESSBAR_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGProgressBar						*/
/*                                                         		*/
/* Module Description: porgress bar.					*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 13Jan01  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGProgressBar.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
#include <TGFrame.h>

namespace ligogui {


/** @name TLGProgressBar
   
    @memo Progress bar widget
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

/** Progress bar. This is a simple widget implementing a progress
    bar with a completion ratio form 0 to 1. It displayes a green
    bar on gray backgrounbd filling up from left to right. The progress
    bar does not redraw itself periodically. Calling update will 
    trigger a redraw.
    
    @memo Progress bar
    @version 1.0
 ************************************************************************/
   class TLGProgressBar : public TGFrame {
   public:
      /** Constructs a new progress bar widget.
          @memo Constructor.
          @param p Parent window
   	  @param w Width of bar
   	  @param h Height of bar
   	  @param progress Reference to progress ratio (0 to 1)
       ******************************************************************/
      TLGProgressBar (const TGWindow* p, Int_t w, Int_t h,
                     float& progress);
      /// Size
      virtual TGDimension GetDefaultSize() const;
      /// Update
      virtual void Update();
   
   protected:
      /// Completion ratio
      float*		fC;
      /// Context to draw bar
      static GContext_t	fgBarGC;
   
      /// Redraw
      void DoRedraw();
   };

//@}

}

#endif //_LIGO_TLGPROGRESSBAR_H

