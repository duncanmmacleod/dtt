/* -*- mode: c++; c-basic-offset: 3; -*- */
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGLBTree, TLGLBTreeContainer and TLGLBTreeEntry                     //
//                                                                      //
// A list tree is a widget that can contain a number of items           //
// arranged in a tree structure. The items are represented by small     //
// folder icons that can be either open or closed.                      //
//                                                                      //
// The TLGLBTree is user callable. The TLGLBTreeContainer and           //
// TLGLBTreeEntry are service classed of the list tree.                 //
//                                                                      //
// A list tree can generate the following events:                       //
// kC_LISTTREE, kCT_ITEMCLICK, which button, location (y<<16|x).        //
// kC_LISTTREE, kCT_ITEMDBLCLICK, which button, location (y<<16|x).     //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
// remove after testing
//#include <iostream>

#include "TLGLBTree.hh"
#include <TGScrollBar.h>
#include <TMath.h>
#include <TGClient.h>
#include <TEnv.h>
#include <TGPicture.h>
#include <TGCanvas.h>
#include <TVirtualX.h>

#include <iostream> // For debug messages...  JCB

namespace ligogui {

   FontStruct_t TLGLBTreeContainer::fgDefaultFontStruct = (FontStruct_t)-1;



   // ClassImp(TLGLBTreeEntry)
   // ClassImp(TLGLBTreeContainer)
   // ClassImp(TLGLBTree)

   //--- Some utility functions ---------------------------------------------------
   static Int_t FontHeight(FontStruct_t f)
   {
      int max_ascent, max_descent;
      gVirtualX->GetFontProperties(f, max_ascent, max_descent);
      return max_ascent + max_descent;
   }

   static Int_t FontAscent(FontStruct_t f)
   {
      int max_ascent, max_descent;
      gVirtualX->GetFontProperties(f, max_ascent, max_descent);
      return max_ascent;
   }

   static Int_t FontTextWidth(FontStruct_t f, const char *c)
   {
      return gVirtualX->TextWidth(f, c, strlen(c));
   }

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGLBTreeEntry                                                       //
//                                                                      //
// A listbox entry contains a tree entry of a list box tree.            //
// This is a helper class for TLGLBTree.                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

//______________________________________________________________________________
   TLGLBTreeEntry::TLGLBTreeEntry(TGClient *client, const char *name,
                     const char* fullname,
                     const TGPicture *opened,
                     const TGPicture *closed)
   {
   // Create list tree item.
   
      fLength = strlen(name);
      fText   = new char[fLength+1];
      strcpy(fText, name);
   
      if (fullname != 0) {
         fFullLength = strlen(fullname);
         fFullname   = new char[fFullLength+1];
         strcpy(fFullname, fullname);
      }
      else {
         fFullLength = strlen(name);
         fFullname   = new char[fFullLength+1];
         strcpy(fFullname, name);
      }
   
      fOpenPic   = opened;
      fClosedPic = closed;
      fPicWidth  = TMath::Max(fOpenPic->GetWidth(), fClosedPic->GetWidth());
   
      fOpen = fActive = kFALSE;
   
      fParent = 0;
      fFirstchild = 0;
      fLastchild  = 0;
      fPrevsibling = 0;
      fNextsibling = 0;
   
      fUserData = 0;
   
      fClient = client;
   }

//______________________________________________________________________________
   TLGLBTreeEntry::~TLGLBTreeEntry()
   {
   // Delete list tree item.
   
      delete [] fText;
      delete [] fFullname;
      //--- Let him that allocated the picture free it.
      //fClient->FreePicture(fOpenPic);
      //fClient->FreePicture(fClosedPic);
   }

//______________________________________________________________________________
   void TLGLBTreeEntry::AddChild(TLGLBTreeEntry* ent)
   {
      ent->fParent = this;
      ent->fNextsibling = 0;
      if (!fFirstchild) {
	 ent->fPrevsibling = 0;
	 fFirstchild = ent;
      } else {
	 ent->fPrevsibling = fLastchild;
	 fLastchild->fNextsibling = ent;
      }
      fLastchild  = ent;
   }

//______________________________________________________________________________
   void TLGLBTreeEntry::Rename(const char *new_name)
   {
   // Rename a list tree item.
   
      delete [] fText;
      fLength = strlen(new_name);
      fText = new char[fLength + 1];
      strcpy(fText, new_name);
   }

//___________________________________________________________________________
   void TLGLBTreeEntry::SetPictures(const TGPicture* opened, 
                     const TGPicture* closed)
   {
   // Change list tree item icons.
   
      //--- Let him that allocated the picture free it.
      //fClient->FreePicture(fOpenPic);
      //fClient->FreePicture(fClosedPic);
      fOpenPic   = opened;
      fClosedPic = closed;
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGLBTreeContainer                                                   //
//                                                                      //
// A listbox tree container contains a tree of TLGLBTreeEntry.          //
// This is a helper class for TLGLBTree.                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

//______________________________________________________________________________
   TLGLBTreeContainer::TLGLBTreeContainer(TGWindow *p, UInt_t w, UInt_t h, 
                     UInt_t options, ULong_t back) :
   TGFrame(p, w, h, options, back)
   {
      if (fgDefaultFontStruct == (FontStruct_t)-1) {
         char small_font[256];
         strcpy (small_font, gEnv->GetValue ("Gui.SmallFont", 
                                  "-adobe-helvetica-medium-r-*-*-10-"
                                  "*-*-*-*-*-iso8859-1"));
         fgDefaultFontStruct = gClient->GetFontByName (small_font);
      }
   
   // Create a list tree widget.
   
      GCValues_t gcv;
   
      fMsgWindow = p;
   
      fFont = fgDefaultFontStruct;
   
      if (!fClient->GetColorByName("#808080", fGrayPixel))
         fClient->GetColorByName("black", fGrayPixel);
   
      gcv.fLineStyle = kLineSolid;
      gcv.fLineWidth = 0;
      gcv.fFillStyle = kFillSolid;
      gcv.fFont = gVirtualX->GetFontHandle(fFont);
      gcv.fBackground = fgWhitePixel;
      gcv.fForeground = fgBlackPixel;
   
      gcv.fMask = kGCLineStyle  | kGCLineWidth  | kGCFillStyle |
         kGCForeground | kGCBackground | kGCFont;
      //fDrawGC = gVirtualX->CreateGC(fId, &gcv);
      fDrawGC = fClient->GetGC(&gcv)->GetGC();
   
      gcv.fLineStyle = kLineOnOffDash;
      gcv.fForeground = fGrayPixel;
      //fLineGC = gVirtualX->CreateGC(fId, &gcv);
      fLineGC = fClient->GetGC(&gcv)->GetGC();
      gVirtualX->SetDashes(fLineGC, 0, "x1\x1", 2);
   
      gcv.fBackground = fgDefaultSelectedBackground;
      gcv.fForeground = fgWhitePixel;
      gcv.fLineStyle = kLineSolid;
      gcv.fMask = kGCLineStyle  | kGCLineWidth  | kGCFillStyle |
         kGCForeground | kGCBackground | kGCFont;
      //fHighlightGC = gVirtualX->CreateGC(fId, &gcv);
      fHighlightGC = fClient->GetGC(&gcv)->GetGC();
   
      fFirst = fSelected = 0;
      fDefw = (UInt_t) -1;
      fDefh = (UInt_t) -1;
   
      fHspacing = 2;
      fVspacing = 2;  // 0;
      fIndent   = 3;  // 0;
      fMargin   = 2;
   
      gVirtualX->GrabButton(fId, kAnyButton, kAnyModifier,
                           kButtonPressMask | kButtonReleaseMask,
                           kNone, kNone);

      fDefOpenPic = 0;
      fDefClosedPic = 0;
   }

//______________________________________________________________________________
   TLGLBTreeContainer::~TLGLBTreeContainer()
   {
   // Delete list tree widget.
   
      TLGLBTreeEntry *item, *sibling;
   
      //gVirtualX->DeleteGC(fDrawGC);
      //gVirtualX->DeleteGC(fLineGC);
      //gVirtualX->DeleteGC(fHighlightGC);
      item = fFirst;
      while (item) {
         if (item->fFirstchild) PDeleteChildren(item->fFirstchild);
         sibling = item->fNextsibling;
         delete item;
         item = sibling;
      }
      if (fClient && fDefOpenPic) {
	 fClient->FreePicture(fDefOpenPic);
	 fDefOpenPic = 0;
      }
      if (fClient && fDefClosedPic) {
	 fClient->FreePicture(fDefClosedPic);
	 fDefClosedPic = 0;
      }
   }

//---- highlighting utilities

//______________________________________________________________________________
   void TLGLBTreeContainer::HighlightItem(TLGLBTreeEntry *item, Bool_t state, Bool_t draw)
   {
   // Highlight tree item.
   
      if (item) {
         if ((item == fSelected) && !state) {
            fSelected = 0;
            if (draw) DrawItemName(item);
         } 
         else if (state != item->fActive) {
            item->fActive = state;
            if (draw) DrawItemName(item);
         }
      }
   }

//______________________________________________________________________________
   void TLGLBTreeContainer::HighlightChildren(TLGLBTreeEntry *item, Bool_t state, Bool_t draw)
   {
   // Higlight item children.
   
      while (item) {
         HighlightItem(item, state, draw);
         if (item->fFirstchild)
            HighlightChildren(item->fFirstchild, state, (item->fOpen) ? draw : kFALSE);
         item = item->fNextsibling;
      }
   }

//______________________________________________________________________________
   void TLGLBTreeContainer::UnselectAll(Bool_t draw)
   {
   // Unselect all items.
   
      HighlightChildren(fFirst, kFALSE, draw);
   }

//______________________________________________________________________________
   Bool_t TLGLBTreeContainer::HandleButton(Event_t *event)
   {
   // Handle button events in the list tree.
   
      TLGLBTreeEntry *item;
   
      if (event->fType == kButtonPress) {
         if ((item = FindItem(event->fY)) != 0) {
            if (fSelected) fSelected->fActive = kFALSE;
            UnselectAll(kTRUE);
            fSelected = item;
            HighlightItem(item, kTRUE, kTRUE);
            SendMessage(fMsgWindow, MK_MSG (kC_CONTAINER, kCT_ITEMCLICK),
			Long_t(item->GetUserData()), (event->fY << 16) | event->fX);
         }
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGLBTreeContainer::HandleDoubleClick(Event_t *event)
   {
   // Handle double click event in the list tree (only for kButton1).
   
      TLGLBTreeEntry *item;
   
      if (event->fCode == kButton1 && (item = FindItem(event->fY)) != 0) {
         item->fOpen = !item->fOpen;
         if (item != fSelected) { // huh?!
            if (fSelected) fSelected->fActive = kFALSE;
            UnselectAll(kTRUE);
            fSelected = item;
            HighlightItem(item, kTRUE, kTRUE);
         }
         fClient->NeedRedraw(this); 
         DoRedraw();
         SendMessage(fMsgWindow, MK_MSG(kC_CONTAINER, kCT_ITEMDBLCLICK),
		     Long_t(item->GetUserData()), (event->fY << 16) | event->fX);
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGLBTreeContainer::HandleExpose(Event_t *event)
   {
   // Handle expose event in the list tree.
   
      Draw(event->fY, (Int_t)event->fHeight);
      return kTRUE;
   }

//---- drawing functions

//______________________________________________________________________________
   void TLGLBTreeContainer::DoRedraw()
   {
   // Redraw list tree.
      gVirtualX->ClearWindow(fId);
      Draw(0, (Int_t)fHeight);
   }

//______________________________________________________________________________
   void TLGLBTreeContainer::Draw (Int_t yevent, Int_t hevent)
   {
   // Draw list tree widget.
   
      TLGLBTreeEntry *item;
      Int_t  x, y, xbranch;
      UInt_t width, height, old_width, old_height;
   
   // Overestimate the expose region to be sure to draw an item that gets
   // cut by the region
      fExposeTop = yevent - FontHeight(fFont);
      fExposeBottom = yevent + hevent + FontHeight(fFont);
      old_width  = fDefw;
      old_height = fDefh;
   
      fDefw = fDefh = 1;
   
      x = fMargin;
      y = fMargin;
      item = fFirst;
      while (item) {
         xbranch = -1;
         DrawItem(item, x, y, &xbranch, &width, &height);
      
         width += x + fHspacing + fMargin;
      
         if (width > fDefw) fDefw = width;
      
         y += height + fVspacing;
         if (item->fFirstchild && item->fOpen)
            y = DrawChildren(item->fFirstchild, x, y, xbranch);
      
         item = item->fNextsibling;
      }
   
      fDefh = y + fMargin;
   
      if ((old_width != fDefw) || (old_height != fDefh))
         ((TLGLBTree *)GetParent()->GetParent())->Layout();
   }

//______________________________________________________________________________
   Int_t TLGLBTreeContainer::DrawChildren (TLGLBTreeEntry *item, Int_t x, 
                     Int_t y, Int_t xroot)
   {
   // Draw children of item in list tree.
   
      UInt_t width, height;
      Int_t  xbranch;
   
      x += fIndent + (Int_t)item->fPicWidth;
      while (item) {
         xbranch = xroot;
         DrawItem(item, x, y, &xbranch, &width, &height);
      
         width += x + fHspacing + fMargin;
      
         if (width > fDefw) fDefw = width;
      
         y += height + fVspacing;
         if ((item->fFirstchild) && (item->fOpen))
            y = DrawChildren(item->fFirstchild, x, y, xbranch);
      
         item = item->fNextsibling;
      }
      return y;
   }

//______________________________________________________________________________
   void TLGLBTreeContainer::DrawItem(TLGLBTreeEntry *item, Int_t x, Int_t y, Int_t *xroot,
                     UInt_t *retwidth, UInt_t *retheight)
   {
   // Draw list tree item.
   
      Int_t  xpic, ypic, xbranch, ybranch, xtext, ytext, yline, xc;
      UInt_t height;
      const TGPicture *pic;
   
   // Select the pixmap to use, if any
      if (item->fOpen)
         pic = item->fOpenPic;
      else
         pic = item->fClosedPic;
   
   // Compute the height of this line
      height = FontHeight(fFont);
      xpic = x;
      xtext = x + fHspacing + (Int_t)item->fPicWidth;
      if (pic) {
         if (pic->GetHeight() > height) {
            ytext = y + (Int_t)((pic->GetHeight() - height) >> 1);
            height = pic->GetHeight();
            ypic = y;
         } 
         else {
            ytext = y;
            ypic = y + (Int_t)((height - pic->GetHeight()) >> 1);
         }
         xbranch = xpic + (Int_t)(item->fPicWidth >> 1);
         ybranch = ypic + (Int_t)pic->GetHeight();
         yline = ypic + (Int_t)(pic->GetHeight() >> 1);
      } 
      else {
         ypic = ytext = y;
         xbranch = xpic + (Int_t)(item->fPicWidth >> 1);
         yline = ybranch = ypic + (Int_t)(height >> 1);
         yline = ypic + (Int_t)(height >> 1);
      }
   
   // height must be even, otherwise our dashed line wont appear properly
      ++height; height &= ~1;
   
   // Save the basic graphics info for use by other functions
      item->fY      = y;
      item->fXtext  = xtext;
      item->fYtext  = ytext;
      item->fHeight = height;
   
      if ((y+(Int_t)height >= fExposeTop) && (y <= fExposeBottom)) {
         if (*xroot >= 0) {
            xc = *xroot;
         
            if (item->fNextsibling)
               gVirtualX->DrawLine(fId, fLineGC, xc, y, xc, y+height);
            else 
               gVirtualX->DrawLine(fId, fLineGC, xc, y, xc, yline);
         
            TLGLBTreeEntry *p = item->fParent;
            while (p) {
               xc -= (fIndent + (Int_t)p->fPicWidth);
               if (p->fNextsibling)
                  gVirtualX->DrawLine(fId, fLineGC, xc, y, xc, y+height);
               p = p->fParent;
            }
            gVirtualX->DrawLine(fId, fLineGC, *xroot, yline, xpic/*xbranch*/, yline);
            DrawNode(item, *xroot, yline);
         }
         if (item->fOpen && item->fFirstchild)
            gVirtualX->DrawLine(fId, fLineGC, xbranch, ybranch/*yline*/,
                               xbranch, y+height);
      
         if (pic)
            pic->Draw(fId, fDrawGC, xpic, ypic);
      
         DrawItemName(item);
      }
   
      *xroot = xbranch;
      *retwidth = FontTextWidth(fFont, item->fText) + item->fPicWidth;
      *retheight = height;
   }

//______________________________________________________________________________
   void TLGLBTreeContainer::DrawItemName(TLGLBTreeEntry *item)
   {
   // Draw name of list tree item.
   
      UInt_t width;
   
      width = FontTextWidth(fFont, item->fText);
      if (item->fActive || item == fSelected) {
         gVirtualX->SetForeground(fDrawGC, fgDefaultSelectedBackground);
         gVirtualX->FillRectangle(fId, fDrawGC,
                              item->fXtext, item->fYtext, width, FontHeight(fFont));
         gVirtualX->SetForeground(fDrawGC, fgBlackPixel);
         gVirtualX->DrawString(fId, fHighlightGC,
                              item->fXtext, item->fYtext + FontAscent(fFont),
                              item->fText, item->fLength);
      } 
      else {
         gVirtualX->FillRectangle(fId, fHighlightGC,
                              item->fXtext, item->fYtext, width, FontHeight(fFont));
         gVirtualX->DrawString(fId, fDrawGC,
                              item->fXtext, item->fYtext + FontAscent(fFont),
                              item->fText, item->fLength);
      }
   }

//______________________________________________________________________________
   void TLGLBTreeContainer::DrawNode(TLGLBTreeEntry *item, Int_t x, Int_t y)
   {
   // Draw node (little + in box).
   
      if (item->fFirstchild) {
         gVirtualX->DrawLine(fId, fHighlightGC, x, y-2, x, y+2);
         gVirtualX->SetForeground(fHighlightGC, fgBlackPixel);
         gVirtualX->DrawLine(fId, fHighlightGC, x-2, y, x+2, y);
         if (!item->fOpen)
            gVirtualX->DrawLine(fId, fHighlightGC, x, y-2, x, y+2);
         gVirtualX->SetForeground(fHighlightGC, fGrayPixel);
         gVirtualX->DrawLine(fId, fHighlightGC, x-4, y-4, x+4, y-4);
         gVirtualX->DrawLine(fId, fHighlightGC, x+4, y-4, x+4, y+4);
         gVirtualX->DrawLine(fId, fHighlightGC, x-4, y+4, x+4, y+4);
         gVirtualX->DrawLine(fId, fHighlightGC, x-4, y-4, x-4, y+4);
         gVirtualX->SetForeground(fHighlightGC, fgWhitePixel);
      }
   }

//______________________________________________________________________________
   void TLGLBTreeContainer::RemoveReference(TLGLBTreeEntry *item)
   {
   // This function removes the specified item from the linked list.
   // It does not do anything with the data contained in the item, though.
   
      TLGLBTreeEntry* prev   = item->fPrevsibling;
      TLGLBTreeEntry* next   = item->fNextsibling;
      TLGLBTreeEntry* parent = item->fParent;

   // if there is a previous sibling, just skip over item to be dereferenced
      if (prev) {
         prev->fNextsibling = next;
      } 
   // if not, there is a new start node.
      else if (parent) {
	 parent->fFirstchild = next;
      }
      else {
	 fFirst = next;
      }

   // if there is a next item, fix the back chain.
      if (next) {
	 next->fPrevsibling = prev;
      }

   // if there isn't a next item, fix the parrent end-pointer.
      else if (parent) {
	 parent->fLastchild = prev;
      }
   }

//______________________________________________________________________________
   void TLGLBTreeContainer::PDeleteChildren(TLGLBTreeEntry *item)
   {
   // Delete children of item from list.
   
      while (item) {
         if (item->fFirstchild) {
            PDeleteChildren(item->fFirstchild);
            item->fFirstchild = 0;
            item->fLastchild = 0;
         }
         TLGLBTreeEntry* sibling = item->fNextsibling;
         delete item;
         item = sibling;
      }
   }

//______________________________________________________________________________
   void TLGLBTreeContainer::InsertChild(TLGLBTreeEntry *parent, TLGLBTreeEntry *item)
   {
   // Insert child in list.
      
      if (!parent) {  // if parent == 0, this is a top level entry
	 item->fParent      = 0;
	 item->fNextsibling = 0;

         if (fFirst) {
            TLGLBTreeEntry* i = fFirst;
            while (i->fNextsibling) i = i->fNextsibling;
            i->fNextsibling = item;
            item->fPrevsibling = i;
         } 
         else {
            fFirst = item;
	    item->fPrevsibling = 0;
         }
      }
      else {

	 parent->AddChild(item);
      
      } 
   }

//______________________________________________________________________________
   void TLGLBTreeContainer::InsertChildren(TLGLBTreeEntry *parent, 
                     TLGLBTreeEntry *item)
   {
   // Insert a list of ALREADY LINKED children into another list
   
   // Save the reference for the next item in the new list
      while (item) {
	 TLGLBTreeEntry* next = item->fNextsibling;
	 RemoveReference(item);
	 InsertChild(parent, item);
	 item = next;
      }
   }

//______________________________________________________________________________
   Int_t TLGLBTreeContainer::SearchChildren(TLGLBTreeEntry *item, Int_t y, Int_t findy,
                     TLGLBTreeEntry **finditem)
   {
   // Search child item.
   
      UInt_t height;
      const TGPicture *pic;
   
      while (item) {
      // Select the pixmap to use, if any
         if (item->fOpen)
            pic = item->fOpenPic;
         else
            pic = item->fClosedPic;
      
      // Compute the height of this line
         height = FontHeight(fFont);
         if (pic && (pic->GetHeight() > height))
            height = pic->GetHeight();
      // height must be even, otherwise our dashed line wont appear properly
         ++height; height &= ~1;
      
         if ((findy >= y) && (findy <= y + (Int_t)height)) {
            *finditem = item;
            return -1;
         }
      
         y += (Int_t)height + fVspacing;
         if ((item->fFirstchild) && (item->fOpen)) {
            y = SearchChildren(item->fFirstchild, y, findy, finditem);
            if (*finditem) 
               return -1;
         }
      
         item = item->fNextsibling;
      }
   
      return y;
   }

//______________________________________________________________________________
   TLGLBTreeEntry *TLGLBTreeContainer::FindItem(Int_t findy)
   {
   // Find item at postion findy.
   
      Int_t  y;
      UInt_t height;
      TLGLBTreeEntry *item, *finditem;
      const TGPicture *pic;
   
      y = fMargin;
      item = fFirst;
      finditem = 0;
      while (item && !finditem) {
      // Select the pixmap to use, if any
         if (item->fOpen)
            pic = item->fOpenPic;
         else
            pic = item->fClosedPic;
      
      // Compute the height of this line
         height = FontHeight(fFont);
         if (pic && (pic->GetHeight() > height))
            height = pic->GetHeight();
      // height must be even, otherwise our dashed line wont appear properly
         ++height; height &= ~1;
      
         if ((findy >= y) && (findy <= y + (Int_t)height))
            return item;
      
         y += (Int_t)height + fVspacing;
         if ((item->fFirstchild) && (item->fOpen)) {
            y = SearchChildren(item->fFirstchild, y, findy, &finditem);
         //if (finditem) return finditem;
         }
         item = item->fNextsibling;
      }
   
      return finditem;
   }

//----- Public Functions


//______________________________________________________________________________
   void TLGLBTreeContainer::CaculateDefaultSize() const
   {
      UInt_t width, height, x, y;
   
      (UInt_t&)fDefw = 1;
      (UInt_t&)fDefh = 1;
      x = fMargin;
      y = fMargin;
   
      TLGLBTreeEntry* item = fFirst;
      while (item) {
         GetItemSize (item, &width, &height);
         width += x + fHspacing + fMargin;
         if (width > fDefw) (UInt_t&)fDefw = width;
         y += height + fVspacing;
      
         if (item->fFirstchild && item->fOpen)
            y = GetChildrenSize (item->fFirstchild, x, y);
         item = item->fNextsibling;
      }
      (UInt_t&)fDefh = y + fMargin;
   }

//______________________________________________________________________________
   void TLGLBTreeContainer::GetItemSize (TLGLBTreeEntry* item, 
                     UInt_t* retwidth, UInt_t* retheight) const
   {
      UInt_t height;
      const TGPicture *pic;
   
   // Select the pixmap to use, if any
      if (item->fOpen)
         pic = item->fOpenPic;
      else
         pic = item->fClosedPic;
   
   // Compute the height of this line
      height = FontHeight(fFont);
      if (pic && (pic->GetHeight() > height)) {
         height = pic->GetHeight();
      } 
   
   // height must be even, otherwise our dashed line wont appear properly
      ++height; height &= ~1;
   
      *retwidth = FontTextWidth(fFont, item->fText) + item->fPicWidth;
      *retheight = height;
   }

//______________________________________________________________________________
   UInt_t TLGLBTreeContainer::GetChildrenSize (TLGLBTreeEntry* item, 
                     UInt_t x, UInt_t y) const
   {
      UInt_t width, height;
   
      x += fIndent + (Int_t)item->fPicWidth;
      while (item) {
         GetItemSize (item, &width, &height);
         width += x + fHspacing + fMargin;
         if (width > fDefw) (UInt_t&)fDefw = width;
         y += height + fVspacing;
      
         if ((item->fFirstchild) && (item->fOpen))
            y = GetChildrenSize (item->fFirstchild, x, y);
         item = item->fNextsibling;
      }
      return y;
   }

//______________________________________________________________________________
   TGDimension TLGLBTreeContainer::GetDefaultSize() const
   { 
      if ((fDefh == (UInt_t)-1)  || (fDefw == (UInt_t)-1))
         CaculateDefaultSize();
      return TGDimension(fDefw, fDefh);
   }

//______________________________________________________________________________
   UInt_t TLGLBTreeContainer::GetDefaultHeight() const
   {
      if (fDefh == (UInt_t)-1) 
         CaculateDefaultSize();
      return fDefh; 
   }

//______________________________________________________________________________
   UInt_t TLGLBTreeContainer::GetDefaultWidth() const
   { 
      if (fDefw == (UInt_t)-1) 
         CaculateDefaultSize();
      return fDefw; 
   }

//______________________________________________________________________________
   TLGLBTreeEntry *TLGLBTreeContainer::AddItem(TLGLBTreeEntry *parent, 
                     const char *string,
                     const TGPicture *open, const TGPicture *closed)
   {
      return AddItem (parent, string, (char*) 0, open, closed);
   }

//______________________________________________________________________________
   TLGLBTreeEntry *TLGLBTreeContainer::AddItem(TLGLBTreeEntry *parent, 
                     const char *string, void *userData, 
                     const TGPicture *open, const TGPicture *closed)
   {
      return AddItem (parent, string, 0, userData, open, closed);
   }

//______________________________________________________________________________
   TLGLBTreeEntry *TLGLBTreeContainer::AddItem(TLGLBTreeEntry *parent, 
                     const char *string, const char* fullname, 
                     const TGPicture *open, const TGPicture *closed)
   {
   // Add item to list tree. Returns new item.
      TLGLBTreeEntry *item;
   
      if (!open) {
	 if (!fDefOpenPic) 
	    fDefOpenPic = fClient->GetPicture("ofolder_t.xpm");
	 open = fDefOpenPic;
      }
      if (!closed) {
	 if (!fDefClosedPic)
	    fDefClosedPic = fClient->GetPicture("folder_t.xpm");
	 closed = fDefClosedPic;
      }
   
      item = new TLGLBTreeEntry (fClient, string, fullname, open, closed);
      if (item)
         InsertChild (parent, item);
   
      fDefw = fDefh = (UInt_t)-1;
      fClient->NeedRedraw(this);
   
      return item;
   }

//______________________________________________________________________________
   TLGLBTreeEntry *TLGLBTreeContainer::AddItem(TLGLBTreeEntry *parent, 
                     const char *string, const char* fullname, void *userData, 
                     const TGPicture *open, const TGPicture *closed)
   {
      TLGLBTreeEntry *item = AddItem (parent, string, fullname, open, closed);
      if (item) item->SetUserData(userData);
      return item;
   }

//______________________________________________________________________________
   void TLGLBTreeContainer::RenameItem(TLGLBTreeEntry *item, const char *string)
   {
   // Rename item in list tree.
   
      item->Rename(string);
      fDefw = fDefh = (UInt_t)-1;
      fClient->NeedRedraw(this);
   }

//______________________________________________________________________________
   Int_t TLGLBTreeContainer::DeleteItem(TLGLBTreeEntry *item)
   {
   // Delete item from list tree.
      if (!item && !fFirst)
         return 0;
   
      if (!item) {
         PDeleteChildren(fFirst);
         fSelected = 0;
         fFirst = 0;
      }
      else {
         if (item->fFirstchild)
            PDeleteChildren(item->fFirstchild);
         item->fFirstchild = 0;
         item->fLastchild = 0;
         RemoveReference(item);
         if (fSelected == item)
            fSelected = 0;
         delete item;
      }
   
      fDefw = fDefh = (UInt_t)-1;
      fClient->NeedRedraw(this);
   
      return 1;
   }

//______________________________________________________________________________
   Int_t TLGLBTreeContainer::RecursiveDeleteItem(TLGLBTreeEntry *item, void *ptr)
   {
   // Delete item with fUserData == ptr. Search tree downwards starting
   // at item.
   
      if (item && ptr) {
         if (item->fUserData == ptr)
            DeleteItem(item);
         else {
            if (item->fOpen && item->fFirstchild)
               RecursiveDeleteItem(item->fFirstchild,  ptr);
            RecursiveDeleteItem(item->fNextsibling, ptr);
         }
      }
      return 1;
   }

//______________________________________________________________________________
   Int_t TLGLBTreeContainer::DeleteChildren(TLGLBTreeEntry *item)
   {
   // Delete children of item from list.
   
      if (item->fFirstchild)
         PDeleteChildren(item->fFirstchild);
   
      item->fFirstchild = 0;
      item->fLastchild = 0;
   
      fDefw = fDefh = (UInt_t)-1;
      fClient->NeedRedraw(this);
   
      return 1;
   }

//______________________________________________________________________________
   Int_t TLGLBTreeContainer::Reparent(TLGLBTreeEntry *item, TLGLBTreeEntry *newparent)
   {
   // Make newparent the new parent of item.
   
   // Remove the item from its old location.
      RemoveReference(item);
   
   // The item is now unattached. Reparent it.
      InsertChild(newparent, item);
   
      fDefw = fDefh = (UInt_t)-1;
      fClient->NeedRedraw(this);
   
      return 1;
   }

//______________________________________________________________________________
   Int_t TLGLBTreeContainer::ReparentChildren(TLGLBTreeEntry *item,
                     TLGLBTreeEntry *newparent)
   {
   // Make newparent the new parent of the children of item.
   
      TLGLBTreeEntry *first;
   
      if (item->fFirstchild) {
         first = item->fFirstchild;
         item->fFirstchild = 0;
         item->fLastchild = 0;
      
         InsertChildren(newparent, first);
      
         fDefw = fDefh =(UInt_t) -1;
         fClient->NeedRedraw(this);
         return 1;
      }
      return 0;
   }

//______________________________________________________________________________
extern "C" 
   Int_t CompareTLGLBTreeEntry (const void *item1, const void *item2)
   {
      return strcmp((*((TLGLBTreeEntry **) item1))->GetText(),
                   (*((TLGLBTreeEntry **) item2))->GetText());
   }

//______________________________________________________________________________
   Int_t TLGLBTreeContainer::Sort(TLGLBTreeEntry *item)
   {
   // Sort items starting with item.
   
      TLGLBTreeEntry *first, *parent, **list;
      size_t i, count;
   
   // Get first child in list;
      while (item->fPrevsibling) item = item->fPrevsibling;
   
      first = item;
      parent = first->fParent;
   
   // Count the children
      count = 1;
      while (item->fNextsibling) item = item->fNextsibling, count++;
      if (count <= 1) 
         return 1;
   
      list = new TLGLBTreeEntry* [count];
      list[0] = first;
      count = 1;
      while (first->fNextsibling) {
         list[count] = first->fNextsibling;
         count++;
         first = first->fNextsibling;
      }
   
      ::qsort(list, count, sizeof(TLGLBTreeEntry*), &CompareTLGLBTreeEntry);
   
      list[0]->fPrevsibling = 0;
      for (i = 0; i < count; i++) {
         if (i < count - 1)
            list[i]->fNextsibling = list[i + 1];
         if (i > 0)
            list[i]->fPrevsibling = list[i - 1];
      }
      list[count - 1]->fNextsibling = 0;
      if (parent) {
         parent->fFirstchild = list[0];
         parent->fLastchild = list[count-1];
      }
      else {
         fFirst = list[0];
      }   
      delete [] list;
   
      fDefw = fDefh = (UInt_t)-1;
      fClient->NeedRedraw(this);
   
      return 1;
   }

//______________________________________________________________________________
   Int_t TLGLBTreeContainer::SortSiblings(TLGLBTreeEntry *item)
   {
   // Sort siblings of item.
   
      return Sort(item);
   }

//______________________________________________________________________________
   Int_t TLGLBTreeContainer::SortChildren(TLGLBTreeEntry *item)
   {
   // Sort children of item.
   
      TLGLBTreeEntry *first;
   
      if (item) {
         first = item->fFirstchild;
         if (first)
            SortSiblings(first);
      } 
      else {
         if (fFirst) {
            first = fFirst->fFirstchild;
            if (first)
               SortSiblings(first);
         }
      }
      return 1;
   }

//______________________________________________________________________________
   TLGLBTreeEntry *TLGLBTreeContainer::FindSiblingByName(TLGLBTreeEntry *item, const char *name)
   {
   // Find sibling of item by name.
   
   // Get first child in list
      if (item) {
         while (item->fPrevsibling)
            item = item->fPrevsibling;
      
         while (item) {
            if (strcmp(item->fText, name) == 0)
               return item;
            item = item->fNextsibling;
         }
         return item;
      }
      return 0;
   }

//______________________________________________________________________________
   TLGLBTreeEntry *TLGLBTreeContainer::FindSiblingByData(TLGLBTreeEntry *item, 
                     void *userData)
   {
   // Find sibling of item by userData.
   
   // Get first child in list
      if (item) {
         while (item->fPrevsibling)
            item = item->fPrevsibling;
      
         while (item) {
            if (item->fUserData == userData)
               return item;
            item = item->fNextsibling;
         }
         return item;
      }
      return 0;
   }

//______________________________________________________________________________
   TLGLBTreeEntry *TLGLBTreeContainer::FindByData (TLGLBTreeEntry* item,
                     void *userData)
   {
   // Find item by userData.
      if (!item) {
         if (fFirst == 0) 
            return 0;
         else
            item = fFirst;
      }
   
   // Get first sibling in list
      while (item->fPrevsibling) {
         item = item->fPrevsibling;
      }
   
      while (item) {
         if (item->fUserData == userData)
            return item;
         if (item->fFirstchild) {
            TLGLBTreeEntry* found = FindByData (item->fFirstchild, userData);
            if (found) 
               return found;
         }
         item = item->fNextsibling;
      }
   // not found
      return 0;
   }

//______________________________________________________________________________
   TLGLBTreeEntry *TLGLBTreeContainer::FindByFullname (TLGLBTreeEntry* item, 
                     const char* fullname, const int rate)
   {
   // Find item by userData.
      if (!item) {
         if (fFirst == 0) 
            return 0;
         else
            item = fFirst;
      }
   
   // Get first sibling in list
      while (item->fPrevsibling) {
         item = item->fPrevsibling;
      }
   
      while (item) {
	 // Always compare the name, only compare the rate if it's not -1 or 0.
         if (item->fFullname && (strcmp (item->fFullname, fullname) == 0) && 
	      ((rate == -1) || (rate == 0 )|| (rate == (long) (item->fUserData))))
            return item;
         if (item->fFirstchild) {
            TLGLBTreeEntry* found = FindByFullname (item->fFirstchild, fullname, rate);
            if (found) 
               return found;
         }
         item = item->fNextsibling;
      }
   // not found
      return 0;
   }

//______________________________________________________________________________
   TLGLBTreeEntry *TLGLBTreeContainer::FindChildByName(TLGLBTreeEntry *item, 
                     const char *name)
   {
   // Find child of item by name.
   
   // Get first child in list
      if (item && item->fFirstchild) {
         item = item->fFirstchild;
      } 
      else if (!item && fFirst) {
         item = fFirst;
      } 
      else {
         item = 0;
      }
   
      while (item) {
         if (strcmp(item->fText, name) == 0)
            return item;
         item = item->fNextsibling;
      }
      return 0;
   }

//______________________________________________________________________________
   TLGLBTreeEntry *TLGLBTreeContainer::FindChildByData(TLGLBTreeEntry *item, 
                     void *userData)
   {
   // Find child of item by userData.
   
   // Get first child in list
      if (item && item->fFirstchild) {
         item = item->fFirstchild;
      } 
      else if (!item && fFirst) {
         item = fFirst;
      } 
      else {
         item = 0;
      }
   
      while (item) {
         if (item->fUserData == userData)
            return item;
         item = item->fNextsibling;
      }
      return 0;
   }

//______________________________________________________________________________
   void TLGLBTreeContainer::HighlightItem(TLGLBTreeEntry *item)
   {
   // make sure tree is unfolded
      TLGLBTreeEntry* p = item->fParent;
      while (p) {
         p->fOpen = kTRUE;
         p = p->fParent;
      }
   
   // Highlight item.
      if (fSelected) fSelected->fActive = kFALSE;
      UnselectAll(kTRUE);
      fSelected = item;
      HighlightItem(item, kTRUE, kFALSE);
      fClient->NeedRedraw(this);
   }

//______________________________________________________________________________
   void TLGLBTreeContainer::ClearHighlighted()
   {
   // Un highlight items.
   
      UnselectAll(kFALSE);
      fClient->NeedRedraw(this);
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGLBTree                                                             //
//                                                                      //
// A listbox contains a container frame which is viewed through a       //
// viewport. If the container is larger than the viewport than a        //
// vertical scrollbar is added.                                         //
//                                                                      //
// Selecting an item in the listbox will generate the event:            //
// kC_COMMAND, kCM_LISTBOX, listbox id, item id.                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

//______________________________________________________________________________
   TLGLBTree::TLGLBTree(const TGWindow *p, Int_t id,
                     UInt_t options, ULong_t back) :
   TGCompositeFrame(p, 10, 10, options, back)
   {
   // Create a listbox.
   
      fMsgWindow = p;
      fListBoxId = id;
   
      fItemVsize = 1;
      fIntegralHeight = kTRUE;
   
      InitListBox();
   }

//______________________________________________________________________________
   TLGLBTree::~TLGLBTree()
   {
   // Delete a listbox widget.
   
      delete fVScrollbar;
      delete fLbc;
      delete fVport;
   }

//______________________________________________________________________________
   void TLGLBTree::InitListBox()
   {
   // initiate the internal classes of a list box
   
      fVport = new TGViewPort (this, 6, 6, kChildFrame, fgWhitePixel);
      fVScrollbar = new TGVScrollBar (this, kDefaultScrollBarWidth, 6);
      fLbc = new TLGLBTreeContainer (fVport, 10, 10, 
                           kVerticalFrame, fgWhitePixel);
      fLbc->Associate(this);
      SetContainer(fLbc);
   
      AddFrame (fVport, 0);
      AddFrame (fVScrollbar, 0);
      AddFrame (fLbc, 0);
   
      fVScrollbar->Associate(this);
      fSBLastpos = -1;
   
      gVirtualX->SelectInput(fVScrollbar->GetId(), kButtonPressMask |
                           kButtonReleaseMask | kPointerMotionMask);
      gVirtualX->SelectInput(fLbc->GetId(), kButtonPressMask |
                           kButtonReleaseMask /*| kPointerMotionMask */);
   }

//______________________________________________________________________________
   void TLGLBTree::DrawBorder()
   {
   // Draw borders of the list box widget.
   
#if ROOT_VERSION_CODE > ROOT_VERSION(3,5,5)
      GContext_t shadow = GetShadowGC()();
      GContext_t black = GetBlackGC()();
      GContext_t hilight = GetHilightGC()();
      GContext_t bckgnd = GetBckgndGC()();
#else
      GContext_t shadow = fgShadowGC();
      GContext_t black = fgBlackGC();
      GContext_t hilight = fgHilightGC();
      GContext_t bckgnd = fgBckgndGC();
#endif
      switch (fOptions & (kSunkenFrame | kRaisedFrame | kDoubleBorder)) {
         case kSunkenFrame | kDoubleBorder:
            gVirtualX->DrawLine(fId, shadow, 0, 0, fWidth-2, 0);
            gVirtualX->DrawLine(fId, shadow, 0, 0, 0, fHeight-2);
            gVirtualX->DrawLine(fId, black, 1, 1, fWidth-3, 1);
            gVirtualX->DrawLine(fId, black, 1, 1, 1, fHeight-3);
         
            gVirtualX->DrawLine(fId, hilight, 0, fHeight-1, fWidth-1, fHeight-1);
            gVirtualX->DrawLine(fId, hilight, fWidth-1, fHeight-1, fWidth-1, 0);
            gVirtualX->DrawLine(fId, bckgnd,  1, fHeight-2, fWidth-2, fHeight-2);
            gVirtualX->DrawLine(fId, bckgnd,  fWidth-2, 1, fWidth-2, fHeight-2);
            break;
      
         default:
            TGCompositeFrame::DrawBorder();
            break;
      }
   }

//______________________________________________________________________________
   void TLGLBTree::Resize(UInt_t w, UInt_t h)
   {
   // Resize the listbox widget. If fIntegralHeight is true make the height
   // an integral number of the maximum height of a single entry.
   
      if (fIntegralHeight)
         h = TMath::Max(fItemVsize, ((h - (fBorderWidth << 1)) / fItemVsize) * fItemVsize)
            + (fBorderWidth << 1);
      TGCompositeFrame::Resize(w, h);
      fSBLastpos = -1;
   }

//______________________________________________________________________________
   void TLGLBTree::MoveResize(Int_t x, Int_t y, UInt_t w, UInt_t h)
   {
   // Move and resize the listbox widget.
   
      if (fIntegralHeight)
         h = TMath::Max(fItemVsize, ((h - (fBorderWidth << 1)) / fItemVsize) * fItemVsize)
            + (fBorderWidth << 1);
      TGCompositeFrame::MoveResize(x, y, w, h);
      fSBLastpos = -1;
   }

//______________________________________________________________________________
   TGDimension TLGLBTree::GetDefaultSize() const
   {
   // Return default size of listbox widget.
   
      UInt_t h;
   
      if (fIntegralHeight)
         h = TMath::Max(fItemVsize, ((fHeight - (fBorderWidth << 1)) / fItemVsize) * fItemVsize)
            + (fBorderWidth << 1);
      else
         h = fHeight;
   
      return TGDimension(fWidth, h);
   }

//______________________________________________________________________________
   void TLGLBTree::Layout()
   {
   // Layout the listbox components.
   
      TGFrame *container;
      UInt_t   cw, ch, tch;
      Bool_t   need_vsb;
   
      need_vsb = kFALSE;
   
      container = GetContainer();
   
   // test whether we need vertical scrollbar or not
   
      cw = fWidth - (fBorderWidth << 1);
      ch = fHeight - (fBorderWidth << 1);
   
      container->SetWidth(cw);
      container->SetHeight(ch);
   
      if (container->GetDefaultHeight() > ch) {
         need_vsb = kTRUE;
         cw -= fVScrollbar->GetDefaultWidth();
         if ((Int_t) cw < 0) {
            Warning("Layout", "width would become too small, setting to 10");
            cw = 10;
         }
         container->SetWidth(cw);
      }
   
      if (need_vsb) {
         fVScrollbar->MoveResize(cw+fBorderWidth, fBorderWidth, 
                              fVScrollbar->GetDefaultWidth(), ch);
         fVScrollbar->MapWindow();
      } 
      else {
         fVScrollbar->UnmapWindow();
         fVScrollbar->SetPosition(0);
      }
   
      fVport->MoveResize(fBorderWidth, fBorderWidth, cw, ch);
      container->Layout();
      tch = TMath::Max(container->GetDefaultHeight(), ch);
      container->SetHeight(0); // force a resize in TGFrame::Resize
      container->Resize(cw, tch);
      // fVport->SetPos(0, 0);
   
      if (need_vsb) {
         fVScrollbar->SetRange(container->GetHeight()/fItemVsize, 
                              fVport->GetHeight()/fItemVsize);
      }
      gClient->NeedRedraw (fLbc);
   }

//______________________________________________________________________________
   void* TLGLBTree::GetSelected() const
   {
   // Return id of selected listbox item.
      TLGLBTreeEntry * item = GetSelectedEntry();
      if (item) {
         return item->GetUserData();
      }
      else 
         return 0;
   }

//______________________________________________________________________________
   const char* TLGLBTree::GetSelectedName() const
   {
   // Return id of selected listbox item.
      TLGLBTreeEntry * item = GetSelectedEntry();
      if (item) 
         return item->GetFullname();
      else 
         return 0;
   }

//______________________________________________________________________________
   Bool_t TLGLBTree::GetSelection (void *userData) const {
      TLGLBTreeEntry * item = fLbc->FindByData (0, userData);
      if (item) 
         return item->IsActive();
      else 
         return kFALSE;
   }

//______________________________________________________________________________
   Bool_t TLGLBTree::GetSelectionByName (const char* fullname) const {
      TLGLBTreeEntry * item = fLbc->FindByFullname (0, fullname);
      if (item) 
         return item->IsActive();
      else 
         return kFALSE;
   }

//______________________________________________________________________________
   TLGLBTreeEntry *TLGLBTree::Select (void *userData, Bool_t sel) { 
      TLGLBTreeEntry * item = fLbc->FindByData (0, userData);
      if (item) {
         if (sel)
            fLbc->HighlightItem (item);
         else
            fLbc->ClearHighlighted ();
         return item;
      }
      else {
         fLbc->ClearHighlighted ();
         return 0;
      }
   } 

//______________________________________________________________________________
   TLGLBTreeEntry *TLGLBTree::SelectByName (const char* fullname, Bool_t sel, const int rate) 
   { 
      // fLbc is a TLGLBTreeContainer pointer.  Call TLGLBTreeContainer::FindByFullname().
      TLGLBTreeEntry * item = fLbc->FindByFullname (0, fullname, rate);
      if (item) {
         if (sel) {
            fLbc->HighlightItem (item);
         }
         else {
            fLbc->ClearHighlighted ();
         }
         return item;
      }
      else {
         fLbc->ClearHighlighted ();
         return 0;
      }
   } 

//______________________________________________________________________________
   Bool_t TLGLBTree::ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2)
   {
   // Process messages generated by the listbox container and forward
   // messages to the listbox message handling window.
      switch (GET_MSG(msg)) {
         case kC_VSCROLL:
            switch (GET_SUBMSG(msg)) {
               case kSB_SLIDERTRACK:
                  fVport->SetVPos(Int_t(-parm1 * fItemVsize));
                  gClient->NeedRedraw (fLbc);
                  break;
               case kSB_SLIDERPOS:
                  fVport->SetVPos(Int_t(-parm1 * fItemVsize));
                  if (parm1 != fSBLastpos) {
                     gClient->NeedRedraw (fLbc);
                     fSBLastpos = parm1;
                  }
                  break;
            }
            break;
      
         case kC_CONTAINER:
            switch (GET_SUBMSG(msg)) {
               case kCT_ITEMCLICK:
                  SendMessage (fMsgWindow, MK_MSG(kC_LISTTREE, kCT_ITEMCLICK),
                              fListBoxId, parm1);
                  break;
               case kCT_ITEMDBLCLICK:
                  SendMessage (fMsgWindow, MK_MSG(kC_LISTTREE, kCT_ITEMDBLCLICK),
                              fListBoxId, parm1);
                  break;
            }
            break;
      
         default:
            break;
      
      }
      return kTRUE;
   }


}
