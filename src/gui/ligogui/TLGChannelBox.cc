/* -*- mode: c++; c-basic-offset: 3; -*- */
/* Version $Id: TLGChannelBox.cc 7574 2016-02-16 20:17:52Z james.batch@LIGO.ORG $ */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/* Module Name:  TLGChannelBox						     */
/*                                                                           */
/* Module Description:  ROOT GUI widget for handling LIGO channel names      */
/*                                                                           */
/*---------------------------------------------------------------------------*/

#include "TLGChannelBox.hh"
#include <stdlib.h>
#include <cstring>
#include <ctype.h>
#include <iostream>
#include <TVirtualX.h>
#include <TGClient.h>
#include <TGPicture.h>
#include <TSystem.h>
#include <strings.h>
#include <stdio.h>

namespace ligogui {
   using namespace std;

   static const int my_debug = 0 ;

//--- Some utility functions ---------------------------------------------------
#if 0
   static Int_t FontHeight(FontStruct_t f)
   {
      int max_ascent, max_descent;
      gVirtualX->GetFontProperties(f, max_ascent, max_descent);
      return max_ascent + max_descent;
   }

   static Int_t FontAscent(FontStruct_t f)
   {
      int max_ascent, max_descent;
      gVirtualX->GetFontProperties(f, max_ascent, max_descent);
      return max_ascent;
   }

   static Int_t FontTextWidth(FontStruct_t f, const char *c)
   {
      return gVirtualX->TextWidth(f, c, strlen(c));
   }
#endif

   //-----------------------------------Sort functions

   int ChannelTree_channelcmpNameRate(const void *s1, const void *s2) {
      const ChannelEntry *c1 = (ChannelEntry *) s1 ;
      const ChannelEntry *c2 = (ChannelEntry *) s2 ;
      int nameval = strcasecmp(c1->Name(), c2->Name()) ;
      // If the names are identical, compare based on rate.
      if (nameval == 0)
      {
	 if (c1->Rate() < c2->Rate())
	 {
	    if (my_debug) cerr << c1->Name() << " has two rates, " << c1->Rate() << ", " << c2->Rate() << endl ;
	    return -1 ;
         } else if (c1->Rate() == c2->Rate()) {
	    return 0 ;
	 } else {
	    if (my_debug) cerr << c1->Name() << " has two rates, " << c1->Rate() << ", " << c2->Rate() << endl ;
	    return 1 ;
	 }
      }
      return nameval ;
   }

   int ChannelTree_channelcmp (const void* s1, const void* s2) {
      return strcasecmp(((ChannelEntry*)s1)->Name(), 
			((ChannelEntry*)s2)->Name());
   }

extern "C" 
   int ChannelTree_channelcmprate (const void* s1, const void* s2) {
      const ChannelEntry* c1 = (ChannelEntry*)s1;
      const ChannelEntry* c2 = (ChannelEntry*)s2;
      if (((c1->Rate() > 0) && (c1->Rate() <= 16)) !=
	  ((c2->Rate() > 0) && (c2->Rate() <= 16))) {
         return ((c1->Rate() > 0) && (c1->Rate() <= 16)) ? 1 : -1;
      }
      else {
         return strcasecmp (c1->Name(), c2->Name());
      }
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// ChannelTree                                                          //
//                                                                      //
// A list of channel names                                              //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   ChannelTree::ChannelTree (const ChannelEntry* chns, UInt_t chnnum,
			     Bool_t copy, Int_t type)
      : fChnClient (0), fLeftArrow (0), fChannels (0), fChnNum (0), 
	fOwned(kTRUE), fChannelTreeType (type)
   {
      SetChannels (chns, chnnum, copy);
   }


   ChannelTree::ChannelTree (const char* const* chnnames, UInt_t chnnum,
			     Int_t type)
      : fChnClient (0), fLeftArrow (0), fChannels (0), fChnNum (0), 
	fOwned(kTRUE), fChannelTreeType (type)
   {
       SetChannels (chnnames, chnnum);
   }


   ChannelTree::ChannelTree (const char* chnnames, Int_t type)
      : fChnClient (0), fLeftArrow (0), fChannels (0), fChnNum (0), 
	fOwned(kTRUE), fChannelTreeType (type)
   {
      if (chnnames != 0) {
         SetChannels (chnnames);
      }
   }


   ChannelTree::~ChannelTree ()
   {
      ReSize(0);
      if (fChnClient && fLeftArrow) {
	 fChnClient->FreePicture(fLeftArrow);
	 fLeftArrow = 0;
      }      
   }

   Bool_t
   ChannelTree::ReSize(UInt_t chnnum) {
      if (fChannels != NULL) {
	 if (fOwned) delete[] fChannels;
	 fChannels = 0;
      }
      fChnNum = 0;
      fOwned  = kTRUE;
      
      if (chnnum) {
	 fChannels = new ChannelEntry [chnnum];
	 if (fChannels == NULL) return kFALSE;
	 fChnNum = chnnum;
      }
      return kTRUE;
   }

   void  
   ChannelTree::SetChannelClient(TGClient* tc) {
      if (fChnClient && fLeftArrow) {
	 fChnClient->FreePicture(fLeftArrow);
	 fLeftArrow = 0;
      }
      fChnClient = tc;
      fLeftArrow = fChnClient->GetPicture("arrow_left.xpm");
   }

   Bool_t ChannelTree::SetChannels (const ChannelEntry* chns, UInt_t chnnum,
				    Bool_t copy)
   {
      // allocate memory
      ReSize(0);

      if ((chnnum == 0) || (chns == 0)) {
         return kTRUE;
      }

      if (copy) {
	 if (!ReSize(chnnum)) return kFALSE;
	 for (UInt_t i = 0; i < fChnNum; ++i) {
	    fChannels[i] = chns[i];
	 }
	 //cout << "Channels deep copied: " << chnnum << endl;
      } else {
	 fChannels = const_cast<ChannelEntry*>(chns);
	 fChnNum   = chnnum;
	 fOwned    = kFALSE;
	 //cout << "Channels shallow copied: " << chnnum << endl;
      }
      return kTRUE;
   }


   Bool_t 
   ChannelTree::SetChannels (const char* const* chnnames, UInt_t chnnum)
   {
      // allocate memory
      if (!ReSize(chnnum)) {
         return kFALSE;
      }

      if ((chnnum == 0) || (chnnames == 0)) {
         return kTRUE;
      }
   
      // assign channels
      for (UInt_t i = 0; i < chnnum; i++) {
         fChannels[i].SetName (chnnames[i]);
         fChannels[i].SetUDN ("");
         fChannels[i].SetRate (0);
      }
   
      // sort first - The list must be sorted so we can easily put
      // channels into the branches of the tree.
      if ((fChannelTreeType & kChannelTreeSeparateSlow) != 0) {
         ::qsort(fChannels, chnnum, sizeof (ChannelEntry),  
		 ChannelTree_channelcmprate);
      }
      else {
         ::qsort(fChannels, chnnum, sizeof (ChannelEntry), 
		 ChannelTree_channelcmpNameRate) ; // Compare both name and data rate.
      }
      cout << "Channels set from string list: " << chnnum 
	   << " type: " << fChannelTreeType << endl;
      return kTRUE;
   }


   Bool_t ChannelTree::SetChannels (const char* chnnames)
   {
      ReSize(0);
      if (my_debug) {
	 cerr << "ChannelTree::SetChannels( char *chnnames)" << endl ;
	 for (int i = 0; i < 256; i++) 
	    cerr << chnnames[i] ;
	 cerr << endl ;
      }
      return MakeChannelList (chnnames, fChannels, fChnNum, fChannelTreeType);
   }

   Bool_t 
   ChannelTree::MakeChannelList (const char* chnnames,	ChannelEntry*& chns, 
				 UInt_t& chnnum, Int_t tree_type)
   {
      if (my_debug) cerr << "ChannelTree::MakeChannelList( char * chnnames, ...)" << endl ;
      // delete existing memory
      if (chns != 0) {
         delete [] chns;
         chns = 0;
      }
      chnnum = 0;
      if (chnnames == 0) {
         return kTRUE;
      }

      // Count the number of channels in chnnames by assuming the names
      // have spaces between them.  Count the spaces in the chnnames array
      // of characters, the number of spaces + 1 should be the number of 
      // channels, represented by size.
      UInt_t size = 1;
      int i = 0;
      while  (chnnames[i] != 0) {
         if (isspace (chnnames[i])) {
            size++;
         }
         i++;
      }

      // Allocate an array of ChannelEntry class instances, one for each
      // channel name.  The ChannelEntry class has fName, fRate, and fUDN
      // fields, along with methods to set and access them.`
      chns = new ChannelEntry [size];
      if (chns == 0) {
         return kFALSE;
      }
      // assign channel names
      char* p = new char [strlen (chnnames) + 1];
      if (p == 0) {
         return kFALSE;
      }
      strcpy (p, chnnames);
      char* lasts;
      char* tok = strtok_r (p, " \t\n\f\r\v", &lasts);
      while ((tok != 0) && (chnnum < size)) {
         chns[chnnum].SetName (tok);
         chns[chnnum].SetUDN ("");
         chns[chnnum].SetRate (0);
         tok = strtok_r (0, " \t\n\f\r\v", &lasts);
         // check if following token represents a UDN
         if (tok && (*tok == '@')) {
            chns[chnnum].SetUDN (tok + 1);
            tok = strtok_r (0, " \t\n\f\r\v", &lasts);
         }
         // check if following token represents sampling rate?
         bool num = true;
         for (char* c = tok; c && *c && num; c++) {
            num = isdigit (*c) || (*c == '.');
         }
         if (tok && *tok && num) {
            chns[chnnum].SetRate (atof (tok));
            tok = strtok_r (0, " \t\n\f\r\v", &lasts);
         }
         chnnum++;
      }
      delete [] p;
      SortChannelList(chns, chnnum, tree_type);
      //cout << "Channels set in MakeChannelList: " << chnnum 
      //     << " type: " << tree_type << endl;
      return kTRUE;
   }

   //-----------------------------------  Sort channe list for tree
   Bool_t 
   ChannelTree::SortChannelList(ChannelEntry chns[], UInt_t chnnum, 
				Int_t tree_type) {
      if (my_debug) cerr << "ChannelTree::SortChannelList()" << endl ;
      if ((tree_type & kChannelTreeSeparateSlow) != 0) {
         ::qsort(chns, chnnum, sizeof (ChannelEntry),  
		 ChannelTree_channelcmprate);
      }
      else {
         ::qsort(chns, chnnum, sizeof (ChannelEntry), 
		  ChannelTree_channelcmpNameRate) ; // Compare both name and data rate.
      }
      if (my_debug) cerr << "ChannelTree::SortChannelList() - return true" << endl ;
      return kTRUE;
   }


   Bool_t ChannelTree::GetIfoSub (const char* chn, 
                     char* ifo, char* sub, char* rest)
   {
      *ifo = 0;
      *sub = 0;

      const char* cur = chn;
      const char* p = strchr (cur, ':');
      if (p == 0) {
         return kFALSE;
      }
      size_t lifo = p - cur;
      if (lifo > 63) lifo = 63;
      memcpy(ifo, cur, lifo);
      ifo[lifo] = 0;
      cur = p+1;

      // vacuum channels!
      char* w = strchr (ifo, '-');
      if (w) {
	 size_t lsub = lifo;
	 lifo = w - ifo;
         *w++ = 0;

	 lsub -= lifo + 1;
         memcpy (sub, w, lsub);
         sub[lsub] = 0;

	 size_t lrest = strlen(cur);
	 if (lrest > 255) lrest = 255;
         memcpy(rest, cur, lrest);
         rest[lrest] = 0;
         return  (lifo == 3 && lsub == 2) ? kTRUE : kFALSE;
      }
      // normal channels
      else {
         p = strchr (cur, '-');
         if (p == NULL) {
            return kFALSE;
         }
         
	 size_t lsub = p - cur;
         memcpy (sub, cur, lsub);
         sub[lsub] = 0;

	 size_t lrest = strlen(++p);
         strncpy (rest, p, lrest);
         rest[lrest] = 0;
         return (lifo == 2 && lsub == 3) ? kTRUE : kFALSE;
      }
   }

   Bool_t ChannelTree::GetIfoSubLoc (const char* chn, 
                     char* ifo, char* sub, char* loc, char* rest)
   {
      *ifo = 0;
      *sub = 0;
      *loc = 0;

      const char* cur = chn;

      //  find the ':', copy preceding text to ifo;
      const char* p = strchr (cur, ':');
      if (p == 0) return kFALSE;
      size_t lifo = p - cur;
      if (lifo > 63) lifo = 63;
      memcpy(ifo, cur, lifo);
      ifo[lifo] = 0;
      cur = p + 1;

      // vacuum channels!
      char* w = strchr (ifo, '-');
      if (w) {
	 //  Move subsystem name from ifo
	 size_t lsub = lifo;
	 lifo = w - ifo;
	 *w++ = 0;

	 lsub -= lifo + 1;
         memcpy(sub, w, lsub);
         sub[lsub] = 0;

	//  Find separator between location and rest.
         p = strchr (cur, '_');
         if (p == 0) {
            strcpy (rest, "*");
	    size_t lrest = strlen(cur);
	    if (lrest > 255) lrest = 255;
            memcpy(loc, cur, lrest);
            rest[lrest] = 0;
         }
         else {
	    size_t len = p - cur;
	    if (len > 255) len = 255;
	    strncpy (loc, cur, len);
	    loc[len] = 0;

	    len = strlen(++p);
	    if (len > 255) len = 255;
            memcpy(rest, p, len);
            rest[len] = 0;
	 }
         return  (lifo == 3 && lsub == 2) ? kTRUE : kFALSE;
      }
      // normal channels
      else {
	 // Copy subsystem name
         p = strchr (cur, '-');
         if (p == 0) {
            return kFALSE;
         }
	 size_t lsub = p - cur;
	 if (lsub > 63) lsub = 63;
         memcpy (sub, cur, lsub);
         sub[lsub] = 0;
         cur = p + 1;

	 // Copy rest if available
         p = strchr (cur, '_');
         if (p == 0) {
            strcpy (rest, "*");
	    size_t lrest = strlen(cur);
	    if (lrest > 255) lrest = 255; 
	    memcpy(loc, cur, lrest);
	    loc[lrest] = 0;
         }
         else {
	    size_t len = p - cur;
	    if (len > 255) len = 255; 
	    memcpy(loc, cur, len);
	    loc[len] = 0;

	    len = strlen(++p);
	    if (len > 255) len = 255; 
            strncpy (rest, p, len);
            rest[len] = 0;
         }

	 // Copy location if present
         return (lifo == 2 && lsub == 3) ? kTRUE : kFALSE;
      }
   }

   Int_t ChannelTree::GetLocFromRest(char *rest, char *loc)
   {
      char *cur = rest ;
      char *p ;
      size_t len ;

      *loc = 0 ;
      // Find the separator between sections in the name.
      p = strchr(cur, '_') ;
      // If there isn't a separator, or the remainder past
      // the separator is "_DQ", leave.
      if (p == 0 || strcmp(p, "_DQ") == 0)
      {
	 // If there are no more separators, leave rest unchanged and return false.
	 strcpy(loc, "*") ;
	 return 0 ;
      }
      len = p - cur ; 
      if (len > 63) len = 63 ; 	// If this happens, it's going to look funny.
      memcpy(loc, cur, len) ;   // Copy the chars from rest to loc
      loc[len] = '\0' ;		// Terminate the string.
      cur = p+1 ;		// Point to the remainder of the rest beyond the '_' 
      len = strlen(cur) ;	// See how many chars are left in rest
      if (len > 0)
      {
	 memmove(rest, cur, len) ;	// Move the remainder of the string to rest.
	 rest[len] = '\0' ;	// Terminate the rest at the new length.
      }
      return 1 ;
   }

   static char*
   itoa(int i, char* p) {
      if (i/10 != 0) p = itoa(i/10, p);
      *p++ = (i % 10) + '0';
      return p;
   }

   inline char*
   cpy_str(char* d, const char* s) {
     while (*s) *d++ = *s++;
     *d = 0;
     return d;
   }

   static void 
   displayedName (const char* rest, Int_t type, 
		  const ChannelEntry* chn, char* buf)
   {
      int mode = 0;
      if ((type & kChannelTreeShowRate)   &&   chn->Rate() > 0)  mode += 1;
      if ((type & kChannelTreeShowSource) && *(chn->UDN()) != 0) mode += 2;

      buf = cpy_str(buf, rest);
      if (!mode) return;

      buf = cpy_str(buf, " (");
      if ((mode & 1) != 0) buf = itoa(int(chn->Rate()), buf);
      if ((mode & 2) != 0) {
	buf = cpy_str(buf, " @ ");
	buf = cpy_str(buf, chn->UDN());
      }
      cpy_str(buf, ")");
      return;
      //switch (mode) {
      //case 0:
      // sprintf(buf, "%s", rest);
      // break;
      //case 1:
      // sprintf(buf, "%s (%g)", rest, chn->Rate());
      // break;
      //case 2:
      // sprintf(buf, "%s ( @ %s)", rest, chn->UDN());
      // break;
      //case 3:
      // sprintf(buf, "%s (%g @ %s)", rest, chn->Rate(), chn->UDN());
      // break;
      //}
      //return TString(buf);
   }

   //--------  Build the channel tree
   // The channel tree is displayed in channel selection combo box widgets
   // as a tree with branches for IFO, subsystem, and location.  As an example
   // if a channel name is H2:SUS-ITMY_BQF_EXC, the IFO is H2, the subsystem is
   // SUS, and the location is ITMY.  
   Bool_t ChannelTree::BuildChannelTree ()
   {
      // Get rid of any old data for the tree.
      DeleteChannels();
      if (fChannels == 0) {
         return kTRUE;
      }

      // go through list and add to tree
      char prevIfo[64] = {0};
      char ifo[64] = {0};
      TLGLBTreeEntry* prevIfoItems = 0;
      char prevSub[64] = {0};
      char sub[64] = {0};
      TLGLBTreeEntry* prevSubItems = 0;
      char prevLoc[256] = {0};
      char loc[256] = {0};
      TLGLBTreeEntry* prevLocItems = 0;
      // Add for additional channel hierarchy - JCB
      char prevLoc2[256] = {0} ;
      char loc2[256] = {0};
      TLGLBTreeEntry *prevLoc2Items = 0 ;
      char prevLoc3[256] = {0} ;
      char loc3[256] = {0} ;
      TLGLBTreeEntry *prevLoc3Items = 0 ;
      char prevLoc4[256] = {0} ;
      char loc4[256] = {0} ;
      TLGLBTreeEntry *prevLoc4Items = 0 ;
      // End additional hierarchy - JCB
      char rest[256] = {0};
      int* oddItems = new int [fChnNum];
      int oddNum = 0;
      if (oddItems == 0) {
         return kFALSE;
      }
   
      for (UInt_t i = 0; i < fChnNum; i++) {
         bool succ = false;
         if (fChannelTreeType & (kChannelTreeLevel3 | kChannelTreeLevel6)) {
	     // Parse the channel name into IFO, subsystem, and location.
            succ = GetIfoSubLoc (fChannels[i].Name(), ifo, sub, loc, rest);
         }
         else {
	    // Parse the channel name into IFO and subsystem.
            succ = GetIfoSub (fChannels[i].Name(), ifo, sub, rest);
         }
// For additional hierarchy. - JCB
	 int levels ; 
	 if (fChannelTreeType & (kChannelTreeLevel3 | kChannelTreeLevel6))
	    levels = 3 ;
	 else
	    levels = 2 ;
	 if (fChannelTreeType & kChannelTreeLevel6)
	 {
	    // Get another level from the remainder of the name.
	    levels += GetLocFromRest(rest, loc2) ;
	 }
	 if (fChannelTreeType & kChannelTreeLevel6 && levels > 3)
	 {
	    // Get another level from the remainder of the name.
	    levels += GetLocFromRest(rest, loc3) ;
	 }
	 if (fChannelTreeType & kChannelTreeLevel6 && levels > 4)
	 {
	    // Get another level from the remainder of the name.
	    levels += GetLocFromRest(rest, loc4) ;
	 }
// end - JCB
         if (succ) {
	    // Use the data rate to decide if this is a slow channel or fast.
            if (fChannelTreeType & kChannelTreeSeparateSlow) {
               if ((fChannels[i].Rate() > 0) && (fChannels[i].Rate() <= 16)) 
	       {
                  strcpy (ifo + strlen (ifo), " (slow)");
               }
            }
            // create site/ifo branch if necessary
            if ((strcasecmp (prevIfo, ifo) != 0) || (prevIfoItems == 0)) 
	    {
               prevIfoItems = AddChannel (0, ifo);
               strcpy (prevIfo, ifo);
	       // Each time a new ifo branch is created, a new subsystem branch must be started.
               prevSubItems = 0;
               strcpy (prevSub, "");
            }
            if (prevIfoItems == 0) 
	    {
               delete [] oddItems;
               return kFALSE;
            }
            // create subsystem branch if necessary
            if ((strcasecmp (prevSub, sub) != 0) || (prevSubItems == 0)) 
	    {
               prevSubItems = AddChannel (prevIfoItems, sub);
               strcpy (prevSub, sub);
	       // Each time a new subsystem branch is created, a new location branch must be started.
	       prevLocItems = 0 ;	// JCB bugzilla 356
	       strcpy (prevLoc, "") ;	// JCB bugzilla 356
            }
            if (prevSubItems == 0) 
	    {
               delete [] oddItems;
               return kFALSE;
            }
            // create location branch if necessary
            if (fChannelTreeType & kChannelTreeLevel3) 
	    {
               if ((strcasecmp (prevLoc, loc) != 0) || (prevLocItems == 0)) 
	       {
                  prevLocItems = AddChannel (prevSubItems, loc);
                  strcpy (prevLoc, loc);
		  // Added for additional hierarchy - JCB
		  prevLoc2Items = 0 ;
		  strcpy (prevLoc2, "") ;
               }
               if (prevLocItems == 0) 
	       {
                  delete [] oddItems;
                  return kFALSE;
               }
            }
	    // For additional levels of hierarchy - JCB
	    if (fChannelTreeType & kChannelTreeLevel6 && levels > 3)
	    {
	       // Create a new branch if necessary.
	       if ((strcasecmp(prevLoc2, loc2) != 0) || (prevLoc2Items == 0))
	       {
		  prevLoc2Items = AddChannel(prevLocItems, loc2) ;
		  strcpy(prevLoc2, loc2) ;
		  // Each time a new branch is created, a new loc. branch must be started.
		  prevLoc3Items = 0 ;
		  strcpy (prevLoc3, "") ;
	       }
	       if (prevLoc2Items == 0)
	       {
		  delete [] oddItems ;
		  return kFALSE ;
	       }
	    }
	    if (fChannelTreeType & kChannelTreeLevel6 && levels > 4)
	    {
	       // Create a new branch if necessary.
	       if ((strcasecmp(prevLoc3, loc3) != 0) || (prevLoc3Items == 0))
	       {
		  prevLoc3Items = AddChannel(prevLoc2Items, loc3) ;
		  strcpy(prevLoc3, loc3) ;
		  prevLoc4Items = 0 ;
		  strcpy(prevLoc4, "") ;
	       }
	       if (prevLoc3Items == 0)
	       {
		  delete [] oddItems ;
		  return kFALSE ;
	       }
	    }
	    if (fChannelTreeType & kChannelTreeLevel6 && levels > 5)
	    {
	       // Create a new branch if necessary.
	       if ((strcasecmp(prevLoc4, loc4) != 0) || (prevLoc4Items == 0))
	       {
		  prevLoc4Items = AddChannel(prevLoc3Items, loc4) ;
		  strcpy(prevLoc4, loc4) ;
	       }
	       if (prevLoc4Items == 0)
	       {
		  delete [] oddItems ;
		  return kFALSE ;
	       }
	    }
	    // Create the channel entry.
	    TLGLBTreeEntry *pItems ;
	    if (levels > 5)
	       pItems = prevLoc4Items ;
	    else if (levels > 4)
	       pItems = prevLoc3Items ;
	    else if (levels > 3)
	       pItems = prevLoc2Items ;
	    else if (levels > 2)
	       pItems = prevLocItems ;
	    else
	       pItems = prevSubItems ;

	    char entryname[128];
	    displayedName (rest, fChannelTreeType, fChannels+i, entryname) ;

            if (fChnClient == 0) 
	    {
               AddChannel (pItems, entryname, fChannels[i].Name(), (long) (fChannels[i].Rate()));
            }
            else 
	    {
               AddChannel (pItems, entryname, fChannels[i].Name(), (long) (fChannels[i].Rate()), fLeftArrow, fLeftArrow);
            }
         }
         // oddball channel names: mark them and add at end
         else 
	 {
            oddItems[oddNum] = i;
            oddNum++;
         }
      }
   
      // check odd channel names
      if (oddNum > 0) {
         prevIfoItems = AddChannel (0, "others");
         if (prevIfoItems == 0) {
            delete [] oddItems;
            return kFALSE;
         }
         for (int i = 0; i < oddNum; i++) {
	   char entryname[128];
	   displayedName (fChannels[oddItems[i]].Name(), fChannelTreeType, 
			  fChannels + oddItems[i], entryname);
            if (fChnClient == 0) {
               AddChannel (prevIfoItems, entryname,
                          fChannels[oddItems[i]].Name(), oddItems[i]);
            }
            else {
               AddChannel (prevIfoItems, entryname, 
                          fChannels[oddItems[i]].Name(), oddItems[i], 
                          fLeftArrow, fLeftArrow);
            }
         }
      }
   
      delete [] oddItems;
      return kTRUE;
   }


   Int_t ChannelTree::GetChannelId (const char* channelname) const 
   {
      for (UInt_t i = 0; i < fChnNum; i++) {
         if (strcasecmp (fChannels[i].Name(), channelname) == 0) {
            return i;
         }
      }
      return -1;
   }


   const char* ChannelTree::GetChannelName (Int_t id) const
   {
      if ((id < 0) || ((UInt_t)id >= fChnNum)) {
         return 0;
      }
      else {
         return fChannels[id].Name();
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGChannelListbox                                                    //
//                                                                      //
// A tree list box to deal with channel names                           //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGChannelListbox::TLGChannelListbox (TGWindow* p, Int_t id, 
                     ChannelEntry* chnnames, UInt_t chnnum, 
                     Int_t type, UInt_t option, ULong_t back)
   :  TLGLBTree (p, id, option, back), 
   ChannelTree (chnnames, chnnum, type)
   {
      SetChannelClient(fClient);
      BuildChannelTree();
   }


   TLGChannelListbox::TLGChannelListbox (TGWindow* p, Int_t id, 
                     const char* chnnames, Int_t type, 
                     UInt_t option, ULong_t back)
   : TLGLBTree (p, id, option, back), ChannelTree (chnnames, type)
   {
      SetChannelClient(fClient);
      BuildChannelTree();
   }


   TLGChannelListbox::~TLGChannelListbox ()
   {
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGChannelCombobox                                                   //
//                                                                      //
// A tree combobox to deal with channel names                           //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGChannelCombobox::TLGChannelCombobox (TGWindow* p, Int_t id, 
					   ChannelEntry* chnnames, 
					   UInt_t chnnum, Bool_t copy,
					   Int_t type, Bool_t editable, 
					   UInt_t option, ULong_t back)
   : TLGComboTree (p, id, editable, option),
     ChannelTree (chnnames, chnnum, copy, type)
   {
      SetChannelClient(fClient);
      // This is the ChannelTree::BuildChannelTree().
      BuildChannelTree();
   }


   TLGChannelCombobox::TLGChannelCombobox (TGWindow* p, Int_t id, 
                     const char* chnnames, Int_t type, 
                     Bool_t editable, UInt_t option, ULong_t back)
   : TLGComboTree (p, id, editable, option), ChannelTree (chnnames, type)
   {
      SetChannelClient(fClient);
      // This is the ChannelTree::BuildChannelTree().
      BuildChannelTree();
   }


   TLGChannelCombobox::~TLGChannelCombobox ()
   {
   }

   const char * TLGChannelCombobox::GetChannelNameStripped() {
     snprintf(fChannelNameStripped, sizeof fChannelNameStripped, "%s", GetChannel());

     char *start, *stop;

     //find first non-white space character
     for(start=fChannelNameStripped; *start <= 32 && *start; ++start);

     //zero out any white space characters at the end
     for(stop=fChannelNameStripped + strlen(fChannelNameStripped); *stop <= 32 && (stop != start); --stop) *stop = 0 ;

     return start;
   }

}
