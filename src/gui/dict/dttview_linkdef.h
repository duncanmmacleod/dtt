#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;

#pragma link C++ namespace ligogui;

#pragma link C++ class ligogui::TLGCalibrationDialog;

#pragma link C++ enum ligogui::EExportTypeConversion;
#pragma link C++ enum ligogui::EExportOutputType;
#pragma link C++ struct ligogui::ExportColumn_t;
#pragma link C++ struct ligogui::ExportOption_t;
#pragma link C++ class ligogui::TLGExportDialog;

#pragma link C++ enum ligogui::ERefTraceMod;
#pragma link C++ struct ligogui::ReferenceTrace_t;
#pragma link C++ struct ligogui::ReferenceTraceList_t;
#pragma link C++ struct ligogui::MathTable_t;
#pragma link C++ class ligogui::TLGReferenceDialog;

#pragma link C++ enum ligogui::EPlotStyle;
#pragma link C++ enum ligogui::EXRangeType;
#pragma link C++ enum ligogui::EYRangeType;
#pragma link C++ enum ligogui::EUnitType;
#pragma link C++ enum ligogui::EAxisScale;
#pragma link C++ enum ligogui::ERange;
#pragma link C++ enum ligogui::ECursorStyle;
#pragma link C++ enum ligogui::ECursorType;
#pragma link C++ enum ligogui::ELegendPlacement;
#pragma link C++ enum ligogui::ELegendStyle;
#pragma link C++ enum ligogui::ELegendText;
#pragma link C++ struct ligogui::OptionValues_t;
#pragma link C++ struct ligogui::OptionTraces_t;
#pragma link C++ struct ligogui::OptionRange_t;
#pragma link C++ struct ligogui::OptionUnits_t;
#pragma link C++ struct ligogui::OptionCursor_t;
#pragma link C++ struct ligogui::OptionConfig_t;
#pragma link C++ struct ligogui::OptionStyle_t;
#pragma link C++ struct ligogui::OptionAxis_t;
#pragma link C++ struct ligogui::OptionLegend_t;
#pragma link C++ struct ligogui::OptionParam_t;
#pragma link C++ struct ligogui::OptionAll_t;
#pragma link C++ class ligogui::OptionArray;
#pragma link C++ class ligogui::TLGOptions;
#pragma link C++ class ligogui::TLGOptionTraces;
#pragma link C++ class ligogui::TLGOptionRange;
#pragma link C++ class ligogui::TLGOptionUnits;
#pragma link C++ class ligogui::TLGOptionCursor;
#pragma link C++ class ligogui::TLGOptionConfig;
#pragma link C++ class ligogui::TLGOptionStyle;
#pragma link C++ class ligogui::TLGOptionAxis;
#pragma link C++ class ligogui::TLGOptionLegend;
#pragma link C++ class ligogui::TLGOptionParam;
#pragma link C++ class ligogui::TLGOptionTab;
#pragma link C++ class ligogui::TLGOptionDialog;

#pragma link C++ class ligogui::TLGPostScript;
#pragma link C++ class ligogui::TLGPrintParam;
#pragma link C++ class ligogui::TLGPrintDialog;

// #pragma link C++ class ligogui::xsilHandlerUnknown;
// #pragma link C++ class ligogui::xsilHandlerData;
// #pragma link C++ class ligogui::xsilHandlerOptions;
// #pragma link C++ class ligogui::xsilHandlerCalibration;
// #pragma link C++ class ligogui::xsilHandlerQueryData;
// #pragma link C++ class ligogui::xsilHandlerQueryOptions;
// #pragma link C++ class ligogui::xsilHandlerQueryReferenence;
// #pragma link C++ class ligogui::xsilHandlerQueryCalibration;
// #pragma link C++ class ligogui::xsilHandlerQueryMath;
// #pragma link C++ class ligogui::xsilHandlerQueryUnknown;
#pragma link C++ class ligogui::TLGSaver;
#pragma link C++ class ligogui::TLGRestorer;
#pragma link C++ class ligogui::TLGXMLSaver;
#pragma link C++ class ligogui::TLGXMLRestorer;

#pragma link C++ class ligogui::TLGraphExtensions;
// #pragma link C++ class ligogui::TLGraph;
#pragma link C++ class ligogui::TLGPadLayout;
#pragma link C++ class ligogui::TLGPad;
#pragma link C++ class ligogui::TLGMultiPadLayout;
#pragma link C++ class ligogui::TLGMultiPadLayoutGrid;
#pragma link C++ class ligogui::TLGMultiPad;
#pragma link C++ class ligogui::TLGLayoutDialog;
#pragma link C++ class ligogui::TLGPadMain;

#pragma link C++ enum ligogui::ESaveRestoreFlag;
#pragma link C++ enum ligogui::EButtonCommandIdentifiers;
#pragma link C++ enum ligogui::EMenuCommandIdentifiers;
#pragma link C++ class ligogui::NotificationMessage;
#pragma link C++ class ligogui::TLGMainWindow;

#pragma link C++ class ligogui::PlotList;

#pragma link C++ class ligogui::BugReportDlg;

#endif

