#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;

#pragma link C++ namespace ligogui;
#pragma link C++ class ligogui::TLGMonitorSelection;
#pragma link C++ class ligogui::TLGMonitorSelectionDialog;
//#pragma link C++ class ligogui::TLGMonitorDatum;
//#pragma link C++ class ligogui::TLGMonitorDatumList;
//#pragma link C++ class ligogui::TLGMonitorMgr;
//#pragma link C++ class ligogui::xsilHandlerMonitor;
//#pragma link C++ class ligogui::xsilHandlerQueryMonitor;

#endif

