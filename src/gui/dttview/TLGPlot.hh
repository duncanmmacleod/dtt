/* Version $Id: TLGPlot.hh 7192 2014-10-10 17:14:03Z james.batch@LIGO.ORG $ */
#ifndef _LIGO_TLGPLOT_H
#define _LIGO_TLGPLOT_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGPlot							*/
/*                                                         		*/
/* Module Description: High level plotting tools for signal analysis:	*/
/*		       Plot - general plotting routine			*/
/*		       BodePlot, TSPlot, PSPlot - specialized routines	*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 20Nov99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGPlot.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

//#include "PlotSet.hh"

   class PlotSet;
   class PlotDescriptor;
   class AttDataDescriptor;
   class BasicDataDescriptor;

namespace ligogui {

   class TLGMultiPad;


/** @name TLGPlot
    This header exports a global plotting pool and the main potting
    routines.

    @memo Plotting routines
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

   /// Global variable describing a plot set or pool
   PlotSet& gPlotSet();

   /// Resets the global plot pool and closes all associated plot pads
   void ResetPlots();

   /// Maximum numbner of elements in plot list
   const int kMaxPlotList = 8;

   /// Deafult number of pads
   int& gDefaultPadNumber();


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// PlotList                                                             //
//                                                                      //
// List of plot descriptors                                             //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

/** Describes a mini container which can hold up to kMaxPlotList
    pointers to plot descriptors. This container is used by the
    Plot routines to display muliple curves in the same plot. This
    container does not own the plot descriptors but rather 
    stores pointer to them.
   
    @memo Mini container for plot descriptors
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class PlotList {
      friend TLGMultiPad* Plot (PlotList& plist, const char* plottype);
   
   protected:
      /// list of plot descriptors
      PlotDescriptor* 	fList[kMaxPlotList];
      /// size of container
      int		fSize;
   
   public:
      /** Constructs an empty container.
          @memo Default constructor.
          @return void
       ******************************************************************/
      PlotList () : fSize (0) {
      }
      /** Constructs a container with up to 8 elements.
          @memo Constructor.
   	  @param p1 plot descriptor
   	  @param p2 plot descriptor
   	  @param p3 plot descriptor
   	  @param p4 plot descriptor
   	  @param p5 plot descriptor
   	  @param p6 plot descriptor
   	  @param p7 plot descriptor
   	  @param p8 plot descriptor
          @return void
       ******************************************************************/
      PlotList (PlotDescriptor* p1, PlotDescriptor* p2 = 0,
               PlotDescriptor* p3 = 0, PlotDescriptor* p4 = 0,
               PlotDescriptor* p5 = 0, PlotDescriptor* p6 = 0,
               PlotDescriptor* p7 = 0, PlotDescriptor* p8 = 0) 
      : fSize (0) {
         Add (p1);Add (p2);Add (p3);Add (p4);
         Add (p5);Add (p6);Add (p7);Add (p8); }
   
      /** Returns the number of elements in the container.
          @memo Size method.
          @return number of elements
       ******************************************************************/
      int Size() const {
         return fSize; }
   
      /** Returns the plot descriptor with given index.
          @memo Access method.
          @param index Index into conatiner
          @return Pointer to plot descriptor
       ******************************************************************/
      PlotDescriptor* operator() (int index) const {
         return ((index >= 0) && (index < fSize)) ? fList[index] : 0; }
   
      /** Add a plot descriptor to the container.
          @memo Add method.
          @param pl Pointer to plot descriptor
       ******************************************************************/
      void Add (PlotDescriptor* pl) {
         if ((pl != 0) && (fSize < kMaxPlotList)) fList[fSize++] = pl; }
      /** Removes a plot descriptor from the container.
          @memo Remove method.
          @param index Index into conatiner
       ******************************************************************/
      void Remove (int index) {
         if (index >= 0)
            for (int i = index + 1; i < fSize; i++) fList[i-1] = fList[i];
         fSize--; }
      /** Clears the container.
          @memo Clear method.
       ******************************************************************/
      void Clear () {
         fSize = 0; }
   };


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Plot                                                                 //
//                                                                      //
// Main plot routines                                                   //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

/** @name Plot
    These are the main (overloaded) plotting routines.

    @memo Main plotting routines
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{	
   /** Bring up the plot panel.
       @memo Bring up the plot panel
       @return pointer to multi pad
    *********************************************************************/
   TLGMultiPad* Plot();

   /** The plot will use the specified graph type and channel names,
       if they are supplied. If not it will query the user data object.
       The plot type is deduced from the graph type.

       A plot descriptor is automatically generated and added to the 
       global pool.
 
       @memo Plot routine (user data object).
       @param data User data object which derives from AttDataDescriptor
       @param graphtype Graph type used by plot
       @param Achn A channel name used by plot
       @param Bchn B channel name used by plot
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   TLGMultiPad* Plot (const AttDataDescriptor& data, 
                     const char* graphtype = 0, 
                     const char* Achn = 0, const char* Bchn = 0);

   /** The plot will use the specified graph type and channel names,
       if they are supplied. If not it will query the user data object.
       The specified plot type is used for plotting. Possible values
       are specified in TLGCore.h.

       A plot descriptor is automatically generated and added to the 
       global pool. 
 
       @memo Plot routine (user data object & explicit plot type).
       @param plotype String describing the type of plot to be used
       @param data User data object which derives from AttDataDescriptor
       @param graphtype Graph type used by plot
       @param Achn A channel name used by plot
       @param Bchn B channel name used by plot
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   TLGMultiPad* Plot (const char* plottype, 
                     const AttDataDescriptor& data, 
                     const char* graphtype = 0, 
                     const char* Achn = 0, const char* Bchn = 0);

   /** The plot uses the specified graph type and channel names.
       The plot type is deduced from the graph type. Possible values
       are specified in TLGCore.h.

       The data descriptor is adopted by the routine.

       A plot descriptor is automatically generated and added to the 
       global pool. 
 
       @memo Plot routine (data descriptor).
       @param plotype String describing the type of plot to be used
       @param data Data descriptor
       @param graphtype Graph type used by plot
       @param Achn A channel name used by plot
       @param Bchn B channel name used by plot
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   TLGMultiPad* Plot (BasicDataDescriptor* data, 
                     const char* graphtype, 
                     const char* Achn, const char* Bchn = 0);

   /** The plot uses the specified graph type and channel names.
       The specified plot type is used for plotting. Possible values
       are specified in TLGCore.h.

       The data descriptor is adopted by the routine.

       A plot descriptor is automatically generated and added to the 
       global pool. 
 
       @memo Plot routine (data descriptor & explicit plot type).
       @param plotype String describing the type of plot to be used
       @param data Data descriptor
       @param graphtype Graph type used by plot
       @param Achn A channel name used by plot
       @param Bchn B channel name used by plot
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   TLGMultiPad* Plot (const char* plottype, 
                     BasicDataDescriptor* data, 
                     const char* graphtype, 
                     const char* Achn, const char* Bchn = 0);

   /** The plot type is deduced from the graph type if not specified. 
       Possible values are specified in TLGCore.h.

       The plot descriptor is added to the global plot pool if
       it isn't owned by another pool. The plot pool will adopt 
       the plot descriptor.

       If a user supplied plot pool should be used, the data 
       descriptor or the user data object must first be added to
       the plot pool. The returned plot descriptor can then be used 
       together with this routine to display the plot.
 
       @memo Plot routine (plot descriptor).
       @param data Plot descriptor
       @param plotype String describing the type of plot to be used
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   TLGMultiPad* Plot (PlotDescriptor* data, const char* plottype = 0);

   /** Same as above (reversed argument list).

       @memo Plot routine (plot descriptor).
       @param plotype String describing the type of plot to be used
       @param data Plot descriptor
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   TLGMultiPad* Plot (const char* plottype, PlotDescriptor* data);

   /** Displays multiple curves in the same plot.

       The plot type is deduced from the graph type of the first
       plot descriptor in the list if not specified. 
       Possible values are specified in TLGCore.h.

       The plot descriptors are added to the global plot pool if
       aren't owned by another pool. The plot pool will adopt
       the plot descriptors.

       If a user supplied plot pool should be used, the data 
       descriptors or the user data objects must first be added to
       the plot pool. The returned plot descriptors can then be used 
       together with this routine to display the plot.
 
       @memo Multi plot routine (list of plot descriptors).
       @param plist Plot descriptor list
       @param plotype String describing the type of plot to be used
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   TLGMultiPad* Plot (PlotList& plist, const char* plottype = 0);
//@}


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// BodePlot                                                             //
//                                                                      //
// Bode plot routines                                                   //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

/** @name BodePlot
    These are the (overloaded) Bode plot routines.

    @memo Bode plot routines
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{
   /** The plot will use the specified graph type and channel names,
       if they are supplied. If not it will query the user data object.
       The plot type is Bode plot ("Transfer function").

       A plot descriptor is automatically generated and added to the 
       global pool.
 
       @memo Bode plot routine (user data object).
       @param data User data object which derives from AttDataDescriptor
       @param graphtype Graph type used by plot
       @param Achn A channel name used by plot
       @param Bchn B channel name used by plot
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   TLGMultiPad* BodePlot (const AttDataDescriptor& data, 
                     const char* graphtype = 0, 
                     const char* Achn = 0, const char* Bchn = 0);

   /** The plot uses the specified graph type and channel names.
       The plot type is Bode plot ("Transfer function").

       The data descriptor is adopted by the routine.

       A plot descriptor is automatically generated and added to the 
       global pool. 
 
       @memo Bode plot routine (data descriptor).
       @param plotype String describing the type of plot to be used
       @param data Data descriptor
       @param graphtype Graph type used by plot
       @param Achn A channel name used by plot
       @param Bchn B channel name used by plot
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   TLGMultiPad* BodePlot (BasicDataDescriptor* data, 
                     const char* graphtype, 
                     const char* Achn, const char* Bchn = 0);

   /** The plot type is Bode plot ("Transfer function").

       The plot descriptor is added to the global plot pool if
       it isn't owned by another pool. The plot pool will adopt 
       the plot descriptor.

       If a user supplied plot pool should be used, the data 
       descriptor or the user data object must first be added to
       the plot pool. The returned plot descriptor can then be used 
       together with this routine to display the plot.
 
       @memo Bode plot routine (plot descriptor).
       @param data Plot descriptor
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   TLGMultiPad* BodePlot (PlotDescriptor* data);

   /** Displays multiple curves in the same Bode plot.

       The plot type is deduced from the graph type of the first
       plot descriptor in the list if not specified. 
       Possible values are specified in TLGCore.h.

       The plot descriptors are added to the global plot pool if
       aren't owned by another pool. The plot pool will adopt 
       the plot descriptors.

       If a user supplied plot pool should be used, the data 
       descriptors or the user data objects must first be added to
       the plot pool. The returned plot descriptors can then be used 
       together with this routine to display the plot.
 
       @memo Multi Bode plot routine (list of plot descriptors).
       @param plist Plot descriptor list
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   TLGMultiPad* BodePlot (PlotList& plist);
//@}


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TSPlot                                                               //
//                                                                      //
// Time series plot routines                                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

/** @name TSPlot
    These are the (overloaded) time series plotting routines.

    @memo Time series plotting routines
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{
   /** The plot will use the specified graph type and channel names,
       if they are supplied. If not it will query the user data object.
       The plot type is "Time series".

       A plot descriptor is automatically generated and added to the 
       global pool.
 
       @memo Time series plot routine (user data object).
       @param data User data object which derives from AttDataDescriptor
       @param graphtype Graph type used by plot
       @param Achn A channel name used by plot
       @param Bchn B channel name used by plot
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   TLGMultiPad* TSPlot (const AttDataDescriptor& data, 
                     const char* graphtype = 0, 
                     const char* Achn = 0, const char* Bchn = 0);

   /** The plot uses the specified graph type and channel names.
       The plot type is "Time series".

       The data descriptor is adopted by the routine.

       A plot descriptor is automatically generated and added to the 
       global pool. 
 
       @memo Time series plot routine (data descriptor).
       @param plotype String describing the type of plot to be used
       @param data Data descriptor
       @param graphtype Graph type used by plot
       @param Achn A channel name used by plot
       @param Bchn B channel name used by plot
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   TLGMultiPad* TSPlot (BasicDataDescriptor* data, 
                     const char* graphtype, 
                     const char* Achn, const char* Bchn = 0);

   /** The plot type is "Time series".

       The plot descriptor is added to the global plot pool if
       it isn't owned by another pool. The plot pool will adopt 
       the plot descriptor.

       If a user supplied plot pool should be used, the data 
       descriptor or the user data object must first be added to
       the plot pool. The returned plot descriptor can then be used 
       together with this routine to display the plot.
 
       @memo Time series plot routine (plot descriptor).
       @param data Plot descriptor
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   TLGMultiPad* TSPlot (PlotDescriptor* data);

   /** Displays multiple curves in the same Time series plot.

       The plot type is deduced from the graph type of the first
       plot descriptor in the list if not specified. 
       Possible values are specified in TLGCore.h.

       The plot descriptors are added to the global plot pool if
       aren't owned by another pool. The plot pool will adopt 
       the plot descriptors.

       If a user supplied plot pool should be used, the data 
       descriptors or the user data objects must first be added to
       the plot pool. The returned plot descriptors can then be used 
       together with this routine to display the plot.
 
       @memo Time series multi plot routine (list of plot descriptors).
       @param plist Plot descriptor list
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   TLGMultiPad* TSPlot (PlotList& plist);
//@}


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// PSPlot                                                               //
//                                                                      //
// Power spectrum plot routines                                         //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

/** @name PSPlot
    These are the (overloaded) power spectrum plotting routines.

    @memo Power spectrum plotting routines
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{
   /** The plot will use the specified graph type and channel names,
       if they are supplied. If not it will query the user data object.
       The plot type is "Power spectrum".

       A plot descriptor is automatically generated and added to the 
       global pool.
 
       @memo Power spectrum plot routine (user data object).
       @param data User data object which derives from AttDataDescriptor
       @param graphtype Graph type used by plot
       @param Achn A channel name used by plot
       @param Bchn B channel name used by plot
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   TLGMultiPad* PSPlot (const AttDataDescriptor& data, 
                     const char* graphtype = 0, 
                     const char* Achn = 0, const char* Bchn = 0);

   /** The plot uses the specified graph type and channel names.
       The plot type is "Power spectrum".

       The data descriptor is adopted by the routine.

       A plot descriptor is automatically generated and added to the 
       global pool. 
 
       @memo Power spectrum plot routine (data descriptor).
       @param plotype String describing the type of plot to be used
       @param data Data descriptor
       @param graphtype Graph type used by plot
       @param Achn A channel name used by plot
       @param Bchn B channel name used by plot
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   TLGMultiPad* PSPlot (BasicDataDescriptor* data, 
                     const char* graphtype, 
                     const char* Achn, const char* Bchn = 0);

   /** The plot type is "Power spectrum".

       The plot descriptor is added to the global plot pool if
       it isn't owned by another pool. The plot pool will adopt 
       the plot descriptor.

       If a user supplied plot pool should be used, the data 
       descriptor or the user data object must first be added to
       the plot pool. The returned plot descriptor can then be used 
       together with this routine to display the plot.
 
       @memo Power spectrum plot routine (plot descriptor).
       @param data Plot descriptor
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   TLGMultiPad* PSPlot (PlotDescriptor* data);

   /** Displays multiple curves in the same power spectrum plot.

       The plot type is deduced from the graph type of the first
       plot descriptor in the list if not specified. 
       Possible values are specified in TLGCore.h.

       The plot descriptors are added to the global plot pool if
       aren't owned by another pool. The plot pool will adopt 
       the plot descriptors.

       If a user supplied plot pool should be used, the data 
       descriptors or the user data objects must first be added to
       the plot pool. The returned plot descriptors can then be used 
       together with this routine to display the plot.
 
       @memo Power spectrum multi plot routine (list of plot descriptors).
       @param plist Plot descriptor list
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   TLGMultiPad* PSPlot (PlotList& plist);
//@}


//@}
}


#endif // _LIGO_TLGPLOT_H

