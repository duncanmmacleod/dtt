/* Version $Id: TLGPlot.cc 6319 2010-09-17 18:06:52Z james.batch@LIGO.ORG $ */

#include "PlotSet.hh"
#include "TLGPlot.hh"
#include "TLGPad.hh"
#include "TLGPrint.hh"
#include "TLGExport.hh"
#include "TLGMath.hh"
#include "Table.hh"
#include <iostream>
#include <string.h>


namespace ligogui {
   using namespace std;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// gPlots                                                               //
//                                                                      //
// Global set of plots                                                  //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   PlotSet* gPlots = 0;
   OptionAll_t** gOptions = 0;
   Int_t gMaxOptions = 100;
   TLGPrintParam* gDefPrint = 0;
   ImportOption_t* gDefImport = 0;
   ExportOption_t* gDefExport = 0;
   ReferenceTraceList_t* gDefReferences = 0;
   MathTable_t* gDefMath = 0;
   calibration::Table* gDefCalTable = 0;
   int gDefaultPadNum = 1;


//______________________________________________________________________________
   PlotSet& gPlotSet() 
   {
      if (!gPlots) {
         gPlots = new (nothrow) PlotSet;
         gOptions = new (nothrow) OptionAll_t*[gMaxOptions];
         for (int i = 0; gOptions && (i < gMaxOptions); i++) {
            gOptions[i] = 0;
         }
         gDefPrint = new (nothrow) TLGPrintParam;
         gDefImport = new (nothrow) ImportOption_t;
         SetDefaultImportOptions (*gDefImport);
         gDefExport = new (nothrow) ExportOption_t;
         SetDefaultExportOptions (*gDefExport);
         gDefReferences = new (nothrow) ReferenceTraceList_t;
         SetDefaultReferenceTraces (*gDefReferences);
         gDefMath = new (nothrow) MathTable_t;
         SetDefaultMathTable (*gDefMath);
         gDefCalTable = new (nothrow) calibration::Table;
         SetDefaultTable (*gDefCalTable);
      }
      return *gPlots;
   }

//______________________________________________________________________________
   void ResetPlots()
   {
      if (gPlots) {
         delete gPlots;
         gPlots= 0;
      }
   }

//______________________________________________________________________________
   int& gDefaultPadNumber() 
   {
      return gDefaultPadNum;
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Plot                                                                 //
//                                                                      //
// Main plot routine                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGMultiPad* Plot ()
   {
      // bring up a new plot window
      // create new pad main window
      TLGPadMain* win = new TLGPadMain (gClient->GetRoot(), gPlotSet(), 
                           kTRUE);
      if (win == 0) {
         return 0;
      }
      // setup pads
      TLGMultiPad* pads = win->GetPads();
      pads->SetStoreOptionList (gOptions, gMaxOptions);
      pads->SetPadLayoutAndNumber (gDefaultPadNumber());
      pads->SetDefPrintSetup (gDefPrint);
      pads->SetDefImportOpt (gDefImport);
      pads->SetDefExportOpt (gDefExport);
      pads->SetReferenceTraces (gDefReferences);
      pads->SetMathTable (gDefMath);
      pads->SetCalibrationTable (gDefCalTable);
      pads->ResetPads();
   
      return pads;
   }

//______________________________________________________________________________
   TLGMultiPad* Plot (const AttDataDescriptor& data, 
                     const char* graphtype, 
                     const char* Achn, const char* Bchn)
   {
      // add plot to pool
      PlotDescriptor* plotd = gPlotSet().Add (data, graphtype, Achn, Bchn);
      if (plotd == 0) {
         return 0;
      }
      return Plot (plotd, plotd->GetGraphType());
   }

//______________________________________________________________________________
   TLGMultiPad* Plot (const char* plottype, 
                     const AttDataDescriptor& data, 
                     const char* graphtype, 
                     const char* Achn, const char* Bchn)
   {
      // add plot to pool
      PlotDescriptor* plotd = gPlotSet().Add (data, graphtype, Achn, Bchn);
      if (plotd == 0) {
         return 0;
      }
      return Plot (plotd, plottype);
   }

//______________________________________________________________________________
   TLGMultiPad* Plot (BasicDataDescriptor* data, const char* graphtype, 
                     const char* Achn, const char* Bchn)
   {
      // add plot to pool
      PlotDescriptor* plotd = gPlotSet().Add (data, graphtype, Achn, Bchn);
      if (plotd == 0) {
         return 0;
      }
      return Plot (plotd, plotd->GetGraphType());
   }

//______________________________________________________________________________
   TLGMultiPad* Plot (const char* plottype, 
                     BasicDataDescriptor* data, 
                     const char* graphtype, 
                     const char* Achn, const char* Bchn)
   {
      // add plot to pool
      PlotDescriptor* plotd = gPlotSet().Add (data, graphtype, Achn, Bchn);
      if (plotd == 0) {
         return 0;
      }
      return Plot (plotd, plottype);
   }

//______________________________________________________________________________
   TLGMultiPad* Plot (PlotDescriptor* data, const char* plottype)
   {
      if (data == 0) {
         return 0;
      }
      // check plot descriptor owner
      if (!data->HasOwner()) {
         if (gPlotSet().Add (data) == 0) {
            return 0;
         }
      }
      // get plot set
      PlotSet*	pset = data->GetOwner();
      // create plot name
      TString 	name = data->GetGraphType() + TString (": ");
      if (data->GetBChannel() != 0) {
         name += data->GetBChannel() + TString ("/");
      }
      name += data->GetAChannel();
      // create new pad main window
      TLGPadMain* win = new TLGPadMain (gClient->GetRoot(), *pset, kTRUE, name);
      if (win == 0) {
         pset->Remove (data);
         return 0;
      }
      // setup pads
      gPlotSet();
      TLGMultiPad* pads = win->GetPads();
      pads->SetStoreOptionList (gOptions, gMaxOptions);
      pads->SetPadLayoutAndNumber (gDefaultPadNumber());
      pads->SetDefPrintSetup (gDefPrint);
      pads->SetDefImportOpt (gDefImport);
      pads->SetDefExportOpt (gDefExport);
      pads->SetReferenceTraces (gDefReferences);
      pads->SetMathTable (gDefMath);
      pads->SetCalibrationTable (gDefCalTable);
      pads->ShowPlot (data, plottype, 0);
      if (gDefaultPadNumber() > 1) {
         pads->ShowPlot (data, plottype, 1, 1);
      }
      return pads;
   }

//______________________________________________________________________________
   TLGMultiPad* Plot (const char* plottype, PlotDescriptor* data)
   {
      return Plot (data, plottype);
   }

//______________________________________________________________________________
   TLGMultiPad* Plot (PlotList& plist, const char* plottype)
   {
      if (plist.Size() == 0) {
         return 0;
      }
      // check plot descriptor owner
      PlotSet* pset;
      if (plist(0)->HasOwner()) {
         pset = plist(0)->GetOwner();
      }
      else {
         pset = &gPlotSet();
         if (pset->Add (plist(0)) == 0) {
            plist.Remove (0);
            return 0;
         }
      }
      for (int i = 1; i < plist.Size(); i++) {
         // add to plot set if no owner
         if (!plist(i)->HasOwner()) {
            if (pset->Add (plist(i)) == 0) {
               plist.Remove (i);
               return 0;
            }
         }
         // make sure owners are identical
         else if (plist(0)->GetOwner() != pset) {
            return 0;
         }
      }
   
      // create plot name
      TString 	name = plist(0)->GetGraphType();
      // create new pad main window
      TLGPadMain* win = new TLGPadMain (gClient->GetRoot(), *pset, 
                           kTRUE, name);
      if (win == 0) {
         return 0;
      }
      // setup pads
      gPlotSet();
      TLGMultiPad* pads = win->GetPads();
      pads->SetStoreOptionList (gOptions, gMaxOptions);
      pads->SetPadLayoutAndNumber (gDefaultPadNumber());
      pads->SetDefPrintSetup (gDefPrint);
      pads->SetDefImportOpt (gDefImport);
      pads->SetDefExportOpt (gDefExport);
      pads->SetReferenceTraces (gDefReferences);
      pads->SetMathTable (gDefMath);
      pads->SetCalibrationTable (gDefCalTable);
      const PlotDescriptor** pl = (const PlotDescriptor**) plist.fList;
      pads->ShowMultiPlot (pl, plist.Size(), plottype, 0);
   
      return pads;
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// BodePlot                                                             //
//                                                                      //
// Bode plot routine                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGMultiPad* BodePlot (const AttDataDescriptor& data, 
                     const char* graphtype, 
                     const char* Achn, const char* Bchn)
   {
      int oldpadnum = gDefaultPadNumber();
      if (gDefaultPadNumber() <= 1) gDefaultPadNumber() = 2;
      TLGMultiPad* pad = 
         Plot (kPTTransferFunction, data, graphtype, Achn, Bchn);
      gDefaultPadNumber() = oldpadnum;
      return pad;
   }

//______________________________________________________________________________
   TLGMultiPad* BodePlot (BasicDataDescriptor* data, 
                     const char* graphtype, 
                     const char* Achn, const char* Bchn)
   {
      int oldpadnum = gDefaultPadNumber();
      if (gDefaultPadNumber() <= 1) gDefaultPadNumber() = 2;
      TLGMultiPad* pad =  
         Plot (kPTTransferFunction, data, graphtype, Achn, Bchn);
      gDefaultPadNumber() = oldpadnum;
      return pad;
   }

//______________________________________________________________________________
   TLGMultiPad* BodePlot (PlotDescriptor* data)
   {
      int oldpadnum = gDefaultPadNumber();
      if (gDefaultPadNumber() <= 1) gDefaultPadNumber() = 2;
      TLGMultiPad* pad = Plot (kPTTransferFunction, data);
      gDefaultPadNumber() = oldpadnum;
      return pad;
   }

//______________________________________________________________________________
   TLGMultiPad* BodePlot (PlotList& plist)
   {
      int oldpadnum = gDefaultPadNumber();
      if (gDefaultPadNumber() <= 1) gDefaultPadNumber() = 2;
      TLGMultiPad* pad = Plot (plist, kPTTransferFunction);
      gDefaultPadNumber() = oldpadnum;
      return pad;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TSPlot                                                               //
//                                                                      //
// Time series plot routine                                             //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGMultiPad* TSPlot (const AttDataDescriptor& data, 
                     const char* graphtype, 
                     const char* Achn, const char* Bchn)
   {
      return Plot (kPTTimeSeries, data, graphtype, Achn, Bchn);
   }

//______________________________________________________________________________
   TLGMultiPad* TSPlot (BasicDataDescriptor* data, 
                     const char* graphtype, 
                     const char* Achn, const char* Bchn)
   {
      return Plot (kPTTimeSeries, data, graphtype, Achn, Bchn);
   }

//______________________________________________________________________________
   TLGMultiPad* TSPlot (PlotDescriptor* data)
   {
      return Plot (kPTTimeSeries, data);
   }

//______________________________________________________________________________
   TLGMultiPad* TSPlot (PlotList& plist)
   {
      return Plot (plist, kPTTimeSeries);
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// PSPlot                                                               //
//                                                                      //
// Power spectrum plot routine                                          //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGMultiPad* PSPlot (const AttDataDescriptor& data, 
                     const char* graphtype, 
                     const char* Achn, const char* Bchn)
   {
      return Plot (kPTPowerSpectrum, data, graphtype, Achn, Bchn);
   }

//______________________________________________________________________________
   TLGMultiPad* PSPlot (BasicDataDescriptor* data, 
                     const char* graphtype, 
                     const char* Achn, const char* Bchn)
   {
      return Plot (kPTPowerSpectrum, data, graphtype, Achn, Bchn);
   }

//______________________________________________________________________________
   TLGMultiPad* PSPlot (PlotDescriptor* data)
   {
      return Plot (kPTPowerSpectrum, data);
   }

//______________________________________________________________________________
   TLGMultiPad* PSPlot (PlotList& plist)
   {
      return Plot (plist, kPTPowerSpectrum);
   }

}
