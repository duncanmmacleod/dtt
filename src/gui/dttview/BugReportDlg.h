//
// Created by erik.vonreis on 11/25/19.
//

#ifndef DTT_CDS_BUGREPORTDLG_H
#define DTT_CDS_BUGREPORTDLG_H

#include <string>
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGButton.h>
#include <TGListBox.h>
#include <TGComboBox.h>
#include <TGTextEntry.h>
#include <TGTextView.h>
#include <TGTableLayout.h>
#include "TGFrame.h"
#include <memory>


using namespace std;

namespace ligogui
{
    /** dialog box opened up when the user goes to Help -> Report Bugs
     *
     */
    class BugReportDlg : public TGTransientFrame
    {
    protected:
        //where do we go to report a bug?
        string report_url, report_email;



        unique_ptr<TGVerticalFrame> fMainGroup;
        //introduction
        unique_ptr<TGLabel> fHeaderLabel;

        unique_ptr<TGHorizontalFrame> fURLFrame;
        //copyable view of the bug report URL
        unique_ptr<TGTextView> fBugReportURL;

        //try to open the URL in a browser
        unique_ptr<TGTextButton> fOpenInBrowser;
        //shows status update when open in browser is clicked.
        unique_ptr<TGTextView> fOpenBrowserStatus;

        //linker between web and email
        unique_ptr<TGLabel> fMiddleLabel;
        //copyable view of email address
        unique_ptr<TGTextView> fEmailTextView;

        unique_ptr<TGTextButton> fClose;

        void Setup();

        void OpenInBrowser();

    public:
        BugReportDlg(const TGWindow* p, const TGWindow* main, string report_url, string report_email);

        virtual ~BugReportDlg();

        virtual void CloseWindow();

        //main message handler
        virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1,
                                       Long_t parm2);


    };

}
#endif //DTT_CDS_BUGREPORTDLG_H
