/* version $Id: TLGOptions.hh 6737 2012-09-21 22:27:25Z james.batch@LIGO.ORG $ */
#ifndef _LIGO_TLGOPTIONS_H
#define _LIGO_TLGOPTIONS_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGOptions						*/
/*                                                         		*/
/* Module Description: Options for Graphics:				*/
/*		       plot type, trace selection, axes, cursor,	*/
/*		       legend						*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 29Oct99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGOptions.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include <TGFrame.h>
#include <TGComboBox.h>
#include <TGLabel.h>
#include <TGTextEntry.h>
#include <TGButton.h>
#include <TGTab.h>
#include <TAttLine.h>
#include <TAttMarker.h>
#include <TAttAxis.h>
#include <TAttFill.h>
#include <TAttText.h>
#include "TLGFrame.hh"
#include "TLGMultiTab.hh"
#include "ComponentHolder.h"

   class PlotMap;

namespace ligogui {

   class TLGTextEntry;
   class TLGNumericEntry;
   class TLGNumericControlBox;
   class TLGColorComboBox;
   class TLGMarkerStyleComboBox;
   class TLGLineStyleComboBox;
   class TLGFillStyleComboBox;
   class TLGFontSelection;
   class TLGOptionTab;


/** @name TLGOptions
    This header exports options which are used for plotting and
    defines a graphical user interface to modify them.

    Options for selecting traces, units, axes parameters, cursor
    control and setting the style are managed by tab widget with
    corresponding option tabs. Options are set and read through 
    one large strcuture data type. 
   
    @memo Plotting options
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{
/** @name Constants
    Constants for option parameters, messages and widget IDs.
   
    @memo Option type constants
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{
   /// Name of traces option panel
   const char* const kGOptTracesName = "Traces";
   /// Name of units option panel
   const char* const kGOptRangeName = "Range";
   /// Name of unit option panel
   const char* const kGOptUnitsName = "Units";
   /// Name of cursor option panel
   const char* const kGOptCursorName = "Cursor";
   /// Name of Configuration option panel
   const char* const kGOptConfigName = "Config";
   /// Name of style option panel
   const char* const kGOptStyleName = "Style";
   /// Name of x axis option panel
   const char* const kGOptAxisXName = "X-axis";
   /// Name of y axis option panel
   const char* const kGOptAxisYName = "Y-axis";
   /// Name of legend option panel
   const char* const kGOptLegendName = "Legend";
   /// Name of parameter option panel
   const char* const kGOptParamName = "Param";

   /// Maximum number of traces
   const int kMaxTraces = 8;
   /// Maximum number of option tabs
   const int kMaxTabs = 10;
   /// Width of option panel
   const int kOptionWidth = 440; // JCB
   // JCB const int kOptionWidth = 300;
   /// Height of option panel
   const int kOptionHeight = 353; //280

   /// Option Message 
   const int kC_OPTION = 120;
   /// Submessage: Option has changed
   const int kCM_OPTCHANGED = 1;
   /// Submessage: Option dialogbox has closed
   const int kCM_OPTCLOSE = 2;
   /// Submessage: Option dialogbox requests update
   const int kCM_OPTUPDATE = 3;
   /// Submessage; Request cursor update
   const int kCM_OPTCURSOR = 5;
   /// Submessage; Cursor update notification
   const int kCM_OPTCURSORNEW = 6;
   /// Submessage; Request calibration dialog notification
   const int kCM_CAL = 7;

   /// Widget ID of option panel tab
   const int kGOptTabID = 162;

   /// Widget ID of traces option panel
   const int kGOptTracesID = 180;
   /// Widget ID of units option panel
   const int kGOptRangeID= 181;
   /// Widget ID of units option panel
   const int kGOptUnitsID = 182;
   /// Widget ID of cursor option panel
   const int kGOptCursorID = 183;
   /// Widget ID of configuration option panel
   const int kGOptConfigID = 184;
   /// Widget ID of style option panel
   const int kGOptStyleID = 185;
   /// Widget ID of x axis option panel
   const int kGOptAxisXID= 186;
   /// Widget ID of y axis option panel
   const int kGOptAxisYID= 187;
   /// Widget ID of legend option panel
   const int kGOptLegendID = 188;
   /// Widget ID of parameter option panel
   const int kGOptParamID = 189;

   /// Widget ID of graph type
   const int kGOptTraceGraphType= 250;
   /// Widget IDs of trace acvtive option
   const int kGOptTraceActive = 251; // 251 - 258
   /// Widget IDs of A channel selection option
   const int kGOptTraceAChannel = kGOptTraceActive + kMaxTraces; // 259 - 266
   /// Widget IDs of B channel selection option
   const int kGOptTraceBChannel = kGOptTraceAChannel + kMaxTraces; // 267 - 274
   /// Widget IDs of line checkbox
   const int kGOptTraceLine = kGOptTraceBChannel + kMaxTraces; // 275 - 282
   /// Widget IDs of line color
   const int kGOptTraceLineColor = kGOptTraceLine + kMaxTraces; // 283 - 290
   /// Widget IDs of line style
   const int kGOptTraceLineStyle = kGOptTraceLineColor + kMaxTraces; // 291 - 298
   /// Widget IDs of line size
   const int kGOptTraceLineSize = kGOptTraceLineStyle + kMaxTraces; // 299 - 306
   /// Widget IDs of marker checkbox
   const int kGOptTraceMarker = kGOptTraceLineSize + kMaxTraces; // 307 - 314
   /// Widget IDs of marker color
   const int kGOptTraceMarkerColor = kGOptTraceMarker + kMaxTraces; // 315 - 322
   /// Widget IDs of marker style
   const int kGOptTraceMarkerStyle = kGOptTraceMarkerColor + kMaxTraces; // 323 - 330
   /// Widget IDs of marker style
   const int kGOptTraceMarkerSize = kGOptTraceMarkerStyle + kMaxTraces; // 331 - 338
   /// Widget IDs of marker checkbox
   const int kGOptTraceBar = kGOptTraceMarkerSize + kMaxTraces; // 339 - 346
   /// Widget IDs of marker color
   const int kGOptTraceBarColor = kGOptTraceBar + kMaxTraces; // 347 - 354
   /// Widget IDs of marker style
   const int kGOptTraceBarStyle = kGOptTraceBarColor + kMaxTraces; // 355 - 362
   /// Widget IDs of marker style
   const int kGOptTraceBarSize = kGOptTraceBarStyle + kMaxTraces; // 363 - 370

   /// Widget ID of X range
   const int kGOptRangeX = 450;
   /// Widget ID of X linear axis
   const int kGOptRangeXLinear = 451;
   /// Widget ID of X log axis
   const int kGOptRangeXLog = 452;
   /// Widget ID of X range automatic
   const int kGOptRangeXAutomatic = 453;
   /// Widget ID of X range manual
   const int kGOptRangeXManual = 454;
   /// Widget ID of X range from
   const int kGOptRangeXFrom = 455;
   /// Widget ID of X range to
   const int kGOptRangeXTo = 456;
   /// Widget ID of Y range
   const int kGOptRangeY = 460;
   /// Widget ID of Y linear axis
   const int kGOptRangeYLinear = 461;
   /// Widget ID of Y log axis
   const int kGOptRangeYLog = 462;
   /// Widget ID of Y range automatic
   const int kGOptRangeYAutomatic = 463;
   /// Widget ID of Y range manual
   const int kGOptRangeYManual = 464;
   /// Widget ID of Y range from
   const int kGOptRangeYFrom = 465;
   /// Widget ID of Y range to
   const int kGOptRangeYTo = 466;
   /// Widget ID of bin
   const int kGOptRangeBin = 467;
   /// Widget ID of bin log spacing
   const int kGOptRangeBinLogSpacing = 468;

   /// Widget ID of x axis title
   const int kGOptAxisXTitle = 500;
   /// Widget ID of x axis title centering
   const int kGOptAxisXTitleCenter = 501;
   /// Widget ID of x axis title size
   const int kGOptAxisXTitleSize = 502;
   /// Widget ID of x axis title offset
   const int kGOptAxisXTitleOfs = 503;
   /// Widget ID of x axis ticks length
   const int kGOptAxisXTicksLen = 504;
   /// Widget ID of x axis ticks ob both sides
   const int kGOptAxisXTicksBoth = 505;
   /// Widget ID of x axis ticks divisions
   const int kGOptAxisXTicksDiv = 506;
   /// Widget ID of x axis axis and ticks color
   const int kGOptAxisXAxisColor = 509;
   /// Widget ID of x axis labels size
   const int kGOptAxisXLabelsSize = 510;
   /// Widget ID of x axis title
   const int kGOptAxisXLabelsOfs = 511;
   /// Widget ID of x axis labels color
   const int kGOptAxisXLabelsColor = 512;
   /// Widget ID of x axis grid
   const int kGOptAxisXGrid = 513;
   /// Widget ID of x axis font
   const int kGOptAxisXFont = 514;
   /// Widget ID of x axis title color
   const int kGOptAxisXTitleColor = 515;

   /// Widget ID of y axis title
   const int kGOptAxisYTitle = 550;
   /// Widget ID of y axis title centering
   const int kGOptAxisYTitleCenter = 551;
   /// Widget ID of y axis title size
   const int kGOptAxisYTitleSize = 552;
   /// Widget ID of y axis title offset
   const int kGOptAxisYTitleOfs = 553;
   /// Widget ID of y axis ticks length
   const int kGOptAxisYTicksLen = 554;
   /// Widget ID of y axis ticks ob both sides
   const int kGOptAxisYTicksBoth = 555;
   /// Widget ID of y axis ticks divisions
   const int kGOptAxisYTicksDiv = 556;
   /// Widget ID of y axis axis and ticks color
   const int kGOptAxisYAxisColor = 559;
   /// Widget ID of y axis labels size
   const int kGOptAxisYLabelsSize = 560;
   /// Widget ID of y axis title
   const int kGOptAxisYLabelsOfs = 561;
   /// Widget ID of y axis labels color
   const int kGOptAxisYLabelsColor = 562;
   /// Widget ID of y axis grid
   const int kGOptAxisYGrid = 563;
   /// Widget ID of y axis font
   const int kGOptAxisYFont = 564;
   /// Widget ID of y axis title color
   const int kGOptAxisYTitleColor = 565;

   /// Widget ID of cursor active
   const int kGOptCursorActive = 600;
   /// Widget ID of cursor active
   const int kGOptCursorStyle = 602;
   /// Widget ID of cursor active
   const int kGOptCursorType = 606;
   /// Widget ID of cursor X
   const int kGOptCursorX = 608;
   /// Widget ID of cursor Y
   const int kGOptCursorY = 610;
   /// Widget ID of cursor statitics
   const int kGOptCursorStatistics = 612;
   /// Widget ID of cursor statistics value
   const int kGOptCursorVal = 613;

   /// Widget ID of style title
   const int kGOptStyleTitle = 650;
   /// Widget ID of style title color
   const int kGOptStyleTitleColor = 651;
   /// Widget ID of style title font
   const int kGOptStyleTitleFont = 652;
   /// Widget ID of style title justification
   const int kGOptStyleTitleAdjust = 653;
   /// Widget ID of style margins
   const int kGOptStyleMargin = 656;

   /// Widget ID of automatic plot configuration
   const int kGOptConfigAutoConf = 700;
   /// Widget ID of respect user selection
   const int kGOptConfigRespectUser = 701;
   /// Widget ID of automatic axes configuration
   const int kGOptConfigAutoAxes = 702;
   /// Widget ID of automatic binning
   const int kGOptConfigAutoBin = 703;
   /// Widget ID of automatic binning
   const int kGOptConfigAutoTimeAdjust = 704;
   /// Widget ID of save configuration
   const int kGOptConfigSave = 705;
   /// Widget ID of restore configuration
   const int kGOptConfigRestore = 706;

   /// Widget ID of X values
   const int kGOptUnitXValues = 750;
   /// Widget ID of Y values
   const int kGOptUnitYValues = 751;
   /// Widget ID of X units
   const int kGOptUnitXUnits = 752;
   /// Widget ID of Y units
   const int kGOptUnitYUnits = 753;
   /// Widget ID of X units magnitude
   const int kGOptUnitXMag = 754;
   /// Widget ID of Y units magnitude
   const int kGOptUnitYMag = 755;
   /// Widget ID of X slope
   const int kGOptUnitXSlope = 756;
   /// Widget ID of X offset
   const int kGOptUnitXOffset = 757;
   /// Widget ID of Y slope
   const int kGOptUnitYSlope = 758;
   /// Widget ID of Y offset
   const int kGOptUnitYOffset = 759;
   /// Widget ID of calibration
   const int kGOptUnitCalibration = 760;

   /// Widget ID of legend text
   const int kGOptLegendText = 800;
   /// Widget ID of legend palcement
   const int kGOptLegendPlaceStart = 801;
   const int kGOptLegendPlaceTopRight = 801;
   const int kGOptLegendPlaceBottomRight = 802;
   const int kGOptLegendPlaceBottomLeft = 803;
   const int kGOptLegendPlaceTopLeft = 804;

   /// Widget ID of legend X adjust
   const int kGOptLegendXAdjust = 805;
   /// Widget ID of legend Y adjust
   const int kGOptLegendYAdjust = 806;
   /// Widget ID of legend text size
   const int kGOptLegendTextSize = 807;
   /// Widget ID of legend symbol style
   const int kGOptLegendSymbolStart = 808;
   const int kGOptLegendSymbolSame = 808;
   const int kGOptLegendSymbolNone = 809;
   /// Widget ID of legend text selection
   const int kGOptLegendTextSelStart = 810;
   const int kGOptLegendTextSelAuto = 810;
   const int kGOptLegendTextSelUser = 811;
   /// Widget ID of legend show
   const int kGOptLegendShow = 812;

   /// Widget ID of parameter show
   const int kGOptParamShow = 850;
   /// Widget ID of parameter time format
   const int kGOptParamTimeFormat = 851;
   /// Widget ID of parameter variable
   const int kGOptParamVar = 853;
   /// Widget ID of parameter display all
   const int kGOptParamDisplayAll = 858; // hist (mito) 8/8/2002

//@}


/** @name Enumerated types
    Enumerated types used by plot options.
   
    @memo Plot option enumerated types
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{
   /// Plot style enumerated type
   enum EPlotStyle {
   /// Lines only
   kPlotStyleLine = 0,
   /// Markers only
   kPlotStyleMarker = 1,
   /// Lines and markers
   kPlotStyleLineMarker = 2,
   /// Bar chart
   kPlotStyleBar = 3
   };

   /// X Range type
   enum EXRangeType {
   /// Normal: x
   kUnitNormal = 0,
   /// Date format: x -> 00:00:00
   kUnitTime = 1,
   /// Angular frequency format: 2*PI*x
   kUnitAngularF = 2
   };

   /// Y Range type (order must be the same as EDataCopy)
   enum EYRangeType {
   /// Magnitude: y for real and |y| for complex
   kUnitMagnitude = 0,
   /// dB Magnitude: 20 log10(|y|)
   kUnitdBMagnitude = 1,
   /// Real: Re(y)
   kUnitReal = 2,
   /// Imaginary: Im(y)
   kUnitImaginary = 3,
   /// Real or imaginary part (depending on trace number)
   kUnitRealImaginary = 4,
   /// Phase (degree): 180/Pi*ATan(Im(y),Re(y))
   kUnitPhaseDeg = 5,
   /// Phase (rad): ATan(Im(y),Re(y))
   kUnitPhaseRad = 6,
   /// Continuous phase (degree)
   kUnitPhaseDegCont = 7,
   /// Continuous phase (rad)
   kUnitPhaseRadCont = 8
   };

   /// Unit type
   enum EUnitType {
   /// None
   kUnitNone = 0,
   /// List
   kUnitList = 1,
   /// User
   kUnitUser = 2
   };

   /// Axis scaling type
   enum EAxisScale {
   /// linear
   kAxisScaleLinear = 0,
   /// logarithmic
   kAxisScaleLog = 1
   };

   /// Axis ranging type
   enum ERange {
   /// automatic
   kRangeAutomatic = 0,
   /// manual
   kRangeManual = 1
   };

   /// Cursor style
   enum ECursorStyle {
   /// none
   kCursorInvisible = 0,
   /// Cross
   kCursorCross = 1,
   /// Vertical
   kCursorVertical = 2,
   /// Horizontal
   kCursorHorizontal = 3
   };

   /// Cursor type
   enum ECursorType {
   /// Absolute values
   kCursorAbsolute = 0,
   /// Relative values
   kCursorDifference = 1
   };

   /// Legend placement
   enum ELegendPlacement {
   /// top right
   kLegendTopRight = 0,
   /// bottom right
   kLegendBottomRight = 1,
   /// bottom left
   kLegendBottomLeft = 2,
   /// top left
   kLegendTopLeft = 3
   };

   /// Legend symbol style
   enum ELegendStyle {
   /// same symbol as trace
   kLegendSameAsTrace = 0,
   /// no symbols
   kLegendNone = 1
   };

   /// Legend text
   enum ELegendText {
   /// Auto text
   kLegendAutoText = 0,
   /// User text
   kLegendUserText = 1
   };

//@}


/** @name Option structures
    Structures used by plot options.
   
    @memo Plot option structures
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{
   /// Basic option value type
   struct OptionValues_t {
      /// No options
   };

   /// Trace options
   struct OptionTraces_t : public OptionValues_t {
      /// Graph type 
      TString		fGraphType;
      /// Active state of traces
      Bool_t		fActive[kMaxTraces];
      /// Selected A channels
      TString		fAChannel[kMaxTraces];
      /// Selected B channels
      TString		fBChannel[kMaxTraces];
      /// Plot style
      EPlotStyle	fPlotStyle[kMaxTraces];
      /// Line attributes
      TAttLine		fLineAttr[kMaxTraces];
      /// Symbol attributes
      TAttMarker	fMarkerAttr[kMaxTraces];
      /// Bar width
      Float_t		fBarWidth[kMaxTraces];
      /// Fill attributes
      TAttFill		fBarAttr[kMaxTraces];
   };

   /// Range options
   struct OptionRange_t : public OptionValues_t {
      /// Scaling type
      EAxisScale	fAxisScale[2];
      /// Range
      ERange		fRange[2];
      /// Range from
      Double_t		fRangeFrom[2];
      /// Range to
      Double_t		fRangeTo[2];
      /// Binning
      Int_t		fBin;
      /// Bin log spacing
      Bool_t		fBinLogSpacing;
   };

   /// Units control options
   struct OptionUnits_t : public OptionValues_t {
      /// X values
      EXRangeType	fXValues;
      /// Y values
      EYRangeType	fYValues;
      /// X Unit string
      TString		fXUnit;
      /// Y Unit string
      TString		fYUnit;
      /// X magnitude
      Int_t		fXMag;
      /// Y magnitude
      Int_t		fYMag;
      /// X slope
      Float_t		fXSlope;
      /// X offset
      Float_t		fXOffset;
      /// Y slope
      Float_t		fYSlope;
      /// Y offset
      Float_t		fYOffset;
   };

   /// Cursor control options
   struct OptionCursor_t : public OptionValues_t {
      /// Cursors active?
      Bool_t		fActive[2];
      /// Current trace asscoiated with cursor
      Int_t		fTrace;
      /// Cursor style
      ECursorStyle	fStyle;
      /// Cursor type
      ECursorType	fType;
      /// Cursor X values
      Double_t		fX[2];
      /// Cursor horizontal values
      Double_t		fH[2];
      /// Valid cursor trace
      Bool_t		fValid[kMaxTraces];
      /// Cursor Y values
      Double_t		fY[kMaxTraces][2];
      /// Number of points between cursors
      Double_t		fN[kMaxTraces];
      /// X difference between cursors
      Double_t		fXDiff[kMaxTraces];
      /// Y difference between cursors
      Double_t		fYDiff[kMaxTraces];
      /// Mean value between cursors
      Double_t		fMean[kMaxTraces];
      /// RMS value between cursors
      Double_t		fRMS[kMaxTraces];
      /// Standard deviation between cursors
      Double_t		fStdDev[kMaxTraces];
      /// Sum between cursors
      Double_t		fSum[kMaxTraces];
      /// Square sum between cursors
      Double_t		fSqrSum[kMaxTraces];
      /// Area between cursors (includes last point!)
      Double_t		fArea[kMaxTraces];
      /// RMS area between cursors (includes last point!)
      Double_t		fRMSArea[kMaxTraces];
      /// Peak position between cursors
      Double_t		fPeakX[kMaxTraces];
      /// Peak value between cursors
      Double_t		fPeakY[kMaxTraces];
      /// Center value (Mean of X direction) between cursors
      Double_t          fCenter[kMaxTraces];
      /// Width (standard deviation in X direction) between cursors
      Double_t          fWidth[kMaxTraces];
   };

   /// Configuration control options
   struct OptionConfig_t : public OptionValues_t {
      /// Reconfigure plot automatically when graph type changes
      Bool_t		fAutoConf;
      /// Respect user selection when trying to reconfigure
      Bool_t		fRespectUser;
      /// Reconfigure axes automatically when units change
      Bool_t		fAutoAxes;
      /// Automatic binning if number of points larger than 1000
      Bool_t		fAutoBin;
      /// Automatic time adjust for time trace with unequal start time
      Bool_t		fAutoTimeAdjust;
   };

   /// Style options
   struct OptionStyle_t : public OptionValues_t {
      /// Plot title
      TString		fTitle;
      /// Plot title attributes
      TAttText		fTitleAttr;
      /// Pad margin: left, right, top, bottom
      Float_t		fMargin[4];
   };

   /// Axis options
   struct OptionAxis_t : public OptionValues_t {
      /// Axis title
      TString		fAxisTitle;
      /// Axis attributes
      TAttAxis		fAxisAttr;
      /// Grid on/off
      Bool_t 		fGrid;
      /// Axes on both side of the frame
      Bool_t		fBothSides;
      /// Center title
      Bool_t		fCenterTitle;
   };

   /// Legend control options
   struct OptionLegend_t : public OptionValues_t {
      /// Show?
      Bool_t		fShow;
      /// Placement
      ELegendPlacement	fPlacement;
      /// X adjust
      Float_t		fXAdjust;
      /// Y adjust
      Float_t		fYAdjust;
      /// Symbol style
      ELegendStyle	fSymbolStyle;
      /// Text style
      ELegendText	fTextStyle;
      /// Text size
      Float_t		fSize;
      /// Text
      TString		fText[kMaxTraces];
   };

   /// Parameter control options
   struct OptionParam_t : public OptionValues_t {
      /// Show?
      Bool_t		fShow;
      /// Include start time?
      Bool_t		fT0;
      /// Include number of averages
      Bool_t		fAvg;
      /// Include special
      Bool_t		fSpecial;
      /// Include statistics (histogram only)
      Bool_t      fStat;
   	/// Display under/overflow bins for histograms?
      Bool_t      fUOBins;
      /// Date/time format in UTC?
      Bool_t		fTimeFormatUTC;
      /// text size
      Float_t		fTextSize;
   };


   /// Structure for all options
   struct OptionAll_t {
      /// Name of option set
      TString		fName;
      /// Trace options
      OptionTraces_t	fTraces;
      /// Range options
      OptionRange_t	fRange;
      /// Units options
      OptionUnits_t	fUnits;
      /// Cursor options
      OptionCursor_t	fCursor;
      /// Configuration options
      OptionConfig_t	fConfig;
      /// Style options
      OptionStyle_t	fStyle;
      /// X axis options
      OptionAxis_t	fAxisX;
      /// Y axis options
      OptionAxis_t	fAxisY;
      /// Legend options
      OptionLegend_t	fLegend;
      /// Parameter options
      OptionParam_t	fParam;
   };

   /// Class for managing plot settings of multiple pads and windows
   class OptionArray {
   private:
      OptionArray (const OptionArray&);
      OptionArray& operator= (const OptionArray&);
   
   protected:
      /// Array of options
      OptionAll_t***	fOptions;
      /// Maximum number of windows
      Int_t		fMaxWin;
      /// Maximum number of options per window
      Int_t		fMaxPad;
   
   public:
      /// Option matrix
      typedef OptionAll_t** OptionMatrix_t;
      /// Option array
      typedef OptionAll_t* OptionArray_t;
   
      /** Create an array of  option settings with a maximum of maxwin
          windows and maxpad pads each. */
      OptionArray (Int_t maxwin = 11, Int_t maxpad = 100);
      /// Destructor
      ~OptionArray ();
      /// Get maximum windows
      Int_t GetMaxWin () const {
         return fMaxWin; }
      /// Get maximum pad
      Int_t GetMaxPad () const {
         return fMaxPad; }
      /// Get list of options associated with window index
      OptionAll_t** operator[] (Int_t index) const {
         return fOptions[index]; }
      /// Get option associated with window index1 and pad index2
      OptionAll_t* operator() (Int_t index1, Int_t index2) const {
         return fOptions[index1][index2]; }
   };

//@}



/** @name Option tabs
    Tabs used by the option panel tab widget.
   
    @memo Option tabs
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{

/** Basic option panel tab. For internal use only.
   
    @memo Basic option panel tab.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGOptions : public TGCompositeFrame, public TGWidget {
   
   protected:
      /// Pointer to plot options values
      OptionValues_t*	fOptionValues;
      /// Option name
      TString		fOptionName;
      /// Default width of option panel
      UInt_t		fDefw;
      /// Default height of option panel
      UInt_t		fDefh;
   
   public:
      /// Constructor
      TLGOptions (const TGWindow* p, const char* optname, Int_t id,
                 OptionValues_t* optvals);
      /// Destrcutor
      virtual ~TLGOptions ();
   
      virtual OptionValues_t* GetOptionValues () const {
         return fOptionValues; }
      virtual void SetOptionValues (OptionValues_t* optvals) {
         fOptionValues = optvals; }
      virtual const char* GetOptionName () const {
         return (const char*) fOptionName; }
      virtual void SetOptionName (const char* name) {
         fOptionName = TString (name); }
   
      virtual void UpdateOptions () = 0;
      virtual void ReadOptions () const {
      }
   
      virtual TGDimension GetDefaultSize () const {
         return TGDimension (fDefw, fDefh); }
   
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
      virtual void SendUpdateMessage (Long_t parm1, Long_t parm2 = 0);
      virtual void SendCursorMessage (Long_t parm1, Long_t parm2 = 0);
   };



/** Trace option panel tab. For internal use only.
   
    @memo Trace option panel tab.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGOptionTraces : public TLGOptions {
   
   protected:
      /// list of plots
      const PlotMap*	fPlotList;
      /// Graph type frame
      TGHorizontalFrame* fGTF;
      /// Graph type label
      TGLabel*		fGraphTypeLabel;
      /// Graph type combobox
      TGComboBox*	fGraphType;
      /// Tabs for traces
      TGTab*		fTraces;
      /// Currently selected trace
      Int_t		fCurTrace;
      /// Check box for active state
      TGCheckButton*	fActive;
      /// channel group frame
      TGGroupFrame* 	fCF;
      /// A channel frames
      TGHorizontalFrame* fAF;
      /// A channel labels
      TGLabel*		fAChannelLabel;
      /// A channel selection boxes
      TGComboBox*	fAChannel;
      /// B channel frames
      TGHorizontalFrame* fBF;
      /// B channel labels
      TGLabel*		fBChannelLabel;
      /// B channel selection boxes
      TGComboBox*	fBChannel;
      /// style group frame
      TGGroupFrame* 	fSF;
      /// Line frame
      TGCompositeFrame*	fLF;
      /// Marker frame
      TGCompositeFrame*	fMF;
      /// Bar/Fill frame
      TGCompositeFrame*	fFF;
      /// Line check box
      TGCheckButton*	fLine;
      /// Line color
      TLGColorComboBox* fLineColor;
      /// Line style
      TLGLineStyleComboBox* fLineStyle;
      /// Line thickness
      TLGNumericControlBox* fLineSize;
      /// Marker check box
      TGCheckButton*	fMarker;
      /// Marker color
      TLGColorComboBox*	fMarkerColor;
      /// Marker style
      TLGMarkerStyleComboBox* fMarkerStyle;
      /// Marker size
      TLGNumericControlBox* fMarkerSize;
      /// Bar check box
      TGCheckButton*	fBar;
      /// Bar/Fill color
      TLGColorComboBox*	fBarColor;
      /// Bar/Fill style
      TLGFillStyleComboBox* fBarStyle;
      /// Marker size
      TLGNumericControlBox* fBarSize;
      /// Layout hints for top/left
      TGLayoutHints*	fL1;
      /// Layout hints for top/expandX
      TGLayoutHints*	fL2;
      /// Layout hints for centerY/left
      TGLayoutHints*	fL3;
      /// Layout hints for centerY/expandX
      TGLayoutHints*	fL4;
      /// Layout hints for top/expandX (extended top paddding)
      TGLayoutHints*	fL5;
      /// Layout hints for expandY/expandX (negative paddding)
      TGLayoutHints*	fL6;
      /// Layout hints for top/expandX (neg. x padding)
      TGLayoutHints*	fL7;
      /// Layout hints for centerY/expandX (alt. padding)
      TGLayoutHints*	fL8;
      /// Layout hints for centerY/right (left padding)
      TGLayoutHints*	fL9;
      /// Layout hints for top/expandX (neg. x/neg. bottom padding)
      TGLayoutHints*	fL10;
   
      void BuildPlotType (Int_t level);
   
   public:
      TLGOptionTraces (const TGWindow* p, Int_t id, 
                      OptionTraces_t* optvals, const PlotMap& ptypes);
      virtual ~TLGOptionTraces ();
   
      virtual void UpdateOptions ();
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };



/** Range option panel tab. For internal use only.
   
    @memo Range option panel tab.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGOptionRange : public TLGOptions {
   
   protected:
      /// list of plots
      const PlotMap*	fPlotList;
      /// X/Y group
      TGGroupFrame*	fGroup[2];
      /// Scale group
      TGCompositeFrame*	fF2[2];
      /// Scale label
      TGLabel*		fScaleLabel[2];
      /// Lienar scale
      TGRadioButton*	fScaleLinear[2];
      /// Logarithmic scale
      TGRadioButton*	fScaleLog[2];
      /// Range groups
      TGCompositeFrame*	fF3[2];
      /// Range label
      TGLabel*		fRangeLabel[2];
      /// Automatic range
      TGRadioButton*	fRangeAutomatic[2];
      /// Manual range
      TGRadioButton*	fRangeManual[2];
      /// Range from/to groups
      TGCompositeFrame*	fF4[2];
      /// start range label
      TGLabel*		fRangeFromLabel[2];
      /// start range
      TLGNumericControlBox*	fRangeFrom[2];
      /// stop range label
      TGLabel*		fRangeToLabel[2];
      /// stop range
      TLGNumericControlBox*	fRangeTo[2];
      /// Bin group
      TGGroupFrame*	fGB;
      /// Bin group first line
      TGCompositeFrame*	fFB;
      /// Bin
      TLGNumericControlBox*	fBin;
      /// bin label
      TGLabel*		fBinLabel;
      /// Bin log spacing
      TGCheckButton*	fBinLogSpacing;
   
      /// Layout hints for top/left
      TGLayoutHints*	fL1;
      /// Layout hints for top/expandX
      TGLayoutHints*	fL2;
      /// Layout hints for centerY/left
      TGLayoutHints*	fL3;
      /// Layout hints for centerY/expandX
      TGLayoutHints*	fL4;
      /// Layout hints for top/expandX (extended top paddding)
      TGLayoutHints*	fL5;
      /// Layout hints for expandY/expandX (negative paddding)
      TGLayoutHints*	fL6;
      /// Layout hints for top/expandX (no x padding)
      TGLayoutHints*	fL7;
      /// Layout hints for centerY/expandX (alt. padding)
      TGLayoutHints*	fL8;
      /// Layout hints for centerY/expandX
      TGLayoutHints*	fL9;
   
   public:
      TLGOptionRange (const TGWindow* p, Int_t id, 
                     OptionRange_t* optvals, const PlotMap& ptypes);
      virtual ~TLGOptionRange ();
   
      virtual void UpdateOptions ();
   
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };



/** Units option panel tab. For internal use only.
   
    @memo Units option panel tab.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGOptionUnits : public TLGOptions {
   protected:
      /// Parent tab
      TLGOptionTab* 	fTab;
      /// List of x units
      TList*		fXUnitList;
      /// List of y units
      TList*		fYUnitList;
   
      /// Display group
      TGGroupFrame*	fG1;
      /// Units group
      TGGroupFrame*	fG2;
      /// Scaling group
      TGGroupFrame*	fG3;
      /// Button group
      TGCompositeFrame*	fG4;
      /// Display first line
      TGCompositeFrame*	fF1;
      /// Display second line
      TGCompositeFrame*	fF2;
      /// Units first line
      TGCompositeFrame*	fF3;
      /// Units second line
      TGCompositeFrame*	fF4;
      /// Scaling first line
      TGCompositeFrame*	fF5;
      /// Scaling second line
      TGCompositeFrame*	fF6;
      /// X values label
      TGLabel*		fXValuesLabel;
      /// Y values label
      TGLabel*		fYValuesLabel;
      /// X values selection
      TGComboBox*	fXValues;
      /// Y values selection
      TGComboBox*	fYValues;
      /// X units label
      TGLabel*		fXUnitLabel;
      /// Y units label
      TGLabel*		fYUnitLabel;
      /// X unit selection
      TGComboBox*	fXUnit;
      /// X unit magnitude selection
      TGComboBox*	fXMag;
      /// Y unit selection
      TGComboBox*	fYUnit;
      /// Y unit magnitude selection
      TGComboBox*	fYMag;
      /// X slope label
      TGLabel*		fXSlopeLabel;
      /// X slope
      TLGNumericControlBox* fXSlope;
      /// X offset label
      TGLabel*		fXOffsetLabel;
      /// X offset
      TLGNumericControlBox* fXOffset;
      /// Y slope label
      TGLabel*		fYSlopeLabel;
      /// Y slope
      TLGNumericControlBox* fYSlope;
      /// Y offset label
      TGLabel*		fYOffsetLabel;
      /// Y offset
      TLGNumericControlBox* fYOffset;
      /// Calibration button
      TGButton*		fCalibration;
      // 
      //    /// Y unit type selection
      // TGButton*		fYUnitTypeSel[3];
      //    /// Y unit selection
      // TGComboBox*	fYUnit;
      //    /// Y mag label
      // TGLabel*		fYMagLabel;
      //    /// Y unit magnitude selection
      // TGComboBox*	fYMag;
      //    /// Y unit user string
      // TLGTextEntry*	fYUnitUser;
      //    /// Y slope label
      // TGLabel*		fYSlopeLabel;
      //    /// Y slope
      // TLGNumericControlBox* fYSlope;
      //    /// Y offset label
      // TGLabel*		fYOffsetLabel;
      //    /// Y offset
      // TLGNumericControlBox* fYOffset;
      //    /// X slope label
      // TGLabel*		fXSlopeLabel;
      //    /// X slope
      // TLGNumericControlBox* fXSlope;
      //    /// X offset label
      // TGLabel*		fXOffsetLabel;
      //    /// X offset
      // TLGNumericControlBox* fXOffset;
      /// Layout hints for top/left
      TGLayoutHints*	fL1;
      /// Layout hints for top/expandX
      TGLayoutHints*	fL2;
      /// Layout hints for centerY/left
      TGLayoutHints*	fL3;
      /// Layout hints for centerY/expandX
      TGLayoutHints*	fL4;
      /// Layout hints for top/expandX (extended top paddding)
      TGLayoutHints*	fL5;
      /// Layout hints for expandY/expandX (negative paddding)
      TGLayoutHints*	fL6;
      /// Layout hints for top/expandX (no x padding)
      TGLayoutHints*	fL7;
      /// Layout hints for centerY/expandX (alt. padding)
      TGLayoutHints*	fL8;
      /// Layout hints for expandY/expandX (very negative paddding)
      TGLayoutHints*	fL9;
   
   public:
      TLGOptionUnits (const TGWindow* p, Int_t id, OptionUnits_t* optvals, 
                     TLGOptionTab* tab, TList* xunits = 0, 
                     TList* yunits = 0);
      virtual ~TLGOptionUnits ();
   
      virtual void UpdateOptions ();
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };



/** Cursor control option panel tab. For internal use only.
   
    @memo Cursor control option panel tab.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGOptionCursor : public TLGOptions {
   
   protected:
      /// list of plots
      const PlotMap*	fPlotList;
      /// Currently selected trace
      Int_t		fCurTrace;
      /// Currently selected statitsics values
      Int_t		fCurStat;
   
      /// Active/type frame
      TGCompositeFrame*	fF0;
      /// Active group
      TGGroupFrame*	fF1;
      /// Type group
      TGGroupFrame*	fF2;
      /// Value group
      TGGroupFrame*	fF3;
      /// Statistics group
      TGGroupFrame*	fF4;
      /// Orientation group
      TGGroupFrame*	fF5;
      /// Style frames
      TGCompositeFrame*	fF6[3];
      /// Values first line
      TGCompositeFrame*	fF7;
      /// Values second line
      TGCompositeFrame*	fF8;
      /// Statitsics first line
      TGCompositeFrame*	fF9;
      /// Trace selection label
      TGLabel*		fTraceLabel;
      /// Tabs for traces
      TGTab*		fTraces;
      /// Cursor active
      TGCheckButton*	fActive[2];
      /// Cursor style 
      TGRadioButton*	fStyle[4];
      /// Cursor type 
      TGRadioButton*	fType[2];
      /// Cursor X value labels
      TGLabel*		fXLabel[2];
      /// Cursor X values
      TLGNumericControlBox* fX[2];
      /// Cursor Y value labels
      TGLabel*		fYLabel[2];
      /// Cursor Y values
      TLGNumericControlBox* fY[2];
      /// Statitsics selection
      TGComboBox*	fStatistics;
      /// values
      TLGNumericEntry* 	fVal[2];
   
      /// Layout hints for top/left
      TGLayoutHints*	fL1;
      /// Layout hints for top/expandX
      TGLayoutHints*	fL2;
      /// Layout hints for centerY/left
      TGLayoutHints*	fL3;
      /// Layout hints for centerY/expandX
      TGLayoutHints*	fL4;
      /// Layout hints for top/expandX (extended top paddding)
      TGLayoutHints*	fL5;
      /// Layout hints for expandY/expandX (negative paddding)
      TGLayoutHints*	fL6;
      /// Layout hints for top/expandX (neg. x padding)
      TGLayoutHints*	fL7;
      /// Layout hints for centerY/expandX (alt. padding)
      TGLayoutHints*	fL8;
      /// Layout hints for centerY/right (left padding)
      TGLayoutHints*	fL9;
      /// Layout hints for top/expandX (neg. x/neg. bottom padding)
      TGLayoutHints*	fL10;
   
   public:
      TLGOptionCursor (const TGWindow* p, Int_t id, 
                      OptionCursor_t* optvals, const PlotMap& ptypes);
      virtual ~TLGOptionCursor ();
   
      virtual void UpdateOptions ();
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };



/** Config option panel tab. For internal use only.
   
    @memo Config option panel tab.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGOptionConfig : public TLGOptions {
   protected:
      /// Parent tab
      TLGOptionTab* 	fTab;
      /// Group frame for auto config
      TGGroupFrame*	fG1;
      /// Automatic plot configuration check box
      TGCheckButton*	fAutoConf;
      /// Respect user check box
      TGCheckButton*	fRespectUser;
      /// Automatic axes configuration check box
      TGCheckButton*	fAutoAxes;
      /// Automatic binning check box
      TGCheckButton*	fAutoBin;
      /// Automatic time shift check box
      TGCheckButton*	fAutoTimeAdjust;
      /// Frame for line with buttons
      TGCompositeFrame*	fFB;
      /// Save settings button
      TGButton*		fSave;
      /// Restore settings button
      TGButton*		fRestore;
      /// Layout hints 
      TGLayoutHints*	fL1;
      /// Layout hints
      TGLayoutHints*	fL2;
      /// Layout hints
      TGLayoutHints*	fL3;
      /// Layout hints 
      TGLayoutHints*	fL4;
      /// Layout hints
      TGLayoutHints*	fL5;
      /// Layout hints
      TGLayoutHints*	fL6;
   
   public:
      TLGOptionConfig (const TGWindow* p, Int_t id, 
                      OptionConfig_t* optvals, TLGOptionTab* tab);
      virtual ~TLGOptionConfig ();
   
      virtual void UpdateOptions ();
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };



/** Style option panel tab. For internal use only.
   
    @memo Style option panel tab.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGOptionStyle : public TLGOptions {
   
   protected:
      /// Group frame for title
      TGGroupFrame*	fG1;
      /// Frame for line with title
      TGCompositeFrame*	fF1;
      /// Frame for line with font selection
      TGCompositeFrame*	fF2;
      /// Frame for line with title adjustment
      TGCompositeFrame*	fF3;
      /// Entry field for plot title
      TGTextEntry*	fTitle;
      /// Title color
      TLGColorComboBox*	fTitleColor;
      /// Title font 
      TLGFontSelection*	fTitleFont;
      /// Title adjustment
      TGRadioButton*	fTitleAdjust[3];
      /// Group frame for plot margin
      TGGroupFrame*	fG2;
      /// Frame for line with margin selection
      TGCompositeFrame*	fF4;
      /// Labels for margins
      TGLabel*		fMarginLabel[4];
      /// Plot margins
      TLGNumericControlBox* fMargin[4];
   
      /// Layout hints for group frames top/expandx bottom padding only
      TGLayoutHints*	fL1;
      /// Layout hints for line frames top/expandx no padding
      TGLayoutHints*	fL2;
      /// Layout hints for centerY/left
      TGLayoutHints*	fL3;
      /// Layout hints for centerY/expandX
      TGLayoutHints*	fL4;
      /// Layout hints for line frames top/expandx negative padding
      TGLayoutHints*	fL5;
      /// Layout hints for centerY/right
      TGLayoutHints*	fL6;
   
   public:
      TLGOptionStyle (const TGWindow* p, Int_t id, 
                     OptionStyle_t* optvals);
      virtual ~TLGOptionStyle ();
   
      virtual void UpdateOptions ();
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };



/** Axes option panel tab. For internal use only.
   
    @memo Axes option panel tab.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGOptionAxis : public TLGOptions {
   
   protected:
      /// X axis?
      Bool_t 		fXAxis;
      /// Title group
      TGGroupFrame*	fF1;
      /// Ticks group
      TGGroupFrame*	fF2;
      /// Label group
      TGGroupFrame*	fF3;
      /// Title first line
      TGCompositeFrame*	fF4;
      /// Title second line
      TGCompositeFrame*	fF5;
      /// Ticks first line
      TGCompositeFrame*	fF6;
      /// Ticks second line
      TGCompositeFrame*	fF7;
      /// Labels first line
      TGCompositeFrame*	fF8;
      /// Button group	
      TGGroupFrame*	fF9;
      /// Button first line	
      TGCompositeFrame*	fF10;
      /// Axis title
      TLGTextEntry*	fTitle;
      /// Title centering
      TGCheckButton*	fTitleCenter;
      /// Title size label
      TGLabel*		fTitleSizeLabel;
      /// Title size
      TLGNumericControlBox* fTitleSize;
      /// Title offset label
      TGLabel*		fTitleOfsLabel;
      /// Title offset
      TLGNumericControlBox* fTitleOfs;
      /// Ticks length label
      TGLabel*		fTicksLenLabel;
      /// Ticks length
      TLGNumericControlBox* fTicksLen;
      /// Ticks on both side
      TGCheckButton*	fTicksBoth;
      /// Ticks divisions label
      TGLabel*		fTicksDivLabel;
      /// Ticks divisions
      TLGNumericControlBox* fTicksDiv[3];
      /// Ticks/Axis color
      TGComboBox*	fAxisColor;
      /// Labels size label
      TGLabel*		fLabelsSizeLabel;
      /// Labels size
      TLGNumericControlBox* fLabelsSize;
      /// Labels offset label
      TGLabel*		fLabelsOfsLabel;
      /// Labels offset
      TLGNumericControlBox* fLabelsOfs;
      /// Labels color
      TGComboBox*	fLabelsColor;
      /// Grid check box
      TGCheckButton*	fGrid;
      /// Font selection
      TLGFontSelection*	fFontSel;
      /// Title color
      TGComboBox*	fTitleColor;
   
      /// Layout hints for group frames top/expandx bottom padding only
      TGLayoutHints*	fL1;
      /// Layout hints for line frames top/expandx no padding
      TGLayoutHints*	fL2;
      /// Layout hints for centerY/left
      TGLayoutHints*	fL3;
      /// Layout hints for centerY/expandX
      TGLayoutHints*	fL4;
      /// Layout hints for line frames top/expandx negative padding
      TGLayoutHints*	fL5;
      /// Layout hints for centerY/right
      TGLayoutHints*	fL6;
   
   public:
      TLGOptionAxis (const TGWindow* p, Int_t id, 
                    OptionAxis_t* optvals, Bool_t xaxis = kTRUE);
      virtual ~TLGOptionAxis ();
   
      virtual Bool_t IsXAxis () const {
         return fXAxis; }
   
      virtual void UpdateOptions ();
      virtual  Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };



/** Legend option panel tab. For internal use only.
   
    @memo Legend option panel tab.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGOptionLegend : public TLGOptions {
   protected:
      /// Currently selected trace
      Int_t		fCurTrace;



      /// Text selection tabs
      TGTab *		fTextTab;
      /// User text entry
      TLGTextEntry *	        fText;
      /// Show button
      TGButton *		fShow;
      /// Placement radio buttons
      TGButton *		fPlace_top_left;
      TGButton *		fPlace_top_right;
      TGButton *		fPlace_bottom_left;
      TGButton * 		fPlace_bottom_right;

      /// X adjust
      TLGNumericControlBox *	fXAdj;
      /// Y adjust	
      TLGNumericControlBox *	fYAdj;
      /// Size	
      TLGNumericControlBox *	fSize;
      /// Symbol style radio buttons
      TGButton *		fSymbolStyleSame;
      TGButton *		fSymbolStyleNone;
      /// Text selection radio buttons
      TGButton *		fTextSelAuto;
      TGButton *		fTextSelUser;

   public:
      TLGOptionLegend (const TGWindow* p, Int_t id, 
                      OptionLegend_t* optvals);
   
      virtual void UpdateOptions ();
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };



/** Parameter option panel tab. For internal use only.
   
    @memo Parameter option panel tab.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGOptionParam : public TLGOptions {
   protected:
      /// Frame for show
      TGCompositeFrame*	fFS;
      /// Time format group
      TGGroupFrame*	fGT;
      /// Variable group
      TGGroupFrame*	fGV;
      /// Line frames
      TGCompositeFrame*	fF[1];
      /// Show button
      TGButton*		fShow;
      /// Time format radio buttons
      TGButton*		fTimeFormat[2];
      /// Variable check buttons
      TGButton*		fVar[5]; // hist (mito) 8/8/2002
   
      /// Layout hints for top/left
      TGLayoutHints*	fL1;
      /// Layout hints for centerY/expandX
      TGLayoutHints*	fL2;
      /// Layout hints for centerY/left
      TGLayoutHints*	fL3;
      /// Layout hints for top/expandX (no x padding)
      TGLayoutHints*	fL4;
   
   public:
      TLGOptionParam (const TGWindow* p, Int_t id, 
                     OptionParam_t* optvals);
      virtual ~TLGOptionParam ();
   
      virtual void UpdateOptions ();
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };

//@}


/** Function to set an option structure to its default values.
   
    @memo Set default graphics options.
    @param opt All options structure
    @return void
 ************************************************************************/
   void SetDefaultGraphicsOptions (OptionAll_t& opt);


/** Option panel tab widget. For internal use only.
   
    @memo Option panel tab widget.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGOptionTab : public TLGMultiTab {
   
   protected:
      /// Plot types
      const PlotMap* 	fPlotList;
      /// All option values
      OptionAll_t*	fOptionValues;
      /// List of stored options
      OptionAll_t**	fStoreOptions;
      /// Size of stored options list
      Int_t		fStoreOptionsMax;
      /// List of x units
      TList*		fXUnitList;
      /// List of y units
      TList*		fYUnitList;
   
      /// Tabs
      TGCompositeFrame*	fTab[kMaxTabs];
      /// Traces option tab
      TLGOptionTraces*	fTabTraces;
      /// Range option tab
      TLGOptionRange*	fTabRange;
      /// Units option tab
      TLGOptionUnits*	fTabUnits;
      /// Cursor option tab
      TLGOptionCursor*	fTabCursor;
      /// Configuration option tab
      TLGOptionConfig*	fTabConfig;
      /// Style option tab
      TLGOptionStyle*	fTabStyle;
      /// X axis option tab
      TLGOptionAxis*	fTabAxisX;
      /// Y axis option tab
      TLGOptionAxis*	fTabAxisY;
      /// Style option tab
      TLGOptionLegend*	fTabLegend;
      /// Style option tab
      TLGOptionParam*	fTabParam;
      // Tab layout hints
      TGLayoutHints*	fL;
   
   public:
      TLGOptionTab (const TGWindow* p, Int_t id,
                   OptionAll_t* optvals, const PlotMap& plotlist,
                   OptionAll_t** list = 0, Int_t max = 0,
                   TList* xunits = 0, TList* yunits = 0);
   
      virtual ~TLGOptionTab ();
   
      virtual OptionAll_t* GetOptionValues () const {
         return fOptionValues; }
      virtual void SetOptionValues (OptionAll_t* optvals) {
         fOptionValues = optvals; }
   
      virtual void SetStoreOptionList (OptionAll_t** list, Int_t max) {
         fStoreOptions = list; fStoreOptionsMax = max; }
      virtual Bool_t CalibrationDialog ();
      virtual Bool_t SaveRestoreDialog (Bool_t save);
   
      virtual void UpdateOptions ();
      virtual void UpdateAxis (Int_t axis);
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };



/** Option panel dialog window. For internal use only.
   
    @memo Option panel dialog window.
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGOptionDialog : public TLGTransientFrame {
   
   protected:
      /// Option modified?
      Bool_t		fModified;
      /// Plot types
      const PlotMap* 	fPlotList;
      /// Option value pointer
      OptionAll_t*	fOptionValues;
      /// Option values temporary storage
      OptionAll_t	fOptionTemp;
      /// Option tab panel
      TLGOptionTab*	fOptionTabs;
      /// Button frame
      TGCompositeFrame*	fButtonFrame;
      /// OK button
      TGButton*		fOkButton;
      /// Update button
      TGButton*		fUpdateButton;
      /// Cancel button
      TGButton*		fCancelButton;
      /// Layout hints for buttons
      TGLayoutHints*	fL1;
      /// Layout hints for option tab and button frame
      TGLayoutHints*	fL2;
   
   public:
      TLGOptionDialog (const TGWindow *p, const TGWindow *main,
                      const char* name, OptionAll_t* optvals, 
                      const PlotMap& plotlist, 
                      OptionAll_t** list = 0, Int_t max = 0,
                      TList* xunits = 0, TList* yunits = 0);
      virtual ~TLGOptionDialog ();
      virtual void CloseWindow();
   
      virtual void SetStoreOptionList (OptionAll_t** list, Int_t max) {
         fOptionTabs->SetStoreOptionList (list, max); }
      virtual void UpdateOptions ();
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, Long_t);
   };


//@}
}

#endif //_LIGO_TLGOPTIONS_H
