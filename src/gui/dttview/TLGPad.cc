/* -*- mode: c++; c-basic-offset: 3; -*- */
/* version $Id: TLGPad.cc 7982 2018-01-07 03:51:27Z john.zweizig@LIGO.ORG $ */
#include "PConfig.h"
#ifdef P__LINUX
#define _ISOC99_SOURCE
#endif
#include <memory>
#include <TROOT.h>
#include <TSystem.h>
#include <TMath.h>
#include <TCanvas.h>
#include <TEnv.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TAxis.h>
#include <TH1.h>
#include <TLine.h>
#include <TText.h>
#include <TLegend.h>
#include <TPostScript.h>
#include <TGFileDialog.h>
#include <TGMsgBox.h>
#include <TPaveStats.h> // hist (mito)
#include <TVirtualHistPainter.h>
#include <cmath>
#include <strings.h>
#if HAVE_ISFINITE
#define finite isfinite
#elif defined( P__SOLARIS )
#include <ieeefp.h>
#elif defined (P__LINUX)
// #define finite isfinite
#elif defined (P__DARWIN)
// #define finite isfinite
#elif defined (P__WIN32)
// OK
#else
#error "Need finite/isfinite macro"
#endif
#include <cstring>
#include <TVirtualX.h>
#include "TLGPad.hh"
#include "PlotSet.hh"
#include "Calibrations.hh"
#include "TLGMath.hh"
#include "TLGCalDlg.hh"
#include "TLGExport.hh"
#include "TLGEntry.hh"
#include "TLGPrint.hh"
#include "gdssigproc.h"


// #define DEBUG_RANGE_SETTING 1
// These includes are for debug messages.
#include <iostream>
#include <fstream>

static int my_debug = 0 ;

namespace ligogui {
   using namespace std;
   //using namespace calibration;


   const Float_t kPlotLimit = 1E30;
   const Int_t maxDisplayPoints = 10000;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Data conversion routines                                             //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   inline Bool_t DataCopyConvertY (TLGPad::EDataCopy cpy, 
                     Float_t& reout, Float_t re, Float_t im)
   {
      if (!finite(re) || !finite(im)) {
         reout = 0;
         return kFALSE;
      }
      switch (cpy) {
         case TLGPad::kDCpyAsIs:
         case TLGPad::kDCpyMagnitude:
            reout = TMath::Hypot (re, im);
            return kTRUE;
         case TLGPad::kDCpydBMagnitude:
            {
               float x = TMath::Hypot (re, im);
               if (x > 0) {
                  reout =  20. * TMath::Log10 (x);
               }
               else {
                  reout =  -1000.;
               }
            }
            return kTRUE;
         case TLGPad::kDCpyReal:
            reout = re;
            return kTRUE;
         case TLGPad::kDCpyImaginary:
            reout = im;
            return kTRUE;
         case TLGPad::kDCpyPhaseDeg:
         case TLGPad::kDCpyPhaseDegCont:
            reout = 180. / TMath::Pi() * TMath::ATan2 (im, re);
            return kTRUE;
         case TLGPad::kDCpyPhaseRad:
         case TLGPad::kDCpyPhaseRadCont:
            reout = TMath::ATan2 (im, re);
            return kTRUE;
      }
      return kFALSE;
   }

//______________________________________________________________________________
   inline Bool_t DataCopyConvertY (TLGPad::EDataCopy cpy, Float_t& reout, 
                     Float_t re)
   {
      if (!finite(re)) {
         reout = 0;
         return kFALSE;
      }
      switch (cpy) {
         case TLGPad::kDCpyAsIs:
         case TLGPad::kDCpyMagnitude:
         case TLGPad::kDCpyReal:
            reout = re;
            return kTRUE;
         case TLGPad::kDCpydBMagnitude:
            {
               float x = fabs (re);
               if (x > 0) {
                  reout = 20. * TMath::Log10 (x);
               }
               else {
                  reout = -1000.;
               }
               return kTRUE;
            }
         case TLGPad::kDCpyImaginary:
         case TLGPad::kDCpyPhaseDeg:
         case TLGPad::kDCpyPhaseDegCont:
         case TLGPad::kDCpyPhaseRad:
         case TLGPad::kDCpyPhaseRadCont:
            reout = 0;
            return kTRUE;
      }
      return kFALSE;
   }
//______________________________________________________________________________
   inline Bool_t DataCopyConvertY (TLGPad::EDataCopy cpy, Double_t& reout, 
                     Double_t re)
   {
      if (!finite(re)) {
         reout = 0;
         return kFALSE;
      }
      switch (cpy) {
         case TLGPad::kDCpyAsIs:
         case TLGPad::kDCpyMagnitude:
         case TLGPad::kDCpyReal:
            reout = re;
            return kTRUE;
         case TLGPad::kDCpydBMagnitude:
            {
               double x = (double)fabs (re);
               if (x > 0) {
                  reout = 20. * TMath::Log10 (x);
               }
               else {
                  reout = -1000.;
               }
               return kTRUE;
            }
         case TLGPad::kDCpyImaginary:
         case TLGPad::kDCpyPhaseDeg:
         case TLGPad::kDCpyPhaseDegCont:
         case TLGPad::kDCpyPhaseRad:
         case TLGPad::kDCpyPhaseRadCont:
            reout = 0;
            return kTRUE;
      }
      return kFALSE;
   }

//______________________________________________________________________________
   inline Bool_t DataCopyConvertX (TLGPad::EDataCopy cpy, 
                     Float_t& out, Float_t x)
   {
      if (!finite(x)) {
         out = 0;
         return kFALSE;
      }
      else {
         out = x;
         return kTRUE;
      }
   }

//______________________________________________________________________________
   inline Bool_t DataCopyConvertC (TLGPad::EDataCopy cpy, 
                     Float_t& reout, Float_t& imout, 
                     Float_t re, Float_t im)
   {
      if (!finite(re) || !finite(im)) {
         reout = 0;
         imout = 0;
         return kFALSE;
      }
      else {
         reout = re;
         imout = im;
         return kTRUE;
      }
   }

//______________________________________________________________________________
   Bool_t DataCopyConvert (DataCopy& dat, const BasicDataDescriptor& desc, 
                     TLGPad::EDataCopy cpy = TLGPad::kDCpyAsIs) 
   {
      Bool_t ret  = kFALSE;
      Bool_t cmplx = (desc.IsComplex() && (cpy == TLGPad::kDCpyAsIs));
      Int_t n = desc.GetN();
      // complex-to-real data conversion
      if (desc.IsComplex() && (cpy != TLGPad::kDCpyAsIs)) {
         Float_t* x = new Float_t [n];
         Float_t* y = new Float_t [n];
         Float_t* xx = desc.GetX();
         Float_t* yy = desc.GetY();
         if ((x != 0) && (xx != 0) && (y != 0) && (yy != 0)) {
            // convert data into desired format
            Int_t j = 0;
            for (Int_t i = 0; i < n; i++) {
               if (DataCopyConvertX (TLGPad::kDCpyAsIs, x[j], xx[i])) {
                  DataCopyConvertY (cpy, y[j], yy[2 * i], yy[2 * i + 1]);
                  j++;
               }
            }
            n = j;
            // check phase 'jumps'
            if ((cpy == TLGPad::kDCpyPhaseDegCont) || 
               (cpy == TLGPad::kDCpyPhaseRadCont)) {
               Float_t lim = 
                  (cpy == TLGPad::kDCpyPhaseDegCont) ? 180. : TMath::Pi();
               Float_t dy;
               Float_t jump;
               for (Int_t i = 1; i < n; i++) {
                  dy = y[i-1] - y[i];
                  if ((dy < lim) || (dy > lim)) {
                     jump = dy / (2. * lim);
                     if (TMath::Abs (jump - TMath::Floor (jump)) < 0.5) {
                        jump = TMath::Floor (jump);
                     }
                     else {
                        jump = TMath::Ceil (jump);
                     }
                     y[i] += 2. * lim * jump;
                  }
               }
            }
            // set data
            ret = (n > 0) && dat.SetData (x, y, n, cmplx);
         }
         delete [] x;
         delete [] y;
      }
      // real-to-real data conversion
      else if (!desc.IsComplex()) {// && (cpy != TLGPad::kDCpyAsIs)) {
         Float_t* x = new Float_t [n];
         Float_t* y = new Float_t [n];
         Float_t* xx = desc.GetX();
         Float_t* yy = desc.GetY();
         if ((x != 0) && (xx != 0) && (y != 0) && (yy != 0)) {
            // convert data into desired format
            Int_t j = 0;
            for (Int_t i = 0; i < n; i++) {
               if (DataCopyConvertX (TLGPad::kDCpyAsIs, x[j], xx[i])) {
                  DataCopyConvertY (cpy, y[j], yy[i]);
                  j++;
               }
            }
            n = j;
            // set data
            ret = (n > 0) && dat.SetData (x, y, n, cmplx);
         }
         delete [] x;
         delete [] y;
      }
      // complex-to-complex conversion
      else {
         Float_t* x = new Float_t [n];
         Float_t* y = new Float_t [2*n];
         Float_t* xx = desc.GetX();
         Float_t* yy = desc.GetY();
         if ((x != 0) && (xx != 0) && (y != 0) && (yy != 0)) {
            // convert data into desired format
            Int_t j = 0;
            for (Int_t i = 0; i < n; i++) {
               if (DataCopyConvertX (TLGPad::kDCpyAsIs, x[j], xx[i])) {
                  DataCopyConvertC (cpy, y[2*j], y[2*j+1], yy[2*i], yy[2*i+1]);
                  j++;
               }
            }
            n = j;
            ret = (n > 0) && dat.SetData (x, y, n, cmplx);
         }
         delete [] x;
         delete [] y;
      }
      return ret;
   }

//______________________________________________________________________________
   void RMSCorrection (BasicDataDescriptor* dcpy, bool forward) 
   {
      // correction for RMS
      if (forward) {
         double y, dx, rms = 0;
         int n = dcpy->GetN();
         for (int i = n - 1; i >= 0; i--) {
            y = dcpy->GetY()[i]*dcpy->GetY()[i];
            dx = (i == n - 1) ? 
               dcpy->GetX()[n-1] - dcpy->GetX()[n-2] : 
               dcpy->GetX()[i+1] - dcpy->GetX()[i];
            rms += y * fabs (dx);
            dcpy->GetY()[i] = sqrt (rms);
         }
      }
      else {
         int n = dcpy->GetN();
         double dy, dx;
         for (int i = 0; i < n; ++i) {
            if (i < n -1) {
               dy = dcpy->GetY()[i]*dcpy->GetY()[i] - 
                  dcpy->GetY()[i+1]*dcpy->GetY()[i+1];
               dx = dcpy->GetX()[i+1] - dcpy->GetX()[i];
            }
            else {
               dy = dcpy->GetY()[n-1]*dcpy->GetY()[n-1];
               dx = dcpy->GetX()[n-1] - dcpy->GetX()[n-2];
            }
            if (dx) dy /= dx;
            dcpy->GetY()[i] = sqrt (fabs (dy));
         }
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGraph                                                              //
//                                                                      //
// Graph used by TPad to show a plot                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   const Int_t NPMAX = 204;
   const Int_t kNoStats = BIT(9);
   const Int_t kClipFrame = BIT (10);

   static Double_t xwork[NPMAX];
   static Double_t ywork[NPMAX];
   static Double_t xworkl[NPMAX];
   static Double_t yworkl[NPMAX];

//_____________________________________________________________________________
   TLGraph::TLGraph(TLGPad* p, Int_t n, const Float_t* x, const Float_t* y) : 
      TLGraphExtensions (p), TGraph (n, x, y)  {
   }

//_____________________________________________________________________________
   void TLGraph::Draw (Option_t* opt) {
      TVirtualPad*	padsave = gPad;
      gPad = fParent->GetCanvas();
      TGraph::Draw (opt);
      fBarOpt = (strchr (opt, 'B') != 0);
#if ROOT_VERSION_CODE < ROOT_VERSION(5,20,0)
      if ((strchr (opt, 'A') != 0) && (fHistogram == 0)) {
         fHistogram = new TH1F (GetName(), GetTitle (), 40, 0, 10);
         if (fHistogram != 0) {
            fHistogram->SetBit (kNoStats);
            fHistogram->SetDirectory (0);
         }
      }
#else
      if (fBarOpt) gStyle->SetBarWidth(fBarWidth);
      if ((strchr (opt, 'A') != 0) && (fHistogram == 0)) {
	 GetHistogram();
      }
#endif
      gPad = padsave;
   }

#if ROOT_VERSION_CODE >= ROOT_VERSION(5,20,0)
   //__________________________________________________________________________
   void TLGraph::SetMinimumX (Double_t min) {
      fMinimumX = min;
      GetXaxis()->SetLimits(min, fMaximumX);
   }

   void TLGraph::SetMaximumX (Double_t max) {
      fMaximumX = max;
      GetXaxis()->SetLimits(fMinimumX, max);
   }
#endif

//______________________________________________________________________________
#if ROOT_VERSION_CODE >= ROOT_VERSION(3,4,2)
   void TLGraph::ComputeRange (Double_t& xmin, Double_t& ymin, 
			       Double_t& xmax, Double_t& ymax) const
#else
   void TLGraph::ComputeRange (Double_t& xmin, Double_t& ymin, 
			       Double_t& xmax, Double_t& ymax) 
#endif
   {
      xmin = fParent->fRangeMin[0];
      if (gPad->GetLogx() && (xmin <= 0) && (fParent->fRangeMinPos[0] > 0)) {
         xmin = fParent->fRangeMinPos[0];
      }
      ymin = fParent->fRangeMin[1];
      if (gPad->GetLogy() && (ymin <= 0) && (fParent->fRangeMinPos[1] > 0)) {
         ymin = fParent->fRangeMinPos[1];
      }
      xmax = fParent->fRangeMax[0];
      ymax = fParent->fRangeMax[1];
   }

//______________________________________________________________________________
   void TLGraph::ComputeLimits (Double_t& xmin, Double_t& ymin, 
                     Double_t& xmax, Double_t& ymax)
   {
      Double_t uxmin, uxmax;
      Double_t rwxmin,rwxmax, rwymin, rwymax, maximum, minimum;
     
      ComputeRange (rwxmin, rwymin, rwxmax, rwymax);
   
      if (rwxmin == rwxmax) rwxmax += 1.;
      if (rwymin == rwymax) rwymax += 1.;
      Double_t dx = 0.1*(rwxmax-rwxmin);
      Double_t dy = 0.1*(rwymax-rwymin);
      uxmin    = rwxmin - dx;
      uxmax    = rwxmax + dx;
      minimum  = rwymin - dy;
      maximum  = rwymax + dy;
      if (fMinimum != -1111) rwymin = minimum = fMinimum;
      if (fMaximum != -1111) rwymax = maximum = fMaximum;
   // ::MODIFIED:: DS, 11/19/99
      if (fBarOpt) {
         uxmax += (rwxmax - rwxmin) / (fNpoints > 1 ? fNpoints - 1 : 1);
      }
      if (fMinimumX != -1111) rwxmin = uxmin = fMinimumX;
      if (fMaximumX != -1111) rwxmax = uxmax = fMaximumX;
   // ::END::MODIFIED:: DS, 11/19/99
      if (uxmin < 0 && rwxmin >= 0) {
         if (gPad->GetLogx()) uxmin = 0.9*rwxmin;
         else                 uxmin = 0;
      }
      if (uxmax > 0 && rwxmax <= 0) {
         if (gPad->GetLogx()) uxmax = 1.1*rwxmax;
         else                 uxmax = 0;
      }
      if (minimum < 0 && rwymin >= 0) {
         if(gPad->GetLogy()) minimum = 0.9*rwymin;
         else                minimum = 0;
      }
      if (maximum > 0 && rwymax <= 0) {
         if(gPad->GetLogy()) maximum = 1.1*rwymax;
         else                maximum = 0;
      }
      if (minimum <= 0 && gPad->GetLogy()) minimum = 0.001*maximum;
      if (uxmin <= 0 && gPad->GetLogx()) {
         if (uxmax > 1000) uxmin = 1;
         else              uxmin = 0.001*uxmax;
      }
      xmin = uxmin;
      xmax = uxmax;
      ymin = minimum;
      ymax = maximum;
   }

//______________________________________________________________________________

#if ROOT_VERSION_CODE < ROOT_VERSION(5,20,0)
//______________________________________________________________________________
   void TLGraph::PaintGraph(Int_t npoints, const Double_t *x, 
                     const Double_t *y, const Option_t *chopt)
   {
   //*-*-*-*-*-*-*-*-*-*-*-*Control function to draw a graph*-*-*-*-*-*-*-*-*-*-*
   //*-*                    ================================
   //
   //
   //   Draws one dimensional graphs. The aspect of the graph is done
   // according to the value of the chopt.
   //
   // _Input parameters:
   //
   //  npoints : Number of points in X or in Y.
   //  X(N) or X(2) : X coordinates or (XMIN,XMAX) (WC space).
   //  Y(N) or Y(2) : Y coordinates or (YMIN,YMAX) (WC space).
   //  chopt : Option.
   //
   //  chopt='L' :  A simple polyline beetwen every points is drawn
   //
   //  chopt='F' :  A fill area is drawn ('CF' draw a smooth fill area)
   //
   //  chopt='A' :  Axis are drawn around the graph
   //
   //  chopt='C' :  A smooth Curve is drawn
   //
   //  chopt='*' :  A Star is plotted at each point
   //
   //  chopt='P' :  Idem with the current marker
   //
   //  chopt='B' :  A Bar chart is drawn at each point
   //
   //  chopt='1' :  ylow=rwymin
   //
   //
      Int_t OptionLine , OptionAxis , OptionCurve,OptionStar ,OptionMark;
      Int_t OptionBar  , OptionR    , OptionOne;
      Int_t OptionFill , OptionZ    ,OptionCurveFill;
      Int_t i, npt, nloop;
      Int_t drawtype;
      Double_t xlow, xhigh, ylow, yhigh;
      Double_t barxmin, barxmax, barymin, barymax;
      Double_t uxmin, uxmax;
      Double_t x1, xn, y1, yn;
      Double_t dbar, bdelta;
   
   //*-* ______________________________________
   //cerr << "TLGraph::PaintGraph()" << endl ; 
      if (npoints <= 0) {
         Error("PaintGraph", "illegal number of points (%d)", npoints);
         return;
      }
      TString opt = chopt;
      opt.ToUpper();
   
      if(opt.Contains("L")) OptionLine = 1;  
      else OptionLine = 0;
      if(opt.Contains("A")) OptionAxis = 1;  
      else OptionAxis = 0;
      if(opt.Contains("C")) OptionCurve= 1;  
      else OptionCurve= 0;
      if(opt.Contains("*")) OptionStar = 1;  
      else OptionStar = 0;
      if(opt.Contains("P")) OptionMark = 1;  
      else OptionMark = 0;
      if(opt.Contains("B")) OptionBar  = 1;  
      else OptionBar  = 0;
      if(opt.Contains("R")) OptionR    = 1;  
      else OptionR    = 0;
      if(opt.Contains("1")) OptionOne  = 1;  
      else OptionOne  = 0;
      if(opt.Contains("F")) OptionFill = 1;  
      else OptionFill = 0;
      OptionZ    = 0;
   //*-*           If no "drawing" option is selected and if chopt<>' '
   //*-*           nothing is done.
   
      if (OptionLine+OptionFill+OptionCurve+OptionStar+OptionMark+OptionBar == 0) {
         if (strlen(chopt) == 0)  OptionLine=1;
         else   
            return;
      }
   
      if (OptionStar) SetMarkerStyle(3);
   
      OptionCurveFill = 0;
      if (OptionCurve && OptionFill) {
         OptionCurveFill = 1;
         OptionFill      = 0;
      }
   
   // ::MODIFIED:: DS, 12/5/99
   //*-*  Set Clipping option
      // gPad->SetBit(kClipFrame, TestBit(kClipFrame));
   // ::END::MODIFIED:: DS, 12/5/99
   
   //*-*-           Draw the Axis with a fixed number of division: 510
   
      Double_t rwxmin,rwxmax, rwymin, rwymax, maximum, minimum;
   
      if (OptionAxis) {
      
         rwxmin = rwxmax = x[0];
         rwymin = rwymax = y[0];
         for (i=1;i<npoints;i++) {
            if (x[i] < rwxmin) rwxmin = x[i];
            if (x[i] > rwxmax) rwxmax = x[i];
            if (y[i] < rwymin) rwymin = y[i];
            if (y[i] > rwymax) rwymax = y[i];
         }
      
         ComputeRange(rwxmin, rwymin, rwxmax, rwymax);  //this is redefined in TGraphErrors
      
         if (rwxmin == rwxmax) rwxmax += 1.;
         if (rwymin == rwymax) rwymax += 1.;
         Double_t dx = 0.1*(rwxmax-rwxmin);
         Double_t dy = 0.1*(rwymax-rwymin);
         uxmin    = rwxmin - dx;
         uxmax    = rwxmax + dx;
         minimum  = rwymin - dy;
         maximum  = rwymax + dy;
         if (fMinimum != -1111) rwymin = minimum = fMinimum;
         if (fMaximum != -1111) rwymax = maximum = fMaximum;
      // ::MODIFIED:: DS, 11/19/99
         if (OptionBar) {
            uxmax += (rwxmax - rwxmin) / (npoints > 1 ? npoints-1 : 1);
         }
         if (fMinimumX != -1111) rwxmin = uxmin = fMinimumX;
         if (fMaximumX != -1111) rwxmax = uxmax = fMaximumX;
      // ::END::MODIFIED:: DS, 11/19/99
         if (uxmin < 0 && rwxmin >= 0) {
            if (gPad->GetLogx()) uxmin = 0.9*rwxmin;
            else                 uxmin = 0;
         }
         if (uxmax > 0 && rwxmax <= 0) {
            if (gPad->GetLogx()) uxmax = 1.1*rwxmax;
            else                 uxmax = 0;
         }
         if (minimum < 0 && rwymin >= 0) {
            if(gPad->GetLogy()) minimum = 0.9*rwymin;
            else                minimum = 0;
         }
         if (maximum > 0 && rwymax <= 0) {
            if(gPad->GetLogy()) maximum = 1.1*rwymax;
            else                maximum = 0;
         }
         if (minimum <= 0 && gPad->GetLogy()) minimum = 0.001*maximum;
         if (uxmin <= 0 && gPad->GetLogx()) {
            if (uxmax > 1000) uxmin = 1;
            else              uxmin = 0.001*uxmax;
         }
         rwxmin = uxmin;
         rwxmax = uxmax;
         rwymin = minimum;
         rwymax = maximum;
         if (fHistogram) {
            fHistogram->SetMinimum(rwymin);
            fHistogram->SetMaximum(rwymax);
            fHistogram->GetXaxis()->SetLimits(rwxmin,rwxmax);
            fHistogram->GetYaxis()->SetLimits(rwymin,rwymax);
         }
      
      //*-*-  Create a temporary histogram and fill each channel with the function value
         if (!fHistogram) {
            fHistogram = new TH1F(GetName(),GetTitle(),40,rwxmin,rwxmax);
            if (!fHistogram) 
               return;
            fHistogram->SetMinimum(rwymin);
            fHistogram->SetBit(kNoStats);
            fHistogram->SetMaximum(rwymax);
            fHistogram->GetYaxis()->SetLimits(rwymin,rwymax);
            fHistogram->SetDirectory(0);
         }
         fHistogram->Paint();
      }
   
      //*-*  Set Clipping option
      gPad->SetBit(kClipFrame, TestBit(kClipFrame));
   
      TF1 *fit = 0;
      if (fFunctions) fit = (TF1*)fFunctions->First();
      if (fit) {
         if (fHistogram) {
            TVirtualHistPainter::HistPainter(fHistogram)->PaintStat(0,fit);
         } 
         else {
            TH1F *hfit = new TH1F("___0","",2,0,1);
            TVirtualHistPainter::HistPainter(hfit)->PaintStat(0,fit);
            delete hfit;
         }
      }
      rwxmin   = gPad->GetUxmin();
      rwxmax   = gPad->GetUxmax();
      rwymin   = gPad->GetUymin();
      rwymax   = gPad->GetUymax();
      uxmin    = gPad->PadtoX(rwxmin);
      uxmax    = gPad->PadtoX(rwxmax);
      if (fHistogram) {
         maximum = fHistogram->GetMaximum();
         minimum = fHistogram->GetMinimum();
      } 
      else {
         maximum = gPad->PadtoY(rwymax);
         minimum = gPad->PadtoY(rwymin);
      }
   
   //*-*-           Set attributes
      TAttLine::Modify();
      TAttFill::Modify();
      TAttMarker::Modify();
   
   //*-*-           Draw the graph with a polyline or a fill area
   
      if (OptionLine || OptionFill) {
         x1       = x[0];
         xn       = x[npoints-1];
         y1       = y[0];
         yn       = y[npoints-1];
         nloop = npoints;
         if (OptionFill && (xn != x1 || yn != y1)) nloop++;
         npt = 0;
         for (i=1;i<=nloop;i++) {
            if (i > npoints) {
               xwork[npt] = xwork[0];  ywork[npt] = ywork[0];
            } 
            else {
               xwork[npt] = x[i-1];      ywork[npt] = y[i-1];
               npt++;
            }
            if (npt == NPMAX || i == nloop) {
               ComputeLogs(npt, OptionZ);
               Int_t bord = gStyle->GetDrawBorder();
               if (OptionR) {
                  if (OptionFill) {
                     gPad->PaintFillArea(npt,yworkl,xworkl);
                     if (bord) gPad->PaintPolyLine(npt,yworkl,xworkl);
                  }
                  else         gPad->PaintPolyLine(npt,yworkl,xworkl);
               }
               else {
                  if (OptionFill) {
                     gPad->PaintFillArea(npt,xworkl,yworkl);
                     if (bord) gPad->PaintPolyLine(npt,xworkl,yworkl);
                  }
                  else         gPad->PaintPolyLine(npt,xworkl,yworkl);
               }
               xwork[0] = xwork[npt-1];  ywork[0] = ywork[npt-1];
               npt      = 1;
            }
         }
      }
   
   //*-*-           Draw the graph with a smooth Curve. Smoothing via Smooth
   
      if (OptionCurve) {
         x1 = x[0];
         xn = x[npoints-1];
         y1 = y[0];
         yn = y[npoints-1];
         drawtype = 1;
         nloop = npoints;
         if (OptionCurveFill) {
            drawtype += 1000;
            if (xn != x1 || yn != y1) nloop++;
         }
         if (!OptionR) {
            npt = 0;
            for (i=1;i<=nloop;i++) {
               if (i > npoints) {
                  xwork[npt] = xwork[0];  ywork[npt] = ywork[0];
               }
               else {
                  if (y[i-1] < minimum || y[i-1] > maximum) 
                     continue;
                  if (x[i-1] < uxmin    || x[i-1] > uxmax)  
                     continue;
                  xwork[npt] = x[i-1];      ywork[npt] = y[i-1];
                  npt++;
               }
               ComputeLogs(npt, OptionZ);
               if (yworkl[npt-1] < rwymin || yworkl[npt-1] > rwymax) {
                  if (npt > 2) {
                     ComputeLogs(npt, OptionZ);
                     Smooth(npt,xworkl,yworkl,drawtype);
                  }
                  xwork[0] = xwork[npt-1]; ywork[0] = ywork[npt-1];
                  npt=1;
                  continue;
               }
               if (npt >= NPMAX) {
                  ComputeLogs(npt, OptionZ);
                  Smooth(npt,xworkl,yworkl,drawtype);
                  xwork[0] = xwork[npt-1]; ywork[0] = ywork[npt-1];
                  npt      = 1;
               }
            }  //endfor (i=0;i<nloop;i++)
            if (npt > 1) {
               ComputeLogs(npt, OptionZ);
               Smooth(npt,xworkl,yworkl,drawtype);
            }
         }
         else {
            drawtype += 10;
            npt    = 0;
            for (i=1;i<=nloop;i++) {
               if (i > npoints) {
                  xwork[npt] = xwork[0];  ywork[npt] = ywork[0];
               } 
               else {
                  if (y[i-1] < minimum || y[i-1] > maximum) 
                     continue;
                  if (x[i-1] < uxmin    || x[i-1] > uxmax)  
                     continue;
                  xwork[npt] = x[i-1];      ywork[npt] = y[i-1];
                  npt++;
               }
               ComputeLogs(npt, OptionZ);
               if (xworkl[npt-1] < rwxmin || xworkl[npt-1] > rwxmax) {
                  if (npt > 2) {
                     ComputeLogs(npt, OptionZ);
                     Smooth(npt,xworkl,yworkl,drawtype);
                  }
                  xwork[0] = xwork[npt-1]; ywork[0] = ywork[npt-1];
                  npt=1;
                  continue;
               }
               if (npt >= NPMAX) {
                  ComputeLogs(npt, OptionZ);
                  Smooth(npt,xworkl,yworkl,drawtype);
                  xwork[0] = xwork[npt-1]; ywork[0] = ywork[npt-1];
                  npt      = 1;
               }
            } //endfor (i=1;i<=nloop;i++)
            if (npt > 1) {
               ComputeLogs(npt, OptionZ);
               Smooth(npt,xworkl,yworkl,drawtype);
            }
         }
      }
   
   //*-*-           Draw the graph with a '*' on every points
   
      if (OptionStar) {
         SetMarkerStyle(3);
         npt = 0;
         for (i=1;i<=npoints;i++) {
            if (y[i-1] >= minimum && y[i-1] <= maximum && x[i-1] >= uxmin  && x[i-1] <= uxmax) {
               xwork[npt] = x[i-1];      ywork[npt] = y[i-1];
               npt++;
            }
            if (npt == NPMAX || i == npoints) {
               ComputeLogs(npt, OptionZ);
               if (OptionR)  gPad->PaintPolyMarker(npt,yworkl,xworkl);
               else          gPad->PaintPolyMarker(npt,xworkl,yworkl);
               npt = 0;
            }
         }
      }
   
   //*-*-           Draw the graph with the current polymarker on
   //*-*-           every points
   
      if (OptionMark) {
         npt = 0;
         for (i=1;i<=npoints;i++) {
            if (y[i-1] >= minimum && y[i-1] <= maximum && x[i-1] >= uxmin  && x[i-1] <= uxmax) {
               xwork[npt] = x[i-1];      ywork[npt] = y[i-1];
               npt++;
            }
            if (npt == NPMAX || i == npoints) {
               ComputeLogs(npt, OptionZ);
               if (OptionR) gPad->PaintPolyMarker(npt,yworkl,xworkl);
               else         gPad->PaintPolyMarker(npt,xworkl,yworkl);
               npt = 0;
            }
         }
      }
   
   //*-*-           Draw the graph as a bar chart
   
      if (OptionBar) {
         if (!OptionR) {
            barxmin = x[0];
            barxmax = x[0];
            for (i=1;i<npoints;i++) {
               if (x[i] < barxmin) barxmin = x[i];
               if (x[i] > barxmax) barxmax = x[i];
            }
         // ::MODIFIED:: DS, 11/19/99
            // bdelta = (barxmax-barxmin)/float(npoints);
            if (npoints > 1) {
               bdelta = (barxmax-barxmin)/float(npoints-1);
            }
            else {
               bdelta = 1;
            }
            bdelta = fBarDelta;
         // ::END::MODIFIED:: DS, 11/19/99
         }
         else {
            barymin = y[0];
            barymax = y[0];
            for (i=1;i<npoints;i++) {
               if (y[i] < barymin) barymin = y[i];
               if (y[i] > barymax) barymax = y[i];
            }
            bdelta = (barymax-barymin)/float(npoints);
         }
      // ::MODIFIED:: DS, 11/19/99
         // dbar  = 0.5*bdelta*gStyle->GetBarWidth();
         dbar = 0.5*bdelta*fBarWidth;
      // ::END::MODIFIED:: DS, 11/19/99
         if (!OptionR) {
            for (i=1;i<=npoints;i++) {
               xlow  = x[i-1] - dbar;
               xhigh = x[i-1] + dbar;
            // ::MODIFIED:: DS, 11/19/99
               xlow += fBarOffset * bdelta;
               xhigh += fBarOffset * bdelta;
            // ::END::MODIFIED:: DS, 11/19/99
               yhigh = y[i-1];
               if (!OptionOne) ylow = TMath::Max((Double_t)0,gPad->GetUymin());
               else            ylow = gPad->GetUymin();
            // ::MODIFIED:: DS, 11/19/99
               if (ylow > yhigh) {
                  Double_t temp = ylow;
                  ylow = yhigh;
                  yhigh = temp;
               }
            // ::END::MODIFIED:: DS, 11/19/99
               xwork[0] = xlow;
               ywork[0] = ylow;
               xwork[1] = xhigh;
               ywork[1] = yhigh;
               ComputeLogs(2, OptionZ);
               if (yworkl[0] < gPad->GetUymin()) yworkl[0] = gPad->GetUymin();
               if (yworkl[1] < gPad->GetUymin()) 
                  continue;
            // ::MODIFIED:: DS, 11/19/99
               // if (yworkl[1] > gPad->GetUymax()) yworkl[0] = gPad->GetUymax();
               if (yworkl[1] > gPad->GetUymax()) yworkl[1] = gPad->GetUymax();
            // ::END::MODIFIED:: DS, 11/19/99
               if (yworkl[0] > gPad->GetUymax()) 
                  continue;
            // ::MODIFIED:: DS, 11/19/99
               TAttFill::Modify(); 
            // ::END::MODIFIED:: DS, 11/19/99
               gPad->PaintBox(xworkl[0],yworkl[0],xworkl[1],yworkl[1], "B");
            }
         }
         else {
            for (i=1;i<=npoints;i++) {
               xhigh = x[i-1];
               ylow  = y[i-1] - dbar;
               yhigh = y[i-1] + dbar;
               xlow     = TMath::Max((Double_t)0, gPad->GetUxmin());
               xwork[0] = xlow;
               ywork[0] = ylow;
               xwork[1] = xhigh;
               ywork[1] = yhigh;
               ComputeLogs(2, OptionZ);
               TAttFill::Modify(); // ::MODIFIED:: DS, 11/19/99
               gPad->PaintBox(xworkl[0],yworkl[0],xworkl[1],yworkl[1], "B");
            }
         }
      }
      gPad->ResetBit(kClipFrame);
   }
#endif

//______________________________________________________________________________
   void TLGraph::ComputeLogs(Int_t npoints, Int_t opt)
   {
   //*-*-*-*-*-*-*-*-*-*-*-*Convert WC from Log scales*-*-*-*-*-*-*-*-*-*-*-*
   //*-*                    ==========================
   //
   //   Take the LOG10 of xwork and ywork according to the value of Options
   //   and put it in xworkl and yworkl.
   //
   //  npoints : Number of points in xwork and in ywork.
   //
   
      for (Int_t i=0;i<npoints;i++) {
         xworkl[i] = xwork[i];
         yworkl[i] = ywork[i];
         if (gPad->GetLogx()) {
            if (xworkl[i] > 0) xworkl[i] = TMath::Log10(xworkl[i]);
            else               xworkl[i] = gPad->GetX1();
         }
         if (!opt && gPad->GetLogy()) {
            if (yworkl[i] > 0) yworkl[i] = TMath::Log10(yworkl[i]);
            else               yworkl[i] = gPad->GetY1();
         }
      //     if (yworkl[i] > gPad->GetUymax()) yworkl[i] = gPad->GetUymax();
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGPadLayout                                                         //
//                                                                      //
// Layout manager for graphics pad                                      //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGPadLayout::TLGPadLayout (TLGPad* pad) 
   
   {
      fPad = pad;
      fList = fPad->GetList();
   }

//______________________________________________________________________________
   void TLGPadLayout::Layout ()
   {
      if (my_debug) cerr << "TLGPadLayout::Layout ()" << endl ;
      // get size of pad and option panel
      TGDimension	padsize = fPad->GetSize();
      TGDimension 	pansize (10000, 10000);
      if (fPad->fOptionTabs) pansize = fPad->fOptionTabs->GetDefaultSize();
      TGDimension	hidesize = fPad->fOptionsHide->GetDefaultSize();
   
      // get pad sizes
      if (!fList) {
         return;
      }
      TGLayoutHints* hints = ((TGFrameElement*)(fList->First()))->fLayout;
      UInt_t		pad_left = hints->GetPadLeft();
      UInt_t		pad_right = hints->GetPadRight();
      UInt_t		pad_top = hints->GetPadTop();
      UInt_t		pad_bottom = hints->GetPadBottom();
      UInt_t		bw = fPad->GetBorderWidth();
   
      // big enough for option panel?
      Bool_t		hide = fPad->fHidePanel;
      if ((padsize.fWidth < 100 + pansize.fWidth) ||
         (padsize.fHeight < pansize.fHeight)/2) {
         if (!fPad->fHidePanel) {
            hide = kTRUE;
         }
      }
   
      // calculate coordinates of panel and canvas
      Int_t 		panx;
      Int_t 		pany;
      UInt_t 		panw;
      UInt_t 		panh;
      Int_t 		canx;
      Int_t 		cany;
      UInt_t 		canw;
      UInt_t 		canh;
      panw = (hide) ? hidesize.fWidth : pansize.fWidth;

      if (padsize.fWidth < bw + (pad_left << 1) + pad_right + panw) {
         canw = 0;
      } else {
         canw = padsize.fWidth - bw - (pad_left << 1) - pad_right - panw;
      }

      if (padsize.fHeight < bw + pad_top + pad_bottom) { 
         panh = 0;
      } else {
         panh = padsize.fHeight - bw - pad_top - pad_bottom;
      }
      canh = panh;
      if (fPad->fLeftPanel) {
         panx = pad_left + (bw >> 1);
         canx = panx + panw + pad_left;
      }
      else {
         canx = pad_left + (bw >> 1);
         panx = canx + canw + pad_left;
      }
      pany = cany = pad_top + (bw >> 1);
   
      // handle hide
      if (hide) {
         fPad->fOptionsHide->SetPicture (fPad->fPicUnhide);
      }
      else {
         fPad->fOptionsHide->SetPicture (fPad->fPicHide);
      }
   
      // caluclate coordinates of hide button and option tabs/dialog button
      Int_t 		hidex = panx;
      Int_t		hidey = pany;
      Int_t		diagx = hidex;
      Int_t		diagy = hidey + hidesize.fHeight + pad_top;
      TGDimension	diagsize = hidesize;
      Int_t		tabx = hidex;
      Int_t		taby = pany + 25; // panh - pansize.fHeight;
      TGDimension	tabsize = pansize;
   
      // move all frames to their location
      fPad->fOptionsHide->MoveResize (hidex, hidey, hidesize.fWidth, 
                           hidesize.fHeight);
      fPad->fGraph->MoveResize (canx, cany, canw, canh);
   
      // lower/raise tab/diag
      if (hide) {
         fPad->fPanelVisible = kFALSE;
         fPad->fOptionsDialog->MoveResize (diagx, diagy, diagsize.fWidth, 
                              diagsize.fHeight);
         if (fPad->fOptionTabs != 0) {
            fPad->fOptionTabs->MoveResize (diagx, diagy, diagsize.fWidth, 
                                 diagsize.fHeight);
            fPad->fOptionTabs->LowerWindow();
         }
         fPad->fOptionsDialog->RaiseWindow();
      }
      else {
         fPad->fPanelVisible = kTRUE;
         fPad->fOptionsDialog->MoveResize (tabx, taby, tabsize.fWidth, 
                              tabsize.fHeight);
         if (fPad->fOptionTabs != 0) {
            fPad->fOptionTabs->MoveResize (tabx, taby, tabsize.fWidth, 
                                 tabsize.fHeight);
            fPad->fOptionsDialog->LowerWindow();
            fPad->fOptionTabs->RaiseWindow();
         }
      }
      if (my_debug) cerr << "TLGPadLayout::Layout () return" << endl ;
   }

//______________________________________________________________________________
   TGDimension TLGPadLayout::GetDefaultSize () const
   {
      //TGDimension 	size (0,0);
      TGDimension 	msize = fPad->GetSize();
      UInt_t		options = fPad->GetOptions();
   
      if ((options & kFixedWidth) && (options & kFixedHeight)) {
         return msize;
      }
   
      if (msize.fWidth < 100) {
         msize.fWidth = 100;
      }
      if (msize.fHeight < 100) {
         msize.fHeight = 100;
      }
      return msize;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGPad::DisplayParam                                                 //
//                                                                      //
// Graphics pad display parameters 				        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGPad::DisplayParam::DisplayParam()
   {
      fValid = kFALSE;
      fLastTimeShift = 0.0;
      fLastXConv = kUnitNormal;
      fLastDCpy = kDCpyAsIs;
      fUnitX = "";
      fUnitY = "";
      fMagX = 1;
      fMagY = 1;
      fN1 = 0;
      fN2 = 0;
      fScaling[0] = 1.0;
      fScaling[1] = 1.0;
      fOffset[0] = 0.0;
      fOffset[1] = 0.0;
      fBin = 1;
      fLogspacing = kFALSE;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGPad                                                               //
//                                                                      //
// Graphics pad (includes an option panel and a graphics canvas)        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGPad::TLGPad (const TGWindow* p, const char* name, Int_t id, 
                  PlotSet& plots, OptionAll_t** list, Int_t max,
                  const OptionAll_t* optinit)
   : TGCompositeFrame (p, 800, 600, kVerticalFrame), 
   TGWidget (id), fPlotSet (&plots), fStoreOptions (list),
   fStoreOptionsMax (max), fManualRangeUpdate (0), 
   fEnablePanel (kTRUE), fEnablePanelDialog (kTRUE), fHidePanel (kTRUE), 
   fLeftPanel (kTRUE), fPanelVisible (kFALSE), fOptionDialogbox (0), fDisconnectMessage(0)
   {
      fRangeMinPos[0] = 1e-6;
      fRangeMinPos[1] = 1e-6;
      fRangeMin[0] = fRangeMin[1] = 0;
      fRangeMax[0] = fRangeMax[1] = 0;
      // Intialize options and plots
      fPadName = name;
      if (optinit != 0) {
         fOptions = *optinit;
         if (fOptions.fRange.fRange[0] == kRangeAutomatic) {
            fManualRangeUpdate |= 0x03;
         }
         if (fOptions.fRange.fRange[1] == kRangeAutomatic) {
            fManualRangeUpdate |= 0x0C;
         }
      }
      else {
         SetDefaultGraphicsOptions (fOptions);
         fManualRangeUpdate = 0x0F;
      }
      for (Int_t trace = 0; trace < kMaxTraces; trace++) {
         fData[trace] = 0;
         fOriginalPlotD[trace] = 0;
         fPlot[trace] = 0;
         fPlotId[trace] = -1;
      }
   
      // histogram frame, directory & statistics box
      fHFrame = 0; // hist (mito)
      fStatBox = 0; // hist (mito)
   
      // Initialzie parameter text
      for (int i = 0; i < 3; i++) {
         fParamText[i] = 0;
      }
      // Initialize legend
      fLegend = 0;
   
      // initialize cursor parameters
      fCursorLine[0] = 0;
      fCursorLine[1] = 0;
      fCursorMarker[0] = 0;
      fCursorMarker[1] = 0;
      fCursorTimer = 0;
      fCursorStackSize = 0;
   
      // Create default canvas style; white background
      fStyle = new TStyle();
      fStyle->SetPadColor (10);
      fStyle->SetCanvasColor (10);
      fStyle->SetTitleColor (10);
   
      // Carry over the paper size from the global style. JCB
      // But in general, the paper size probably isn't known
      // at this point unless the Print dialog has been opened.
      {
	 float x, y ;
	 gStyle->GetPaperSize(x, y) ;
	 fStyle->SetPaperSize(x, y) ;
      }

      fL = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2);

   
      // get button pictures
      fPicHide = fClient->GetPicture ("arrow_left.xpm");
      if (!fPicHide) 
         Error ("TLGPad", "arrow_left.xpm not found");
      fPicUnhide = fClient->GetPicture ("arrow_right.xpm");
      if (!fPicUnhide) 
         Error ("TLGPad", "arrow_right.xpm not found");
      fPicDialog = fClient->GetPicture ("arrow_up.xpm");
      if (!fPicDialog) 
         Error ("TLGPad", "arrow_up.xpm not found");
   
      // create option panel
      fOptionsHide = new TGPictureButton (this, fPicHide, kGOptHideID);
      fOptionsHide->Associate (this);
      fOptionsHide->SetToolTipText ("Hide/Show the option panel");
      AddFrame (fOptionsHide, fL);
      fOptionsDialog = new TGPictureButton (this, fPicDialog, kGOptDialogID);
      fOptionsDialog->Associate (this);
      fOptionsDialog->SetToolTipText ("Show an option dialogbox");
      AddFrame (fOptionsDialog, fL);
      fOptionTabs = 0; // Create on demand only!
      // fOptionTabs = new TLGOptionTab (this, kGOptTabID, &fOptions,
                           // fPlotSet->PlotList());
      // fOptionTabs->Associate (this);
      // AddFrame (fOptionTabs, fL);
   
      // create graphics canvas
      fGraph = new TRootEmbeddedCanvas (fPadName, this, kCanvasWidth, 
                           kCanvasHeight);
      AddFrame (fGraph, fL);
      SetLayoutManager (new TLGPadLayout (this));
   
      // Register pad
      if (fPlotSet != 0) {
         fPlotSet->RegisterPad (this);
      }
   }

//______________________________________________________________________________
   TLGPad::~TLGPad ()
   {
      // Unregister pad
      if (fPlotSet != 0) {
         fPlotSet->UnregisterPad (this);
      }
      // delete graphs
      for (Int_t trace = 0; trace < kMaxTraces; trace++) {
         delete fData[trace];
         delete fPlot[trace];
      }
      if (fHFrame) delete fHFrame; //hist (mito)
      if (fStatBox) delete fStatBox;
      // delete legend
      delete fLegend;
      // delete parameters
      for (int i = 0; i < 3; i++) {
         delete fParamText[i];
      }
      // delete cursors
      delete fCursorLine[0];
      delete fCursorLine[1];
      delete fCursorMarker[0];
      delete fCursorMarker[1];
      delete fCursorTimer;
      // delete style
      delete fStyle;
      // delete GUI widgets
      delete fOptionsHide;
      delete fOptionsDialog;
      delete fOptionTabs;
      delete fGraph;
      delete fL;
      if(fDisconnectMessage)
      {
        delete fDisconnectMessage;
      }
   }

//______________________________________________________________________________
   void TLGPad::SetStoreOptionList (OptionAll_t** list, Int_t max) {
      fStoreOptions = list; 
      fStoreOptionsMax = max;
      if (fOptionTabs != 0) {
         fOptionTabs->SetStoreOptionList (list, max);
      }
      if (fOptionDialogbox != 0) {
         fOptionDialogbox->SetStoreOptionList (list, max);
      }
   }

//______________________________________________________________________________
   void TLGPad::PanelDialog (Bool_t newstate)
   {
      if (!fEnablePanelDialog || fPanelVisible) {
         return;
      }
   
      if (fOptionDialogbox == 0) {
         if (newstate) {
            // create new dialogbox
            fOptionDialogbox = new TLGOptionDialog (fClient->GetRoot(), 
                                 this, fPadName, &fOptions,
                                 fPlotSet->GetPlotMap(), 
                                 fStoreOptions, fStoreOptionsMax,
                                 &fXUnitList, &fYUnitList);
         }
         else {
            // nothing to do
            return;
         }
      }
      else {
         if (newstate) {
            // send to foreground
            fOptionDialogbox->RaiseWindow();
         }
         else {
            // dismiss
            SendMessage (fOptionDialogbox, 
                        MK_MSG ((EWidgetMessageTypes)kC_OPTION, 
                               (EWidgetMessageTypes)kCM_OPTCLOSE), fId, 0);
            fOptionDialogbox = 0;
         }
      }
   }

//______________________________________________________________________________
   void TLGPad::HidePanel (Bool_t newstate)
   {
      if ((newstate != fHidePanel) && fEnablePanel) {
         if (newstate || (fOptionDialogbox == 0)) {
            if (fOptionTabs == 0) {
               fOptionTabs = new TLGOptionTab (this, kGOptTabID, &fOptions,
                                    fPlotSet->GetPlotMap(), 
                                    fStoreOptions, fStoreOptionsMax,
                                    &fXUnitList, &fYUnitList);
               fOptionTabs->Associate (this);
               //TGLayoutHints *hint = new TGLayoutHints(kLHintsTop);
               AddFrame (fOptionTabs, fL);
               // move off screen to avoid flickering
               fOptionTabs->Move (-10000, -10000); 
               MapSubwindows ();
            }
            fHidePanel = newstate;
            if (!newstate) UpdateOptionPanels (kTRUE, kFALSE);
            Layout();
         }
      }
   }

//______________________________________________________________________________
   void TLGPad::LeftPanel (Bool_t newstate)
   {
      if (newstate != fLeftPanel) {
         fLeftPanel = newstate;
         Layout();
      }
   }

//______________________________________________________________________________
   void TLGPad::EnablePanel (Bool_t newstate)
   {
      if (newstate != fEnablePanel) {
         fEnablePanel = newstate;
         if (fEnablePanel) {
            fOptionsHide->SetState (kButtonUp);
         }
         else {
            fOptionsHide->SetState (kButtonDisabled);
            fHidePanel = kTRUE;
         }
         Layout();
      }
   }

//______________________________________________________________________________
   void TLGPad::EnablePanelDialog (Bool_t newstate)
   {
      if (newstate != fEnablePanelDialog) {
         fEnablePanelDialog = newstate;
         if (fEnablePanelDialog) {
            fOptionsDialog->SetState (kButtonUp);
         }
         else {
            fOptionsDialog->SetState (kButtonDisabled);
         }
         Layout();
      }
   }

//______________________________________________________________________________
   Bool_t checkPlotLimits (Float_t& x) 
   {
      if (fabs (x) < kPlotLimit) {
         return kTRUE;
      }
      else if (x < 0) {
         x = - kPlotLimit;
         return kFALSE;
      }
      else {
         x = kPlotLimit;
         return kFALSE;
      }
   }

//______________________________________________________________________________
   Bool_t checkPlotLimits (Double_t& x) 
   {
      if (fabs (x) < kPlotLimit) {
         return kTRUE;
      }
      else if (x < 0) {
         x = (Double_t) ( - kPlotLimit);
         return kFALSE;
      }
      else {
         x = (Double_t) kPlotLimit;
         return kFALSE;
      }
   }

//______________________________________________________________________________
   Float_t TLGPad::ConvX (Float_t x) const
   {
      if (!finite(x)) {
         return 0;
      }
      checkPlotLimits (x);
      if (fOptions.fUnits.fXValues == kUnitAngularF) {
         x *= 2. * TMath::Pi();
      }
      return fOptions.fUnits.fXSlope * 
         ((fExtraXSlope * (x + fExtraTimeShift) - fExtraXOffset) -
         fOptions.fUnits.fXOffset);
      //return fOptions.fUnits.fXSlope * (x -  fOptions.fUnits.fXOffset);
   }

//______________________________________________________________________________
   Double_t TLGPad::ConvX (Double_t x) const
   {
      if (!finite(x)) {
         return 0;
      }
      checkPlotLimits (x);
      if (fOptions.fUnits.fXValues == kUnitAngularF) {
         x *= 2. * TMath::Pi();
      }
      return fOptions.fUnits.fXSlope * 
         ((fExtraXSlope * (x + fExtraTimeShift) - fExtraXOffset) -  
         fOptions.fUnits.fXOffset);
      //return (Double_t) fOptions.fUnits.fXSlope * (x -  fOptions.fUnits.fXOffset);
   }

//______________________________________________________________________________
   Float_t TLGPad::ConvY (Float_t y, Bool_t pwrcorr) const
   {
      if (!finite(y)) {
         return 0;
      }
      checkPlotLimits (y);
      return (pwrcorr ? 0.5 : 1.) *
         fOptions.fUnits.fYSlope * (y -  fOptions.fUnits.fYOffset);
   }

//______________________________________________________________________________
   Double_t TLGPad::ConvY (Double_t y, Bool_t pwrcorr) const
   {
      if (!finite(y)) {
         return 0;
      }
      checkPlotLimits (y);
      return (pwrcorr ? 0.5 : 1.) *
         (Double_t) fOptions.fUnits.fYSlope * (y -  fOptions.fUnits.fYOffset);
   }

//______________________________________________________________________________
   Bool_t TLGPad::DisplayChanged (Int_t trace, const PlotDescriptor* plotd, 
                     Int_t& n1, Int_t& n2, Int_t& bin, 
                     Bool_t& logspacing, EDataCopy& cpy)
   {
      if ((plotd == 0) || (trace < 0) || (trace >= kMaxTraces)) {
         fLastDisp[trace].fValid = kFALSE;
         return kFALSE;
      }
   
      Bool_t oldrangeok = kTRUE; // hist (mito)
      if (PlotTypeId(plotd->GetGraphType()) != 11) { // hist (mito)
      
         // compute range
         n1 = 0;
         n2 = plotd->GetData()->GetN() - 1;
         if (fOptions.fRange.fRange[0] == kRangeManual) {
            Double_t min = fOptions.fRange.fRangeFrom[0];
            Double_t max = fOptions.fRange.fRangeTo[0];
            if (min > max) {
               Double_t temp = min; min = max; max = temp;
            }
            while ((n1 < n2 - 1) && 
                  (ConvX (plotd->GetData()->GetX()[n1+1]) < min)) {
               n1++;
            }
            while ((n1 < n2 - 1) && 
                  (ConvX(plotd->GetData()->GetX()[n2-1]) > max)) {
               n2--;
            }
            n1 -= (n2 - n1) / 10;
            n2 += (n2 - n1) / 10;
            if (n1 < 0) n1 = 0;
            if (n2 >= plotd->GetData()->GetN()) {
               n2 = plotd->GetData()->GetN() - 1;
            }
         }
      
         // compute binning
         bin = fOptions.fRange.fBin;
         if (bin < 1) bin = 1;
         if (fOptions.fConfig.fAutoBin && 
            ((n2 - n1) / bin > maxDisplayPoints)) {
            Int_t newbin = (n2 - n1) / bin / maxDisplayPoints;
            if (newbin > 1) bin *= newbin;
         }
         n1 = bin * (n1 / bin);
         n2 = bin * (n2 / bin) + bin - 1;
         if (n2 >= plotd->GetData()->GetN()) n2 -= bin;
      
         // check on log spacing (no log bining if data is not
         // linearly spaced
         logspacing = fOptions.fRange.fBinLogSpacing;
         if (logspacing) {
            if (n2 > n1 + 1) {
               //  just check 2 intervals in the middle
               Int_t i = (n1 + n2) / 2;
               Double_t dx1 = plotd->GetData()->GetX()[i] - 
                  plotd->GetData()->GetX()[i-1];
               Double_t dx2 = plotd->GetData()->GetX()[i+1] - 
                  plotd->GetData()->GetX()[i];
               if (dx1 - dx2 > 1e-8 * (dx1 + dx2)) {
                  logspacing = kFALSE;
               }
               else {
                  // ignore first point if zero
                  if (plotd->GetData()->GetX()[n1] == 0) {
                     n1++; n2++;
                     if (n2 >= plotd->GetData()->GetN()) n2 -= bin;
                  }
               }
            }
            else {
               logspacing = kFALSE;
            }
         }
      
         // compute conversion type
         cpy = kDCpyAsIs;
         cpy = (EDataCopy) fOptions.fUnits.fYValues;
         if (cpy == kDCpyAsIs) {
            cpy = TMath::Odd (trace) ? kDCpyImaginary : kDCpyReal;
         }
      
         // check if old range would do 
         // Bool_t oldrangeok = kTRUE;
         if (fOptions.fRange.fRange[0] == kRangeManual) {
            Double_t min = fOptions.fRange.fRangeFrom[0];
            Double_t max = fOptions.fRange.fRangeTo[0];
            if (min > max) {
               Double_t temp = min; min = max; max = temp;
            }
            // check if old N1 would do
            if (!fLastDisp[trace].fValid || 
               (fLastDisp[trace].fN1 >= n2) || 
               (ConvX (plotd->GetData()->GetX()[fLastDisp[trace].fN1]) > min)) {
               oldrangeok = kFALSE;
            }
            // check if old N2 would do
            if (!fLastDisp[trace].fValid || 
               (fLastDisp[trace].fN2 >= plotd->GetData()->GetN()) || 
               (ConvX (plotd->GetData()->GetX()[fLastDisp[trace].fN2]) < max)) {
               oldrangeok = kFALSE;
            }
            // check if old range is significantly larger
            if (!fLastDisp[trace].fValid || 
               (fLastDisp[trace].fN2 - fLastDisp[trace].fN1 > 2 * (n2 - n1))) {
               oldrangeok = kFALSE;
            }
         }
         else {
            // check if old range would do
            if ((fLastDisp[trace].fN1 != n1) || (fLastDisp[trace].fN2 != n2)) {
               oldrangeok = kFALSE;
            }
         }
      
      } 
      else { // hist (mito)
      
         n1 = 0;
         n2 = plotd->GetData()->GetN();
         if (fOptions.fRange.fRange[0] == kRangeManual) {
            Double_t min = fOptions.fRange.fRangeFrom[0];
            Double_t max = fOptions.fRange.fRangeTo[0];
            if (min > max) {
               Double_t temp = min; min = max; max = temp;
            }
            while (n1 < n2 && plotd->GetData()->GetXBinEdges()[n1] < min) n1++;
            while (n1 < n2 && plotd->GetData()->GetXBinEdges()[n2] > max) n2--;
         }
      
         if (!fLastDisp[trace].fValid || 
            (fLastDisp[trace].fN1 != n1) || (fLastDisp[trace].fN2 != n2) ||
            fOptions.fRange.fRangeFrom[0] < 
            plotd->GetData()->GetXBinEdges()[0] ||
            fOptions.fRange.fRangeTo[0] > 
            plotd->GetData()->GetXBinEdges()[plotd->GetData()->GetN()+1])
            oldrangeok = kFALSE;
         bin = fOptions.fRange.fBin;
         if (bin<1) bin=1;
         logspacing = kFALSE;
         // cpy = kDCpyMagnitude;
         cpy = kDCpyAsIs;
         cpy = (EDataCopy) fOptions.fUnits.fYValues;
         if (cpy == kDCpyAsIs) {
            cpy = TMath::Odd (trace) ? kDCpyImaginary : kDCpyReal;
         }
      }
   
      // now check if old display data can be reused
      if ((fOriginalPlotD[trace] != plotd) ||
         !fLastDisp[trace].fValid || 
         (fabs (fLastDisp[trace].fLastTimeShift - fExtraTimeShift) > 1E-8) ||
         (fLastDisp[trace].fLastXConv != fOptions.fUnits.fXValues) ||
         (fLastDisp[trace].fLastDCpy != cpy) ||
         (fLastDisp[trace].fBin != bin) ||
         (fLastDisp[trace].fLogspacing != logspacing) ||
         (fLastDisp[trace].fScaling[0] != fOptions.fUnits.fXSlope) ||
         (fLastDisp[trace].fScaling[1] != fOptions.fUnits.fYSlope) ||
         (fLastDisp[trace].fOffset[0] != fOptions.fUnits.fXOffset) ||
         (fLastDisp[trace].fOffset[1] != fOptions.fUnits.fYOffset) ||
         (fLastDisp[trace].fUnitX != fOptions.fUnits.fXUnit) ||
         (fLastDisp[trace].fUnitY != fOptions.fUnits.fYUnit) ||
         (fLastDisp[trace].fMagX != fOptions.fUnits.fXMag) ||
         (fLastDisp[trace].fMagY != fOptions.fUnits.fYMag) ||
         !oldrangeok)
	 {
         fLastDisp[trace].fValid = kTRUE;
         fLastDisp[trace].fLastTimeShift = fExtraTimeShift;
         fLastDisp[trace].fLastXConv = fOptions.fUnits.fXValues;
         fLastDisp[trace].fLastDCpy = cpy;
         fLastDisp[trace].fBin = bin;
         fLastDisp[trace].fLogspacing = logspacing;
         fLastDisp[trace].fScaling[0] = fOptions.fUnits.fXSlope;
         fLastDisp[trace].fScaling[1] = fOptions.fUnits.fYSlope;
         fLastDisp[trace].fOffset[0] = fOptions.fUnits.fXOffset;
         fLastDisp[trace].fOffset[1] = fOptions.fUnits.fYOffset;
         fLastDisp[trace].fN1 = n1;
         fLastDisp[trace].fN2 = n2;
         fLastDisp[trace].fUnitX = fOptions.fUnits.fXUnit;
         fLastDisp[trace].fUnitY = fOptions.fUnits.fYUnit;
         fLastDisp[trace].fMagX = fOptions.fUnits.fXMag;
         fLastDisp[trace].fMagY = fOptions.fUnits.fYMag;
         return kTRUE;
      }
      else {
         n1 = fLastDisp[trace].fN1;
         n2 = fLastDisp[trace].fN2;
         return kFALSE;
      }
   }

//______________________________________________________________________________
   void TLGPad::SetPlotData (Int_t trace, const PlotDescriptor* plotd)
   {
      if ((trace < 0) || (trace >= kMaxTraces)) {
         return;
      }
     
      // delete plot data
      if ((plotd == 0) || (plotd->GetData() == 0)) {
         delete fData[trace];
         fData[trace] = 0;
         //fLastDCpy[trace] = kDCpyAsIs;
         fOriginalPlotD[trace] = 0;
         fLastDisp[trace].fValid = kFALSE;
      }
      else {
         // extra time shift for time trace
         unsigned int t0;
         unsigned int t0nsec;
         double dt = 0.0;
         bool moveT0 = false;
         if ((PlotTypeId (fOptions.fTraces.fGraphType) == 0) &&
            (plotd->Param().GetStartTime (t0, t0nsec))) {
            if (fT0 == 0) {
               fT0 = t0;
               fT0NSec = t0nsec;
            }
            dt = double(t0) - double(fT0) + 
               (double (t0nsec) - double(fT0NSec)) / 1E9;
            if (fabs (dt) < 1E-8) dt = 0;
            if (fOptions.fConfig.fAutoTimeAdjust && (fabs (dt) > 1E-8) &&
               (fabs (dt) < kMaxTimeAdjust)) moveT0 = true;
         }
         fExtraTimeShift = moveT0 ? dt : 0.0;
         // first get unit descriptors and x scaling
         const calibration::Unit* calx = 
            plotd->Cal().FindUnit (kCalUnitX, fOptions.fUnits.fXUnit);
         const calibration::Unit* caly = 
            plotd->Cal().FindUnit (kCalUnitY, fOptions.fUnits.fYUnit);
         const calibration::UnitScaling* scalex = 
            dynamic_cast<const calibration::UnitScaling*> (calx);
         fExtraXSlope = calibration::Unit::Factor((calibration::Unit::EUnitMagnitude)
                              fOptions.fUnits.fXMag);
         fExtraXOffset = 0;
         if (scalex && !scalex->IsYScaling()) {
            fExtraXSlope *= scalex->GetSlope();
            fExtraXOffset += scalex->GetOffset();
         }
      
         // check if we have to recalculate the plot points
         Int_t n1;
         Int_t n2;
         Int_t bin;
         Bool_t logspacing;
         EDataCopy cpy;
         if (DisplayChanged (trace, plotd, n1, n2, bin, logspacing, cpy)) {
            if (PlotTypeId(fOptions.fTraces.fGraphType) != 11) {
               // copy data and bin it
               DataCopy* dcpy = new DataCopy ();
               if (dcpy != 0) {
                  Int_t len = (n2 - n1 + 1) / bin;
                  dcpy->SetData ((float*)0, (float*)0, 
                                len, plotd->GetData()->IsComplex());
                  sDataCopy (dcpy->GetX(), plotd->GetData()->GetX(),
                            kFALSE, n1, n2 - n1 + 1, bin, logspacing);
                  sDataCopy (dcpy->GetY(), plotd->GetData()->GetY(),
                            plotd->GetData()->IsComplex(), n1, n2 - n1 + 1, 
                            bin, logspacing);
               }
            
               // make calibration corrections
               Bool_t powerunit = kFALSE;
               Bool_t rmstrace = kFALSE;
               // Y cal first
               if (caly && (dcpy != 0)) {
                  rmstrace = 
                     dynamic_cast<const FreqRMSDataDescriptor*>(plotd->GetData()) ||
                     ((plotd->Cal().Domain(kCalUnitX) == kCalDomainFrequency) &&
                     strstr (plotd->GetAChannel(), "(RMS)"));
                  // unwrap RMS
                  if (rmstrace) {
                     RMSCorrection (dcpy, false);
                  }
               	  // Apply calibration
                  caly->Apply (dcpy->GetX(), dcpy->GetY(), dcpy->GetN(), 
                              (calibration::Unit::EUnitMagnitude) 
                              fOptions.fUnits.fYMag, dcpy->IsComplex());
                  powerunit = caly->IsPower();
               }
               // time adjust next
               if (moveT0) {
                  for (int i = 0; i < dcpy->GetN(); ++i) {
                     dcpy->GetX()[i] += fExtraTimeShift;
                  }
               }
               // finally apply x cal
               if (calx && (dcpy != 0)) {
                  calx->Apply (dcpy->GetX(), dcpy->GetY(), dcpy->GetN(), 
                              (calibration::Unit::EUnitMagnitude) 
                              fOptions.fUnits.fXMag, kFALSE);
               }
               if (caly && (dcpy != 0)) {
               	  // re-compute RMS
                  if (rmstrace) {
                     RMSCorrection (dcpy, true);
                  }
               }
               // compute displayed points, ie., magnitude
               delete fData[trace];
               fData[trace] = new DataCopy ();
               if ((fData[trace] != 0) && (dcpy != 0) &&
                  (DataCopyConvert (*(DataCopy*)fData[trace], *dcpy, cpy))) {
                  //fLastDCpy[trace] = cpy;
                  fOriginalPlotD[trace] = plotd;
               }
               else {
                  delete fData[trace];
                  fData[trace] = 0;
                  //fLastDCpy[trace] = kDCpyAsIs;
                  fOriginalPlotD[trace] = 0;
                  fLastDisp[trace].fValid = kFALSE;
               }
               delete dcpy;
               // apply display scaling
               if (powerunit && (cpy != TLGPad::kDCpydBMagnitude)) {
                  powerunit = kFALSE;
               }
               fExtraXSlope = 1;
               fExtraXOffset = 0;
               fExtraTimeShift = 0;
               if (fData[trace] != 0) {
                  for (int i = 0; i < fData[trace]->GetN(); i++) {
                     fData[trace]->GetX()[i] = ConvX (fData[trace]->GetX()[i]);
                     fData[trace]->GetY()[i] = 
                        ConvY (fData[trace]->GetY()[i], powerunit);
                  }
               }
            
            
            }
            else { // SetData for hist (mito)
            
               if(fOptions.fAxisX.fAxisTitle == "") {
                  fOptions.fAxisX.fAxisTitle = plotd->GetData()->GetXLabel();
                  if(fOptions.fAxisX.fAxisTitle == "") {
                     fOptions.fAxisX.fAxisTitle = "X Axis";
                  }
               }
            
               if(fOptions.fAxisY.fAxisTitle == "") {
                  fOptions.fAxisY.fAxisTitle = plotd->GetData()->GetNLabel();
                  if(fOptions.fAxisY.fAxisTitle == "") {
                     fOptions.fAxisY.fAxisTitle = "Count";
                  }
               }
            
               HistDataCopy* hdcpy = new HistDataCopy();
               if (bin < 1 && bin >= plotd->GetData()->GetN()) bin = 1;
            
               // rebinning
               Int_t nbin = plotd->GetData()->GetN();
               nbin = nbin/bin + (nbin%bin > 0);
               Double_t* edges = new Double_t[nbin+1];
               Double_t* contents = new Double_t[nbin+2];     
               for(Int_t i=0; i<nbin+2;i++) contents[i]=0;
               contents[0]=plotd->GetData()->GetBinContents()[0];
               contents[nbin+1]=plotd->GetData()->GetBinContents()[plotd->GetData()->GetN()+1];
               edges[nbin]=plotd->GetData()->GetXBinEdges()[plotd->GetData()->GetN()];
               for(Int_t i=1 ; i<=plotd->GetData()->GetN(); i++) {
                  contents[(i-1)/bin + 1] += plotd->GetData()->GetBinContents()[i];
                  if ( (i-1)%bin == 0 ) edges[(i-1)/bin] =  plotd->GetData()->GetXBinEdges()[i-1];
               }
            
               // y range convert (magnitude,dB,phase and etc.)
               for (Int_t i=0;i<nbin+2;i++) DataCopyConvertY(cpy,contents[i],contents[i]);
            
               // x & y unit convert
               Bool_t powerunit = kFALSE;
               if (powerunit && (cpy != TLGPad::kDCpydBMagnitude)) {
                  powerunit = kFALSE;
               }
               for (Int_t i=0; i<nbin+2;i++) contents[i]=ConvY(contents[i],powerunit)*
                     calibration::Unit::Factor ((calibration::Unit::EUnitMagnitude)
                                       fOptions.fUnits.fYMag);
               for (Int_t i=0; i<nbin+1;i++) edges[i]=ConvX(edges[i]);
            
               Int_t nent = plotd->GetData()->GetNEntries();
            
               // copy temp array to temp HistDataCopy
               hdcpy->SetData(edges,contents,nbin,
                             plotd->GetData()->GetXLabel(),
                             plotd->GetData()->GetNLabel(),
                             nent,plotd->GetData()->GetStats(),
                             plotd->GetData()->IsXY());
            
               delete fData[trace];
               fData[trace] = new HistDataCopy();
               if (fData[trace] != 0 && plotd->GetData() != 0) {
                  ((HistDataCopy*)fData[trace])->SetData(*hdcpy);
                  fOriginalPlotD[trace] = plotd;
               } 
               else {
                  delete fData[trace];
                  fData[trace]=0;
                  fOriginalPlotD[trace] = 0;
                  fLastDisp[trace].fValid = kFALSE;
               }
               delete hdcpy;
            }
	 }
         fExtraXSlope = 1;
         fExtraXOffset = 0;
         fExtraTimeShift = 0;
      }
   }

//______________________________________________________________________________
   void TLGPad::DrawPlot ()
   {
      // set global parameters first
      TCanvas* canv = GetCanvas();
      if (canv != 0) {
         // set canvas non-editable and reset background color
         canv->SetDoubleBuffer();
         //canv->SetEditable (kFALSE);
         canv->SetEditable (); // (1-8-2002 mito)
         canv->SetFillColor (fStyle->GetCanvasColor());
         if (gVirtualPS == 0) {
            canv->Clear();  // hist (mito)
         }
         canv->SetLeftMargin (0.01);
         canv->SetRightMargin (0.01);
         canv->SetTopMargin (0.01);
         canv->SetBottomMargin (0.01);
         canv->SetLeftMargin (fOptions.fStyle.fMargin[0]);
         canv->SetRightMargin (fOptions.fStyle.fMargin[1]);
         canv->SetTopMargin (fOptions.fStyle.fMargin[2]);
         canv->SetBottomMargin (fOptions.fStyle.fMargin[3]);
         // delete old title
         TObject* oldtitle = canv->GetPrimitive ("title");
         if (oldtitle != 0) {
            delete oldtitle;
         }
         // set grid, ticks and log
         canv->SetGrid (fOptions.fAxisX.fGrid, fOptions.fAxisY.fGrid);
         canv->SetTicks (fOptions.fAxisX.fBothSides, 
                        fOptions.fAxisY.fBothSides);
         canv->SetLogx (fOptions.fRange.fAxisScale[0] == kAxisScaleLog);
         canv->SetLogy (fOptions.fRange.fAxisScale[1] == kAxisScaleLog);
      }
      if (fStyle != 0) {
         // set title attributes and position
         fStyle->SetTitleFont (fOptions.fStyle.fTitleAttr.GetTextFont());
         fStyle->SetTitleTextColor 
            (fOptions.fStyle.fTitleAttr.GetTextColor());
         fStyle->SetTitleH (fOptions.fStyle.fTitleAttr.GetTextSize());
         fStyle->SetTitleY (0.995);
         Float_t wt = TMath::Min (0.6, 0.05 + 0.015 * 
                              fOptions.fStyle.fTitle.Length());
         Int_t align = fOptions.fStyle.fTitleAttr.GetTextAlign();
         if (align / 10 == 2) {
            fStyle->SetTitleX ((1. - wt) / 2.);
         }
         else if (align / 10 == 3) {
            fStyle->SetTitleX (1. - wt - 0.01);
         }
         else {
            fStyle->SetTitleX (0.01);
         }
      }
      // time offset is zero
      fT0 = 0;
      fT0NSec = 0;
      // loop over traces
      for (Int_t tr = 0; tr < kMaxTraces; tr++) {
         DrawPlot (tr);
      }
      // set bar offsets
      Int_t barnum = 0;
      for (Int_t tr = 0; tr < kMaxTraces; tr++) {
         if ((fPlot[tr] != 0) && (fPlotId[tr] == 0) &&
            (fOptions.fTraces.fPlotStyle[tr] == kPlotStyleBar)) {
            barnum++;
         }
      }
      if (barnum > 1) {
         Int_t bar = 0;
         for (Int_t tr = 0; tr < kMaxTraces; tr++) {
            if ((fPlot[tr] != 0) && (fPlotId[tr] == 0) &&
               (fOptions.fTraces.fPlotStyle[tr] == kPlotStyleBar)) { 
               ((TLGraph*)fPlot[tr])->SetBarOffset 
                  (0.5 * bar / (barnum + 1.));
               bar++;
            }   
         }
      }
   }

//______________________________________________________________________________
   void TLGPad::DrawPlot (Int_t trace)
   {
      // check range of trace
      if ((trace < 0) || (trace >= kMaxTraces)) {
         return;
      }
      // get plot descriptor
      const PlotDescriptor* plotd = 0;
      if (fOptions.fTraces.fActive[trace]) {
         plotd = fPlotSet->Get (fOptions.fTraces.fGraphType, 
                              fOptions.fTraces.fAChannel[trace],
                              fOptions.fTraces.fBChannel[trace]);
         if (plotd == 0) {
            plotd = fPlotSet->Get (fOptions.fTraces.fGraphType, 
                                 fOptions.fTraces.fAChannel[trace], 0);
         }
      }
   
      // delete old graph
      delete fPlot[trace];
      fPlot[trace] = 0;
      fPlotId[trace] = -1;
   
   
      // finished if trace is inactive
     if ((plotd == 0) || !fOptions.fTraces.fActive[trace]) {
         SetPlotData (trace, 0);
         return;
      }
      // set data
      SetPlotData (trace, plotd);
      // check if valid data arrays
      if ( PlotTypeId(fOptions.fTraces.fGraphType) != 11) { // hist mito
         if ((fData[trace] == 0) ||
            (fData[trace]->GetX() == 0) || 
            (fData[trace]->GetY() == 0)) {
            SetPlotData (trace, 0);
           return;
         }
      }
      else {
         if ((fData[trace] == 0) ||
            (fData[trace]->GetXBinEdges() == 0) || 
            (fData[trace]->GetBinContents() == 0)){ // hist mito
            SetPlotData (trace, 0);
            return;
         }
      }
   
      // add new graph
      if (PlotTypeId(fOptions.fTraces.fGraphType) == 11) { // hist (mito)
      
         std::string hname,objname;
         hname = "__H";
         objname = "__H";
         int i = 0;
         while(gROOT->FindObject(objname.c_str())) {
            char sfx[10];
            sprintf(sfx,"%d",i);
            objname = hname + sfx;
            ++i;
         }
      
         if ( fOptions.fParam.fUOBins ) {
            Int_t nbins_org = fData[trace]->GetN();
            Int_t nbins = nbins_org + 2;
            const double* edges_org = fData[trace]->GetXBinEdges();
            double* edges = new double[ nbins + 1 ];
            memcpy(edges + 1, edges_org, (nbins_org + 1) * sizeof(double) );
            edges[0] = 2 * edges_org[0] - edges_org[1];
            edges[nbins] = 2 * edges_org[nbins_org] - edges_org[nbins_org - 1];
            fPlot[trace] = new TH1F (objname.c_str(),"1DHistogram", nbins, edges );
            delete[] edges;
         }
         else {
            fPlot[trace] = new TH1F (objname.c_str(),"1DHistogram", 
                                 fData[trace]->GetN(),
                                 fData[trace]->GetXBinEdges());
         }
      
         Int_t offset = (fOptions.fParam.fUOBins) ? 1 : 0;
         for (Int_t i = 0; i < fData[trace]->GetN()+2; i++) { // fill histogram bins
            ((TH1F*)fPlot[trace])->SetBinContent
               (i + offset, (Stat_t)fData[trace]->GetBinContents()[i]);
         }
      
         ((TH1F*)fPlot[trace])->SetEntries(fData[trace]->GetNEntries());
         Stat_t stat[4];
         for (int i=0;i<4;i++) stat[i] = (Stat_t)(fData[trace]->GetStats()[i]);
         ((TH1F*)fPlot[trace])->PutStats(stat); // set statistics
         ((TH1F*)fPlot[trace])->SetStats(0);
      
         fPlotId[trace] = 3;
      }
      else if ((fData[trace]->GetEX()==0)&&(fData[trace]->GetEY()==0)){
         fPlot[trace] = new TLGraph (this, fData[trace]->GetN(), 
                              fData[trace]->GetX(), fData[trace]->GetY());
         fPlotId[trace] = 0;
         ((TLGraph*)fPlot[trace])->SetBarWidth(fOptions.fTraces.fBarWidth[trace]);
      }
      else if ((fData[trace]->GetEXhigh() == 0) && 
              (fData[trace]->GetEYhigh() == 0)) {
         fPlotId[trace] = 1;
         fPlot[trace] = new TGraphErrors (fData[trace]->GetN(), 
                              fData[trace]->GetX(), fData[trace]->GetY(), 
                              fData[trace]->GetEX(), fData[trace]->GetEY());
      }
      else {
         fPlotId[trace] = 2;
         fPlot[trace] = new TGraphAsymmErrors
            (fData[trace]->GetN(), 
            fData[trace]->GetX(), fData[trace]->GetY(), 
            fData[trace]->GetEX(), fData[trace]->GetEXhigh(), 
            fData[trace]->GetEY(), fData[trace]->GetEYhigh());
      }

      // set style: line and marker
      if (fPlotId[trace] != 3) {
         fOptions.fTraces.fLineAttr[trace].Copy (*(TGraph*)fPlot[trace]);
         fOptions.fTraces.fMarkerAttr[trace].Copy (*(TGraph*)fPlot[trace]);
         fOptions.fTraces.fBarAttr[trace].Copy (*(TGraph*)fPlot[trace]);
      
         switch (fOptions.fTraces.fPlotStyle[trace]) {
            case kPlotStyleLine:
               strcpy (fPlotDrawOption[trace], "L");
               break;
            case kPlotStyleMarker: 
               strcpy (fPlotDrawOption[trace], "P");
               break;
            case kPlotStyleLineMarker:
               strcpy (fPlotDrawOption[trace], "LP");
               break;
            case kPlotStyleBar:
               strcpy (fPlotDrawOption[trace], "B");
               break;
         
         }
      }
      else { // hist (mito)
      
         fOptions.fTraces.fLineAttr[trace].Copy (*(TH1F*)fPlot[trace]);
         fOptions.fTraces.fMarkerAttr[trace].Copy (*(TH1F*)fPlot[trace]);
         fOptions.fTraces.fBarAttr[trace].Copy (*(TH1F*)fPlot[trace]);
      
         if (fOptions.fTraces.fPlotStyle[trace] == kPlotStyleLine) {
            if (fOptions.fTraces.fBarAttr[trace].GetFillStyle() != 0) {
               ((TH1F*)fPlot[trace])->SetFillStyle(0);
            }
         }
         else if (fOptions.fTraces.fPlotStyle[trace] == kPlotStyleBar) {
            if (fOptions.fTraces.fBarAttr[trace].GetFillStyle() != 0) {
               ((TH1F*)fPlot[trace])->SetLineColor(1);
            }
         }
      }
   }

//______________________________________________________________________________
   void TLGPad::UpdatePlot (const BasicPlotDescriptor* plotd, bool force)
   {
#ifdef DEBUG_RANGE_SETTING
      cerr << "TLGPad::UpdatePlot()" << endl ;
#endif
      // get canvas
      TCanvas*	canv = GetCanvas();
      if (canv == 0) {
         return;
      }
      TVirtualPad* padsave = gPad;
      canv->cd();
   
      // check if plot descriptor is used by this pad
      Bool_t newdata = kFALSE;
      if (force || (plotd != 0)) {
         Bool_t needRedraw = kFALSE;
         for (Int_t trace = 0; trace < kMaxTraces; trace++) {
            if (force || (fOriginalPlotD[trace] == plotd)) {
               needRedraw = kTRUE;
               fOriginalPlotD[trace] = 0;
            }
         } 
         if (!needRedraw) {
            gPad = padsave;
            return;
         }
         newdata = kTRUE;
      }
   
      {
	 float x, y ;
	 gStyle->GetPaperSize(x, y) ;
	 // cerr << "TLGPad::UpdatePlot() gStyle paper size x=" << x 
	 //     << " y=" << y << endl ;
      }
      // draw graphs
   
      // Carry over the paper size from the global style. // JCB
      {
	 float x, y ;
	 gStyle->GetPaperSize(x, y) ;
	 fStyle->SetPaperSize(x, y) ;
      } // JCB -end

      TStyle*	savestyle = gStyle;
      gStyle = fStyle;
      DrawPlot ();
   
      // add units to axis title if needed
      TString axisTitle[2];
      axisTitle[0] = fOptions.fAxisX.fAxisTitle;
      axisTitle[1] = fOptions.fAxisY.fAxisTitle;
   
      // Determine units
#ifdef DEBUG_RANGE_SETTING
      cerr << "TLGPad::UpdatePlot() Determine Units" << endl ;
#endif
      for (int i = 0; i < 2; i++) {
      // skip if units are already present
         if (axisTitle[i].First ('(') >= 0) {
            continue;
         }
         TString unit = "";
      // check if conversion is phase
         EYRangeType cpy = (i == 1) ? fOptions.fUnits.fYValues :
            (EYRangeType) fOptions.fUnits.fXValues;
         if ((cpy == kUnitPhaseDeg) || (cpy == kUnitPhaseDegCont)) {
            unit = "deg";
#ifdef DEBUG_RANGE_SETTING
	    cerr << "  unit = deg" << endl ;
#endif
         }
         else if ((cpy == kUnitPhaseRad) || (cpy == kUnitPhaseRadCont)) {
            unit = "rad";
#ifdef DEBUG_RANGE_SETTING
	    cerr << "  unit = rad" << endl ;
#endif
         }
         // else loop over traces
         else {
            Bool_t first = kTRUE;
            for (Int_t trace = kMaxTraces - 1; trace >= 0; trace--) {
            // skip if invalid
               if ((fPlot[trace] == 0) || 
                  (fOriginalPlotD[trace] == 0) || 
                  !fOriginalPlotD[trace]->Cal().IsValid()) {
                  continue;
               }
            // get unit descriptor
               const calibration::Unit* cal = (i == 0) ?
                  fOriginalPlotD[trace]->Cal().FindUnit (
                                    kCalUnitX, fOptions.fUnits.fXUnit) : 
                  fOriginalPlotD[trace]->Cal().FindUnit (
                                    kCalUnitY, fOptions.fUnits.fYUnit);
            // take if first, compare to previous otherwise
               if (cal && first) {
                  unit = cal->GetTrueName();
                  first = kFALSE;
               }
               else if (cal && (unit != cal->GetTrueName())) {
                  unit = "*"; // star if units are not identical
               }
            }
#ifdef DEBUG_RANGE_SETTING
	    cerr << "  unit:" << unit << endl;
#endif
         }
         // add magnitude qualifier
         Int_t mag = (i == 0) ? 
            fOptions.fUnits.fXMag : fOptions.fUnits.fYMag;
         if (unit != "*") {
            if (unit.Length() > 0) {
               unit = calibration::Unit::Mag 
                  ((calibration::Unit::EUnitMagnitude)mag).c_str() + unit;
#ifdef DEBUG_RANGE_SETTING
	       cerr << "  Unit:" << unit << endl ;
#endif
            }
            else if (mag != 0) {
               char buf [64];
               sprintf (buf, "10^%i", mag);
               unit = buf;
#ifdef DEBUG_RANGE_SETTING
	       cerr << "  Unit mag = " << unit << endl ;
#endif
            }
         }
         // check if conversion is dB
         if (cpy == kUnitdBMagnitude) {
            unit = TString (unit.Length() > 0 ? "dB " : "dB") + unit;
#ifdef DEBUG_RANGE_SETTING
	    cerr << "  unit - Add dB" << endl;
#endif
         }
         // add it to axis title
         if (unit.Length() > 0) {
            axisTitle[i] += " (" + unit + ")";
#ifdef DEBUG_RANGE_SETTING
	    cerr << (i ? "  Y " : "  X ") << "axisTitle: " 
	    	 << axisTitle[i] << endl;
#endif
         }
      }
   
   
   
      // set Frame & fRangeMin & fRangeMax only for histograms
      if (PlotTypeId(fOptions.fTraces.fGraphType)==11) {
#ifdef DEBUG_RANGE_SETTING
	 cerr << "TLGPad::UpdatePlot() Set Frame, fRangeMin, "
	      <<"fRangeMax for histograms" << endl ;
#endif
         GetHistRange(fRangeMin[0],fRangeMin[1],fRangeMax[0],fRangeMax[1]);
         Double_t dx = fRangeMax[0] - fRangeMin[0];
         fRangeMax[0] += 0.1 * dx;
         fRangeMin[0] -= 0.1 * dx;
         Double_t dy = fRangeMax[1] - fRangeMin[1];
         fRangeMax[1] += 0.1 * dy;
         for (Int_t j=0 ; j<2; j++) {
            if (fOptions.fRange.fRange[j] == kRangeManual) {
               fRangeMin[j] = fOptions.fRange.fRangeFrom[j];
               fRangeMax[j] = fOptions.fRange.fRangeTo[j];
            }
         }
         for(Int_t i=0;i<2;i++) {
            if (fRangeMin[i] == fRangeMax[i]) fRangeMax[i]=fRangeMin[i]+1;
         }
         if (fOptions.fRange.fAxisScale[1] == kAxisScaleLog && fRangeMin[1]<=0 ) {
            fRangeMin[1] = 0.01;
         }
         fHFrame =  canv->DrawFrame(fRangeMin[0],fRangeMin[1],fRangeMax[0],fRangeMax[1],
                              fOptions.fStyle.fTitle);
      }
// JCB start
      // Fix axis range.  This needs to be done before the plots are drawn so the proper
      // x and y axis values are known.
      {
	 int trace = -1 ;

	 // Find the first defined trace.
	 for (trace = kMaxTraces-1; trace >= 0; trace--)
	    if (fPlot[trace])
	       break ;
	 // If a valid trace was found, use it.
	 if (trace >= 0 ) 
	 {
	    if(PlotTypeId(fOptions.fTraces.fGraphType)!=11)  // 11 is Histogram
	    {
#ifdef DEBUG_RANGE_SETTING
	       cerr << "TLGPad::UpdatePlot() Fix Axis Range" << endl ;
#endif
	       // j == 0, x axis, j == 1, y axis.
	       for (int j = 0; j < 2; j++) 
	       {
		  if (fOptions.fRange.fRange[j] == kRangeManual) 
		  {
		     // Since the range for the axis is set to manual, assume that the desired
		     // values have been set by the user.  These may have been read in from a 
		     // file as well.  The values will be taken from the graph options data.
#ifdef DEBUG_RANGE_SETTING
		     cerr << "  manual range for " << (j ? "y" : "x") 
			  << " axis " << endl ;
#endif
		     fRangeMin[j] = fOptions.fRange.fRangeFrom[j];
		     fRangeMax[j] = fOptions.fRange.fRangeTo[j];
		     // Make sure min is min and max is max.
		     if (fRangeMin[j] > fRangeMax[j]) 
		     {
			Double_t temp = fRangeMin[j];
			fRangeMin[j] = fRangeMax[j];
			fRangeMax[j] = temp;
		     }
		     if (j == 0) 
		     {
			// X axis
#ifdef DEBUG_RANGE_SETTING
			cerr << "    fPlotId[" << trace << "] = " 
			     << fPlotId[trace] << endl ;
#endif
			switch (fPlotId[trace]) 
			{
			   case 0:  // Time Series
#ifdef DEBUG_RANGE_SETTING
			      cerr << "  TLGraph::SetMinimumX(fRangeMin[0]) "
			           << fRangeMin[j] << endl ;
#endif
			      ((TLGraph*)fPlot[trace])->SetMinimumX (fRangeMin[j]);
#ifdef DEBUG_RANGE_SETTING
			      cerr << "  TLGraph::SetMaximumX(fRangeMin[0]) " 
				   << fRangeMax[j] << endl ;
#endif
			      ((TLGraph*)fPlot[trace])->SetMaximumX (fRangeMax[j]);
			      break;
			   case 1:  // Power Spectrum
			   case 2:  // Coherence
			      if (fRangeMin[j] >= 0) 
			      {
				 Double_t d = fRangeMax[j] - fRangeMin[j];
				 fRangeMin[j] += d / 12.;
				 fRangeMax[j] -= d / 12.;
#ifdef DEBUG_RANGE_SETTING
				 cerr << "  --- fRangeMin[0] = " 
				      << fRangeMin[j] << endl ;
				 cerr << "  --- fRangeMax[0] = " 
				      << fRangeMax[j] << endl ;
#endif
			      }
			      break;
			}
		     }
		     else 
		     {
			// Y axis - kRangeManual
			((TGraph*)fPlot[trace])->SetMinimum (fRangeMin[j]);
			((TGraph*)fPlot[trace])->SetMaximum (fRangeMax[j]);
#ifdef DEBUG_RANGE_SETTING
			cerr << "    calling TGraph::SetMinimum (fRangeMin[" 
			     << j << "]) " << fRangeMin[j] << endl ;
			cerr << "    calling TGraph::SetMaximum (fRangeMax[" 
			     << j << "]) " << fRangeMax[j] << endl ;
#endif
		     }
		  }
		  else 
		  // fOptions.fRange.fRange[j] == kRangeAutomatic
		  {
#ifdef DEBUG_RANGE_SETTING
		     cerr << "  automatic range for " << (j ? "y" : "x") 
			  << " axis " << endl ;
#endif
		     if ((j == 1) && 
			(fOptions.fRange.fRange[0] == kRangeManual)) 
		     {
#ifdef DEBUG_RANGE_SETTING
		       cerr << "    x axis is manual, calling GetRange for y axis" << endl ;
#endif
			// This gets the minimum and maximum y range between the minimum and maximum x values which were previously set.
			GetRange (j, fRangeMin[j], fRangeMax[j], fRangeMinPos[j],
				 fRangeMin, fRangeMax);
#ifdef DEBUG_RANGE_SETTING
			cerr << "      fRangeMin[" << j << "] = " 
			     << fRangeMin[j] << endl ;
			cerr << "      fRangeMax[" << j << "] = " 
			     << fRangeMax[j] << endl ;
			cerr << "      fRangeMinPos[" << j << "] = " 
			     << fRangeMinPos[j] << endl ;
#endif
			// JCB Add SetMinimum, SetMaximum to fix Y range bug 
			//     when X range is manual.
			((TGraph*)fPlot[trace])->SetMinimum (fRangeMin[j]);
			((TGraph*)fPlot[trace])->SetMaximum (fRangeMax[j]);
#ifdef DEBUG_RANGE_SETTING
			cerr << "      calling TGraph::SetMinimum (fRangeMin[" 
			     << j << "]) " << fRangeMin[j] << endl ;
			cerr << "      calling TGraph::SetMaximum (fRangeMax[" 
			     << j << "]) " << fRangeMax[j] << endl ;
#endif
			// JCB - end
		     }
		     else 
		     {
#ifdef DEBUG_RANGE_SETTING
		       cerr << "    " << (j ? "Y Axis, " : "X axis, ") 
		            << "fRange[0] == " 
		            << (fOptions.fRange.fRange[0] == kRangeManual ? 
		                 "kRangeManual" : "kRangeAutomatic") 
		            << endl ;
		       cerr << "    calling GetRange for " << (j ? "y" : "x") 
			    << " axis" << endl ;
#endif
			GetRange (j, fRangeMin[j], fRangeMax[j], fRangeMinPos[j]);
#ifdef DEBUG_RANGE_SETTING
			cerr << "      fRangeMin[" << j << "] = " 
			     << fRangeMin[j] << endl ;
			cerr << "      fRangeMax[" << j << "] = " 
			     << fRangeMax[j] << endl ;
			cerr << "      fRangeMinPos[" << j << "] = " 
			     << fRangeMinPos[j] << endl ;
#endif
		     }
		  }
	       }
	    }
	 
	    // get limits
#ifdef DEBUG_RANGE_SETTING
	    cerr << "  ComputeLimits" << endl ;
#endif
	    if (fPlotId[trace] == 0) {
#ifdef DEBUG_RANGE_SETTING
	       cerr << "   fPlotId[" << trace << "] == 0" << endl ;
#endif
	       ((TLGraph*)fPlot[trace])->ComputeLimits 
		  (fLimitX[0], fLimitY[0], fLimitX[1], fLimitY[1]);
	    }
	    else {
#ifdef DEBUG_RANGE_SETTING
	       cerr << "   fPlotId[" << trace << "] != 0" << endl ;
#endif
	       fLimitX[0] = fRangeMin[0];
	       fLimitX[1] = fRangeMax[0];
	       fLimitY[0] = fRangeMin[1];
	       fLimitY[1] = fRangeMax[1];
	    }
#ifdef DEBUG_RANGE_SETTING
	    cerr << "  fLimitX[] = " << fLimitX[0] << ", " << fLimitX[1] << endl ;
	    cerr << "  fLimitY[] = " << fLimitY[0] << ", " << fLimitY[1] << endl ;
#endif
	    // set bar delta
	    Int_t N = 0;
	    for (Int_t tr = 0; tr < kMaxTraces; tr++) {
	       if ((fData[tr] != 0) && (fData[tr]->GetN() > N)) {
#ifdef DEBUG_RANGE_SETTING
		  cerr << "  Set bar delta" << endl ;
#endif
		  N = fData[tr]->GetN();
	       }
	    }
	    Float_t bdelta = (fRangeMax[0] - fRangeMin[0]) /
	       (Float_t)(N > 1 ? N - 1 : 1);
	    for (Int_t tr = 0; tr < kMaxTraces; tr++) {
	       if ((fPlot[tr] != 0) && (fPlotId[tr] == 0)) {
		  ((TLGraph*)fPlot[tr])->SetBarDelta (0);
	       }
	       if ((fPlot[tr] != 0) && (fPlotId[tr] == 0) &&
		  (fOptions.fTraces.fPlotStyle[tr] == kPlotStyleBar)) {
		  ((TLGraph*)fPlot[tr])->SetBarDelta (bdelta);
	       }
	    }
	 }
      }
// JCB - end

      // add graphs to canvas: make sure plot 0 is on top
#ifdef DEBUG_RANGE_SETTING
      cerr << "TLGPad::UpdatePlot() Add graphs to canvas" << endl; 
#endif
      Bool_t	first = kTRUE;
      Int_t	axisgraph = -1;
      for (Int_t trace = kMaxTraces - 1; trace >= 0; trace--) {
         if (fPlot[trace] == 0) {
            continue;
         }
         // draw option
         char opt[12];
         if (fPlotId[trace] != 3){ // hist (mito)
            if (first) {
               strcpy (opt, "A");
               strcpy (opt + 1, fPlotDrawOption[trace]);
            }
            else {
               strcpy (opt, fPlotDrawOption[trace]);
            }
         }
         else {
            strcpy (opt, "same");
         } 

         // add graph - The axis needed to be defined before this, not after!
#ifdef DEBUG_RANGE_SETTING
	 cerr << "TLGPad::UpdatePlot() Call fPlot[" << trace 
	      << "]->Draw(" << opt << ")" << endl ;
#endif
         fPlot[trace]->Draw (opt);

#ifdef DEBUG_RANGE_SETTING
	 cerr << "  done with call to Draw." << endl;
#endif

         // set axis style; only works after first graph is drawn!
         if (first) {
            axisgraph = trace;
            for (int i = 0; i < 2; i++) {
               TAxis* axis;
               if (fPlotId[trace] == 3){ // hist (mito)
                  axis = (i == 0) ? fHFrame->GetXaxis() 
		                  : fHFrame->GetYaxis();
               }
               else {
                  axis = (i == 0) ? ((TGraph*)fPlot[trace])->GetXaxis() 
		                  : ((TGraph*)fPlot[trace])->GetYaxis();
               }
               OptionAxis_t* vals = (i == 0) ? &fOptions.fAxisX 
		                             : &fOptions.fAxisY;
               if ((axis == 0) || (vals == 0)) {
                  continue;
               }
               vals->fAxisAttr.Copy (*axis);
               axis->CenterTitle (vals->fCenterTitle);
               axis->SetTitle (axisTitle[i]);
            }
            // set title
            if (fPlotId[trace] != 3){ // hist (mito)
               ((TGraph*)fPlot[trace])->SetTitle (fOptions.fStyle.fTitle);
            }
         
            canv->Modified (kTRUE);
            first = kFALSE;
         }
      }

      // save plot type
      fPlotType = fOptions.fTraces.fGraphType;
   
      // set cursors & parameters & update canvas
      UpdateCursor (kTRUE, kTRUE, kFALSE);
      UpdateParameters ();
      UpdateLegend();
      UpdateDisconnectMessage();
      canv->Update();
   
      // update manual ranges if necessary
      if ((axisgraph != -1) && (fManualRangeUpdate != 0) &&
         (fPlot[axisgraph] != 0)) {
         for (int j = 0; j < 2; j++) {
            TAxis* axis;
            if (fPlotId[axisgraph] == 3) {  // hist (mito)
               axis = (j == 0) ?
                  ((TH1F*)fPlot[axisgraph])->GetXaxis() : 
                  ((TH1F*)fPlot[axisgraph])->GetYaxis();
            }
            else {
               axis = (j == 0) ?
                  ((TGraph*)fPlot[axisgraph])->GetXaxis() : 
                  ((TGraph*)fPlot[axisgraph])->GetYaxis();
            }
            if ((axis != 0) &&
               (fOptions.fRange.fRange[j] == kRangeAutomatic)) {
               if (((fManualRangeUpdate >> j >> j) & 0x01) != 0) {
                  fOptions.fRange.fRangeFrom[j] = axis->GetXmin();
               }
               if (((fManualRangeUpdate >> j >> j) & 0x02) != 0) {
                  fOptions.fRange.fRangeTo[j] = axis->GetXmax();
               }
            }
         }
         UpdateOptions();
         fManualRangeUpdate = 0;
      }
      gStyle = savestyle;
      gPad = padsave;
      canv->SetEditable(kFALSE); // (1-8-2002 mito)
#ifdef DEBUG_RANGE_SETTING
      cerr << "TLGPad::UpdatePlot() end" << endl ;
#endif
   }

//______________________________________________________________________________
   void TLGPad::UpdateOptionPanels (Bool_t panel, Bool_t dialog)
   {
      if (panel && (fOptionTabs != 0)) {
         fOptionTabs->UpdateOptions();
      }
      if (dialog && (fOptionDialogbox != 0)) {
         fOptionDialogbox->UpdateOptions();
      }
   }

   void TLGPad::UpdateDisconnectMessage()
   {
       // set default pad
       TCanvas*	canv = GetCanvas();
       if (canv == 0) {
           return;
       }
       TVirtualPad*	padsave = gPad;
       gPad = canv;
       //find out if we should show
       bool should_show = fPlot[0] != 0 && fProgramStatus.Disconnected();

       if(should_show)
       {

           if(fDisconnectMessage)
           {
               delete fDisconnectMessage;
           }
           fDisconnectMessage = new TPaveText(0.8,0.01,0.99,0.20,"NDC");
           fDisconnectMessage->AddText("Disconnected");
           fDisconnectMessage->AddText("from Data Source");
           //((TText*)fDisconnectMessage->GetListOfLines()->Last())->SetTextColor(kRed);
           //fDisconnectMessage

           unsigned int height = canv->GetWindowHeight();
           unsigned int width = canv->GetWindowWidth();
           const float refheight = 600.0f;
           const float refwidth = 800.0f;
           float size_mult_h = (float)height / refheight;
           float size_mult_w = (float)width / refwidth;
           float size_mult = min(size_mult_h, size_mult_w);
           fDisconnectMessage->SetTextSize(fOptions.fParam.fTextSize * size_mult);
           fDisconnectMessage->SetTextFont(fOptions.fStyle.fTitleAttr.GetTextFont());
           fDisconnectMessage->SetTextColor(kRed);
           fDisconnectMessage->Draw();
       }
       else  //don't show disconnect message
       {
       }
       gPad = padsave;
   }

//______________________________________________________________________________
   void TLGPad::UpdateLegend ()
   {
      // delete old legend
      if (fLegend) {
         delete fLegend;
         fLegend = 0;
      }
      // Show legend?
      if (!fOptions.fLegend.fShow) {
         return;
      }
      // build entries to legend
      TObject*  legendsym[kMaxTraces];
      TString	legendlabel[kMaxTraces];
      TString	legendopt[kMaxTraces];
      Int_t n = 0;
      Int_t maxlen = 0;
      for (int tr = 0; tr < kMaxTraces; tr++) {
         if ((fPlot[tr] == 0) || (fOriginalPlotD[tr] == 0)) {
            continue;
         }
         legendlabel[n] = "";
         if (fOptions.fLegend.fTextStyle == kLegendAutoText) {
            if (fOriginalPlotD[tr]->GetBChannel()) {
               legendlabel[n] = fOriginalPlotD[tr]->GetBChannel();
               legendlabel[n] += " / ";
            }
            legendlabel[n] += fOriginalPlotD[tr]->GetAChannel();
         }
         else {
            legendlabel[n] = fOptions.fLegend.fText[tr];
         }
         legendsym[n] = 0;
         legendopt[n] = "";
         if (fOptions.fLegend.fSymbolStyle == kLegendSameAsTrace) {
            if ((fOptions.fTraces.fPlotStyle[tr] == kPlotStyleMarker) ||
               (fOptions.fTraces.fPlotStyle[tr] == kPlotStyleLineMarker)) {
               legendsym[n] = fPlot[tr];
               legendopt[n] = "p";
            }
            else if (fOptions.fTraces.fPlotStyle[tr] == kPlotStyleLine) {
               legendsym[n] = fPlot[tr];
               legendopt[n] = "l";
            }
            else if (fOptions.fTraces.fPlotStyle[tr] == kPlotStyleBar) {
               legendsym[n] = fPlot[tr];
               legendopt[n] = "f";
            }
         }
         if (legendlabel[n].Length() > maxlen) {
            maxlen = legendlabel[n].Length();
         }
         n++;
      }
      if (n == 0) {
         return;
      }
   
      // set default pad
      TCanvas*	canv = GetCanvas();
      if (canv == 0) {
         return;
      }
      TVirtualPad*	padsave = gPad;
      gPad = canv;
      // calulate position
      Coord_t x1;
      Coord_t y1;
      Coord_t x2;
      Coord_t y2;
      Coord_t w = fOptions.fLegend.fSize * 1.25 * (maxlen + 1) / 110.;
      Coord_t h = fOptions.fLegend.fSize * 1.25 * n / 30. + 0.01;
      Coord_t bwx = 0.02 + fOptions.fLegend.fXAdjust;
      Coord_t bwy = 0.02 + fOptions.fLegend.fYAdjust;
      switch (fOptions.fLegend.fPlacement) {
         case kLegendTopRight:
         default:
            x2 = 1. - bwx - fOptions.fStyle.fMargin[1];
            y2 = 1. - bwy - fOptions.fStyle.fMargin[2];
            x1 = x2 - w;
            y1 = y2 - h;
            break;
         case kLegendBottomRight:
            x2 = 1. - bwx - fOptions.fStyle.fMargin[1];
            y1 = bwy + fOptions.fStyle.fMargin[3];
            x1 = x2 - w;
            y2 = y1 + h;
            break;
         case kLegendBottomLeft:
            x1 = bwx + fOptions.fStyle.fMargin[0];
            y1 = bwy + fOptions.fStyle.fMargin[3];
            x2 = x1 + w;
            y2 = y1 + h;
            break;
         case kLegendTopLeft:
            x1 = bwx + fOptions.fStyle.fMargin[0];
            y2 = 1. - bwy - fOptions.fStyle.fMargin[2];
            x2 = x1 + w;
            y1 = y2 - h;
            break;
      }
      // create a new legend
      fLegend = new TLegend (x1, y1, x2, y2, 0, "NDC");
      if (!fLegend) {
         gPad = padsave;
         return;
      }

      // Set fill color.  This is needed because starting with root 5.34.21,
      // the fill color started showing up as red.  Bugzilla 754
      fLegend->SetFillColor(kWhite) ;

      // add entries
      for (int i = 0; i < n; i++) {
         fLegend->AddEntry (legendsym[i], legendlabel[i], legendopt[i]);
      }
   
      // adjust font size
      fLegend->SetTextSize(0); // let the Legend compute the text size that fits!
      // fLegend->SetTextSize(0.7*h/n);
   
      // draw
      fLegend->Draw();
   
      gPad = padsave;
   }

//______________________________________________________________________________
   void TLGPad::UpdateParameters (Bool_t* allthesame)
   {
      string t0 = "";
      Bool_t t0Defined = kFALSE;
      Bool_t t0Same = kTRUE;
      string avg = "";
      Bool_t avgDefined = kFALSE;
      Bool_t avgSame = kTRUE;
      string bin;
      Bool_t binDefined = kFALSE;
      Bool_t binSame = kTRUE;
      string third = "";
      Bool_t thirdDefined = kFALSE;
      Bool_t thirdSame = kTRUE;
      if (allthesame) *allthesame = kTRUE;
      // reset text
      TCanvas*	canv = GetCanvas();
      if (canv == 0) {
         return;
      }
      TVirtualPad*	padsave = gPad;
      gPad = canv;
      // quit of not shown
      if (!fOptions.fParam.fShow) {
         for (int i = 0; i < 3; i++) {
            if (fParamText[i] != 0) {
               // fParamText[i]->SetTitle ("");
               delete fParamText[i];
               fParamText[i] = 0;
            }
         }
         gPad = padsave;
         return;
      }
      // get values
      Int_t cur = ((fOptions.fCursor.fTrace >= 0) && 
                  (fOptions.fCursor.fTrace < kMaxTraces)) ?
         fOptions.fCursor.fTrace : 0;
      Bool_t validcur = ((fPlot[cur] != 0) && (fOriginalPlotD[cur] != 0));
      if (validcur) {
         t0Defined = fOriginalPlotD[cur]->Param().GetStartTime (t0,
                              fOptions.fParam.fTimeFormatUTC,
                              -fLastDisp[cur].fLastTimeShift);
         avgDefined = fOriginalPlotD[cur]->Param().GetAverages (avg);
         thirdDefined = fOriginalPlotD[cur]->Param().GetThird (third);
         if (fLastDisp[cur].fBin > 1) {
            char binc[32];
            sprintf (binc, "Bin=%i", fLastDisp[cur].fBin);
            bin = binc;
            if (fLastDisp[cur].fLogspacing) bin += "L";
         }
         binDefined = kTRUE;
      }
      // check if parameters are identical for all traces
      for (int tr = 0; tr < kMaxTraces; tr++) {
         if ((fPlot[tr] == 0) || (fOriginalPlotD[tr] == 0) || 
            (validcur && (tr == cur))) {
            continue;
         }
         if (!validcur) {
            cur = tr;
            validcur = kTRUE;
            t0Defined = fOriginalPlotD[cur]->Param().GetStartTime (
                                 t0, fOptions.fParam.fTimeFormatUTC,
                                 -fLastDisp[cur].fLastTimeShift);
            avgDefined = fOriginalPlotD[cur]->Param().GetAverages (avg);
            thirdDefined = fOriginalPlotD[cur]->Param().GetThird (third);
            if (fLastDisp[cur].fBin > 1) {
               char binc[32];
               sprintf (binc, "Bin=%i", fLastDisp[cur].fBin);
               bin = binc;
               if (fLastDisp[cur].fLogspacing) bin += "L";
            }
            binDefined = kTRUE;
            continue;
         }
         // check T0
         string _t0 = "";
         if (t0Defined) {
            if (!fOriginalPlotD[tr]->Param().GetStartTime (_t0,
                                 fOptions.fParam.fTimeFormatUTC,
                                 -fLastDisp[tr].fLastTimeShift) ||
               (t0 != _t0)) {
               t0Same = kFALSE;
               if (allthesame) *allthesame = kFALSE;
            }
         }
         // check averages
         string _avg = "";
         if (avgDefined) {
            if (!fOriginalPlotD[tr]->Param().GetAverages (_avg) ||
               (avg != _avg)) {
               avgSame = kFALSE;
               if (allthesame) *allthesame = kFALSE;
            }
         }
         // check third
         string _third = "";
         if (thirdDefined) {
            if (!fOriginalPlotD[tr]->Param().GetThird (_third) ||
               (third != _third)) {
               thirdSame = kFALSE;
               if (allthesame) *allthesame = kFALSE;
            }
         }
      	 // check bin
         string _bin;
         if (fLastDisp[tr].fBin > 1) {
            char binc[32];
            sprintf (binc, "Bin=%i", fLastDisp[cur].fBin);
            _bin = binc;
            if (fLastDisp[cur].fLogspacing) _bin += "L";
         }
         if (bin != _bin) {
            binSame = kFALSE;
            if (allthesame) *allthesame = kFALSE;
         }
      }
      // if not at least one valid trace, return
      if (!validcur) {
         for (int i = 0; i < 3; i++) {
            if (fParamText[i] != 0) {
               //fParamText[i]->SetTitle ("");
               delete fParamText[i];
               fParamText[i] = 0;
            }
         }
         gPad = padsave;
         return;
      }
   
      // text attributes
      TAttText	attr (11, 0, 1, fOptions.fStyle.fTitleAttr.GetTextFont(),
                    fOptions.fParam.fTextSize);
      // draw text for T0
      if (t0Defined && fOptions.fParam.fT0) {
         if (!t0Same) {
            t0 = "*" + t0;
            attr.SetTextColor (fOptions.fTraces.fLineAttr[cur].GetLineColor());
         }
         else {
            attr.SetTextColor (1);
         }
         attr.SetTextAlign (11);
         if (fParamText[0]) {
            fParamText[0]->SetTitle (t0.c_str());
         }
         else {
            fParamText[0] = new TText (0.02, 0.009, t0.c_str());
            if (fParamText[0] != 0) {
               fParamText[0]->SetNDC();
               // fParamText[0]->Draw();
            }
         }
         fParamText[0]->Draw();  // hist (mito)
         if (fParamText[0] != 0) {
            attr.Copy (*fParamText[0]);
         }
      }
      else if (fParamText[0] != 0) {
         // fParamText[0]->SetTitle ("");
         delete fParamText[0];
         fParamText[0] = 0;
      }     
   
      // draw text for averages/binning
      if ((avgDefined || binDefined) && fOptions.fParam.fAvg) {
         string txt;
         if ((avgDefined && !avgSame) ||
            (binDefined && !binSame)) {
            txt = "*";
            attr.SetTextColor (fOptions.fTraces.fLineAttr[cur].GetLineColor());
         }
         else {
            attr.SetTextColor (1);
         }
         if (avgDefined) txt += avg;
         if (avgDefined && !bin.empty()) txt += "/";
         if (!bin.empty()) txt += bin;
         attr.SetTextAlign (21);
         if (fParamText[1]) {
            fParamText[1]->SetTitle (txt.c_str());
         }
         else {
            fParamText[1] = new TText (0.5, 0.009, txt.c_str());
            if (fParamText[1] != 0) {
               fParamText[1]->SetNDC();
               // fParamText[1]->Draw();
            }
         }
         fParamText[1]->Draw();   // hist (mito)
         if (fParamText[1] != 0) {
            attr.Copy (*fParamText[1]);
         }
      }
      else if (fParamText[1] != 0) {
         // fParamText[1]->SetTitle ("");
         delete fParamText[1];
         fParamText[1] = 0;
      }
   
      // draw text for third parameter
      if (thirdDefined && fOptions.fParam.fSpecial) {
         if (!thirdSame) {
            third = "*" + third;
            attr.SetTextColor (fOptions.fTraces.fLineAttr[cur].GetLineColor());
         }
         else {
            attr.SetTextColor (1);
         }
         attr.SetTextAlign (31);
         if (fParamText[2] != 0) {
            fParamText[2]->SetTitle (third.c_str());
         }
         else {
            fParamText[2] = new TText (0.98, 0.009, third.c_str());
            if (fParamText[2] != 0) {
               fParamText[2]->SetNDC();
               // fParamText[2]->Draw();
            }
         }
         fParamText[2]->Draw(); // hist (mito)
         if (fParamText[2] != 0) {
            attr.Copy (*fParamText[2]);
         }
      }
      else if (fParamText[2] != 0) {
         // fParamText[2]->SetTitle ("");
         delete fParamText[2];
         fParamText[2] = 0;
      }
   
      // draw stat box for hist (mito)
      if (PlotTypeId(fOptions.fTraces.fGraphType)==11 && 
         fOptions.fParam.fStat) {      
         fStatBox = new TPaveText(0.8,0.86,0.99,0.99,"NDC");
      
         Char_t stattext[50];
         Stat_t stat[4];
         for (int i=0;i<4;i++) stat[i] = (Stat_t)(fData[cur]->GetStats()[i]);
         // set text for number of entries
         sprintf(stattext,"NEnt = %d",fData[cur]->GetNEntries());
         fStatBox->AddText(stattext);
      
         // set text for mean
         Stat_t mean;
         if (stat[0] == 0) mean = 0;
         else mean = stat[2]/stat[0];
         sprintf(stattext,"Mean = %.3g",mean);
         fStatBox->AddText(stattext);
      
         // set text for standard deviation
         Stat_t sdev;
         if (stat[0] == 0) sdev = 0;
         else {
            sdev = TMath::Abs(stat[3]/stat[0]-stat[2]*stat[2]/(stat[0]*stat[0]));
            sdev = TMath::Sqrt(sdev);
         }
         sprintf(stattext,"SDev = %.3g",sdev);
         fStatBox->AddText(stattext);
      
      	// set text for underflow
         sprintf(stattext,"Under = %.3g",fData[cur]->GetBinContents()[0]);
         fStatBox->AddText(stattext);
      
      	// set text for overflow
         int nbin = fData[cur]->GetN();
         sprintf(stattext,"Over = %.3g",fData[cur]->GetBinContents()[nbin+1]);
         fStatBox->AddText(stattext);
      
         fStatBox->SetTextSize(0.8*fOptions.fParam.fTextSize);
         fStatBox->SetTextFont(fOptions.fStyle.fTitleAttr.GetTextFont());
         if (fOptions.fTraces.fPlotStyle[cur] == kPlotStyleLine) {
            fStatBox->SetTextColor(fOptions.fTraces.fLineAttr[cur].GetLineColor());
         }
         else if (fOptions.fTraces.fPlotStyle[cur] == kPlotStyleBar) {
            fStatBox->SetTextColor(fOptions.fTraces.fBarAttr[cur].GetFillColor());
         }
         fStatBox->SetTextAlign(12);
      
         fStatBox->Draw();
      }
   
      gPad = padsave;
   }

//______________________________________________________________________________
   void TLGPad::UpdateCursor (Bool_t recompute, Bool_t draw, Bool_t update,
                     Int_t changeopt, Bool_t premodified)
   {
      OptionCursor_t* cur = &fOptions.fCursor;
      Bool_t modified = premodified;
   
      // compute new cursor values
      if (recompute) {
         // set valid traces
         for (int tr = 0; tr < kMaxTraces; tr++) {
            cur->fValid[tr] = (fPlot[tr] != 0) && (fData[tr] != 0);
         }
         // round cursor position to closest x value of current trace
         const Float_t* x;
         const Float_t* y;
         Float_t* hx = 0; // hist (mito)
         Float_t* hy = 0; // hist (mito)
         Int_t	  n;
      
         if ((cur->fTrace >= 0) && (cur->fTrace < kMaxTraces) &&
            (cur->fValid[cur->fTrace])) {
         
            n = fData[cur->fTrace]->GetN();
            if (fPlotId[cur->fTrace]==3) { // hist (mito)
               hx = new Float_t[n];
               for(Int_t i=0; i<n; i++) 
                  hx[i] = (fData[cur->fTrace]->GetXBinEdges()[i]+
                          fData[cur->fTrace]->GetXBinEdges()[i+1])/2;
               x = hx;
            }
            else {
               x = fData[cur->fTrace]->GetX();
            }
         
            for (int j = 0; j < 2; j++) {
               // get closest x position
               Int_t pos = -1;
               Float_t valx = 0;
               Float_t x0 = cur->fX[j];
               if ((j == 1) && (cur->fType == kCursorDifference) && 
                  cur->fActive[0] && cur->fActive[1]) {
                  x0 += cur->fX[0];
               }
               for (int i = 0; i < n; i++) {
                  if ((pos == -1) || 
                     (fabs (x[i] - x0) < fabs (valx - x0))) {
                     pos = i;
                     valx = x[i];
                  }
               }
               if (pos == -1) {
                  break;
               }
               // increase/decrease position if necessary
               if (changeopt / 1000000 - 1 == j) {
                  int sign = (changeopt % 1000000 / 10000 == 0) ? 1 : -1;
                  int stepsize = changeopt % 100;
                  if (stepsize == 0) {
                     stepsize = 1;
                  }
                  else if (stepsize == 1) {
                     stepsize = 10;
                  }
                  else if (stepsize == 2) {
                     stepsize = 100;
                  }
                  else {
                     stepsize = 1000;
                  }
                  pos += sign * stepsize;
                  if (pos < 0) pos = 0;
                  if (pos >= n) pos = n - 1;
               }
               // set new position
               cur->fX[j] = x[pos];
            }
            if (hx) { // hist (mito)
               delete[] hx;
               hx = 0;
            }
         }
         else {
            for (int i = 0; i < 2; i++) {
               if (cur->fX[i] < fRangeMin[0]) {
                  cur->fX[i] = fRangeMin[0];
               }
               if (cur->fX[i] > fRangeMax[0]) {
                  cur->fX[i] = fRangeMax[0];
               }
            }
         }
         // compute cursor values
         for (int tr= 0; tr < kMaxTraces; tr++) {
            cur->fY[tr][0] = 0;
            cur->fY[tr][1] = 0;
            if (cur->fValid[tr]) {
               n = fData[tr]->GetN();
               if (fPlotId[tr] == 3) { // hist (mito)
                  hx = new Float_t[n];
                  hy = new Float_t[n];
                  for(Int_t i=0; i<n; i++) {
                     hx[i]=(fData[tr]->GetXBinEdges()[i]+
                           fData[tr]->GetXBinEdges()[i+1])/2;
                     hy[i]=(Float_t) fData[tr]->GetBinContents()[i+1];
                  }
                  x = hx;
                  y = hy;
               }
               else {
                  x = fData[tr]->GetX();
                  y = fData[tr]->GetY();
               }
               for (int j = 0; j < 2; j++) {
                  // get closest x position
                  Int_t pos = -1;
                  Float_t valx = 0;
                  Float_t x0 = cur->fX[j];
                  if ((j == 1) && (cur->fType == kCursorDifference) && 
                     cur->fActive[0] && cur->fActive[1]) {
                     x0 += cur->fX[0];
                  }
                  for (int i = 0; i < n; i++) {
                     if ((pos == -1) || 
                        (fabs (x[i] - x0) < fabs (valx - x0))) {
                        pos = i;
                        valx = x[i];
                     }
                  }
                  if (pos != -1) {
                     cur->fY[tr][j] = y[pos];
                  }
               }
            }
            if (hx) { // hist (mito)
               delete[] hx;
               hx = 0;
            }
            if (hy) {
               delete[] hy;
               hy = 0;
            }
         }
         // compute statistics
         for (int tr= 0; tr < kMaxTraces; tr++) {
            cur->fN[tr] = 0;
            cur->fMean[tr] = 0;
            cur->fRMS[tr] = 0;
            cur->fStdDev[tr] = 0;
            cur->fSum[tr] = 0;
            cur->fSqrSum[tr] = 0;
            cur->fArea[tr] = 0;
            cur->fRMSArea[tr] = 0;
            cur->fPeakX[tr] = 0;
            cur->fPeakY[tr] = 0;
            cur->fCenter[tr] = 0;
            cur->fWidth[tr] = 0;
         
            if (!cur->fValid[tr]) {
               continue;
            }
            n = fData[tr]->GetN();
            if (fPlotId[tr]==3) { // hist (mito)
               hx = new Float_t[n];
               hy = new Float_t[n];
               for(Int_t i=0; i<n; i++) {
                  hx[i]=(fData[tr]->GetXBinEdges()[i]+
                        fData[tr]->GetXBinEdges()[i+1])/2;
                  hy[i]=fData[tr]->GetBinContents()[i+1];
               }
               x = hx;
               y = hy;
            }
            else {
               x = fData[tr]->GetX();
               y = fData[tr]->GetY();
            }
            if (n <= 0) {
               continue;
            }
            int pos[2];
            pos[0] = 0;
            Float_t x0[2];
            x0[0] = cur->fX[0];
            x0[1] = cur->fX[1];
            if (!cur->fActive[0]) {
               x0[0] = x[0];
            }
            if (!cur->fActive[1]) {
               x0[1] = x[n-1];
            }
            if ((cur->fType == kCursorDifference) && 
               cur->fActive[0] && cur->fActive[1]) {
               x0[1] += x0[0];
            }
            if (x0[0] > x0[1]) {
               Float_t tmp = x0[0];
               x0[0] = x0[1];
               x0[1] = tmp;
            }
            while ((pos[0] < n) && (x[pos[0]] < x0[0])) {
               pos[0]++;
            }
            pos[1] = n - 1;
            while ((pos[1] >= 0) &&(x[pos[1]] > x0[1])) {
               pos[1]--;
            }
            if (pos[0] > pos[1]) {
               continue;
            }
            cur->fN[tr] = pos[1] - pos[0] + 1;
            cur->fPeakX[tr] = x[pos[0]];
            cur->fPeakY[tr] = y[pos[0]];
            cur->fXDiff[tr] = x[pos[1]] - x[pos[0]];
            if (cur->fX[0] > cur->fX[1]) {
               cur->fXDiff[tr] *= -1.;
            }
            cur->fYDiff[tr] = y[pos[1]] - y[pos[0]];
            Double_t w;
            for (int i = pos[0]; i <= pos[1]; i++) {
               cur->fSum[tr] += y[i];
               cur->fSqrSum[tr] += y[i]*y[i];
               w = (n <= 1) ? 0 : 
                  ((i < n - 1) ? x[i+1] - x[i] : x[i] - x[i - 1]);
               cur->fArea[tr] += y[i] * w;
               cur->fRMSArea[tr] += y[i]*y[i] * w;
               if (y[i] > cur->fPeakY[tr]) {
                  cur->fPeakY[tr] = y[i];
                  cur->fPeakX[tr] = x[i];
               }
            
               cur->fCenter[tr] += y[i]*x[i];
               cur->fWidth[tr]  += y[i]*x[i]*x[i];
            
            }
            cur->fMean[tr] = cur->fSum[tr] / cur->fN[tr];
            cur->fRMS[tr] = TMath::Sqrt (cur->fSqrSum[tr] / cur->fN[tr]);
            cur->fRMSArea[tr] = TMath::Sqrt (cur->fRMSArea[tr]);
            cur->fCenter[tr] = (cur->fSum[tr]) ?  cur->fCenter[tr] / cur->fSum[tr] : 0;
            cur->fWidth[tr] = (cur->fSum[tr]) ? 
               TMath::Sqrt (cur->fWidth[tr]/cur->fSum[tr] - 
                           cur->fCenter[tr]*cur->fCenter[tr]) : 0;
            if (cur->fN[tr] > 1) {
               cur->fStdDev[tr] = TMath::Sqrt (1./(cur->fN[tr] - 1) * 
                                    (cur->fSqrSum[tr] - 1./cur->fN[tr]*
                                    cur->fSum[tr]*cur->fSum[tr]));
            }
            if (hx) { // hist (mito)
               delete[] hx;
               hx = 0;
            }
            if (hx) { // hist (mito)
               delete[] hy;
               hy = 0;
            }
         }
      
         // now update panel values
         const Long_t curnew = MK_MSG ((EWidgetMessageTypes) kC_OPTION, 
                              (EWidgetMessageTypes) kCM_OPTCURSORNEW);
         if (fOptionTabs != 0) {
            SendMessage (fOptionTabs, curnew, fId, 0);
         }
         if (fOptionDialogbox != 0) {
            SendMessage (fOptionDialogbox, curnew, fId, 0);
         }
      }
   
      // draw cursors
      if (draw) {
         TCanvas*	canv = GetCanvas();
         bool isEditable = canv->IsEditable(); // (1-8-2002 mito)
         canv->SetEditable(); // (1-8-2002 mito)
         TVirtualPad*	padsave = gPad;
         gPad = canv;
         for (int i = 0; i < 2; i++) {
            if (fCursorLine[i] != 0) {
               delete fCursorLine[i];
               fCursorLine[i] = 0;
               modified = kTRUE;
            }
            if (fCursorMarker[i] != 0) {
               delete fCursorMarker[i];
               fCursorMarker[i] = 0;
               modified = kTRUE;
            }
         }
         switch (cur->fStyle) {
            case kCursorInvisible:
               {
                  break;
               }
            case kCursorCross:
               {
                  if ((cur->fTrace < 0) || (cur->fTrace >= kMaxTraces) ||
                     (!cur->fValid[cur->fTrace])) {
                     break;
                  }
                  // compute cursor coordinates
                  double x[2];
                  double y[2];
                  x[0] = cur->fX[0];
                  x[1] = cur->fX[1];
                  y[0] = cur->fY[cur->fTrace][0];
                  y[1] = cur->fY[cur->fTrace][1];
                  if ((cur->fType == kCursorDifference) && 
                     cur->fActive[0] && cur->fActive[1]) {
                     x[1] += x[0];
                  }
                  if ((fPlot[cur->fTrace] != 0) && (fPlotId[cur->fTrace]==0)) {
                     Double_t dx = 0;
                     dx = ((TLGraph*)fPlot[cur->fTrace])->GetBarOffset() *
                        ((TLGraph*)fPlot[cur->fTrace])->GetBarDelta();
                     x[0] += dx;
                     x[1] += dx;
                  }
               #if ROOT_VERSION_CODE < ROOT_VERSION(3,10,1)
                  ComputeLogs (2, 2, x, y);
               #endif
                  // draw cursor
                  for (int i = 0; i < 2; i++) {
                     if (cur->fActive[i]) {
                        fCursorMarker[i] = new TMarker (x[i], y[i], 2);
                        fCursorMarker[i]->SetMarkerColor 
                           (fOptions.fTraces.fMarkerAttr[cur->fTrace].
                           GetMarkerColor());
                        fCursorMarker[i]->SetMarkerSize (3);
                        fCursorMarker[i]->Draw();
                        modified = kTRUE;
                     }
                  }
                  break;
               }
            case kCursorVertical:
               {
                  // compute cursor coordinates
                  double x[2];
                  double y[2];
                  x[0] = cur->fX[0];
                  x[1] = cur->fX[1];
                  y[0] = fLimitY[0];
                  y[1] = fLimitY[1];
                  if ((cur->fType == kCursorDifference) && 
                     cur->fActive[0] && cur->fActive[1]) {
                     x[1] += x[0];
                  }
                  double dx = 0;
                  for (int tr = 0; tr < kMaxTraces; tr++) {
                     if ((fPlot[tr] != 0) && (fPlotId[tr] == 0) &&
                        (fOptions.fTraces.fPlotStyle[tr] == kPlotStyleBar)) {
                        dx = 0.5 * ((TLGraph*)fPlot[tr])->GetBarDelta();
                     }
                  }
                  x[1] += dx;
               #if ROOT_VERSION_CODE < ROOT_VERSION(3,10,1)
                  ComputeLogs (2, 2, x, y);
               #endif
                  // draw cursor
                  for (int i = 0; i < 2; i++) {
                     if (cur->fActive[i] && (cur->fTrace >= 0) &&
                        (cur->fTrace < kMaxTraces)) {
                        fCursorLine[i] = new TLine (x[i], y[0], x[i], y[1]);
                        fOptions.fTraces.fLineAttr[cur->fTrace].
                           Copy (*fCursorLine[i]);
                        fCursorLine[i]->Draw();
                        modified = kTRUE;
                     }
                  }
                  break;
               }
            case kCursorHorizontal:
               {
                  // compute cursor coordinates
                  double x[2];
                  double y[2];
                  x[0] = fLimitX[0];
                  x[1] = fLimitX[1];
                  y[0] = cur->fH[0];
                  y[1] = cur->fH[1];
                  if ((cur->fType == kCursorDifference) && 
                     cur->fActive[0] && cur->fActive[1]) {
                     y[1] += y[0];
                  }
                  // check range
                  for (int i = 0; i < 2; i++) {
                     if (y[i] < fLimitY[0]) {
                        y[i] = fLimitY[0];
                     }
                     if (y[i] > fLimitY[1]) {
                        y[i] = fLimitY[1];
                     }
                  }
               #if ROOT_VERSION_CODE < ROOT_VERSION(3,10,1)
                  ComputeLogs (2, 2, x, y);
               #endif
                  // draw cursor
                  for (int i = 0; i < 2; i++) {
                     if (cur->fActive[i] && (cur->fTrace >= 0) &&
                        (cur->fTrace < kMaxTraces)) {
                        fCursorLine[i] = new TLine (x[0], y[i], x[1], y[i]);
                        fOptions.fTraces.fLineAttr[cur->fTrace].
                           Copy (*fCursorLine[i]);
                        fCursorLine[i]->Draw();
                        modified = kTRUE;
                     }
                  }
                  break;
               }
         }
         if (update && modified) {
            canv->Update();
         }
         if (isEditable) canv->SetEditable(); // (1-8-2002 mito)
         else canv->SetEditable(kFALSE); // (1-8-2002 mito)
         gPad = padsave;
      }
   }

//______________________________________________________________________________
   void TLGPad::UpdateUnits ()
   {
      // clear old units
      fXUnitList.Delete();
      fYUnitList.Delete();
      // lookup all plot descriptors associated with the current plot name
      for (PlotSet::iterator i = fPlotSet->begin(); 
          i != fPlotSet->end(); i++) {
         if (fOptions.fTraces.fGraphType == i->GetGraphType() && 
            PlotTypeId(i->GetGraphType()) != 11) { // hist (mito)
            for (calibration::UnitList::const_iterator j =
                i->Cal().Units(kCalUnitX).begin(); 
                j != i->Cal().Units(kCalUnitX).end(); ++j) {
               if (!fXUnitList.FindObject ((*j)->GetName())) {
                  fXUnitList.Add (new TNamed ((*j)->GetName(), ""));
               }
            }
            for (calibration::UnitList::const_iterator j =
                i->Cal().Units(kCalUnitY).begin(); 
                j != i->Cal().Units(kCalUnitY).end(); ++j) {
               if (!fYUnitList.FindObject ((*j)->GetName())) {
                  fYUnitList.Add (new TNamed ((*j)->GetName(), ""));
               }
            }
         }
      }
   }

//______________________________________________________________________________
   void TLGPad::Update (bool force)
   {
      UpdateUnits();
      UpdateOptions();
      UpdatePlot (0, force);
   }

//______________________________________________________________________________
   void TLGPad::ComputeLogs (Int_t nx, Int_t ny, double x[], double y[])
   {
      for (Int_t i=0;i<nx;i++) {
         if (fOptions.fRange.fAxisScale[0] == kAxisScaleLog) {
            if (x[i] > 0) x[i] = TMath::Log10(x[i]);
            else          x[i] = gPad->GetX1();
         }
      }
      for (Int_t i=0;i<ny;i++) {
         if (fOptions.fRange.fAxisScale[1] == kAxisScaleLog) {
            if (y[i] > 0) y[i] = TMath::Log10(y[i]);
            else          y[i] = gPad->GetY1();
         }
      }
   }

//______________________________________________________________________________
   void TLGPad::GetRange (Int_t axis, Double_t& min, Double_t& max,
                     Double_t& minpos, 
                     const Double_t* low, const Double_t* high)
   {
      Bool_t	first = kTRUE;
      min = 0;
      minpos = 0;
      max = 0;
   
      for (Int_t trace = 0; trace < kMaxTraces; trace++) {
	 // If there's no data or the trace isn't active, skip it.
         if ((fData[trace] == 0) || !fOptions.fTraces.fActive[trace]) {
            continue;
         }
      
         // calculate min/max
         Double_t r1 = 0;
         Double_t r2 = 0;
         Double_t r3 = 0;
         Float_t* x = (axis == 0) ? 
            fData[trace]->GetX() : fData[trace]->GetY();
         if (x == 0) {
            continue;
         }
         Float_t* range = 0;
         if ((low != 0) || (high != 0)) {
            range = (axis == 0) ? fData[trace]->GetY() : fData[trace]->GetX();
            if (range == 0) {
               continue;
            }
         }
         Int_t N = fData[trace]->GetN();
         Bool_t	f = kTRUE;
         if (N > 0) {
            r1 = r2 = x[0];
         }
	 // Go thru all the data points of axis.
         for (Int_t i = 0; i < N; i++) 
	 {
	    // if the other axis' value is less than *low, don't consider this point
	    // for finding the min or max of axis' values.  
            if ((low != 0) && (range[i] < *low)) {
               continue;
            }
	    // If the other axis' value is greater than *high, don't consider this point
	    // for finding the min or max of the axis' values.
            if ((high != 0) && (range[i] > *high)) {
               continue;
            }
	    // first point to consider, assign R1, R2 the first data value.
            if (f) {
               r1 = r2 = x[i];
               f = kFALSE;
            }
            else {
               if (x[i] < r1) r1 = x[i]; // If the data point is less than r1, it's a new min.
               if (x[i] > r2) r2 = x[i]; // If the data point is greater than r2, it's a new max.
            }
	    // If the data point is greater than 0, see if it's less than the current value of r3. If so
	    // (or in the case that r3 is 0, meaning it's not been set), assign the data value to r3.
            if ((x[i] > 0) && ((r3 <= 0) || (x[i] < r3))) {
               r3 = x[i];
            }
         }
      
         if (first) 
	 {
	    // If this is the first trace, just assign min and max the values found.
            min = r1;
            max = r2;
            first = kFALSE;
         }
         else 
	 {
	    // Otherwise, compare the min and max values of the previous trace(s) 
	    // with the values from the current trace.
            if (r1 < min) min = r1;
            if (r2 > max) max = r2;
         }
	 // If the least positive value of this trace is less than the previous least value,
	 // make this one the least positive value.
         if ((r3 > 0) && ((minpos <= 0) || (r3 < minpos))) {
            minpos = r3;
         }
      }
   }

//____________________________________________________________________hist (mito)
   void TLGPad::GetHistRange(Double_t& xmin,Double_t& ymin, 
                     Double_t& xmax, Double_t& ymax)
   {
      Double_t x0,y0,x1,y1;
      x0=x1=y0=y1=0;
   
      if( fData[0] ) {
         Int_t nbin = fData[0]->GetN();
         const double* edge = fData[0]->GetXBinEdges();
         x0 = 2 * edge[0] - edge[1];
         x1 = 2 * edge[nbin]- edge[nbin - 1];
      
         Int_t i = (fOptions.fParam.fUOBins) ? 0 : 1;
         Int_t end = (fOptions.fParam.fUOBins) ? nbin + 1 : nbin;
         for( ; i <= end; ++i){
            if(y1 < fData[0]->GetBinContents()[i]) 
               y1 = fData[0]->GetBinContents()[i];
         }
      }
   
      for (Int_t i = 1 ; i < kMaxTraces ; i++) {
         if( fData[i] ) {
            Int_t nbin = fData[i]->GetN();
            const double* edge = fData[i]->GetXBinEdges();
            if(x0 > 2 * edge[0] - edge[1]) 
               x0 = 2 * edge[0] - edge[1];
            if(x1 < 2 * edge[nbin]- edge[nbin - 1]) 
               x1 = 2 * edge[nbin]- edge[nbin - 1];
         
            Int_t j = (fOptions.fParam.fUOBins) ? 0 : 1;
            Int_t end = (fOptions.fParam.fUOBins) ? nbin + 1 : nbin;
            for( ; j <= end; ++j){
               if(y1 < fData[i]->GetBinContents()[j]) 
                  y1 = fData[i]->GetBinContents()[j];
            }
         }
      }
   
      xmin = x0;
      xmax = x1;
      ymin = y0;
      ymax = y1;
   
   }

//______________________________________________________________________________
   Int_t TLGPad::PlotTypeId (const char* plottype)
   {
      if (plottype == 0) {
         return -1;
      }
      else if (strncasecmp (plottype, kPTTimeSeries, 
                           strlen (kPTTimeSeries)) == 0) {
         return 0;
      }
      else if (strncasecmp (plottype, kPTPowerSpectrum, 
                           strlen (kPTPowerSpectrum)) == 0) {
         return 1;
      }
      else if (strncasecmp (plottype, kPTCrossCorrelation, 
                           strlen (kPTCrossCorrelation)) == 0) {
         return 3;
      }
      else if (strncasecmp (plottype, kPTTransferFunction, 
                           strlen (kPTTransferFunction)) == 0) {
         return 4;
      }
      else if (strncasecmp (plottype, kPTCoherenceFunction, 
                           strlen (kPTCoherenceFunction)) == 0) {
         return 5;
      }
      else if (strncasecmp (plottype, kPTTransferCoefficients, 
                           strlen (kPTTransferCoefficients)) == 0) {
         return 6;
      }
      else if (strncasecmp (plottype, kPTCoherenceCoefficients, 
                           strlen (kPTCoherenceCoefficients)) == 0) {
         return 7;
      }
      else if (strncasecmp (plottype, kPTHarmonicCoefficients, 
                           strlen (kPTHarmonicCoefficients)) == 0) {
         return 8;
      }
      else if (strncasecmp (plottype, kPTIntermodulationCoefficients, 
                           strlen (kPTIntermodulationCoefficients)) == 0) {
         return 9;
      }
      else if (strncasecmp (plottype, kPTFrequencySeries, 
                           strlen (kPTFrequencySeries)) == 0) {
         return 10;
      }
      // must be after coherence coefficients and coherence function
      else if (strncasecmp (plottype, kPTCoherence, 
                           strlen (kPTCoherence)) == 0) {
         return 2;
      }
      else if (strncasecmp (plottype, kPTHistogram1, 
                           strlen (kPTHistogram1)) == 0) {
         return 11;
      }
      else {
         // Unknown
         return 1000;
      }
   }

//______________________________________________________________________________
   void TLGPad::Configure (Int_t conf, Int_t variant)
   {
#ifdef DEBUG_RANGE_SETTING
      cerr << "TLGPad::Configure(" << conf << ", " << variant << ")" << endl ;
#endif
      // Defaults
      for (Int_t tr = 0; tr < kMaxTraces; tr++) {
         fOptions.fTraces.fPlotStyle[tr] = kPlotStyleLine;
      }
      fOptions.fUnits.fYValues = kUnitMagnitude;
      fOptions.fRange.fAxisScale[0] = kAxisScaleLinear;
      fOptions.fRange.fAxisScale[1] = kAxisScaleLinear;
      fOptions.fRange.fRange[0] = kRangeAutomatic;
      fOptions.fRange.fRange[1] = kRangeAutomatic;
      fOptions.fAxisX.fAxisTitle = "";
      fOptions.fAxisY.fAxisTitle = "";
      fOptions.fStyle.fTitle = "";
      fOptions.fUnits.fXUnit = "Default";
      fOptions.fUnits.fYUnit = "Default";
      fOptions.fUnits.fXMag = 0;
      fOptions.fUnits.fYMag = 0;
      fOptions.fUnits.fXSlope = 1.;
      fOptions.fUnits.fXOffset = 0.;
      fOptions.fUnits.fYSlope = 1.;
      fOptions.fUnits.fYOffset = 0.;
   
      switch (conf) {
         // Time series
         case 0:
            {
               fManualRangeUpdate = 0x0F;
               switch (variant) {
                  case 0:
                  default: 
                     fOptions.fUnits.fYValues = kUnitMagnitude;
                     fOptions.fAxisY.fAxisTitle = "Signal";
                     break;
                  case 1:
                     fOptions.fUnits.fYValues = kUnitReal;
                     fOptions.fAxisY.fAxisTitle = "Real";
                     break;
                  case 2:
                     fOptions.fUnits.fYValues = kUnitImaginary;
                     fOptions.fAxisY.fAxisTitle = "Imaginary";
                     break;
                  case 3:
                     fOptions.fUnits.fYValues = kUnitPhaseDeg;
                     fOptions.fAxisY.fAxisTitle = "Phase";
                     fOptions.fRange.fRangeFrom[1] = -200;
                     fOptions.fRange.fRangeTo[1] = 200;
                     fManualRangeUpdate &= 0x0C;
                     break;
               }
               fOptions.fAxisX.fAxisTitle = "Time";
               fOptions.fStyle.fTitle = kPTTimeSeries;
               break;
            }
         // Frequency series
         case 10:
            {
               fManualRangeUpdate = 0x0F;
               switch (variant) {
                  case 0:
                  default: 
                     fOptions.fUnits.fYValues = kUnitMagnitude;
                     fOptions.fAxisY.fAxisTitle = "Magnitude";
                     break;
                  case 1:
                     fOptions.fUnits.fYValues = kUnitReal;
                     fOptions.fAxisY.fAxisTitle = "Real";
                     break;
                  case 2:
                     fOptions.fUnits.fYValues = kUnitImaginary;
                     fOptions.fAxisY.fAxisTitle = "Imaginary";
                     break;
                  case 3:
                     fOptions.fUnits.fYValues = kUnitPhaseDeg;
                     fOptions.fAxisY.fAxisTitle = "Phase";
                     fOptions.fRange.fRangeFrom[1] = -200;
                     fOptions.fRange.fRangeTo[1] = 200;
                     fManualRangeUpdate &= 0x0C;
                     break;
               }
               fOptions.fAxisX.fAxisTitle = "Frequency";
               fOptions.fStyle.fTitle = kPTFrequencySeries;
               break;
            }
         // Power spectrum
         case 1:
            {
               fManualRangeUpdate = 0x09;
               switch (variant) {
                  case 0:
                  default: 
                     fOptions.fRange.fAxisScale[1] = kAxisScaleLog;
                     fManualRangeUpdate |= 0x04;
                     break;
                  case 1:
                     fOptions.fRange.fAxisScale[1] = kAxisScaleLinear;
                     fOptions.fRange.fRangeFrom[1] = 0;
                     break;
               }
               fOptions.fUnits.fYValues = kUnitMagnitude;
               fOptions.fRange.fRangeFrom[0] = 0;
               fOptions.fAxisX.fAxisTitle = "Frequency";
               fOptions.fAxisY.fAxisTitle = "Magnitude";
               fOptions.fStyle.fTitle = kPTPowerSpectrum;
               break;
            }
         // Coherence
         case 2:
            {
               fManualRangeUpdate = 0x02;
               fOptions.fUnits.fYValues = kUnitMagnitude;
               fOptions.fRange.fRangeFrom[0] = 0;
               fOptions.fRange.fRange[1] = kRangeManual;
               fOptions.fRange.fRangeFrom[1] = 0;
               fOptions.fRange.fRangeTo[1] = 1;
               fOptions.fAxisX.fAxisTitle = "Frequency";
               fOptions.fAxisY.fAxisTitle = "Coherence";
               fOptions.fStyle.fTitle = kPTCoherence;   
               break;
            }
         // Cross correlation
         case 3:
            {
               fManualRangeUpdate = 0x02;
               switch (variant) {
                  case 0:
                  default: 
                     fOptions.fUnits.fYValues = kUnitdBMagnitude;
                     fOptions.fAxisY.fAxisTitle = "Magnitude";
                     fManualRangeUpdate |= 0x0C;
                     break;
                  case 1:
                     fOptions.fUnits.fYValues = kUnitPhaseDeg;
                     fOptions.fAxisY.fAxisTitle = "Phase";
                     fOptions.fRange.fRangeFrom[1] = -200;
                     fOptions.fRange.fRangeTo[1] = 200;
                     break;
               }
               fOptions.fRange.fAxisScale[1] = kAxisScaleLinear;
               fOptions.fRange.fRangeFrom[0] = 0;
               fOptions.fAxisX.fAxisTitle = "Frequency";
               fOptions.fStyle.fTitle = kPTCrossCorrelation;
               break;
            }
         // Transfer function (Bode plot)
         case 4:
            {
               fManualRangeUpdate = 0x0F;
               switch (variant) {
                  case 0:
                  default: 
                     fOptions.fUnits.fYValues = kUnitdBMagnitude;
                     fOptions.fAxisY.fAxisTitle = "Magnitude";
                     break;
                  case 1:
                     fOptions.fUnits.fYValues = kUnitPhaseDegCont;
                     fOptions.fAxisY.fAxisTitle = "Phase";
                     fOptions.fRange.fRangeFrom[1] = -200;
                     fOptions.fRange.fRangeTo[1] = 200;
                     fManualRangeUpdate &= 0x03;
                     break;
               }
               fOptions.fRange.fAxisScale[0] = kAxisScaleLog;
               fOptions.fRange.fAxisScale[1] = kAxisScaleLinear;
               fOptions.fAxisX.fAxisTitle = "Frequency";
               fOptions.fStyle.fTitle = kPTTransferFunction;
               break;
            }
         // Coherence function
         case 5:
            {
               fManualRangeUpdate = 0x03;
               fOptions.fRange.fAxisScale[0] = kAxisScaleLog;
               fOptions.fRange.fRange[1] = kRangeManual;
               fOptions.fRange.fRangeFrom[1] = 0;
               fOptions.fRange.fRangeTo[1] = 1;
               fOptions.fAxisX.fAxisTitle = "Frequency";
               fOptions.fAxisY.fAxisTitle = "Coherence";
               fOptions.fStyle.fTitle = kPTCoherenceFunction;
               break;
            }
         // Transfer coefficients
         case 6:
            {
               for (Int_t tr = 0; tr < kMaxTraces; tr++) {
                  fOptions.fTraces.fPlotStyle[tr] = kPlotStyleBar;
               }
               fManualRangeUpdate = 0x0F;
               switch (variant) {
                  case 0:
                  default: 
                     fOptions.fUnits.fYValues = kUnitMagnitude;
                     fOptions.fRange.fAxisScale[1] = kAxisScaleLog;
                     fOptions.fAxisY.fAxisTitle = "Magnitude";
                     break;
                  case 1:
                     fOptions.fUnits.fYValues = kUnitPhaseDeg;
                     fOptions.fAxisY.fAxisTitle = "Phase";
                     fOptions.fRange.fRangeFrom[1] = -200;
                     fOptions.fRange.fRangeTo[1] = 200;
                     fManualRangeUpdate &= 0x03;
                     break;
               }
               fOptions.fAxisX.fAxisTitle = "Frequency";
               fOptions.fStyle.fTitle = kPTTransferCoefficients;
               break;
            }
         // Coherence coefficients
         case 7:
            {
               for (Int_t tr = 0; tr < kMaxTraces; tr++) {
                  fOptions.fTraces.fPlotStyle[tr] = kPlotStyleBar;
               }
               fManualRangeUpdate = 0x03;
               fOptions.fRange.fRange[1] = kRangeManual;
               fOptions.fRange.fRangeFrom[1] = 0;
               fOptions.fRange.fRangeTo[1] = 1;
               fOptions.fAxisX.fAxisTitle = "Frequency";
               fOptions.fAxisY.fAxisTitle = "Coherence";
               fOptions.fStyle.fTitle = kPTCoherenceCoefficients;
               break;
            }
         // Harmonic coefficients
         case 8:
            {
               for (Int_t tr = 0; tr < kMaxTraces; tr++) {
                  fOptions.fTraces.fPlotStyle[tr] = kPlotStyleBar;
               }
               fManualRangeUpdate = 0x0F;
               fOptions.fUnits.fYValues = kUnitMagnitude;
               fOptions.fRange.fAxisScale[1] = kAxisScaleLog;
               fOptions.fAxisY.fAxisTitle = "Magnitude";
               fOptions.fAxisX.fAxisTitle = "Frequency";
               fOptions.fStyle.fTitle = kPTHarmonicCoefficients;
               break;
            }
         // Intermodulation coefficients
         case 9:
            {
               for (Int_t tr = 0; tr < kMaxTraces; tr++) {
                  fOptions.fTraces.fPlotStyle[tr] = kPlotStyleBar;
               }
               fManualRangeUpdate = 0x0F;
               fOptions.fUnits.fYValues = kUnitMagnitude;
               fOptions.fRange.fAxisScale[1] = kAxisScaleLog;
               fOptions.fAxisY.fAxisTitle = "Magnitude";
               fOptions.fAxisX.fAxisTitle = "Frequency";
               fOptions.fStyle.fTitle = kPTIntermodulationCoefficients;
               break;
            }
         // 1-d Histogram (mito)
         case 11:
            {
            
               for (Int_t tr=0; tr < kMaxTraces; tr++) {
                  fOptions.fTraces.fPlotStyle[tr] = kPlotStyleBar;
                  fOptions.fTraces.fBarAttr[tr].SetFillStyle(0);
               }
            
               fManualRangeUpdate = 0x0F;
               fOptions.fAxisX.fAxisTitle = "";
               fOptions.fAxisY.fAxisTitle = "";
               fOptions.fStyle.fTitle = kPTHistogram1;
               fOptions.fLegend.fPlacement = kLegendTopLeft;
            
               break;
            }
      }
      fOptions.fRange.fBinLogSpacing = 
         fOptions.fRange.fAxisScale[0] == kAxisScaleLog;
   
      // preferred magnitude
      Bool_t first = kTRUE;
      Int_t magx = 0;
      Int_t magy = 0;
      for (Int_t tr = 0; tr < kMaxTraces; tr++) {
         if (conf == 11) { // hist (mito)
            magx = 0;
            magy = 0;
            first = kFALSE;
         }
         else {
            if (fOptions.fTraces.fActive[tr]) {
               const PlotDescriptor* pd = 
                  fPlotSet->Get (fOptions.fTraces.fGraphType, 
                                fOptions.fTraces.fAChannel[tr],
                                fOptions.fTraces.fBChannel[tr]);
               if (pd == 0) {
                  pd = fPlotSet->Get (fOptions.fTraces.fGraphType, 
                                     fOptions.fTraces.fAChannel[tr], 0);
               }
               if ((pd != 0) && (pd->Cal().IsValid())) {
                  Int_t mx = pd->Cal().GetPreferredMag (kCalUnitX);
                  Int_t my = pd->Cal().GetPreferredMag (kCalUnitY);
                  if (first) {
                     magx = mx;
                     magy = my;
                     first = kFALSE;
                  }
                  else {
                     if (magx != mx) {
                        magx = 0;
                     }
                     if (magy != my) {
                        magy = 0;
                     }
                  }
               }
            }
         }
      }
      if (!first) {
         fOptions.fUnits.fXMag = magx;
         fOptions.fUnits.fYMag = magy;
      }
#ifdef DEBUG_RANGE_SETTING
      cerr << "fOptions.fUnits.fYValues = " ;
      switch (fOptions.fUnits.fYValues)
      {
	 case kUnitMagnitude : cerr << "Magnitude" ; break ;
	 case kUnitdBMagnitude : cerr << "dB Magnitude" ; break ;
	 case kUnitReal : cerr << "Re(y)" ; break ;
	 case kUnitImaginary : cerr << "Im(y)" ; break ;
	 case kUnitRealImaginary : cerr << "Re or Im" ; break ;
	 case kUnitPhaseDeg : cerr << "Phase (degree)" ; break ;
	 case kUnitPhaseRad : cerr << "Phase (rad)" ; break ;
	 case kUnitPhaseDegCont : cerr << "Cont. Phase (degree)" ; break ;
	 case kUnitPhaseRadCont : cerr << "Cont. Phase (rad)" ; break ;
	 default : cerr << "Unknown" ; break ;
      }
      cerr << endl;
      cerr << "fOptions.fRange.fAxisScale[0] = " << (fOptions.fRange.fAxisScale[0] ? "Log" : "Linear") << endl ;
      cerr << "fOptions.fRange.fAxisScale[1] = " << (fOptions.fRange.fAxisScale[1] ? "Log" : "Linear") << endl ;
      cerr << "fOptions.fRange.fRange[0] = " << (fOptions.fRange.fRange[0] ? "Manual" : "Automatic") << endl ;
      cerr << "fOptions.fRange.fRange[1] = " << (fOptions.fRange.fRange[1] ? "Manual" : "Automatic") << endl ;
      cerr << "fOptions.fAxisX.fAxisTitle = " << fOptions.fAxisX.fAxisTitle << endl ;
      cerr << "fOptions.fAxisY.fAxisTitle = " << fOptions.fAxisY.fAxisTitle << endl ;
      cerr << "fOptions.fStyle.fTitle = " << fOptions.fStyle.fTitle << endl ;
      cerr << "fOptions.fUnits.fXUnit = " << fOptions.fUnits.fXUnit << endl ;
      cerr << "fOptions.fUnits.fYUnit = " << fOptions.fUnits.fYUnit << endl ;
      cerr << "fOptions.fUnits.fXMag = " << fOptions.fUnits.fXMag << endl ;
      cerr << "fOptions.fUnits.fYMag = " << fOptions.fUnits.fYMag << endl ;
      cerr << "fOptions.fUnits.fXSlope = " << fOptions.fUnits.fXSlope << endl ;
      cerr << "fOptions.fUnits.fXOffset = " << fOptions.fUnits.fXOffset <<endl;
      cerr << "fOptions.fUnits.fYSlope = " << fOptions.fUnits.fYSlope << endl ;
      cerr << "fOptions.fUnits.fYOffset = " <<fOptions.fUnits.fYOffset << endl;
      cout << "TLGPad::Configure() end" << endl;
#endif
   }

//______________________________________________________________________________
   Int_t TLGPad::ShowPlot (const PlotDescriptor* data, 
                     const char* plottype, Int_t variant,
                     Bool_t update)
   {
      return ShowMultiPlot (&data, 1, plottype, variant, update);
   }

//______________________________________________________________________________
   Int_t TLGPad::ShowMultiPlot (const PlotDescriptor* data[], 
                     Int_t plotnum, const char* plottype, 
                     Int_t variant, Bool_t update)
   {
      // reset options in pad
      if (fOptions.fConfig.fAutoConf) {
         SetDefaultGraphicsOptions (fOptions);
      }
   
      // Select plots
      if ((data != 0) && (plotnum > 0)) {
         Bool_t first = kTRUE;
         for (Int_t tr = 0; (tr < plotnum) && (tr < kMaxTraces); tr++) {
            if (data[tr] == 0) {
               continue;
            }
            if (first) {
               fOptions.fTraces.fGraphType = data[tr]->GetGraphType();
               // guess plot type from first descriptor if not set
               if (plottype == 0) {
                  plottype = fOptions.fTraces.fGraphType;
               }
               first = kFALSE;
            }
            fOptions.fTraces.fAChannel[tr] = data[tr]->GetAChannel();
            if (data[tr]->GetBChannel() != 0) {
               fOptions.fTraces.fBChannel[tr] = data[tr]->GetBChannel();
            }
            fOptions.fTraces.fActive[tr] = kTRUE;
         }
      }
      // check if configure
      Int_t conf = PlotTypeId (plottype);
      if (fOptions.fConfig.fAutoConf && (conf >= 0)) {
         Configure (conf, variant);
      }
   
      if (update) {
         Update (kTRUE);
      }
      return 1;
   }

//______________________________________________________________________________
   Bool_t TLGPad::PostScript (const TString& filename, Int_t wtype)
   {
      //cerr << "TLGPad::PostScript()" << endl ;
      TVirtualPad* padsave = gPad;
      TVirtualPS*  pssave = gVirtualPS;
      gPad = GetCanvas();
      gVirtualPS = 0;
      //gStyle->SetPaperSize();
      TPostScript	ps (filename, wtype);
      if (gVirtualPS == 0) {
         gPad = padsave;
         gVirtualPS = pssave;
         return kFALSE;
      }
      UpdatePlot (0, kFALSE);
      ps.Close();
      gPad = padsave;
      gVirtualPS = pssave;
      //cerr << "TLGPad::PostScript() end" << endl ;
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGPad::Fill (PlotSet& pl, const char* winid, const char* padid)
   {
      // loop over traces
      for (Int_t tr = 0; tr < kMaxTraces; tr++) {
         if (!fData[tr] || !fOriginalPlotD[tr]) {
            continue;
         }
      	 // compute range
         int n1 = 0;
         int n2 = fData[tr]->GetN() - 1;
         if (fOptions.fRange.fRange[0] == kRangeManual) {
            Double_t min = fOptions.fRange.fRangeFrom[0];
            Double_t max = fOptions.fRange.fRangeTo[0];
            if (min > max) {
               Double_t temp = min; min = max; max = temp;
            }
            while ((n1 <= n2) && (fData[tr]->GetX()[n1] < min * (1. - 1E-6))) {
               n1++;
            }
            while ((n1 <= n2) && (fData[tr]->GetX()[n2] > max * (1. + 1E-6))) {
               n2--;
            }
            if (n1 < 0) n1 = 0;
            if (n2 >= fData[tr]->GetN()) {
               n2 = fData[tr]->GetN() - 1;
            }
         }
         int len = n2 - n1 + 1;
         // make name
         TString name = winid ? winid : "";
         if (winid) name += "_";
         name += padid ? padid : "";
         if (padid) name += "_";
         char traceid[16];
         sprintf (traceid, "trace%i", tr);
         name += traceid;
         // Data Descriptor reference
         BasicDataDescriptor* data = 
            new IndirectDataDescriptor (*(fData[tr]), n1, len);
         // Plot descriptor clone w/o calibration
         PlotDescriptor* pd = new PlotDescriptor 
            (data, fOriginalPlotD[tr]->GetGraphType(), (const char*)name,
            fOriginalPlotD[tr]->GetBChannel() ? (const char*)name : 0);
         if (pd) {
            pd->Param() = fOriginalPlotD[tr]->Param();
            pd->Cal().Clone (fOriginalPlotD[tr]->Cal());
            pl.Add (pd);
         }
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGPad::ProcessMessage (Long_t msg, Long_t parm1, Long_t parm2)
   {
      switch (GET_MSG (msg)) {
         case kC_COMMAND:
            {
               switch (GET_SUBMSG (msg)) {
                  case kCM_BUTTON:
                     {
                        switch (parm1) {
                           case kGOptHideID:
                              HidePanel (!fHidePanel);
                              break;
                           case kGOptDialogID:
                              PanelDialog (kTRUE);
                              break;
                        }
                     }
               }
               break;
            }
         case kC_OPTION:
            {
               switch (GET_SUBMSG (msg)) {
                  case kCM_OPTCLOSE:
                     {
                        fOptionDialogbox = 0;
                        break;
                     }
                  case kCM_OPTUPDATE:
                  case kCM_OPTCHANGED:
                     {
                        // restore of plot settings
                        if (parm1 == 1) {
                           UpdatePlot (0, kFALSE);
                        }
                        // update when plot type has changed
                        else if (fOptions.fConfig.fAutoConf && 
                                (fPlotSet->GetPlotMap().GetId 
                                (fOptions.fTraces.fGraphType) >= 0) &&
                                (fPlotType != fOptions.fTraces.fGraphType)) {
                           Configure (PlotTypeId 
                                     (fOptions.fTraces.fGraphType), 0);
                           Update (kFALSE);
                        }
                        // normal update: only settings have changed
                        else {
                           UpdatePlot (0, kFALSE);
                        }
                        break;
                     }
                  case kCM_OPTCURSOR:
                     {
                        if (fCursorStackSize < kMaxCursorStack) {
                           // put request in stack
                           fCursorStack[2*fCursorStackSize] = parm1;
                           fCursorStack[2*fCursorStackSize+1] = parm2;
                           fCursorStackSize++;
                           // set timer
                           if (fCursorTimer == 0) {
                              fCursorTimer = 
                                 new TTimer (this, kCursorDelay, kTRUE);
                              if (fCursorTimer) fCursorTimer->TurnOn();
                           }
                        }
                        break;
                     }
                  case kCM_CAL:
                     {
                        SendMessage (fMsgWindow, msg, parm1, parm2);
                        break;
                     }
               }
               break;
            }
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGPad::HandleTimer (TTimer* timer)
   {
      // work through cursor stack; only update last
      if (fCursorStackSize > 0) {
         Bool_t allthesame;
         UpdateParameters (&allthesame);
         for (int i = 0; i < fCursorStackSize; i++) {
            Bool_t silent = (i < fCursorStackSize - 1);
            UpdateCursor (fCursorStack[2*i] != 0, !silent, !silent, 
                         fCursorStack[2*i+1], !allthesame);
         }
         fCursorStackSize = 0;
      }
      delete fCursorTimer;
      fCursorTimer = 0;
      return kTRUE;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGMultiPadLayout                                                    //
//                                                                      //
// Layout manager for TLGMultiPad                                       //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGMultiPadLayout::TLGMultiPadLayout (TLGMultiPad* mpad)
   {
      fMPad = mpad;
   }

//______________________________________________________________________________
   void TLGMultiPadLayout::Layout ()
   {
      if (my_debug) cerr << "TLGMultiPadLayout::Layout()" << endl ;
      // get size of pad and option panel
      TGDimension	mpadsize = fMPad->GetSize();
   
      // determine grid parameters
      Int_t rows = fMPad->fRows;
      Int_t cols = fMPad->fCols;
      Int_t padding = 3;
      // button position
      Int_t bh = 25;
      Int_t bw = (mpadsize.fWidth - 2 * padding) / 
         fMPad->fButtons.GetSize();
      Int_t bx = padding;
      Int_t by = mpadsize.fHeight - bh - padding;
      // zero position  of pads
      Int_t x0 = padding;
      Int_t y0 = padding;
      UInt_t dx = (mpadsize.fWidth - padding) / cols;
      UInt_t dy = (mpadsize.fHeight - 2 * padding - bh) / rows;
   
      // place pads
      Int_t x = 0 ;
      Int_t y = 0 ;
      UInt_t w = 0 ;
      UInt_t h = 0 ;
      for (int pad = 0; pad < fMPad->fPadNum; pad++) {
         // Zoom state
         if (fMPad->fPadZoom >= 0) {
            // Pad is active
            if (pad == fMPad->fPadZoom ) {
               fMPad->fPadList[pad]->MoveResize (x0, y0,
                                    cols * dx - padding, rows * dy - padding);
               //fMPad->fPadList[pad]->GetCanvas()->ForceUpdate();
            }
            // Pad is not active: hide outside normal screen area
            else {
               fMPad->fPadList[pad]->MoveResize (bx, by + 10000, 
                                    cols * dx - padding, rows * dy - padding);
            }
         }
         // Normal (non-zoom) state
         else {
            // Pad is active
            if (fMPad->GetPadCoordinates (pad, x, y, w, h)) {
               // cout << "x = " << x0 + x * dx << " y = " << y0 + y * dy <<
               // " w = " << w * dx - padding << " h = " << h * dy - padding << endl;
               fMPad->fPadList[pad]->MoveResize (x0 + x * dx, y0 + y * dy,
                                    w * dx - padding, h * dy - padding);
               //fMPad->fPadList[pad]->GetCanvas()->ForceUpdate();
            }
            // Pad is not active: hide outside normal scree area
            else {
               fMPad->fPadList[pad]->MoveResize (bx, by + 10000, 
                                    w * dx - padding, h * dy - padding);
            }
         }
      }
   
      // place buttons
      TIter	next (&fMPad->fButtons);
      TGButton* b;
      while ((b = (TGButton*) (next())) != 0) {
         b->MoveResize (bx, by, bw, bh);
         bx += bw;
      }
      if (my_debug) cerr << "TLGMultiPadLayout::Layout() return" << endl ;
   }

//______________________________________________________________________________
   TGDimension TLGMultiPadLayout::GetDefaultSize () const
   {
      TGDimension 	msize = fMPad->GetSize();
      UInt_t		options = fMPad->GetOptions();
   
      if ((options & kFixedWidth) && (options & kFixedHeight)) {
         return msize;
      }
   
      if (msize.fWidth < 100) {
         msize.fWidth = 100;
      }
      if (msize.fHeight < 100) {
         msize.fHeight = 100;
      }
      return msize;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGMultiPadLayoutGrid                                                //
//                                                                      //
// Layout grid for TLGPads                                              //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGMultiPadLayoutGrid::TLGMultiPadLayoutGrid (Int_t layout)
   {
      if (my_debug) cerr << "TLGMultiPadLayoutGrid::TLGMultiPadLayoutGrid(layout = " << layout << ")" << endl ;
      for (int i = 0; i < kGMaxPad; i++) {
         fPadPos[i] = i;
      }
      SetPadLayout (layout);
      if (my_debug) cerr << "TLGMultiPadLayoutGrid::TLGMultiPadLayoutGrid(layout) return" << endl ;
   }

//______________________________________________________________________________
   TLGMultiPadLayoutGrid::TLGMultiPadLayoutGrid (Int_t rows, Int_t columns)
   {
      if (my_debug) cerr << "TLGMultiPadLayoutGrid::TLGMultiPadLayoutGrid(rows = " << rows << ", columns = " << columns << ")" << endl ;
      for (int i = 0; i < kGMaxPad; i++) {
         fPadPos[i] = i;
      }
      SetPadLayout (rows, columns);
      fLayout = rows * columns;
      if (my_debug) cerr << "TLGMultiPadLayoutGrid::TLGMultiPadLayoutGrid(rows, columns) return" << endl ;
   }

//______________________________________________________________________________
   void TLGMultiPadLayoutGrid::SetPadLayout (Int_t layout)
   {
      if (my_debug) cerr << "TLGMultiPadLayoutGrid::SetPadLayout(layout = " << layout << ")" << endl ;
      fLayout = layout;
      switch (layout) {
         case 1: 
            {
               SetPadLayout (1, 1);
               break;
            }
         case 2: 
            {
               SetPadLayout (2, 1);
               break;
            }
         case 3: 
            {
               SetPadLayout (2, 2);
               (*this)(2,2) = -1;
               break;
            }
         case 4: 
            {
               SetPadLayout (2, 2);
               break;
            }
         case 5: 
            {
               SetPadLayout (3, 2);
               (*this)(3,2) = -1;
               break;
            }
         case 6: 
            {
               SetPadLayout (3, 2);
               break;
            }
         case 7: 
            {
               SetPadLayout (3, 3);
               (*this)(2,3) = -1;
               (*this)(3,3) = -1;
               break;
            }
         case 8: 
            {
               SetPadLayout (3, 3);
               (*this)(3,3) = -1;
               break;
            }
         case 9: 
            {
               SetPadLayout (3, 3);
               break;
            }
         case 10: 
            {
               SetPadLayout (4, 3);
               (*this)(3,3) = -1;
               (*this)(4,3) = -1;
               break;
            }
         case 11: 
            {
               SetPadLayout (4, 3);
               (*this)(4,3) = -1;
               break;
            }
         case 12: 
            {
               SetPadLayout (4, 3);
               break;
            }
         case 13: 
            {
               SetPadLayout (4, 4);
               (*this)(2,4) = -1;
               (*this)(3,4) = -1;
               (*this)(4,4) = -1;
               break;
            }
         case 14: 
            {
               SetPadLayout (4, 4);
               (*this)(3,4) = -1;
               (*this)(4,4) = -1;
               break;
            }
         case 15: 
            {
               SetPadLayout (4, 4);
               (*this)(4,4) = -1;
               break;
            }
         case 16: 
            {
               SetPadLayout (4, 4);
               break;
            }
         case 101: 
            {
               SetPadLayout (1, 2);
               break;
            }
         case 102: 
            {
               SetPadLayout (3, 1);
               break;
            }
         case 103: 
            {
               SetPadLayout (4, 1);
               break;
            }
         case 104: 
            {
               SetPadLayout (2, 2);
               (*this)(1,1) = 0;
               (*this)(1,2) = 0;
               (*this)(2,1) = 1;
               (*this)(2,2) = 2;
               break;
            }
         case 105: 
            {
               SetPadLayout (3, 3);
               (*this)(1,1) = 0;
               (*this)(1,2) = 0;
               (*this)(2,1) = 0;
               (*this)(2,2) = 0;
               (*this)(3,1) = 1;
               (*this)(3,2) = 2;
               (*this)(1,3) = 3;
               (*this)(2,3) = 4;
               (*this)(3,3) = 5;
               break;
            }
         default:
            {
               SetPadLayout (1);
               break;
            }
      }
      if (my_debug) cerr << "TLGMultiPadLayoutGrid::SetPadLayout(layout) return" << endl ;
   }

//______________________________________________________________________________
   void TLGMultiPadLayoutGrid::SetPadLayout (Int_t rows, Int_t columns)
   {
      if (my_debug) cerr << "TLGMultiPadLayoutGrid::SetPadLayout(rows = " << rows << ", columns = " << columns<< ")" << endl ;
      if ((rows > 0) && (rows <= kGMaxGrid) &&
         (columns > 0) && (columns <= kGMaxGrid)) {
         fRows = rows;
         fCols = columns;
         Int_t n = 0;
         for (int j = 1; j <= fCols; j++) {
            for (int i = 1; i <= fRows; i++) {
               (*this)(i, j) = n++;
            }
         }
      }
      if (my_debug) cerr << "TLGMultiPadLayoutGrid::SetPadLayout(row, column) return" << endl ;
   }

//______________________________________________________________________________
   Bool_t TLGMultiPadLayoutGrid::GetPadCoordinates (Int_t pad, Int_t& x, 
                     Int_t& y, UInt_t& w, UInt_t& h)
   {
      Int_t pos = GetPadPosition (pad);
      if (pos < 0) {
         return kFALSE;
      }
      for (int i = 1; i <= fRows; i++) {
         for (int j = 1; j <= fCols; j++) {
            if ((*this)(i, j) == pos) {
               // found start
               x = j - 1;
               y = i - 1;
               w = 1;
               h = 1;
               // get width and height
               Int_t jw = j + 1;
               Int_t ih = i + 1;
               while ((jw <= fCols) && ((*this)(i, jw) == pos)) {
                  w++;
                  jw++;
               }
               while ((ih <= fRows) && ((*this)(ih, j) == pos)) {
                  h++;
                  ih++;
               }
               return kTRUE;
            }
         }
      }
      return kFALSE;
   }

//______________________________________________________________________________
   void TLGMultiPadLayoutGrid::SetPadPosition (Int_t pad, Int_t pos)
   {
      if ((pad >= 0) && (pad < kGMaxPad)) {
         if (pos < 0) pos = -1;
         fPadPos[pad] = pos;
      }
   }

//______________________________________________________________________________
   Int_t TLGMultiPadLayoutGrid::GetPadPosition (Int_t pad)
   {
      if ((pad >= 0) && (pad < kGMaxPad)) {
         return fPadPos[pad];
      }
      else {
         return -1;
      }
   }

//______________________________________________________________________________
   void TLGMultiPadLayoutGrid::SwapPads (Int_t pad1, Int_t pad2)
   {
      if ((pad1 >= 0) && (pad1 < kGMaxPad) && 
         (pad2 >= 0) && (pad2 < kGMaxPad)) {
         Int_t dummy = fPadPos[pad1];
         fPadPos[pad1] = fPadPos[pad2];
         fPadPos[pad2] = dummy;
      }
   }

//______________________________________________________________________________
   Int_t& TLGMultiPadLayoutGrid::operator ()(Int_t row, Int_t col)
   {
      static Int_t dummy;
   
      if ((row < 1) || (row > kGMaxGrid) || 
         (col < 1) || (col > kGMaxGrid)) {
         return dummy;
      }
      else {
         return fGrid [(row - 1) * kGMaxGrid + (col - 1)];
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGMultiPad::ActionPlotPads                                          //
//                                                                      //
// Multi pad (contains multiple TLGPad's                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool TLGMultiPad::ActionPlotPads::Export (ExportOption_t* defex,
                     const PlotSet& pset, TLGMultiPad& mpad)
   {
      if (!defex) defex = mpad.GetDefExportOpt();
      if (mpad.GetPlotSet()) {
         return ExportToFileDlg (gClient->GetRoot(), &mpad, 
                              *mpad.GetPlotSet(), defex,
                              mpad.GetCalibrationTable());
      }
      else {
         return false;
      }
   }

//______________________________________________________________________________
   bool TLGMultiPad::ActionPlotPads::Import (ImportOption_t* defim,
                     const PlotSet& pset, TLGMultiPad& mpad)
   {
      if (!defim) defim = mpad.GetDefImportOpt();
      if (mpad.GetPlotSet()) {
         return ImportFromFileDlg (gClient->GetRoot(), &mpad, 
                              *mpad.GetPlotSet(), defim,
                              mpad.GetCalibrationTable());
      }
      else {
         return false;
      }
   }




//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGMultiPad                                                          //
//                                                                      //
// Multi pad (contains multiple TLGPad's                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGMultiPad::TLGMultiPad (const TGWindow* p, const char* name, 
                     PlotSet& plots, Int_t id, Int_t layout)
   : TGCompositeFrame (p, 800, 600, kVerticalFrame), TGWidget (id), 
   TLGMultiPadLayoutGrid (layout), fPlotSet (&plots), 
   fPadNum (0), fPadName (name), fPadZoom (-1), fStoreOptions (0),
   fStoreOptionsMax (0), fDefPrintSetup (0), fDefImportOpt (0), 
   fDefExportOpt (0), fRefTraces (0), fMathTable (0), fAction (0)
   {
      for (int i = 0; i < kGMaxPad; i++) {
         fPadList[i] = 0;
      }
      // setup layout
      if ((layout > 0) && (layout <= 16)) {
         fPadNum = layout;
      }
      else 
         switch (layout) {
            case 101:
               fPadNum = 2;
               break;
            case 102:
               fPadNum = 3;
               break;
            case 103:
               fPadNum = 4;
               break;
            case 104:
               fPadNum = 3;
               break;
            case 105:
               fPadNum = 6;
               break;
            default:
               fPadNum = 1;
               break;
         }
      // create pads
      for (int i = 0; i < fPadNum; i++) {
         char 	buf [256];
         sprintf (buf, "%s(%i)", (const char*) fPadName, i);
         fPadList[i] = new TLGPad (this, buf, fId + i + 1, *fPlotSet,
				   fStoreOptions, fStoreOptionsMax);
         fPadList[i]->Associate (this);
         AddFrame (fPadList[i], 0);
      }
      // create buttons, add them to fButtons list.
      TGButton* btn = new TGTextButton (this, "Reset", kGMPadResetID);
      btn->SetToolTipText ("Resets the plot settings of the graphics pads");
      AddButton (btn);
      btn = new TGTextButton (this, "Zoom", kGMPadZoomID);
      btn->SetToolTipText ("Select between single and multi pad display");
      AddButton (btn);
      btn = new TGTextButton (this, "Active", kGMPadActiveID);
      btn->SetToolTipText ("Sets the next pad to be the active one");
      AddButton (btn);
      btn = new TGTextButton (this, "New", kGMPadNewID);
      btn->SetToolTipText ("Creates a new multi pad window");
      AddButton (btn);
      btn = new TGTextButton (this, "Options...", kGMPadOptionID);
      btn->SetToolTipText ("Changes global options of the graphics pad");
      AddButton (btn);
      btn = new TGTextButton (this, "Import...", kGMPadImportID);
      btn->SetToolTipText ("Imports data from file");
      AddButton (btn);
      btn = new TGTextButton (this, "Export...", kGMPadExportID);
      btn->SetToolTipText ("Exports data to file");
      AddButton (btn);
      btn = new TGTextButton (this, "Reference...", kGMPadReferenceID);
      btn->SetToolTipText ("Sets a reference trace");
      AddButton (btn);
      btn = new TGTextButton (this, "Calibration...", kGMPadCalibrationID);
      btn->SetToolTipText ("Shows the calibration edit window");
      AddButton (btn);
//      btn = new TGTextButton (this, "Math...", kGMPadMathID);
//      btn->SetToolTipText ("Shows the calibration math editor");
//      AddButton (btn);
      btn = new TGTextButton (this, "Print...", kGMPadPrintID);
      btn->SetToolTipText ("Print the visbile pad(s)");
      AddButton (btn);
   
      // set layout manager
      SetLayoutManager (new TLGMultiPadLayout (this));
      SetActivePad (0);
   }

//______________________________________________________________________________
   TLGMultiPad::~TLGMultiPad ()
   {
      // Delete pads
      for (int i = 0; i < fPadNum; i++) {
         delete fPadList[i];
      }
      // Delete buttons
      fButtons.Delete();
   }

//______________________________________________________________________________
   void TLGMultiPad::SetStoreOptionList (OptionAll_t** list, Int_t max) {
      fStoreOptions = list; 
      fStoreOptionsMax = max;
      for (int pad = 0; pad < fPadNum; pad++) {
         if (fPadList[pad] != 0) {
            fPadList[pad]->SetStoreOptionList (list, max);
         }
      }
   }

//______________________________________________________________________________
   void TLGMultiPad::SetPadLayoutAndNumber (Int_t layout) 
   {
      if (my_debug) cerr << "TLGMultiPad::SetPadLayoutAndNumber(layout = " << layout << ")" << endl ;
      Int_t old_layout = fLayout ;
      Int_t pads = 0;
      if (layout > 0) {
         // setup layout
         if ((layout > 0) && (layout <= 16)) {
            pads = layout;
         }
         else 
            switch (layout) {
               case 101:
                  pads = 2;
                  break;
               case 102:
                  pads = 3;
                  break;
               case 103:
                  pads = 4;
                  break;
               case 104:
                  pads = 3;
                  break;
               case 105:
                  pads = 6;
                  break;
               default:
                  pads = -1;
                  break;
            }
         if (pads > 0) {
            SetPadLayout (layout);
	    // Need to force a layout if the old layout is different from
	    // the new layout, even if the number of pads is the same.
	    // Bugzilla 1041
            SetPadNumber (pads, (old_layout != layout));
         }
      }
      if (my_debug) cerr << "TLGMultiPad::SetPadLayoutAndNumber(layout) return" << endl ;
   }

//______________________________________________________________________________
   void TLGMultiPad::SetPadNumber (Int_t pads, Int_t force) 
   {
      if (my_debug) cerr << "TLGMultiPad::SetPadNumber(pads = " << pads << ", force = " << force << ")" << endl ;
      if (my_debug) cerr << "  fPadNum = " << fPadNum << endl ;
      // If the number of pads is illegal or the layout hasn't changed, leave.
      if ((pads <= 0) || (pads > kGMaxPad) || !force) {
	 if (my_debug) cerr << "TLGMultiPad::SetPadNumber() return, nothing to do" << endl ;
         return;
      }
      Int_t save = GetActivePad();
      fPadZoom = -1;
   
      // Need to create additional pads
      if (pads > fPadNum) {
         for (int i = fPadNum; i < pads; i++) {
            char buf[64];
            sprintf (buf, "(%i)", i);
            TString padname = fPadName + buf;
            fPadList[i] = new TLGPad (this, padname, fId + i + 1, *fPlotSet,
                                 fStoreOptions, fStoreOptionsMax);
            fPadList[i]->Associate (this);
            AddFrame (fPadList[i], 0);
         }
         fPadNum = pads;
         MapSubwindows ();
         SetActivePad (save);
      }
      // Delete pads which aren't needed anymore
      else {
         for (int i = pads; i < fPadNum; i++) {
            HideFrame (fPadList[i]);
            RemoveFrame (fPadList[i]);
         }
         fPadNum = pads;
         MapSubwindows ();
         if (save >= fPadNum) {
            SetActivePad (0);
         }
         for (int j = pads; j < fPadNum; j++) {
            delete fPadList[j];
         }
      }
      Layout();
      if (my_debug) cerr << "TLGMultiPad::SetPadNumber() return" << endl ;
   }

//______________________________________________________________________________
   void TLGMultiPad::AddButton (TGButton* btn)
   {
      AddFrame (btn, 0);
      fButtons.Add (btn);
   }

//______________________________________________________________________________
   Int_t TLGMultiPad::SetActivePad (Int_t pad)
   {
      Int_t save = GetActivePad ();
      // check if a new pad has to be activated
      if ((pad >= 0) && (pad < fPadNum) && (pad != save)) {
         fPadList[pad]->GetCanvas()->cd();
         // if in zoomed state, change zoom to active pad
         if ((fPadZoom >= 0) && (pad != fPadZoom)) {
            Zoom (pad);
         }
         // else update both the inactivated and the activated pad
         else {
            if ((save >= 0) && (save < fPadNum)) {
               fPadList[save]->GetCanvas()->Update();
            }
            fPadList[pad]->GetCanvas()->Update();
         }
      }
      // return old pad
      return save;
   }

//______________________________________________________________________________
   Int_t TLGMultiPad::GetActivePad () const
   {
      for (int pad = 0; pad < fPadNum; pad++) {
         if (fPadList[pad]->GetCanvas()->GetCanvasID() == 
            gPad->GetCanvasID()) {
            return pad;
         }
      }
      return -1;
   }

//______________________________________________________________________________
   Int_t TLGMultiPad::Zoom (Int_t pad)
   {
      if (pad < 0) pad = -1;
      if ((fPadZoom == pad) || (pad >= fPadNum)) {
         return fPadZoom;
      }
      if (fPadZoom >= 0) {
         GetPad (fPadZoom)->HidePanel (fHidePanelBeforeZoom);
      }
      Int_t save = fPadZoom;
      fPadZoom = pad;
      if (fPadZoom >= 0) {
         fHidePanelBeforeZoom = GetPad (fPadZoom)->GetHideState();
         GetPad (fPadZoom)->HidePanel (kTRUE);
      }
      Layout();
      return save;
   }

//______________________________________________________________________________
   void TLGMultiPad::UpdatePlot (const BasicPlotDescriptor* plotd)
   {
      //cerr << "TLGMultiPad::UpdatePlot()" << endl ;
      for (int pad = 0; pad < fPadNum; pad++) {
         fPadList[pad]->UpdatePlot (plotd);
      }
      //cerr << "TLGMultiPad::UpdatePlot() end" << endl ;
   }

//______________________________________________________________________________
   void TLGMultiPad::Update (bool force)
   {
      for (int pad = 0; pad < fPadNum; pad++) {
         fPadList[pad]->Update (force);
      }
   }

//______________________________________________________________________________
   Int_t TLGMultiPad::ShowPlot (const PlotDescriptor* data, 
                     const char* plottype, Int_t pad, Bool_t update)
   {
      return ShowMultiPlot (&data, 1, plottype, pad, update);
   }

//______________________________________________________________________________
   Int_t TLGMultiPad::ShowPlot (const PlotDescriptor* data, 
                     const char* plottype, Int_t pad, Int_t variant, 
                     Bool_t update)
   {
      // check if pad is avaialable
      TLGPad* p = GetPad (pad);
      if (p != 0) {
         return p->ShowPlot (data, plottype, variant, update);
      }
      else {
         return 0;
      }
   
   }

//______________________________________________________________________________
   Int_t TLGMultiPad::ShowMultiPlot (const PlotDescriptor* data[], 
                     Int_t plotnum, const char* plottype, Int_t pad, 
                     Bool_t update)
   {
      // determine number of pads
      Int_t padnum = 1;
      if ((plottype != 0) &&
         ((strcasecmp (plottype, kPTCrossCorrelation) == 0) || 
         (strcasecmp (plottype, kPTTransferFunction) == 0) ||
         (strcasecmp (plottype, kPTTransferCoefficients) == 0))) {
         padnum = 2;
      }
      // check if pads are available
      if (pad < 0) pad = 0;
      if (pad + padnum > 16) {
         return 0;
      }
      if (pad + padnum > GetPadNumber()) {
         SetPadNumber (pad + padnum);
      }
      // loop over padnumbers/variants
      Int_t show = 0;
      for (Int_t i = 0; i < padnum; i++) {
         show += ShowMultiPlot (data, plotnum, plottype, pad + i, i, update);
      }
      return show;
   }

//______________________________________________________________________________
   Int_t TLGMultiPad::ShowMultiPlot (const PlotDescriptor* data[], 
                     Int_t plotnum, const char* plottype, 
                     Int_t pad, Int_t variant, Bool_t update)
   {
      // check if pad is avaialable
      TLGPad* p = GetPad (pad);
      if (p != 0) {
         return p->ShowMultiPlot (data, plotnum, plottype, variant, update);
      }
      else {
         return 0;
      }
   }

//______________________________________________________________________________
   Bool_t TLGMultiPad::PrintPS (TLGPrintParam& print,
                     Int_t* errcode)
   {
      //cerr << "TLGMultiPad::PrintPS()" << endl;
      // Setup print job
      TString filename;
      if (!print.Setup (filename)) {
         if (errcode) *errcode = print.ErrorCode();
         return kFALSE;
      }
      Bool_t ret = kTRUE;
      if (errcode) *errcode = 0;
   
      // setup page boundaries
      Float_t w = print.fPaperSizeWidth;
      Float_t h = print.fPaperSizeHeight;
      if (print.fPageOrientation == 1) {
         Float_t t = w;
         w = h;
         h = t;
      }
      //cerr << "  PrintPS setting gStyle paper size w=" << w << " h=" << h << endl;
      gStyle->SetPaperSize (w, h);
      Int_t format = 0;
      if (fabs (print.fPaperSizeWidth - 21.59) < 1E-3) {
         format = 100;
      }
      else if (fabs (print.fPaperSizeWidth - 27.94) < 1E-3) {
         format = 300;
      }
      else if (fabs (print.fPaperSizeWidth - 20.99) < 1E-3) {
         format = 4;
      }
      else if (fabs (print.fPaperSizeWidth - 297.0) < 1E-3) {
         format = 3;
      }
      format *= 1000;
   
      // setup print canvas
      TVirtualPad* padsave = gPad;
      TVirtualPS*  pssave = gVirtualPS;
      Int_t zones;
      Int_t zonenum;
      switch (print.fPageLayout) {
         case 0:
         default:
	    // 1 up
            zones = 110;
            zonenum = 1;
            break;
         case 1:
	    // 2 up
            if (print.fPageOrientation == 1) {
               zones = 120;
            }
            else {
               zones = 120;
            }
            zonenum = 2;
            break;
         case 2:
	    // 4 up
            zones = 220;
            zonenum = 4;
            break;
      }
   
      // setup page orientation
      Int_t wtype;
      if (print.fPrintToFile && 
         ((print.fFileFormat == TLGPrintParam::kEPS) || 
         (print.fFileFormat == TLGPrintParam::kEPSI))) {
         zonenum = 1;
         wtype = format + 113;
      }
      else if (print.fPageOrientation == 1) {
	 // Landscape
         wtype = format + zones + 2;
      }
      else {
	 // Portrait
         wtype = format + zones + 1;
      }
   
      // get list of pads to print
      TLGPad*	list[100];
      Int_t	len = 0;
      switch (print.fPlotSelection) {
         case 0:
         default:
	    // Current pad
            list[0] = GetPad (GetActivePad());
            len = 1;
            break;
         case 1:
	    // First pad
            list[0] = GetPad (0);
            len = 1;
            break;
         case 2:
	    // Second pad
            list[0] = GetPad (1);
            len = 1;
            break;
         case 3:
	    // All pads
            for (int i = 0; (i < 100) && (i < GetPadNumber()); i++) {
               list[len++] = GetPad (i);
            }
            // swap order for 4up
            if (print.fPageLayout == 2) {
               while (len % 4 != 0) {
                  list[len++] = 0;
               }
               for (int i = 0; i < len / 4; i++) {
                  TLGPad* t = list[4*i + 1];
                  list[4*i + 1] = list[4*i + 2];
                  list[4*i + 2] = t;
               }
            }
            break;
      }
      if ((len == 0) || (list[0] == 0)) {
         if (errcode) *errcode = -3;
         ret = kFALSE;
      }
      if (ret && print.fPrintToFile && 
         ((print.fFileFormat == TLGPrintParam::kEPS) || 
         (print.fFileFormat == TLGPrintParam::kEPSI)) &&
         (len > 1)) {
         if (errcode) *errcode = 1;
         len = 1;
      }
      if (ret && print.fPrintToFile && 
         ((print.fFileFormat == TLGPrintParam::kJPEG) || 
         (print.fFileFormat == TLGPrintParam::kAI)) &&
         (len > zonenum)) {
         if (errcode) *errcode = 2;
         len = zonenum;
      }
      // print pads
      TPostScript*	ps = 0;
      if (ret) {
         // Setup postscript object
         gVirtualPS = 0;
         ps = new TLGPostScript (filename, wtype);
         if ((ps == 0) || (gVirtualPS == 0)) {
            if (errcode) *errcode = -4;
            ret = kFALSE;
         }
      }

      if (ret) {
         // loop over pages
         for (int i = 0; i < len; i++) {
            if (list[i] == 0) {
               ps->NewPage();
               continue;
            }
            // setup new page/zone
            gPad = list[i]->GetCanvas();
// Deep in the bowels of ROOT, TGraph::Draw() will call NewPage() so don't do it here.
// The call to UpdatePlot() will do it... JCB
//            ps->NewPage();
            // print
            list[i]->UpdatePlot (0, kFALSE);
         }
         ps->Close();
      }
      delete ps;
   
      // Finish print job
      if (!print.Finish (!ret)) {
         if (errcode) print.ErrorCode();
         ret = kFALSE;
      }
   
      // cleanup
      gPad = padsave;
      gVirtualPS = pssave;

      //cerr << "TLGMultiPad::PrintPS() end" << endl;
   
      return ret;
   }

//______________________________________________________________________________
// Respond to the Print... button or File->Print menu item.
   Bool_t TLGMultiPad::PrintPSDlg (const TLGPrintParam* defprint)
   {
      TLGPrintParam	pdlg;
      if (defprint) pdlg = *defprint;
      else if (fDefPrintSetup) pdlg = *fDefPrintSetup;
      if (pdlg.ShowDialog (fClient->GetRoot(), this)) {
         Int_t err;
         if (!PrintPS (pdlg, &err)) {
            TString msg;
            switch (err) {
               case -1:
                  msg = "Need ghostscript (gs) to create\n"
                     "PDF, JPEG, EPSI and Adobe Illustrator files.";
                  break;
               case -2:
                  msg = "Need epstool to create EPSI file.";
                  break;
               case -3:
                  msg = "Nothing to print.";
                  break;
               default:
                  msg = "Unable to complete print job.";
                  break;
            }
            new TGMsgBox (fClient->GetRoot(), this, "Error", 
                         msg, kMBIconStop, kMBOk);
            return kFALSE;
         }
         else if ((err >= 1) && (err <= 2)) {
            TString msg;
            switch (err) {
               case 1:
                  msg = "Multiple pages and multiple plots per page\n"
                     "are not supported for EPS and EPSI formats.";
                  break;
               case 2:
                  msg = "Multiple pages are not supported for\n"
                     "JPEG and Adobe Illustrator formats.";
                  break;
            }
            new TGMsgBox (fClient->GetRoot(), this, "Warning", 
                         msg, kMBIconExclamation, kMBOk);
         }
         return kTRUE;
      }
      else {
         return kFALSE;
      }
   }

//______________________________________________________________________________
// Respond to a click on the Import... button
   Bool_t TLGMultiPad::ImportDlg (ImportOption_t* defim)
   {
      if (fAction != 0) {
         return fAction->Import (defim, *fPlotSet, *this);
      }
      else {
         if (!defim) defim = fDefImportOpt;
         if (fPlotSet) {
            return ImportFromFileDlg (fClient->GetRoot(), this, *fPlotSet, 
                                 defim, fCalTable);
         }
         else {
            return kFALSE;
         }
      }
   }

//______________________________________________________________________________
// Respond to a click on the Export... button
   Bool_t TLGMultiPad::ExportDlg (ExportOption_t* defex)
   {
      if (fAction != 0) {
         return fAction->Export (defex, *fPlotSet, *this);
      }
      else {
         if (!defex) defex = fDefExportOpt;
         if (fPlotSet) {
            return ExportToFileDlg (fClient->GetRoot(), this, *fPlotSet, defex,
                                 fCalTable);
         }
         else {
            return kFALSE;
         }
      }
   }

//______________________________________________________________________________
// Respond to a click on the Reference... button
   Bool_t TLGMultiPad::ReferenceTracesDlg (ReferenceTraceList_t* ref)
   {
      if (!ref) ref = fRefTraces;
      if (fPlotSet && ref) {
         return ReferenceTraceDlg (fClient->GetRoot(), this, 
                              *fPlotSet, *ref, fCalTable);
      }
      else {
         return kFALSE;
      }
   }

//______________________________________________________________________________
// Respond to a click on the Math... button
   Bool_t TLGMultiPad::MathDlg (MathTable_t* math)
   {
      if (!math) math = fMathTable;
      if (fPlotSet && math) {
         return MathEditor (fClient->GetRoot(), this, *fPlotSet, *math);
      }
      else {
         return kFALSE;
      }
   }

//______________________________________________________________________________
   Bool_t TLGMultiPad::CalibrationEditDlg (calibration::Table* cal)
   {
      if (!cal) cal = fCalTable;
      if (fPlotSet && cal) {
         return CalibrationTableDlg (fClient->GetRoot(), this, 
                              *fPlotSet, *cal);
      }
      else {
         return kFALSE;
      }
   }

//______________________________________________________________________________
   Bool_t TLGMultiPad::CalibrationImportDlg (calibration::Table* cal)
   {
      if (!cal) cal = fCalTable;
      if (fPlotSet && cal) {
         return CalibrationTableImport (fClient->GetRoot(), this, 
                              *fPlotSet, *cal);
      }
      else {
         return kFALSE;
      }
   }

//______________________________________________________________________________
   Bool_t TLGMultiPad::CalibrationExportDlg (calibration::Table* cal)
   {
      if (!cal) cal = fCalTable;
      if (fPlotSet && cal) {
         return CalibrationTableExport (fClient->GetRoot(), this, 
                              *fPlotSet, *cal);
      }
      else {
         return kFALSE;
      }
   }

//______________________________________________________________________________
   Bool_t TLGMultiPad::Fill (PlotSet& pl, const char* winid)
   {
      // loop over pads
      for (Int_t pad = 0; pad < fPadNum; ++pad) {
         if (!fPadList[pad]) {
            continue;
         }
         // make pad name
         char padid[16];
         sprintf (padid, "pad%i", pad);
         // add traces from pad
         fPadList[pad]->Fill (pl, winid, padid);
      }
      return kTRUE;
   }

//______________________________________________________________________________
// Respond to a click on the Options... button or Window->Layout menu.
   Bool_t TLGMultiPad::OptionDlg()
   {
      // Get the current layout value to pass to the dialog box so
      // it knows what is currently used.  
      Int_t layout = GetPadLayout();
      new TLGLayoutDialog (fClient->GetRoot(), this, &layout);
      SetPadLayoutAndNumber (layout);
      return kTRUE;
   }

//______________________________________________________________________________
// Respond to a click on the Reset button.
   void TLGMultiPad::ResetPads()
   {
      if (fAction != 0) {
         fAction->Reset (*fPlotSet, *this);
      }
      else {
      }
   }

//______________________________________________________________________________
// Respond to a click on the New button or Window->New menu.
   TLGPadMain* TLGMultiPad::NewWindow()
   {
      TLGPadMain* mpad = 
         new TLGPadMain (gClient->GetRoot(), *fPlotSet, fPadName); 
      if ((mpad != 0) && (mpad->GetPads() != 0)) {
         mpad->GetPads()->SetStoreOptionList (
                              fStoreOptions, fStoreOptionsMax);
         mpad->GetPads()->SetDefPrintSetup (fDefPrintSetup);
         mpad->GetPads()->SetDefImportOpt (fDefImportOpt);
         mpad->GetPads()->SetDefExportOpt (fDefExportOpt);
         mpad->GetPads()->SetReferenceTraces (fRefTraces);
         mpad->GetPads()->SetMathTable (fMathTable);
         mpad->GetPads()->SetCalibrationTable (fCalTable);
         mpad->GetPads()->SetActionPlotPads (fAction);
      }
      return mpad;
   }

//______________________________________________________________________________
   Bool_t TLGMultiPad::ProcessButtons (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      switch (parm1) {
         case kGMPadResetID:
            {
               ResetPads();
               break;
            }
         case kGMPadZoomID:
            {
               if (fPadZoom >= 0) {
                  Zoom (-1);
               }
               else {
                  Zoom (GetActivePad());
               }
               break;
            }
         case kGMPadActiveID:
            {
               Int_t pad = GetActivePad() + 1;
               if (pad >= fPadNum) {
                  pad = 0;
               }
               SetActivePad (pad);
               break;
            }
         case kGMPadNewID:
            {
               NewWindow();
               break;
            }
         case kGMPadOptionID:
            {
               OptionDlg ();
               break;
            }
         case kGMPadImportID:
            {
               ImportDlg (fDefImportOpt);
               break;
            }
         case kGMPadExportID:
            {
               ExportDlg (fDefExportOpt);
               break;
            }
         case kGMPadReferenceID:
            {
               ReferenceTracesDlg (fRefTraces);
               break;
            }
         case kGMPadCalibrationID:
            {
               CalibrationEditDlg (fCalTable);
               break;
            }
         case kGMPadMathID:
            {
               MathDlg (fMathTable);
               break;
            }
         case kGMPadPrintID:
            {
               PrintPSDlg (fDefPrintSetup);
               break;
            }
         default:
            {
               SendMessage (fMsgWindow, msg, parm1, parm2);
               break;
            }
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGMultiPad::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      switch (GET_MSG (msg)) {
         case kC_COMMAND:
            {
               switch (GET_SUBMSG (msg)) {
                  case kCM_BUTTON:
                     {
                        return ProcessButtons (msg, parm1, parm2);
                     }
               }
               break;
            }
         case kC_OPTION:
            {
               if (GET_SUBMSG (msg) == kCM_CAL) {
                  CalibrationEditDlg (fCalTable);
               }
               break;
            }
      }
      return kTRUE;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGLayoutDialog                                                      //
//                                                                      //
// Dialogbox for TLGMultiPad layout                                     //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGLayoutDialog::TLGLayoutDialog (const TGWindow *p, const TGWindow *main,
                     Int_t* layout)
   : TLGTransientFrame (p, main, 10, 10), fLayout (layout)
   {
      if (!fLayout) {
         delete this;
      }
      fSelected = *fLayout;
   
      // set dialog box title
      SetWindowName ("Graphics Pad Options");
      SetIconName ("Graphics Pad Options");
      SetClassHints ("TLGLayoutDialog", "TLGLayoutDialog");
   
      // create child windows
      fL1 = new TGLayoutHints (kLHintsBottom | kLHintsExpandX, 2, 2, 2, 2);
      fL2 = new TGLayoutHints (kLHintsLeft | kLHintsTop, 2, 2, 2, 2);
      fLayoutFrame = new TGGroupFrame 
         (this, "Layout", kVerticalFrame | kSunkenFrame);
   
      fLayoutSelection[0] = new TGRadioButton (fLayoutFrame, 
                           "One single pad", 1);
      fLayoutSelection[1] = new TGRadioButton (fLayoutFrame, 
                           "Two vertical pads", 2);
      fLayoutSelection[2] = new TGRadioButton (fLayoutFrame, 
                           "Three pads in a 2x2 grid", 3);
      fLayoutSelection[3] = new TGRadioButton (fLayoutFrame, 
                           "Four pads in 2x2 grid", 4);
      fLayoutSelection[4] = new TGRadioButton (fLayoutFrame, 
                           "Five pads in 3x2 grid", 5);
      fLayoutSelection[5] = new TGRadioButton (fLayoutFrame, 
                           "Six pads in 3x2 grid", 6);
      fLayoutSelection[6] = new TGRadioButton (fLayoutFrame, 
                           "Seven pads in 3x3 grid", 7);
      fLayoutSelection[7] = new TGRadioButton (fLayoutFrame, 
                           "Eight pads in 3x3 grid", 8);
      fLayoutSelection[8] = new TGRadioButton (fLayoutFrame, 
                           "Nine pads in a 3x3 grid", 9);
      fLayoutSelection[9] = new TGRadioButton (fLayoutFrame, 
                           "Ten pads in a 4x3 grid", 10);
      fLayoutSelection[10] = new TGRadioButton (fLayoutFrame, 
                           "Eleven pads in a 4x3 grid", 11);
      fLayoutSelection[11] = new TGRadioButton (fLayoutFrame, 
                           "Twelve pads in 4x3 grid", 12);
      fLayoutSelection[12] = new TGRadioButton (fLayoutFrame, 
                           "Thirteen pads in 4x4 grid", 13);
      fLayoutSelection[13] = new TGRadioButton (fLayoutFrame, 
                           "Fourteen pads in 4x4 grid", 14);
      fLayoutSelection[14] = new TGRadioButton (fLayoutFrame, 
                           "Fifteen pads in 4x4 grid", 15);
      fLayoutSelection[15] = new TGRadioButton (fLayoutFrame, 
                           "Sixteen pads in a 4x4 grid", 16);
      fLayoutSelection[16] = new TGRadioButton (fLayoutFrame, 
                           "Two horizontal pads", 101);
      fLayoutSelection[17] = new TGRadioButton (fLayoutFrame, 
                           "Three pads in a 3x1 grid", 102);
      fLayoutSelection[18] = new TGRadioButton (fLayoutFrame, 
                           "Four pads in 4x1 grid", 103);
      fLayoutSelection[19] = new TGRadioButton (fLayoutFrame, 
                           "Three pads in 2x2 grid (one large 1x2 pad)", 104);
      fLayoutSelection[20] = new TGRadioButton (fLayoutFrame, 
                           "Six pads in 3x3 grid (one large 2x2 pad)", 105);
      // Figure out which button to have selected based on
      // the fSelected value passed to this function.
      if ((fSelected >= 1) && (fSelected <= 16)) {
         fLayoutSelection[fSelected - 1]->SetState (kButtonDown);
      }
      else if ((fSelected >= 101) && (fSelected <= 105)) {
         fLayoutSelection[fSelected - 100 + 16 - 1]->SetState (kButtonDown);
      }
      else {
         fSelected = 1;
         fLayoutSelection[0]->SetState (kButtonDown);
      }
      for (int i = 0; i < kLayoutNum; i++) {
         fLayoutSelection[i]->Associate (this);
         fLayoutFrame->AddFrame (fLayoutSelection[i], fL2);
      }
      AddFrame (fLayoutFrame, fL2);
   
      fButtonFrame = new TGCompositeFrame 
         (this, 100, 20, kHorizontalFrame | kSunkenFrame);
      fOkButton = new TGTextButton (fButtonFrame, 
                           new TGHotString ("   &Ok   "), 1);
      fOkButton->Associate (this);
      fButtonFrame->AddFrame (fOkButton, fL1);
      fCancelButton = new TGTextButton (fButtonFrame, 
                           new TGHotString (" &Cancel "), 0);
      fCancelButton->Associate (this);
      fButtonFrame->AddFrame (fCancelButton, fL1);
      AddFrame (fButtonFrame, fL1);
   
      // resize & move to center
      MapSubwindows ();
      Resize (GetDefaultSize());
      Window_t wdum;
      int ax;
      int ay;
      gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                           ((TGFrame*)main)->GetWidth() - (fWidth >> 1), 
                           (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                           ax, ay, wdum);
      Move (ax, ay);
      SetWMPosition (ax, ay);
      SetMWMHints (kMWMDecorAll, kMWMFuncAll, kMWMInputModeless);
      MapWindow();
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   TLGLayoutDialog::~TLGLayoutDialog ()
   {
      delete fOkButton;
      delete fCancelButton;
      delete fButtonFrame;
   
      for (int i = 0; i < kLayoutNum; i++) {
         delete fLayoutSelection[i];
      }
      delete fLayoutFrame;
   
      delete fL1;
      delete fL2;
   }

//______________________________________________________________________________
   void TLGLayoutDialog::CloseWindow()
   {
      DeleteWindow();
   }

//______________________________________________________________________________
   Bool_t TLGLayoutDialog::ProcessMessage (Long_t msg, Long_t parm1, Long_t)
   {
      switch (GET_MSG (msg)) {
         case kC_COMMAND:
            {
               switch (GET_SUBMSG (msg)) {
                  case kCM_BUTTON:
                     {
                        switch (parm1) {
                           // ok
                           case 1:
                              {
                                 *fLayout = fSelected;
                                 DeleteWindow();
                                 break;
                              }
                           // cancel
                           case 0:
                              {
                                 DeleteWindow();
                                 break;
                              }
                        }
                        break;
                     }
                  case kCM_RADIOBUTTON:
                     {
                        switch (parm1) {
                        
                           case 1:
                           case 2:
                           case 3:
                           case 4:
                           case 5:
                           case 6:
                           case 7:
                           case 8:
                           case 9:
                           case 10:
                           case 11:
                           case 12:
                           case 13:
                           case 14:
                           case 15:
                           case 16:
                           case 101:
                           case 102:
                           case 103:
                           case 104:
                           case 105:
                              {
                                 if (parm1 != fSelected) {
                                    if ((fSelected >= 1) && (fSelected <= 16)) {
                                       fLayoutSelection[fSelected - 1]->SetState (kButtonUp);
                                    }
                                    else if ((fSelected >= 101) && (fSelected <= 105)) {
                                       fLayoutSelection[fSelected - 100 + 16 - 1]->SetState (kButtonUp);
                                    }
                                    fSelected = parm1;
                                 }
                                 break;
                              }
                        }
                        break;
                     }
               }
               break;
            }
      }
      return kTRUE;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGPadMain                                                           //
//                                                                      //
// Main window containing a TLGMultiPad                                 //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGPadMain::TLGPadMain (const TGWindow *p, PlotSet& plots, 
                     const char* name, UInt_t w, UInt_t h, UInt_t options)
   : TLGMainFrame (p, w, h, options), fName (name ? name : "Plot Pad"), 
   fPlotSet (&plots)
   {
      Init (kFALSE);
   }

//______________________________________________________________________________
   TLGPadMain::TLGPadMain (const TGWindow *p, PlotSet& plots, 
                     Bool_t decorate_menu, const char* name, 
                     UInt_t w, UInt_t h, UInt_t options)
   : TLGMainFrame (p, w, h, options),  fName (name ? name : "Plot Pad"),
   fPlotSet (&plots)
   {
      Init (decorate_menu);
   }

//______________________________________________________________________________
   void TLGPadMain::Init (Bool_t decorate_menu)
   {
      MenuSetup (this, 0);
      // Create multipad
      fL = new TGLayoutHints (kLHintsExpandX | kLHintsExpandY, 2, 2, 5, 5);
      fMPad = new TLGMultiPad (this, fName, *fPlotSet, 1, 1);
      fMPad->Associate(this);
      TGTextButton* exitButton = new TGTextButton (fMPad, "Exit", 1);
      exitButton->SetToolTipText ("Exit main window");
      fMPad->AddButton (exitButton);
      AddFrame (fMPad, fL);
      fMPad->SetActivePad (0);
      SetMultiPad (fMPad);
   
      // Set pad name
      SetWindowName (fName);
      SetIconName (fName);
      SetClassHints (fName, fName);
   
      // Show window
      MapSubwindows ();
      Resize (GetDefaultSize());
      MapWindow ();
      // Register with plot set
      fPlotSet->RegisterWindow (this);
   }

//______________________________________________________________________________
   TLGPadMain::~TLGPadMain()
   {
      // unregister
      if (fPlotSet) {
         fPlotSet->UnregisterWindow (this);
      }
      // delete pad
      delete fMPad;
      delete fL;
   }

//______________________________________________________________________________
   Bool_t TLGPadMain::ProcessMessage (Long_t msg, Long_t parm1, Long_t parm2)
   {
      switch (GET_MSG (msg)) {
         case kC_COMMAND:
            {
               switch (GET_SUBMSG (msg)) {
                  case kCM_BUTTON:
                     {
                        switch (parm1) {
                           case 1:
                              CloseWindow();
                              break;
                        }
                     }
                  case kCM_MENU: 
                     {
                        ProcessMenu (parm1, parm2);
                     }
               }
               break;
            }
      }
      return kTRUE;
   }

//______________________________________________________________________________
   void TLGPadMain::CloseWindow()
   {
      DeleteWindow();
   }


#ifndef __NO_NAMESPACE
}
#endif
