/* version $Id: TLGMainWindow.cc 7582 2016-03-01 22:25:48Z john.zweizig@LIGO.ORG $ */
/* -*- mode: c++; c-basic-offset: 3; -*- */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/* Module Name:  TLGMainWindow						     */
/*                                                                           */
/* Module Description:  main window of diagnostics viewer		     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
#include "PConfig.h"
#include <pthread.h>
#include <strings.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <deque>
#include <string>

#include <RVersion.h>
#include <TGClient.h>
#include <TApplication.h>
#include <TVirtualX.h>
#include <TGListBox.h>
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGButton.h>
#include <TGMsgBox.h>
#include <TGMenu.h>
#include <TGComboBox.h>
#include <TGFileDialog.h>
#include <TSystem.h>
#include <TString.h>
#include <TEnv.h>

#include "PlotSet.hh"
#include "TLGMainWindow.hh"
#include "TLGEntry.hh"
#include "TLGPad.hh"
#include "TLGPrint.hh"
#include "TLGExport.hh"
#include "TLGMath.hh"
#include "TLGCalDlg.hh"
#include "TLGOptions.hh"
#include "TLGSave.hh"
#include "Table.hh"
#include "tconv.h"

static const int my_debug = 0 ;

namespace ligogui {
   using namespace std;



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// MsgQueueMutex	                                                //
//                                                                      //
// Message queue                                                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class MsgQueueMutex {
   public:
      MsgQueueMutex () {
         pthread_mutex_init (&mux, NULL); }
      ~MsgQueueMutex () {
         pthread_mutex_destroy (&mux); }
      MsgQueueMutex (const MsgQueueMutex&) {
         pthread_mutex_init (&mux, NULL); }
      MsgQueueMutex& operator= (const MsgQueueMutex&) {
         return *this; }
      void lock () {
         pthread_mutex_lock (&mux); }
      void unlock () {
         pthread_mutex_unlock (&mux); }
      bool trylock () {
         return (pthread_mutex_trylock (&mux) == 0); }
   protected:
      pthread_mutex_t		mux;
   };



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Constants       	                                                //
//                                                                      //
// Constants                                                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   const Long_t kHeartbeatInterval = 100; // 100 ms
   const Long_t kX11WatchdogInterval = 120*1000; // 2 minutes


   GContext_t TLGMainWindow::fgButtonGC = 0;
   FontStruct_t TLGMainWindow::fgButtonFont = 0;
   Cursor_t TLGMainWindow::fWaitCursor = (Cursor_t)-1;



   static const char* const gSaveAsTypes[] = { 
   "LIGO light weight", "*.xml",
   "All files", "*",
   0, 0 };


   static const char* const gOpenTypes[] = { 
   "LIGO light weight", "*.xml",
   "Script file", "*.cmd",
   "All files", "*",
   0, 0 };


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// ExtraXML       	                                                //
//                                                                      //
// ExtraXML                                                             //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class ExtraXML : public ostringstream {
   public:
      ExtraXML () {
      }
   };



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// ActionPlots       	                                                //
//                                                                      //
// ActionPlots                                                          //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class ActionPlots : public TLGMultiPad::ActionPlotPads {
   protected:
      /// Reference to main window
      TLGMainWindow* 	fWin;
   
   public:
      /// Default constructor
      explicit ActionPlots (TLGMainWindow* w) : fWin (w) {
      }
      virtual ~ActionPlots(void) {}
      /// apply reset
      virtual bool Reset (const PlotSet& pset, TLGMultiPad& mpad);
      /// export
      virtual bool Export (ExportOption_t* defex,
                        const PlotSet& pset, TLGMultiPad& mpad);
   };

//______________________________________________________________________________
   bool ActionPlots::Reset (const PlotSet& pset, TLGMultiPad& mpad) 
   {
      fWin->ShowDefaultPlot (kTRUE, &mpad); 
      return true; 
   }

//______________________________________________________________________________
   bool ActionPlots::Export (ExportOption_t* defex,
                     const PlotSet& pset, TLGMultiPad& mpad) 
   {
      typedef vector<PlotDescriptor*> PlotDescList;
   
      // make plot decriptors for pad traces
      PlotSet traces;
      // get plot descriptors from embedded pads
      if (fWin->fMPad) {
         fWin->fMPad->Fill (traces, "win0");
      }
      // loop over multi pads
      const PlotSet::winlist* list = fWin->fPlot->GetRegisteredWindows();
      if (!list) {
         return kFALSE;
      }
      Int_t i = 2;
      for (PlotSet::winlist::const_iterator el = list->begin(); 
          el != list->end(); ++el, ++i) {
         TLGPadMain* w = dynamic_cast <TLGPadMain*> (*el);
         if (!w) {
            continue;
         }
         // get plot descriptors from window
         char winid[16];
         sprintf (winid, "win%i", i - 1);
         w->GetPads()->Fill (traces, winid);
      }
      PlotDescList pdlist;
      for (PlotSet::iterator i = traces.begin();
          i != traces.end(); ++i) {
         pdlist.push_back (&*i);
      }
      ((PlotSet&)pset).Merge (traces);
   
      // call export
      bool ret = TLGMultiPad::ActionPlotPads::Export (defex, pset, mpad);
   
      // cleanup plot decriptors for traces
      for (PlotDescList::iterator i = pdlist.begin();
          i != pdlist.end(); ++i) {
         delete *i;
      }
      return ret;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGMainWindow       	                                                //
//                                                                      //
// TLGMainWindow                                                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGMainWindow::TLGMainWindow (const TGWindow *p, const char* title)
   : TLGMainFrame (p, 10, 10), fFiletype (0), fHeartbeat (0),
   fSkipHeartbeats (0), fX11Watchdog (0)
   {
      fWindowTitle = title ? title : "Diagnostics Viewer"; 
      if (fWaitCursor == (Cursor_t)-1) {
         fWaitCursor = gVirtualX->CreateCursor (kWatch);
      }
   
      // create plot set
      fPlot = new PlotSet ();
      // create printer defaults
      fPrintDef = new TLGPrintParam();
      fPrintDef->fPageOrientation = 1;
      // create import options defaults
      fImportDef = new ImportOption_t;
      SetDefaultImportOptions (*fImportDef);
      // create export options defaults
      fExportDef = new ExportOption_t;
      SetDefaultExportOptions (*fExportDef);
      // create reference traces list
      fRefTraces = new ReferenceTraceList_t;
      SetDefaultReferenceTraces (*fRefTraces);
      // create math function table
      fMathTable = new MathTable_t;
      SetDefaultMathTable (*fMathTable);
      // create calibration table
      fCalTable = new calibration::Table;
      calibration::SetDefaultTable (*fCalTable);
      // initialize reset
      fAction = new ActionPlots (this);
      // create option array
      fStoreOptions = new OptionArray (kPlotMax, kPlotOptionsMax);
      // init xml objects
      fXMLObjects = 0;
      // init message queue
      fMsgQueueLock = new MsgQueueMutex();
   
      // Setup graphics context
      if (fgButtonGC == 0) {
         fgButtonFont = gClient->GetFontByName 
            (gEnv->GetValue
            ("Gui.NormalFont",
            "-adobe-helvetica-bold-r-*-*-14-*-*-*-*-*-iso8859-1"));
         GCValues_t   gval;
         gval.fMask = kGCForeground | kGCFont;
         gval.fFont = gVirtualX->GetFontHandle (fgButtonFont);
         gval.fForeground = fgBlackPixel;
         //fgButtonGC = gVirtualX->CreateGC(gClient->GetRoot()->GetId(), &gval);
         fgButtonGC = gClient->GetGC(&gval)->GetGC();
      }
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::Setup (const char* filename, Int_t padnum)
   {
      return SetupWH(1100, 870, filename, padnum) ;
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::SetupWH (int w, int h, const char* filename, Int_t padnum)
   {
      MenuSetup (this, 0);
   
      // Status bar
      fStatus = GetStatusBar();
      if (fStatus != 0) {
         fStatusBarLayout = 
            new TGLayoutHints (kLHintsExpandX | kLHintsBottom, 0, 0, 0, 0);
         AddFrame (fStatus, fStatusBarLayout);
      }
      else {
         fStatusBarLayout = 0;
      }
   
      // Main window
      fMainWindowLayout = 
         new TGLayoutHints (kLHintsExpandX | kLHintsExpandY, 0, 0, 0, 0);
      fMainWindowFrame = new TGHorizontalFrame (this, 10, 10, kSunkenFrame);
      AddFrame (fMainWindowFrame, fMainWindowLayout);
      fMPad = AddMainWindow (fMainWindowFrame, fMainWindowLayout, 
                           *fPlot, padnum);
      SetMultiPad (fMPad);
   
      // setup of multipad
      if (fMPad != 0) {
         fMPad->SetDefPrintSetup (fPrintDef);
         fMPad->SetDefImportOpt (fImportDef);
         fMPad->SetDefExportOpt (fExportDef);
         fMPad->SetReferenceTraces (fRefTraces);
         fMPad->SetMathTable (fMathTable);
         fMPad->SetCalibrationTable (fCalTable);
         fMPad->SetStoreOptionList ((*fStoreOptions)[0],
                              fStoreOptions->GetMaxPad());
         fMPad->SetActionPlotPads (fAction);
      }
   
      // Buttons
      fButtonFrameLayout = 
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 0, 0, 0, 0);
      fButtonLayout =
         new TGLayoutHints (kLHintsExpandX | kLHintsTop, 25, 25, 10, 6);
      fButtonFrame = new TGHorizontalFrame (this, 10, 10);
      AddFrame (fButtonFrame, fButtonFrameLayout);
      for (int i = 0; i < kButtonMax; i++) {
         fButtons[i] = 0;
      }
      AddButtons (fButtonFrame, fButtons, kButtonMax, fButtonLayout);
   
      // Setup window
      SetWindowName (fWindowTitle);
      MapSubwindows ();
      Resize (w, h);
      MapWindow ();
   
      // restore plots from file if filename is provided
      fFilename = "";
      TString error;
      InitSettings();
      if ((filename != 0) && (strlen (filename) != 0) &&
         RestoreFromFile (fFiletype, filename, error)) {
         fFilename = filename;
         SetWindowName (fWindowTitle + " - " + fFilename);
         fPlot->Update();
      }
   
      // initialze heartbeat timer
      fHeartbeat = new TTimer (this, kHeartbeatInterval, kTRUE);
      fHeartbeat->TurnOn();
      // initialze X11 watchdog
      fX11Watchdog = new TTimer (this, kX11WatchdogInterval, kTRUE);
      fX11Watchdog->TurnOn();
   
      return kTRUE;
   }

//______________________________________________________________________________
   TGStatusBar* TLGMainWindow::GetStatusBar()
   {
      TGStatusBar* status = new TGStatusBar (this, 100, 25);
      Int_t 	parts[4] = {100, 10, 14, 6};
      status->SetParts (parts, 1);
      return status;
   }

//______________________________________________________________________________
   TLGMultiPad* TLGMainWindow::AddMainWindow (TGCompositeFrame* p,
                     TGLayoutHints* mainLayout, PlotSet& plotset,
                     int padnum)
   {
      if (padnum < 1) padnum = 1;
      TLGMultiPad* pad = 
         new TLGMultiPad (p, "Plot", plotset, kMainFrameId, padnum);
      if (padnum > 0) pad->GetPad(0)->HidePanel (kFALSE);
      if (padnum > 1) pad->GetPad(1)->HidePanel (kFALSE);
      p->AddFrame (pad, mainLayout);
      return pad;
   }

//______________________________________________________________________________
   int TLGMainWindow::AddButtons (TGCompositeFrame* p, TGButton* btns[], 
                     int max, TGLayoutHints* btnLayout,
                     GContext_t btnGC, FontStruct_t btnFont)
   {
      return AddStdButtons (kButtonStart | kButtonPause | kButtonResume |
                           kButtonAbort, 
                           p, btns, max, btnLayout, btnGC, btnFont);
   }

//______________________________________________________________________________
   int TLGMainWindow::AddStdButtons (int flag, TGCompositeFrame* p, 
                     TGButton* btns[], int max, TGLayoutHints* btnLayout,
                     GContext_t btnGC, FontStruct_t btnFont)
   {
      const int nBDef(9);
      const char* const buttontext[] = 
         {"Clear", "Update", "Run", "Start", "Pause", "Resume", 
         "Abort", "Stop", "Exit"};
      const char* const buttonhelp[] = 
         {"Clear data",
         "Update data", 
         "Start taking data", 
         "Start the measurement", 
         "Pause the measurement", 
         "Resume the measurement", 
         "Abort the measurement", 
         "Stop taking data", 
         "Exit program"};

      //--------------------------------  Mask off flags for undefined buttons
      flag &= (1 << nBDef) - 1;

      //--------------------------------  Set max to nButtons.
      int num = 0;
      for (int i = 0; i < max; i++) {
         if ((flag & (0x01 << i)) != 0) {
            btns[i] = new TGTextButton (p, buttontext[i], kB_CLEAR + i, 
                                 btnGC, btnFont);
            btns[i]->Associate (this);
            btns[i]->SetToolTipText (buttonhelp[i]);
            // disable certain buttons by default
            if ((i == 4) || (i == 5) || (i == 6)) {
               btns[i]->SetState (kButtonDisabled);
            }
            p->AddFrame (btns[i], btnLayout);
            num++;
         }
         else {
            btns[i] = 0;
         }
      }
      return num;
   }

//______________________________________________________________________________
   TLGMainWindow::~TLGMainWindow()
   {
      // delete main window
      if (fMPad) delete fMPad;
      delete fMainWindowLayout;
      delete fMainWindowFrame;
      // delete buttons
      for (int i = 0; i < kButtonMax; i++) {
         if (fButtons[i]) delete fButtons[i];
      }
      delete fButtonFrame;
      delete fButtonLayout;
      delete fButtonFrameLayout;
      // delete status bar
      if (fStatus) delete fStatus;
      if (fStatusBarLayout) delete fStatusBarLayout;
      // delete plot set
      delete fPlot;
      // delete default values
      delete fPrintDef;
      delete fImportDef;
      delete fExportDef;
      // delete options
      if (fXMLObjects) delete fXMLObjects;
      delete fRefTraces;
      delete fMathTable;
      delete fCalTable;
      delete fStoreOptions;
      delete fAction;
      // delete timer
      delete fHeartbeat;
      delete fX11Watchdog;
      // delete message queue
      for (messagequeue::iterator i = fMsgQueue.begin();
          i != fMsgQueue.end(); i++) {
         delete *i;
      }
      fMsgQueue.clear();
      delete fMsgQueueLock;
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::Shutdown()
   {
      return kTRUE;
   }

//______________________________________________________________________________
   void TLGMainWindow::CloseWindow()
   {
      Int_t	ret = 0;
      new TGMsgBox (gClient->GetRoot(), this, "Exit", 
                   "Do you really want to quit?", kMBIconQuestion, 
                   kMBYes | kMBNo, &ret);
      if ((ret == kMBYes) && Shutdown()) {
         TGMainFrame::CloseWindow();
         gApplication->Terminate(0);
      }
   }

//______________________________________________________________________________
   TLGRestorer* TLGMainWindow::GetXMLRestorer (const char* filename, 
                     ESaveRestoreFlag restoreflag, TString& error,
                     ostream* extra)
   {
      return new (nothrow) TLGXMLRestorer 
         (filename, (TLGXMLRestorer::ERestoreFlag)restoreflag, 
         error, extra);
   }

//______________________________________________________________________________
   TLGRestorer* TLGMainWindow::GetRestorer (Int_t filetype, 
                     const char* filename, ESaveRestoreFlag restoreflag, 
                     TString& error)
   {
      TLGRestorer* rest = 0;
      if (filetype == 0) {
         if (fXMLObjects) delete fXMLObjects;
         fXMLObjects = new ExtraXML;
         rest = GetXMLRestorer (filename, restoreflag, error, fXMLObjects);
         fFiletype = filetype;
      }
      else {
         error = "Unrecognized file format";
      }
      return rest;
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::RestoreFromFile (Int_t filetype,
                     const char* filename, TString& error)
   {
      Bool_t ret = kTRUE;
      TLGRestorer* restore = 
         GetRestorer (filetype, filename, fFileSaveFlag, error);
      if (restore == 0) {
         return kFALSE;
      }
      if (ret) {
         ret = restore->Setup();
      }
      if (ret && (fFileRestoreFlag != kSaveRestoreParameterOnly)) {
         ret = restore->Data (*fPlot);
      }
      if (ret && (fFileRestoreFlag != kSaveRestoreParameterOnly)) {
         ret = restore->ReferenceList (*fRefTraces);
      }
      if (ret && fSettingsRestoreFlag) {
         ret = restore->PlotSettings (*fStoreOptions);
      }
      if (ret && fSettingsRestoreFlag) {
         ret = restore->Math (*fMathTable);
      }
      if (ret && fCalibrationRestoreFlag) {
         ret = restore->CalibrationData (*fCalTable);
      }
      if (((restore != 0) && !restore->Done (ret))) {
         ret = kFALSE;
      }
      delete restore;
   
      // Setup plots
      if (ret) {
         ShowPlots();
      }
      return ret;
   }

//______________________________________________________________________________
   TLGSaver* TLGMainWindow::GetXMLSaver (const char* filename, 
                     ESaveRestoreFlag saveflag, 
                     TString& error, string* extra)
   {
      return new (nothrow) TLGXMLSaver 
         (filename, (TLGSaver::ESaveFlag)saveflag, error, extra);
   }

//______________________________________________________________________________
   TLGSaver* TLGMainWindow::GetSaver (Int_t filetype, 
                     const char* filename, ESaveRestoreFlag saveflag, 
                     TString& error)
   {
      TLGSaver* sav = 0;
      if (filetype == 0) {
         string* extra = 
            fXMLObjects ? new (nothrow) string (fXMLObjects->str()) : 0;
         sav = GetXMLSaver (filename, saveflag, error, extra);
         fFiletype = filetype;
      }
      else {
         error = "Unrecognized file format";
      }
      return sav;
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::SaveToFile (Int_t filetype,
                     const char* filename, TString& error)
   {
      cout << "save to " << filename << endl;
      Bool_t ret = kTRUE;
      TLGSaver* save = GetSaver (filetype, filename, fFileSaveFlag, error);
      if (save == 0) {
         return kFALSE;
      }
      if (ret) {
         ret = save->Setup();
	 if (!ret && my_debug)
	    cerr << "TLGMainWindow::SaveToFile() fail, line " << __LINE__ << endl ;
      }
      if (ret && (fFileSaveFlag != kSaveRestoreParameterOnly)) {
         ret = save->ReferenceList (*fRefTraces);
	 if (!ret && my_debug)
	    cerr << "TLGMainWindow::SaveToFile() fail, line " << __LINE__ << endl ;
      }
      if (ret && fSettingsSaveFlag) {
         GetPlotSettings();
         ret = save->PlotSettings (*fStoreOptions);
	 if (!ret && my_debug)
	    cerr << "TLGMainWindow::SaveToFile() fail, line " << __LINE__ << endl ;
      }
      if (ret && fSettingsSaveFlag) {
         ret = save->Math (*fMathTable);
	 if (!ret && my_debug)
	    cerr << "TLGMainWindow::SaveToFile() fail, line " << __LINE__ << endl ;
      }
      if (ret && fCalibrationSaveFlag) {
         ret = save->CalibrationData (*fCalTable);
	 if (!ret && my_debug)
	    cerr << "TLGMainWindow::SaveToFile() fail, line " << __LINE__ << endl ;
      }
      if (ret && (fFileSaveFlag != kSaveRestoreParameterOnly)) {
         ret = save->Data (*fPlot);
	 if (!ret && my_debug)
	    cerr << "TLGMainWindow::SaveToFile() fail, line " << __LINE__ << endl ;
      }
      if (!save->Done (ret)) {
         ret = kFALSE;
      }
      delete save;
      return ret;
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::FileNew()
   {
      ClearResults();
      fFilename = "";
      SetWindowName (fWindowTitle);
      InitSettings();
      for (int i = 0; i < 16; i++) {
         OptionAll_t* opt = fMPad ? fMPad->GetPlotOptions (i) : 0;
         if (opt) opt->fTraces.fGraphType = "";
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::FileOpen()
   {
      TGFileInfo	info;
      info.fFilename = 0;
      info.fIniDir = 0;
   #if ROOT_VERSION_CODE >= ROOT_VERSION(3,1,6)
      info.fFileTypes = const_cast<const char**>(gOpenTypes);
   #else
      info.fFileTypes = const_cast<char**>(gOpenTypes);
   #endif
   #if 1
      info.fFileTypeIdx = 4 ;
      {
	 new TLGFileDialog(this, &info, kFDOpen) ;
      }
      if (!info.fFilename)
   #else
      if (!TLGFileDialog (this, info, kFDOpen, 0))
   #endif
      {
         return kFALSE;
      }
      gVirtualX->SetCursor (fId, fWaitCursor);
      gVirtualX->Update();
      ClearResults ();
      TString error;
      Bool_t ret = RestoreFromFile (fFiletype, info.fFilename, error);
      if (!ret) {
         new TGMsgBox (gClient->GetRoot(), this, "Error", 
                      error, kMBIconStop, kMBOk);
      }
      else {
         fFilename = info.fFilename;
         SetWindowName (fWindowTitle + " - " + fFilename);
         //ShowPlots();
      }
      fPlot->Update();
      gVirtualX->SetCursor (fId, kNone);
   #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
      delete [] info.fFilename;
   #endif
      return ret;
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::FileSave()
   {
      if (fFilename.Length() == 0) {
         return FileSaveAs();
      }
      gVirtualX->SetCursor (fId, fWaitCursor);
      gVirtualX->Update();
      TString error;
      Bool_t ret = SaveToFile (fFiletype, fFilename, error);
      gVirtualX->SetCursor (fId, kNone);
      if (!ret) {
         new TGMsgBox (gClient->GetRoot(), this, "Error", 
                      error, kMBIconStop, kMBOk);
      }
      return ret;
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::FileSaveAs()
   {
      // file save as dialog
      TGFileInfo info;
      info.fFilename = 0;
      info.fIniDir = 0;
   #if ROOT_VERSION_CODE >= ROOT_VERSION(3,1,6)
      info.fFileTypes = const_cast<const char**>(gSaveAsTypes);
   #else
      info.fFileTypes = const_cast<char**>(gSaveAsTypes);
   #endif
   #if 1
      info.fFileTypeIdx = 0 ;
      {
	 new TLGFileDialog(this, &info, kFDSave) ;
      }
      if (!info.fFilename)
   #else
      if (!TLGFileDialog (this, info, kFDSave, ".xml"))
   #endif
      {
         return kFALSE;
      }
      gVirtualX->SetCursor (fId, fWaitCursor);
      gVirtualX->Update();
      TString error;
      Bool_t ret = SaveToFile (fFiletype, info.fFilename, error);
      gVirtualX->SetCursor (fId, kNone);
      if (!ret) {
         new TGMsgBox (gClient->GetRoot(), this, "Error", 
                      error, kMBIconStop, kMBOk);
      }
      else {
         fFilename = info.fFilename;
         SetWindowName (fWindowTitle + " - " + fFilename);
      }
   #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
      delete [] info.fFilename;
   #endif
      return ret;
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::FileExport()
   {
      return TLGMainMenu::FileExport();
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::FileImport()
   {
      return TLGMainMenu::FileImport();
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::GetPlotSettings ()
   {
      OptionArray& opts = *fStoreOptions;
   
      // delete old settings of pads
      for (int i = 1; i < opts.GetMaxWin(); i++) {
         fMPadLayout[i] = 0;
         for (int j = 0; j < opts.GetMaxPad(); j++) {
            if (opts(i,j)) delete opts(i,j);
            opts[i][j] = 0;
         }
      }
   
      // get plot settings from embedded pads
      if (fMPad) {
         fMPadLayout[1] = fMPad->GetPadLayout();
         for (Int_t j = 0; (j < fMPad->GetPadNumber()) &&
             (j < opts.GetMaxPad()); j++) {
            OptionAll_t* opt = fMPad->GetPlotOptions (j);
            if (opt) {
               opts[1][j] = new OptionAll_t (*opt);
            }
         }
      }
   
      // loop over multi pads
      const PlotSet::winlist* list = fPlot->GetRegisteredWindows();
      if (!list) {
         return kFALSE;
      }
      Int_t i = 2;
      for (PlotSet::winlist::const_iterator el = list->begin(); 
          el != list->end(); ++el, ++i) {
         TLGPadMain* w = dynamic_cast <TLGPadMain*> (*el);
         if (!w) {
            continue;
         }
         // get pad settings
         fMPadLayout[i] = w->GetPads()->GetPadLayout();
         for (Int_t j = 0; j < w->GetPads()->GetPadNumber(); j++) {
            OptionAll_t* opt =  w->GetPads()->GetPlotOptions (j);
            if (opt && (i < opts.GetMaxWin()) && (j < opts.GetMaxPad())) {
               opts[i][j] = new OptionAll_t (*opt);
            }
         }
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::SetPlotSettings ()
   {
      OptionArray& opts = *fStoreOptions;
      Bool_t ret = kTRUE;
   
      // get number desired of windows
      Int_t win = 1;
      for (int i = 2; i < opts.GetMaxWin(); i++) {
         if (opts[i][0] != 0) {
            win++;
         }
      }
      // count active windows 
      const PlotSet::winlist* list = fPlot->GetRegisteredWindows();
      if (!list) {
         return kFALSE;
      }
      // if active windows smaller than desired, create new ones
      if (fMPad) {
         for (int i = list->size() + 1; i < win; i++) {
            if (fMPad->NewWindow() == 0) {
               ret = kFALSE;
            }
         }
      }
      // set plot settings in embedded pads
      fMPadLayout[1] = 0;
      for (int i = 0; i < opts.GetMaxPad(); i++) {
         if (opts[1][i] != 0) {
            fMPadLayout[1]++;
         }
      }  
      if (fMPadLayout[1] <= 0) fMPadLayout[1] = 2;
      if (fMPad) {
         if (fMPad->GetPadLayout() != fMPadLayout[1]) {
            fMPad->SetPadLayoutAndNumber (fMPadLayout[1]);
         }
         for (Int_t j = 0; j < fMPad->GetPadNumber(); j++) {
            OptionAll_t* opt = fMPad->GetPlotOptions (j);
            if ((j < opts.GetMaxPad()) && opts[1][j] && opt) {
               *opt = *(opts[1][j]);
            }
         }
      }
   
      // set plot settings in multi pad windows
      Int_t i = 2;
      for (PlotSet::winlist::const_iterator el = list->begin(); 
          el != list->end(); ++el, ++i) {
         TLGPadMain* w = dynamic_cast <TLGPadMain*> (*el);
         if (!w) {
            continue;
         }
         if (i >= opts.GetMaxWin()) {
            break;
         }
         // set multipad layout
         if (fMPadLayout[i] <= 0) fMPadLayout[i] = 2;
         if (w->GetPads()->GetPadLayout() != fMPadLayout[i]) {
            w->GetPads()->SetPadLayoutAndNumber (fMPadLayout[i]);
         }
         // set plot settings
         for (Int_t j = 0; j < w->GetPads()->GetPadNumber(); j++) {
            OptionAll_t* opt = w->GetPads()->GetPlotOptions (j);
            if ((i < opts.GetMaxWin()) && (j < opts.GetMaxPad()) &&
               opts[i][j] && opt) {
               *opt = *opts[i][j];
            }
         }
         i++;
      }
      return ret;
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::ShowDefaultPlot (Bool_t updatePads,
                     TLGMultiPad* mpads, Int_t hint)
   {
      // Determine default plot type
      int meastype = hint;
      if ((hint >= 0) && (hint < 6)) {
         meastype = hint;
      }
      else {
         // check what's there
         bool hasFS = false;
         bool hasPS = false;
         bool hasTS = false;
         bool hasTFunc = false;
         bool hasTCoef = false;
         bool hasHist1 = false; // hist (mito)
         for (PlotSet::iterator iter = fPlot->begin();
             iter != fPlot->end(); ++iter) {
            cout << "type = :>" << iter->GetGraphType() << "<:" << endl;
            if (strcasecmp (iter->GetGraphType(), 
                           kPTFrequencySeries) == 0) {
               hasFS = true;
            }
            else if (strcasecmp (iter->GetGraphType(), 
                                kPTPowerSpectrum) == 0) {
               hasPS = true;
            }
            else if (strcasecmp (iter->GetGraphType(), 
                                kPTTransferFunction) == 0) {
               hasTFunc = true;
            }
            else if (strcasecmp (iter->GetGraphType(), 
                                kPTTransferCoefficients) == 0) {
               hasTCoef = true;
            }
            else if (strcasecmp (iter->GetGraphType(), 
                                kPTTimeSeries) == 0) {
               hasTS = true;
            }
            else if (strcasecmp (iter->GetGraphType(), 
                                kPTHistogram1) == 0) { // hist (mito)
               hasHist1 = true;
            }
         }
         if (hasPS) {
            meastype = 0;
         }
         else if (hasFS) {
            meastype = 4;
         }
         else if (hasTFunc) {
            meastype = 1;
         }
         else if (hasTCoef) {
            meastype = 2;
         }
         else if (hasTS) {
            meastype = 3;
         }
         else if (hasHist1) { // hist (mito)
            meastype = 5;
         }
      }
      // Setup plot type
      TString graph[2];
      switch (meastype) {
         case 0:
            graph[0] = kPTPowerSpectrum;
            graph[1] = kPTCoherence;
            break;
         case 1:
            graph[0] = kPTTransferFunction;
            graph[1] = kPTTransferFunction;
            break;
         case 2:
            graph[0] = kPTTransferCoefficients;
            graph[1] = kPTTransferCoefficients;
            break;
         case 3:
            graph[0] = kPTTimeSeries;
            graph[1] = kPTTimeSeries;
            break;
         case 4:
            graph[0] = kPTFrequencySeries;
            graph[1] = kPTFrequencySeries;
            break;
         case 5: //hist (mito)
            graph[0] = kPTHistogram1;
            graph[1] = kPTHistogram1;
            break;
      }
   
      // Create default set
      const PlotDescriptor* pset[2][8];
      Int_t num[2]; num[0] = 0; num[1] = 0;
      for (PlotSet::iterator iter = fPlot->begin();
          iter != fPlot->end(); ++iter) {
         for (int i = 0; i < 2; i++) {
            if ((num[i] < 8) &&
               (graph[i] == iter->GetGraphType())) { // &&
               //(strchr (iter->GetAChannel(), '[') == 0) &&
               //(strchr (iter->GetAChannel(), '(') == 0)) {
               pset[i][num[i]++] = &*iter;
            }
         }
      }
   
      // Display curves
      TLGMultiPad* pads = mpads ? mpads : fMPad;
      // transfer function and coefficients
      if ((meastype == 1) ||
         (meastype == 2)) {
         if (num[0] > 0) {
            OptionAll_t* opt = pads->GetPlotOptions (0);
            if ((opt != 0) && 
               ((mpads != 0) || (opt->fTraces.fGraphType.Length() == 0) ||
		(opt->fConfig.fAutoConf && !opt->fConfig.fRespectUser))) {
               if (mpads != 0) SetDefaultGraphicsOptions (*opt);
               pads->ShowMultiPlot (pset[0], num[0], graph[0], 0, 0, kFALSE);
            }
            opt = pads->GetPlotOptions (1);
            if ((opt != 0) && 
               ((mpads != 0) || (opt->fTraces.fGraphType.Length() == 0) ||
		(opt->fConfig.fAutoConf && !opt->fConfig.fRespectUser))) {
               if (mpads != 0) SetDefaultGraphicsOptions (*opt);
               pads->ShowMultiPlot (pset[0], num[0], graph[0], 1, 1, kFALSE);
            }
         }
         // set manual x range for Bode Plot
         // if (meastype == 1) {
            // for (int i = 0; i < 2; i++) {
               // OptionAll_t* opt = pads->GetPlotOptions(i);
               // if (opt != 0) {
                  // opt->fRange.fRange[0] = kRangeManual;
                  // opt->fRange.fRangeFrom[0] = 0.9 * fParam->fMeas.fStart;
                  // opt->fRange.fRangeTo[0] = 1.1 * fParam->fMeas.fStop;
               // }
            // }
         // }
      }
      // power spectrum/coherence
      else if (meastype == 0) {
         if (num[0] > 0) {
            OptionAll_t* opt = pads->GetPlotOptions (0);
            if ((opt != 0) && 
               ((mpads != 0) || (opt->fTraces.fGraphType.Length() == 0) ||
		(opt->fConfig.fAutoConf && !opt->fConfig.fRespectUser))) {
               if (mpads != 0) SetDefaultGraphicsOptions (*opt);
               pads->ShowMultiPlot (pset[0], num[0], graph[0], 0, 0, kFALSE);
            }
         }
         if (num[1] > 0) {
            OptionAll_t* opt = pads->GetPlotOptions (1);
            if ((opt != 0) && 
               ((mpads != 0) || (opt->fTraces.fGraphType.Length() == 0) ||
		(opt->fConfig.fAutoConf && !opt->fConfig.fRespectUser))) {
               if (mpads != 0) SetDefaultGraphicsOptions (*opt);
               pads->ShowMultiPlot (pset[1], num[1], graph[1], 1, 0, kFALSE);
            }
         }
      }
      // time series
      else if (meastype == 3) {
         OptionAll_t* opt = pads->GetPlotOptions (0);
         if ((opt != 0) && 
            ((mpads != 0) || (opt->fTraces.fGraphType.Length() == 0) ||
	     (opt->fConfig.fAutoConf && !opt->fConfig.fRespectUser))) {
            if (mpads != 0) SetDefaultGraphicsOptions (*opt);
            pads->ShowMultiPlot (pset[0], num[0], graph[0], 0, 0, kFALSE);
         }
         opt = pads->GetPlotOptions (1);
         if ((opt != 0) && 
            ((mpads != 0) || (opt->fTraces.fGraphType.Length() == 0) ||
	     (opt->fConfig.fAutoConf && !opt->fConfig.fRespectUser))) {
            if (mpads != 0) SetDefaultGraphicsOptions (*opt);
            TLGPad* p = pads->GetPad(1);
            if (p != 0) {
               p->Configure (0, 0);
            }
         }
      }
      // frequency series
      else if (meastype == 4) {
         OptionAll_t* opt = pads->GetPlotOptions (0);
         if ((opt != 0) && 
            ((mpads != 0) || (opt->fTraces.fGraphType.Length() == 0) ||
	     (opt->fConfig.fAutoConf && !opt->fConfig.fRespectUser))) {
            if (mpads != 0) SetDefaultGraphicsOptions (*opt);
            pads->ShowMultiPlot (pset[0], num[0], graph[0], 0, 0, kFALSE);
         }
         opt = pads->GetPlotOptions (1);
         if ((opt != 0) && 
            ((mpads != 0) || (opt->fTraces.fGraphType.Length() == 0) ||
	     (opt->fConfig.fAutoConf && !opt->fConfig.fRespectUser))) {
            if (mpads != 0) SetDefaultGraphicsOptions (*opt);
            pads->ShowMultiPlot (pset[1], num[1], graph[1], 1, 3, kFALSE);
         }
      }
      // histogram1 (mito)
      else if (meastype == 5) {
         OptionAll_t* opt = pads->GetPlotOptions (0);
         if ((opt != 0) && 
            ((mpads != 0) || (opt->fTraces.fGraphType.Length() == 0) ||
	     (opt->fConfig.fAutoConf && !opt->fConfig.fRespectUser))) {
            if (mpads != 0) SetDefaultGraphicsOptions (*opt);
            pads->ShowMultiPlot (pset[0], num[0], graph[0], 0, 0, kFALSE);
         }
      }
      // Update windows if necessary
      if (updatePads) {
         fPlot->Update();
      }
      return  kTRUE;
   }


//______________________________________________________________________________
   Bool_t TLGMainWindow::ClearResults (Bool_t askfirst, Bool_t all)
   {
      if (fPlot->Empty()) {
         return kTRUE;
      }
      if (askfirst) {
         Int_t ret;
         string msg = "This will clear all results\n"
            "from the diagnostics test.\n"
            "Do you want to continue?";
         new TGMsgBox (gClient->GetRoot(), this, "Clear results", 
                      msg.c_str(), kMBIconQuestion, 
                      kMBYes | kMBNo, &ret);
         if (ret != kMBYes) {
            return kFALSE;
         }
      }
      fPlot->Clear (all);
      fPlot->Update();
      if (fXMLObjects) delete fXMLObjects; fXMLObjects = 0;
      if (all && fCalTable) {
         fCalTable->Clear();
         fCalTable->ClearChannels();
      }
      if (all && fRefTraces) SetDefaultReferenceTraces (*fRefTraces);
      if (all && fMathTable) SetDefaultMathTable (*fMathTable);
      return kTRUE;
   }


//______________________________________________________________________________
   Bool_t TLGMainWindow::ShowPlots ()
   {
      // Get plot settings
      if (fSettingsRestoreFlag) {
         cout << "Restore plot settings" << endl;
         return SetPlotSettings ();
      }
      else {
         cout << "default plot settings" << endl;
         return ShowDefaultPlot (kFALSE);
      }
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      switch (GET_MSG (msg)) {
         case kC_COMMAND:
            {
               switch (GET_SUBMSG (msg)) {
                  case kCM_BUTTON:
                     {
                        return ProcessButton (parm1, parm2);
                     }
                  case kCM_MENU: 
                     {
                        return ProcessMenu (parm1, parm2);
                     }
               }
               break;
            }
      }
      return kTRUE;
   }

//______________________________________________________________________________
   void TLGMainWindow::SetStatusMsg (const char* msg)
   {
      string fStatusMsg = msg;
      fStatus->SetText (msg, 0);
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::ButtonClear ()
   {
      ClearResults();
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::ButtonUpdate ()
   {
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::ButtonRun ()
   {
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::ButtonStart ()
   {
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::ButtonPause ()
   {
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::ButtonResume ()
   {
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::ButtonAbort ()
   {
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::ButtonStop ()
   {
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::ButtonExit ()
   {
      CloseWindow();;
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::ProcessButton (Long_t parm1, Long_t parm2) 
   {
      //cout << "Process button " << parm1 << endl;
      switch (parm1) {
         case kB_CLEAR:
            {
               return ButtonClear();
            }
         case kB_UPDATE:
            {
               return ButtonUpdate();
            }
         case kB_RUN:
            {
               return ButtonRun();
            }
         case kB_START:
            {
               return ButtonStart();
            }
         case kB_PAUSE:
            {
               return ButtonPause();
            }
         case kB_RESUME:
            {
               return ButtonResume();
            }
         case kB_ABORT:
            {
               return ButtonAbort();
            }
         case kB_STOP:
            {
               return ButtonStop();
            }
         case kB_EXIT:
            {
               return ButtonExit();
            }
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::HandleTimer (TTimer* timer)
   {
      // quit, if X11 died
      if (timer == fX11Watchdog) {
         if (!gXDisplay) gApplication->Terminate(0);
         timer->Reset();
      }
      
      // heartbeat: check messages
      else if (timer == fHeartbeat) {
         // sometimes skip a heartbeat to avoid overloading the GUI
         if (fSkipHeartbeats) {
            fSkipHeartbeats--;
         }
         // else check notification message queue
         else {
            NotificationMessage* msg = 0;
            do {
               // pop message from queue
               fMsgQueueLock->lock();
               if (fMsgQueue.empty()) {
                  msg = 0;
               }
               else {
                  msg = fMsgQueue[0];
                  fMsgQueue.pop_front ();
               }
               fMsgQueueLock->unlock();
               // process it
               if (msg) {
                  fSkipHeartbeats = 3;
                  msg->Process (*this);
                  delete msg;
               }
            } while (msg);
         }
         timer->Reset();
      }
   
      // return
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGMainWindow::SendNotification (NotificationMessage* msg) 
   {
      if (msg == 0) {
         return kFALSE;
      }
      fMsgQueueLock->lock();
      fMsgQueue.push_back (msg);
      fMsgQueueLock->unlock();
      return kTRUE;
   }


}
