/* -*- mode: c++; c-basic-offset: 3; -*- */
/* version $Id: TLGSave.cc 6344 2010-11-10 06:19:42Z john.zweizig@LIGO.ORG $ */
#include "TLGSave.hh"
#include "DataDesc.hh"
#include "PlotSet.hh"
#include "Calibrations.hh"
#include "Xsil.hh"
#include "XsilStd.hh"
#include "TLGColor.hh"
#include "TLGMath.hh"
#include "TLGOptions.hh"
#include <iostream>
#include <fstream>
#include <string.h>
#include <strings.h>
#include <cstdio>
#include <cstdlib>

namespace ligogui {
   using namespace std;
   using namespace xml;


/*----------------------------------------------------------------------*/
/*  									*/
/*  Data parameters							*/
/*  									*/
/*----------------------------------------------------------------------*/
   const char stDataSubtype[] = "Subtype";


/*----------------------------------------------------------------------*/
/*  									*/
/*  Plot Settings							*/
/*  									*/
/*----------------------------------------------------------------------*/
   const char stPlot[] = "Plot";
   const char stPlotName[] = "Name";
   const char stPlotTracesGraphType[] = "TracesGraphType";
   const char stPlotTracesActive[] = "TracesActive";
   const char stPlotTracesAChannel[] = "TracesAChannel";
   const char stPlotTracesBChannel[] = "TracesBChannel";
   const char stPlotTracesPlotStyle[] = "TracesPlotStyle";
   const char stPlotTracesLineAttrColor[] = "TracesLineAttrColor";
   const char stPlotTracesLineAttrStyle[] = "TracesLineAttrStyle";
   const char stPlotTracesLineAttrWidth[] = "TracesLineAttrWidth";
   const char stPlotTracesMarkerAttrColor[] = "TracesMarkerAttrColor";
   const char stPlotTracesMarkerAttrStyle[] = "TracesMarkerAttrStyle";
   const char stPlotTracesMarkerAttrSize[] = "TracesMarkerAttrSize";
   const char stPlotTracesBarAttrColor[] = "TracesBarAttrColor";
   const char stPlotTracesBarAttrStyle[] = "TracesBarAttrStyle";
   const char stPlotTracesBarAttrWidth[] = "TracesBarAttrWidth";
   const char stPlotRangeAxisScale[] = "RangeAxisScale";
   const char stPlotRangeRange[] = "RangeRange";
   const char stPlotRangeRangeFrom[] = "RangeRangeFrom";
   const char stPlotRangeRangeTo[] = "RangeRangeTo";
   const char stPlotRangeBin[] = "RangeBin";
   const char stPlotRangeBinLogSpacing[] = "RangeBinLogSpacing";
   const char stPlotUnitsXValues[] = "UnitsXValues";
   const char stPlotUnitsYValues[] = "UnitsYValues";
   const char stPlotUnitsXUnit[] = "UnitsXUnit";
   const char stPlotUnitsYUnit[] = "UnitsYUnit";
   const char stPlotUnitsXMag[] = "UnitsXMag";
   const char stPlotUnitsYMag[] = "UnitsYMag";
   const char stPlotUnitsXSlope[] = "UnitsXSlope";
   const char stPlotUnitsYSlope[] = "UnitsYSlope";
   const char stPlotUnitsXOffset[] = "UnitsXOffset";
   const char stPlotUnitsYOffset[] = "UnitsYOffset";
   const char stPlotCursorActive[] = "CursorActive";
   const char stPlotCursorTrace[] = "CursorTrace";
   const char stPlotCursorStyle[] = "CursorStyle";
   const char stPlotCursorType[] = "CursorType";
   const char stPlotCursorX[] = "CursorX";
   const char stPlotCursorH[] = "CursorH";
   const char stPlotCursorValid[] = "CursorValid";
   const char stPlotCursorY[] = "CursorY";
   const char stPlotCursorN[] = "CursorN";
   const char stPlotCursorXDiff[] = "CursorXDiff";
   const char stPlotCursorYDiff[] = "CursorYDiff";
   const char stPlotCursorMean[] = "CursorMean";
   const char stPlotCursorRMS[] = "CursorRMS";
   const char stPlotCursorStdDev[] = "CursorStdDev";
   const char stPlotCursorSum[] = "CursorSum";
   const char stPlotCursorSqrSum[] = "CursorSqrSum";
   const char stPlotCursorArea[] = "CursorArea";
   const char stPlotCursorRMSArea[] = "CursorRMSArea";
   const char stPlotCursorPeakX[] = "CursorPeakX";
   const char stPlotCursorPeakY[] = "CursorPeakY";
   const char stPlotCursorCenter[] = "CursorCenter"; // hist mito
   const char stPlotCursorWidth[] = "CursorWidth"; // hist mito
   const char stPlotConfigAutoConfig[] = "ConfigAutoConfig";
   const char stPlotConfigRespectUser[] = "ConfigRespectUser";
   const char stPlotConfigAutoAxes[] = "ConfigAutoAxes";
   const char stPlotConfigAutoBin[] = "ConfigAutoBin";
   const char stPlotConfigAutoTimeAdjust[] = "ConfigAutoTimeAdjust";
   const char stPlotStyleTitle[] = "StyleTitle";
   const char stPlotStyleTitleAlign[] = "StyleTitleAlign";
   const char stPlotStyleTitleAngle[] = "StyleTitleAngle";
   const char stPlotStyleTitleColor[] = "StyleTitleColor";
   const char stPlotStyleTitleFont[] = "StyleTitleFont";
   const char stPlotStyleTitleSize[] = "StyleTitleSize";
   const char stPlotStyleMargin[] = "StyleMargin";
   const char stPlotAxisXTitle[] = "AxisXTitle";
   const char stPlotAxisXAxisAttrAxisColor[] = "AxisXAxisAttrAxisColor";
   const char stPlotAxisXAxisAttrLabelColor[] = "AxisXAxisAttrLabelColor";
   const char stPlotAxisXAxisAttrLabelFont[] = "AxisXAxisAttrLabelFont";
   const char stPlotAxisXAxisAttrLabelOffset[] = "AxisXAxisAttrLabelOffset";
   const char stPlotAxisXAxisAttrLabelSize[] = "AxisXAxisAttrLabelSize";
   const char stPlotAxisXAxisAttrNdividions[] = "AxisXAxisAttrNdividions";
   const char stPlotAxisXAxisAttrTickLength[] = "AxisXAxisAttrTickLength";
   const char stPlotAxisXAxisAttrTitleOffset[] = "AxisXAxisAttrTitleOffset";
   const char stPlotAxisXAxisAttrTitleSize[] = "AxisXAxisAttrTitleSize";
   const char stPlotAxisXAxisAttrTitleColor[] = "AxisXAxisAttrTitleColor";
   const char stPlotAxisXGrid[] = "AxisXGrid";
   const char stPlotAxisXBothSides[] = "AxisXBothSides";
   const char stPlotAxisXCenterTitle[] = "AxisXCenterTitle";
   const char stPlotAxisYTitle[] = "AxisYTitle";
   const char stPlotAxisYAxisAttrAxisColor[] = "AxisYAxisAttrAxisColor";
   const char stPlotAxisYAxisAttrLabelColor[] = "AxisYAxisAttrLabelColor";
   const char stPlotAxisYAxisAttrLabelFont[] = "AxisYAxisAttrLabelFont";
   const char stPlotAxisYAxisAttrLabelOffset[] = "AxisYAxisAttrLabelOffset";
   const char stPlotAxisYAxisAttrLabelSize[] = "AxisYAxisAttrLabelSize";
   const char stPlotAxisYAxisAttrNdividions[] = "AxisYAxisAttrNdividions";
   const char stPlotAxisYAxisAttrTickLength[] = "AxisYAxisAttrTickLength";
   const char stPlotAxisYAxisAttrTitleOffset[] = "AxisYAxisAttrTitleOffset";
   const char stPlotAxisYAxisAttrTitleSize[] = "AxisYAxisAttrTitleSize";
   const char stPlotAxisYAxisAttrTitleColor[] = "AxisYAxisAttrTitleColor";
   const char stPlotAxisYGrid[] = "AxisYGrid";
   const char stPlotAxisYBothSides[] = "AxisYBothSides";
   const char stPlotAxisYCenterTitle[] = "AxisYCenterTitle";
   const char stPlotLegendShow[] = "LegendShow";
   const char stPlotLegendPlacement[] = "LegendPlacement";
   const char stPlotLegendXAdjust[] = "LegendXAdjust";
   const char stPlotLegendYAdjust[] = "LegendYAdjust";
   const char stPlotLegendSymbolStyle[] = "LegendSymbolStyle";
   const char stPlotLegendTextStyle[] = "LegendTextStyle";
   const char stPlotLegendSize[] = "LegendSize";
   const char stPlotLegendText[] = "LegendText";
   const char stPlotParamShow[] = "ParamShow";
   const char stPlotParamT0[] = "ParamT0";
   const char stPlotParamAvg[] = "ParamAvg";
   const char stPlotParamSpecial[] = "ParamSpecial";
   const char stPlotParamStat[] = "ParamStat"; // hist mito
   const char stPlotParamUOBins[] = "ParamUOBins"; // hist mito 8/8/2002
   const char stPlotParamTimeFormatUTC[] = "ParamTimeFormatUTC";
   const char stPlotParamTextSize[] = "ParamTextSize";


//______________________________________________________________________________
   ostream& operator<< (ostream& os, const xsilHandler::attrlist& attr) 
   {
      for (xsilHandler::attrlist::const_iterator i = attr.begin();
          i != attr.end(); i++) {
         //if (i != attr.begin()) os << " ";
         os << " " << i->first << "=\"" << i->second << "\"";
      }
      return os;
   }

//______________________________________________________________________________
   bool convert_precision (void*& data, int N, bool& is_double, bool to_double)
   {
      if (!data || (N <= 0) ||
         (is_double && to_double) || (!is_double && !to_double)) {
         return true;
      }
      if (to_double) {
         double* y = new (nothrow) double [N];
         if (!y) {
            return false;
         }
         float* x = (float*)data;
         for (int i = 0; i < N; ++i) {
            y[i] = x[i];
         }
         delete [] (char*) data;
         data = y;
      }
      else {
         float* y = new (nothrow) float [N];
         if (!y) {
            return false;
         }
         double* x = (double*)data;
         for (int i = 0; i < N; ++i) {
            y[i] = x[i];
         }
         delete [] (char*) data;
         data = y;
      }
      is_double = to_double;
      return true;
   }

#if 0
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// xsilHandlerUnknown                                                   //
//                                                                      //
// Handles unknown records                                              //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   xsilHandlerUnknown::xsilHandlerUnknown (ostream& os, 
                     const attrlist* attr, bool ignore)
   : xsilHandler (ignore), fOs (&os), fTrailer (attr != 0),
   fComplex (false), fDouble (false), fData (0)           
   {
      fDim[0] = 0; fDim[1] = 0; fDim[2] = 0; fDim[3] = 0;
      if (fTrailer) {
         *fOs << xsilIndent (1) << xsilTagBegin (xmlContainer) <<
            (*attr) << xmlTagClosing << endl;
      }
   }

//______________________________________________________________________________
   xsilHandlerUnknown::~xsilHandlerUnknown()
   {
      if (fData == 0) {
         xsilDataEnd<float> ().write (*fOs, fTrailer);
      }
      else if (fComplex) {
         if (fDouble) {
            xsilDataEnd<complex<double> > x
               (fDim[0], fDim[1], fDim[2], fDim[3], 
               (complex<double>*)fData);
            x.write (*fOs, fTrailer);
         }
         else {
            xsilDataEnd<complex<float> > x
               (fDim[0], fDim[1], fDim[2], fDim[3], 
               (complex<float>*)fData);
            x.write (*fOs, fTrailer);
         }
      }
      else {
         if (fDouble) {
            xsilDataEnd<double> x (fDim[0], fDim[1], fDim[2], fDim[3], 
                                 (double*)fData);
            x.write (*fOs, fTrailer);
         }
         else {
            xsilDataEnd<float> x (fDim[0], fDim[1], fDim[2], fDim[3], 
                                 (float*)fData);
            x.write (*fOs, fTrailer);
         }
      }
      *fOs << endl;
      if (fData) delete [] (char*) fData;
   }

//______________________________________________________________________________
   bool xsilHandlerUnknown::HandleParameter (const std::string& name,
                     const attrlist& attr, const bool& p, int N)
   {
      attrlist::const_iterator ui = attr.find (xmlUnit);
      const char* u = (ui == attr.end()) ? 0 : ui->second.c_str();
      *fOs << xsilParameter<bool> (name.c_str(), u, p, N) << endl;
      return true;
   }

//______________________________________________________________________________
   bool xsilHandlerUnknown::HandleParameter (const std::string& name,
                     const attrlist& attr, const char& p, int N)
   {
      attrlist::const_iterator ui = attr.find (xmlUnit);
      const char* u = (ui == attr.end()) ? 0 : ui->second.c_str();
      *fOs << xsilParameter<char> (name.c_str(), u, p, N) << endl;
      return true;
   }

//______________________________________________________________________________
   bool xsilHandlerUnknown::HandleParameter (const std::string& name,
                     const attrlist& attr, const short& p, int N)
   {
      attrlist::const_iterator ui = attr.find (xmlUnit);
      const char* u = (ui == attr.end()) ? 0 : ui->second.c_str();
      *fOs << xsilParameter<short> (name.c_str(), u, p, N) << endl;
      return true;
   }

//______________________________________________________________________________
   bool xsilHandlerUnknown::HandleParameter (const std::string& name,
                     const attrlist& attr, const int& p, int N)
   {
      attrlist::const_iterator ui = attr.find (xmlUnit);
      const char* u = (ui == attr.end()) ? 0 : ui->second.c_str();
      *fOs << xsilParameter<int> (name.c_str(), u, p, N) << endl;
      return true;
   }

//______________________________________________________________________________
   bool xsilHandlerUnknown::HandleParameter (const std::string& name,
                     const attrlist& attr, const long long& p, int N)
   {
      attrlist::const_iterator ui = attr.find (xmlUnit);
      const char* u = (ui == attr.end()) ? 0 : ui->second.c_str();
      *fOs << xsilParameter<long long> (name.c_str(), u, p, N) << endl;
      return true;
   }

//______________________________________________________________________________
   bool xsilHandlerUnknown::HandleParameter (const std::string& name,
                     const attrlist& attr, const float& p, int N)
   {
      attrlist::const_iterator ui = attr.find (xmlUnit);
      const char* u = (ui == attr.end()) ? 0 : ui->second.c_str();
      *fOs << xsilParameter<float> (name.c_str(), u, p, N) << endl;
      return true;
   }

//______________________________________________________________________________
   bool xsilHandlerUnknown::HandleParameter (const std::string& name,
                     const attrlist& attr, const double& p, int N)
   {
      attrlist::const_iterator ui = attr.find (xmlUnit);
      const char* u = (ui == attr.end()) ? 0 : ui->second.c_str();
      *fOs << xsilParameter<double> (name.c_str(), u, p, N) << endl;
      return true;
   }

//______________________________________________________________________________
   bool xsilHandlerUnknown::HandleParameter (const std::string& name,
                     const attrlist& attr, const complex<float>& p, 
                     int N)
   {
      attrlist::const_iterator ui = attr.find (xmlUnit);
      const char* u = (ui == attr.end()) ? 0 : ui->second.c_str();
      *fOs << xsilParameter<complex<float> > (name.c_str(), u, p, N) << endl;
      return true;
   }

//______________________________________________________________________________
   bool xsilHandlerUnknown::HandleParameter (const std::string& name,
                     const attrlist& attr, const complex<double>& p, 
                     int N)
   {
      attrlist::const_iterator ui = attr.find (xmlUnit);
      const char* u = (ui == attr.end()) ? 0 : ui->second.c_str();
      *fOs << xsilParameter<complex<double> > (name.c_str(), u, p, N) << endl;
      return true;
   }

//______________________________________________________________________________
   bool xsilHandlerUnknown::HandleParameter (const std::string& name,
                     const attrlist& attr, const string& p)
   {
      attrlist::const_iterator ui = attr.find (xmlUnit);
      const char* u = (ui == attr.end()) ? 0 : ui->second.c_str();
      *fOs << xsilParameter<const char*> (name.c_str(), u, p.c_str()) << endl;
      return true;
   }

//______________________________________________________________________________
   bool xsilHandlerUnknown::HandleTime (const std::string& name,
                     const attrlist& attr,
                     unsigned long sec, unsigned long nsec)
   {
      *fOs << xsilTime (name.c_str(), sec, nsec) << endl;
      return true;
   }

//______________________________________________________________________________
   bool xsilHandlerUnknown::HandleData (const std::string& name,
                     float* x, int dim1, int dim2, int dim3, int dim4) 
   {
      if (!name.empty()) {
         xsilArray<float> (name.c_str(), dim1, dim2, dim3, 
                          dim4, x).write (*fOs, fTrailer);
         *fOs << endl;
         return false;
      }
      else {
         if (fData) delete [] (char*) fData;
         fDim[0] = dim1;
         fDim[1] = dim2;
         fDim[2] = dim3;
         fDim[3] = dim4;
         fData = x;
         fDouble = false;
         fComplex = false;
         return true; 
      }
   }

//______________________________________________________________________________
   bool xsilHandlerUnknown::HandleData (const std::string& name,
                     std::complex<float>* x, 
                     int dim1, int dim2, int dim3, int dim4) 
   {
      if (!name.empty()) {
         xsilArray<complex<float> > (name.c_str(), dim1, dim2, dim3, 
                              dim4, x).write (*fOs, fTrailer);
         *fOs << endl;
         return false;
      }
      else {
         if (fData) delete [] (char*) fData;
         fDim[0] = dim1;
         fDim[1] = dim2;
         fDim[2] = dim3;
         fDim[3] = dim4;
         fData = (float*)x;
         fDouble = false;
         fComplex = true;
         return true; 
      }
   }

//______________________________________________________________________________
   bool xsilHandlerUnknown::HandleData (const std::string& name,
                     double* x, int dim1, int dim2, int dim3, int dim4) 
   {
      if (!name.empty()) {
         xsilArray<double> (name.c_str(), dim1, dim2, dim3, 
                           dim4, x).write (*fOs, fTrailer);
         *fOs << endl;
         return false;
      }
      else {
         if (fData) delete [] (char*) fData;
         fDim[0] = dim1;
         fDim[1] = dim2;
         fDim[2] = dim3;
         fDim[3] = dim4;
         fData = x;
         fDouble = true;
         fComplex = false;
         return true;
      }
   }

//______________________________________________________________________________
   bool xsilHandlerUnknown::HandleData (const std::string& name,
                     std::complex<double>* x, 
                     int dim1, int dim2, int dim3, int dim4) 
   {
      if (!name.empty()) {
         xsilArray<complex<double> > (name.c_str(), dim1, dim2, dim3, 
                              dim4, x).write (*fOs, fTrailer);
         *fOs << endl;
         return false;
      }
      else {
         if (fData) delete [] (char*) fData;
         fDim[0] = dim1;
         fDim[1] = dim2;
         fDim[2] = dim3;
         fDim[3] = dim4;
         fData = (float*)x;
         fDouble = true;
         fComplex = true;
         return true; 
      }
   }

//______________________________________________________________________________
   bool xsilHandlerUnknown::ConvertPrecision (bool to_double)
   {
      int N = 1;
      for (int i = 0; i < 4; ++i) {
         if (fDim[i] > 0) N *= fDim[i];
      }
      if (fComplex) N *= 2;
      return convert_precision (fData, N, fDouble, to_double);
   }
#endif



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// xsilHandlerUnknown                                                   //
//                                                                      //
// Handles unknown records                                              //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class xsilHandlerLdasData : public xsilHandler {
   protected:
      xsilHandlerData&	fParent;
   
   public:
      /// Constructor
      xsilHandlerLdasData (xsilHandlerData& parent)
      : fParent (parent){
      }
      /// data callback (calls parent)
      virtual bool DataHandler (const std::string& name,
                        int type, void* x, int size, 
                        int dim1, int dim2 = 0, int dim3 = 0,
                        int dim4 = 0) {
         return fParent.DataHandler (name, type, x, size, 
                              dim1, dim2, dim3, dim4); }
   };



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// xsilHandlerData                                                      //
//                                                                      //
// Handles plot data objects                                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   xsilHandlerData::xsilHandlerData (const string& name,
                     DataType dtype, PlotSet& pset, 
                     calibration::Table* caltable,
                     ReferenceTrace_t* ref, int reftrace)
   : xsilHandlerUnknown (fOsPrm), fName (name), fType (dtype), 
   fSubtype (-1), fStep (-1), fSec (0), fNsec (0), fX0 (0), fDx (0), 
   fAvrg (1), fBW (0), fBins (0), fErrors (0), fNBinx(0), fNData(0), 
   fStats(0), fPlot (&pset), fCal (caltable), fRef (ref), 
   fRefTrace (reftrace)
   {
      //cout << "Data handler for " << name << endl;
      // LDAS: get channel name and time from container name
      if (fType == kTimeSeries) {
         int count = 0;
	 for (const char* p = name.c_str(); *p; ++p) {
	    if (*p == '[') ++count;
	 }
         if (count == 2) {
	    fAChn = name;
	 }
      }
      // LDAS: get channel name and time from container name
      else if (fType == kLdasTimeSeries) {
         string n = fName;
         for (int i = 0; i < 5; ++i) {
            bool esc = false;
	    unsigned int pos;
            for (pos = 0; (pos < n.length()) &&
                ((n[pos] != ':') || esc); ++pos) {
               esc = (n[pos] == '\\');
            }
            switch (i) {
               // channel name
               case 0:
                  {
                     fAChn = n.substr (0, pos);
                     string::size_type p;
                     while ((p = fAChn.find ("\\:")) != string::npos) {
                        fAChn.erase (p, 1);
                     }
                     break;
                  }
               // GPS sec
               case 3:
                  {
                     fSec =  atol (n.substr (0, pos).c_str());
                     break;
                  }
               // GPS sec
               case 4:
                  {
                     fNsec =  atol (n.substr (0, pos).c_str());
                     break;
                  }
               // garbage
               default:
                  {
                     break;
                  }
            }
            n.erase (0, pos);
            if (!n.empty()) n.erase (0, 1);
         }
      }
   }

//______________________________________________________________________________
   static const char* const tsext[] = 
   {"", "(STD.DEV.)", "(MIN)", "(MAX)", "(RMS)"};

//______________________________________________________________________________
   string stepName (const string& n, int step, int ref = -1) 
   {
      if (step < 0) {
         if (ref < 0) {
            return n;
         }
         else {
            char buf[1024];
            sprintf (buf, "%s(REF%i)", n.c_str(), ref);   
            return string (buf); 
         }
      }
      else {
         if (ref < 0) {
            char buf[1024];
            sprintf (buf, "%s[%i]", n.c_str(), step);
            return string (buf);
         }
         else {
            char buf[1024];
            sprintf (buf, "%s[%i](REF%i)", n.c_str(), step, ref);
            return string (buf);
         }
      }
   }

//______________________________________________________________________________
   xsilHandlerData::~xsilHandlerData()
   {
      if (fData == 0) {
         return;
      }
   
      // Guess subtype if not specified
      if (fSubtype < 0) {
         switch (fType) {
            case kTimeSeries:
            case kLdasTimeSeries:
               fSubtype = fComplex ? 1 : 0;
               break;
            case kSpectrum:
               fSubtype = fComplex ? 1 : 0;
               break;
            case kTransferFunction:
               fSubtype = fComplex ? 3 : 5;
               break;
            case kCoefficients:
               fSubtype = fComplex ? 4 : 7;
               break;
            case kHistogram:
               fSubtype = 0;
         }
      }
      if (fSubtype < 0) {
         return;
      }
   
      // check dimensions
      if (fDim[1] == 0) {
         fDim[1] = fDim[0];
         fDim[0] = 1;
      }
      // check A channel 
      if (fAChn.empty()) {
         fAChn = fName;
         fStep = -1;
      }
   
      // add data to plot set
      DataRefCount* dr = 0;
      switch (fType) {
      
         // add time series
         case kTimeSeries:
         case kLdasTimeSeries:
            if (fDouble) {
               if (!ConvertPrecision (false)) {
                  return;
               }
            }
            switch (fSubtype) {
               case 0:
                  if (!fComplex && (fDx != 0) && (fDim[0] == 1)) {
                     dr = new (nothrow) DataRefCount 
                        (fX0, fDx, (float*)fData, fDim[1]);
                  }
                  break;
               case 1:
                  if (fComplex && (fDx != 0) && (fDim[0] == 1)) {
                     dr = new (nothrow) DataRefCount 
                        (fX0, fDx, (float*)fData, fDim[1], 1, kTRUE);
                  }
                  break;
               case 2:
                  if (!fComplex && (fDx != 0) && (fDim[0] == 1)) {
                     dr = new (nothrow) DataRefCount 
                        (fX0, fDx, (float*)fData, fDim[1]);
                  }
                  break;
               case 3:
                  if (!fComplex && (fDx != 0) && (fDim[0] == 5)) {
                     dr = new (nothrow) DataRefCount 
                        (fX0, fDx, (float*)fData, fDim[1], fDim[0]);
                  }
                  break;
               case 4:
                  if (!fComplex && (fDim[0] == 2)) {
                     dr = new (nothrow) DataRefCount (
                                          (float*)fData, fDim[1]);
                  }
                  break;
               case 5:
                  if (fComplex && (fDim[0] == 2)) {
                     dr = new (nothrow) DataRefCount 
                        ((float*)fData, fDim[1], 1, kTRUE);
                  }
                  break;
               case 6:
                  if (!fComplex && (fDim[0] == 2)) {
                     dr = new (nothrow) DataRefCount (
                                          (float*)fData, fDim[1]);
                  }
                  break;
               case 7:
                  if (!fComplex && (fDim[0] == 6)) {
                     dr = new (nothrow) DataRefCount 
                        ((float*)fData, fDim[1], fDim[0] - 1);
                  }
                  break;
            }
            if (dr != 0) {
               fData = 0;
               ParameterDescriptor prm;
               prm.SetStartTime (fSec, fNsec);
               if (fAvrg > 1) prm.SetAverages (fAvrg);
               prm.SetUser (fOsPrm.str().c_str());
               //cout << "M = " << dr->GetM() << endl;
               for (int i = 0; i < dr->GetM(); i++) {
                  DataDescriptor* dat = new (nothrow) DataRef (*dr, i);
                  if (dat == 0) 
                     break;
                  string n = stepName (fAChn, fStep, -1) + tsext[i];
                  n = stepName (n, -1, fRefTrace);
                  calibration::Descriptor cal;
                  if (fCal) {
                     cal.SetValid();
                     cal.SetTime (Time (fSec, fNsec));
                     cal.SetChannel (0, n.c_str());
                     cal.SetDomain (kCalUnitX, kCalDomainTime);
                  }
                  PlotDescriptor* pd = new (nothrow) PlotDescriptor
                     (dat, kPTTimeSeries, n.c_str(), 0, &prm, &cal);
                  if (pd == 0) {
                     delete dat;
                     break;
                  }
                  fPlot->Add (pd);
                  // fill reference trace
                  if (fRef) {
                     fRef->fModified = kRefTraceModNo;
                     fRef->fGraph = pd->GetGraphType();
                     fRef->fAChn = 
                        (stepName (fAChn, fStep, -1) + tsext[i]).c_str();
                     fRef->fBChn = "";
                     fRef->fValid = kTRUE;
                  }
               }
            }
            break;
      
         // add spectrum
         case kSpectrum:
            if (fDouble) {
               if (!ConvertPrecision (false)) {
                  return;
               }
            }
            switch (fSubtype) {
               case 0:
                  if (fComplex && (fDx != 0) && (fDim[0] == 1)) {
                     dr = new (nothrow) DataRefCount 
                        (fX0, fDx, (float*)fData, fDim[1], 1, kTRUE);
                  }
                  break;
               case 1:
                  if (!fComplex && (fDx != 0) && (fDim[0] == 1)) {
                     dr = new (nothrow) DataRefCount 
                        (fX0, fDx, (float*)fData, fDim[1]);
                  }
                  break;
               case 2:
                  if (fComplex && (fDx != 0) && (fDim[0] > 0)) {
                     dr = new (nothrow) DataRefCount 
                        (fX0, fDx, (float*)fData, fDim[1], fDim[0], kTRUE);
                  }
                  break;
               case 3:
                  if (!fComplex && (fDx != 0) && (fDim[0] > 0)) {
                     dr = new (nothrow) DataRefCount 
                        (fX0, fDx, (float*)fData, fDim[1], fDim[0]);
                  }
                  break;
               case 4:
                  if (fComplex && (fDim[0] == 2)) {
                     dr = new (nothrow) DataRefCount 
                        ((float*)fData, fDim[1], 1, kTRUE);
                  }
                  break;
               case 5:
                  if (!fComplex && (fDim[0] == 2)) {
                     dr = new (nothrow) DataRefCount (
                                          (float*)fData, fDim[1]);
                  }
                  break;
               case 6:
                  if (fComplex && (fDim[0] > 1)) {
                     dr = new (nothrow) DataRefCount 
                        ((float*)fData, fDim[1], fDim[0] - 1, kTRUE);
                  }
                  break;
               case 7:
                  if (!fComplex && (fDim[0] > 1)) {
                     dr = new (nothrow) DataRefCount 
                        ((float*)fData, fDim[1], fDim[0] - 1);
                  }
                  break;
            }
            if (dr != 0) {
               fData = 0; // don't delete data!
               ParameterDescriptor prm;
               prm.SetStartTime (fSec, fNsec);
               if (fAvrg > 1) prm.SetAverages (fAvrg);
               char buf[256];
               sprintf (buf, "BW=%g", fBW);
               prm.SetThird (buf);
               prm.SetUser (fOsPrm.str().c_str());
               // frequency series
               if ((fSubtype == 0) || (fSubtype == 4)) {
                  DataDescriptor* dat = new (nothrow) DataRef (*dr);
                  calibration::Descriptor cal;
                  if (fCal) {
                     cal.SetValid();
                     cal.SetTime (Time (fSec, fNsec));
                     cout << fAChn.c_str() << endl;
                     cal.SetChannel (0, fAChn.c_str());
                     cal.SetDomain (kCalUnitX, kCalDomainFrequency);
                     cal.SetBW (kCalUnitY, fBW);
                     cal.SetDensity (kCalUnitY, 
                                    kCalDensityAmpPerRtHz);
                  }
                  PlotDescriptor* pd = new (nothrow) PlotDescriptor
                     (dat, kPTFrequencySeries, 
                     stepName (fAChn, fStep, fRefTrace).c_str(), 0, 
                     &prm, &cal);
                  if ((pd == 0) || (dat == 0)) {
                     if (dat) delete dat;
                     if (pd) delete pd;
                     dat = 0;
                  }
                  else {
                     fPlot->Add (pd);
                     // fill reference trace
                     if (fRef) {
                        fRef->fModified = kRefTraceModNo;
                        fRef->fGraph = pd->GetGraphType();
                        fRef->fAChn = stepName (fAChn, fStep).c_str();
                        fRef->fBChn = "";
                        fRef->fValid = kTRUE;
                     }
                  }
               }
               // power spectrum
               else if ((fSubtype == 1) || (fSubtype == 5)) {
                  DataDescriptor* dat = new (nothrow) DataRef (*dr);
                  cout << "Is XY ? " << (dat->IsXY() ? "yes" : "no") << endl;
                  calibration::Descriptor cal;
                  if (fCal) {
                     cal.SetValid();
                     cal.SetTime (Time (fSec, fNsec));
                     cal.SetChannel (0, fAChn.c_str());
                     cal.SetDomain (kCalUnitX, kCalDomainFrequency);
                     cal.SetBW (kCalUnitY, fBW);
                     cal.SetDensity (kCalUnitY, 
                                    kCalDensityAmpPerRtHz);
                  }
                  PlotDescriptor* pd = new (nothrow) PlotDescriptor
                     (dat, kPTPowerSpectrum, 
                     stepName (fAChn, fStep, fRefTrace).c_str(), 0, 
                     &prm, &cal);
                  if ((pd == 0) || (dat == 0)) {
                     if (dat) delete dat;
                     if (pd) delete pd;
                     dat = 0;
                  }
                  else {
                     fPlot->Add (pd);
                     // fill reference trace
                     if (fRef) {
                        fRef->fModified = kRefTraceModNo;
                        fRef->fGraph = pd->GetGraphType();
                        fRef->fAChn = stepName (fAChn, fStep).c_str();
                        fRef->fBChn = "";
                        fRef->fValid = kTRUE;
                     }
                  }
                  // add rms
                  if (fRefTrace < 0) {
                     BasicDataDescriptor* dat2 = 0;
                     if (dat != 0) {
                        dat2 = new (nothrow) FreqRMSDataDescriptor (dat);
                     }
                     if (fCal) {
                     // trick to display both in the same plot w/ same units
                        cal.SetBW (kCalUnitY, 1.0);
                     }
                     string n = stepName (fAChn, fStep) + "(RMS)";
                     pd = new (nothrow) PlotDescriptor
                        (dat2, kPTPowerSpectrum, n.c_str(), 0, &prm, &cal);
                     if ((pd == 0) || (dat2 == 0)) {
                        if (dat2) delete dat2;
                        if (pd) delete pd;
                     }
                     else {
                        fPlot->Add (pd);
                     }
                  }
               }
               // cross power spectrum
               else if ((fSubtype == 2) || (fSubtype == 6)) {
	          int m = 0;
                  for (int i = 0; i < dr->GetM() + 1; i++) {
                     if (((int)fBChn.size() <= i) || fBChn[i].empty()) {
                        continue;
                     }
                     DataDescriptor* dat = new (nothrow) DataRef (*dr, m++);
                     if (dat == 0) {
                        continue;
                     }
                     calibration::Descriptor cal;
                     if (fCal) {
                        cal.SetValid();
                        cal.SetTime (Time (fSec, fNsec));
                        cal.SetRelation (kCalChnRelMul);
                        cal.SetChannel (0, fAChn.c_str());
                        cal.SetChannel (1, fBChn[i].c_str());
                        cal.SetDomain (kCalUnitX, kCalDomainFrequency);
                        cal.SetBW (kCalUnitY, fBW);
                        cal.SetExpo (0, 1);
                        cal.SetConj (1, kTRUE);
                        cal.SetDensity (kCalUnitY, 
                                       kCalDensityPwrPerHz);
                     }
                     PlotDescriptor* pd = new (nothrow) PlotDescriptor
                        (dat, kPTCrossCorrelation, 
                        stepName (fAChn, fStep, fRefTrace).c_str(), 
                        stepName (fBChn[i], fStep, fRefTrace).c_str(), 
                        &prm, &cal);
                     if (pd == 0) {
                        delete dat;
                        break;
                     }
                     else {
                        fPlot->Add (pd);
                        // fill reference trace
                        if (fRef && (i == 0)) {
                           fRef->fModified = kRefTraceModNo;
                           fRef->fGraph = pd->GetGraphType();
                           fRef->fAChn = stepName (fAChn, fStep).c_str();
                           fRef->fBChn = stepName (fBChn[0], fStep).c_str();
                           fRef->fValid = kTRUE;
                        }
                     }
                  }
               }
               // coherence spectrum
               else if ((fSubtype == 3) || (fSubtype == 7)) {
	          int m = 0;
                  for (int i = 0; i < dr->GetM() + 1; i++) {
                     if (((int)fBChn.size() <= i) || fBChn[i].empty()) {
                        continue;
                     }
                     DataDescriptor* dat = new (nothrow) DataRef (*dr, m++);
                     if (dat == 0) {
                        continue;
                     }
                     calibration::Descriptor cal;
                     if (fCal) {
                        cal.SetValid();
                        cal.SetTime (Time (fSec, fNsec));
                        cal.SetRelation (kCalChnRelMul);
                        cal.SetChannel (0, fAChn.c_str());
                        cal.SetChannel (1, fBChn[i].c_str());
                        cal.SetDomain (kCalUnitX, kCalDomainFrequency);
                        cal.SetBW (kCalUnitY, fBW);
                        cal.SetExpo (0, 0);
                        cal.SetExpo (1, 0);
                     }
                     PlotDescriptor* pd = new (nothrow) PlotDescriptor
                        (dat, kPTCoherence, 
                        stepName (fAChn, fStep, fRefTrace).c_str(), 
                        stepName (fBChn[i], fStep, fRefTrace).c_str(), 
                        &prm, &cal);
                     if (pd == 0) {
                        delete dat;
                        break;
                     }
                     else {
                        fPlot->Add (pd);
                        // fill reference trace
                        if (fRef && (i == 0)) {
                           fRef->fModified = kRefTraceModNo;
                           fRef->fGraph = pd->GetGraphType();
                           fRef->fAChn = stepName (fAChn, fStep).c_str();
                           fRef->fBChn = stepName (fBChn[0], fStep).c_str();
                           fRef->fValid = kTRUE;
                        }
                     }
                  }
               }
            }
            break;
      
         // add transfer function
         case kTransferFunction:
            if (fDouble) {
               if (!ConvertPrecision (false)) {
                  return;
               }
            }
            switch (fSubtype) {
               case 0:
                  if (fComplex && (fDx != 0) && (fDim[0] > 0)) {
                     dr = new (nothrow) DataRefCount 
                        (fX0, fDx, (float*)fData, fDim[1], fDim[0], kTRUE);
                  }
                  break;
               case 1:
                  if (fComplex && (fDx != 0) && (fDim[0] > 0)) {
                     dr = new (nothrow) DataRefCount 
                        (fX0, fDx, (float*)fData, fDim[1], fDim[0], kTRUE);
                  }
                  break;
               case 2:
                  if (!fComplex && (fDx != 0) && (fDim[0] > 0)) {
                     dr = new (nothrow) DataRefCount 
                        (fX0, fDx, (float*)fData, fDim[1], fDim[0]);
                  }
                  break;
               case 3:
                  if (fComplex && (fDim[0] > 1)) {
                     dr = new (nothrow) DataRefCount 
                        ((float*)fData, fDim[1], fDim[0] - 1, kTRUE);
                  }
                  break;
               case 4:
                  if (fComplex && (fDim[0] > 1)) {
                     dr = new (nothrow) DataRefCount 
                        ((float*)fData, fDim[1], fDim[0] - 1, kTRUE);
                  }
                  break;
               case 5:
                  if (!fComplex && (fDim[0] > 1)) {
                     dr = new (nothrow) DataRefCount 
                        ((float*)fData, fDim[1], fDim[0] - 1);
                  }
                  break;
            }
            if (dr != 0) {
               fData = 0;
               ParameterDescriptor prm;
               prm.SetStartTime (fSec, fNsec);
               if (fAvrg > 1) prm.SetAverages (fAvrg);
               prm.SetUser (fOsPrm.str().c_str());
               for (int i = 0; i < dr->GetM() + 1; i++) {
                  if (((int)fBChn.size() <= i) || fBChn[i].empty()) {
                     continue;
                  }
                  DataDescriptor* dat = new (nothrow) DataRef (*dr, i);
                  if (dat == 0) {
                     continue;
                  }
                  calibration::Descriptor cal;
                  if (fCal) {
                     cal.SetValid();
                     cal.SetTime (Time (fSec, fNsec));
                     cal.SetDomain (kCalUnitX, kCalDomainFrequency);
                     cal.SetRelation (kCalChnRelMul);
                     cal.SetChannel (0, fAChn.c_str());
                     cal.SetChannel (1, fBChn[i].c_str());
                     if ((fSubtype == 2) || (fSubtype == 5)) {
                        cal.SetExpo (0, 0);
                        cal.SetExpo (1, 0);
                     }
                     else {
                        cal.SetExpo (0, -1);
                     }
                  }
                  PlotDescriptor* pd = new (nothrow) PlotDescriptor
                     (dat, ((fSubtype == 2) || (fSubtype == 5)) ?
                     kPTCoherenceFunction : kPTTransferFunction, 
                     stepName (fAChn, fStep, fRefTrace).c_str(), 
                     stepName (fBChn[i], fStep, fRefTrace).c_str(), 
                     &prm, &cal);
                  if (pd == 0) {
                     delete dat;
                     break;
                  }
                  else {
                     fPlot->Add (pd);
                     // fill reference trace
                     if (fRef && (i == 0)) {
                        fRef->fModified = kRefTraceModNo;
                        fRef->fGraph = pd->GetGraphType();
                        fRef->fAChn = stepName (fAChn, fStep).c_str();
                        fRef->fBChn = stepName (fBChn[0], fStep).c_str();
                        fRef->fValid = kTRUE;
                     }
                  }
               }
            }
            break;
      
         // add coefficients
         case kCoefficients:
            switch (fSubtype) {
               case 0:
                  break;
               case 1:
                  break;
               case 2:
                  break;
               case 3:
                  break;
               case 4:
                  break;
               case 5:
                  break;
               case 6:
                  break;
               case 7:
                  break;
               case 8:
                  break;
            }
            break;
      
       // histogram (mito)
         case kHistogram:
         
            bool valid = true;
            switch (fSubtype) { // check for valid data
               case 0:
                  if (!fData) valid = false;
                  break;
               case 1:
                  if (!fData || !fBins) valid = false;
                  break;
               case 6:
                  if (!fErrors || !fBins) valid = false;
                  break;
               case 7:
                  if (!fData || !fErrors || !fBins) valid = false;
                  break;
               default :
                  valid = false;
                  break;
            }
         
            if (!valid) {
               if (!ConvertPrecision (true)) {
                  return;
               }
            }
            HistDataRef* hdr = 0;
         
            int nbin = fDim[1] - 2;
            switch (fSubtype) {
               case 0:
                  hdr = new HistDataRef(fX0, fDx, (double*)fData, (double*)0, fStats, 
                                       nbin, fXLabel.c_str(), fNLabel.c_str(), fNData);
                  break;
               case 1:
                  hdr = new HistDataRef(fBins, (double*)fData, (double*)0, fStats, 
                                       nbin, fXLabel.c_str(), fNLabel.c_str(), fNData);
                  break;
               case 2:
                  break;
               case 3:
                  break;
               case 4:
                  break;
               case 5:
                  break;
               case 6:
                  hdr = new HistDataRef(fX0, fDx, (double*)fData, fErrors, fStats, 
                                       nbin, fXLabel.c_str(), fNLabel.c_str(), fNData);
                  break;
               case 7:
                  hdr = new HistDataRef(fBins, (double*)fData, fErrors, fStats, 
                                       nbin, fXLabel.c_str(), fNLabel.c_str(), fNData);
                  break;
               case 8:
                  break;
               case 9:
                  break;
               case 10:
                  break;
               case 11:
                  break;
            }
         
            if (hdr != 0) {
               fData = 0;
               ParameterDescriptor prm;
               prm.SetStartTime(fSec, fNsec);
               prm.SetUser(fOsPrm.str().c_str());
               string n = stepName (fAChn, fStep);
               PlotDescriptor* pd = new (nothrow) PlotDescriptor
                  (hdr,kPTHistogram1,n.c_str(),0,&prm,0);
               if (pd == 0) {
                  delete hdr;
                  break;
               }
               else fPlot->Add(pd);
            }
            break;
      
      }
   }

//______________________________________________________________________________
   bool xsilHandlerData::HandleParameter (const std::string& name,
                     const attrlist& attr, const int& p, int N) 
   {
      if (strcasecmp (name.c_str(), "ObjectType") == 0) {
         return true;
      }
      if (strcasecmp (name.c_str(), "Subtype") == 0) {
         fSubtype = p;
         return true;
      }
      if (strcasecmp (name.c_str(), "MeasurementNumber") == 0) {
         fStep = p;
      }
      else if ((strcasecmp (name.c_str(), "Averages") == 0) &&
              ((fType == kTimeSeries) || (fType == kSpectrum) ||
              (fType == kTransferFunction) ||
              (fType == kCoefficients))) {
         fAvrg = p;
      }
      else if ((strcasecmp (name.c_str(),"NBinx") == 0) &&
              (fType == kHistogram)) { // hist mito
         fNBinx = p;
      }
      else if ((strcasecmp (name.c_str(),"NData") == 0) &&
              (fType == kHistogram)) { // hist mito
         fNData = p;
      }
      return xsilHandlerUnknown::HandleParameter (name, attr, p, N);
   }

//______________________________________________________________________________
   bool xsilHandlerData::HandleParameter (const std::string& name,
                     const attrlist& attr, const double& p, int N) 
   {
      if ((strcasecmp (name.c_str(), "BW") == 0) &&
         (fType == kSpectrum)) {
         fBW = p;
      }
      else if ((strcasecmp (name.c_str(), "dt") == 0) &&
              (fType == kTimeSeries)) {
         fDx = p;
      }
      else if ((strcasecmp (name.c_str(), "df") == 0) &&
              (fType != kTimeSeries)) {
         fDx = p;
      }
      else if ((strcasecmp (name.c_str(), "f0") == 0) &&
              (fType != kTimeSeries)) {
         fX0 = p;
      }
      //==== Histogram Bin (mito)
      else if ((strcasecmp (name.c_str(), "XLowEdge") == 0) &&
              (fType == kHistogram)) {
         fX0 = p;
      }      
      else if ((strcasecmp (name.c_str(), "XSpacing") == 0) &&
              (fType == kHistogram)) {
         fDx = p;
      }
      //==== Histogram Statistics Parameters (mito)
      else if ((strcasecmp (name.c_str(), "SumWeight") == 0) &&
              (fType == kHistogram)) {
         if (!fStats) fStats = new double[10];
         fStats[0] = p;
      }
      else if ((strcasecmp (name.c_str(), "SumWeightSqr") == 0) &&
              (fType == kHistogram)) {
         if (!fStats) fStats = new double[10];
         fStats[1] = p;
      }
      else if ((strcasecmp (name.c_str(), "SumWeightX") == 0) &&
              (fType == kHistogram)) {
         if (!fStats) fStats = new double[10];
         fStats[2] = p;
      }
      else if ((strcasecmp (name.c_str(), "SumWeightXSqr") == 0) &&
              (fType == kHistogram)) {
         if (!fStats) fStats = new double[10];
         fStats[3] = p;
      }
      //==== LDAS ====
      else if ((strncasecmp (name.c_str(), "sampleRate", 10) == 0) &&
              (fType == kLdasTimeSeries)) {
         if (p > 0) fDx = 1. / p;
      }
      else if ((strncasecmp (name.c_str(), "timeOffset", 10) == 0) &&
              (fType == kLdasTimeSeries)) {
         Time t0 (fSec, fNsec);
         t0 += Interval (p);
         fSec = t0.getS();
         fNsec = t0.getN();
      }
      return xsilHandlerUnknown::HandleParameter (name, attr, p, N);
   }

//______________________________________________________________________________
   bool xsilHandlerData::HandleParameter (const std::string& name,
                     const attrlist& attr, const std::string& p) 
   {
      string n;
      int i1 = -1;
      int i2 = -1;
      xsilStd::analyzeName (name, n, i1, i2);
      if ((strcasecmp (name.c_str(), "Channel") == 0) &&
         (fType == kTimeSeries)) {
         int count = 0;
	 for (const char* pt = fAChn.c_str(); *pt; ++pt) {
	    if (*pt == '[') ++count;
	 }
         if (count != 2) {
            fAChn = p;
	 }
      }
      else if ((strcasecmp (name.c_str(), "ChannelA") == 0) &&
              ((fType == kSpectrum) || (fType == kTransferFunction))) {
         fAChn = p;
      }
      else if ((strcasecmp (n.c_str(), "ChannelB") == 0) && (i1 >= 0) &&
              ((fType == kSpectrum) || (fType == kTransferFunction))) {
         if ((int)fBChn.size() <= i1) {
            fBChn.resize(i1 + 1);
         }
         fBChn[i1] = p;
      }
      //==== Histogram Title & Labels (mito)
      else if ((strcasecmp (name.c_str(), "Title") == 0) &&
              (fType == kHistogram)) {
         fTitle = p;
         fAChn = p;
      }
      else if ((strcasecmp (name.c_str(), "XLabel") == 0) &&
              (fType == kHistogram)) {
         fXLabel = p;
      }
      else if ((strcasecmp (name.c_str(), "NLabel") == 0) &&
              (fType == kHistogram)) {
         fNLabel = p;
      }
      return xsilHandlerUnknown::HandleParameter (name, attr, p); 
   }

//______________________________________________________________________________
   bool xsilHandlerData::HandleTime (const std::string& name,
                     const attrlist& attr, 
                     unsigned long sec, unsigned long nsec) 
   {
      if (strcasecmp (name.c_str(), "t0") == 0) {
         fSec = sec;
         fNsec = nsec;
         //return true;
      }
      return xsilHandlerUnknown::HandleTime (name, attr, sec, nsec);
   }

//______________________________________________________________________________
   bool xsilHandlerData::HandleData (const std::string& name,
                     float* x, int dim1, int dim2, int dim3, int dim4) 
   {
      if ((fType == kHistogram) &&
         ((strcasecmp (name.c_str(), "XBins") == 0) ||
         (strcasecmp (name.c_str(), "Contents") == 0) ||
         (strcasecmp (name.c_str(), "Errors") == 0))) {
         int N = 1;
         if (dim1 > 0) N *= dim1;
         if (dim2 > 0) N *= dim2;
         if (dim3 > 0) N *= dim3;
         if (dim4 > 0) N *= dim4;
         bool is_double = false;
         if (!convert_precision ((void*&)x, N, is_double, true)) {
            return false;
         }
         return HandleData (name, (double*)x, dim1, dim2, dim3, dim4);
      }
      else {
         return xsilHandlerUnknown::HandleData (
                              "", x, dim1, dim2, dim3, dim4);
      }
   }

//______________________________________________________________________________
   bool xsilHandlerData::HandleData (const std::string& name,
                     std::complex<float>* x, 
                     int dim1, int dim2, int dim3, int dim4) 
   {
      return xsilHandlerUnknown::HandleData (
                           "", x, dim1, dim2, dim3, dim4);
   }

//______________________________________________________________________________
   bool xsilHandlerData::HandleData (const std::string& name,
                     double* x, int dim1, int dim2, int dim3, int dim4) 
   {
      if ((strcasecmp (name.c_str(), "XBins") == 0) &&
         (fType == kHistogram)) {
         // load bins
         if (fBins) delete [] (char*) fBins;
         fDim[0] = dim1;
         fBins = x;
         fDouble = true;
         fComplex = false;
         return true;
      }
      else if ((strcasecmp (name.c_str(), "Contents") == 0) &&
              (fType == kHistogram)) {
         // load errors
         if (fData) delete [] (char*) fData;
         fDim[1] = dim1;
         fData = x;
         fDouble = true;
         fComplex = false;
         return true;
      }
      else if ((strcasecmp (name.c_str(), "Errors") == 0) &&
              (fType == kHistogram)) {
         // load errors
         if (fErrors) delete [] (char*) fErrors;
         fDim[1] = dim1;
         fErrors = x;
         fDouble = true;
         fComplex = false;
         return true;
      }
      else {
         return xsilHandlerUnknown::HandleData (
                              "", x, dim1, dim2, dim3, dim4);
      }
   }

//______________________________________________________________________________
   bool xsilHandlerData::HandleData (const std::string& name,
                     std::complex<double>* x, 
                     int dim1, int dim2, int dim3, int dim4) 
   {
      return xsilHandlerUnknown::HandleData (
                           "", x, dim1, dim2, dim3, dim4);
   }
//______________________________________________________________________________
   xsilHandler* xsilHandlerData::GetHandler (const attrlist& attr) 
   {
      if (fType == kLdasTimeSeries) {
         return new xsilHandlerLdasData (*this);
      }
      else {
         return xsilHandlerUnknown::GetHandler (attr); 
      }
   }

//______________________________________________________________________________
   bool xsilHandlerData::ConvertPrecision (bool to_double)
   {
      int N = 1;
      for (int i = 0; i < 4; ++i) {
         if (fDim[i] > 0) N *= fDim[i];
      }
      if (fComplex) N *= 2;
      return convert_precision (fData, N, fDouble, to_double);
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// xsilHandlerOptions                                                   //
//                                                                      //
// Handles plot data objects                                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   xsilHandlerOptions::xsilHandlerOptions (OptionAll_t& opt)
   : fOpt (&opt)
   {
   }

//______________________________________________________________________________
   xsilHandlerOptions::~xsilHandlerOptions()
   {
   }

//______________________________________________________________________________
   bool xsilHandlerOptions::HandleParameter (const std::string& name,
                     const attrlist& attr, const bool& p, int N) 
   {
      int i1;
      int i2;
      string n;
      if (!xsilStd::analyzeName (name, n, i1, i2)) {
         return true;
      }
      else if (strcasecmp (n.c_str(), stPlotTracesActive) == 0) {
         if ((i1 >= 0) && (i1 < kMaxTraces)) {
            fOpt->fTraces.fActive[i1] = p;
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotRangeBinLogSpacing) == 0) {
         fOpt->fRange.fBinLogSpacing = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotCursorActive) == 0) {
         if (N >= 1) fOpt->fCursor.fActive[0] = (&p)[0];
         if (N >= 2) fOpt->fCursor.fActive[1] = (&p)[1];
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotCursorValid) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fCursor.fValid[i] = (&p)[i];
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotConfigAutoConfig) == 0) {
         fOpt->fConfig.fAutoConf = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotConfigRespectUser) == 0) {
         fOpt->fConfig.fRespectUser = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotConfigAutoAxes) == 0) {
         fOpt->fConfig.fAutoAxes = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotConfigAutoBin) == 0) {
         fOpt->fConfig.fAutoBin = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotConfigAutoTimeAdjust) == 0) {
         fOpt->fConfig.fAutoTimeAdjust = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisXGrid) == 0) {
         fOpt->fAxisX.fGrid = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisXBothSides) == 0) {
         fOpt->fAxisX.fBothSides = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisXCenterTitle) == 0) {
         fOpt->fAxisX.fCenterTitle = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisYGrid) == 0) {
         fOpt->fAxisY.fGrid = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisYBothSides) == 0) {
         fOpt->fAxisY.fBothSides = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisYCenterTitle) == 0) {
         fOpt->fAxisY.fCenterTitle = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotLegendShow) == 0) {
         fOpt->fLegend.fShow = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotParamShow) == 0) {
         fOpt->fParam.fShow = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotParamT0) == 0) {
         fOpt->fParam.fT0 = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotParamAvg) == 0) {
         fOpt->fParam.fAvg = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotParamSpecial) == 0) {
         fOpt->fParam.fSpecial = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotParamStat) == 0) {
         fOpt->fParam.fStat = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotParamUOBins) == 0) {
         fOpt->fParam.fUOBins = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotParamTimeFormatUTC) == 0) {
         fOpt->fParam.fTimeFormatUTC = p;
         return true; 
      }
      else {
         return false;
      }
   }

//______________________________________________________________________________
   bool xsilHandlerOptions::HandleParameter (const std::string& name,
                     const attrlist& attr, const int& p, int N)
   {
      int i1;
      int i2;
      string n;
      if (!xsilStd::analyzeName (name, n, i1, i2)) {
         return true;
      }
      else if (strcasecmp (n.c_str(), stPlotTracesPlotStyle) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fTraces.fPlotStyle[i] = (EPlotStyle)(&p)[i];
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotTracesLineAttrColor) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fTraces.fLineAttr[i].SetLineColor 
                  (gPlotColorLookup().Add ((&p)[i]));
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotTracesLineAttrStyle) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fTraces.fLineAttr[i].SetLineStyle ((&p)[i]);
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotTracesMarkerAttrColor) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fTraces.fMarkerAttr[i].SetMarkerColor
                  (gPlotColorLookup().Add ((&p)[i]));
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotTracesMarkerAttrStyle) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fTraces.fMarkerAttr[i].SetMarkerStyle ((&p)[i]);
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotTracesBarAttrColor) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fTraces.fBarAttr[i].SetFillColor 
                  (gPlotColorLookup().Add ((&p)[i]));
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotTracesBarAttrStyle) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fTraces.fBarAttr[i].SetFillStyle ((&p)[i]);
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotRangeAxisScale) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fRange.fAxisScale[i] = (EAxisScale)(&p)[i];
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotRangeRange) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fRange.fRange[i] = (ERange)(&p)[i];
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotRangeBin) == 0) {
         fOpt->fRange.fBin = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotUnitsXValues) == 0) {
         fOpt->fUnits.fXValues = (EXRangeType)p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotUnitsYValues) == 0) {
         fOpt->fUnits.fYValues = (EYRangeType)p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotUnitsXMag) == 0) {
         fOpt->fUnits.fXMag = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotUnitsYMag) == 0) {
         fOpt->fUnits.fYMag = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotCursorTrace) == 0) {
         fOpt->fCursor.fTrace = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotCursorStyle) == 0) {
         fOpt->fCursor.fStyle = (ECursorStyle)p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotCursorType) == 0) {
         fOpt->fCursor.fType = (ECursorType)p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotStyleTitleAlign) == 0) {
         fOpt->fStyle.fTitleAttr.SetTextAlign (p);
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotStyleTitleColor) == 0) {
         fOpt->fStyle.fTitleAttr.SetTextColor (gPlotColorLookup().Add (p));
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotStyleTitleFont) == 0) {
         fOpt->fStyle.fTitleAttr.SetTextFont (p);
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisXAxisAttrAxisColor) == 0) {
         fOpt->fAxisX.fAxisAttr.SetAxisColor (gPlotColorLookup().Add (p));
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisXAxisAttrLabelColor) == 0) {
         fOpt->fAxisX.fAxisAttr.SetLabelColor (gPlotColorLookup().Add (p));
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisXAxisAttrTitleColor) == 0) {
         fOpt->fAxisX.fAxisAttr.SetTitleColor (gPlotColorLookup().Add (p));
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisXAxisAttrLabelFont) == 0) {
         fOpt->fAxisX.fAxisAttr.SetLabelFont (p);
         fOpt->fAxisX.fAxisAttr.SetTitleFont (p);
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisXAxisAttrNdividions) == 0) {
         fOpt->fAxisX.fAxisAttr.SetNdivisions (p);
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisYAxisAttrAxisColor) == 0) {
         fOpt->fAxisY.fAxisAttr.SetAxisColor (gPlotColorLookup().Add (p));
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisYAxisAttrLabelColor) == 0) {
         fOpt->fAxisY.fAxisAttr.SetLabelColor (gPlotColorLookup().Add (p));
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisYAxisAttrTitleColor) == 0) {
         fOpt->fAxisY.fAxisAttr.SetTitleColor (gPlotColorLookup().Add (p));
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisYAxisAttrLabelFont) == 0) {
         fOpt->fAxisY.fAxisAttr.SetLabelFont (p);
         fOpt->fAxisY.fAxisAttr.SetTitleFont (p);
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisYAxisAttrNdividions) == 0) {
         fOpt->fAxisY.fAxisAttr.SetNdivisions (p);
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotLegendPlacement) == 0) {
         fOpt->fLegend.fPlacement = (ELegendPlacement)p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotLegendSymbolStyle) == 0) {
         fOpt->fLegend.fSymbolStyle = (ELegendStyle)p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotLegendTextStyle) == 0) {
         fOpt->fLegend.fTextStyle = (ELegendText)p;
         return true; 
      }
      else {
         return false;
      }
   }


//______________________________________________________________________________
   bool xsilHandlerOptions::HandleParameter (const std::string& name,
                     const attrlist& attr, const double& p, int N)
   {
      int i1;
      int i2;
      string n;
      if (!xsilStd::analyzeName (name, n, i1, i2)) {
         return true;
      }
      else if (strcasecmp (n.c_str(), stPlotTracesLineAttrWidth) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
	    if (N > i) 
	       fOpt->fTraces.fLineAttr[i].SetLineWidth (Width_t((&p)[i]));
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotTracesMarkerAttrSize) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fTraces.fMarkerAttr[i].SetMarkerSize ((&p)[i]);
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotTracesBarAttrWidth) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fTraces.fBarWidth[i] = (&p)[i];
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotRangeRangeFrom) == 0) {
         if (N >= 1) fOpt->fRange.fRangeFrom[0] = (&p)[0];
         if (N >= 2) fOpt->fRange.fRangeFrom[1] = (&p)[1];
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotRangeRangeTo) == 0) {
         if (N >= 1) fOpt->fRange.fRangeTo[0] = (&p)[0];
         if (N >= 2) fOpt->fRange.fRangeTo[1] = (&p)[1];
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotUnitsXSlope) == 0) {
         fOpt->fUnits.fXSlope = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotUnitsYSlope) == 0) {
         fOpt->fUnits.fYSlope = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotUnitsXOffset) == 0) {
         fOpt->fUnits.fXOffset = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotUnitsYOffset) == 0) {
         fOpt->fUnits.fYOffset = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotCursorX) == 0) {
         if (N >= 1) fOpt->fCursor.fX[0] = (&p)[0];
         if (N >= 2) fOpt->fCursor.fX[1] = (&p)[1];
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotCursorH) == 0) {
         if (N >= 1) fOpt->fCursor.fH[0] = (&p)[0];
         if (N >= 2) fOpt->fCursor.fH[1] = (&p)[1];
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotCursorY) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            for (int j = 0; j < 2; j++) {
               if (N > i) fOpt->fCursor.fY[i][j] = (&p)[2*i+j];
            }
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotCursorN) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fCursor.fN[i] = (&p)[i];
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotCursorXDiff) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fCursor.fXDiff[i] = (&p)[i];
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotCursorYDiff) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fCursor.fYDiff[i] = (&p)[i];
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotCursorMean) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fCursor.fMean[i] = (&p)[i];
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotCursorRMS) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fCursor.fRMS[i] = (&p)[i];
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotCursorStdDev) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fCursor.fStdDev[i] = (&p)[i];
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotCursorSum) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fCursor.fSum[i] = (&p)[i];
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotCursorSqrSum) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fCursor.fSqrSum[i] = (&p)[i];
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotCursorArea) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fCursor.fArea[i] = (&p)[i];
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotCursorRMSArea) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fCursor.fRMSArea[i] = (&p)[i];
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotCursorPeakX) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fCursor.fPeakX[i] = (&p)[i];
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotCursorPeakY) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fCursor.fPeakY[i] = (&p)[i];
         }
         return true; 
      }
      
      //==== hist (mito)
      else if (strcasecmp (n.c_str(), stPlotCursorCenter) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fCursor.fCenter[i] = (&p)[i];
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotCursorWidth) == 0) {
         for (int i = 0; i < kMaxTraces; i++) {
            if (N > i) fOpt->fCursor.fWidth[i] = (&p)[i];
         }
         return true; 
      }
      //====
      
      else if (strcasecmp (n.c_str(), stPlotStyleTitleAngle) == 0) {
         fOpt->fStyle.fTitleAttr.SetTextAngle (p);
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotStyleTitleSize) == 0) {
         fOpt->fStyle.fTitleAttr.SetTextSize (p);
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotStyleMargin) == 0) {
         if (N >= 1) fOpt->fStyle.fMargin[0] = (&p)[0];
         if (N >= 2) fOpt->fStyle.fMargin[1] = (&p)[1];
         if (N >= 3) fOpt->fStyle.fMargin[2] = (&p)[2];
         if (N >= 4) fOpt->fStyle.fMargin[3] = (&p)[3];
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisXAxisAttrLabelOffset) == 0) {
         fOpt->fAxisX.fAxisAttr.SetLabelOffset (p);
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisXAxisAttrLabelSize) == 0) {
         fOpt->fAxisX.fAxisAttr.SetLabelSize (p);
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisXAxisAttrTickLength) == 0) {
         fOpt->fAxisX.fAxisAttr.SetTickLength (p);
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisXAxisAttrTitleOffset) == 0) {
         fOpt->fAxisX.fAxisAttr.SetTitleOffset (p);
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisXAxisAttrTitleSize) == 0) {
         fOpt->fAxisX.fAxisAttr.SetTitleSize (p);
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisYAxisAttrLabelOffset) == 0) {
         fOpt->fAxisY.fAxisAttr.SetLabelOffset (p);
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisYAxisAttrLabelSize) == 0) {
         fOpt->fAxisY.fAxisAttr.SetLabelSize (p);
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisYAxisAttrTickLength) == 0) {
         fOpt->fAxisY.fAxisAttr.SetTickLength (p);
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisYAxisAttrTitleOffset) == 0) {
         fOpt->fAxisY.fAxisAttr.SetTitleOffset (p);
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisYAxisAttrTitleSize) == 0) {
         fOpt->fAxisY.fAxisAttr.SetTitleSize (p);
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotLegendXAdjust) == 0) {
         fOpt->fLegend.fXAdjust = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotLegendYAdjust) == 0) {
         fOpt->fLegend.fYAdjust = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotLegendSize) == 0) {
         fOpt->fLegend.fSize = p;
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotParamTextSize) == 0) {
         fOpt->fParam.fTextSize = p;
         return true; 
      }
      else {
         return false;
      }
   }

//______________________________________________________________________________
   bool xsilHandlerOptions::HandleParameter (const std::string& name,
                     const attrlist& attr, const std::string& p)
   {
      int i1;
      int i2;
      string n;
      if (!xsilStd::analyzeName (name, n, i1, i2)) {
         return true;
      }
      else if (strcasecmp (n.c_str(), stPlotName) == 0) {
         fOpt->fName = p.c_str();
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotTracesGraphType) == 0) {
         fOpt->fTraces.fGraphType = p.c_str();
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotTracesAChannel) == 0) {
         if ((i1 >= 0) && (i1 < kMaxTraces)) {
            fOpt->fTraces.fAChannel[i1] = p.c_str();
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotTracesBChannel) == 0) {
         if ((i1 >= 0) && (i1 < kMaxTraces)) {
            fOpt->fTraces.fBChannel[i1] = p.c_str();
         }
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotUnitsXUnit) == 0) {
         fOpt->fUnits.fXUnit = p.c_str();
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotUnitsYUnit) == 0) {
         fOpt->fUnits.fYUnit = p.c_str();
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotStyleTitle) == 0) {
         fOpt->fStyle.fTitle = p.c_str();
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisXTitle) == 0) {
         fOpt->fAxisX.fAxisTitle = p.c_str();
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotAxisYTitle) == 0) {
         fOpt->fAxisY.fAxisTitle = p.c_str();
         return true; 
      }
      else if (strcasecmp (n.c_str(), stPlotLegendText) == 0) {
         if ((i1 >= 0) && (i1 < kMaxTraces)) {
            fOpt->fLegend.fText[i1] = p.c_str();
         }
         return true; 
      }
      else {
         return false;
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// xsilHandlerCalibration                                               //
//                                                                      //
// Handles calibration records                                          //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   xsilHandlerCalibration::xsilHandlerCalibration (
                     calibration::Table& cals, bool overwrite) 
   : xsilHandler (true), fCal (&cals), fOverwrite (overwrite)
   {
      fRec = new (nothrow) calibration::Calibration;
   }

//______________________________________________________________________________
   xsilHandlerCalibration::~xsilHandlerCalibration()
   {
      if (fRec && fRec->GetChannel() && *fRec->GetChannel()) {
         fCal->AddChannel (fRec->GetChannel());
         fCal->Add (*fRec, fOverwrite);
      }
      if (fRec) delete fRec;
   }

//______________________________________________________________________________
   bool xsilHandlerCalibration::HandleParameter (const std::string& name,
                     const attrlist& attr, const bool& p, int N)
   {
      if (fRec == 0) {
         return false;
      }
      if (strcasecmp (name.c_str(), "Default") == 0) {
         fRec->SetDefault (p);
         return true;
      }
      else {
         return false;
      }
   }

//______________________________________________________________________________
   bool xsilHandlerCalibration::HandleParameter (const std::string& name,
                     const attrlist& attr,
                     const int& p, int N)
   {
      if (fRec == 0) {
         return false;
      }
      if (strcasecmp (name.c_str(), "Duration") == 0) {
         fRec->SetDuration (p);
         return true;
      }
      else if (strcasecmp (name.c_str(), "PreferredMag") == 0) {
         fRec->SetPreferredMag (p);
         return true;
      }
      else if (strcasecmp (name.c_str(), "PreferredD") == 0) {
         fRec->SetPreferredD (p);
         return true;
      }
      else {
         return false;
      }
   }

//______________________________________________________________________________
   bool xsilHandlerCalibration::HandleParameter (const std::string& name,
                     const attrlist& attr, const float& p, int N)
   {
      if (fRec == 0) {
         return false;
      }
      if ((strcasecmp (name.c_str(), "TransferFunction") == 0) &&
         (N > 2)) {
         fRec->SetTransferFunction (&p, N / 3);
         return true;
      }
      else {
         return false;
      }
   }

//______________________________________________________________________________
   bool xsilHandlerCalibration::HandleParameter (const std::string& name,
                     const attrlist& attr, const double& p, int N)
   {
      if (fRec == 0) {
         return false;
      }
      if (strcasecmp (name.c_str(), "Conversion") == 0) {
         fRec->SetConversion (p);
         return true;
      }
      else if (strcasecmp (name.c_str(), "Offset") == 0) {
         fRec->SetOffset (p);
         return true;
      }
      else if (strcasecmp (name.c_str(), "TimeDelay") == 0) {
         fRec->SetTimeDelay (p);
         return true;
      }
      else if (strcasecmp (name.c_str(), "Gain") == 0) {
         double gain = 1; int pnum = 0; int znum = 0;
         float val = 0; const float* pzs = &val;
         fRec->GetPoleZeros (gain, pnum, znum, pzs);
         fRec->SetPoleZeros (p, pnum, znum, pzs);
         return true;
      }
      else {
         return false;
      }
   }

//______________________________________________________________________________
   bool xsilHandlerCalibration::HandleParameter (const std::string& name,
                     const attrlist& attr,
                     const std::complex<double>& p, int N)
   {
      if (fRec == 0) {
         return false;
      }
      if ((strcasecmp (name.c_str(), "Poles") == 0) && (N > 0)) {
         double gain = 1; int pnum = 0; int znum = 0;
         float val = 0; const float* pzs = &val;
         fRec->GetPoleZeros (gain, pnum, znum, pzs);
         pnum = N;
         float* data = 
            new (nothrow) float [4 * ((pnum > znum) ? pnum : znum)];
         if (data) {
            for (int i = 0; i < pnum; i++) {
               data[4*i] = ((double*)&p)[2*i];
               data[4*i+1] = ((double*)&p)[2*i+1];
            }
            for (int i = 0; i < znum; i++) {
               data[4*i+2] = pzs[4*i+2];
               data[4*i+3] = pzs[4*i+3];
            }
            fRec->SetPoleZeros (gain, pnum, znum, data);
            delete [] data;
         }
         return true;
      }
      else if ((strcasecmp (name.c_str(), "Zeros") == 0) && (N > 0)) {
         double gain = 1; int pnum = 0; int znum = 0;
         float val = 0; const float* pzs = &val;
         fRec->GetPoleZeros (gain, pnum, znum, pzs);
         znum = N;
         float* data = 
            new (nothrow) float [4 * ((pnum > znum) ? pnum : znum)];
         if (data) {
            for (int i = 0; i < pnum; i++) {
               data[4*i] = pzs[4*i];
               data[4*i+1] = pzs[4*i+1];
            }
            for (int i = 0; i < znum; i++) {
               data[4*i+2] = ((double*)&p)[2*i];
               data[4*i+3] = ((double*)&p)[2*i+1];
            }
            fRec->SetPoleZeros (gain, pnum, znum, data);
            delete [] data;
         }
         return true;
      }
      else {
         return false;
      }
   }

//______________________________________________________________________________
   bool xsilHandlerCalibration::HandleParameter (const std::string& name,
                     const attrlist& attr, const std::string& p)
   {
      if (fRec == 0) {
         return false;
      }
      if (strcasecmp (name.c_str(), "Channel") == 0) {
         fRec->SetChannel (p.c_str());
         return true;
      }
      else if (strcasecmp (name.c_str(), "Reference") == 0) {
         fRec->SetRef (p.c_str());
         return true;
      }
      else if (strcasecmp (name.c_str(), "Unit") == 0) {
         fRec->SetUnit (p.c_str());
         return true;
      }
      else if (strcasecmp (name.c_str(), "Comment") == 0) {
         fRec->SetComment (p.c_str());
         return true;
      }
      else {
         return false;
      }
   }

//______________________________________________________________________________
   bool xsilHandlerCalibration::HandleTime (const std::string& name,
                     const attrlist& attr,
                     unsigned long sec, unsigned long nsec)
   {
      if (fRec == 0) {
         return false;
      }
      fRec->SetTime (Time(sec));
      return true;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// xsilHandlerQueryData                                                 //
//                                                                      //
// Handler query for data records   	                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   xsilHandler* xsilHandlerQueryData::GetHandler (const attrlist& attr) 
   {
      if (fPlot == 0) {
         return 0;
      }
      attrlist::const_iterator ni = attr.find (xmlName);
      attrlist::const_iterator ti = attr.find (xmlType);
      attrlist::const_iterator fi = attr.find (xmlFlag);
      if ((ni == attr.end()) || (ti == attr.end())) {
         return 0;
      }
      string n;
      int reftrace = -1;
      if (strncasecmp (ni->second.c_str(), "Reference", 9) == 0) {
         int i = 0; 
         int j = 0;
         if (!xsilStd::analyzeName (ni->second, n, i, j) || 
            (i < 0) || (i >= kMaxTraces)) {
            return 0; 
         }
         reftrace = i;
      }
      else if (strstr (ni->second.c_str(), "(REF") != 0) {
         const char* p = strstr (ni->second.c_str(), "(REF") + 4;
         reftrace = atoi (p);
         if ((reftrace < 0) || (reftrace >= kMaxTraces)) {
            reftrace = kMaxTraces - 1;
         }
         n = ni->second;
         n.erase (n.find ("(REF"));
      }
      else {
         n = ni->second;
      }
      ReferenceTrace_t* ref = 0;
      if (fRef && (reftrace >= 0)) {
         ref = fRef->fTraces + reftrace;
      }
      if (strcasecmp (ti->second.c_str(), "TimeSeries") == 0) {
         if (!fRawData && (fi != attr.end()) &&
            (strcasecmp (fi->second.c_str(), "TimeSeries") == 0)) {
            return 0;
         }
         else {
            return new (nothrow) xsilHandlerData (n,
                                 xsilHandlerData::kTimeSeries, 
                                 *fPlot, fCal, ref, reftrace);
         }
      }
      if (strcasecmp (ti->second.c_str(), "LDASTimeSeries") == 0) {
         return new (nothrow) xsilHandlerData (n,
                              xsilHandlerData::kLdasTimeSeries, 
                              *fPlot, fCal, ref, reftrace);
      }
      if (strcasecmp (ti->second.c_str(), "Spectrum") == 0) {
         return new (nothrow) xsilHandlerData (n,
                              xsilHandlerData::kSpectrum, 
                              *fPlot, fCal, ref, reftrace);
      }
      if (strcasecmp (ti->second.c_str(), "TransferFunction") == 0) {
         return new (nothrow) xsilHandlerData (n,
                              xsilHandlerData::kTransferFunction, 
                              *fPlot, fCal, ref, reftrace);
      }
      if (strcasecmp (ti->second.c_str(), "Coefficients") == 0) {
         return new (nothrow) xsilHandlerData (n,
                              xsilHandlerData::kCoefficients, 
                              *fPlot, fCal, ref, reftrace);
      }
      if (strcasecmp (ti->second.c_str(), "Histogram") == 0) { // hist mito
         return new (nothrow) xsilHandlerData (n,
                              xsilHandlerData::kHistogram, 
                              *fPlot, fCal, ref, reftrace);
      }
      else {
         return 0;
      }
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// xsilHandlerQueryOptions                                              //
//                                                                      //
// Handler query for option array 	                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   xsilHandler* xsilHandlerQueryOptions::GetHandler (
                     const attrlist& attr) 
   {
      attrlist::const_iterator ni = attr.find (xmlName);
      if ((ni != attr.end()) &&
         (strncasecmp (ni->second.c_str(), "Plot", 4) == 0)) {
         int i = 0; 
         int j = 0;
         string n;
         if (!xsilStd::analyzeName (ni->second, n, i, j) || 
            (i < 0) || (i >= fOpt->GetMaxWin()) || 
            (j < 0) || (j >= fOpt->GetMaxPad())) {
            return 0; 
         }
         if ((*fOpt)(i,j) == 0) {
            (*fOpt)[i][j] = new (nothrow) OptionAll_t;
            if ((*fOpt)(i,j) == 0) {
               return 0;
            }
         }
         SetDefaultGraphicsOptions (*(*fOpt)(i,j));
         return new (nothrow) xsilHandlerOptions (*(*fOpt)(i,j));
      }
      else {
         return 0;
      }
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// xsilHandlerQueryReferenence                                          //
//                                                                      //
// Handler query for reference trace records                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   xsilHandler* xsilHandlerQueryReferenence::GetHandler (
                     const attrlist& attr) 
   {
      attrlist::const_iterator ni = attr.find (xmlName);
      if ((ni != attr.end()) &&
         (strncasecmp (ni->second.c_str(), "Reference", 9) == 0)) {
         return 0;
      }
      else {
         return 0;
      }
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// xsilHandlerQueryCalibration                                          //
//                                                                      //
// Handler query for calibration records                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   xsilHandler* xsilHandlerQueryCalibration::GetHandler (
                     const attrlist& attr) 
   {
      attrlist::const_iterator ni = attr.find (xmlName);
      if ((ni != attr.end()) &&
         (strncasecmp (ni->second.c_str(), "Calibration", 11) == 0)) {
         return new xsilHandlerCalibration (*fCal, fOverwrite);
      }
      else {
         return 0;
      }
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// xsilHandlerQueryMath	                                                //
//                                                                      //
// Handler query for math table 	                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   xsilHandler* xsilHandlerQueryMath::GetHandler (const attrlist& attr) 
   {
      attrlist::const_iterator ni = attr.find (xmlName);
      if ((ni != attr.end()) &&
         (strncasecmp (ni->second.c_str(), "Math", 4) == 0)) {
         return 0;
      }
      else {
         return 0;
      }
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// xsilHandlerQueryUnknown                                              //
//                                                                      //
// Handler query for unknown data objects                               //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   xsilHandler* xsilHandlerQueryUnknown::GetHandler (const attrlist& attr) 
   {
      attrlist::const_iterator ni = attr.find (xmlName);
      if ((ni != attr.end()) && fOs && 
         (strncasecmp (ni->second.c_str(), "Index", 5) != 0)) {
         return new xsilHandlerUnknown (*fOs, &attr, false);
      }
      else {
         return 0;
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGXMLSaver                                                          //
//                                                                      //
// Write XML data  to file                                              //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   Bool_t TLGXMLSaver::Setup ()
   {
      if (fOut) delete fOut;
      fOut = new (nothrow) ofstream (fFilename);
      if (!fOut || !(*fOut)) {
         if (fOut) {
            delete fOut;
         }
         fOut = 0;
         *fError = TString ("File open failed for\n") + fFilename;
         return kFALSE;
      }
      *fOut << xsilHeader() << endl;
      if (!*fOut) {
         return kFALSE;
      }
      if (fXMLObjs) {
         *fOut << (*fXMLObjs);
      }
      if (!*fOut) {
         return kFALSE;
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGXMLSaver::Data (PlotSet& pset)
   {
      if (!fOut) {
         return kFALSE;
      }
      if (!pset.Empty()) *fOut << pset << endl;
      return !!*fOut;
   }

//______________________________________________________________________________
   Bool_t TLGXMLSaver::PlotSettings (OptionArray& opts)
   {
      if (!fOut) {
         return kFALSE;
      }
      *fOut << opts << endl;
      return !!*fOut;
   }

//______________________________________________________________________________
   Bool_t TLGXMLSaver::ReferenceList (ReferenceTraceList_t& ref)
   {
      if (!fOut) {
         return kFALSE;
      }
      bool empty = true;
      for (int i = 0; i < kMaxReferenceTraces; i++) 
         if (ref.fTraces[i].fValid) empty = false;
      if (!empty) {
         *fOut << ref << endl;
      }
      return !!*fOut;
   }

//______________________________________________________________________________
   Bool_t TLGXMLSaver::CalibrationData (calibration::Table& cals)
   {
      if (!fOut) {
         return kFALSE;
      }
      if (cals.Len() > 0) {
         *fOut << cals << endl;
      }
      return !!*fOut;
   }

//______________________________________________________________________________
   Bool_t TLGXMLSaver::Math (MathTable_t& math)
   {
      if (!fOut) {
         return kFALSE;
      }
      *fOut << math << endl;
      return !!*fOut;
   }

//______________________________________________________________________________
   Bool_t TLGXMLSaver::Done (Bool_t success)
   {
      if (!fOut) {
         return kFALSE;
      }
      bool err = !success;
      if (success) {
         *fOut << xsilTrailer() << endl;
         err = !*fOut;
      }
      delete fOut;
      fOut = 0;
      if (!success) {
         remove (fFilename);
         *fError = "File writing error";
      }
      return !err;
   }

//______________________________________________________________________________
   string noRef (const char* chn) 
   {
      string s (chn);
      string::size_type pos = s.find ("(REF");
      if (pos != string::npos) {
         s.erase (pos);
      }
      return s;
   }

//______________________________________________________________________________
   bool TLGXMLSaver::GetChannelList (PlotSet& pset,
                     const PlotDescriptor& plotd, 
                     std::vector<std::string>& AChannels,
                     std::vector<std::string>& BChannels)
   {
      AChannels.clear();
      BChannels.clear();
   
      // deal with referenced copy
      // const DataRef* dr;
      // if ((dr = dynamic_cast<const DataRef*>(plotd.GetData())) &&
         // dr->GetRef()) {
      // }
      // 
      // //  normal descriptors
      // else {
      if (!plotd.GetAChannel()) {
         return false;
      }
      AChannels.push_back (noRef (plotd.GetAChannel()));
      if (plotd.GetBChannel()) {
         BChannels.push_back (noRef (plotd.GetBChannel()));
      }
      // }
      return true;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGXMLRestorer                                                       //
//                                                                      //
// Read XML data fomr file                                              //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   Bool_t TLGXMLRestorer::Setup ()
   {
      if (fInp) delete fInp;
      fInp = new (nothrow) ifstream (fFilename);
      if (!fInp || !(*fInp)) {
         if (fInp) {
            delete fInp;
         }
         fInp = 0;
         *fError = TString ("File open failed for\n") + fFilename;
         return kFALSE;
      }
      return !!*fInp;
   }

//______________________________________________________________________________
   Bool_t TLGXMLRestorer::Done (Bool_t success)
   {
      if (!fInp) {
         return kFALSE;
      }
      bool err = !success;
      if (success) {
         xsilHandlerQueryData dataQ (fPlots, 
                              fFileRestoreFlag == kRestoreAll, fCal, fRef);
         if (fPlots && (fFileRestoreFlag != kRestoreParameterOnly)) 
            fXml.AddHandler (dataQ);
         xsilHandlerQueryOptions optQ (fOpts);
         if (fOpts) fXml.AddHandler (optQ);
         xsilHandlerQueryReferenence refQ (fRef);
         if (fRef) fXml.AddHandler (refQ);
         xsilHandlerQueryCalibration calQ (fCal);
         if (fCal) fXml.AddHandler (calQ);
         xsilHandlerQueryMath mathQ (fMath);
         if (fMath) fXml.AddHandler (mathQ);
         xsilHandlerQueryUnknown unknownQ (fOs);
         if (fOs) fXml.AddHandler (unknownQ);
         err = !fXml.Parse (*fInp);
      }
      delete fInp;
      fInp = 0;
      if (err) {
         *fError = "File reading error";
      }
      else {
         // compute auxiliary traces
         if (fPlots) {
            AddComputedTraces();
         }
         // add units
         if (fPlots && fCal) {
            for (PlotSet::iterator i = fPlots->begin(); 
                i != fPlots->end(); i++) {
               fCal->AddUnits (i->Cal());
            }
         }
      }
      return !err;
   }

//______________________________________________________________________________
   void TLGXMLRestorer::AddComputedTrace (PlotSet& pset,
                     vector<PlotDescriptor*>& pds,
                     const PlotDescriptor& pd,
                     calibration::Table* caltable)
   {
      // add rms to power spectrum
      if (strcmp (pd.GetGraphType(), kPTPowerSpectrum) == 0) {
      }
      
      // add transfer function from cross spectrum
      else if (strcmp (pd.GetGraphType(), kPTCrossCorrelation) == 0) {
         // does A PSD exists?
         const PlotDescriptor* apsd = 
            pset.Get (kPTPowerSpectrum, pd.GetAChannel());
         if ((pd.GetData() == 0) || 
            (apsd == 0) || (apsd->GetData() == 0))  {
            return; 
         }
         // does transfer function alrady exists?
         if (pset.Get (kPTTransferFunction, pd.GetAChannel(),
                      pd.GetBChannel()) != 0) {
            return;
         }
         // ok: compute transfer function
         BasicDataDescriptor* dat = 0;
         dat = new (nothrow) DivSqrDataDescriptor (pd.GetData(),
                              apsd->GetData());
         calibration::Descriptor cal (apsd->Cal().GetTime(),
                              caltable ? kPTTransferFunction : 0,
                              pd.GetAChannel(), pd.GetBChannel());
         PlotDescriptor* npd = new (nothrow) PlotDescriptor 
            (dat, kPTTransferFunction, pd.GetAChannel(),
            pd.GetBChannel(), &pd.Param(), &cal);
         if ((npd == 0) || (dat == 0)) {
            if (dat) delete dat;
            if (npd) delete npd;
         }
         else {
            pds.push_back (npd);
         }
      }
   }

//______________________________________________________________________________
   Bool_t TLGXMLRestorer::AddComputedTraces ()
   {
      vector<PlotDescriptor*>	pds;
      // add transfer function when cross-spectrum/psd exists
      for (PlotSet::iterator i = fPlots->begin(); 
          i != fPlots->end(); i++) {
         AddComputedTrace (*fPlots, pds, *i, fCal);
      }
   
      // add computed traces to pool
      for (vector<PlotDescriptor*>::iterator i = pds.begin();
          i != pds.end(); i++) {
         fPlots->Add (*i);
      }
   
      return kTRUE;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Output operators                                                     //
//                                                                      //
// Write XML data                                                       //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   ostream& operator<< (ostream& os, const ParameterDescriptor& prm)
   {
      int len = 0;
      if (prm.GetUser() && (len = strlen (prm.GetUser()))) {
         if (prm.GetUser()[len-1] == '\n') {
            os.write (prm.GetUser(), len - 1);
         }
         else {
            os << prm.GetUser();
         }
      }
      else {
         bool first = true;
         UInt_t sec;
         UInt_t nsec;
         if (prm.GetStartTime (sec, nsec)) {
            os << xsilTime ("t0", sec, nsec);
            first = false;
         }
         Int_t avg;
         if (prm.GetAverages (avg)) {
            if (first) first = false; 
            else os << endl;
            os << xsilParameter<int> ("Averages", avg);
         }
         string s;
         if (prm.GetThird(s)) {
            string::size_type pos = s.find ('=');
            if (pos != string::npos) {
               string name = s.substr (0, pos);
               string val = s.substr (pos + 1, s.size() - pos - 1);
               if (first) first = false; 
               else os << endl;
               if (val.find_first_not_of (" 0123456789.eE+-") == 
                  string::npos) {
                  if (val.find_first_of (".eE") == string::npos) {
                     int i = atoi (val.c_str());
                     os  << xsilParameter <int> (name.c_str(), i);
                  }
                  else {
                     double x = atof (val.c_str());
                     os  << xsilParameter <double> (name.c_str(), x);
                  }
               }
               else {
                  os  << xsilParameter <const char*> 
                     (name.c_str(), val.c_str());
               }
            }
         }
      }
      return os;
   }

//______________________________________________________________________________
   bool writePlotDescriptor (ostream& os, const PlotDescriptor& pd, 
                     int& resnum, int& refnum, bool compress)
   {
      // write data begin
      char name[256]; 
      char flag[256];
      const char* p;
      if ((strcmp (pd.GetGraphType(), kPTTimeSeries) == 0) &&
         ((p = strchr (pd.GetAChannel(), '[')) != 0) &&
         (strchr (p + 1, '[') != 0)) {
         sprintf (name, "%s", pd.GetAChannel());
         strcpy (flag, "TimeSeries");
      }
      else if ((p = strstr (pd.GetAChannel(), "(REF")) != 0) {
         refnum = atoi (p + 4);
         sprintf (name, "Reference[%i]", refnum);
         strcpy (flag, "Result");
      }
      else if (resnum >= 0) {
         sprintf (name, "Result[%i]", resnum);
         strcpy (flag, "Result");
         resnum++;
      }
      else {
         if (pd.GetBChannel()) {
            sprintf (name, "%s/%s", pd.GetBChannel(), pd.GetAChannel()); 
         }
         else {
            sprintf (name, "%s", pd.GetAChannel()); 
         }
         strcpy (flag, "Result");
      }
      xsilStd::DataType dtype;
      string type;
      int subtype;
      // Map the PlotDescriptor::fGraphType to the xsilStd::DataType.
      if (!xsilStd::GetDataType (pd.GetGraphType(), dtype, subtype)) {
         return false;
      }
      // Convert the xsilStd::DataType to a string (from IO/xml/Xsil.hh)
      type = xsilStd::Typename (dtype);
   
      // get data array, dimensions and subtype
      bool cmplx = false;
      int dim1 = 0;
      int dim2 = 0;
      const float* data = 0;
      const double* hdata = 0; // hist mito
      bool xydata;
      bool copy;
   
      if (strcmp (pd.GetGraphType(), kPTHistogram1) == 0) { // hist mito
         if (!xsilStd::GetDataInfo (pd.GetData(),
                              dim1, dim2, cmplx, xydata, &hdata, copy)) {
            return false;
         }
      }
      else {
         if (!xsilStd::GetDataInfo (pd.GetData(),
                              dim1, dim2, cmplx, xydata, &data, copy)) {
            return false;
         }
      }
   
      if (xydata) {
         subtype = xsilStd::DataSubtypeXY (dtype, subtype);
      }
   
      // histogram bin-error array ON ? 
      if (strcmp (pd.GetGraphType(), kPTHistogram1) == 0) {
         if (pd.GetData()->GetBinErrors()) subtype += 6;
      }
   
      // write data tag
      // Output <LIGO_LW Name="name" Type="type.c_str()">
      //          <Param Name="Flag" Type="string">flag</Param>
      os << xsilDataBegin (name, type.c_str(), flag) << endl;
      // Output <Param Name="Subtype" Type="stDataSubtype">subtype</Param>
      os << xsilParameter<int> (stDataSubtype, subtype) << endl;
   
      // write parameters
      os << pd.Param() << endl;
   
      // write data
      if (data == 0 && hdata == 0) { // hist mito
         os << xsilDataEnd<float> ();
      }
      
      // write histogram data 
      else if (strcmp (pd.GetGraphType(), kPTHistogram1) == 0) { // hist mito
      
         int offset = 0;
      
         if (xydata) {
         
            for (int i = 0; i < dim2 - 1; ++i) {// mito
               cout << hdata[i] << endl;
            }
            os << xsilArray<double> ("XBins", dim2 - 1, hdata) << endl;
            offset += dim2 - 1;
         }
      
         os << xsilArray<double> ("Contents", dim2, hdata + offset) << endl;
         offset += dim2;
      
         if ( subtype >= 6) {
            os << xsilArray<double> ("Errors", dim2, hdata + offset) << endl;
         }
      
         os << xsilDataEnd<double> ();
      }
      
      // write data other than histogram
      else if (!cmplx) {
         if (dim1 <= 1) {
            os << xsilDataEnd<float> (dim2, data);
         }
         else {
            os << xsilDataEnd<float> (dim1, dim2, data);
         }
      }
      else {
         if (dim1 <= 1) {
            os << xsilDataEnd<complex<float> > (dim2, 
                                 (complex<float>*)data);
         }
         else {
            os << xsilDataEnd<complex<float> > (dim1, dim2, 
                                 (complex<float>*)data);
         }
      }
      if (copy) {
         delete [] (char*) data;
      }
      return true;
   }

//______________________________________________________________________________
   ostream& operator<< (ostream& os, const PlotDescriptor& pd)
   {
      int resnum = -1;
      int refnum = 0;
      writePlotDescriptor (os, pd, resnum, refnum);
      return os;
   }

//______________________________________________________________________________
   ostream& operator<< (ostream& os, const PlotSet& pset)
   {
      // set dirty bit of plot descriptors
      for (PlotSet::const_iterator i = pset.begin(); i != pset.end(); i++) {
         i->SetMarked();
      }
      // write all dirty & not-caluclated descriptors
      int resnum = 0;
      int refnum = 0;
      bool first = true;
      for (PlotSet::const_iterator i = pset.begin(); i != pset.end(); i++) {
         // check dirty to avoid writing referenced objects multiple times
         if (i->IsCalculated() || !i->IsMarked()) {
            continue; 
         }
         if (first) first = false; 
         else os << endl;
         writePlotDescriptor (os, *i, resnum, refnum, true);
         i->SetMarked (kFALSE);
      }
      return os;
   }

//______________________________________________________________________________
   ostream& operator<< (ostream& os, const OptionArray& opts)
   {
      bool first = true;
      for (int i = 0; i < opts.GetMaxWin(); i++) {
         for (int j = 0; j < opts.GetMaxPad(); j++) {
            if (opts(i,j) == 0) {
               continue;
            }
            if (first) { 
               first = false;
            }
            else {
               os << endl;
            }
            char name[256];
            sprintf (name, "Plot[%i][%i]", i, j);
            os << xsilDataBegin (name, "Plot", xmlObjTypeSettings) << endl;
            os << xsilParameter<const char*> (stPlotName, opts(i,j)->fName) << endl;
            // traces
            os << xsilParameter<const char*> (stPlotTracesGraphType, 
                                 opts(i,j)->fTraces.fGraphType) << endl;
            for (int k = 0; k < kMaxTraces; k++) {
               char buf [256];
               sprintf (buf, "%s[%i]", stPlotTracesActive, k);
               os << xsilParameter<bool> (buf, opts(i,j)->fTraces.fActive[k]) << endl;
               sprintf (buf, "%s[%i]", stPlotTracesAChannel, k);
               os << xsilParameter<const char*> (buf, opts(i,j)->fTraces.fAChannel[k]) << endl;
               sprintf (buf, "%s[%i]", stPlotTracesBChannel, k);
               os << xsilParameter<const char*> (buf, opts(i,j)->fTraces.fBChannel[k]) << endl;
            }
            bool pb[kMaxTraces];
            int pi[kMaxTraces];
            double pd[kMaxTraces];
            for (int k = 0; k < kMaxTraces; k++) {
               pi[k] = opts(i,j)->fTraces.fPlotStyle[k];
            }
            os << xsilParameter<int> (stPlotTracesPlotStyle, pi[0], kMaxTraces) << endl;
            for (int k = 0; k < kMaxTraces; k++) {
               pi[k] = TPlotColorLookup::ColorType
                  (opts(i,j)->fTraces.fLineAttr[k].GetLineColor()).Compatibility();
               //pi[k] = opts(i,j)->fTraces.fLineAttr[k].GetLineColor();
            }
            os << xsilParameter<int> (stPlotTracesLineAttrColor, pi[0], kMaxTraces) << endl;
            for (int k = 0; k < kMaxTraces; k++) {
               pi[k] = opts(i,j)->fTraces.fLineAttr[k].GetLineStyle();
            }
            os << xsilParameter<int> (stPlotTracesLineAttrStyle, pi[0], kMaxTraces) << endl;
            for (int k = 0; k < kMaxTraces; k++) {
               pd[k] = opts(i,j)->fTraces.fLineAttr[k].GetLineWidth();
            }
            os << xsilParameter<double> (stPlotTracesLineAttrWidth, pd[0], kMaxTraces) << endl;
            for (int k = 0; k < kMaxTraces; k++) {
               pi[k] = TPlotColorLookup::ColorType
                  (opts(i,j)->fTraces.fMarkerAttr[k].GetMarkerColor()).Compatibility();
               //pi[k] = opts(i,j)->fTraces.fMarkerAttr[k].GetMarkerColor();
            }
            os << xsilParameter<int> (stPlotTracesMarkerAttrColor, pi[0], kMaxTraces) << endl;
            for (int k = 0; k < kMaxTraces; k++) {
               pi[k] = opts(i,j)->fTraces.fMarkerAttr[k].GetMarkerStyle();
            }
            os << xsilParameter<int> (stPlotTracesMarkerAttrStyle, pi[0], kMaxTraces) << endl;
            for (int k = 0; k < kMaxTraces; k++) {
               pd[k] = opts(i,j)->fTraces.fMarkerAttr[k].GetMarkerSize();
            }
            os << xsilParameter<double> (stPlotTracesMarkerAttrSize, pd[0], kMaxTraces) << endl;
            for (int k = 0; k < kMaxTraces; k++) {
               pi[k] = TPlotColorLookup::ColorType
                  (opts(i,j)->fTraces.fBarAttr[k].GetFillColor()).Compatibility();
               //pi[k] = opts(i,j)->fTraces.fBarAttr[k].GetFillColor();
            }
            os << xsilParameter<int> (stPlotTracesBarAttrColor, pi[0], kMaxTraces) << endl;
            for (int k = 0; k < kMaxTraces; k++) {
               pi[k] = opts(i,j)->fTraces.fBarAttr[k].GetFillStyle();
            }
            os << xsilParameter<int> (stPlotTracesBarAttrStyle, pi[0], kMaxTraces) << endl;
            for (int k = 0; k < kMaxTraces; k++) {
               pd[k] = opts(i,j)->fTraces.fBarWidth[k];
            }
            os << xsilParameter<double> (stPlotTracesBarAttrWidth, pd[0], kMaxTraces) << endl;
            // range
            pi[0] = opts(i,j)->fRange.fAxisScale[0];
            pi[1] = opts(i,j)->fRange.fAxisScale[1];
            os << xsilParameter<int> (stPlotRangeAxisScale, pi[0], 2) << endl;
            pi[0] = opts(i,j)->fRange.fRange[0];
            pi[1] = opts(i,j)->fRange.fRange[1];
            os << xsilParameter<int> (stPlotRangeRange, pi[0], 2) << endl;
            os << xsilParameter<double> (stPlotRangeRangeFrom, 
                                 opts(i,j)->fRange.fRangeFrom[0], 2) << endl;
            os << xsilParameter<double> (stPlotRangeRangeTo, 
                                 opts(i,j)->fRange.fRangeTo[0], 2) << endl;
            os << xsilParameter<int> (stPlotRangeBin, 
                                 opts(i,j)->fRange.fBin) << endl;
            os << xsilParameter<bool> (stPlotRangeBinLogSpacing, 
                                 opts(i,j)->fRange.fBinLogSpacing) << endl;
            // units
            os << xsilParameter<int> (stPlotUnitsXValues, 
                                 opts(i,j)->fUnits.fXValues) << endl;
            os << xsilParameter<int> (stPlotUnitsYValues, 
                                 opts(i,j)->fUnits.fYValues) << endl;
            os << xsilParameter<const char*> (stPlotUnitsXUnit, 
                                 opts(i,j)->fUnits.fXUnit) << endl;
            os << xsilParameter<const char*> (stPlotUnitsYUnit, 
                                 opts(i,j)->fUnits.fYUnit) << endl;
            os << xsilParameter<int> (stPlotUnitsXMag, 
                                 opts(i,j)->fUnits.fXMag) << endl;
            os << xsilParameter<int> (stPlotUnitsYMag, 
                                 opts(i,j)->fUnits.fYMag) << endl;
            os << xsilParameter<double> (stPlotUnitsXSlope, 
                                 opts(i,j)->fUnits.fXSlope) << endl;
            os << xsilParameter<double> (stPlotUnitsYSlope, 
                                 opts(i,j)->fUnits.fYSlope) << endl;
            os << xsilParameter<double> (stPlotUnitsXOffset, 
                                 opts(i,j)->fUnits.fXOffset) << endl;
            os << xsilParameter<double> (stPlotUnitsYOffset, 
                                 opts(i,j)->fUnits.fYOffset) << endl;
            // cursor
            pb[0] = opts(i,j)->fCursor.fActive[0];
            pb[1] = opts(i,j)->fCursor.fActive[1];
            os << xsilParameter<bool> (stPlotCursorActive, pb[0], 2) << endl;
            os << xsilParameter<int> (stPlotCursorTrace, 
                                 opts(i,j)->fCursor.fTrace) << endl;
            os << xsilParameter<int> (stPlotCursorStyle, 
                                 opts(i,j)->fCursor.fStyle) << endl;
            os << xsilParameter<int> (stPlotCursorType, 
                                 opts(i,j)->fCursor.fType) << endl;
            os << xsilParameter<double> (stPlotCursorX, 
                                 opts(i,j)->fCursor.fX[0], 2) << endl;
            os << xsilParameter<double> (stPlotCursorH, 
                                 opts(i,j)->fCursor.fH[0], 2) << endl;
            for (int k = 0; k < kMaxTraces; k++) {
               pb[k] = opts(i,j)->fCursor.fValid[k];
            }
            os << xsilParameter<bool> (stPlotCursorValid, pb[0], kMaxTraces) << endl;
            os << xsilParameter<double> (stPlotCursorY, 
                                 opts(i,j)->fCursor.fY[0][0], 2*kMaxTraces) << endl;
            os << xsilParameter<double> (stPlotCursorN, 
                                 opts(i,j)->fCursor.fN[0], kMaxTraces) << endl;
            os << xsilParameter<double> (stPlotCursorXDiff, 
                                 opts(i,j)->fCursor.fXDiff[0], kMaxTraces) << endl;
            os << xsilParameter<double> (stPlotCursorYDiff, 
                                 opts(i,j)->fCursor.fYDiff[0], kMaxTraces) << endl;
            os << xsilParameter<double> (stPlotCursorMean,
                                 opts(i,j)->fCursor.fMean[0], kMaxTraces) << endl;
            os << xsilParameter<double> (stPlotCursorRMS, 
                                 opts(i,j)->fCursor.fRMS[0], kMaxTraces) << endl;
            os << xsilParameter<double> (stPlotCursorStdDev,
                                 opts(i,j)->fCursor.fStdDev[0], kMaxTraces) << endl;
            os << xsilParameter<double> (stPlotCursorSum,
                                 opts(i,j)->fCursor.fSum[0], kMaxTraces) << endl;
            os << xsilParameter<double> (stPlotCursorSqrSum,
                                 opts(i,j)->fCursor.fSqrSum[0], kMaxTraces) << endl;
            os << xsilParameter<double> (stPlotCursorArea, 
                                 opts(i,j)->fCursor.fArea[0], kMaxTraces) << endl;
            os << xsilParameter<double> (stPlotCursorRMSArea, 
                                 opts(i,j)->fCursor.fRMSArea[0], kMaxTraces) << endl;
            os << xsilParameter<double> (stPlotCursorPeakX,
                                 opts(i,j)->fCursor.fPeakX[0], kMaxTraces) << endl;
            os << xsilParameter<double> (stPlotCursorPeakY,
                                 opts(i,j)->fCursor.fPeakY[0], kMaxTraces) << endl;
            os << xsilParameter<double> (stPlotCursorCenter,
                                 opts(i,j)->fCursor.fCenter[0], kMaxTraces) << endl; // hist mito
            os << xsilParameter<double> (stPlotCursorWidth,
                                 opts(i,j)->fCursor.fWidth[0], kMaxTraces) << endl; // hist mito
         
            // config
            os << xsilParameter<bool> (stPlotConfigAutoConfig, 
                                 opts(i,j)->fConfig.fAutoConf) << endl;
            os << xsilParameter<bool> (stPlotConfigRespectUser, 
                                 opts(i,j)->fConfig.fRespectUser) << endl;
            os << xsilParameter<bool> (stPlotConfigAutoAxes, 
                                 opts(i,j)->fConfig.fAutoAxes) << endl;
            os << xsilParameter<bool> (stPlotConfigAutoBin, 
                                 opts(i,j)->fConfig.fAutoBin) << endl;
            os << xsilParameter<bool> (stPlotConfigAutoTimeAdjust, 
                                 opts(i,j)->fConfig.fAutoTimeAdjust) << endl;
            // style
            os << xsilParameter<const char*> (stPlotStyleTitle, 
                                 opts(i,j)->fStyle.fTitle) << endl;
            os << xsilParameter<int> (stPlotStyleTitleAlign, 
                                 opts(i,j)->fStyle.fTitleAttr.GetTextAlign()) << endl;
            os << xsilParameter<double> (stPlotStyleTitleAngle, 
                                 opts(i,j)->fStyle.fTitleAttr.GetTextAngle()) << endl;
            os << xsilParameter<int> (stPlotStyleTitleColor,
                                 TPlotColorLookup::ColorType
                                 (opts(i,j)->fStyle.fTitleAttr.GetTextColor()).
                                 Compatibility()) << endl;
            // os << xsilParameter<int> (stPlotStyleTitleColor,
                                 // opts(i,j)->fStyle.fTitleAttr.GetTextColor()) << endl;
            os << xsilParameter<int> (stPlotStyleTitleFont,
                                 opts(i,j)->fStyle.fTitleAttr.GetTextFont()) << endl;
            os << xsilParameter<double> (stPlotStyleTitleSize, 
                                 opts(i,j)->fStyle.fTitleAttr.GetTextSize()) << endl;
            for (int k = 0; k < 4; k++) {
               pd[k] = opts(i,j)->fStyle.fMargin[k];
            }
            os << xsilParameter<double> (stPlotStyleMargin,pd[0], 4) << endl;
            // x axis
            os << xsilParameter<const char*> (stPlotAxisXTitle, 
                                 opts(i,j)->fAxisX.fAxisTitle) << endl;
            os << xsilParameter<int> (stPlotAxisXAxisAttrAxisColor, 
                                 TPlotColorLookup::ColorType
                                 (opts(i,j)->fAxisX.fAxisAttr.GetAxisColor()).
                                 Compatibility()) << endl;
            // os << xsilParameter<int> (stPlotAxisXAxisAttrAxisColor, 
                                 // opts(i,j)->fAxisX.fAxisAttr.GetAxisColor()) << endl;
            os << xsilParameter<int> (stPlotAxisXAxisAttrLabelColor,
                                 TPlotColorLookup::ColorType
                                 (opts(i,j)->fAxisX.fAxisAttr.GetLabelColor()).
                                 Compatibility()) << endl;
            // os << xsilParameter<int> (stPlotAxisXAxisAttrLabelColor, 
                                 // opts(i,j)->fAxisX.fAxisAttr.GetLabelColor()) << endl;
            os << xsilParameter<int> (stPlotAxisXAxisAttrLabelFont,
                                 opts(i,j)->fAxisX.fAxisAttr.GetLabelFont()) << endl;
            os << xsilParameter<double> (stPlotAxisXAxisAttrLabelOffset,
                                 opts(i,j)->fAxisX.fAxisAttr.GetLabelOffset()) << endl;
            os << xsilParameter<double> (stPlotAxisXAxisAttrLabelSize,
                                 opts(i,j)->fAxisX.fAxisAttr.GetLabelSize()) << endl;
            os << xsilParameter<int> (stPlotAxisXAxisAttrNdividions, 
                                 opts(i,j)->fAxisX.fAxisAttr.GetNdivisions()) << endl;
            os << xsilParameter<double> (stPlotAxisXAxisAttrTickLength,
                                 opts(i,j)->fAxisX.fAxisAttr.GetTickLength()) << endl;
            os << xsilParameter<double> (stPlotAxisXAxisAttrTitleOffset,
                                 opts(i,j)->fAxisX.fAxisAttr.GetTitleOffset()) << endl;
            os << xsilParameter<double> (stPlotAxisXAxisAttrTitleSize, 
                                 opts(i,j)->fAxisX.fAxisAttr.GetTitleSize()) << endl;
            os << xsilParameter<int> (stPlotAxisXAxisAttrTitleColor,
                                 TPlotColorLookup::ColorType
                                 (opts(i,j)->fAxisX.fAxisAttr.GetTitleColor()).
                                 Compatibility()) << endl;
            os << xsilParameter<bool> (stPlotAxisXGrid, 
                                 opts(i,j)->fAxisX.fGrid) << endl;
            os << xsilParameter<bool> (stPlotAxisXBothSides,
                                 opts(i,j)->fAxisX.fBothSides) << endl;
            os << xsilParameter<bool> (stPlotAxisXCenterTitle, 
                                 opts(i,j)->fAxisX.fCenterTitle) << endl;
            // y axis
            os << xsilParameter<const char*> (stPlotAxisYTitle, 
                                 opts(i,j)->fAxisY.fAxisTitle) << endl;
            os << xsilParameter<int> (stPlotAxisYAxisAttrAxisColor, 
                                 TPlotColorLookup::ColorType
                                 (opts(i,j)->fAxisY.fAxisAttr.GetAxisColor()).
                                 Compatibility()) << endl;
            // os << xsilParameter<int> (stPlotAxisYAxisAttrAxisColor, 
                                 // opts(i,j)->fAxisY.fAxisAttr.GetAxisColor()) << endl;
            os << xsilParameter<int> (stPlotAxisYAxisAttrLabelColor,
                                 TPlotColorLookup::ColorType
                                 (opts(i,j)->fAxisY.fAxisAttr.GetLabelColor()).
                                 Compatibility()) << endl;
            // os << xsilParameter<int> (stPlotAxisYAxisAttrLabelColor, 
                                 // opts(i,j)->fAxisY.fAxisAttr.GetLabelColor()) << endl;
            os << xsilParameter<int> (stPlotAxisYAxisAttrLabelFont,
                                 opts(i,j)->fAxisY.fAxisAttr.GetLabelFont()) << endl;
            os << xsilParameter<double> (stPlotAxisYAxisAttrLabelOffset,
                                 opts(i,j)->fAxisY.fAxisAttr.GetLabelOffset()) << endl;
            os << xsilParameter<double> (stPlotAxisYAxisAttrLabelSize,
                                 opts(i,j)->fAxisY.fAxisAttr.GetLabelSize()) << endl;
            os << xsilParameter<int> (stPlotAxisYAxisAttrNdividions, 
                                 opts(i,j)->fAxisY.fAxisAttr.GetNdivisions()) << endl;
            os << xsilParameter<double> (stPlotAxisYAxisAttrTickLength,
                                 opts(i,j)->fAxisY.fAxisAttr.GetTickLength()) << endl;
            os << xsilParameter<double> (stPlotAxisYAxisAttrTitleOffset,
                                 opts(i,j)->fAxisY.fAxisAttr.GetTitleOffset()) << endl;
            os << xsilParameter<double> (stPlotAxisYAxisAttrTitleSize, 
                                 opts(i,j)->fAxisY.fAxisAttr.GetTitleSize()) << endl;
            os << xsilParameter<int> (stPlotAxisYAxisAttrTitleColor,
                                 TPlotColorLookup::ColorType
                                 (opts(i,j)->fAxisY.fAxisAttr.GetTitleColor()).
                                 Compatibility()) << endl;
            os << xsilParameter<bool> (stPlotAxisYGrid, 
                                 opts(i,j)->fAxisY.fGrid) << endl;
            os << xsilParameter<bool> (stPlotAxisYBothSides,
                                 opts(i,j)->fAxisY.fBothSides) << endl;
            os << xsilParameter<bool> (stPlotAxisYCenterTitle, 
                                 opts(i,j)->fAxisY.fCenterTitle) << endl;
            // legend
            os << xsilParameter<bool> (stPlotLegendShow, 
                                 opts(i,j)->fLegend.fShow) << endl;
            os << xsilParameter<int> (stPlotLegendPlacement, 
                                 opts(i,j)->fLegend.fPlacement) << endl;
            os << xsilParameter<double> (stPlotLegendXAdjust, 
                                 opts(i,j)->fLegend.fXAdjust) << endl;
            os << xsilParameter<double> (stPlotLegendYAdjust, 
                                 opts(i,j)->fLegend.fYAdjust) << endl;
            os << xsilParameter<int> (stPlotLegendSymbolStyle, 
                                 opts(i,j)->fLegend.fSymbolStyle) << endl;
            os << xsilParameter<int> (stPlotLegendTextStyle, 
                                 opts(i,j)->fLegend.fTextStyle) << endl;
            os << xsilParameter<double> (stPlotLegendSize, 
                                 opts(i,j)->fLegend.fSize) << endl;
            for (int k = 0; k < kMaxTraces; k++) {
               char buf[256];
               sprintf (buf, "%s[%i]", stPlotLegendText, k);
               os << xsilParameter<const char*> (buf, opts(i,j)->fLegend.fText[k]) << endl;
            }
            // parameter
            os << xsilParameter<bool> (stPlotParamShow, 
                                 opts(i,j)->fParam.fShow) << endl;
            os << xsilParameter<bool> (stPlotParamT0, 
                                 opts(i,j)->fParam.fT0) << endl;
            os << xsilParameter<bool> (stPlotParamAvg, 
                                 opts(i,j)->fParam.fAvg) << endl;
            os << xsilParameter<bool> (stPlotParamSpecial, 
                                 opts(i,j)->fParam.fSpecial) << endl;
            os << xsilParameter<bool> (stPlotParamStat, 
                                 opts(i,j)->fParam.fStat) << endl; // hist mito
            os << xsilParameter<bool> (stPlotParamUOBins, 
                                 opts(i,j)->fParam.fUOBins) << endl; // hist mito 8/8/2002						
            os << xsilParameter<bool> (stPlotParamTimeFormatUTC, 
                                 opts(i,j)->fParam.fTimeFormatUTC) << endl;
            os << xsilParameter<double> (stPlotParamTextSize, 
                                 opts(i,j)->fParam.fTextSize) << endl;
            os << xsilDataEnd<float> ();
         }
      }
      return os;
   }

//______________________________________________________________________________
   ostream& operator<< (ostream& os, const ReferenceTraceList_t& rl)
   {
      return os;
   }

//______________________________________________________________________________
   ostream& operator<< (ostream& os, const calibration::Table& ct)
   {
      const int size = 16384;
      char*	buf = new (nothrow) char [size];
      if (buf == 0) {
         return os;
      }
   
      for (int i = 0; i < ct.Len(); i++) {
         string s;
         if (ct[i].Xml (CALNORMAL, s, i)) {
         
            if (!s.empty() && (i + 1 == ct.Len()) && 
               (s[s.size()-1] == '\n')) {
               s.erase (s.size()-1);
            }
            os << s;
         }
      }
      return os;
   }

//______________________________________________________________________________
   ostream& operator<< (ostream& os, const MathTable_t& mt)
   {
      return os;
   }


}
