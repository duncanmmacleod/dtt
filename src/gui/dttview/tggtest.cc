/* Version $Id: tggtest.cc 6319 2010-09-17 18:06:52Z james.batch@LIGO.ORG $ */

#include <time.h>
#include <iostream>
#include <cmath>
#include <TROOT.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TEnv.h>
#include <TGraph.h>
#include "PlotSet.hh"
#include "TLGOptions.hh"
#include "TLGPlot.hh"
#include "TLGPad.hh"


   using namespace ligogui;
   using namespace std;


   class MainMainFrame : public TGMainFrame {
   private:
      OptionAll_t	 fOptions;
      PlotSet		 fPlots;
      TGCompositeFrame*	 fTextFrame;
      TGLayoutHints*     fL1;
      TGButton*		 fExitButton;
      TLGOptionTab*	 f3;
      TLGPad*		 fPad;
      TLGMultiPad*	 fMPad;
   
   public:
      MainMainFrame (const TGWindow *p, UInt_t w, UInt_t h);
      virtual ~MainMainFrame();
   
      virtual void CloseWindow();
      virtual Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t);
   };


   MainMainFrame::MainMainFrame (const TGWindow *p, UInt_t w, UInt_t h)
   : TGMainFrame (p, w, h), fPad (0)
   {
   /*------------------------------------------------------------------------*/
   /* Set font for label text.                                               */
   /*------------------------------------------------------------------------*/
   
      FontStruct_t labelfont;
      labelfont = gClient->GetFontByName 
         (gEnv->GetValue
         ("Gui.NormalFont",
         "-adobe-helvetica-medium-r-*-*-12-*-*-*-*-*-iso8859-1"));
   
   /* Define new graphics context. Only the fields specified in
      fMask will be used. */
   
      GCValues_t   gval;
      gval.fMask = kGCForeground | kGCFont;
      gval.fFont = gVirtualX->GetFontHandle(labelfont);
   
      fL1 = new TGLayoutHints (kLHintsExpandX | kLHintsExpandY, 2, 2, 5, 5);
   
      // prepare graphics data
      int i;
      Float_t x[100], y[100];
      Int_t n = 20;
      for (i = 0; i < n; i++) {
         x[i] = i * 0.1;
         y[i] = 10 * sin (x[i]+ 0.2);
      }
      fPlots.Add (new DataCopy (x, y, n), "Time series", "H1:LSC-TEST_1");
      // TGraph* gr = new TGraph (n, x, y);
      // gr->Draw ("AC*");
      // fMPad->SetActivePad (1);
      Float_t x2[100], y2[100];
      Int_t n2 = 20;
      for (i = 0; i < n2; i++) {
         x2[i] = i * 0.1;
         y2[i] = 15 * cos (x2[i]+ 0.2);
      }
      fPlots.Add (new DataCopy (x2, y2, n2), "Time series", "H1:LSC-TEST_2");
      fPlots.Add (new DataCopy (x2, y2, n2), "Coherence", "H1:LSC-TEST_2");
      Double_t hx[11],hy[12],hs[4];
      Int_t hn = 10;
      for (i = 0; i < hn+1; i++) hx[i] = (Double_t)i;
      for (i = 0; i < hn+2; i++) {
         hy[i] = (Double_t)i;
         hs[2] += (Double_t)i;
         hs[3] += (Double_t)i*i;
      }
      hs[0]=hs[1]=12;
      fPlots.Add(new HistDataCopy(hx,hy,hn,"N Sigma","Count",hn,hs,0),"1-d Histogram","H1:LSC-TEST_3");
      Double_t hx2[21],hy2[22],hs2[4];
      Int_t hn2 = 20;
      for (i = 0; i < hn2+1; i++) hx2[i] = (Double_t)i*0.5;
      for (i = 0; i < hn2+2; i++) {
         hy2[i] = (Double_t)i;
         hs2[2] += (Double_t)i;
         hs2[3] += (Double_t)i*i;
      }
      hs2[0]=hs2[1]=22;
      fPlots.Add(new HistDataCopy(hx2,hy2,hn2,"N Sigma","Count",hn2,hs2,0),"1-d Histogram","H1:LSC-TEST_4");
   
      // TGraph* gr2 = new TGraph (n2, x2, y2);
      // gr2->Draw ("ALP*");
      // fMPad->SetActivePad (0);
   
      // create new pads
      fMPad = new TLGMultiPad (this, "Plot", fPlots, 1, 2);
      fMPad->Associate(this);
      fMPad->AddButton (new TGTextButton (fMPad, "Exit", 1));
      //fPad->LeftPanel (kFALSE);
      //fPad->EnablePanelDialog (kFALSE);
      AddFrame (fMPad, fL1);
      fMPad->SetActivePad (0);
   
      // fL1 = new TGLayoutHints (kLHintsCenterX | kLHintsTop, 2, 2, 5, 5);
      // fExitButton = new TGTextButton (this, "    E&xit    ", 1);
      // fExitButton->Associate (this);
      // AddFrame (fExitButton, fL1);
   
      SetWindowName ("TLG Test");
      SetIconName ("TLG Test");
      SetClassHints ("TLG Test", "TLG Test");
      SetWMPosition (0,0);
      MapSubwindows ();
   
      // we need to use GetDefault...() to initialize the layout algorithm...
      Resize (GetDefaultSize());
      MapWindow ();
   
      // Plot (new DataCopy (x2, y2, n2), "Time series", "H1:LSC-TEST_2");
      Plot(new HistDataCopy(hx,hy,hn,"N Sigma","Count",hn,hs,0),"1-d Histogram","H1:LSC-TEST_3");
   }


   MainMainFrame::~MainMainFrame()
   {
      delete fTextFrame;
      delete fExitButton;
      delete fL1;
   }


   Bool_t MainMainFrame::ProcessMessage (Long_t msg, Long_t parm1, Long_t)
   {
      switch (GET_MSG (msg)) {
         case kC_COMMAND:
            {
               switch (GET_SUBMSG (msg)) {
                  case kCM_BUTTON:
                     {
                        switch (parm1) {
                           case 1:
                              CloseWindow();
                              break;
                        }
                     }
                  case kCM_MENU: 
                     {
                     }
               }
               break;
            }
      }
      return kTRUE;
   }


   void MainMainFrame::CloseWindow()
   {
   /* Got close message for this MainFrame. Calls parent CloseWindow()
    (which destroys the window) and terminate the application.
    The close message is generated by the window manager when its close
    window menu item is selected. */
   
      TGMainFrame::CloseWindow();
      gApplication->Terminate(0);
   }


   TROOT root("GUI", "TLG Test");



   int main(int argc, char **argv)
   {
      TApplication theApp ("Diagnostics tests", &argc, argv);
      MainMainFrame mainWindow (gClient->GetRoot(), 600, 240);
      theApp.Run();
   
      return 0;
   }
