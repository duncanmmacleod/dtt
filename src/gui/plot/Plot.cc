#include <time.h>
#include <stdio.h>
#include <string.h>
#include "Plot.hh"
#include "DataDescTraits.hh"
#include "TLGPad.hh"
#include "SweptSine.hh"
#include "SweptSineDlg.hh"

   using namespace std;
   using namespace ligogui;

//______________________________________________________________________________
extern "C" {
   // These functions are needed by the FilterDesign class
   ligogui::TLGMultiPad* bodeplot___dynamic (const float*, 
                     const float*, int, const char*);
   ligogui::TLGMultiPad* tsplot___dynamic (const TSeries&);
}

//______________________________________________________________________________
   SweptSine* gSweptSineParam = 0;

   SweptSine& GetSweptSineParam() 
   {
      if (!gSweptSineParam) gSweptSineParam = new SweptSine;
      return *gSweptSineParam;
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// PlotListAdd                                                          //
//                                                                      //
// Add a container to a plot list                                       //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
template <class container>
   void PlotListAdd (PlotList& plist, const container& cntr) 
   {
      static int unique = 0;
   
      if (plist.Size() < kMaxPlotList) {
         PlotDescriptor* pl = GetPlotDescriptor (cntr);
         if (pl) {
            // make sure we have an A channel name
            if (strlen (pl->GetAChannel()) == 0) {
               char buf[1024];
               sprintf (buf, "%s %i", pl->GetGraphType(), ++unique);
               pl->SetAChannel (buf);
            }
            plist.Add (pl);
         }
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Plot                                                                 //
//                                                                      //
// Plotting routines                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGMultiPad* Plot (const TSeries& t1, const TSeries& t2, 
                     const TSeries& t3, const TSeries& t4, 
                     const TSeries& t5, const TSeries& t6, 
                     const TSeries& t7, const TSeries& t8)
   {
      PlotList plist;
      PlotListAdd (plist, t1);
      PlotListAdd (plist, t2);
      PlotListAdd (plist, t3);
      PlotListAdd (plist, t4);
      PlotListAdd (plist, t5);
      PlotListAdd (plist, t6);
      PlotListAdd (plist, t7);
      PlotListAdd (plist, t8);
      if (plist.Size() <= 0) {
         return 0;
      }
      else {
         return Plot (plist, kPTTimeSeries);
      }
   }

//______________________________________________________________________________
   TLGMultiPad* Plot (const FSeries& f1, const FSeries& f2, 
                     const FSeries& f3, const FSeries& f4, 
                     const FSeries& f5, const FSeries& f6, 
                     const FSeries& f7, const FSeries& f8)
   {
      PlotList plist;
      PlotListAdd (plist, f1);
      PlotListAdd (plist, f2);
      PlotListAdd (plist, f3);
      PlotListAdd (plist, f4);
      PlotListAdd (plist, f5);
      PlotListAdd (plist, f6);
      PlotListAdd (plist, f7);
      PlotListAdd (plist, f8);
      if (plist.Size() <= 0) {
         return 0;
      }
      else {
         return Plot (plist, kPTFrequencySeries);
      }
   }

//______________________________________________________________________________
   TLGMultiPad* Plot (const FSpectrum& f1, const FSpectrum& f2, 
                     const FSpectrum& f3, const FSpectrum& f4, 
                     const FSpectrum& f5, const FSpectrum& f6, 
                     const FSpectrum& f7, const FSpectrum& f8)
   {
      PlotList plist;
      PlotListAdd (plist, f1);
      PlotListAdd (plist, f2);
      PlotListAdd (plist, f3);
      PlotListAdd (plist, f4);
      PlotListAdd (plist, f5);
      PlotListAdd (plist, f6);
      PlotListAdd (plist, f7);
      PlotListAdd (plist, f8);
      if (plist.Size() <= 0) {
         return 0;
      }
      else {
         return Plot (plist, kPTPowerSpectrum);
      }
   }

//______________________________________________________________________________
   TLGMultiPad* Plot (const Histogram1& h1, const Histogram1& h2, 
                     const Histogram1& h3, const Histogram1& h4, 
                     const Histogram1& h5, const Histogram1& h6, 
                     const Histogram1& h7, const Histogram1& h8)
   {
      PlotList plist;
      PlotListAdd (plist, h1);
      PlotListAdd (plist, h2);
      PlotListAdd (plist, h3);
      PlotListAdd (plist, h4);
      PlotListAdd (plist, h5);
      PlotListAdd (plist, h6);
      PlotListAdd (plist, h7);
      PlotListAdd (plist, h8);
      if (plist.Size() <= 0) {
         return 0;
      }
      else {
         return Plot (plist, kPTHistogram1);
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// BodePlot                                                             //
//                                                                      //
// Make a bode plot                                                     //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool AddBodePlot (PlotList& plist,
                    const float* f, const float* tf, int n,
                    const char* name)
   {
      // check if there is space in the plot list
      if (plist.Size() >= kMaxPlotList) {
         return false;
      }
      // make data array for transfer function
      int N = n;
      DataDescriptor* tfunc = 
         new (std::nothrow) DataCopy ((float*)f, (float*)tf, N, true);
      if (!tfunc) {
         return false;
      }
      // set channel name
      char def[128];
      sprintf (def, "filter%i", plist.Size());
      string achn (name ? name : def);
      achn += "_in";
      string bchn (name ? name : def);
      bchn += "_out";
      // Add plot descriptor to plot list
      PlotDescriptor* pd = new PlotDescriptor (tfunc, kPTTransferFunction, 
                           achn.c_str(), bchn.c_str());
      plist.Add (pd);
      return true;
   }

//______________________________________________________________________________
   bool AddBodePlot (PlotList& plist,
                    const Pipe& f1, const char* name1,
                    const SweptSine& ss)
   {
      // check if there is space in the plot list
      if (plist.Size() >= kMaxPlotList) {
         return false;
      }
      // make data array for transfer function
      int N = ss.GetPoints();
      DataDescriptor* tfunc = 
         new (std::nothrow) DataCopy (0, 0, N, true);
      if (!tfunc) {
         return false;
      }
      // compute swpet sine response
      if (!ss.Sweep (f1, tfunc->GetX(), (fComplex*)tfunc->GetY())) {
         delete tfunc;
         return false;
      }
      // set channel name
      char def[128];
      sprintf (def, "filter%i", plist.Size());
      string achn (name1 ? name1 : def);
      achn += "_in";
      string bchn (name1 ? name1 : def);
      bchn += "_out";
      // Add plot descriptor to plot list
      PlotDescriptor* pd = new PlotDescriptor (tfunc, kPTTransferFunction, 
                           achn.c_str(), bchn.c_str());
      plist.Add (pd); 
      return true;
   }

//______________________________________________________________________________
   ligogui::TLGMultiPad* BodePlot (const Pipe& f1, const char* name1,
                     const SweptSine& ss)
   {
      PlotList plist;
      if (AddBodePlot (plist, f1, name1, ss)) {
         return ligogui::BodePlot (plist);
      }
      else {
         for (int i = 0; i < plist.Size(); ++i) {
            delete plist(i);
         }
         return 0;
      }
   }

//______________________________________________________________________________
   ligogui::TLGMultiPad* BodePlot (const Pipe& f1, const SweptSine& ss)
   {
      return BodePlot (f1, "filter", ss);
   }

//______________________________________________________________________________
   ligogui::TLGMultiPad* BodePlot (const Pipe& f1, const char* name1,
                     const Pipe& f2, const char* name2,
                     const SweptSine& ss)
   {
      PlotList plist;
      if (AddBodePlot (plist, f1, name1, ss) &&
         AddBodePlot (plist, f2, name2, ss)) {
         return ligogui::BodePlot (plist);
      }
      else {
         for (int i = 0; i < plist.Size(); ++i) {
            delete plist(i);
         }
         return 0;
      }
   }

//______________________________________________________________________________
   ligogui::TLGMultiPad* BodePlot (const Pipe& f1, const char* name1,
                     const Pipe& f2, const char* name2,
                     const Pipe& f3, const char* name3,
                     const SweptSine& ss)
   {
      PlotList plist;
      if (AddBodePlot (plist, f1, name1, ss) &&
         AddBodePlot (plist, f2, name2, ss) &&
         AddBodePlot (plist, f3, name3, ss)) {
         return ligogui::BodePlot (plist);
      }
      else {
         for (int i = 0; i < plist.Size(); ++i) {
            delete plist(i);
         }
         return 0;
      }
   }

//______________________________________________________________________________
   ligogui::TLGMultiPad* BodePlot (const Pipe& f1, const char* name1,
                     const Pipe& f2, const char* name2,
                     const Pipe& f3, const char* name3,
                     const Pipe& f4, const char* name4,
                     const SweptSine& ss)
   {
      PlotList plist;
      if (AddBodePlot (plist, f1, name1, ss) &&
         AddBodePlot (plist, f2, name2, ss) &&
         AddBodePlot (plist, f3, name3, ss) &&
         AddBodePlot (plist, f4, name4, ss)) {
         return ligogui::BodePlot (plist);
      }
      else {
         for (int i = 0; i < plist.Size(); ++i) {
            delete plist(i);
         }
         return 0;
      }
   }

//______________________________________________________________________________
   ligogui::TLGMultiPad* BodePlot (const Pipe& f1, const char* name1,
                     const Pipe& f2, const char* name2,
                     const Pipe& f3, const char* name3,
                     const Pipe& f4, const char* name4,
                     const Pipe& f5, const char* name5,
                     const SweptSine& ss)
   {
      PlotList plist;
      if (AddBodePlot (plist, f1, name1, ss) &&
         AddBodePlot (plist, f2, name2, ss) &&
         AddBodePlot (plist, f3, name3, ss) &&
         AddBodePlot (plist, f4, name4, ss) &&
         AddBodePlot (plist, f5, name5, ss)) {
         return ligogui::BodePlot (plist);
      }
      else {
         for (int i = 0; i < plist.Size(); ++i) {
            delete plist(i);
         }
         return 0;
      }
   }

//______________________________________________________________________________
   ligogui::TLGMultiPad* BodePlot (const Pipe& f1, const char* name1)
   {
      SweptSine prm = GetSweptSineParam();
      Bool_t ret = kFALSE;
      new TLGSweptSineDialog (gClient->GetRoot(), gClient->GetRoot(),
                           prm, ret);
      if (!ret) {
         return 0;
      }
      else {
         ligogui::TLGMultiPad* pad = BodePlot (f1, name1, prm);
         GetSweptSineParam() = prm;
         return pad;
      }
   }

//______________________________________________________________________________
   ligogui::TLGMultiPad* BodePlot (const Pipe& f1, const char* name1,
                     const Pipe& f2, const char* name2)
   {
      SweptSine prm = GetSweptSineParam();
      Bool_t ret = kFALSE;
      new TLGSweptSineDialog (gClient->GetRoot(), gClient->GetRoot(),
                           prm, ret);
      if (!ret) {
         return 0;
      }
      else {
         ligogui::TLGMultiPad* pad = 
            BodePlot (f1, name1, f2, name2, prm);
         GetSweptSineParam() = prm;
         return pad;
      }
   }

//______________________________________________________________________________
   ligogui::TLGMultiPad* BodePlot (const Pipe& f1, const char* name1,
                     const Pipe& f2, const char* name2,
                     const Pipe& f3, const char* name3)
   {
      SweptSine prm = GetSweptSineParam();
      Bool_t ret = kFALSE;
      new TLGSweptSineDialog (gClient->GetRoot(), gClient->GetRoot(),
                           prm, ret);
      if (!ret) {
         return 0;
      }
      else {
         ligogui::TLGMultiPad* pad = 
            BodePlot (f1, name1, f2, name2, f3, name3, prm);
         GetSweptSineParam() = prm;
         return pad;
      }
   }

//______________________________________________________________________________
   ligogui::TLGMultiPad* BodePlot (const Pipe& f1, const char* name1,
                     const Pipe& f2, const char* name2,
                     const Pipe& f3, const char* name3,
                     const Pipe& f4, const char* name4)
   {
      SweptSine prm = GetSweptSineParam();
      Bool_t ret = kFALSE;
      new TLGSweptSineDialog (gClient->GetRoot(), gClient->GetRoot(),
                           prm, ret);
      if (!ret) {
         return 0;
      }
      else {
         ligogui::TLGMultiPad* pad = 
            BodePlot (f1, name1, f2, name2, f3, name3,
                     f4, name4, prm);
         GetSweptSineParam() = prm;
         return pad;
      }
   }

//______________________________________________________________________________
   ligogui::TLGMultiPad* BodePlot (const Pipe& f1, const char* name1,
                     const Pipe& f2, const char* name2,
                     const Pipe& f3, const char* name3,
                     const Pipe& f4, const char* name4,
                     const Pipe& f5, const char* name5)
   {
      SweptSine prm = GetSweptSineParam();
      Bool_t ret = kFALSE;
      new TLGSweptSineDialog (gClient->GetRoot(), gClient->GetRoot(),
                           prm, ret);
      if (!ret) {
         return 0;
      }
      else {
         ligogui::TLGMultiPad* pad = 
            BodePlot (f1, name1, f2, name2, f3, name3, 
                     f4, name4, f5, name5, prm);
         GetSweptSineParam() = prm;
         return pad;
      }
   }

//______________________________________________________________________________
   TLGMultiPad* Plot (float x0, float dx, float y[], int n,
                     const char* graphtype, 
                     const char* Achn, const char* Bchn)
   { 
      return 0; }
//______________________________________________________________________________
   TLGMultiPad* Plot (double x0, double dx, double y[], int n,
                     const char* graphtype, 
                     const char* Achn, const char* Bchn)
   { 
      return 0; }
//______________________________________________________________________________
   TLGMultiPad* Plot (std::complex<float> x0, 
                     std::complex<float> dx, 
                     std::complex<float> y[], int n,
                     const char* graphtype, 
                     const char* Achn, const char* Bchn)
   { 
      return 0; }
//______________________________________________________________________________
   TLGMultiPad* Plot (std::complex<double> x0, 
                     std::complex<double> dx, 
                     std::complex<double> y[], int n,
                     const char* graphtype, 
                     const char* Achn, const char* Bchn)
   { 
      return 0; }
//______________________________________________________________________________
   TLGMultiPad* Plot (float x[], float y[], int n,
                     const char* graphtype, 
                     const char* Achn, const char* Bchn)
   { 
      return 0; }
//______________________________________________________________________________
   TLGMultiPad* Plot (double x[], double y[], int n,
                     const char* graphtype, 
                     const char* Achn, const char* Bchn)
   { 
      return 0; }
//______________________________________________________________________________
   TLGMultiPad* Plot (std::complex<float> x[], 
                     std::complex<float> y[], int n,
                     const char* graphtype, 
                     const char* Achn, const char* Bchn)
   { 
      return 0; }
//______________________________________________________________________________
   TLGMultiPad* Plot (std::complex<double> x[], 
                     std::complex<double> y[], int n,
                     const char* graphtype, 
                     const char* Achn, const char* Bchn)
   { 
      return 0; }		     		     		     


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Extern                                                               //
//                                                                      //
// These functions are needed by the FilterDesign class			//
//                                                                      //
//////////////////////////////////////////////////////////////////////////
extern "C" {
   ligogui::TLGMultiPad* bodeplot___dynamic (const float* f, 
                     const float* tf, int n, const char* name)
   {
      if (!gClient) {
         return 0;
      }
      PlotList plist;
      if (AddBodePlot (plist, f, tf, n, name)) {
         return ligogui::BodePlot (plist);
      }
      else {
         for (int i = 0; i < plist.Size(); ++i) {
            delete plist(i);
         }
         return 0;
      }
   }

   ligogui::TLGMultiPad* tsplot___dynamic (const TSeries& ts)
   {
      if (!gClient) {
         return 0;
      }
      return Plot (ts);
   }

}

