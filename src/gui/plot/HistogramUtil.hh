#ifndef HISTOGRAMFUNC_HH
#define HISTOGRAMFUNC_HH

#include "TH1.h"
#include "TH2.h"
#include "Histogram1.hh"
#include "Histogram2.hh"

//namespace events {

/** @name Histogram Conversion and Plot functions.
  * This section contains functions to convert and plot histograms.
  * @memo functions to convert and plot histograms.
  * @author Masahiro Ito
  * @version 1.0; Last modified Jan. 12, 2002
  */
//@{

/** Histogram plot and conversion functions.
    @memo Histogram plot and conversion functions.
    @author Written December 2001 by Masahiro Ito
******************************************************************/

/** Convert DMT histogram (Histogram1) to ROOT histogram (TH1D)
    @memo Convert DMT histogram (Histogram1) to ROOT histogram (TH1D)
    @param h DMT histogram (Histogram1)
    @param rh ROOT histogram (TH1D)
 ******************************************************************/
   void ConvertHistogram(const Histogram1& h, TH1D& rh);

/** Convert DMT histogram (Histogram2) to ROOT histogram (TH2D)
    @memo Convert DMT histogram (Histogram2) to ROOT histogram (TH2D)
    @param h DMT histogram (Histogram2)
    @param rh ROOT histogram (TH2D)
 ******************************************************************/
   void ConvertHistogram(const Histogram2& h, TH2D& rh);

/** Convert ROOT histogram (TH1D) to DMT histogram (Histogram1)
    @memo Convert ROOT histogram (TH1D) to DMT histogram (Histogram1)
    @param rh ROOT histogram (TH1D)
    @param h DMT histogram (Histogram1)
 ******************************************************************/
   void ConvertHistogram(TH1D& rh, Histogram1& h);

/** Convert ROOT histogram (TH2D) to DMT histogram (Histogram2)
    @memo Convert ROOT histogram (TH2D) to DMT histogram (Histogram2)
    @param rh ROOT histogram (TH2D)
    @param h DMT histogram (Histogram2)
 ******************************************************************/
   void ConvertHistogram(TH2D& rh, Histogram2& h);

/** Plot 1-D histogram (TLGPad)
    @memo Plot 1-D histogram (TLGPad)
    @param h DMT histogram (Histogram1)
 ******************************************************************/
   void PlotHistogram(const Histogram1& h);

/** Plot 1-D histogram (ROOT)
    @memo Plot 1-D histogram (ROOT)
    @param h DMT histogram (Histogram1)
    @param opt ROOT histogram plot option
 ******************************************************************/
   void PlotRootHistogram(const Histogram1& h, const char* opt = "");

/** Plot 2-D histogram (ROOT)
    @memo Plot 2-D histogram (ROOT)
    @param h DMT histogram (Histogram2)
    @param opt ROOT histogram plot option
 ******************************************************************/
   void PlotRootHistogram(const Histogram2& h, const char* opt = "");

//@}

//}

#endif
