#ifndef _LIGO_SWEPTSINEDLG_H
#define _LIGO_SWEPTSINEDLG_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: SweptSineDlg						*/
/*                                                         		*/
/* Module Description: Dialog box for selecting swept sine parameters	*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 20Jul02  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: SweptSineDlg.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include <TGFrame.h>
#include <TGLabel.h>
#include <TGButton.h>
#include <TGComboBox.h>
#include <TGTextEntry.h>
#include "SweptSine.hh"
#include "TLGFrame.hh"


namespace ligogui {
   class TLGNumericControlBox;
}

/** Dialog box to select the parameters of a swept sine.
    Use as follows:
    \begin{verbatim}
    SweptSine param;
    Bool_t ret;
    new TLGSweptSineDialog (gClient->GetRoot(), gClient->GetRoot(),
                            param, ret);
    if (ret) {
       ...
    }
    \end{verbatim}
   
    @memo Swept sine dialog box
    @author Written July 2002 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGSweptSineDialog : public ligogui::TLGTransientFrame {
   protected:
      /// Swept sine parameters
      SweptSine*	fParam;
      /// Ok
      Bool_t*		fOk;
   
      /// Frames
      TGCompositeFrame* fF[6];
      /// Groups
      TGGroupFrame* 	fGroup[2];
      /// Layout hints
      TGLayoutHints*	fL[8];
      /// Labels
      TGLabel*		fLabel[9];
      /// Buttons
      TGButton*		fButton[2];
      /// Sampling frequency
      ligogui::TLGNumericControlBox* fSample;
      /// Swept sine start frequency
      ligogui::TLGNumericControlBox* fStart;
      /// Swept sine stop frequency
      ligogui::TLGNumericControlBox* fStop;
      /// Swept sine points
      ligogui::TLGNumericControlBox* fPoints;
      /// Swept sine settling time
      ligogui::TLGNumericControlBox* fSettling;
      /// Swept sine measurement time selection
      TGButton*			     fMeasTimeSel[2];
      /// Swept sine measurement time selection
      ligogui::TLGNumericControlBox* fMeasTime[2];
      /// Swept sine sweep type
      TGButton*		fSweepType[2];
      /// Swept sine window
      TGComboBox*	fWindow;
   
   public:
      /// Constructor
      TLGSweptSineDialog (const TGWindow *p, const TGWindow *main,
                        SweptSine& prm, Bool_t& ret);
      /// Destructor
      virtual ~TLGSweptSineDialog ();
      /// Close window
      virtual void CloseWindow();
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, 
                        Long_t parm2);
   };


#endif // _LIGO_SWEPTSINEDLG_H
