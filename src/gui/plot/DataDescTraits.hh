#ifndef _LIGO_DATADESCRIPTOR_TRAITS_H
#define _LIGO_DATADESCRIPTOR_TRAITS_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: Plot							*/
/*                                                         		*/
/* Module Description: High level plotting tools for signal analysis:	*/
/*		       Plot - general plotting routine			*/
/*		       BodePlot, TSPlot, PSPlot - specialized routines	*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 20Nov99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGPlot.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include "PlotSet.hh"
#include "calibration/Descriptor.hh"
#include "TSeries.hh"
#include "FSeries.hh"
#include "FSpectrum.hh"
#include "DVector.hh"
#include "Histogram1.hh"


namespace ligogui {

   bool GetParameterDescriptor (const TSeries& cntr,
                     ParameterDescriptor& prm);
   bool GetParameterDescriptor (const FSeries& cntr,
                     ParameterDescriptor& prm);
   bool GetParameterDescriptor (const FSpectrum& cntr,
                     ParameterDescriptor& prm);
   bool GetParameterDescriptor (const Histogram1& cntr,
                     ParameterDescriptor& prm);

   bool GetCalibrationDescriptor (const TSeries& cntr,
                     calibration::Descriptor& cal);
   bool GetCalibrationDescriptor (const FSeries& cntr,
                     calibration::Descriptor& cal);
   bool GetCalibrationDescriptor (const FSpectrum& cntr,
                     calibration::Descriptor& cal);
   bool GetCalibrationDescriptor (const Histogram1& cntr,
                     calibration::Descriptor& cal);


/** @name Trait classes for generating data descriptors

    @memo Container traits
    @author Written November 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

/** @name Basic traits class
    Traits class for a DMT container.
    @memo Basic traits class
    @author Written November 2001 by Daniel Sigg
 ************************************************************************/
template <class container>
   struct container_traits {
      /// Container type
      typedef container container_type;
      /// Contains data?
      static bool HasData (const container& cntr) {
         return false; }
      /// X start value
      static float x0 (const container& cntr) {
         return 0; }
      /// X spacing
      static float dx (const container& cntr) {
         return 1; }
      /// Is complex?
      static bool isCmplx (const container& cntr) {
         return false; }
      /// Get A channel name
      static const char* getAName (const container& cntr) {
         return ""; }
      /// Get B channel name
      static const char* getBName (const container& cntr) {
         return 0; }
      /// Get graph type
      static const char* graphType () {
         return ""; }
      /// post processing
      static void postprocess (float y[], int n) {
      }
      /// Get parameter descriptor
      static ParameterDescriptor* getPrm (const container& cntr,
                        ParameterDescriptor& prm) {
         prm = ParameterDescriptor();
         return &prm; }
      /// Get calibration descriptor
      static calibration::Descriptor* getCal (const container& cntr,
                        calibration::Descriptor& cal) {
         cal.Init();
         return &cal; }
   };


/** @name Traits class specialization for TSeries
    Traits class for TSeries.
    @memo TSeries traits class
    @author Written November 2001 by Daniel Sigg
 ************************************************************************/
template<>
   struct container_traits<TSeries> {
      /// Container type
      typedef TSeries container_type;
      /// Contains data?
      static bool HasData (const TSeries& ser) {
         return ser.refData() != 0; }
      /// X start value
      static float x0 (const TSeries& ser) {
         return 0; }
      /// X spacing
      static float dx (const TSeries& ser) {
         return ser.getTStep(); }
      /// Is complex?
      static bool isCmplx (const TSeries& ser) {
         return ser.getF0() != 0; }
      /// Get A channel name
      static const char* getAName (const TSeries& ser) {
         return ser.getName(); }
      /// Get B channel name
      static const char* getBName (const TSeries& ser) {
         return 0; }
      /// Get graph type
      static const char* graphType () {
         return "Time Series"; }
      /// post processing
      static void postprocess (float y[], int n) {
      }
      /// Get parameter descriptor
      static ParameterDescriptor* getPrm (const TSeries& ser,
                        ParameterDescriptor& prm) {
         GetParameterDescriptor (ser, prm);
         return &prm; }
      /// Get calibration descriptor
      static calibration::Descriptor* getCal (const TSeries& ser,
                        calibration::Descriptor& cal) {
         GetCalibrationDescriptor (ser, cal);
         return &cal; }
   };


/** @name Traits class specialization for FSeries
    Traits class for FSeries.
    @memo FSeries traits class
    @author Written November 2001 by Daniel Sigg
 ************************************************************************/
template<>
   struct container_traits<FSeries> {
      /// Container type
      typedef FSeries container_type;
      /// Contains data?
      static bool HasData (const FSeries& ser) {
         return ser.refData() != 0; }
      /// X start value
      static float x0 (const FSeries& ser) {
         return ser.getLowFreq(); }
      /// X spacing
      static float dx (const FSeries& ser) {
         return ser.getFStep(); }
      /// Is complex?
      static bool isCmplx (const FSeries& ser) {
         return true; }
      /// Get A channel name
      static const char* getAName (const FSeries& ser) {
         return ser.getName(); }
      /// Get B channel name
      static const char* getBName (const FSeries& ser) {
         return 0; }
      /// Get graph type
      static const char* graphType () {
         return "Frequency series"; }
      /// post processing
      static void postprocess (float y[], int n) {
      }
      /// Get parameter descriptor
      static ParameterDescriptor* getPrm (const FSeries& ser,
                        ParameterDescriptor& prm) {
         GetParameterDescriptor (ser, prm);
         return &prm; }
      /// Get calibration descriptor
      static calibration::Descriptor* getCal (const FSeries& ser,
                        calibration::Descriptor& cal) {
         GetCalibrationDescriptor (ser, cal);
         return &cal; }
   };


/** @name Traits class specialization for FSpectrum
    Traits class for FSpectrum.
    @memo FSpectrum traits class
    @author Written November 2001 by Daniel Sigg
 ************************************************************************/
template<>
   struct container_traits<FSpectrum> {
      /// Container type
      typedef FSpectrum container_type;
      /// Contains data?
      static bool HasData (const FSpectrum& ser) {
         return ser.refData() != 0; }
      /// X start value
      static float x0 (const FSpectrum& ser) {
         return ser.getLowFreq(); }
      /// X spacing
      static float dx (const FSpectrum& ser) {
         return ser.getFStep(); }
      /// Is complex?
      static bool isCmplx (const FSpectrum& ser) {
         return false; }
      /// Get A channel name
      static const char* getAName (const FSpectrum& ser) {
         return ser.getName(); }
      /// Get B channel name
      static const char* getBName (const FSpectrum& ser) {
         return 0; }
      /// Get graph type
      static const char* graphType () {
         return "Power spectrum"; }
      /// post processing
      static void postprocess (float y[], int n) {
         for (int i = 0; i < n; ++i) y[i] = sqrt (fabs (y[i])); }
      /// Get parameter descriptor
      static ParameterDescriptor* getPrm (const FSpectrum& ser,
                        ParameterDescriptor& prm) {
         GetParameterDescriptor (ser, prm);
         return &prm; }
      /// Get calibration descriptor
      static calibration::Descriptor* getCal (const FSpectrum& ser,
                        calibration::Descriptor& cal) {
         GetCalibrationDescriptor (ser, cal);
         return &cal; }
   };


/** @name Traits class specialization for Histogram1
    Traits class for Histogram1.
    @memo Histogram1 traits class
    @author Written November 2001 by Daniel Sigg
 ************************************************************************/
template<>
   struct container_traits<Histogram1> {
      /// Container type
      typedef Histogram1 container_type;
      /// Contains data?
      static bool HasData (const Histogram1& ser) {
         return ser.GetNBins() > 0; }
      /// X start value
      static float x0 (const Histogram1& ser) {
         return ser.GetBinLowEdge(0); }
      /// X spacing
      static float dx (const Histogram1& ser) {
         return ser.GetBinSpacing(); }
      /// Is complex?
      static bool isCmplx (const Histogram1& ser) {
         return false; }
      /// Get A channel name
      static const char* getAName (const Histogram1& ser) {
         return ser.GetTitle(); }
      /// Get B channel name
      static const char* getBName (const Histogram1& ser) {
         return 0; }
      /// Get graph type
      static const char* graphType () {
         return "1-D Histogram"; }
      /// post processing
      static void postprocess (float y[], int n) {
      }
      /// Get parameter descriptor
      static ParameterDescriptor* getPrm (const Histogram1& ser,
                        ParameterDescriptor& prm) {
         GetParameterDescriptor (ser, prm);
         return &prm; }
      /// Get calibration descriptor
      static calibration::Descriptor* getCal (const Histogram1& ser,
                        calibration::Descriptor& cal) {
         GetCalibrationDescriptor (ser, cal);
         return &cal; }
   };


/** @name Get data descriptor function
    Template function to convert a container into a data descriptor. 
    The returned data descriptor has to be deleted by the caller.
    @memo Get data descriptor
    @author Written November 2001 by Daniel Sigg
    @param ser Container
    @return Newly allocated data descriptor, or zero on error
 ************************************************************************/
template <class container>
   inline BasicDataDescriptor* GetDataDescriptor (const container& ser) 
   {
      // get data vector
      const DVector* dvec = ser.refDVect();
      int n;
      if (!dvec || ((n = dvec->getLength()) <= 0)) {
         return 0;
      }
      // create data descriptor
      DataDescriptor* dd = 
         new DataCopy (container_traits<container>::x0 (ser),
                      container_traits<container>::dx (ser), 0, n, 
                      container_traits<container>::isCmplx (ser));
      if (container_traits<container>::isCmplx (ser)) {
         dvec->getData (0, n, (fComplex*)dd->GetY());
      }
      else {
         dvec->getData (0, n, dd->GetY());
      }
      // post process data
      container_traits<container>::postprocess (dd->GetY(), n);
      return dd;
   }


/** @name Get data descriptor specialization for Histogram1
    Template function to convert a Histogram1 into a data descriptor. 
    The returned data descriptor has to be deleted by the caller.
    @memo Get data descriptor
    @author Written November 2001 by Daniel Sigg
    @param ser Container
    @return Newly allocated data descriptor, or zero on error
 ************************************************************************/
template <>
   inline BasicDataDescriptor* GetDataDescriptor (const Histogram1& hist)
   {
      int nent = hist.GetNEntries();
      int nbinx = hist.GetNBins();
      if (nbinx <= 0){
         return 0;
      }
      double*  edge  = new double[nbinx+1];
      double*  content = new double[nbinx+2];
      double*  error = new double[nbinx+2];
      double*  stat = new double[4];
      hist.GetBinLowEdges(edge);
      hist.GetBinContents(content);
      hist.GetStats(stat);
      bool xydata = (hist.GetBinType() == Histogram1::kVariableBin);
      HistDataCopy* dc = new HistDataCopy();
      if (hist.IsErrorFlagON()) {
         hist.GetBinErrors(error);
         dc->SetData(edge,content,error,nbinx,
                    hist.GetXLabel(),
                    hist.GetNLabel(),nent,stat,xydata);
      }
      else {
         dc->SetData(edge,content,nbinx,
                    hist.GetXLabel(),
                    hist.GetNLabel(),nent,stat,xydata);
      }
      if (edge)  delete[] edge;
      if (content) delete[] content;
      if (error) delete[] error;
      if (stat) delete[] stat;
      return dc;
   }


/** @name Get plot descriptor function
    Template function to convert a container into a plot descriptor. 
    The returned plot descriptor has to be deleted by the caller.
    @memo Get data descriptor
    @author Written November 2001 by Daniel Sigg
    @param ser Container
    @return Newly allocated plot descriptor, or zero on error
 ************************************************************************/
template <class container>
   inline PlotDescriptor* GetPlotDescriptor (const container& ser) 
   {
      if (!container_traits<container>::HasData (ser)) {
         return 0;
      }
      BasicDataDescriptor* dd = GetDataDescriptor (ser);
      if (!dd) {
         return 0;
      }
      // create plot descriptor
      ParameterDescriptor prm;
      calibration::Descriptor cal;
      PlotDescriptor* pl = new PlotDescriptor (dd,
                           container_traits<container>::graphType(), 
                           container_traits<container>::getAName (ser),
                           container_traits<container>::getBName (ser),
                           container_traits<container>::getPrm (ser, prm),
                           container_traits<container>::getCal (ser, cal));
      return pl;
   }


//@}

}
#endif // _LIGO_DATADESCRIPTOR_TRAITS_H

