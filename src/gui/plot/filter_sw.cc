
#include <TROOT.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TEnv.h>
#include <TTimer.h>
#include "Plot.hh"
#include "PlotSet.hh"
#include "TLGPad.hh"
#include "IIRFilter.hh"
#include "Limiter.hh"
#include "Sine.hh"
#include "constant.hh"
#include <iostream>

   using namespace ligogui;
   using namespace std;


   TROOT root("GUI", "Bode Plot Test");


   class CheckExit : public TTimer {
   public:
      CheckExit (TApplication& app) 
      : TTimer (1000), fApp (&app) {
         Start (1000, kFALSE);
      }
      virtual Bool_t Notify() {
         if (gPlotSet().GetRegisteredWindows()->empty()) {
            fApp->Terminate();
         }
         return kTRUE;
      }
   protected:
      TApplication*	fApp;
   };

   const double fS = 16384.;
   const double dT = 1.0;
   double f = 10;
   double lim = 1000;

   int main(int argc, char **argv)
   {
      if ((argc > 1) && (argv[argc-1][0] != '-')) {
         f = atof (argv[argc-1]);
      }
      if ((argc > 2) && (argv[argc-2][0] != '-')) {
         lim = atof (argv[argc-2]);
      }      
      
      TApplication theApp ("Bode Plot Test", &argc, argv);
  
      dComplex poles[10];
      dComplex zeros[10];
      for (int i = 0; i < 4; ++i) {
         poles[2*i].setMArg (100, pi - pi/4);
	 poles[2*i+1] = ~poles[2*i];
         zeros[2*i].setMArg (30, pi - pi/4);
	 zeros[2*i+1] = ~zeros[2*i];
      }
      IIRFilter dewhite1 (4, poles, 4, zeros, fS, pow(100./30.,4));
      IIRFilter dewhite2 (4, poles, 4, zeros, fS, pow(100./30.,4));
      IIRFilter dewhite3 (4, poles, 4, zeros, fS, pow(100./30.,4));
      IIRFilter dewhite4 (4, poles, 4, zeros, fS, pow(100./30.,4));
      IIRFilter idewhite1 (4, zeros, 4, poles, fS, pow(30./100.,4));
      IIRFilter idewhite2 (4, zeros, 4, poles, fS, pow(30./100.,4));
      IIRFilter idewhite3 (4, zeros, 4, poles, fS, pow(30./100.,4));
      Limiter limit1 (lim);
      Limiter limit2 (lim);
      
      TSeries in (Time (0), Interval (1./fS), dT*fS, Sine (f, 0.0));
      in.setName ("Input");
      TSeries out1;
      TSeries out2;
      TSeries out3;
      TSeries out4;
      TSeries out5;
      TSeries out6;
      TSeries out7;
      out7 = idewhite1 (in);
      idewhite1.reset();
      out7.setName ("After dewhitening");

//       out1 = dewhite1 (in);
//       out2 = idewhite1 (in);
//       out3 = idewhite2 (out1);
//       Plot (in, out3, out1, out2);
//       dewhite1.reset();
//       idewhite1.reset();
//       idewhite2.reset();

      Time t2 = Time ((int)(dT/2.),1E9*fmod(dT/2., 1));
      TSeries in1 = in.extract (Time (0), dT/2);
      TSeries in2 = in.extract (t2, dT/2.);
      
      out1 = limit1 (in1);         // original
      out1.setName ("Output (dewhitener clean)");
      out2 = limit2 (in1);
      out2.setName ("Output (dewhitener preloaded)");
      out3 = idewhite1 (in1);     // preload dewhitener
      idewhite2 (in1);            // preload dewhitener
      idewhite3 (in1);            // preload dewhitener
      dewhite2 (out3);            // preload whitener
      
      out4 = dewhite1 (in2);
      out5 = dewhite2 (in2);
      out1.Append (idewhite1 (limit1 (out4))); // use clean whitener
      out2.Append (idewhite2 (limit2 (out5))); // use preloaded whitener
      out3 = in1;
      out3.Append (out4);
      out3.setName ("DAC (dewhitener clean)");
      out6 = in1;
      out6.Append (out5);
      out6.setName ("DAC (dewhitener preloaded)");
      TLGPad* pad = Plot (in, out1, out2, out3, out6, out7)->GetPad(0);
      OptionAll_t* opt = pad->GetPlotOptions();
      for (int i = 0; i < 8; ++i) {
         opt->fTraces.fLineAttr[i].SetLineWidth (2.0);
      }
      opt->fTraces.fPlotStyle[2] = kPlotStyleMarker;
      opt->fRange.fRange[0] = kRangeManual;
      opt->fRange.fRangeFrom[0] = 0.44;
      opt->fRange.fRangeTo[0] = 0.56;
      opt->fRange.fRange[1] = kRangeManual;
      opt->fRange.fRangeFrom[1] = -4;
      opt->fRange.fRangeTo[1] = 4;
      opt->fLegend.fPlacement = kLegendTopLeft;
      pad->Update();
      CheckExit checkexit (theApp); 
      theApp.Run();
   
      return 0;
   }
