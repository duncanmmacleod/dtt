#ifndef _LIGO_PLOT_H
#define _LIGO_PLOT_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: Plot							*/
/*                                                         		*/
/* Module Description: High level plotting tools for signal analysis:	*/
/*		       Plot - general plotting routine			*/
/*		       BodePlot, TSPlot, PSPlot - specialized routines	*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 20Nov99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGPlot.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include <complex>
#include "TSeries.hh"
#include "FSeries.hh"
#include "FSpectrum.hh"
#include "Histogram1.hh"
#include "Pipe.hh"
#include "TLGPlot.hh"
#include "SweptSine.hh"

namespace ligogui {
   class TLGMultiPad;
}

   using ligogui::Plot;
   using ligogui::gPlotSet;
   using ligogui::ResetPlots;


/** @name Plot
    This header exports a global plotting pool and the main potting
    routines.

    @memo Plotting routines
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

/** @name Plot
    Plotting routines for time series, frequency series, power spectra
    and histograms.

    @memo Plotting routines
    @author Written November 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{

   /** Plot a time series.
       @memo Plot a time series
       @param t1 Time series
       @param t2 Time series
       @param t3 Time series
       @param t4 Time series
       @param t5 Time series
       @param t6 Time series
       @param t7 Time series
       @param t8 Time series
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   ligogui::TLGMultiPad* Plot (const TSeries& t1, 
                     const TSeries& t2 = TSeries(), 
                     const TSeries& t3 = TSeries(), 
                     const TSeries& t4 = TSeries(), 
                     const TSeries& t5 = TSeries(), 
                     const TSeries& t6 = TSeries(), 
                     const TSeries& t7 = TSeries(), 
                     const TSeries& t8 = TSeries());

   /** Plot a frequency series.
       @memo Plot a frequency series
       @param f1 Frequency series
       @param f2 Frequency series
       @param f3 Frequency series
       @param f4 Frequency series
       @param f5 Frequency series
       @param f6 Frequency series
       @param f7 Frequency series
       @param f8 Frequency series
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   ligogui::TLGMultiPad* Plot (const FSeries& f1, 
                     const FSeries& f2 = FSeries(), 
                     const FSeries& f3 = FSeries(), 
                     const FSeries& f4 = FSeries(), 
                     const FSeries& f5 = FSeries(), 
                     const FSeries& f6 = FSeries(), 
                     const FSeries& f7 = FSeries(), 
                     const FSeries& f8 = FSeries());

   /** Plot a power spectrum.
       @memo Plot a power spectrum
       @param p1 Power spectrum
       @param p2 Power spectrum
       @param p3 Power spectrum
       @param p4 Power spectrum
       @param p5 Power spectrum
       @param p6 Power spectrum
       @param p7 Power spectrum
       @param p8 Power spectrum
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   ligogui::TLGMultiPad* Plot (const FSpectrum& p1, 
                     const FSpectrum& p2 = FSpectrum(), 
                     const FSpectrum& p3 = FSpectrum(), 
                     const FSpectrum& p4 = FSpectrum(), 
                     const FSpectrum& p5 = FSpectrum(), 
                     const FSpectrum& p6 = FSpectrum(), 
                     const FSpectrum& p7 = FSpectrum(), 
                     const FSpectrum& p8 = FSpectrum());

   /** Plot a 1D histogram.
       @memo Plot a 1D histogram
       @param h1 1D histogram
       @param h2 1D histogram
       @param h3 1D histogram
       @param h4 1D histogram
       @param h5 1D histogram
       @param h6 1D histogram
       @param h7 1D histogram
       @param h8 1D histogram
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   ligogui::TLGMultiPad* Plot (const Histogram1& h1, 
                     const Histogram1& h2 = Histogram1(), 
                     const Histogram1& h3 = Histogram1(), 
                     const Histogram1& h4 = Histogram1(), 
                     const Histogram1& h5 = Histogram1(), 
                     const Histogram1& h6 = Histogram1(), 
                     const Histogram1& h7 = Histogram1(), 
                     const Histogram1& h8 = Histogram1());

   /** Plot a filter (Bode plot).
       @memo Bode plot of a filter
       @param f Filter
       @param name filter name
       @param ss Swept sine parameters
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   ligogui::TLGMultiPad* BodePlot (const Pipe& f1, const char* name1,
                     const SweptSine& ss);
   ligogui::TLGMultiPad* BodePlot (const Pipe& f1, const SweptSine& ss);
   ligogui::TLGMultiPad* BodePlot (const Pipe& f1, const char* name1,
                     const Pipe& f2, const char* name2,
                     const SweptSine& ss);
   ligogui::TLGMultiPad* BodePlot (const Pipe& f1, const char* name1,
                     const Pipe& f2, const char* name2,
                     const Pipe& f3, const char* name3,
                     const SweptSine& ss);
   ligogui::TLGMultiPad* BodePlot (const Pipe& f1, const char* name1,
                     const Pipe& f2, const char* name2,
                     const Pipe& f3, const char* name3,
                     const Pipe& f4, const char* name4,
                     const SweptSine& ss);
   ligogui::TLGMultiPad* BodePlot (const Pipe& f1, const char* name1,
                     const Pipe& f2, const char* name2,
                     const Pipe& f3, const char* name3,
                     const Pipe& f4, const char* name4,
                     const Pipe& f5, const char* name5,
                     const SweptSine& ss);

   /** Plot a filter (Bode plot). Brings up a dialog box to set the
       plot parameters.
       @memo Bode plot of a filter
       @param f Filter
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   ligogui::TLGMultiPad* BodePlot (const Pipe& f1,
                     const char* name1 = "filter");
   ligogui::TLGMultiPad* BodePlot (const Pipe& f1, const char* name1,
                     const Pipe& f2, const char* name2);
   ligogui::TLGMultiPad* BodePlot (const Pipe& f1, const char* name1,
                     const Pipe& f2, const char* name2,
                     const Pipe& f3, const char* name3);
   ligogui::TLGMultiPad* BodePlot (const Pipe& f1, const char* name1,
                     const Pipe& f2, const char* name2,
                     const Pipe& f3, const char* name3,
                     const Pipe& f4, const char* name4);
   ligogui::TLGMultiPad* BodePlot (const Pipe& f1, const char* name1,
                     const Pipe& f2, const char* name2,
                     const Pipe& f3, const char* name3,
                     const Pipe& f4, const char* name4,
                     const Pipe& f5, const char* name5);

   /** Plot a data array.
       @memo Plot a data array.
       @param x0 Start value in X
       @param dx X spacing
       @param y Y values
       @param n Number of points
       @param graphtype Graph type used by plot
       @param Achn A channel name used by plot
       @param Bchn B channel name used by plot
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   ligogui::TLGMultiPad* Plot (float x0, float dx, float y[], int n,
                     const char* graphtype = 0, 
                     const char* Achn = 0, const char* Bchn = 0);
   ligogui::TLGMultiPad* Plot (double x0, double dx, double y[], int n,
                     const char* graphtype = 0, 
                     const char* Achn = 0, const char* Bchn = 0);
   ligogui::TLGMultiPad* Plot (std::complex<float> x0, 
                     std::complex<float> dx, 
                     std::complex<float> y[], int n,
                     const char* graphtype = 0, 
                     const char* Achn = 0, const char* Bchn = 0);
   ligogui::TLGMultiPad* Plot (std::complex<double> x0, 
                     std::complex<double> dx, 
                     std::complex<double> y[], int n,
                     const char* graphtype = 0, 
                     const char* Achn = 0, const char* Bchn = 0);

   /** Plot a data array.
       @memo Plot a data array.
       @param x X values
       @param y Y values
       @param n Number of points
       @param graphtype Graph type used by plot
       @param Achn A channel name used by plot
       @param Bchn B channel name used by plot
       @return pointer to multi pad which shows the plot (0 if failed)
    *********************************************************************/
   ligogui::TLGMultiPad* Plot (float x[], float y[], int n,
                     const char* graphtype = 0, 
                     const char* Achn = 0, const char* Bchn = 0);
   ligogui::TLGMultiPad* Plot (double x[], double y[], int n,
                     const char* graphtype = 0, 
                     const char* Achn = 0, const char* Bchn = 0);
   ligogui::TLGMultiPad* Plot (std::complex<float> x[], 
                     std::complex<float> y[], int n,
                     const char* graphtype = 0, 
                     const char* Achn = 0, const char* Bchn = 0);
   ligogui::TLGMultiPad* Plot (std::complex<double> x[], 
                     std::complex<double> y[], int n,
                     const char* graphtype = 0, 
                     const char* Achn = 0, const char* Bchn = 0);

//@}

//@}


#endif // _LIGO_PLOT_H

