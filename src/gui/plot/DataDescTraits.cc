#include "PConfig.h"
#include "DataDescTraits.hh"
#include "Xsil.hh"
#include <sstream>
#include <complex>
#include <string>
#include <string.h>
#include <strings.h>
#include <stdio.h>
#include <math.h>

   using namespace std;
   using namespace ligogui;
   using namespace calibration;
   using namespace xml;

namespace ligogui {


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// GetParameterDescriptor                                               //
//                                                                      //
// Get the parameter descriptor                                         //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool GetParameterDescriptor (const TSeries& cntr,
                     ParameterDescriptor& prm)
   {
      prm = ParameterDescriptor();
      prm.SetStartTime (cntr.getStartTime().getS(),
                       cntr.getStartTime().getN());
      int avrg = 0; // TSeries
      if (avrg != 0) {
         prm.SetAverages (avrg);
      }
      else {
         prm.ResetAverages ();
      }
      prm.SetThird (0);
      ostringstream os;
      Time t0 = cntr.getStartTime();
      if (!t0) {
         t0 += Interval (cntr.getTStep());
      }
      os << xsilTime ("t0", t0.getS(), t0.getN()) << endl;
      double dt = (double)cntr.getTStep();
      os << xsilParameter<double> ("dt", dt) << endl;
      double f0 = (double)cntr.getF0();
      if (f0 != 0) 
         os << xsilParameter<double> ("f0", f0) << endl;
      if (!avrg) avrg =1;
      os << xsilParameter<int> ("Averages", avrg) << endl;
      if (container_traits<TSeries>::getAName (cntr)) 
         os << xsilParameter<const char*> 
            ("Channel", container_traits<TSeries>::getAName (cntr)) << endl;
      int n = cntr.getNSample();
      os << xsilParameter<int> ("N", n);
      prm.SetUser (os.str().c_str());
      return true;
   }

//______________________________________________________________________________
   bool GetParameterDescriptor (const FSeries& cntr,
                     ParameterDescriptor& prm)
   {
      prm = ParameterDescriptor();
      prm.SetStartTime (cntr.getStartTime().getS(),
                       cntr.getStartTime().getN());
      int avrg = 0; // :TODO: FSeries
      if (avrg != 0) {
         prm.SetAverages (avrg);
      }
      else {
         prm.ResetAverages ();
      }
      double bw = 0; // :TODO: FSeries
      if (bw > 0) {
         char buf[128];
         sprintf (buf, "BW=%g", bw);
         prm.SetThird (buf);
      }
      else {
         prm.SetThird (0);
      }
      ostringstream os;
      Time t0 = cntr.getStartTime();
      os << xsilTime ("t0", t0.getS(), t0.getN()) << endl;
      double f0 = cntr.getCenterFreq(); // FSeries
      os << xsilParameter<double> ("f0", f0) << endl;
      double df = cntr.getFStep(); // FSeries
      os << xsilParameter<double> ("df", df) << endl;
      os << xsilParameter<double> ("BW", bw) << endl;
      int win = 1;
      os << xsilParameter<int> ("Window", win) << endl;
      if (!avrg) avrg = 1;
      os << xsilParameter<int> ("Averages", avrg) << endl;
      if (container_traits<FSeries>::getAName (cntr)) 
         os << xsilParameter<const char*> 
            ("ChannelA", container_traits<FSeries>::getAName (cntr)) << endl;
      if ( container_traits<FSeries>::getBName (cntr)) 
         os << xsilParameter<const char*> 
            ("ChannelB[0]",  container_traits<FSeries>::getBName (cntr)) << endl;
      int n = cntr.getNStep()+1; // FSeries
      os << xsilParameter<int> ("N", n) << endl;
      os << xsilParameter<int> ("M", 1);
      prm.SetUser (os.str().c_str());
      return true;
   }

//______________________________________________________________________________
   bool GetParameterDescriptor (const FSpectrum& cntr,
                     ParameterDescriptor& prm)
   {
      prm = ParameterDescriptor();
      prm.SetStartTime (cntr.getStartTime().getS(),
                       cntr.getStartTime().getN());
      int avrg = 0; // :TODO: FSpectrum
      if (avrg != 0) {
         prm.SetAverages (avrg);
      }
      else {
         prm.ResetAverages ();
      }
      double bw = 0; // :TODO: FSpectrum
      if (bw > 0) {
         char buf[128];
         sprintf (buf, "BW=%g", bw);
         prm.SetThird (buf);
      }
      else {
         prm.SetThird (0);
      }
      ostringstream os;
      Time t0 = cntr.getStartTime();
      os << xsilTime ("t0", t0.getS(), t0.getN()) << endl;
      double f0 = cntr.getLowFreq(); //FSpectrum
      os << xsilParameter<double> ("f0", f0) << endl;
      double df = cntr.getFStep(); //FSpectrum
      os << xsilParameter<double> ("df", df) << endl;
      os << xsilParameter<double> ("BW", bw) << endl;
      int win = 1;
      os << xsilParameter<int> ("Window", win) << endl;
      avrg = cntr.getCount(); //FSpectrum   
      if (!avrg) avrg = 1;
      os << xsilParameter<int> ("Averages", avrg) << endl;
      if (container_traits<FSpectrum>::getAName (cntr)) 
         os << xsilParameter<const char*> 
            ("ChannelA", container_traits<FSpectrum>::getAName (cntr)) << endl;
      if ( container_traits<FSpectrum>::getBName (cntr)) 
         os << xsilParameter<const char*> 
            ("ChannelB[0]", container_traits<FSpectrum>::getBName (cntr)) << endl;
      int n = cntr.getNStep()+1; //FSpectrum
      os << xsilParameter<int> ("N", n) << endl;
      os << xsilParameter<int> ("M", 1);
      prm.SetUser (os.str().c_str());
      return true;
   }

//______________________________________________________________________________
   bool GetParameterDescriptor (const Histogram1& cntr,
                     ParameterDescriptor& prm)
   {
      prm = ParameterDescriptor();
      prm.SetStartTime (cntr.GetTime().getS(),
                       cntr.GetTime().getN());
      int nent = cntr.GetNEntries();
      int nbinx = cntr.GetNBins();
      if (nbinx<=0){
         return false;
      }
      double*  stat = new double[4];
      cntr.GetStats(stat);
      ostringstream os;
      os << xsilTime("t0",cntr.GetTime().getS(),cntr.GetTime().getN()) << endl;
      os << xsilParameter<int> ("NBinx",nbinx) << endl;
      os << xsilParameter<int> ("NData",nent) << endl;
      os << xsilParameter<double> ("SumWeight",stat[0]) << endl;
      os << xsilParameter<double> ("SumWeightSqr",stat[1]) << endl;
      os << xsilParameter<double> ("SumWeightX",stat[2]) << endl;
      os << xsilParameter<double> ("SumWeightXSqr",stat[3]) << endl;
   
      if (strcasecmp(cntr.GetTitle(),container_traits<Histogram1>::getAName (cntr)) == 0 ) {
         os << xsilParameter<string> ("Title", cntr.GetTitle()) << endl;
      }
      else {
         os << xsilParameter<string> 
            ("Title", container_traits<Histogram1>::getAName (cntr)) << endl;
      }
      if ( cntr.GetXLabel() )
         os << xsilParameter<string> ("XLabel", cntr.GetXLabel()) << endl;
      if ( cntr.GetNLabel() )
         os << xsilParameter<string> ("NLabel", cntr.GetNLabel()) << endl;
      if (cntr.GetBinType() == Histogram1::kFixedBin) {
         os << xsilParameter<double> ("XLowEdge", cntr.GetBinLowEdge(1)) << endl;
         os << xsilParameter<double> 
            ("XSpacing", cntr.GetBinSpacing()) << endl;
      }
      prm.SetUser (os.str().c_str());
   
      if (stat) delete[] stat;
      return true;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// GetParameterDescriptor                                               //
//                                                                      //
// Get the parameter descriptor                                         //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool GetCalibrationDescriptor (const TSeries& cntr,
                     calibration::Descriptor& cal)
   {
      cal.Init();
      cal.SetDomain (kCalUnitX, kCalDomainTime);
      cal.SetChannel (0, container_traits<TSeries>::getAName (cntr));
      cal.SetChannel (1, container_traits<TSeries>::getBName (cntr));
      cal.SetBW (kCalUnitY, 0);
      cal.SetTime (cntr.getStartTime());
      cal.SetValid();
      return true;
   }

//______________________________________________________________________________
   bool GetCalibrationDescriptor (const FSeries& cntr,
                     calibration::Descriptor& cal)
   {
      cal.Init();
      cal.SetDomain (kCalUnitX, kCalDomainFrequency);
      cal.SetDensity (kCalUnitX, kCalDensityAmpPerRtHz);
      cal.SetChannel (0, container_traits<FSeries>::getAName (cntr));
      cal.SetChannel (1, container_traits<FSeries>::getBName (cntr));
      double bw = 0; // :TODO: FSeries
      cal.SetBW (kCalUnitY, bw > 0 ? bw : 0);
      cal.SetTime (cntr.getStartTime());
      cal.SetValid();
      return true;
   }

//______________________________________________________________________________
   bool GetCalibrationDescriptor (const FSpectrum& cntr,
                     calibration::Descriptor& cal)
   {
      cal.Init();
      cal.SetDomain (kCalUnitX, kCalDomainFrequency);
      cal.SetDensity (kCalUnitX, kCalDensityAmpPerRtHz);
      cal.SetChannel (0, container_traits<FSpectrum>::getAName (cntr));
      cal.SetChannel (1, container_traits<FSpectrum>::getBName (cntr));
      double bw = 0; // :TODO: FSpectrum
      cal.SetBW (kCalUnitY, bw > 0 ? bw : 0);
      cal.SetTime (cntr.getStartTime());
      cal.SetValid();
      return true;
   }

//______________________________________________________________________________
   bool GetCalibrationDescriptor (const Histogram1& cntr,
                     calibration::Descriptor& cal)
   {
      cal.Init(); // invalid
      return true;
   }


}
