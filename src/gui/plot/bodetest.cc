
#include <TROOT.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TEnv.h>
#include <TTimer.h>
#include "Plot.hh"
#include "PlotSet.hh"
#include "IIRFilter.hh"
#include "FilterDesign.hh"
#include "iirutil.hh"
#include "constant.hh"
#include <string>
#include <iostream>

   using namespace ligogui;
   using namespace std;



   TROOT root("GUI", "Bode Plot Test");


   class CheckExit : public TTimer {
   public:
      CheckExit (TApplication& app) 
      : TTimer (1000), fApp (&app) {
         Start (1000, kFALSE);
      }
      virtual Bool_t Notify() {
         if (gPlotSet().GetRegisteredWindows()->empty()) {
            fApp->Terminate();
         }
         return kTRUE;
      }
   protected:
      TApplication*	fApp;
   };


   int main(int argc, char **argv)
   {
      string s;
      TApplication theApp ("Bode Plot Test", &argc, argv);
   
      cout << "ORIGINAL FILTER" << endl;
      FilterDesign ds (16384, "original");
      ds.pole2 (100, 10, pow(2*pi*100,2), "f");
      ds.pole (4000, "n");
      ds.zero2 (4000, 10, "n");
      ds.bode (1, 7000, 1001);
      if (isiir (ds())) {
         cout << "Is an IIR filter with " << iirpolecount (ds()) << " poles and " 
            << iirzerocount (ds()) << " zeros" << endl;
      }
      else { 
         cerr << "Not an IIR filter" << endl;
         return 1;
      }
      cout << "TEST INVERSE BILINEAR" << endl;
      if (!iir2zpk (ds(), s, "s")) {
         cerr << "No s-plane representation" << endl;
      }
      cout << s << endl;
      iir2zpk (ds(), s, "f");
      cout << s << endl;
      iir2zpk (ds(), s, "n");
      cout << s << endl;
   
      cout << "TEST BILINEAR" << endl;
      dComplex pole[10];
      dComplex zero[10];
      int npoles = 0;  
      int nzeros = 0;
      double gain;
      iir2zpk (ds(), nzeros, zero, npoles, pole, gain, "s");
      if (!s2z (ds.getFSample(), nzeros, zero, npoles, pole, gain, "s")) {
         cerr << "No s/z-plane representation" << endl;
      }
      // for (int i = 0; i < nzeros; ++i) cout << "zero[" << i << "] = " << zero[i] << endl;
      // for (int i = 0; i < npoles; ++i) cout << "pole[" << i << "] = " << pole[i] << endl;
      FilterDesign dds (16384);
      dds.zroots (nzeros, zero, npoles, pole, gain);
      iir2zpk (dds(), s, "n");
      cout << s << endl;
   
      cout << "TEST Z2Z" << endl;
      int nba = 0;
      double ba[100];
      iir2z (ds(), nzeros, zero, npoles, pole, gain);
      if (!z2z (nzeros, zero, npoles, pole, gain,
               nba, ba, "s")) {
         cerr << "No z/z-plane conversion" << endl;
      }
      FilterDesign dds2 (16384, "Z2Z");
      dds2.sos (nba, ba, "s");
      iir2zpk (dds2(), s, "n");
      cout << s << endl;
      iir2z (dds2(), s, "s");
      cout << s << endl;
      dds2.bode (1, 7000, 1001);
   
      cout << "TEST FILTER DESIGN" << endl;
      iir2zpk (ds(), s, "n");
      FilterDesign ds2 (16384, "zpk");
      if (!ds2.filter (s.c_str())) {
         cerr << "No s-plane filter" << endl;
      }
      else {
         ds2.bode (1, 7000, 1001);
      }
      if (!iir2zpk (ds2(), s, "s")) {
         cerr << "No s-plane representation" << endl;
      }
      cout << s << endl;
      iir2zpk (ds2(), s, "f");
      cout << s << endl;
      iir2zpk (ds2(), s, "n");
      cout << s << endl;
   
      cout << "TEST Z DESIGN" << endl;
      if (!iir2z (ds2(), s, "s")) {
         cerr << "No z-plane representation" << endl;
      }
      cout << s << endl;
      FilterDesign ds3 (16384, "sos");
      if (!ds3.filter (s.c_str())) {
         cerr << "No sos filter" << endl;
      }
      else {
         ds3.bode (1, 7E3, 1001);
      }
   
      iir2z (ds2(), s, "r");
      cout << s << endl;
   
      FilterDesign ds4 (16384, "zplane");
      if (!ds4.filter (s.c_str())) {
         cerr << "No z-plane filter" << endl;
      }
      else {
         ds4.bode (1, 7E3, 1001);
      }
   
      CheckExit checkexit (theApp); 
      theApp.Run();
   
      return 0;
   
      // dComplex pole (-100, 0);
      // dComplex zero (-100, 0);
      // IIRFilter filter1 (1, &pole, 0, 0, 16384, -628.3185);
      // dComplex pole2 (-1E4, 0);
      // IIRFilter filter2 (0, &pole2, 1, &zero, 16384,  16384/2/100);
      // BodePlot (filter1, "pole_100", filter2, "zero_100");
      // CheckExit checkexit (theApp); 
      // theApp.Run();
   // 
      // return 0;
   }
