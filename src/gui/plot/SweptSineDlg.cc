
#include <string.h>
#include <strings.h>
#include "SweptSineDlg.hh"
#include "TLGEntry.hh"
#include <TVirtualX.h>
#include "Uniform.hh"
#include "FlatTop.hh"
#include "Hanning.hh"
#include "Hamming.hh"
#include "Welch.hh"
#include "Bartlett.hh"
#include "BMH.hh"
#include "Blackman.hh"

   using namespace std;
   using namespace ligogui;



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// WindowCombobox                                                       //
//                                                                      //
// Combobox for window selection                                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class WindowCombobox : public TGComboBox {
   public:
      WindowCombobox (TGWindow* p, Int_t id) : TGComboBox (p, id) {
         Resize (110, 22);
         AddEntry ("Uniform", 0);
         AddEntry ("Hanning", 1);
         AddEntry ("Flat-top", 2);
         AddEntry ("Welch", 3);
         AddEntry ("Bartlett", 4);
         AddEntry ("BMH", 5);
         AddEntry ("Hamming", 6);
         AddEntry ("Blackman", 7);
         Select (0);
      }
   };



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGSweptSineDialog                                                   //
//                                                                      //
// Dialog box for selecting swept sine parameters                       //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   TLGSweptSineDialog::TLGSweptSineDialog (const TGWindow *p, 
                     const TGWindow *main,
                     SweptSine& prm, Bool_t& ret)
   : TLGTransientFrame (p, main, 10, 10), fParam (&prm), fOk (&ret)
   {
      // layouts
      fL[0] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsTop, 0, 0, 0, 0);
      fL[1] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsTop, 4, 4, 4, 4);
      fL[2] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsTop, 4, 4, 4, 4);
      fL[3] = new TGLayoutHints (kLHintsRight | kLHintsCenterY, 6, 4, 2, 2);
      fL[4] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsTop, 0, 0, 2, -6);
      fL[5] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2);
      fL[6] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 4, -2, 2, 2);
      fL[7] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsTop, 0, 0, 2, 2);
      // frames
      fF[0] = new TGVerticalFrame (this, 10, 10);
      AddFrame (fF[0], fL[0]);
      fGroup[0] = new TGGroupFrame (fF[0], "Sampling rate");
      fF[0]->AddFrame (fGroup[0], fL[1]);
      fF[2] = new TGHorizontalFrame (fGroup[0], 10, 10);
      fGroup[0]->AddFrame (fF[2], fL[4]);   
      fGroup[1] = new TGGroupFrame (fF[0], "Swept sine parameters");
      fF[0]->AddFrame (fGroup[1], fL[1]);
      fF[3] = new TGHorizontalFrame (fGroup[1], 10, 10);
      fGroup[1]->AddFrame (fF[3], fL[7]);
      fF[4] = new TGHorizontalFrame (fGroup[1], 10, 10);
      fGroup[1]->AddFrame (fF[4], fL[7]);
      fF[5] = new TGHorizontalFrame (fGroup[1], 10, 10);
      fGroup[1]->AddFrame (fF[5], fL[4]);
      // buttons
      fF[1] = new TGHorizontalFrame (this, 10, 10);
      AddFrame (fF[1], fL[2]);
      fButton[0] = 
         new TGTextButton (fF[1], new TGHotString ("     &Cancel     "), 0);
      fButton[0]->Associate (this);
      fF[1]->AddFrame (fButton[0], fL[3]);
      fButton[1] = 
         new TGTextButton (fF[1], new TGHotString ("        &Ok        "), 1);
      fButton[1]->Associate (this);
      fF[1]->AddFrame (fButton[1], fL[3]);
      // Sample
      fLabel[7] = new TGLabel (fF[2], "Sampling:");
      fF[2]->AddFrame (fLabel[7], fL[5]);
      fSample = new TLGNumericControlBox (fF[2], 0, 10, 
                           51, kNESReal, kNEANonNegative);
      fSample->Associate (this);
      fF[2]->AddFrame (fSample, fL[5]);
      fLabel[8] = new TGLabel (fF[2], "Hz");
      fF[2]->AddFrame (fLabel[8], fL[5]);
      // Data line 1
      fLabel[0] = new TGLabel (fF[3], "Start:");
      fF[3]->AddFrame (fLabel[0], fL[5]);
      fStart = new TLGNumericControlBox (fF[3], 0, 10, 
                           10, kNESReal, kNEANonNegative);
      fStart->Associate (this);
      fF[3]->AddFrame (fStart, fL[5]);
      fLabel[1] = new TGLabel (fF[3], "Hz   Stop:");
      fF[3]->AddFrame (fLabel[1], fL[5]);
      fStop = new TLGNumericControlBox (fF[3], 1000, 10, 
                           11, kNESReal, kNEANonNegative);
      fStop->Associate (this);
      fF[3]->AddFrame (fStop, fL[5]);
      fLabel[2] = new TGLabel (fF[3], "Hz   Points:");
      fF[3]->AddFrame (fLabel[2], fL[5]);
      fPoints = new TLGNumericControlBox (fF[3], 1, 6, 12,
                           kNESInteger, kNEAPositive);
      fPoints->Associate (this);
      fF[3]->AddFrame (fPoints, fL[5]);
      // Data line 2
      fLabel[3] = new TGLabel (fF[4], "Measurement Time:");
      fF[4]->AddFrame (fLabel[3], fL[5]);
      fMeasTimeSel[0] = new TGCheckButton (fF[4], "", 21);
      fMeasTimeSel[0]->Associate (this);
      fF[4]->AddFrame (fMeasTimeSel[0], fL[6]);
      fMeasTime[0] = new TLGNumericControlBox (fF[4], 0, 6, 
                           22, kNESReal, kNEANonNegative);
      fMeasTime[0]->Associate (this);
      fF[4]->AddFrame (fMeasTime[0], fL[5]);
      fLabel[4] = new TGLabel (fF[4], "cycles  ");
      fF[4]->AddFrame (fLabel[4], fL[5]);
      fMeasTimeSel[1] = new TGCheckButton (fF[4], "", 23);
      fMeasTimeSel[1]->Associate (this);
      fF[4]->AddFrame (fMeasTimeSel[1], fL[6]);
      fMeasTime[1] = new TLGNumericControlBox (fF[4], 0, 6, 
                           24, kNESReal, kNEANonNegative);
      fMeasTime[1]->Associate (this);
      fF[4]->AddFrame (fMeasTime[1], fL[5]);
      fLabel[5] = new TGLabel (fF[4], "sec    Settling time:");
      fF[4]->AddFrame (fLabel[5], fL[5]);
      fSettling = new TLGNumericControlBox (fF[4], 0, 6, 
                           25, kNESRealOne, kNEANonNegative);
      fSettling->Associate (this);
      fF[4]->AddFrame (fSettling, fL[5]);
      fLabel[4] = new TGLabel (fF[4], "%");
      fF[4]->AddFrame (fLabel[4], fL[5]);
      // Data line 3
      fLabel[6] = new TGLabel (fF[5], "Sweep type:");
      fF[5]->AddFrame (fLabel[6], fL[5]);
      fSweepType[0] = new TGRadioButton (fF[5], "Linear  ", 31);
      fSweepType[0]->Associate (this);
      fF[5]->AddFrame (fSweepType[0], fL[5]);
      fSweepType[1] = new TGRadioButton (fF[5], "Logarithmic    Window:", 32);
      fSweepType[1]->Associate (this);
      fF[5]->AddFrame (fSweepType[1], fL[5]);
      fWindow = new WindowCombobox (fF[5], 33);
      fWindow->Associate (this);
      fF[5]->AddFrame (fWindow, fL[5]);
   
      // set parameters
      fSample->SetNumber (fParam->GetSampling());
      fStart->SetNumber (fParam->GetFStart());
      fStop->SetNumber (fParam->GetFStop());
      fPoints->SetIntNumber (fParam->GetPoints());
      fSettling->SetNumber (100*fParam->GetSettlingTime());
      fMeasTime[0]->SetNumber (fParam->GetCycles());
      fMeasTime[1]->SetNumber (fParam->GetMinTime());
      fMeasTimeSel[0]->SetState (
                           fParam->GetCycles() ? kButtonDown : kButtonUp);
      fMeasTimeSel[1]->SetState (
                           fParam->GetMinTime() ? kButtonDown : kButtonUp);
      if (strncasecmp (fParam->GetSweepType(), "lin", 3) == 0) {
         fSweepType[0]->SetState (kButtonDown);
         fSweepType[1]->SetState (kButtonUp);
      }
      else {
         fSweepType[0]->SetState (kButtonUp);
         fSweepType[1]->SetState (kButtonDown);
      }
      if (dynamic_cast<Uniform*>(fParam->GetWindow())) {
         fWindow->Select (0);
      }
      else if (dynamic_cast<FlatTop*>(fParam->GetWindow())) {
         fWindow->Select (2);
      }
      else if (dynamic_cast<Welch*>(fParam->GetWindow())) {
         fWindow->Select (3);
      }
      else if (dynamic_cast<BMH*>(fParam->GetWindow())) {
         fWindow->Select (4);
      }
      else if (dynamic_cast<Bartlett*>(fParam->GetWindow())) {
         fWindow->Select (5);
      }
      else if (dynamic_cast<Hamming*>(fParam->GetWindow())) {
         fWindow->Select (6);
      }
      else if (dynamic_cast<Blackman*>(fParam->GetWindow())) {
         fWindow->Select (7);
      }
      else {
         fWindow->Select (1);
      }
   
      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
   
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                              (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
                              (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                              ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
   
      // make the message box non-resizable
      SetWMSize(width, height);
      SetWMSizeHints(width, height, width, height, 0, 0);
   
      // set dialog box title
      SetWindowName ("Swept Sine Parameters");
      SetIconName ("Swept Sine Parameters");
      SetClassHints ("SweptSineParamDlg", "SweptSineParamDlg");
   
      SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
                 kMWMDecorMinimize | kMWMDecorMenu,
                 kMWMFuncAll  | kMWMFuncResize    | kMWMFuncMaximize |
                 kMWMFuncMinimize,
                 kMWMInputModeless);
   
      MapWindow();
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   TLGSweptSineDialog::~TLGSweptSineDialog ()
   {
      delete fSample;
      delete fStart;
      delete fStop;
      delete fPoints;
      delete fSettling;
      delete fMeasTimeSel[0];
      delete fMeasTimeSel[1];
      delete fMeasTime[0];
      delete fMeasTime[1];
      delete fSweepType[0];
      delete fSweepType[1];
      delete fWindow;
      delete fButton[0];
      delete fButton[1];
      for (int i = 0; i < 9; ++i) {
         delete fLabel[i];
      }
      delete fGroup[0];
      delete fGroup[1];
      for (int i = 0; i < 6; ++i) {
         delete fF[i];
      }
      for (int i = 0; i < 8; ++i) {
         delete fL[i];
      }
   }

//______________________________________________________________________________
   void TLGSweptSineDialog::CloseWindow()
   {
      if (fOk) *fOk = kFALSE;
      DeleteWindow();
   }

//______________________________________________________________________________
   Bool_t TLGSweptSineDialog::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         switch (parm1) {
            // ok
            case 1:
               {
                  // get parameters
                  fParam->SetSampling (fSample->GetNumber());
                  fParam->SetFStart (fStart->GetNumber());
                  fParam->SetFStop (fStop->GetNumber());
                  fParam->SetPoints (fPoints->GetIntNumber());
                  fParam->SetSettlingTime (fSettling->GetNumber() / 100.);
                  fParam->SetCycles (fMeasTime[0]->GetNumber());
                  fParam->SetMinTime (fMeasTime[1]->GetNumber());
                  if (fMeasTimeSel[0]->GetState() == kButtonUp) {
                     fParam->SetCycles (0);
                  }
                  if (fMeasTimeSel[1]->GetState() == kButtonUp) {
                     fParam->SetMinTime (0);
                  }
                  if (fSweepType[0]->GetState() == kButtonDown) {
                     fParam->SetSweepType ("linear");
                  }
                  else {
                     fParam->SetSweepType ("log");
                  }
                  switch (fWindow->GetSelected()) {
                     case 0:
                        fParam->SetWindow (new Uniform());
                        break;
                     case 2:
                        fParam->SetWindow (new FlatTop());
                        break;
                     case 3:
                        fParam->SetWindow (new Welch());
                        break;
                     case 4:
                        fParam->SetWindow (new BMH());
                        break;
                     case 5:
                        fParam->SetWindow (new Bartlett());
                        break;
                     case 6:
                        fParam->SetWindow (new Hamming());
                        break;
                     case 7:
                        fParam->SetWindow (new Blackman());
                        break;
                     default:
                        fParam->SetWindow (new Hanning());
                        break;
                  }
               
                  if (fOk) *fOk = kTRUE;
                  DeleteWindow();
                  break;
               }
            // cancel
            case 0:
               {
                  if (fOk) *fOk = kFALSE;
                  DeleteWindow();
                  break;
               }
         }
      }
      // Radio buttons
      else if ((GET_MSG (msg) == kC_COMMAND) &&
              (GET_SUBMSG (msg) == kCM_RADIOBUTTON)) {
         switch (parm1) {
            case 31:
            case 32:
               {
                  for (int i = 0; i < 2; i++) {
                     fSweepType[i]->SetState (i == parm1 - 31 ? 
                                          kButtonDown : kButtonUp);
                  }
                  break;
               }
         }
      }
   
      return kTRUE;
   }

