#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
//#pragma link C++ nestedclasses;
//#pragma link C++ nestedtypedefs;

#pragma link C++ function ConvertHistogram;
#pragma link C++ function PlotHistogram;
#pragma link C++ function PlotRootHistogram;
#pragma link C++ function Plot;
#pragma link C++ function BodePlot;
//#pragma link C++ class ligogui;

#pragma link C++ class filterwiz::FilterFile;
#pragma link C++ class filterwiz::FilterModule;
#pragma link C++ typedef filterwiz::FilterModuleList;
#pragma link C++ class std::list<filterwiz::FilterModule>;
#pragma link C++ class std::list<filterwiz::FilterModule>::iterator;
#pragma link C++ class filterwiz::FilterSection;
#pragma link C++ namespace filterwiz;

#endif

