#include "HistogramUtil.hh"
#include "Histogram1.hh"
#include "Histogram2.hh"
#include "DataDesc.hh"
#include "TLGOptions.hh"
#include "TLGPlot.hh"
#include "TLGPad.hh"
#include "TCanvas.h"
#include "TROOT.h"

   using namespace ligogui;
   using namespace std;

/// convert DMT histogram -> ROOT histogram (1-D)

//namespace events {

   void ConvertHistogram(const Histogram1& h, TH1D& rh)
   {
   
      int nbin = h.GetNBins();
   
      if ( h.GetBinType() == Histogram1::kFixedBin ) {
         rh.SetBins(nbin,h.GetBinLowEdge(1),h.GetBinLowEdge(nbin+1));
      }
      else if ( h.GetBinType() == Histogram1::kVariableBin ) {
         double* edge = new double[nbin+1];
         h.GetBinLowEdges(edge);
         rh.GetXaxis()->Set(nbin,edge);
         if (edge) delete[] edge;
      }
      else 
         return;
   
      double* content = new double[nbin+2];
      h.GetBinContents(content);
      rh.SetContent(content);
      if (content) delete[] content;
   
      if ( h.IsErrorFlagON() ) {
         double* error = new double[nbin+2];
         h.GetBinErrors(error);
         rh.SetError(error);
         if (error) delete[] error;
      }
   
      double* stat = new double[4];
      h.GetStats(stat);
      rh.PutStats(stat);
      if (stat) delete[] stat;
   
      rh.SetEntries(h.GetNEntries());
   
      // rh.SetNameTitle("1-D Histogram",h.GetTitle());
      rh.SetXTitle(h.GetXLabel());
      rh.SetYTitle(h.GetNLabel());
   
   
   }

/// convert DMT histogram -> ROOT histogram (2-D)
   void ConvertHistogram(const Histogram2& h, TH2D& rh)
   {
      int nbinx = h.GetNBins(Histogram2::kXAxis);
      int nbiny = h.GetNBins(Histogram2::kYAxis);
   
      if ( h.GetBinType() == Histogram2::kFixedBin ) {
         rh.SetBins(nbinx,
                   h.GetBinLowEdge(1,Histogram2::kXAxis),
                   h.GetBinLowEdge(nbinx+1,Histogram2::kXAxis),
                   nbiny,
                   h.GetBinLowEdge(1,Histogram2::kYAxis),
                   h.GetBinLowEdge(nbiny+1,Histogram2::kYAxis));
      }
      else if ( h.GetBinType() == Histogram2::kVariableBin ) {
         double* edgex = new double[nbinx+1];
         double* edgey = new double[nbiny+1];
         h.GetBinLowEdges(edgex,Histogram2::kXAxis);
         h.GetBinLowEdges(edgey,Histogram2::kYAxis);
         rh.GetXaxis()->Set(nbinx, edgex);
         rh.GetYaxis()->Set(nbiny, edgey);
         if (edgex) delete[] edgex;
         if (edgey) delete[] edgey;
      }
      else 
         return;
   
      double* content = new double[(nbiny+2)*(nbinx+2)];
      h.GetBinContents(content);
      rh.SetContent(content);
      if (content) delete[] content;
   
      if ( h.IsErrorFlagON() ) {
         double* error = new double[(nbiny+2)*(nbinx+2)];
         h.GetBinErrors(error);
         rh.SetError(error);
         if (error) delete[] error;
      }
   
      double* stat = new double[7];
      h.GetStats(stat);
      rh.PutStats(stat);
      if (stat) delete[] stat;
   
      rh.SetEntries(h.GetNEntries());
   
      // rh.SetNameTitle("2-D Histogram",h.GetTitle());
      rh.SetXTitle(h.GetXLabel());
      rh.SetYTitle(h.GetYLabel());
      rh.SetZTitle(h.GetNLabel());
   }

/// convert ROOT histogram -> DMT histogram (1-D)
   void ConvertHistogram(TH1D& rh , Histogram1& h)
   {
   
      h.Reset();
   
      int nbin = rh.GetNbinsX();
      double* edge = new double[nbin+1];
      for (int i = 0; i < nbin+1; ++i)
         edge[i] = rh.GetBinLowEdge(i+1);
   
      int bintype;
      if (rh.GetXaxis()->GetXbins()->fN) {
         bintype = Histogram1::kVariableBin;
      }
      else {
         bintype = Histogram1::kFixedBin;
      }
   
      double* content = new double[nbin+2];
      for (int i = 0; i < nbin+2; ++i) content[i] = rh.GetBinContent(i);
      double* error = new double[nbin+2];
      for (int i = 0; i < nbin+2; ++i) error[i] = rh.GetBinError(i);
      double* stat = new double[4];
      rh.GetStats(stat);
   
      h.SetTitle(rh.GetTitle());
      h.SetXLabel(rh.GetXaxis()->GetTitle());
      h.SetNLabel(rh.GetYaxis()->GetTitle());
      h.SetBinLowEdges(nbin,edge);
      h.SetBinType(bintype);
      h.SetBinContents(content);
      h.SetBinErrors(error);
      h.PutStats(stat);
      h.SetNEntries(int(rh.GetEntries()));
   
      if (edge) delete[] edge;
      if (content) delete[] content;
      if (error) delete[] error;
      if (stat) delete[] stat;
   }

/// convert ROOT histogram -> DMT histogram (2-D)
   void ConvertHistogram(TH2D& rh, Histogram2& h)
   {
   
      h.Reset();
   
      int nbinx = rh.GetNbinsX();
      int nbiny = rh.GetNbinsY();
      double* edgex = new double[nbinx+1];
      double* edgey = new double[nbiny+1];
      for (int i = 0; i < nbinx+1; ++i)
         edgex[i] = rh.GetBinLowEdge(i+1);
      for (int i = 0; i < nbiny+1; ++i)
         edgey[i] = (*(rh.GetYaxis())).GetBinLowEdge(i+1);
   
      int bintype;
      if ((*(rh.GetXaxis())).GetXbins()->fN ||
         (*(rh.GetYaxis())).GetXbins()->fN ) {
         bintype = Histogram2::kVariableBin;
      }
      else {
         bintype = Histogram2::kFixedBin;
      }
   
      int arraysize = (nbinx+2)*(nbiny+2);
   
      double* content = new double[arraysize];
      for (int i = 0; i < arraysize; ++i) 
         content[i] = rh.GetBinContent(i);
      double* error = new double[arraysize];
      for (int i = 0; i < arraysize; ++i) 
         error[i] = rh.GetBinError(i);
      double* stat = new double[7];
      rh.GetStats(stat);
   
      h.SetTitle(rh.GetTitle());
      h.SetXLabel(rh.GetXaxis()->GetTitle());
      h.SetYLabel(rh.GetYaxis()->GetTitle());
      h.SetNLabel(rh.GetZaxis()->GetTitle());
      h.SetBinLowEdges(nbinx,edgex,nbiny,edgey);
      h.SetBinType(bintype);
      h.SetBinContents(content);
      h.SetBinErrors(error);
      h.PutStats(stat);
      h.SetNEntries( int(rh.GetEntries()) );
   
      if (edgex) delete[] edgex;
      if (edgey) delete[] edgey;
      if (content) delete[] content;
      if (error) delete[] error;
      if (stat) delete[] stat;
   }

/// plot 1-D histogram
   void PlotHistogram (const Histogram1& h)
   {
      std::string title;
      if (h.GetTitle()) title = h.GetTitle();
      std::string xlabel;
      if (h.GetXLabel()) xlabel = h.GetXLabel();
      std::string nlabel;
      if (h.GetNLabel()) nlabel = h.GetNLabel(); 
   
      int nbin = h.GetNBins();
      double* edge = new double[nbin+1];
      h.GetBinLowEdges(edge);
   
      double* content = new double[nbin+2];
      h.GetBinContents(content);
   
      double* error = 0;
      if (h.IsErrorFlagON()) {
         error  = new double[nbin+2];
         h.GetBinErrors(error);
      }
   
      int nent = h.GetNEntries();
   
      double* stat = new double[4];
      h.GetStats(stat);
   
      int type = (bool)h.GetBinType();
   
      if (!h.IsErrorFlagON()) {
         Plot(new HistDataCopy(edge, content, nbin, xlabel.c_str(),
			       nlabel.c_str(), nent, stat, type),
	      "1-D Histogram", title.c_str());
      }
      else {
         Plot(new HistDataCopy(edge, content, error, nbin, xlabel.c_str(),
			       nlabel.c_str(), nent, stat, type),
	      "1-D Histogram", title.c_str());
      }
   
      if (edge) delete[] edge;
      if (content) delete[] content;
      if (error) delete[] error;
      if (stat) delete[] stat;
   
   }


   void PlotRootHistogram(const Histogram1& h, const char* opt)
   {
      char name[20] = "c";
      int i = 0;
      while (gROOT->FindObject(name)) {
         sprintf(name,"c%d",i);
         i++;
      }
      TCanvas* c = new TCanvas(name,"1-D Histogram",600,400);
   
      std::string title, hname,objname;
      if (h.GetTitle()) {
         title = h.GetTitle();
         hname = h.GetTitle();
         objname = h.GetTitle();
      }
      else {
         title = "1-D Histogram";
         hname = "1-D Histogram";
         objname = "1-D Histogram";
      }
      i = 0;
      while(gROOT->FindObject(objname.c_str())) {
         char sfx[10];
         sprintf(sfx,"(%d)",i);
         objname = hname + sfx;
         ++i;
      }
   
      TH1D* rh = new TH1D(objname.c_str(),title.c_str(),10,0.,1.);
      ConvertHistogram(h,*rh);
      rh->Draw(opt);
      c->Update();
   }


/// plot 2-D histogram
   void PlotRootHistogram(const Histogram2& h, const char* opt)
   {
      char name[20] = "c";
      int i = 0;
      while (gROOT->FindObject(name)) {
         sprintf(name,"c%d",i);
         i++;
      }
   
      TCanvas* c = new TCanvas(name,"2-D Histogram",600,400);
   
      std::string title, hname,objname;
      if (h.GetTitle()) {
         title = h.GetTitle();
         hname = h.GetTitle();
         objname = h.GetTitle();
      }
      else {
         title = "2-D Histogram";
         hname = "2-D Histogram";
         objname = "2-D Histogram";
      }
      i = 0;
      while(gROOT->FindObject(objname.c_str())) {
         char sfx[10];
         sprintf(sfx,"(%d)",i);
         objname = hname + sfx;
         ++i;
      }
   
      TH2D* rh = new TH2D(objname.c_str(),title.c_str(),10,0.,1.,10,0.,1.);
      ConvertHistogram(h,*rh);
      rh->Draw(opt);
      c->Update();
   }

//}
