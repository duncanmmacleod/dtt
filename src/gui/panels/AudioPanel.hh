#ifndef AUDIOGUI_HH
#define AUDIOGUI_HH

#if !defined(__CINT__) || defined(__MAKECINT__)
#include "TGFrame.h"
#endif

class Time;
class TPad;
class TGLabel;
class TGTextEntry;
class TGCheckButton;
class TGHSlider;

#define MaxVolume 256


/**  This class produces a Root-based GUI for the eval macro. Full 
  *  documentation is contained in the header file. 
  *
  *  Note that because of the CINT limitation on inheriting from compiled
  *  base classes this class is actually two in one. If the class is 
  *  compiled, it inherits from TMainFrame and all button handlers are
  *  associated with the mainframe process event. If the class is interpreted
  *  the Main frame is a separate object and the button handlers use the
  *  setcommand kludge.
  */
class AudioPanel 
#if !defined(__CINT__) || defined(__MAKECINT__)
  : public TGMainFrame
#endif

{
public:

  /**  Enumerate the widgets contained in the evaluate panel.
    *  @memo Widget IDs.
    */
    enum WidgetID {
      WID_SlVolume,
      WID_CBMute,
      WID_CBSpeaker,
      WID_CBPhones
    };

    /**  Construct a gui panel for the eval macro. An optional configuration
      *  file can be specified as an argument.
      *  @memo Default constructor.
      *  @param file Configuration file name.
      */
    AudioPanel(void);

    /**  Panel destructor.
     */
    ~AudioPanel(void);

    /**  Get the formula to evaluate.
      *  @memo Get the evaluation formula.
      *  @return Pointer to the Formula string.
      */
    double getVolume(void) const;

    /**  Get the current state of the 'Frequency Domain' checkbox.
      *  @memo Frequency button state.
      *  @return true if the frequency box is checked.
      */
    bool  getMute(void) const;

    /**  Get the current state of the 'Accumulate' checkbox.
      *  @memo Accumulate button state.
      *  @return true if the accumulate box is checked.
      */
    int getMode(void) const;

    void setTime(const Time& t);
    void setVolume(double vol);
    void setMode(int mode);
    void setMute(bool mute);

 private:
    void Update(void) const;
    Bool_t ProcessMessage(long msg, long par1, long par2);

 private:
    /**  List of objects to be deleted.
     */
    TList* mBin;

    /**  Main Frame pointer. Set to this when TGMainFrame is inherited.
     */
    TGMainFrame* mFrMain;

    /**  Frame containing all of the MainFrame.
     */
    TGCompositeFrame*    mFrAll;

    /**  Control subframe.
     */
    TGCompositeFrame*    mFrControl;

    /**  Time label
     */
    TGLabel*             mLbTime;

    /**  Speaker check button.
     */
    TGCheckButton*       mCBMute;

    /**  Speaker check button.
     */
    TGCheckButton*       mCBSpeaker;

    /**  Headphone check button.
     */
    TGCheckButton*       mCBPhones;

    /**  Headphone check button.
     */
    TGHSlider*           mSlVolume;
};

#endif // AUDIOGUI_HH

