#ifndef EVALGUI_HH
#define EVALGUI_HH

#if !defined(__CINT__) || defined(__MAKECINT__)
#include "TGFrame.h"
#endif

class TPad;
class TGTextEntry;
class TGCheckButton;
class TRootEmbeddedCanvas;

/**  This class produces a Root-based GUI for the eval macro. Full 
  *  documentation is contained in the header file. 
  *
  *  Note that because of the CINT limitation on inheriting from compiled
  *  base classes this class is actually two in one. If the class is 
  *  compiled, it inherits from TMainFrame and all button handlers are
  *  associated with the mainframe process event. If the class is interpreted
  *  the Main frame is a separate object and the button handlers use the
  *  setcommand kludge.
  */
class EvalPanel 
#if !defined(__CINT__) || defined(__MAKECINT__)
  : public TGMainFrame
#endif

{
public:

  /**  Enumerate the widgets contained in the evaluate panel.
    *  @memo Widget IDs.
    */
    enum WidgetID {
      WID_TEChan1,
      WID_TEChan2,
      WID_TEChan3,
      WID_TEChan4,
      WID_TEChan5,
      WID_TEChan6,
      WID_TEChan7,
      WID_TEChan8,
      WID_TEEval,
      WID_TETime, 
      WID_BuReset,
      WID_BuQuit,
      WID_BuFetch,
      WID_BuSave,
      WID_CBFFT,
      WID_CBAcc
    };


    /**  Construct a gui panel for the eval macro. An optional configuration
      *  file can be specified as an argument.
      *  @memo Default constructor.
      *  @param file COnfiguration file name.
      */
    EvalPanel(const char* file=0);

    /**  Panel destructor.
     */
    ~EvalPanel(void);

    /**  Get a pointer to the siplay pad.
      *  @memo Display pad address
      */
    TPad* getOutPad(void) const;

    /**  Get the formula to evaluate.
      *  @memo Get the evaluation formula.
      *  @return Pointer to the Formula string.
      */
    const char* getEval(void) const;

    /**  Get the i'th channel name.
      *  @memo Channel name.
      *  @param i Channel number from 0 - N-1.
      *  @return Pointer to the channel name string.
      */
    const char* getChannel(int i) const;

    /**  Get the current state of the 'Frequency Domain' checkbox.
      *  @memo Frequency button state.
      *  @return true if the frequency box is checked.
      */
    bool  getFdomain(void) const;

    /**  Get the current state of the 'Accumulate' checkbox.
      *  @memo Accumulate button state.
      *  @return true if the accumulate box is checked.
      */
    bool  getAccum(void) const;

    /**  Get the contents of the Time box converted to a double precision 
      *  float.
      *  @memo Get the specified Time interval.
      *  @return The specified time interval.
      */
    double getTime(void) const;

    /**  Clear the change flag.
      *  @memo Reset Change;
      */
    void clearChange();

    /**  Test the current state of the Change flag. The change flag is set 
      *   whenever the Rese or the Quit button is pushed.
      *  @memo Test the change flag.
      *  @return true if a chaange has been recorded.
      */
    bool change(void) const;

    /**  Test the current state of the Run flag. The run flag is set to
      *  true when the panel is constructed, and set to false when the
      *  Quit button is pushed.
      *  @memo test the Run flag.
      *  @return true if the run flag is set.
      */
    bool run(void) const;

    /**  Set the Panel state from a configuration file.
      *  @memo Read a configuration file.
      *  @param file File name.
      */
    void Fetch(const char* file);

    /**  Record the Panel state in the specified configuration file.
      *  @memo Write a configuration file.
      *  @param file File name.
      */
    void Save(const char* file) const;

    Bool_t ProcessMessage(long msg, long par1, long par2);

 private:
    void Update(void) const;
    TGTextEntry* LTEntry(TGCompositeFrame* Frame, const char* Label, int ID);

 private:
    /**  Run flag.
     */
    bool  mRun;

    /**  Change flag.
     */
    bool  mChange;

    /**  List of objects to be deleted.
     */
    TList*               mBin;

    /**  Main Frame pointer. Set to this when TGMainFrame is inherited.
     */
    TGMainFrame*         mFrMain;

    /**  Frame containing all of the MainFrame.
     */
    TGCompositeFrame*    mFrAll;

    /**  Control subframe.
     */
    TGCompositeFrame*    mFrControl;

    /**  Channel name grou subframe.
     */
    TGGroupFrame*        mGFEval;

    /**  Individual channel name Text entry objects.
     */
    TGTextEntry*         mTEChan[8];

    /**  Evaluation formula text entry object.
     */
    TGTextEntry*         mTEEval;

    /**  Time text entry object.
     */
    TGTextEntry*         mTETime;

    /**  Command button grou frame.
     */
    TGGroupFrame*        mGFCmds;

    /**  Frequency domain check button.
     */
    TGCheckButton*       mCBFFT;

    /**  Pointer to the Accumulate mode check button.
     */
    TGCheckButton*       mCBAccum;

    /**  Output Canvas sub-frame.
     */
    TGCompositeFrame*    mFrHist;

    /**  Output canvas.
     */
    TRootEmbeddedCanvas* mCaOutput;
};

#endif // EVALGUI_HH
