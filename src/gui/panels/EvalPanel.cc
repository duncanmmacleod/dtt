/* -*- mode: c++; c-basic-offset: 4; -*- */
//  This class produces a Root-based GUI for the eval macro. Full 
//  documentation is contained in the header file. 
//
//  Note that because of the CINT limitation on inheriting from compiled
//  base classes this class is actually two in one. If the class is 
//  compiled, it inherits from TMainFrame and all button handlers are
//  associated with the mainframe process event. If the class is interpreted
//  the Main frame is a separate object and the button handlers use the
//  setcommand kludge.

#include "EvalPanel.hh"

#ifndef __CINT__
#include "RVersion.h"
#include "TGButton.h"
#include "TGLabel.h"
#include "TGTextBuffer.h"
#include "TGTextEntry.h"
#include "TRootEmbeddedCanvas.h"
#include "TList.h"
#include "TPad.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TGFileDialog.h"
#include <iostream>
#include <fstream>
#include <cstdlib>
#endif

using namespace std;

//--------------------------------------  Labeled Text entry box
TGTextEntry*
EvalPanel::LTEntry(TGCompositeFrame* Frame, const char* LblText, int ID) {
    TGTextEntry* Entry;
    TGCompositeFrame* CFrame;
    TGLabel* Label;

    CFrame = new TGCompositeFrame(Frame,100,25,kHorizontalFrame);
    mBin->Add(CFrame);
    Frame->AddFrame(CFrame,  new TGLayoutHints(kLHintsLeft|kLHintsTop));

    Label = new TGLabel(CFrame, LblText);
    mBin->Add(Label);
    CFrame->AddFrame(Label, new TGLayoutHints(kLHintsLeft|kLHintsTop));

    Entry   = new TGTextEntry(CFrame, "", ID);
    mBin->Add(Entry);
    CFrame->AddFrame(Entry, new TGLayoutHints(kLHintsLeft|kLHintsTop));
    Entry->Resize(200,25);
    return Entry;
}

#ifdef __CINT__

EvalPanel::EvalPanel(const char* file)
  : mRun(true), mChange(false)
{
    char cmd[128];
    sprintf(cmd, 
	    "((EvalPanel*)0x%x)->ProcessMessage($MSG,$PARM1,$PARM2);", 
	    (int) this);
    mFrMain    = new TGMainFrame(gClient->GetRoot(),1,1);

#else

EvalPanel::EvalPanel(const char* file)
  : TGMainFrame(gClient->GetRoot(),1,1), mRun(true), mChange(false)
{
    mFrMain = this;

#endif

    mBin = new TList;
    mFrAll     = new TGCompositeFrame(mFrMain, 100, 100, kVerticalFrame);
    mFrMain->AddFrame(mFrAll, new TGLayoutHints(kLHintsLeft|kLHintsTop));

    //---------------------------------  Add control and Hist subframes
    mFrControl = new TGCompositeFrame(mFrAll,  100,  50, kHorizontalFrame);
    mFrAll->AddFrame(mFrControl, new TGLayoutHints(kLHintsLeft|kLHintsTop));

    mFrHist    = new TGCompositeFrame(mFrAll, 100,  50, kHorizontalFrame);
    mFrAll->AddFrame(mFrHist, new TGLayoutHints(kLHintsLeft|kLHintsTop));

    //---------------------------------  Big Fat Ugly canvas in lower area
    mCaOutput  = new TRootEmbeddedCanvas("Strip", mFrHist, 400, 400);
    mFrHist->AddFrame(mCaOutput, new TGLayoutHints(kLHintsLeft|kLHintsTop));

    //---------------------------------  Evaluation entries.
    mGFEval    = new TGGroupFrame(mFrControl, "Channels", kVerticalFrame);
    mFrControl->AddFrame(mGFEval, new TGLayoutHints(kLHintsLeft|kLHintsTop));
    char Label[80];
    for (int i=0 ; i<8 ; i++) {
        sprintf(Label, "Channel %i", i+1);
        mTEChan[i] = LTEntry(mGFEval, Label, WID_TEChan1+i);
    }

    TGCompositeFrame* Frame;
    Frame = new TGCompositeFrame(mFrControl, 100,  50, kVerticalFrame);
    mFrControl->AddFrame(Frame, new TGLayoutHints(kLHintsLeft|kLHintsTop));
    mBin->Add(Frame);
    
    mTEEval = LTEntry(Frame, "Evaluate: ", WID_TEEval);
    mTETime = LTEntry(Frame, "Time: ", WID_TETime);

    //--------------------------------- Add control frame buttons.
    TGTextButton* Button;
    mGFCmds    = new TGGroupFrame(Frame, "Commands", kHorizontalFrame);
    Frame->AddFrame(mGFCmds, new TGLayoutHints(kLHintsLeft|kLHintsTop));

    Button     = new TGTextButton(mGFCmds, "Reset", WID_BuReset);
#ifdef __CINT__
    Button->SetCommand(cmd);
#else
    Button->Associate(this);
#endif
    mBin->Add(Button);
    mGFCmds->AddFrame(Button, new TGLayoutHints(kLHintsLeft|kLHintsTop));
    Button->Resize(20,30);

    Button     = new TGTextButton(mGFCmds, "Quit", WID_BuQuit);
#ifdef __CINT__
    Button->SetCommand(cmd);
#else
    Button->Associate(this);
#endif
    mBin->Add(Button);
    mGFCmds->AddFrame(Button, new TGLayoutHints(kLHintsLeft|kLHintsTop));
    Button->Resize(20,30);

    Button     = new TGTextButton(mGFCmds, "Fetch", WID_BuFetch);
#ifdef __CINT__
    Button->SetCommand(cmd);
#else
    Button->Associate(this);
#endif
    mBin->Add(Button);
    mGFCmds->AddFrame(Button, new TGLayoutHints(kLHintsLeft|kLHintsTop));
    Button->Resize(20,30);

    Button     = new TGTextButton(mGFCmds, "Save", WID_BuSave);
#ifdef __CINT__
    Button->SetCommand(cmd);
#else
    Button->Associate(this);
#endif
    mBin->Add(Button);
    mGFCmds->AddFrame(Button, new TGLayoutHints(kLHintsLeft|kLHintsTop));
    Button->Resize(20,30);

    mCBFFT  = new TGCheckButton(Frame, "Frequency Domain", WID_CBFFT);
#ifdef __CINT__
    Button->SetCommand(cmd);
#else
    Button->Associate(this);
#endif
    Frame->AddFrame(mCBFFT, new TGLayoutHints(kLHintsLeft|kLHintsTop));
    mCBFFT->Resize(60,30);

    mCBAccum = new TGCheckButton(Frame, "Accumulate", WID_CBAcc);
#ifdef __CINT__
    Button->SetCommand(cmd);
#else
    Button->Associate(this);
#endif
    Frame->AddFrame(mCBAccum, new TGLayoutHints(kLHintsLeft|kLHintsTop));
    mCBAccum->Resize(60,30);

    //---------------------------------  Pop the window up on the screen.
    mFrMain->SetWindowName("Eval Monitor");
    if (file && *file) Fetch(file);
    Update();
}

TPad* 
EvalPanel::getOutPad(void) const {
    return mCaOutput->GetCanvas();
}

const char*
EvalPanel::getEval(void) const {
    return mTEEval->GetText();
}

const char*
EvalPanel::getChannel(int i) const {
    if (i<0 || i>=8) return "";
    return mTEChan[i]->GetText();
}

double
EvalPanel::getTime(void) const {
    const char* s;
    s = mTETime->GetText();
    if (!s || !*s) return 1.0;
    double Ticks = strtod(s, 0);
    if (Ticks == 0.0) Ticks = 1.0;
    return Ticks;
}

bool
EvalPanel::getFdomain(void) const {
    return (mCBFFT->GetState() == kButtonDown);
}

bool
EvalPanel::getAccum(void) const {
    return (mCBAccum->GetState() == kButtonDown);
}

EvalPanel::~EvalPanel(void)
{
    mBin->Delete();
    delete mCaOutput;
    delete mFrHist;
    delete mFrControl;
    delete mFrAll;
#ifdef __CINT__
    delete mFrMain;
#endif
    // for (int i=0 ; i<8 ; i++) delete mTEChan[i]; // deleted by mBin
    // delete mTEEval; // deleted by mBin
}

bool
EvalPanel::run() const 
{
    return mRun;
}

void
EvalPanel::clearChange()
{
    mChange = false;
}

bool
EvalPanel::change() const 
{
    gSystem->ProcessEvents(); // process event
    return mChange || gROOT->IsInterrupted();
}

void
EvalPanel::Update() const 
{
    mFrMain->MapSubwindows();
    mFrMain->Resize(mFrMain->GetDefaultSize());
    mFrMain->MapWindow();
}

Bool_t
EvalPanel::ProcessMessage(long msg, long p1, long p2) {
    TGFileInfo fi;
    const char *filetypes[] = { "All files",     "*",
				0,               0 };
    
    switch(GET_MSG(msg)) {
    case kC_COMMAND:
        switch(GET_SUBMSG(msg)) {
	case kCM_BUTTON:
	    switch(p1) {
	    case WID_BuReset:
	        mChange = true;
		break;
	    case WID_BuQuit:
	        mRun = false;
	        mChange = true;
		break;
	    case WID_BuFetch:
		fi.fIniDir = (char*)".";
#if ROOT_VERSION_CODE >= ROOT_VERSION(3,1,6)
		fi.fFileTypes = filetypes;
#else
		fi.fFileTypes = const_cast<char**>(filetypes);
#endif
		new TGFileDialog(gClient->GetRoot(), mFrMain, kFDOpen, &fi);
		Fetch(fi.fFilename);
#if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
                delete [] fi.fFilename;
#endif
		mChange = true;
		break;
	    case WID_BuSave:
		fi.fIniDir = (char*)".";
#if ROOT_VERSION_CODE >= ROOT_VERSION(3,1,6)
		fi.fFileTypes = filetypes;
#else
		fi.fFileTypes = const_cast<char**>(filetypes);
#endif
		new TGFileDialog(gClient->GetRoot(), mFrMain, kFDOpen, &fi);
		Save(fi.fFilename);
#if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
                delete [] fi.fFilename;
#endif
		break;
	    default:
	        break;
	    }
	default:
	    break;
	}
    default:
        break;
    }
    return true;
}

void
EvalPanel::Save(const char* file) const 
{
    ofstream ostr(file);
    for (int i=0 ; i<8 ; i++) ostr << "C" << i << ": " 
				   << mTEChan[i]->GetText() << endl;
    ostr << "Evaluate: " << mTEEval->GetText() << endl;
    ostr << "Time: "     << mTETime->GetText() << endl;
    ostr << "Frequency: "  << mCBFFT->GetState()   << endl;
    ostr << "Accumulate: " << mCBAccum->GetState() << endl;
    ostr.close();
}

void
EvalPanel::Fetch(const char* file) 
{
    char line[80], text[80];
    int s;

    cout << "Reading configuration file: " << file << endl;
    ifstream istr(file);
    for (int i=0 ; i<8 ; i++) {
        *text = 0;
        istr.getline(line,sizeof(line));
	sscanf(line, "C%i: %s", &s, text);
	mTEChan[i]->SetText(text);
    }

    istr.getline(line,sizeof(line));
    sscanf(line, "Evaluate: %s", text);
    mTEEval->SetText(text);

    istr.getline(line,sizeof(line));
    sscanf(line, "Time: %s", text);
    mTETime->SetText(text);

    istr.getline(line,sizeof(line));
    sscanf(line, "Frequency: %i", &s);
    EButtonState state = (EButtonState)s;
    mCBFFT->SetState(state);

    istr.getline(line,sizeof(line));
    sscanf(line, "Accumulate: %i", &s);
    state = (EButtonState)s;
    mCBAccum->SetState(state);

    istr.close();
}

