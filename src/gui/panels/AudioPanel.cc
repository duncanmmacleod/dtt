//  This class produces a Root-based GUI for an audio interface. Full 
//  documentation is contained in the header file. 
//
//  Note that because of the CINT limitation on inheriting from compiled
//  base classes this class is actually two in one. If the class is 
//  compiled, it inherits from TMainFrame and all button handlers are
//  associated with the mainframe process event. If the class is interpreted
//  the Main frame is a separate object and the button handlers use the
//  setcommand kludge.

#include "AudioPanel.hh"

#ifndef __CINT__
#include "Time.hh"
#include "TGButton.h"
#include "TGSlider.h"
#include "TGString.h"
#include "TGLabel.h"
#include "TGTextBuffer.h"
#include "TList.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TGFileDialog.h"
#include <fstream>
#endif

using namespace std;

#ifdef __CINT__

AudioPanel::AudioPanel(void)
{
    char cmd[128];
    sprintf(cmd, 
	    "((AudioPanel*)0x%x)->ProcessMessage($MSG,$PARM1,$PARM2);", 
	    (int) this);
    mFrMain    = new TGMainFrame(gClient->GetRoot(),1,1);

#else

AudioPanel::AudioPanel(void)
  : TGMainFrame(gClient->GetRoot(),1,1)
{
    mFrMain = this;

#endif

    mBin = new TList;
    mFrAll     = new TGCompositeFrame(mFrMain, 300, 300, kVerticalFrame);
    mFrMain->AddFrame(mFrAll, new TGLayoutHints(kLHintsLeft|kLHintsTop));

    //---------------------------------  Add control and Hist subframes
    mFrControl = new TGCompositeFrame(mFrAll,  300,  300, kVerticalFrame);
    mFrAll->AddFrame(mFrControl, new TGLayoutHints(kLHintsLeft|kLHintsTop));

    mLbTime = new TGLabel(mFrControl, "Time:         ");
    mFrControl->AddFrame(mLbTime, new TGLayoutHints(kLHintsLeft|kLHintsTop));
    mLbTime->Resize(60,20);

    mCBSpeaker = new TGCheckButton(mFrControl, "Speaker", WID_CBSpeaker);
#ifdef __CINT__
    mCBSpeaker->SetCommand(cmd);
#else
    mCBSpeaker->Associate(this);
#endif
    mFrControl->AddFrame(mCBSpeaker, new TGLayoutHints(kLHintsLeft|kLHintsTop));
    mCBSpeaker->Resize(60,20);
    mCBSpeaker->SetState(kButtonDown);

    mCBPhones = new TGCheckButton(mFrControl, "Phones", WID_CBPhones);
#ifdef __CINT__
    mCBPhones->SetCommand(cmd);
#else
    mCBPhones->Associate(this);
#endif
    mFrControl->AddFrame(mCBPhones, new TGLayoutHints(kLHintsLeft|kLHintsTop));
    mCBPhones->Resize(60,20);
    mCBPhones->SetState(kButtonDown);

    mCBMute = new TGCheckButton(mFrControl, "Mute", WID_CBMute);
#ifdef __CINT__
    mCBMute->SetCommand(cmd);
#else
    mCBMute->Associate(this);
#endif
    mFrControl->AddFrame(mCBMute, new TGLayoutHints(kLHintsLeft|kLHintsTop));
    mCBMute->Resize(60,20);

    mSlVolume = new TGHSlider(mFrControl, 256, 1, WID_SlVolume);
#ifdef __CINT__
    mSlVolume->SetCommand(cmd);
#else
    mSlVolume->Associate(this);
#endif
    mFrControl->AddFrame(mSlVolume, new TGLayoutHints(kLHintsLeft|kLHintsTop));
    mSlVolume->Resize(100,20);

    //---------------------------------  Pop the window up on the screen.
    mFrMain->SetWindowName("Audio Monitor");
    Update();
}

void
AudioPanel::setTime(const Time& t) {
    char label[80];
    LocalStr(t, label, "Time: %Y-%m-%d %H:%N:%S");
    TGString* text = new TGString(label);
    mLbTime->SetText(text);
    Layout();
    Update();
}

double
AudioPanel::getVolume(void) const {
    return double(mSlVolume->GetPosition())/MaxVolume;
}

void
AudioPanel::setVolume(double x) {
    mSlVolume->SetPosition(int(x*MaxVolume));
}

bool
AudioPanel::getMute(void) const {
    return (mCBMute->GetState() == kButtonDown);
}

void
AudioPanel::setMute(bool mute) {
    if (mute) mCBMute->SetState(kButtonDown);
    else      mCBMute->SetState(kButtonUp);
}

int
AudioPanel::getMode(void) const {
    int mode = 0;
    if (mCBSpeaker->GetState() == kButtonDown) mode  = 1;
    if (mCBPhones->GetState()  == kButtonDown) mode += 2;
    return mode;
}

void
AudioPanel::setMode(int mode) {
    if ((mode & 1)) mCBSpeaker->SetState(kButtonDown);
    else            mCBSpeaker->SetState(kButtonUp);
    if ((mode & 2)) mCBPhones ->SetState(kButtonDown);
    else            mCBPhones ->SetState(kButtonUp);
}

AudioPanel::~AudioPanel(void)
{
    mBin->Delete();
    delete mCBMute;
    delete mCBPhones;
    delete mCBSpeaker;
    delete mFrControl;
    delete mFrAll;
#ifdef __CINT__
    delete mFrMain;
#endif
}

void
AudioPanel::Update() const 
{
    mFrMain->MapSubwindows();
    mFrMain->Resize(mFrMain->GetDefaultSize());
    mFrMain->MapWindow();
}

Bool_t
AudioPanel::ProcessMessage(long msg, long p1, long p2) {
    return true;
}

