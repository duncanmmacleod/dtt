/* Version $Id: excitation.cc 8197 2019-10-23 00:26:35Z erik.vonreis@LIGO.ORG $ */
/* -*- mode: c++; c-basic-offset: 3; -*- */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: excitation						*/
/*                                                         		*/
/* Module Description: implements a excitation channel management	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

/* Header File List: */
#include <time.h>
#include <unistd.h>
#include "gdsutil.h"
#include "excitation.hh"
#include "testpoint.h"
#include "testpointinfo.h"
#include "rmorg.h"
#include "awgfunc.h"
#include "awgapi.h"
#ifndef _ADVANCED_LIGO
#include "epics.h"
#endif
#include "gdsdatum.hh"
#include "testpointmgr.hh"
#include "FilterDesign.hh"
#include "iirutil.hh"
#include <iostream>

namespace diag {
   using namespace std;
   using namespace thread;

   static int my_debug = 0 ;

   const double excitation::syncDelay = 0.20; // sec
   const double excitation::syncUncertainty = 0.25; // sec
   const double excitation::linkSpeed[2] = 
   {500E3, 900.0}; // char/sec

   excitation::excitation (const string& Chnname, double Wait)
   : chnname (""), channeltype (invalid), writeaccess (false), 
   wait (Wait), slot (-1), inUse (1), isTP (false)
   {
      if (my_debug) cerr << "excitation::excitation(" << Chnname
			 << ", Wait = " << Wait << ")" << endl ;
      setup (Chnname);
   }


   excitation::excitation (const excitation& exc)
   {
      *this = exc;
   }


   excitation::~excitation ()
   {
      reset (true, _ONESEC);
   }


   excitation& excitation::operator= (const excitation& exc)
   {
      if (this != &exc) {
         semlock		lockit (mux);
         semlock		lockit2 (exc.mux);
         chnname = exc.chnname;
         channeltype = exc.channeltype;
         writeaccess = exc.writeaccess;
         chninfo = exc.chninfo;
         wait = exc.wait;
         signals = exc.signals;
         points = exc.points;
         slot = exc.slot;
#ifndef _ADVANCED_LIGO
         epicsvalue = exc.epicsvalue;
#endif
         /* transfer owner ship of channel */
         exc.inUse = 0;
         exc.slot = -1;
      }
      return *this;
   }


   int excitation::capability (capabilityflag cap) const
   {
      semlock		lockit (mux);
      if (channeltype == invalid) {
         return 0;
      }
      switch (cap) {
         case output:
            return (int) writeaccess;
         case GPSsync:
            return (int) ((channeltype == testpoint) || 
                         (channeltype == DAC));
         case periodicsignal:
         case randomsignal:
            return (int) ((channeltype == testpoint) || 
                         (channeltype == DAC) ||
                         (channeltype == DSG));
         case waveform:
            return (int) ((channeltype == testpoint) || 
                         (channeltype == DAC) || 
                         (channeltype == DSG));
         case multiplewaveforms:
            return (int) ((channeltype == testpoint) || 
                         (channeltype == DAC)); 
         default:
            return 0;
      }
   }


   bool excitation::setup (const string& Chnname)
   {
      semlock		lockit (mux);
   
      if (my_debug) cerr << "excitation::setup( channel=" << Chnname << ")"
			 << endl ;

      if ((channeltype != invalid) && (slot >= 0)) {
         awgRemoveChannel (slot);
      }
      channeltype = invalid;
      if (gdsChannelInfo (Chnname.c_str(), &chninfo) < 0) {
#ifndef _ADVANCED_LIGO
         // not a nds channel: check epics
         if (epicsGet (Chnname.c_str(), NULL) != 0) {
            // not a channel
            return false;
         }
         chninfo.dataRate = 1;
#else
	 return false ;
#endif
      }
      chnname = Chnname;
      slot = -1;
   
#ifndef _ADVANCED_LIGO
      // EPICS channel?
      if (chninfo.dataRate < 16) {
         if (epicsGet (chnname.c_str(), &epicsvalue) != 0) {
            // not accessible	
            return false;
         }
         channeltype = EPICS;
         writeaccess = true;
         slot = 0;
         return true;
      }
      else 
#endif
      // test point channel?
      if (tpIsValid (&chninfo, 0, 0)) {
         channeltype = testpoint;
         writeaccess = true;
         slot = awgSetChannel (chnname.c_str());
	 if (my_debug) cerr << "excitation::setup() return slot=" << slot
			    << endl ;
         return (slot >= 0);
      }
      // not a valid channel
      else {
	 if (my_debug) cerr << "excitation::setup() return invalid slot"
			    << endl ;
         return false;
      }
   }


   bool excitation::add (const AWG_Component& comp)
   {
      semlock		lockit (mux);
   
      if (my_debug) cerr << "excitation::add( AWG_Component )" << endl ;

      if ((channeltype == invalid) || 
         (!capability (output))) {
         if (my_debug) cerr << "excitation::add() return false, line "
			    << __LINE__ << endl ;
         return false;
      }
      /* check for valid waveform component */
      if (comp.wtype == awgNone) {
         if (my_debug) cerr << "excitation::add() return true, line "
			    << __LINE__ << endl ;
         return true;
      }
      if (awgIsValidComponent (&comp) == 0) {
         if (my_debug) cerr << "excitation::add() return false, line "
			    << __LINE__ << endl ;
         return false;
      }
      if (!capability (multiplewaveforms) &&
         (signals.size() > 0)) {
         if (my_debug) cerr << "excitation::add() return false, line "
			    << __LINE__ << endl ;
         return false;
      }
      if (!capability (periodicsignal) && 
         ((comp.wtype == awgSine) ||
         (comp.wtype == awgSquare) ||
         (comp.wtype == awgRamp) ||
         (comp.wtype == awgTriangle) ||
         (comp.wtype == awgImpulse))) {
         if (my_debug) cerr << "excitation::add() return false, line "
			    << __LINE__ << endl ;
         return false;
      }
      if (!capability (randomsignal) && 
         ((comp.wtype == awgNoiseN) ||
         (comp.wtype == awgNoiseU))) {
         if (my_debug) cerr << "excitation::add() return false, line "
			    << __LINE__ << endl ;
         return false;
      }
      /* make sure valid parameters for non-AWG devices */
      AWG_Component 	c (comp);
      if (!capability (waveform)) {
         /* advanced features */
         c.duration = -1;
         c.restart = -1;
         c.ramptype = RAMP_TYPE (AWG_PHASING_STEP, 
                              AWG_PHASING_STEP, AWG_PHASING_STEP);
         c.ramptime[0] = 0;
         c.ramptime[1] = 0;	
      }
      /* add waveform */
      if (my_debug) {
         cerr << "  c.start = " << (c.start / _ONESEC) << '.'
	      << (c.start % _ONESEC) << endl ;
         cerr << "  c.duration = " << c.duration << endl ;
         cerr << "  c.restart = " << c.restart << endl ;
         cerr << "  c.ramptime[0] = " << c.ramptime[0] << endl ;
         cerr << "  c.ramptime[1] = " << c.ramptime[1] << endl ;
         cerr << "excitation::add() return true" << endl ;
      }
      signals.push_back (c);
      return true;
   }


   bool excitation::add (const pointlist& Points)
   {
      if (my_debug) cerr << "excitation::add( pointlist )" << endl ;
      points = Points;
      if (my_debug) cerr << "excitation::add() return" << endl ;
      return true;
   }


   bool excitation::add (const_sigiterator begin, 
                     const_sigiterator end) 
   {
      if (my_debug) cerr << "excitation::add( sigiterator )" << endl ;
      for (const_sigiterator iter = begin; iter != end; iter++) {
         if (!add (*iter)) {
            return false;
         }
      }
      if (my_debug) cerr << "excitation::add() return" << endl ;
      return true;
   }


   double excitation::dwellTime () const
   {
      double dwell = wait;
   
      if (my_debug) cerr << "excitation::dwellTime() for channel "
			 << chnname << endl ;
      if (my_debug) cerr << "  wait = " << wait << endl ;

      /* add uncertainly for no GPS units */
      if (capability (GPSsync)) {
	 if (my_debug) cerr << "  dwell += syncDelay, syncDelay = "
			    << syncDelay << endl;
         dwell += syncDelay;
      }
      else {
	 if (my_debug) cerr << "  dwell += syncUncertainty, syncUncertainty = "
			    << syncUncertainty << endl ;
         dwell += syncUncertainty;
      }
      /* estimate download time for arbitrary waveform */
      if (capability (waveform) && !points.empty()) {
         switch (channeltype) {
            case DSG : 
               dwell += sizeof (short) * points.size() / linkSpeed[1]; 
               break;
            case testpoint:
            case DAC:
               dwell += sizeof (float) * points.size() / linkSpeed[0];
               break;
            default:
               break;
         }
      }
      if (my_debug) {
	cerr << "excitation::dwellTime() return dwell = " << dwell << endl;
      }
      return dwell;
   }


    bool excitation::start(tainsec_t start, tainsec_t timeout, tainsec_t rampup)
    {
        semlock lockit(mux);

        if (my_debug)
        {
            cerr << "excitation::start( start=" << start <<
                 ", timeout=" << timeout << ", rampup=" <<
                 rampup << ")" << endl;
        }

        if ((channeltype == invalid) ||
            (!capability(output)))
        {
            return 0;
        }
        if (signals.size() == 0)
        {
            return true;
        }
        /* sort awg components according to time */
        awgSortComponents(&(*signals.begin()), signals.size());
        /* patch start time */
        if (capability(GPSsync) && (start >= 0))
        {
            tainsec_t dt = 0;
            if (signals.size() > 0)
            {
                dt = start - signals.front().start;
            }
            for (signallist::iterator iter = signals.begin();
                 iter != signals.end(); iter++)
            {
                iter->start += dt;
            }
        }
        /* Adjust for ramp up time. */
        if (rampup > 0)
        {
            if (my_debug) cerr << "  excitation::start(), applying rampup to signallist" << endl;
            for (signallist::iterator iter = signals.begin();
                 iter != signals.end(); iter++)
            {
                iter->start += rampup;
            }
        }
        else if (my_debug) cerr << "  excitation::start(), rampup == 0" << endl;


        /* download waveform points */
        if ((channeltype == testpoint) || (channeltype == DAC) ||
            (channeltype == DSG))
        {
            if (!points.empty())
            {
                if (my_debug) cerr << "  excitation::start(), call awgSetWaveform()" << endl;
                if (awgSetWaveform(slot, &(*points.begin()), points.size()) < 0)
                {
                    return false;
                }
            }
        }

        /* set filter */
        if ((channeltype == testpoint) || (channeltype == DAC))
        {
            // reset if empty
            if (filtercmd.empty())
            {
                double y;
                awgSetFilter(slot, &y, 0);
            }
                // set otherwise
            else
            {
                FilterDesign ds((double) chninfo.dataRate);
                if (!ds.filter(filtercmd.c_str()) || !isiir(ds()))
                {
                    return false;
                }
                int nba = 4 * iirsoscount(ds()) + 1;
                double *ba = new double[nba];
                if (!iir2z(ds(), nba, ba))
                {
                    return false;
                }
                if (awgSetFilter(slot, ba, nba) < 0)
                {
                    delete[] ba;
                    return false;
                }
                delete[] ba;
            }
        }

        /* switch on excitation signal */
        switch (channeltype)
        {
#ifndef _ADVANCED_LIGO
            case EPICS:
               {
                  /* epics channel */
                  double d = signals.empty() ? 0 : signals.front().par[3];
                  cerr << "set EPICS channel to " << d << endl;
                  if (epicsPut (chnname.c_str(), d) != 0) {
                     cerr << "channel " << chnname <<
                        " not accessible" << endl;
                     return false;
                  }
                  break;
               }
#endif
            case DSG:
            case testpoint:
            case DAC:
            {
                if (my_debug)
                {
                    cerr << "switch on excitation, signals.size = " << signals.size() << endl;
                    for (const_sigiterator iter = signals.begin();
                         iter != signals.end(); iter++)
                    {
                        cerr << "   start = " << (double) (iter->start % (100 * _ONESEC)) / 1E9;
                        cerr << "   durat = " << (double) iter->duration / 1E9 << endl;
                        cerr << "   wtype = " << (int) (iter->wtype) << endl;
                        cerr << "   restart = " << (double) (iter->restart) << endl;
                        cerr << "   ramptype = " << (int) (iter->ramptype) << endl;
                        cerr << "   ramptime[0] = " << (int) (iter->ramptime[0]) << endl;
                        cerr << "   ramptime[1] = " << (int) (iter->ramptime[1]) << endl;
                    }
                }
                if (awgAddWaveform(slot, &(*signals.begin()),
                                   signals.size()) < 0)
                {
                    return false;
                }
                break;
            }
            default:
            {
                return false;
            }
        }
        if (my_debug) cerr << "excitation::start() return true" << endl;
        return true;
    }


   bool excitation::stop (tainsec_t timeout, tainsec_t ramptime)
   {
      semlock		lockit (mux);
   
      if (my_debug) cerr << "excitation::stop( timeout=" << timeout << ", ramptime=" << ramptime << ")" << endl ;
      if (slot < 0) {
         return true;
      }
   
      signals.clear();
      switch (channeltype) {
#ifndef _ADVANCED_LIGO
         case EPICS:
            /* epics channel */
            cerr << "set EPICS channel to " << epicsvalue << endl;
            if (epicsPut (chnname.c_str(), epicsvalue) != 0) {
               cerr << "channel " << chnname << 
                  " not accessible" << endl;
               return false;
            }
            return true;
#endif
         case DSG:
         case testpoint:
         case DAC:
            if (ramptime <= 0) {
               if (awgClearWaveforms (slot) < 0) {
		  if (my_debug) cerr << "excitation::stop() return false line " << __LINE__ << endl ;
                  return false;
               }
            }
            else {
               if (awgStopWaveform (slot, 2, ramptime) < 0) {
		  if (my_debug) cerr << "excitation::stop() return false line " << __LINE__ << endl ;
                  return false;
               }
            }
	    if (my_debug) cerr << "excitation::stop() return true line " << __LINE__ << endl ;
            return true;
         default:
	    if (my_debug) cerr << "excitation::stop() return true line " << __LINE__ << endl ;
            return true;
      }
   }


   bool excitation::freeze ()
   {
      semlock		lockit (mux);
   
      if (slot < 0) {
         return true;
      }
   
      signals.clear();
      switch (channeltype) {
#ifndef _ADVANCED_LIGO
         case EPICS:
            {
            /* epics channel */
               if (epicsGet (chnname.c_str(), &epicsvalue) != 0) {
                  cerr << "channel " << chnname << 
                     " not accessible" << endl;
                  return false;
               }
               return true;
            }
#endif
         case DSG:
         case testpoint:
         case DAC: 
            {
               if (awgStopWaveform (slot, 1, 0) < 0) {
                  return false;
               }
               return true;
            }
         default:
            {
               return true;
            }
      }
   }

   bool excitation::setGain(double gain, tainsec_t ramptime)
   {
      semlock		lockit (mux);
   
      if (slot < 0) {
         return true;
      }
   
      signals.clear();

      switch(channeltype)
      {
      case DSG:
      case testpoint:
      case DAC:
         if (awgSetGain(slot, gain, ramptime))
         {
            return false;
         }
         else
         {
            return true;
         }
      default:
         return true;
      }
   }

   bool excitation::reset (bool hard, tainsec_t timeout)
   {
      semlock		lockit (mux);
   
      signals.clear();
      points.clear();
   
      if (slot < 0) {
         return true;
      }
      switch (channeltype) {
#ifndef _ADVANCED_LIGO
         case EPICS:
            /* epics channel */
            break;
#endif
         case DSG:
         case testpoint:
         case DAC:
            if (hard) {
               if (awgRemoveChannel (slot) < 0) {
                  slot = -1;
                  return false;
               }
               slot = -1;
            }
            else {
               if (awgClearWaveforms (slot) < 0) {
                  return false;
               }
            }
            break;
         default:
            return true;
      }
      return true;
   }


/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Class Name: excitationManager					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

   excitationManager::excitationManager () :
   silent (false), rampdown (0) {
   }


   bool excitationManager::init (testpointMgr& TPMgr,
                     bool Silent, tainsec_t rdown)
   {
      if (my_debug) cerr << "excitationManager::init(..., rdown=" << rdown << ")" << endl ;
      tpMgr = &TPMgr;
      silent = Silent;
      rampdown = rdown;
      return true;
   }


   bool excitationManager::start (tainsec_t start, tainsec_t timeout, 
				 tainsec_t rampup)
   {
      if (my_debug) cerr << "excitationManager::start(start=" << 
			      start << ", timeout=" << timeout <<
			      ", rampup=" << rampup << ")" << endl ;
      if (silent) {
         return true;
      }
   
      bool		err = false;
      semlock		lockit (mux);
   
      if (start == 0) {
         start = TAInow();
      }
   
      for (excitationlist::iterator iter = excitations.begin();
          iter != excitations.end(); iter++) {
         if (!iter->start (start, timeout, rampup)) {
            err = true;
         }
      }
      if (my_debug) cerr << "excitationManager::start() return " << (err ? "false" : "true") << endl ;
      return !err;
   }


   bool excitationManager::stop (tainsec_t timeout, tainsec_t ramptime)
   {
      if (my_debug) cerr << "excitationManager::stop(timeout=" << timeout << 
			      ", ramptime=" << ramptime << ")" << endl ;
      if (silent) {
         return true;
      }
   
      bool		err = false;
      semlock		lockit (mux);
   
      for (excitationlist::iterator iter = excitations.begin();
          iter != excitations.end(); iter++) {
         if (!iter->stop (timeout, ramptime)) {
            err = true;
         }
      }
      if (my_debug) cerr << "excitationManager::stop() return " << (err ? "false" : "true") << endl ;
      return !err;
   }


   bool excitationManager::freeze ()
   {
      if (silent) {
         return true;
      }
   
      bool		err = false;
      semlock		lockit (mux);
   
      for (excitationlist::iterator iter = excitations.begin();
          iter != excitations.end(); iter++) {
         if (!iter->freeze ()) {
            err = true;
         }
      }
      return !err;
   }

   bool excitationManager::setGain(double gain, tainsec_t ramptime)
   {
      if(silent){
         return true;
      }

      bool err = false;
      semlock lockit(mux);

      for (excitationlist::iterator iter = excitations.begin();
          iter != excitations.end(); iter++) {
         if (!iter->setGain (gain, ramptime)) {
            err = true;
         }
      }
      return !err;
   }

   bool excitationManager::add (const string& channel)
   {
      if (my_debug) cerr << "excitationManager::add(channel=" << channel << ")" << endl ;
      if (silent) {
         return true;
      }
   
      semlock		lockit (mux);
      string		chnname (channelName (channel));
   
      // check if in list
      excitationlist::iterator 	iter;
      for (iter = excitations.begin(); 
          iter != excitations.end(); iter++) {
         if (*iter == chnname) {
            iter->inUse++;
            // set test point if required
            if ((iter->inUse == 1) && (iter->isTP) && (tpMgr != 0)) {
               tpMgr->add (iter->chnname);
            }
            break;
         }
      }
      // if not add
      if (iter == excitations.end()) {
         excitation 	exc (chnname);
         if (!exc) {
            return false;
         }
         excitations.push_back (exc);
         // add to test point list
         excitations.back().isTP = 
            (tpMgr == 0) ? false : tpMgr->add (exc.chnname);
      
         iter = excitations.end();
      }
      if (my_debug) cerr << "excitationManager::add() return true" << endl ;
      return true;
   }


   bool excitationManager::add (const string& channel, 
                     const string& waveform,
                     double settlingtime)
   {
      if (my_debug) cerr << "excitationManager::add(channel=" << channel << 
			", waveform=" << waveform << ", settlingtime=" << 
			settlingtime << ")" << endl ;
      if (silent) {
         return true;
      }
   
      semlock		lockit (mux);
      string		chnname (channelName (channel));
      AWG_Component 	comp[2]; 
      int		cnum;
      float* 		points = 0;
      int		num;
   
      // check if in list
      excitationlist::iterator 	iter;
      for (iter = excitations.begin(); 
          iter != excitations.end(); iter++) {
         if (*iter == chnname) {
            break;
         }
      }
      // if not add
      if (iter == excitations.end()) {
         if (!add (chnname)) {
            return false;
         }
         for (iter = excitations.begin(); 
             iter != excitations.end(); iter++) {
            if (*iter == chnname) {
               break;
            }
         }
         if (iter == excitations.end()) {
            return false;
         }
      }
      iter->wait = settlingtime;
   
      // now determine waveforms
      if (awgWaveformCmd (waveform.c_str(), comp, &cnum, 0, 
                         &points, &num, 
                         iter->channeltype == excitation::DSG) < 0) {
         return false;
      }
   
      if (points != 0) {
         if ((num <= 0) ||
            !iter->add (excitation::pointlist (points, points + num))) {
            awgFree (points);
            return false;
         }
         awgFree (points);
      }
   
      if (cnum <= 0) {
         return false;
      }
      for (int i = 0; i < cnum; i++) {
         if (!iter->add (comp[i])) {
            return false;
         }
      }
   
      if (my_debug) cerr << "excitationManager::add() return true" << endl ;
      return true;
   }


   bool excitationManager:: add (const string& channel,
                     const std::vector<AWG_Component>& awglist)
   {
      if (my_debug) cerr << "excitationManager::add(channel=" << channel << 
			", <vector of AWG_Component)" << endl ;
      if (silent) {
         return true;
      }
   
      semlock		lockit (mux);	// lock mutex */
      string		chnname (channelName (channel));
   
      // check if in list
      excitationlist::iterator 	iter;
      for (iter = excitations.begin(); 
          iter != excitations.end(); iter++) {
         if (*iter == chnname) {
            break;
         }
      }
      // if not add
      if (iter == excitations.end()) {
         if (!add (chnname)) {
            return false;
         }
         for (iter = excitations.begin(); 
             iter != excitations.end(); iter++) {
            if (*iter == chnname) {
               break;
            }
         }
         if (iter == excitations.end()) {
            return false;
         }
      }
      // return if empty list
      if (awglist.empty()) {
         return true;
      }
      // now add awg list
      if (!iter->add (awglist.begin(), awglist.end())) {
         return false;
      }
      if (my_debug) cerr << "excitationManager::add() return true" << endl ;
      return true;
   }


   bool excitationManager::addFilter (const string& channel,
                     const std::string& filtercmd)
   {
      if (silent) {
         return true;
      }
   
      semlock		lockit (mux);
      string		chnname (channelName (channel));
   
      // check if in list
      excitationlist::iterator 	iter;
      for (iter = excitations.begin(); 
          iter != excitations.end(); iter++) {
         if (*iter == chnname) {
            break;
         }
      }
      // if not add
      if (iter == excitations.end()) {
         if (!add (chnname)) {
            return false;
         }
         for (iter = excitations.begin(); 
             iter != excitations.end(); iter++) {
            if (*iter == chnname) {
               break;
            }
         }
         if (iter == excitations.end()) {
            return false;
         }
      }
   
      // set filter command
      iter->filtercmd = filtercmd;
      return true;
   }


   bool excitationManager::del (const string& channel)
   {
      if (silent) {
         return true;
      }
   
      semlock		lockit (mux);	// lock mutex */
      string		chnname (channelName (channel));
   
      // check if in list
      excitationlist::iterator 	iter;
      for (iter = excitations.begin(); 
          iter != excitations.end(); iter++) {
         if (*iter == chnname) {
            break;
         }
      }
      if (iter ==excitations.end()) {
         return true;
      }
   
      // check inUse
      if (--iter->inUse <= 0) {
         // remove test point if in use count reaches zero
         if ((iter->isTP) && (tpMgr != 0)) {
            tpMgr->del (iter->chnname);
         }
      }
   
      return true;
   }


   bool excitationManager::del (tainsec_t timeout)
   {
      if (my_debug) cerr << "excitationManager::del(timeout=" << timeout << ")" << endl ;
      if (silent) {
         return true;
      }
   
      semlock		lockit (mux);	// lock mutex */
   
     // stop all channels
      if (rampdown <= 0) {
         stop (timeout);
      }
      else {
         tainsec_t wait_time = rampdown;  // have to wait a rampup + rampdown period
                                                 // for swept sine, which can take longer 
                                                 // than one rampdown if ramp up is finishing.

         setGain(0, rampdown);
         //stop(timeout, rampdown);
         
         timespec wait;
         wait.tv_sec = wait_time / _ONESEC;
         wait.tv_nsec = wait_time % _ONESEC;
         nanosleep (&wait, 0);
      }
      // loop through channel list and delete test points
      for (excitationlist::iterator iter = excitations.begin();
          iter != excitations.end(); iter++) {
         if ((iter->isTP) && (tpMgr != 0)) {
            tpMgr->del (iter->chnname);
         }
      }
      excitations.clear();
      if (my_debug) cerr << "excitationManager::del() return" << endl ;
      return true;
   }


   double excitationManager::dwellTime () const
   {
      if (my_debug) cerr << "excitationManager::dwellTime()" << endl ;
      if (silent) {
         return 0;
      }
   
      semlock		lockit (mux);	// lock mutex */
      double		dwell = 0;
   
      for (excitationlist::const_iterator iter = excitations.begin();
          iter != excitations.end(); iter++) {
         dwell = max (dwell, iter->dwellTime());
      }
      if (my_debug) cerr << "excitationManager::dwellTime() return " << dwell << endl ;
      return dwell;
   }

}
