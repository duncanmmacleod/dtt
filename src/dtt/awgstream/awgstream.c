/*=============================================================================
awgstream - Command-line client to stream data from a file to the awg front end
             using the SIStr interface
Written Oct 2001 by Peter Shawhan
Modified June 2002 to call SIStrAppInfo
Modified 11 Feb 2003 to print out start time, etc. (unless '-q' is specified)
Modified 11 Feb 2003 to inject calibration lines with S2 freqs and amplitudes
Modified 24 Feb 2005 by Vuk Mandic to make calibration lines optional with '-c'

To compile:
  cc awgstream.c SIStr.o -I$GDS_DIR/src/util -I$GDS_DIR/src/awg \
            -L$GDS_DIR/lib -lawg -ltestpoint -lrt -o awgstream
where GDS_DIR on red      = /opt/CDS/d/gds/diag
              on london   = /opt/LLO/c/gds/diag

To run: $GDS_DIR/lib must be in your LD_LIBRARY_PATH

=============================================================================*/

#define CLIENT "awgstream"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <signal.h>
#include "SIStr.h"


/*===========================================================================*/
void PrintUsage( void )
{
  fprintf( stderr,
  "Usage: awgstream <channel> <rate> <file> [<scale> [<gpstime>]] [-q] [-d]\n" );
  fprintf( stderr,
  "  <channel> is case-sensitive and must be a real excitation channel\n" );
  fprintf( stderr,
  "  <rate> is in Hz and must match the excitation channel's true rate\n" );
  fprintf( stderr,
  "  <file> is the file of waveform data, which must be a FORMATTED ascii file\n" );
  fprintf( stderr,
  "      with the values separated by whitespace (e.g. newlines and/or spaces).\n" );
  fprintf( stderr,
  "      Any conversion error causes the rest of the file to be skipped.\n" );
  fprintf( stderr,
  "      If <file> is '-' (i.e. a dash), data is read from standard input.\n" );
  fprintf( stderr,
  "  <scale> is an optional scale factor to apply to the data in the file.\n" );
  fprintf( stderr,
  "  <gpstime> is the time to start injecting the waveform.  It can take any\n" );
  fprintf( stderr,
  "      real value, not just an integer.  It must occur during the 24-hour\n" );
  fprintf( stderr,
  "      period following the execution of the awgstream command.\n" );
  fprintf( stderr,
  "      If omitted, the waveform injection will start in about %d seconds.\n",
   (int) (SIStr_LEADTIME/1000000000LL) + 4 );
  fprintf( stderr,
  "  Parameters (channel, waveform file, scale factor, and start time) are\n" );
  fprintf( stderr,
  "      echoed to stdout UNLESS the '-q' option is specified.\n" );
  fprintf( stderr,
  "  The '-d' option causes debugging information to be printed to the screen.\n" );
  fprintf( stderr,
  "      Specify '-d' twice for extra information.\n" );
  fprintf( stderr,
  "  The '-c' option causes calibration lines WITH S2 FREQUENCIES AND AMPLITUDES\n" );
  fprintf( stderr,
  "      to be added to the waveform before it is injected.\n" );
  fprintf( stderr, "\nawgstream version %s\n$Id: awgstream.c 1450 2011-07-29 17:09:09Z james.batch@LIGO.ORG $\n", VERSION) ;
  return;
}

SIStream sis;

void sighandler(int foo) {
	SIStrCleanup(&sis);
}

/*===========================================================================*/
int main( int argc, char **argv )
{
  char *arg;
  int iarg;
  int nposarg = 0;
  int callines = 0;
  char *channel = NULL;
  int samprate = 0;
  char *filename = NULL;
  float scale = 1.0;
  double starttime = 0.0;
  int quiet = 0;
  char info[256];
  FILE *file;
  int status;
  float val, outval;
  int isample;
  double elapsed, dt, phi, twopi, cal;

  /*------ Beginning of code ------*/

  if ( argc <= 1 ) {
    PrintUsage(); return 0;
  }

  /*-- Parse command-line arguments --*/
  for ( iarg=1; iarg<argc; iarg++ ) {
    arg = argv[iarg];
    if ( strlen(arg) == 0 ) { continue; }

    /*-- See whether this introduces an option --*/
    if ( arg[0] == '-' && arg[1] != '\0' ) {
      /*-- The only valid options are "-q" and "-d" --*/
      if (( strcmp(arg,"-q") == 0 ) || (strcmp(arg,"--quiet") == 0)) {
	quiet = 1;
      } else if ( strcmp(arg,"-c") == 0 ) {
	callines = 1;
      } else if (( strcmp(arg,"-d") == 0 ) || (strcmp(arg,"--debug") == 0)) {
	SIStr_debug++;
      } else if ((strcmp(arg, "--help") == 0) || (strcmp(arg, "-h") == 0) || (strcmp(arg, "-?") == 0)) {
	 PrintUsage(); return 1;
      } else {
	fprintf( stderr, "Error: Invalid option %s\n", arg );
	PrintUsage(); return 1;
      }

    } else {
      /*-- This is a positional argument --*/

      nposarg++;

      switch (nposarg) {
      case 1:
	channel = arg;
	break;
      case 2:
	samprate = atol( arg );
	if ( samprate < 1 || samprate > 65536 ) {
	  fprintf( stderr, "Error: Invalid sampling rate\n" );
	  PrintUsage(); return 1;
	}
	break;
      case 3:
	filename = arg;
	break;
      case 4:
	scale = atof( arg );
	/*
	if ( scale == 0.0 || scale > 600000000.0 ) {
	  fprintf( stderr, "Error: Invalid scale factor\n" );
	  PrintUsage(); return 1;
	}
	*/
	break;
      case 5:
	starttime = atof( arg );
	if ( starttime < 600000000.0 || starttime > 1800000000.0 ) {
	  fprintf( stderr, "Error: Invalid start time\n" );
	  PrintUsage(); return 1;
	}
	break;
      default:
	fprintf( stderr, "Error: Too many arguments\n" );
	PrintUsage(); return 1;
      }	

    }

  }  /* End loop over command-line arguments */

  /*-- Make sure the required arguments were present --*/
  if ( channel == NULL ) {
    fprintf( stderr, "Error: Channel was not specified\n" );
  }
  if ( samprate == 0 ) {
    fprintf( stderr, "Error: Sampling rate was not specified\n" );
  }
  if ( filename == NULL ) {
    fprintf( stderr, "Error: File was not specified\n" );
  }
  if ( channel == NULL || samprate == 0 || filename == NULL ) {
    PrintUsage(); return 1;
  }

  /*-----------------------------------*/
  /* Report some information about this application and waveform */
  sprintf( info, "%s %s %.6g", CLIENT, filename, scale );
  SIStrAppInfo( info );

  /*-----------------------------------*/
  /* Open the file for reading (if not reading from stdin) */
  if ( strcmp(filename,"-") != 0 ) {
    file = fopen( filename, "r" );
    if ( file == NULL ) {
      /* An error occurred */
      fprintf( stderr, "Error while opening %s for reading: ", filename );
      perror( NULL );
      return -2;
    }
  } else {
    file = stdin;
  }

  /*-----------------------------------*/
  /* Open the Signal Injection Stream */
  status = SIStrOpen( &sis, channel, samprate, starttime );
  if ( SIStr_debug ) { printf( "SIStrOpen returned %d\n", status ); }
  if ( status != SIStr_OK ) {
    fprintf( stderr,
	     "Error while opening SIStream: %s\n", SIStrErrorMsg(status) );
    return 2;
  }
 
  signal(SIGINT, sighandler);
  signal(SIGTERM, sighandler);
  signal(SIGHUP, sighandler);
  signal(SIGABRT, sighandler);
  signal(SIGQUIT, sighandler);
 
  if ( quiet == 0 ) {
    /* Print out the argument values */
    printf( "Channel = %s\n", channel );
    printf( "File    = %s\n", filename );
    printf( "Scale   = %17.6f\n", scale );
    printf( "Start   = %17.6f\n", sis.starttime );
  }

  isample = 0;
  dt = 1.0 / (double) samprate;
  twopi = 8.0 * atan(1.0);

#if 0
  /*-----------------------------------*/
  /* Read data from input file and send it */
  while ( fscanf(file,"%f",&val) == 1 ) {

    /*-- Add in calibration lines.  NOTE: I assume that the calibration line
      frequencies are multiples of 0.05 Hz !!!  This allows us to reset the
      sample counter every 20 seconds, to avoid accumulated round-off error -*/
    isample++;
    if ( isample == 20*samprate ) { isample = 0; }
    elapsed = dt * (double) isample;
    phi = elapsed * twopi;

    if ( callines == 1 ) {
      if ( channel[0] == 'L' ) {           /* L1 */
        cal= 0.02*sin(phi*52.3) + 0.003*sin(phi*166.7) + 0.25*sin(phi*927.7);
      } else if ( channel[1] == '1' ) {    /* H1 */
        cal= 0.0025*sin(phi*41.25) + 0.0028*sin(phi*151.3) + 0.07*sin(phi*973.3);
      } else {                             /* H2 */
        cal= 0.01*sin(phi*41.75) + 0.0033*sin(phi*151.8) + 0.04*sin(phi*973.8);
      }
    } else {
      cal = 0;
    }

    outval = scale * val + (float) cal;

    status = SIStrAppend( &sis, &outval, 1, 1.0 );
    if ( SIStr_debug >= 2 ) { printf( "SIStrAppend returned %d\n", status ); }
    if ( status != SIStr_OK ) {
      fprintf( stderr,
	       "Error while adding data to stream: %s\n",
	       SIStrErrorMsg(status) );
      break;
    }
  }
#else 
/* Try sending the data as a block to the front end instead of one sample at a time.
 * Although this appears to add more data (0 values) when the data file does not contain
 * an integral number * samplerate data points, this would be done in the gds libraries
 * anyway, so the net result is the same.
 */
    {
	float	*inbuf, *pf ;	/* Read one sample rate of data */
	int	i = 0 ;
	int     nread = 0 ;	/* data points read from file. */

	pf = inbuf = (float *) malloc(sizeof(float) * samprate) ;
	if (!inbuf)
	{
	   fprintf(stderr, "Cannot allocate memory for file data\n") ;
	   return (-107) ;
	}

	 do
	 {
	    memset(inbuf, 0, (sizeof(float) * samprate)) ;
	    /* Read a block of samprate data points from the file, if there are that many */
	    while (i < samprate && (nread = fscanf(file, "%f", pf)) == 1)
	    {
		/*-- Add in calibration lines.  NOTE: I assume that the calibration line
		  frequencies are multiples of 0.05 Hz !!!  This allows us to reset the
		  sample counter every 20 seconds, to avoid accumulated round-off error -*/
		isample++;
		if ( isample == 20*samprate ) { isample = 0; }
		elapsed = dt * (double) isample;
		phi = elapsed * twopi;

		if ( callines == 1 ) {
		  if ( channel[0] == 'L' ) {           /* L1 */
		    cal= 0.02*sin(phi*52.3) + 0.003*sin(phi*166.7) + 0.25*sin(phi*927.7);
		  } else if ( channel[1] == '1' ) {    /* H1 */
		    cal= 0.0025*sin(phi*41.25) + 0.0028*sin(phi*151.3) + 0.07*sin(phi*973.3);
		  } else {                             /* H2 */
		    cal= 0.01*sin(phi*41.75) + 0.0033*sin(phi*151.8) + 0.04*sin(phi*973.8);
		  }
		} else {
		  cal = 0;
		}

		*pf = scale * (*pf) + (float) cal;
		pf++ ;
		i++ ;
	    }
	    if (SIStr_debug > 1)
	    {
	       fprintf(stderr, "awgstream: Read %d samples from file.\n", i) ;
	    }
	    /* If not enough points were read, fill the remainder of the array with 0. */
	    while (i < samprate)
	    {
		*(pf++) = 0.0 ;
		i++ ;
	    }

	    /* Send the block of data to the awg. */
	    status = SIStrAppend(&sis, inbuf, samprate, 1.0) ;
	    if ( SIStr_debug >= 2 ) { printf( "SIStrAppend returned %d\n", status ); }
	    if ( status != SIStr_OK ) {
	      fprintf( stderr,
		       "Error while adding data to stream: %s\n",
		       SIStrErrorMsg(status) );
	    }
	    /* Set i to 0 to fill the buffer again if needed. */
	    i = 0 ;
	    pf = inbuf ; /* Point to the start of the buffer. */
	 } while (nread > 0) ;
     }
#endif

  /* Close the input file (unless it is stdin) */
  if ( file != stdin ) {
    fclose( file );
  }

  /*-----------------------------------*/
  /* Close the stream */
  status = SIStrClose( &sis );
  if ( SIStr_debug ) { printf( "SIStrClose returned %d\n", status ); }
  if ( status != SIStr_OK ) {
    fprintf( stderr,
	     "Error while closing SIStream: %s\n", SIStrErrorMsg(status) );
    return 2;
  }

  return 0;
}
