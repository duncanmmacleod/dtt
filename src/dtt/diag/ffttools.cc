/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: ffttools						*/
/*                                                         		*/
/* Module Description: triggered time series measurement		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

/* Header File List: */

#include <time.h>
#include <math.h>
#include <complex>
#include <iostream>
#include <vector>
#include "diagnames.h"
#include "gdsalgorithm.h"
#include "ffttools.hh"
#include "diagdatum.hh"
#include "awgapi.h"


namespace diag {
   using namespace std;
   using namespace thread;

   static const int my_debug = 0 ;

   // maximum number of sampling points per (excitation) period
   const double _MAX_OVERSAMPLING = 4.0;
   // Time ahead buffers are allocated
   const double _TIME_AHEAD = 3.0; 

   const double ffttest::fftSpanFactor = 1.1;

   const fftparam ffttest::tmpresult::fftparamZero = {0, 0, 0, 0};

   ffttest::tmpresult::tmpresult (string Name, int Size, bool Cmplx, 
                     const fftparam& param) 
   : name (Name), cmplx (Cmplx), size (Size), fft (0), x (0), prm (param)
   {
      allocate (Size);
   }


   ffttest::tmpresult::~tmpresult()
   {
      allocate ();
   }


   bool ffttest::tmpresult::allocate (int Size)
   {
      int my_debug=0 ; // kill debug messages for this function.
      if (my_debug) cerr << "ffttest::tmpresult::allocate(Size = " << Size << ")" << endl ;
      lock.writelock();
   
      // delete old arrays
      if (fft != 0) {
         delete [] fft;
         fft = 0;
      }
      if (x != 0) {
         delete [] x;
         x = 0;
      }
      // alocate new ones
      size = Size;
      if (size == 0) {
         lock.unlock();
         if (my_debug) cerr << "ffttest::tmpresult::allocate() return true" << endl ;
         return true;
      }
      else {
         fft = new (nothrow) float[2 * size];
         x = new (nothrow) float[cmplx ? 2 * size :  size];
         prm.windowed_data = x;
         lock.unlock();
         if (my_debug) cerr << "ffttest::tmpresult::allocate() allocated data, return" << endl ;
         return valid();
      }
   }


   bool ffttest::tmpresult::valid () const
   {
      return ((fft != 0) && (x != 0) && (prm.window_coeff != 0));
   }


   ffttest::tmpresult::tmpresult (const tmpresult& tmp) 
   : name (""), cmplx (false), size (0), fft (0), x (0),
   prm (fftparamZero) {
      *this = tmp;
   }


   ffttest::tmpresult& ffttest::tmpresult::operator= (
                     const tmpresult& tmp)
   {
      if (this != &tmp) {
         lock.writelock();
         tmp.lock.writelock();
         name = tmp.name;
         size = tmp.size;
         fft = tmp.fft;
         x = tmp.x;
         prm = tmp.prm;
         prm.windowed_data = x;
         const_cast <tmpresult&>(tmp).fft = 0;
         const_cast <tmpresult&>(tmp).x = 0;
         const_cast <tmpresult&>(tmp).prm.windowed_data = 0;
         tmp.lock.unlock();
         lock.unlock();
      }
      return *this;
   }


   ffttest::ffttest () 
   : stdtest (fftname) , fStart (0), fStop (0), BW (0), overlap (0),
   window (0), measTime (0), settlingTime (0), 
   fftPlan (tmpresult::fftparamZero)
   {
   }


   ffttest::~ffttest () 
   {
      // delete old plan if necessary
      if ((fftPlan.real_plan != 0) || (fftPlan.complex_plan != 0)) {
         psGen (PS_CLEAN_PLAN, &fftPlan, 0, 
               (fftPlan.real_plan != 0) ? DATA_REAL : DATA_COMPLEX, 
               0, 1, 0, 0, 0);
      }
      // deallocate window memory
      if (fftPlan.window_coeff != 0) {
         delete [] fftPlan.window_coeff;
         fftPlan.window_coeff = 0;
      }
   }


   diagtest* ffttest::self () const 
   {
      return new (nothrow) ffttest ();
   }


   bool ffttest::end (std::ostringstream& errmsg)
   {
      if (my_debug) cerr << "ffttest::end()" << endl ;
      semlock		lockit (mux);
      // delete temporary storage
      tmps.clear();
   
      bool rc = stdtest::end (errmsg);
      if (my_debug) cerr << "ffttest::end() return" << endl ;
      return rc ;
   }


   bool ffttest::readParam (ostringstream& errmsg)
   {
      if (my_debug) cerr << "ffttest::readParam()" << endl;

      // call parent method
      if (!stdtest::readParam (errmsg)) {
         if (my_debug) cerr << "ffttest::readParam() return false line " << __LINE__ << endl ;
         return false;
      }
   
      semlock		lockit (mux);
      bool		err = false;
   
      // read start frequency
      if (!test->getParam (*storage->Test,
                          fftStartFrequency, fStart)) {
         errmsg << "Unable to load values from Test." <<
            fftStartFrequency << endl;
         err = true;
      }
      // read stop frequency
      if (!test->getParam (*storage->Test,
                          fftStopFrequency, fStop)) {
         errmsg << "Unable to load value from Test." <<
            fftStopFrequencyDef << endl;
         err = true;
      }
      // read signal bandwidth
      if (!test->getParam (*storage->Test, fftBW, BW)) {
         errmsg << "Unable to load value from Test." << fftBW << endl;
         err = true;
      }
      // read overlap
      if (!test->getParam (*storage->Test, fftOverlap, overlap)) {
         errmsg << "Unable to load value from Test." <<
            fftOverlap << endl;
         err = true;
      }
      // read window
      if (!test->getParam (*storage->Test, fftWindow, window)) {
         errmsg << "Unable to load value from Test." <<
            fftWindow << endl;
         err = true;
      }
      // read remove DC
      if (!test->getParam (*storage->Test, fftRemoveDC, removeDC)) {
         removeDC = false;
      }
      // read number of A channels
      if (!test->getParam (*storage->Test, fftAChannels, AChannels)) {
         errmsg << "Unable to load value from Test." <<
            fftAChannels << endl;
         err = true;
      }
      // read settling time
      if (!test->getParam (*storage->Test, fftSettlingTime, settlingTime)) {
         errmsg << "Unable to load values from Test." <<
            fftSettlingTime << endl;
         err = true;
      }

       if (!test->getParam (*storage->Test, fftBurstNoiseQuietTime_s, burstNoiseQuietTime_s)) {
           burstNoiseQuietTime_s = 0;  //default value for burstNoiseQuietTime_s
       }

      // Read ramp down time
      if (!test->getParam (*storage->Test, fftRampDown, rampDown)) {
	 errmsg << "Unable to load value from Test." << fftRampDown << endl ;
	 err = true ;
      }
      if (my_debug) cerr << "  rampDown parameter = " << rampDown << endl ;
      // Read ramp up time
      if (!test->getParam (*storage->Test, fftRampUp, rampUp)) {
	 errmsg << "Unable to load value from Test." << fftRampUp << endl ;
	 err = true ;
      }
      if (my_debug) cerr << "  rampUp parameter = " << rampUp << endl ;
   
      // read stimuli channel
      if (!readStimuliParam (errmsg, false, allWaveforms)) {
         err = true;
      }
   
      // read measurement channels
      if (!readMeasParam (errmsg)) {
         err = true;
      }
   
      // check heterodyne frequency
      fZoom = 0.0;
      if (!heterodyneFrequency (fZoom)) {
         errmsg << "Channels have inconsistent heterodyne frequency." << endl;
         err = true;
      }
      //cout << "ZOOM FREQUENCY IN FFT MEASUREMENT " << fZoom << endl;
   
      // handle negative AChannels value
      if (AChannels < 0) {
         AChannels = meas.size();
      }
      if (my_debug) cerr << "ffttest::readParam() return " << (err ? "false" : "true") << endl ;
      return !err;
   }


   bool ffttest::calcTimes (std::ostringstream& errmsg,
                     tainsec_t& t0)
   {
      semlock		lockit (mux);
      bool		err = false;
   
      if (my_debug) cerr << "ffttest::calcTimes(..., t0 = " << t0 << endl ;
      // check settling time
      if (settlingTime < 0) {
         errmsg << "Settling time must be positive or zero" << endl;
         if (my_debug) cerr << "ffttest::calcTimes() return false line " << __LINE__ << endl ;
         return false;
      }
      if (burstNoiseQuietTime_s < 0) {
          errmsg << "BurstNoiseQuietTime must be positive or zero" << endl;
          if (my_debug) cerr << "ffttest::calcTimes() return false line " << __LINE__ << endl ;
          return false;
      }
      // check averages
      if (averages < 1) {
         errmsg << "Number of averages must be at least one" << endl;
         err = true;
      }
      // check averages
      if (averages > 100000) {
         errmsg << "Number of averages must be smaller than 100000" << 
            endl;
         err = true;
      }
      // check average type
      if ((averageType < 0) || (averageType > 2)) {
         errmsg << "Illegal average type" << endl;
         err = true;
      }
      // check fStart 
      if (fStart < 0) {
         errmsg << "start frequency must be non-negative" << endl;
         err = true;
      }
      // check fStart and fStop
      if (fStop <= fStart + 1E-6) {
         errmsg << "stop frequency must be larger than start" << endl;
         err = true;
      }
      // check bandwidth
      if (BW < 1E-6) {
         errmsg << "Bandwidth must be positive" << endl;
         err = true;
      }
      // check overlap
      if (overlap >= 1) {
         errmsg << "overlap must be smaller than one" << endl;
         err = true;
      }
      // check window
      if ((window < 0) || (window > 6)) {
         errmsg << "unsupported window function" << endl;
         err = true;
      }
      // check number of A channels
      if ((AChannels < 0) || (AChannels > (int)meas.size())) {
         errmsg << "number of A channels is out of range" << endl;
         err = true;
      }
      if (err) {
         if (my_debug) cerr << "ffttest::calcTimes() return false line " << __LINE__ << endl ;
         return false;
      }
   
      // frequency span; rounded to the next power of 2
      if (fZoom != 0) {
         fSpan = fftSpanFactor * 2 * max (fStop - fZoom, fZoom - fStart);
      }
      else {
         fSpan = fftSpanFactor * (fStop - fStart);
      }
      int	exp;
      frexp (fSpan - 1E-12, &exp);
      fSpan = ldexp (1.0, exp);
   
      // BW
      frexp (BW / sqrt(2.0), &exp);
      BW = ldexp (1.0, exp);
      measTime = 1.0 / BW;
      if (BW >= fSpan/16 - 1E-12) {
         errmsg << "Bandwidth must be smaller than 1/16th of the " <<
            "frequency span" << endl;
         if (my_debug) cerr << "ffttest::calcTimes() return false line " << __LINE__ << endl ;
         return false;
      }
   
      // highest frequency of interest
      if (fZoom != 0) {
         fMaxMeas = fSpan / 2.0;
      }
      else {
         fMaxMeas = max (fStop, fSpan);
      }
      // determine sampling rates
      if (my_debug)
      {
	 cerr << "ffttest::calcTimes() calling samplingFrequencies()" << endl ;
	 cerr << "   fMaxMeas   = " << fMaxMeas << endl ;
	 cerr << "   fMinSample = " << fMinSample << endl ;
	 cerr << "   fSample    = " << fSample << endl ;
      }
      // fMaxMeas is known before this call, fMinSample and fSample are 
      // assigned values in samplingFrequencies().
      samplingFrequencies (fMaxMeas, fMinSample, fSample);
      if (my_debug) 
      {
	 cerr << "   LINE " << __LINE__ << endl ;
	 cerr << "   fMaxMeas   = " << fMaxMeas << endl ;
	 cerr << "   fMinSample = " << fMinSample << endl ;
	 cerr << "   fSample    = " << fSample << endl ;
      }
      if (fZoom == 0) {
         // normal data
         if (fMinSample < fSample - 1E-12) {
            errmsg << "Sampling rate of at least one channel is too small" << 
               " (required " << fSample << ")" << endl;
            err = true;
         }
      }
      else {
         // data already heterodyned
         if ((fStart >= fZoom) ||
            (fStart < fZoom - fSpan / 2.0) ||
            (fStop <= fZoom) ||
            (fStop > fZoom + fSpan / 2.0)) {
            errmsg << "Start/stop frequency incompatible with heterodyne " <<
               "frequency (" << fZoom << ")" << endl;
            err = true;
         }
         if (fMinSample < fSpan - 1E-12) {
            errmsg << "Sampling rate of at least one channel is too small" << 
               " (required " << fSample << ")" << endl;
            err = true;
         }
	 if (my_debug)
	 {
	    cerr << "fHetero    = " << fZoom << endl;
	    cerr << "fMinSample = " << fMinSample << endl;
	    cerr << "fMaxMeas   = " << fMaxMeas << endl;
	    cerr << "fSample    = " << fSample << endl;
	    cerr << "fSpan      = " << fSpan << endl;
	 }
         //fSample = fMinSample;
      }
      if (err) {
         if (my_debug) cerr << "ffttest::calcTimes() return false line " << __LINE__ << endl ;
         return false;
      }
   
      // calculate time grid
      // The fSample is the sample rate of the slowest channel. 
      timeGrid = calcTimeGrid (fSample / 2.0, &t0);
   
      // calculate measurement start time
      if (my_debug) cerr << "ffttest::calcTimes() - settlingTime = " << settlingTime << ", measTime = " << measTime << endl ;
      if (rampUp > (settlingTime * measTime)) {
	 // Use rampUp instead of settlingTime.
	 if (my_debug) cerr << "  use rampUp (" << rampUp << ") instead of settlingTime" << endl ;
	 mStart = adjustForSampling (rampUp, timeGrid);
      }
      else
	 mStart = adjustForSampling (settlingTime * measTime, timeGrid);
      if (my_debug) cerr << "ffttest::calcTimes() - mStart = " << mStart << endl ;
      exct0 = 0;
      mTimeAdd = adjustForSampling (measTime, timeGrid) - measTime;
      if (my_debug) cerr << "ffttest::calcTimes() - mTimeAdd = " << mTimeAdd << endl ;

      // Set the ramp down time for excitations.  The argument to
      // setRampDown() is a long int in nanoseconds, add 0.5 to get
      // rounding correct when the cast to tainsec_t is performed.
      // Use a value from a widget to set the ramp down time.
      testExc->setRampDown((tainsec_t)(rampDown * 1E9 + 0.5)) ;
      if (my_debug) cerr << "  rampDown time is " << rampDown * 1E9 << endl ;
      testExc->setRampUp((tainsec_t)(rampUp * 1E9 + 0.5)) ;
      if (my_debug) cerr << "  rampUp time is " << rampUp * 1E9 << endl ;
   
      // points in the result array
      points = (int)(ceil ((fStop - fStart) / BW) + 1.5);
      // total number of A channels
      numA = AChannels;
      for (stimuluslist::iterator iter = stimuli.begin();
          iter != stimuli.end(); iter++) {
         if (iter->isReadback) {
            numA++;
         }
      }
      // number of result records
      rnumber = (numA - AChannels) + meas.size() + 2 * numA;
   
      // zoom analysis?
      if (fZoom != 0) {
         // data is already heterodyned
         fZoomStart = 0;
         fftPoints = (int) (fSpan / BW + 0.5);
         if (points > fftPoints) {
            points = fftPoints;
         }
         decimate2 = (int) (fSample / fSpan + 0.5);
      	 // do not remove mean after down-conversion!
         removeDC = false;
         cout << "fZoom = " << fZoom << endl;
         cout << "fSpan = " << fSpan << endl;
         cout << "BW    = " << BW << endl;
         cout << "fftPoints = " << fftPoints << endl;
         cout << "decimate2 = " << decimate2 << endl;
         cout << "points = " << points << endl;
      }
      else if ((fStart < 1E-12) || (fStart < fStop / 2.)) {
         // no heterodyne
         fftPoints = (int) (2.0 * fSpan / BW + 0.5);
         if (points > fftPoints/2) {
            points = fftPoints/2;
         }
         fZoom = 0;
         fZoomStart = 0;
         decimate2 = (int) (fSample / (2.0 * fSpan) + 0.5);
      }
      else {
         // heterodyne first
         fftPoints = (int) (fSpan / BW + 0.5);
         if (points > fftPoints) {
            points = fftPoints;
         }
         fZoom = (fStop + fStart) / 2.0;
         fZoom = fStart + BW * (int) ((fStop - fStart) / 2.0 / BW + 0.5);
         fZoomStart = fineAdjustForSampling (t0 + (tainsec_t) (mStart * 1E9), timeGrid);
         decimate2 = (int) (fSample / fSpan + 0.5);
      	 // do not remove mean after down-conversion!
         removeDC = false;
      }
      // cerr << "cT fZoom = " << fZoom << " fftPoints = " << fftPoints << 
         // " points = " << points << endl;
      // cerr << "cT fZoomStart = " << fZoomStart << " fSample = " << fSample << 
         // " decimate2 = " << decimate2 << endl;
   
      // handle averaging
      avrgsize = averages;
      // need more buffers if shorter than _TIME_AHEAD
      double deltaTime = (1.0 - overlap) * (measTime + mTimeAdd);
      if (avrgsize * deltaTime < _TIME_AHEAD) {
         avrgsize = (int) (_TIME_AHEAD / deltaTime);
      }
      // make sure we account for filter delay and overlapped segments
      //if (overlap > 0) {
         // measurement time + filter delay upper limit
      double rlen = (measTime + mTimeAdd + 21./fSample) / 
         (measTime + mTimeAdd + 1E-12);
      int onum = (int) (ceil (rlen / (1.0 - overlap)) + 0.1);
      cerr << "O NUM =============================== " << onum << endl;
      // if (avrgsize < onum) {
         // avrgsize = onum;
      // }
      if (onum > 1) avrgsize += onum;
      avrgsize += 1;
      //}
      cout << "AVRGSIZE IS _________________" << avrgsize << endl;
   
      // delete old plan if necessary
      if ((fftPlan.real_plan != 0) || (fftPlan.complex_plan != 0)) {
         psGen (PS_CLEAN_PLAN, &fftPlan, 0, 
               (fftPlan.real_plan != 0) ? DATA_REAL : DATA_COMPLEX, 
               0, 1, 0, 0, 0);
      }
      // allocate window memory
      if (fftPlan.window_coeff != 0) {
         delete [] fftPlan.window_coeff;
         fftPlan.window_coeff = 0;
      }
      fftPlan.window_coeff = new (nothrow) float[fftPoints];
      if (fftPlan.window_coeff == 0) {
         errmsg << "unable to allocate window memory" << endl;
         if (my_debug) cerr << "ffttest::calcTimes() return false line " << __LINE__ << endl ;
         return false;
      }
      // create FFT plan & calculate window
      {
	 float	*in, *out ;

	 cerr << "ffttest::calcTimes() - Create FFT plan and calculate window, " 
	      << fftPoints << " points" << endl ;
	 // The fftw plan creation really needs input and output
	 // arrays to work properly.
	 if((in = (float *) new float[fftPoints * 2]) == NULL)
	 {
	    errmsg << "Unable to allocate memory for DFT plan" << endl ;
	    return false ;
	 }
	 if ((out = (float *) new float[fftPoints * 2]) == NULL)
	 {
	    errmsg << "Unable to allocate memory for DFT plan" << endl ;
	    return false ;
	 }

	 if (psGen (PS_INIT_ALL, &fftPlan, fftPoints, 
		   (fZoom == 0) ? DATA_REAL : DATA_COMPLEX, in, 
		   1.0/(fSample / decimate2), 
		   OUTPUT_GDSFORMAT, window, out) < 0) 
	 {
	    errmsg << "unable to initialize fft plan and window" << endl;
	    delete [] in ;
	    delete [] out ;
            if (my_debug) cerr << "ffttest::calcTimes() return false line " << __LINE__ << endl ;
	    return false;
	 }
	 // These were just used for scratch memory.
	 delete [] in ;
	 delete [] out ;
      }

      // calculate resolution (window) bandwidth
      double windowNorm = sMean (fftPlan.window_coeff, fftPoints);
      windowNorm *= windowNorm;
      if (windowNorm > 0) {
         windowBW = BW / windowNorm;
      }
      else {
         windowBW = BW;
      }
   
      if (my_debug) cerr << "ffttest::calcTimes() return true" << endl ;
      return true;
   }

    bool ffttest::calcBurstDurations (std::ostringstream& errmsg, tainsec_t *time_meas_ns, tainsec_t *time_pitch_ns, tainsec_t *time_rampup_ns,
                             tainsec_t *time_rampdown_ns, tainsec_t *time_excitation_ns,
                             tainsec_t *time_quiet_ns)
    {
       if(burstNoiseQuietTime_s <= 0.0)
       {
           errmsg << "Burst quiet time must be > 0 when in burst mode." << endl;
           return false;
       }
       if(overlap > 0.0)
       {
           errmsg << "Cannot set both 'overlap' and 'burst noise quiet time' to values greater than 0." << endl;
           return false;
       }
        tainsec_t t_meas_ns = (tainsec_t)((measTime + mTimeAdd) * 1e9 + 0.5);
        tainsec_t t_rampdown_ns = (tainsec_t)((rampDown) * 1e9 + 0.5);
        tainsec_t t_rampup_ns = (tainsec_t)((rampUp) * 1e9 + 0.5);
        tainsec_t t_quiet_ns = (tainsec_t)((burstNoiseQuietTime_s) * 1e9 + 0.5);
        tainsec_t t_pitch_ns = t_meas_ns + max(t_rampup_ns, (tainsec_t)30000000l);
        tainsec_t t_excitation_ns = t_meas_ns - t_rampdown_ns - t_quiet_ns;

        if(time_meas_ns) *time_meas_ns = t_meas_ns;
        if(time_excitation_ns) *time_excitation_ns = t_excitation_ns;
        if(time_rampdown_ns) *time_rampdown_ns = t_rampdown_ns;
        if(time_rampup_ns) *time_rampup_ns = t_rampup_ns;
        if(time_quiet_ns) *time_quiet_ns = t_quiet_ns;
        if(time_pitch_ns) *time_pitch_ns = t_pitch_ns;

        if(t_excitation_ns < 0.0)
        {
            errmsg << "Excitation time must be non-negative. Burst quiet time is probably too long.  time_excitation_ns = " << t_excitation_ns << endl;
            return false;
        }

        return true;
    }

   bool ffttest::newBurstMeasPoint(int i, int measPoint, tainsec_t measure_ns, tainsec_t pitch_ns, tainsec_t rampUp_ns)
   {
       if(burstNoiseQuietTime_s <= 0.0)
       {
           return false;
       }
       tainsec_t start = T0 + rampUp_ns + i * pitch_ns;
       start = fineAdjustForSampling (start, timeGrid);
       intervals.push_back (interval (start, measure_ns));

       // add new partitions
       if (!addMeasPartitions (intervals.back(), measPoint * averages + i,
                               fSample, 0, fZoom, (double) fSample / decimate2,
                               fZoomStart)) {
           if (my_debug) cerr << "ffttest::newMeasPoint() return false line " << __LINE__ << endl ;
           return false;
       }

       // add synchronization point
       if (!addSyncPoint (intervals.back(), i, measPoint)) {
           if (my_debug) cerr << "ffttest::newMeasPoint() return false line " << __LINE__ << endl ;
           return false;
       }

       if (my_debug) cerr << "ffttest::newMeasPoint() return true" << endl ;
       return true;
   }

   bool ffttest::newMeasPoint (int i, int measPoint)
   {
      semlock		lockit (mux);
      if (my_debug) cerr << "ffttest::newMeasPoint( i = " << i << ", measPoint = " << measPoint << ")" << endl ; 
      // calculate start time

      tainsec_t start = T0 + (tainsec_t)
              ((mStart + (double) (i + skipMeas) * (1.0 - overlap) *
              (measTime + mTimeAdd)) * 1E9 + 0.5);


   
      // check if too far behind
      if (RTmode ) {
         tainsec_t now = TAInow();
         if (start < now + _EPOCH) {
            skipMeas = 
               (int) (((double) (now + _EPOCH - T0) / 1E9 - mStart) /
                     (measTime + mTimeAdd) / (1.0 - overlap) + 0.99) - i;
            cerr << "SKIP MEASUREMENTS " << skipMeas << endl;
            if (skipMeas < 0) {
               skipMeas = 0;
            }
            start = T0 + (tainsec_t)
               ((mStart + (double) (i + skipMeas) * (1.0 - overlap) * 
                (measTime + mTimeAdd)) * 1E9 + 0.5);
         }
      }
   
      // fine adjust to time grid & calc. duration
      start = fineAdjustForSampling (start, timeGrid);
      tainsec_t duration =  (tainsec_t) ((measTime + mTimeAdd) * 1E9 + 0.5);

      // add interval
      intervals.push_back (interval (start, duration));
   
      // add new partitions
      if (!addMeasPartitions (intervals.back(), measPoint * averages + i,
                           fSample, 0, fZoom, (double) fSample / decimate2,
                           fZoomStart)) {
         if (my_debug) cerr << "ffttest::newMeasPoint() return false line " << __LINE__ << endl ;
         return false;
      }
   
      // add synchronization point
      if (!addSyncPoint (intervals.back(), i, measPoint)) {
         if (my_debug) cerr << "ffttest::newMeasPoint() return false line " << __LINE__ << endl ;
         return false;
      }
   
      if (my_debug) cerr << "ffttest::newMeasPoint() return true" << endl ;
      return true;
   }


   bool ffttest::calcMeasurements (std::ostringstream& errmsg,
                     tainsec_t t0, int measPoint)
   {
      semlock		lockit (mux);
      tainsec_t		eStart;		// excitation start	
      tainsec_t		dur;		// excitation duration	

      tainsec_t pitch_ns=0, rampUp_ns=0, rampDown_ns=0, excitation_ns=0, measure_ns=0;

      if (my_debug) cerr << "ffttest::calcMeasurements(..., t0 = " << t0 << ", measPoint = " << measPoint << ")" << endl ;
      // determine excitation signals
      if (my_debug) cerr << "  rampUp = " << rampUp << ", rampDown = " << rampDown << endl ;
      eStart = t0 + (tainsec_t) (exct0 * 1E9);
      for (stimuluslist::iterator iter = stimuli.begin();
          iter != stimuli.end(); iter++) {
         if ((iter->waveform == (AWG_WaveType) 10) || (iter->waveform == (AWG_WaveType) 11)) {
            dur = (tainsec_t) ((measTime + mTimeAdd) * 1E9 + 0.5);
         }
         else {
            dur = -1;
         }
         if(burstNoiseQuietTime_s > 0.0)
         {

             if(!calcBurstDurations(errmsg, &measure_ns, &pitch_ns, &rampUp_ns, &rampDown_ns, &excitation_ns, NULL))
             {
                 return false;
             }
             if (!iter->calcBurstSignal(errmsg, eStart, pitch_ns, rampUp_ns, rampDown_ns, excitation_ns, avrgsize ))
             {
                 errmsg << "Unable to calculate burst excitation signal" << endl;
                 if (my_debug) cerr << "ffttest::calcMeasurements() return false line " << __LINE__ << endl;
                 return false;
             }
         }
         else
         {
             if (!iter->calcSignal(eStart, dur, rampUp * 1E9, rampDown * 1E9))
             {
                 errmsg << "Unable to calculate excitation signal" << endl;
                 if (my_debug) cerr << "ffttest::calcMeasurements() return false line " << __LINE__ << endl;
                 return false;
             }
         }
      }
   
      // create initial measurement points
      skipMeas = 0;
      for (int i = 0; i < avrgsize; i++) {
          if(burstNoiseQuietTime_s > 0.0)
          {
              if(!newBurstMeasPoint(i,0, measure_ns, pitch_ns, rampUp_ns))
              {
                  errmsg << "Unable to create measurement points" << endl;
                  if (my_debug) cerr << "ffttest::calcMeasurements() return false line " << __LINE__ << endl;
                  return false;
              }
          }
          else
          {
              if (!newMeasPoint(i))
              {
                  errmsg << "Unable to create measurement points" << endl;
                  if (my_debug) cerr << "ffttest::calcMeasurements() return false line " << __LINE__ << endl;
                  return false;
              }
          }
      }
      if (my_debug) cerr << "ffttest::calcMeasurements() return true" << endl ;
      return true;
   }


   bool ffttest::stopMeasurements (int firstIndex)
   {
      if (my_debug) cerr << "ffttest::stopMeasurements(firstIndex = " << firstIndex << ")" << endl ;
      semlock		lockit (mux);
      // delete temporary storage
      tmps.clear();
   
      bool rc = stdtest::stopMeasurements (firstIndex);
      if (my_debug) cerr << "ffttest::stopMeasurements() return " << (rc ? "true" : "false") << endl ;
      return rc ;
   }


   bool ffttest::analyze (const callbackarg& id, int measnum, bool& note)
   {
      if (my_debug) cerr << "ffttest::analyze(..., measnum = " << measnum << ",...)" << endl ;
      semlock		lockit (mux);
   
      /////////////////////////////////////////////////////////
      // Init analysis first time around		     //
      /////////////////////////////////////////////////////////
   
      if (measnum == 0) {
      
      /////////////////////////// Allocate temporary storage
         tmps.clear();
         for (stimuluslist::iterator iter = stimuli.begin();
             iter != stimuli.end(); iter++) {
            if (iter->duplicate) {
               continue;
            }
            if (iter->isReadback) {
               tmps.push_back (tmpresult (iter->readback, fftPoints, 
                                         fZoom > 0, fftPlan));
               if (!tmps.back().valid()) {
                  if (my_debug) cerr << "ffttest::analyze() return false line " << __LINE__ << endl ;
                  return false;
               }
            }
         }
         for (measlist::iterator iter = meas.begin();
             iter != meas.end(); iter++) {
            if (iter->duplicate) {
               continue;
            }
            tmps.push_back (tmpresult (iter->name, fftPoints, 
                                      fZoom > 0, fftPlan));
            if (!tmps.back().valid()) {
               if (my_debug) cerr << "ffttest::analyze() return false line " << __LINE__ << endl ;
               return false;
            }
         }
      
      /////////////////////////// Create index
         // get index access class
         const diagIndex& 	indx = diagIndex::self();
         // find index / create new index if necessary
         gdsDataObject* 	iobj = storage->findData (stIndex);
         if (iobj == 0) {
            iobj = indx.newObject (0);
            if (iobj == 0) {
               if (my_debug) cerr << "ffttest::analyze() return false line " << __LINE__ << endl ;
               return false;
            }
            storage->addData (*iobj, false);
         }
      
         // write index 
         ostringstream		entry1;
         // psd entry
         int b = 0;
         for (tmpresults::iterator iter = tmps.begin();
             iter != tmps.end(); iter++, b++) {
            diagIndex::channelEntry (entry1, b, iter->name);
            diagIndex::resultEntry (entry1, rindex + b, 0, points, b);
         }
         indx.setEntry (*iobj, icPowerspectrum, step, entry1.str());
      
         // crosscorrelation/coherecne common
         ostringstream		entryCommon;
         int i = 0;
         for (tmpresults::iterator iter = tmps.begin();
             (iter != tmps.end()) && (i < numA); iter++, i++) {
            diagIndex::channelEntry (entryCommon, i, iter->name, 'A');
         }
         b = 0;
         for (tmpresults::iterator iter = tmps.begin();
             iter != tmps.end(); iter++, b++) {
            diagIndex::channelEntry (entryCommon, b, iter->name, 'B');
         }
        // crosscorrelation entry
         ostringstream		entry2;
         i = 0;
         for (tmpresults::iterator iter = tmps.begin();
             (iter != tmps.end()) && (i < numA); iter++, i++) {
            if (i == 0) {
               entry2 << entryCommon.str();
            }
            b = 0;
            for (tmpresults::iterator iter = tmps.begin();
                iter != tmps.end(); iter++, b++) {
               if (b != i) {
                  int ofs = (b < i) ? b * points : (b - 1) * points;
                  diagIndex::resultEntry (entry2, rindex + tmps.size() + i, 
                                       ofs, points, i, b);
               }
            }
         }
         indx.setEntry (*iobj, icCrosscorrelation, step, entry2.str());
      
         // coherence entry
         ostringstream		entry3;
         i = 0;
         for (tmpresults::iterator iter = tmps.begin();
             (iter != tmps.end()) && (i < numA); iter++, i++) {
            if (i == 0) {
               entry3 << entryCommon.str();
            }
            b = 0;
            for (tmpresults::iterator iter = tmps.begin();
                iter != tmps.end(); iter++, b++) {
               if (b != i) {
                  int ofs = (b < i) ? b * points : (b - 1) * points;
                  diagIndex::resultEntry (entry3, 
                                       rindex + tmps.size() + numA + i, 
                                       ofs, points, i, b);
               }
            }
         }
         indx.setEntry (*iobj, icCoherence, step, entry3.str());
      }
   
      /////////////////////////////////////////////////////////
      // Calculate FFT of all channels			     //
      /////////////////////////////////////////////////////////
      if (!callChannelAnalysis (id, measnum, 
                           (channelAnalysis) &ffttest::fft)) {
         if (my_debug) cerr << "ffttest::analyze() return false line " << __LINE__ << endl ;
         return false;
      }
   
      /////////////////////////////////////////////////////////
      // Calculate cross-correlation & coherence	     //
      /////////////////////////////////////////////////////////
      if (!callChannelAnalysis (id, measnum, 
                           (channelAnalysis) &ffttest::cross)) {
         if (my_debug) cerr << "ffttest::analyze() return false line " << __LINE__ << endl ;
         return false;
      }
   
      // return
      note = true;
      if (my_debug) cerr << "ffttest::analyze() return true" << endl ;
      return true;
   }



   bool ffttest::fft (int resultnum, int measnum, 
                     string chnname, bool stim, const callbackarg& id)
   {
      if (my_debug) cerr << "ffttest::fft(resultnum = " << resultnum << ", measnum = " << measnum << ", chnname = " << chnname << ", stim = " << (stim ? "true" : "false") << ",...)" << endl ;
      // get time series access class
      const diagResult& aChn = diagChn::self();
      // get time series
      gdsDataObject* chndat = storage->findData (chnname);
      if ((chndat == 0) || (chndat->value == 0)) {
         if (my_debug) cerr << "ffttest::fft() return false line " << __LINE__ << endl ;

         return false;
      }
      if (chndat->error) {
         cerr << "CHANNEL DATA ERROR *******************************" << endl;
      }
      int N;
      if (!aChn.getParam (*chndat, stTimeSeriesN, N)) {
         if (my_debug) cerr << "ffttest::fft() return false line " << __LINE__ << endl ;
         return false;
      }
   
      // get result access class
      const diagResult* aRes = diagResult::self (stObjectTypeSpectrum);
      if (aRes == 0) {
         if (my_debug) cerr << "ffttest::fft() return false line " << __LINE__ << endl ;
         return false;
      }
   
      // get result object
      string		resname = 
         diagObjectName::makeName (stResult, rindex + resultnum);
      gdsDataObject* 	res = storage->findData (resname);
      if (measnum == 0) {
         // init analysis result first time around
         if (res != 0) {
            storage->erase (resname);
         }
         res = aRes->newObject (0, points, 0, rindex + resultnum, 
                              -1, gds_float32);
         if (res != 0) {
            // set parameters of result
            aRes->setParam (*res, stSpectrumSubtype, 1);
            aRes->setParam (*res, stSpectrumf0, fStart);
            aRes->setParam (*res, stSpectrumdf, BW);
            aRes->setParam (*res, stSpectrumt0, T0);
            aRes->setParam (*res, stSpectrumdt, 
                           1.0 / (measTime + mTimeAdd));
            aRes->setParam (*res, stSpectrumBW, windowBW);
            aRes->setParam (*res, stSpectrumWindow, window);
            aRes->setParam (*res, stSpectrumAverageType, averageType);
            aRes->setParam (*res, stSpectrumAverages, averages);
            aRes->setParam (*res, stSpectrumChannelA, 
                           tmps[resultnum].name);
            aRes->setParam (*res, stSpectrumN, points);
            aRes->setParam (*res, stSpectrumM, 1);
            storage->addData (*res, false);
         }
      }
      if (res == 0) {
         if (my_debug) cerr << "ffttest::fft() return false line " << __LINE__ << endl ;
         return false;
      }
   
      // perform FFT
      tmps[resultnum].lock.writelock();
      int dtype = (fZoom == 0) ? DATA_REAL : DATA_COMPLEX;
   //    cerr << "fft pts = " << fftPoints << " points = " << points << 
   //       " data type = " << dtype << " window = " << window << endl;
      int psmode = PS_TAKE_FFT;
      if (removeDC) {
         psmode |= PS_REMOVE_DC;
      }
      if (psGen (psmode, &tmps[resultnum].prm, fftPoints, dtype, 
                (float*) chndat->value, 1.0/(fSample / decimate2), 
                OUTPUT_GDSFORMAT, window, tmps[resultnum].fft) < 0) {
         tmps[resultnum].lock.unlock();
         if (my_debug) cerr << "ffttest::fft() return false line " << __LINE__ << endl ;
         return false;
      }
      // data rotation to move start frequency to lowest bin
      if (fZoom > 0) {
         if (dataRotator (fftPoints, DATA_COMPLEX, 
                         tmps[resultnum].fft, tmps[resultnum].x) < 0) {
            tmps[resultnum].lock.unlock();
            if (my_debug) cerr << "ffttest::fft() return false line " << __LINE__ << endl ;
            return false;
         }
         int dPoints = fftPoints / 2 - 
            (int) ((fZoom - fStart) / BW + 0.5);
         // cerr << "dPoints = " << dPoints << endl;
         memcpy (tmps[resultnum].fft, tmps[resultnum].x + 2 * dPoints,
                points * sizeof (complex<float>));
      }
      // remove frequency points before fStart
      else if (fStart > 1E-12) {
         // cerr << "Remove frequency points before fStart = " << fStart << endl;
         int dPoints = (int)(fStart / BW + 0.5);
         if (dPoints + points > fftPoints / 2) {
            dPoints = fftPoints / 2 - points;
         }
         // cerr << "fft pts = " << fftPoints << " points = " << points << 
            // " dPoints = " << dPoints << endl;
         if (dPoints > 0) {
            memmove (tmps[resultnum].fft, 
                    tmps[resultnum].fft + 2 * dPoints, 
                    2 * points * sizeof (float));
         }
      }
   
      // calculate PSD and do averaging
      // Use DATA_REAL to treat bin at f=0 special
      if (fftToPs (points,  (fStart >= 1E-12) ? DATA_COMPLEX : DATA_REAL, 
                  tmps[resultnum].fft, tmps[resultnum].x) < 0) {
         tmps[resultnum].lock.unlock();
         if (my_debug) cerr << "ffttest::fft() return false line " << __LINE__ << endl ;
         return false;
      }
   
      // do averaging and store result
      avg_specs		avgprm;
      avgprm.avg_type = (averageType != 1) ? 
         AVG_LINEAR_SQUARE : AVG_EXPON_SQUARE;
      avgprm.dataset_length = points;
      avgprm.data_type = DATA_REAL;
      avgprm.number_of_averages = averages;
      int		num_so_far = measnum;
      if (my_debug) cerr << "points = " << points << " averages = " << averages << "(" << avgprm.avg_type << ")" << " so far = " << measnum << endl;
      // cerr << "psd[" << resultnum << "][0] = " << *(float*) res->value <<
         // " x[0] = " << tmps[resultnum].x[0] << endl;
      if (avg (&avgprm, 1, tmps[resultnum].x, &num_so_far, 
              (float*) res->value) < 0) {
         tmps[resultnum].lock.unlock();
         if (my_debug) cerr << "ffttest::fft() return false line " << __LINE__ << endl ;
         return false;
      }
      // cerr << "psd[" << resultnum << "][0] = " << *(float*) res->value <<
         // " x[0] = " << tmps[resultnum].x[0] << endl;
      aRes->setParam (*res, stSpectrumAverages, 
                     (averageType != 1) ? measnum + 1 : 
                     min (measnum + 1, averages));
      tmps[resultnum].lock.unlock();
   
      if (my_debug) cerr << "ffttest::fft() return true" << endl ;
      return true;
   }


   bool ffttest::cross (int i, int measnum, string chnname, bool stim,
                     const callbackarg& id) 
   {
      if (my_debug) cerr << "ffttest::cross(i = " << i << ", measnum = " << measnum << ", chnname = " << chnname << "stim = " << (stim ? "true" : "false") << ",...)" << endl ;
      // only analyze A channels
      if (i >= numA) {
         return true;
      }
   
      // get result offset and access class
      int resofs = tmps.size();
      const diagResult* aRes = diagResult::self (stObjectTypeSpectrum);
      if (aRes == 0) {
         if (my_debug) cerr << "ffttest::cross() return false line " << __LINE__ << endl ;
         return false;
      }
   
      // initailize result objects the first time around
      if (measnum == 0) {
         // create cross-correlation result object
         string		resname = 
            diagObjectName::makeName (stResult, rindex + resofs + i);
         gdsDataObject* 	res = storage->findData (resname);
         if (res != 0) {
            storage->erase (resname);
         }
         res = aRes->newObject (0, resofs - 1, points, rindex + resofs + i, 
                              -1, gds_complex32);
         if (res == 0) {
            if (my_debug) cerr << "ffttest::cross() return false line " << __LINE__ << endl ;
            return false;
         }
         // set parameters of crosscorrelation result
         aRes->setParam (*res, stSpectrumSubtype, 2);
         aRes->setParam (*res, stSpectrumf0, fStart);
         aRes->setParam (*res, stSpectrumdf, BW);
         aRes->setParam (*res, stSpectrumt0, T0);
         aRes->setParam (*res, stSpectrumdt, 
                        1.0 / (measTime + mTimeAdd));
         aRes->setParam (*res, stSpectrumBW, windowBW);
         aRes->setParam (*res, stSpectrumWindow, window);
         aRes->setParam (*res, stSpectrumAverageType, averageType);
         aRes->setParam (*res, stSpectrumAverages, averages);
         aRes->setParam (*res, stSpectrumChannelA, 
                        tmps[i].name);
         int b = 0;
         for (tmpresults::iterator iter = tmps.begin();
             iter != tmps.end(); iter++, b++) {
            if (b != i) {
               string bChn = diagObjectName::makeName (
                                    stSpectrumChannelB, b);
               aRes->setParam (*res, bChn, iter->name);
            }
         }
         aRes->setParam (*res, stSpectrumN, points);
         aRes->setParam (*res, stSpectrumM, resofs - 1);
         storage->addData (*res, false);
      
         // create coherence result object
         resname = diagObjectName::makeName (stResult, 
                              rindex + resofs + numA + i);
         res = storage->findData (resname);
         if (res != 0) {
            storage->erase (resname);
         }
         res = aRes->newObject (0, resofs - 1, points, 
                              rindex + resofs + numA + i, -1, gds_float32);
         if (res == 0) {
            if (my_debug) cerr << "ffttest::cross() return false line " << __LINE__ << endl ;
            return false;
         }
         // set parameters of result
         aRes->setParam (*res, stSpectrumSubtype, 3);
         aRes->setParam (*res, stSpectrumf0, fStart);
         aRes->setParam (*res, stSpectrumdf, BW);
         aRes->setParam (*res, stSpectrumt0, T0);
         aRes->setParam (*res, stSpectrumdt, 
                        1.0 / (measTime + mTimeAdd));
         aRes->setParam (*res, stSpectrumBW, windowBW);
         aRes->setParam (*res, stSpectrumWindow, window);
         aRes->setParam (*res, stSpectrumAverageType, averageType);
         aRes->setParam (*res, stSpectrumAverages, averages);
         aRes->setParam (*res, stSpectrumChannelA, 
                        tmps[i].name);
         b = 0;
         for (tmpresults::iterator iter = tmps.begin();
             iter != tmps.end(); iter++, b++) {
            if (b != i) {
               string bChn = diagObjectName::makeName (
                                    stSpectrumChannelB, b);
               aRes->setParam (*res, bChn, iter->name);
            }
         }
         aRes->setParam (*res, stSpectrumN, points);
         aRes->setParam (*res, stSpectrumM, resofs - 1);
         storage->addData (*res, false);
      }
   
   
      // Calculate cross-correlation & coherence
      semlock		lock1 (tmps[i].lock);
      int b = 0;
      for (tmpresults::iterator iter = tmps.begin();
          iter != tmps.end(); iter++, b++) {
         if (b != i) {
            semlock		lock2 (tmps[b].lock);
            // get cross-correlation result object
            string		resname = 
               diagObjectName::makeName (stResult, rindex + resofs + i);
            gdsDataObject* 	res = storage->findData (resname);
            if (res == 0) {
               if (my_debug) cerr << "ffttest::cross() return false line " << __LINE__ << endl ;
               return false;
            }
            // calculate cross-correlation
            if (crossPower (points, (fStart >= 1E-12) ? DATA_COMPLEX : DATA_REAL,
                           tmps[i].fft, tmps[b].fft, tmps[i].x) < 0) {
               if (my_debug) cerr << "ffttest::cross() return false line " << __LINE__ << endl ;
               return false;
            }
         
            // average and store cross-correlation result
            int indx = (b < i) ? b : b - 1;
            float* csd = (float*) res->value + indx * 2 * points;
            avg_specs		avgprm;
            avgprm.avg_type = (averageType != 1) ? 
               AVG_LINEAR_VECTOR : AVG_EXPON_VECTOR;
            avgprm.dataset_length = points;
            avgprm.data_type = DATA_COMPLEX;
            avgprm.number_of_averages = averages;
            int			num_so_far = measnum;
            if (avg (&avgprm, 1, tmps[i].x, &num_so_far, csd) < 0) {
               if (my_debug) cerr << "ffttest::cross() return false line " << __LINE__ << endl ;
               return false;
            }
            aRes->setParam (*res, stSpectrumAverages, 
                           (averageType != 1) ? measnum + 1 : 
                           min (measnum + 1, averages));
         
            // get coherence result object
            resname = diagObjectName::makeName (stResult, 
                                 rindex + resofs + numA + i);
            res = storage->findData (resname);
            if (res == 0) {
               if (my_debug) cerr << "ffttest::cross() return false line " << __LINE__ << endl ;
               return false;
            }
            // get PSD result objects
            resname = diagObjectName::makeName (stResult, i);
            gdsDataObject* 	res1 = storage->findData (resname);
            resname = diagObjectName::makeName (stResult, b);
            gdsDataObject* 	res2 = storage->findData (resname);
            if ((res1 == 0) || (res2 == 0)) {
               if (my_debug) cerr << "ffttest::cross() return false line " << __LINE__ << endl ;
               return false;
            }
         
            // calculate coherence and store result
            // Use DATA_REAL to treat bin at f=0 special
            float* coh = (float*) res->value + indx * points;
            if (coherenceCP (points, (float*) res1->value, (float*) res2->value, 
                            csd, coh) < 0) {
               if (my_debug) cerr << "ffttest::cross() return false line " << __LINE__ << endl ;
               return false;
            }
            aRes->setParam (*res, stSpectrumAverages, 
                           (averageType != 1) ? measnum + 1 : 
                           min (measnum + 1, averages));
         }
      }
   
      if (my_debug) cerr << "ffttest::cross() return true" << endl ;
      return true;
   }


}
