/* version $Id: diagnames.h 7690 2016-08-17 00:07:17Z james.batch@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: diagnames.h						*/
/*                                                         		*/
/* Module Description: Names used by diagnostics tests			*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 28Nov98  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: diagnames.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C++ 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			OK			*/
/*			  POSIX			OK			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_DIAGNAMES_C_H
#define _GDS_DIAGNAMES_C_H

#include "gdsmain.h"

#define TEST_RATE 1

/** @name Names of diagnostics parameters.
    This module is a global resource listing of character strings used
    by diagnostics tests. It lists names of diagnostics tests, 
    names of global parameters of diagnostics tests and names of
    parameters associated with an individual diagnostics test.
    It also implement a comparison function which compares two 
    names ignoring case, blank spaces and tab characters.
   
    @memo Lists names associated with diagnostics tests
    @author Written November 1998 by Daniel Sigg
    @version 0.1
 ************************************************************************/

/*@{*/

#include <inttypes.h>

#ifdef __cplusplus
#include <string>
namespace diag {
#endif

/*----------------------------------------------------------------------*/
/*  									*/
/*  Names of diagnostics supervisory tasks				*/
/*  									*/
/*----------------------------------------------------------------------*/
   const char standardSupervisoryName[] = "Standard";


/*----------------------------------------------------------------------*/
/*  									*/
/*  Names of diagnostics tests 						*/
/*  									*/
/*----------------------------------------------------------------------*/
   const char sineresponsename[] = "SineResponse";
   const char sweptsinename[] = "SweptSine";
   const char timeseriesname[] = "TimeSeries";
   const char fftname[] = "FFT";


/*----------------------------------------------------------------------*/
/*  									*/
/*  Names of diagnostics iterators 					*/
/*  									*/
/*----------------------------------------------------------------------*/
   const char repeatIteratorName[] = "Repeat";
   const char sweepIteratorName[] = "Scan";
   const char findIteratorName[] = "Find";


/*----------------------------------------------------------------------*/
/*  									*/
/*  Object types							*/
/*  									*/
/*----------------------------------------------------------------------*/
   const char stCreator[] = "Diagnostics system";
   const char stObjectType[] = "ObjectType";
   const char stObjectFlag[] = "Flag";
   const char stObjectTypeIndex[] = "Index";
   const char stObjectTypeGlobal[] = "DiagnosticsTest";
   const char stObjectTypeDef[] = "Defaults";
   const char stObjectTypeLidax[] = "Fantom";
   const char stObjectTypeSync[] = "Synchronization";
   const char stObjectTypeEnv[] = "Environment";
   const char stObjectTypeScan[] = "Scan";
   const char stObjectTypeFind[] = "Optimization";
   const char stObjectTypeTestParameter[] = "TestParameter";
   const char stObjectTypeTimeSeries[] = "TimeSeries";
   const char stObjectTypeSpectrum[] = "Spectrum";
   const char stObjectTypeTransferFunction[] = "TransferFunction";
   const char stObjectTypeCoefficients[] = "Coefficients";
   const char stObjectTypeMeasurementTable[] = "MeasurementTable";
   const char stObjectTypePlot[] = "Plot";
   const char stObjectTypeCalibration[] = "Calibration";


/*----------------------------------------------------------------------*/
/*  									*/
/*  Globals								*/
/*  									*/
/*----------------------------------------------------------------------*/
   const char stGlobal[] = "";
   const char stInputSource[] = "InputSource";
   const char stInputSourceOnline[] = "online";
   const int  lnInputSourceOnline = 6;
   const char stInputSourceNDS[] = "NDS";
   const int  lnInputSourceNDS = 3;
   const char stInputSourceSENDS[] = "SENDS";
   const int  lnInputSourceSENDS = 5;
   const char stInputSourceLidax[] = "Lidax";
   const int  lnInputSourceLidax = 5;
   const char* const stInputSourceDef = stInputSourceOnline;
   const char stTestType[] = "TestType";
   const char* const stTestTypeDef = fftname;
   const char stSupervisory[] = "Supervisory";
   const char* const stSupervisoryDef = standardSupervisoryName;
   const char stTestIterator[] = "TestIterator";
   const char* const stTestIteratorDef = repeatIteratorName;
   const char stTestName[] = "TestName";
   const char stTestNameDef[] = "";
   const char stTestComment[] = "Comment";
   const char stTestCommentDef[] = "";
   const char stTestTime[] = "TestTime";
   const int64_t stTestTimeDef = 0;
   const char stTestTimeUTC[] = "TestTimeUTC";
   const char stTestTimeUTCDef[] = "";


/*----------------------------------------------------------------------*/
/*  									*/
/*  Defaults								*/
/*  									*/
/*----------------------------------------------------------------------*/
   const char stDef[] = "Def";
   const char stDefAllowCancel[] = "AllowCancel";
   const bool stDefAllowCancelDef = true;
   const char stDefNoStimulus[] = "NoStimulus";
   const bool stDefNoStimulusDef = false;
   const char stDefNoAnalysis[] = "NoAnalysis";
   const bool stDefNoAnalysisDef = false;
   const char stDefKeepTraces[] = "KeepTraces";
   const int stDefKeepTracesDef = 100;
   const char stDefSiteDefault[] = "SiteDefault";
   const char stDefSiteDefaultDef[] = SITE_PREFIX;
   const char stDefSiteForce[] = "SiteForce";
   const char stDefSiteForceDef = ' ';
   const char stDefIfoDefault[] = "IfoDefault";
   const char stDefIfoDefaultDef[] = IFO_PREFIX;
   const char stDefIfoForce[] = "IfoForce";
   const char stDefIfoForceDef = ' ';
   const char stDefPlotWindowLayout[] = "PlotWindowLayout";
   const char stDefPlotWindows[] = "PlotWindows";
   const char stDefCalibrationRecords[] = "CalibrationRecords";
   const char stDefReconnect[] = "Reconnect";
   const bool stDefReconnectDef = false;


/*----------------------------------------------------------------------*/
/*  									*/
/*  Lidax								*/
/*  									*/
/*----------------------------------------------------------------------*/
   const char stLidax[] = "Lidax";
   const char stLidaxServer[] = "Server";
   const char stLidaxUDN[] = "UDN";
   const char stLidaxChannel[] = "Channel";
   const char stLidaxRate[] = "Rate";


/*----------------------------------------------------------------------*/
/*  									*/
/*  Synchronization							*/
/*  									*/
/*----------------------------------------------------------------------*/
   const char stSync[] = "Sync";
   const char stSyncType[] = "Type";
   const int stSyncTypeDef = 0;
   const char stSyncStart[] = "Start";
   const int64_t stSyncStartDef = 0;
   const char stSyncWait[] = "Wait";
   const double stSyncWaitDef = 0;
   const char stSyncRepeat[] = "Repeat";
   const int stSyncRepeatDef = 1;
   const char stSyncRepeatRate[] = "RepeatRate";
   const double stSyncRepeatRateDef = 0;
   const char stSyncSlowDown[] = "SlowDown";
   const double stSyncSlowDownDef = 0;
   const char stSyncWaitForStart[] = "WaitForStart";
   const char stSyncWaitAtEachStep[] = "WaitAtEachStep";
   const char stSyncSignalEndOfStep[] = "SignalEndOfStep";
   const char stSyncSignalEnd[] = "SignalEnd";


/*----------------------------------------------------------------------*/
/*  									*/
/*  Environment								*/
/*  									*/
/*----------------------------------------------------------------------*/
   const char stEnv[] = "Env";
   const char stEnvActive[] = "Active";
   const bool stEnvActiveDef = true;
   const char stEnvChannel[] = "Channel";
   const char stEnvWaveform[] = "Waveform";
   const char stEnvPoints[] = "Points";
   const char stEnvWait[] = "Wait";
   const double stEnvWaitDef = 0;


/*----------------------------------------------------------------------*/
/*  									*/
/*  Scan								*/
/*  									*/
/*----------------------------------------------------------------------*/
   const char stScan[] = "Scan";
   const char stScanActive[] = "Active";
   const bool stScanActiveDef = true;
   const char stScanChannel[] = "Channel";
   const char stScanType[] = "Type";
   const int stScanTypeDef = 0;
   const char stScanDirection[] = "Direction";
   const int stScanDirectionDef = 0;
   const char stScanParameter[] = "Parameter";
   const int stScanParameterDef = 0;
   const char stScanFrequency[] = "Frequency";
   const double stScanFrequencyDef = 0;
   const char stScanAmplitude[] = "Amplitude";
   const double stScanAmplitudeDef = 0;
   const char stScanOffset[] = "Offset";
   const double stScanOffsetDef = 0;
   const char stScanStart[] = "Start";
   const double stScanStartDef = 0;
   const char stScanStop[] = "Stop";
   const double stScanStopDef = 1;
   const char stScanN[] = "N";
   const double stScanNDef = 10;
   const char stScanPoints[] = "Points";
   const char stScanWait[] = "Wait";
   const double stScanWaitDef[] = {0.065, 10};


/*----------------------------------------------------------------------*/
/*  									*/
/*  Find								*/
/*  									*/
/*----------------------------------------------------------------------*/
   const char stFind[] = "Find";
   const char stFindEnable[] = "Enable";
   const bool stFindEnableDef = true;
   const char stFindChange[] = "Change";
   const bool stFindChangeDef = true;
   const char stFindType[] = "Type";
   const int stFindTypeDef = 0;
   const char stFindValue[] = "Value";
   const double stFindValueDef = 0;
   const char stFindFunction[] = "Function";
   const int stFindFunctionDef = 0;
   const char stFindParam[] = "Param";
   const double stFindParamDef[] = {0, 100};
   const char stFindMethod[] = "Method";
   const int stFindMethodDef = 0;


/*----------------------------------------------------------------------*/
/*  									*/
/*  Plot Settings							*/
/*  									*/
/*----------------------------------------------------------------------*/
   const char stPlot[] = "Plot";
   const char stPlotName[] = "Name";
#ifdef STORE_OPTION_VIS
   const char stPlotOptionVisible[] = "OptionsPanelVisible";
#endif
   const char stPlotTracesGraphType[] = "TracesGraphType";
   const char stPlotTracesActive[] = "TracesActive";
   const char stPlotTracesAChannel[] = "TracesAChannel";
   const char stPlotTracesBChannel[] = "TracesBChannel";
   const char stPlotTracesPlotStyle[] = "TracesPlotStyle";
   const char stPlotTracesLineAttrColor[] = "TracesLineAttrColor";
   const char stPlotTracesLineAttrStyle[] = "TracesLineAttrStyle";
   const char stPlotTracesLineAttrWidth[] = "TracesLineAttrWidth";
   const char stPlotTracesMarkerAttrColor[] = "TracesMarkerAttrColor";
   const char stPlotTracesMarkerAttrStyle[] = "TracesMarkerAttrStyle";
   const char stPlotTracesMarkerAttrSize[] = "TracesMarkerAttrSize";
   const char stPlotTracesBarAttrColor[] = "TracesBarAttrColor";
   const char stPlotTracesBarAttrStyle[] = "TracesBarAttrStyle";
   const char stPlotTracesBarAttrWidth[] = "TracesBarAttrWidth";
   const char stPlotRangeAxisScale[] = "RangeAxisScale";
   const char stPlotRangeRange[] = "RangeRange";
   const char stPlotRangeRangeFrom[] = "RangeRangeFrom";
   const char stPlotRangeRangeTo[] = "RangeRangeTo";
   const char stPlotRangeBin[] = "RangeBin";
   const char stPlotRangeBinLogSpacing[] = "RangeBinLogSpacing";
   const char stPlotUnitsXValues[] = "UnitsXValues";
   const char stPlotUnitsYValues[] = "UnitsYValues";
   const char stPlotUnitsXUnit[] = "UnitsXUnit";
   const char stPlotUnitsYUnit[] = "UnitsYUnit";
   const char stPlotUnitsXMag[] = "UnitsXMag";
   const char stPlotUnitsYMag[] = "UnitsYMag";
   const char stPlotUnitsXSlope[] = "UnitsXSlope";
   const char stPlotUnitsYSlope[] = "UnitsYSlope";
   const char stPlotUnitsXOffset[] = "UnitsXOffset";
   const char stPlotUnitsYOffset[] = "UnitsYOffset";
   const char stPlotCursorActive[] = "CursorActive";
   const char stPlotCursorTrace[] = "CursorTrace";
   const char stPlotCursorStyle[] = "CursorStyle";
   const char stPlotCursorType[] = "CursorType";
   const char stPlotCursorX[] = "CursorX";
   const char stPlotCursorH[] = "CursorH";
   const char stPlotCursorValid[] = "CursorValid";
   const char stPlotCursorY[] = "CursorY";
   const char stPlotCursorN[] = "CursorN";
   const char stPlotCursorXDiff[] = "CursorXDiff";
   const char stPlotCursorYDiff[] = "CursorYDiff";
   const char stPlotCursorMean[] = "CursorMean";
   const char stPlotCursorRMS[] = "CursorRMS";
   const char stPlotCursorStdDev[] = "CursorStdDev";
   const char stPlotCursorSum[] = "CursorSum";
   const char stPlotCursorSqrSum[] = "CursorSqrSum";
   const char stPlotCursorArea[] = "CursorArea";
   const char stPlotCursorRMSArea[] = "CursorRMSArea";
   const char stPlotCursorPeakX[] = "CursorPeakX";
   const char stPlotCursorPeakY[] = "CursorPeakY";
   const char stPlotConfigAutoConfig[] = "ConfigAutoConfig";
   const char stPlotConfigRespectUser[] = "ConfigRespectUser";
   const char stPlotConfigAutoAxes[] = "ConfigAutoAxes";
   const char stPlotConfigAutoBin[] = "ConfigAutoBin";
   const char stPlotConfigAutoTimeAdjust[] = "ConfigAutoTimeAdjust";
   const char stPlotStyleTitle[] = "StyleTitle";
   const char stPlotStyleTitleAlign[] = "StyleTitleAlign";
   const char stPlotStyleTitleAngle[] = "StyleTitleAngle";
   const char stPlotStyleTitleColor[] = "StyleTitleColor";
   const char stPlotStyleTitleFont[] = "StyleTitleFont";
   const char stPlotStyleTitleSize[] = "StyleTitleSize";
   const char stPlotStyleMargin[] = "StyleMargin";
   const char stPlotAxisXTitle[] = "AxisXTitle";
   const char stPlotAxisXAxisAttrAxisColor[] = "AxisXAxisAttrAxisColor";
   const char stPlotAxisXAxisAttrLabelColor[] = "AxisXAxisAttrLabelColor";
   const char stPlotAxisXAxisAttrLabelFont[] = "AxisXAxisAttrLabelFont";
   const char stPlotAxisXAxisAttrLabelOffset[] = "AxisXAxisAttrLabelOffset";
   const char stPlotAxisXAxisAttrLabelSize[] = "AxisXAxisAttrLabelSize";
   const char stPlotAxisXAxisAttrNdividions[] = "AxisXAxisAttrNdividions";
   const char stPlotAxisXAxisAttrTickLength[] = "AxisXAxisAttrTickLength";
   const char stPlotAxisXAxisAttrTitleOffset[] = "AxisXAxisAttrTitleOffset";
   const char stPlotAxisXAxisAttrTitleSize[] = "AxisXAxisAttrTitleSize";
   const char stPlotAxisXAxisAttrTitleColor[] = "AxisXAxisAttrTitleColor";
   const char stPlotAxisXGrid[] = "AxisXGrid";
   const char stPlotAxisXBothSides[] = "AxisXBothSides";
   const char stPlotAxisXCenterTitle[] = "AxisXCenterTitle";
   const char stPlotAxisYTitle[] = "AxisYTitle";
   const char stPlotAxisYAxisAttrAxisColor[] = "AxisYAxisAttrAxisColor";
   const char stPlotAxisYAxisAttrLabelColor[] = "AxisYAxisAttrLabelColor";
   const char stPlotAxisYAxisAttrLabelFont[] = "AxisYAxisAttrLabelFont";
   const char stPlotAxisYAxisAttrLabelOffset[] = "AxisYAxisAttrLabelOffset";
   const char stPlotAxisYAxisAttrLabelSize[] = "AxisYAxisAttrLabelSize";
   const char stPlotAxisYAxisAttrNdividions[] = "AxisYAxisAttrNdividions";
   const char stPlotAxisYAxisAttrTickLength[] = "AxisYAxisAttrTickLength";
   const char stPlotAxisYAxisAttrTitleOffset[] = "AxisYAxisAttrTitleOffset";
   const char stPlotAxisYAxisAttrTitleSize[] = "AxisYAxisAttrTitleSize";
   const char stPlotAxisYAxisAttrTitleColor[] = "AxisYAxisAttrTitleColor";
   const char stPlotAxisYGrid[] = "AxisYGrid";
   const char stPlotAxisYBothSides[] = "AxisYBothSides";
   const char stPlotAxisYCenterTitle[] = "AxisYCenterTitle";
   const char stPlotLegendShow[] = "LegendShow";
   const char stPlotLegendPlacement[] = "LegendPlacement";
   const char stPlotLegendXAdjust[] = "LegendXAdjust";
   const char stPlotLegendYAdjust[] = "LegendYAdjust";
   const char stPlotLegendSymbolStyle[] = "LegendSymbolStyle";
   const char stPlotLegendTextStyle[] = "LegendTextStyle";
   const char stPlotLegendSize[] = "LegendSize";
   const char stPlotLegendText[] = "LegendText";
   const char stPlotParamShow[] = "ParamShow";
   const char stPlotParamT0[] = "ParamT0";
   const char stPlotParamAvg[] = "ParamAvg";
   const char stPlotParamSpecial[] = "ParamSpecial";
   const char stPlotParamTimeFormatUTC[] = "ParamTimeFormatUTC";
   const char stPlotParamTextSize[] = "ParamTextSize";


/*----------------------------------------------------------------------*/
/*  									*/
/*  Calibration Record							*/
/*  									*/
/*----------------------------------------------------------------------*/
   const char stCal[] = "Calibration";
   const char stCalChannel[] = "Channel";
   const char stCalTime[] = "Time";
   const char stCalDuration[] = "Duration";
   const char stCalReference[] = "Reference";
   const char stCalUnit[] = "Unit";
   const char stCalConversion[] = "Conversion";
   const char stCalOffset[] = "Offset";
   const char stCalTimeDelay[] = "TimeDelay";
   const char stCalTransferFunction[] = "TransferFunction";
   const char stCalGain[] = "Gain";
   const char stCalPoles[] = "Poles";
   const char stCalZeros[] = "Zeros";
   const char stCalDefault[] = "Default";
   const char stCalPreferredMag[] = "PreferredMag";
   const char stCalPreferredD[] = "PreferredD";
   const char stCalComment[] = "Comment";


/*----------------------------------------------------------------------*/
/*  									*/
/*  Index								*/
/*  									*/
/*----------------------------------------------------------------------*/
   const char stIndex[] = "Index";
   const char stIndexEntry[] = "Entry";

   /* index categories */
   const char icMasterIndex[] = "MasterIndex";
   const char icTimeseries[] = "TimeSeries";
   const char icPowerspectrum[] = "PowerSpectrum";
   const char icCoherence[] = "Coherence";
   const char icCrosscorrelation[] = "CrossCorrelation";
   const char icTransferCoeff[] = "TransferCoefficients";
   const char icTransferMatrix[] = "TransferMatrix";
   const char icCoherenceCoeff[] = "CoherenceCoefficients";
   const char icHarmonicCoeff[] = "HarmonicCoefficients";
   const char icIntermodulationCoeff[] = "IntermodulationCoefficients";
   const char icTransferFunction[] = "TransferFunction";
   const char icCoherenceFunction[] = "CoherenceFunction";
   const char* const icAll[] = {
   icMasterIndex, icTimeseries, 
   icPowerspectrum, icCoherence, icCrosscorrelation, 
   icTransferFunction, icCoherenceFunction, 
   icTransferCoeff, icCoherenceCoeff, 
   icHarmonicCoeff, icIntermodulationCoeff, 
   icTransferMatrix, 0};

   /* index entries */
   const char ieChannel[] = "Channel";
   const char ieChannelA[] = "ChannelA";
   const char ieChannelB[] = "ChannelB";
   const char ieName[] = "Name";
   const char ieOffset[] = "Offset";
   const char ieLength[] = "Length";
   const char* const ieAll[] = {
   ieChannel, ieChannelA, ieChannelB, ieName, ieOffset, ieLength, 0};


/*----------------------------------------------------------------------*/
/*  									*/
/*  Result								*/
/*  									*/
/*----------------------------------------------------------------------*/
   const char stResult[] = "Result";
   const char stChannel[] = "Channel";
   const char stReference[] = "Reference";
   const char stResultSubtype[] = "Subtype";
   const char stResultt0[] = "t0";
   const char stResultdt[] = "dt";
   const char stResultf0[] = "f0";
   const char stResultdf[] = "df";
   const char stResultBW[] = "BW";
   const char stResultWindow[] = "Window";
   const char stResultAverageType[] = "AverageType";
   const char stResultAverages[] = "Averages";
   const char stResultChannel[] = "Channel";
   const char stResultChannelA[] = "ChannelA";
   const char stResultChannelB[] = "ChannelB";
   const char stResultM[] = "M";
   const char stResultN[] = "N";
   const char stResultMeasurementNumber[] = "MeasurementNumber";

   /*  Time Series */
   const char* const stTimeSeriesSubtype = stResultSubtype;
   const int  stTimeSeriesSubtypeDef = 0;
   const char* const stTimeSeriest0 = stResultt0;
   const int64_t stTimeSeriest0Def = 0;
   const char* const stTimeSeriesdt = stResultdt;
   const double stTimeSeriesdtDef = 1;
   const char stTimeSeriestp[] = "tp";
   const double stTimeSeriestpDef = 0;
   const char stTimeSeriestf0[] = "tf0";
   const double stTimeSeriestf0Def = 0;
   const char* const stTimeSeriesf0 = stResultf0;
   const double stTimeSeriesf0Def = 0;
   const char* const stTimeSeriesAverageType = stResultAverageType;
   const int stTimeSeriesAverageTypeDef = 0;
   const char* const stTimeSeriesAverages = stResultAverages;
   const int stTimeSeriesAveragesDef = 1;
   const char stTimeSeriesDecimation[] = "Decimation";
   const int stTimeSeriesDecimationDef = 1;
   const char stTimeSeriesDecimation1[] = "Decimation1";
   const int stTimeSeriesDecimation1Def = 1;
   const char stTimeSeriesDecimationType[] = "DecimationType";
   const int stTimeSeriesDecimationTypeDef = 1;
   const char stTimeSeriesDecimationFilter[] = "DecimationFilter";
   const char stTimeSeriesDecimationDelay[] = "DecimationDelay";
   const double stTimeSeriesDecimationDelayDef = 0.0;
   const char stTimeSeriesDelayTaps[] = "DelayTaps";
   const int stTimeSeriesDelayTapsDef = 0;   
   const char stTimeSeriesTimeDelay[] = "TimeDelay";
   const double stTimeSeriesTimeDelayDef = 0.0;
   const char* const stTimeSeriesChannel = stResultChannel;
   const char stTimeSeriesChannelDef[] = "";
   const char* const stTimeSeriesN = stResultN;
   const int stTimeSeriesNDef = 0;
   const char* const stTimeSeriesMeasurementNumber = 
   stResultMeasurementNumber;

   /* Spectrum */
   const char* const stSpectrumSubtype = stResultSubtype;
   const int  stSpectrumSubtypeDef = 1;
   const char* const stSpectrumf0 = stResultf0;
   const double stSpectrumf0Def = 0;
   const char* const stSpectrumdf = stResultdf;
   const double stSpectrumdfDef = 1;
   const char* const stSpectrumt0 = stResultt0;
   const int64_t stSpectrumt0Def = 0;
   const char* const stSpectrumdt = stResultdt;
   const double stSpectrumdtDef = 0;
   const char* const stSpectrumBW = stResultBW;
   const double stSpectrumBWDef = 0;
   const char* const stSpectrumWindow = stResultWindow;
   const int stSpectrumWindowDef = 1;
   const char* const stSpectrumAverageType = stResultAverageType;
   const int stSpectrumAverageTypeDef = 0;
   const char* const stSpectrumAverages = stResultAverages;
   const int stSpectrumAveragesDef = 1;
   const char* const stSpectrumChannelA = stResultChannelA;
   const char stSpectrumChannelADef[] = "";
   const char* const stSpectrumChannelB = stResultChannelB;
   const char* const stSpectrumN = stResultN;
   const int stSpectrumNDef = 0;
   const char* const stSpectrumM = stResultM;
   const int stSpectrumMDef = 0;
   const char* const stSpectrumMeasurementNumber = 
   stResultMeasurementNumber;

   /* Transfer function */
   const char* const stTransferFunctionSubtype = stResultSubtype;
   const int  stTransferFunctionSubtypeDef = 0;
   const char* const stTransferFunctionf0 = stResultf0;
   const double stTransferFunctionf0Def = 0;
   const char* const stTransferFunctiondf = stResultdf;
   const double stTransferFunctiondfDef = 1;
   const char* const stTransferFunctiont0 = stResultt0;
   const int64_t stTransferFunctiont0Def = 0;
   const char* const stTransferFunctionBW = stResultBW;
   const double stTransferFunctionBWDef = 0;
   const char* const stTransferFunctionWindow = stResultWindow;
   const int stTransferFunctionWindowDef = 0;
   const char* const stTransferFunctionAverageType = stResultAverageType;
   const int stTransferFunctionAverageTypeDef = 0;
   const char* const stTransferFunctionAverages = stResultAverages;
   const int stTransferFunctionAveragesDef = 1;
   const char* const stTransferFunctionChannelA = stResultChannelA;
   const char stTransferFunctionChannelADef[] = "";
   const char* const stTransferFunctionChannelB = stResultChannelB;
   const char stTransferFunctionChannelBDef[] = "";
   const char* const stTransferFunctionN = stResultN;
   const int stTransferFunctionNDef = 0;
   const char* const stTransferFunctionM = stResultM;
   const int stTransferFunctionMDef = 0;
   const char* const stTransferFunctionMeasurementNumber = 
   stResultMeasurementNumber;

   /* Coefficients */
   const char* const stCoefficientsSubtype = stResultSubtype;
   const int  stCoefficientsSubtypeDef = 0;
   const char* const stCoefficientsf = "f";
   const double stCoefficientsf0Def = 0;
   const char* const stCoefficientst0 = stResultt0;
   const int64_t stCoefficientst0Def = 0;
   const char* const stCoefficientsBW = stResultBW;
   const double stCoefficientsBWDef = 0;
   const char* const stCoefficientsWindow = stResultWindow;
   const int stCoefficientsWindowDef = 0;
   const char* const stCoefficientsAverageType = stResultAverageType;
   const int stCoefficientsAverageTypeDef = 0;
   const char* const stCoefficientsAverages = stResultAverages;
   const int stCoefficientsAveragesDef = 1;
   const char* const stCoefficientsChannelA = stResultChannelA;
   const char* const stCoefficientsChannelB = stResultChannelB;
   const char* const stCoefficientsN = stResultN;
   const int stCoefficientsNDef = 0;
   const char* const stCoefficientsM = stResultM;
   const int stCoefficientsMDef = 0;
   const char* const stCoefficientsMeasurementNumber = 
   stResultMeasurementNumber;

   /* MeasurementTable */
   const char* const stMeasurementTableSubtype = stResultSubtype;
   const int  stMeasurementTableSubtypeDef = 0;
   const char* const stMeasurementTablet0 = stResultt0;
   const int64_t stMeasurementTablet0Def = 0;
   const char stMeasurementTableTableLength[] = "TableLength";
   const int stMeasurementTableTableLengthDef = 0;
   const char stMeasurementTableName[] = "Name";
   const char stMeasurementTableNameDef[] = "";
   const char stMeasurementTableUnit[] = "Unit";
   const char stMeasurementTableUnitDef[] = "";
   const char stMeasurementTableDescription[] = "Description";
   const char stMeasurementTableDescriptionDef[] = "";
   const char stMeasurementTableValueType[] = "ValueType";
   const char stMeasurementTableValueTypeDef[] = "";


/*----------------------------------------------------------------------*/
/*  									*/
/*  Test								*/
/*  									*/
/*----------------------------------------------------------------------*/
   /* common */
   const char stTestParameter[] = "Test";
   const char stTestParameterSubtype[] = "Subtype";
   const char stAverages[] = "Averages";
   const char stAverageType[] = "AverageType";
   const char stBurstNoiseQuietTime_s[] = "BurstNoiseQuietTime_s";
   const char stMeasurementChannel[] = "MeasurementChannel";
   const char stMeasurementChannelActive[] = "MeasurementActive";
   const char stMeasurementChannelRate[] = "MeasurementChannelRate" ;
   const char stStimulusActive[] = "StimulusActive";
   const char stStimulusType[] = "StimulusType";
   const char stStimulusChannel[] = "StimulusChannel";
   const char stStimulusReadback[] = "StimulusReadback";
   const char stStimulusFrequency[] = "StimulusFrequency";
   const char stStimulusAmplitude[] = "StimulusAmplitude";
   const char stStimulusOffset[] = "StimulusOffset";
   const char stStimulusPhase[] = "StimulusPhase";
   const char stStimulusRatio[] = "StimulusRatio";
   const char stStimulusFrequencyRange[] = "StimulusFrequencyRange";
   const char stStimulusAmplitudeRange[] = "StimulusAmplitudeRange";
   const char stStimulusFilterCmd[] = "StimulusFilter";
   const char stStimulusPoints[] = "StimulusPoints";


   /* Sine Response */
   const char stSineResponse[] = "SineResponse";
   const char srMeasurementTime[] = "MeasurementTime";
   const double srMeasurementTimeDef[] = {0.1, 10};
   const char srSettlingTime[] = "SettlingTime";
   const double srSettlingTimeDef = 0.25;
   const char srRampDown[] = "RampDown" ;
   const double srRampDownDef = 0 ;
   const char srRampUp[] = "RampUp" ;
   const double srRampUpDef = 0 ;
   const char* const srAverages = stAverages;
   const int srAveragesDef = 1;
   const char* const srAverageType = stAverageType;
   const int srAverageTypeDef = 0;
   const char* const srStimulusActive = stStimulusActive;
   const char* const srStimulusChannel = stStimulusChannel;
   const char* const srStimulusReadback = stStimulusReadback;
   const char* const srStimulusFrequency = stStimulusFrequency;
   const double srStimulusFrequencyDef = 100;
   const char* const srStimulusAmplitude = stStimulusAmplitude;
   const double srStimulusAmplitudeDef = 0;
   const char* const srStimulusOffset = stStimulusOffset;
   const double srStimulusOffsetDef = 0;
   const char* const srStimulusPhase = stStimulusPhase;
   const double srStimulusPhaseDef = 0;
   const char* const srMeasurementChannelActive = 
   stMeasurementChannelActive;
   const char* const srMeasurementChannel = stMeasurementChannel;
   const char* const srMeasurementChannelRate = stMeasurementChannelRate ;
   const char srHarmonicOrder[] = "HarmonicOrder";
   const int srHarmonicOrderDef = 1;
   const char srWindow[] = "Window";
   const int srWindowDef = 1;
   const char srFFTResult[] = "FFTResult";
   const bool srFFTResultDef = true;

   /* Swept Sine */
   const char stSweptSine[] = "SweptSine";
   const char ssSweepType[]= "SweepType";
   const int ssSweepTypeDef = 1;
   const char ssSweepDirection[] = "SweepDirection";
   const int ssSweepDirectionDef = 1;
   const char ssStartFrequency[] = "StartFrequency";
   const double ssStartFrequencyDef = 1;
   const char ssStopFrequency[] = "StopFrequency";
   const double ssStopFrequencyDef = 1000;
   const char ssRampDown[] = "RampDown" ;
   const double ssRampDownDef = 0 ;
   const char ssRampUp[] = "RampUp" ;
   const double ssRampUpDef = 0 ;
   const char ssSweepPoints[] = "SweepPoints";
   const char ssNumberOfPoints[] = "NumberOfPoints";
   const int ssNumberOfPointsDef = 61;
   const char ssAChannels[] = "AChannels";
   const int ssAChannelsDef = 0;
   const char* const ssAverages = stAverages;
   const int ssAveragesDef = 1;
   const char ssMeasurementTime[] = "MeasurementTime";
   const double ssMeasurementTimeDef[] = {0.1, 10};
   const char ssSettlingTime[] = "SettlingTime";
   const double ssSettlingTimeDef = 0.25;
   const char* const ssStimulusChannel = stStimulusChannel;
   const char* const ssStimulusReadback = stStimulusReadback;
   const char* const ssStimulusAmplitude = stStimulusAmplitude;
   const double ssStimulusAmplitudeDef = 0;
   const char* const ssMeasurementChannelActive = 
   stMeasurementChannelActive;
   const char* const ssMeasurementChannel = stMeasurementChannel;
   const char* const ssMeasurementChannelRate = stMeasurementChannelRate ;
   const char ssHarmonicOrder[] = "HarmonicOrder";
   const int ssHarmonicOrderDef = 1;
   const char ssWindow[] = "Window";
   const int ssWindowDef = 1;
   const char ssFFTResult[] = "FFTResult";
   const bool ssFFTResultDef = false;

   /* FFT */
   const char stFFT[] = "FFT";
   const char fftStartFrequency[] = "StartFrequency";
   const double fftStartFrequencyDef = 0;
   const char fftStopFrequency[] = "StopFrequency";
   const double fftStopFrequencyDef = 900;
   const char fftBW[] = "BW";
   const double fftBWDef = 1;
   const char fftRampDown[] = "RampDown" ;
   const double fftRampDownDef = 0 ;
   const char fftRampUp[] = "RampUp" ;
   const double fftRampUpDef = 0 ;
   const char fftOverlap[] = "Overlap";
   const double fftOverlapDef = 0.5;
   const char fftWindow[] = "Window";
   const int fftWindowDef = 1;
   const char fftRemoveDC[] = "RemoveDC";
   const bool fftRemoveDCDef = false;
   const char fftAChannels[] = "AChannels";
   const int fftAChannelsDef = 0;
   const char fftSettlingTime[] = "SettlingTime";
   const double fftSettlingTimeDef = 0;
   const char* const fftAverageType = stAverageType;
   const int fftAverageTypeDef = 0;
   const char* const fftAverages = stAverages;
   const int fftAveragesDef = 10;
   const char* const fftBurstNoiseQuietTime_s = stBurstNoiseQuietTime_s;
   const double fftBurstNoiseQuietTime_sDef=0;
   const char* const fftStimulusActive = stStimulusActive;
   const char* const fftStimulusType = stStimulusType;
   const int fftStimulusTypeDef = 0;
   const char* const fftStimulusChannel = stStimulusChannel;
   const char* const fftStimulusReadback = stStimulusReadback;
   const char* const fftStimulusFrequency = stStimulusFrequency;
   const double fftStimulusFrequencyDef = 100;
   const char* const fftStimulusAmplitude = stStimulusAmplitude;
   const double fftStimulusAmplitudeDef = 0;
   const char* const fftStimulusOffset = stStimulusOffset;
   const double fftStimulusOffsetDef = 0;
   const char* const fftStimulusPhase = stStimulusPhase;
   const double fftStimulusPhaseDef = 0;
   const char* const fftStimulusRatio = stStimulusRatio;
   const double fftStimulusRatioDef = 0.5;
   const char* const fftStimulusFrequencyRange = stStimulusFrequencyRange;
   const double fftStimulusFrequencyRangeDef = 10000;
   const char* const fftStimulusAmplitudeRange = stStimulusAmplitudeRange;
   const double fftStimulusAmplitudeRangeDef = 0;
   const char* const fftStimulusFilterCmd = stStimulusFilterCmd;
   const char* const fftStimulusPoints = stStimulusPoints;
   const char* const fftMeasurementChannelActive = 
   stMeasurementChannelActive;
   const char* const fftMeasurementChannel = stMeasurementChannel;
   const char* const fftMeasurementChannelRate = stMeasurementChannelRate ;

   /* Time Series */
   const char stTimeSeries[] = "TimeSeries";
   const char tsMeasurementTime[] = "MeasurementTime";
   const double tsMeasurementTimeDef = 10;
   const char tsPreTriggerTime[] = "PreTriggerTime";
   const double tsPreTriggerTimeDef = 0;
   const char tsSettlingTime[] = "SettlingTime";
   const double tsSettlingTimeDef = 0;
   const char tsRampDown[] = "RampDown" ;
   const double tsRampDownDef = 0 ;
   const char tsRampUp[] = "RampUp" ;
   const double tsRampUpDef = 0 ;
   const char tsDeadTime[] = "DeadTime";
   const double tsDeadTimeDef = 0;
   const char tsBW[] = "BW";
   const double tsBWDef = 10000;
   const char tsIncludeStatistics[] = "IncludeStatistics";
   const bool tsIncludeStatisticsDef = false;
   const char* const tsAverages = stAverages;
   const int tsAveragesDef = 1;
   const char* const tsBurstNoiseQuietTime_s = stBurstNoiseQuietTime_s;
   const int tsBurstNoiseQuietTime_sDef = 0;
   const char* const tsAverageType = stAverageType;
   const int tsAverageTypeDef = 0;
   const char* const tsFilter = "Filter";
   const char* const tsFilterDef = 0;
   const char* const tsStimulusActive = stStimulusActive;
   const char* const tsStimulusType = stStimulusType;
   const int tsStimulusTypeDef = 0;
   const char* const tsStimulusChannel = stStimulusChannel;
   const char* const tsStimulusReadback = stStimulusReadback;
   const char* const tsStimulusFrequency = stStimulusFrequency;
   const double tsStimulusFrequencyDef = 0;
   const char* const tsStimulusAmplitude = stStimulusAmplitude;
   const double tsStimulusAmplitudeDef = 0;
   const char* const tsStimulusOffset = stStimulusOffset;
   const double tsStimulusOffsetDef = 0;
   const char* const tsStimulusPhase = stStimulusPhase;
   const double tsStimulusPhaseDef = 0;
   const char* const tsStimulusRatio = stStimulusRatio;
   const double tsStimulusRatioDef = 0.5;
   const char* const tsStimulusFrequencyRange = stStimulusFrequencyRange;
   const double tsStimulusFrequencyRangeDef = 10000;
   const char* const tsStimulusAmplitudeRange = stStimulusAmplitudeRange;
   const double tsStimulusAmplitudeRangeDef = 0;
   const char* const tsStimulusFilterCmd = stStimulusFilterCmd;
   const char* const tsStimulusPoints = stStimulusPoints;
   const char* const tsMeasurementChannelActive = 
   stMeasurementChannelActive;
   const char* const tsMeasurementChannel = stMeasurementChannel;
   const char* const tsMeasurementChannelRate = stMeasurementChannelRate ;

   /* Random Response */
   const char stRandomResponse[] = "RandomResponse";


/*----------------------------------------------------------------------*/
/*  									*/
/*  Names of global diagnostics test parameters				*/
/*  									*/
/*----------------------------------------------------------------------*/
   const char* const 	diag_classname = "test class";
   const char* const 	diag_testname = "test name";


/*----------------------------------------------------------------------*/
/*  									*/
/*  Names of test iterator parameters					*/
/*  									*/
/*----------------------------------------------------------------------*/
   const char* const 	testiter_repeatnum = "iterations";


/*----------------------------------------------------------------------*/
/*  									*/
/*  Comparison function (ignores case, blanks and tabs)			*/
/*  									*/
/*----------------------------------------------------------------------*/
#ifdef __cplusplus
   extern "C" 
#endif
/** Compares two names by ignoring case, blank spaces and tab characters.
    The function returns a negative value if s1 < s1, zero if s1 = s2
    and a positive value if s1 > s2. 

    In C++ this function is overloaded
    using string objects instead of character pointers for either and 
    both arguments.

    @param s1 first name
    @param s2 second name
    @return <0 if s1 < s2, 0 if s1 == s2 and >0 if s1 > s2
    @author DS, November 98
    @see Diagnostics Test Facility
 ************************************************************************/
   int compareTestNames (const char* s1, const char* s2);

/*@}*/

#ifdef __cplusplus
   inline int compareTestNames (const std::string& s1, const char* s2) {
      return compareTestNames (s1.c_str(), s2);
   }
   inline int compareTestNames (const char* s1, const std::string& s2) {
      return compareTestNames (s1, s2.c_str());
   }
   inline int compareTestNames (const std::string& s1, const std::string& s2) {
      return compareTestNames (s1.c_str(), s2.c_str());
   }

}
#endif

#endif /* _GDS_DIAGNAMES_C_H */
