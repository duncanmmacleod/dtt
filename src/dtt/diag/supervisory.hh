/* version $Id: supervisory.hh 6916 2013-11-15 18:15:10Z james.batch@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: supervisory.h						*/
/*                                                         		*/
/* Module Description: supervisory class for test scheduling		*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 12Apr99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: testiter.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C++ 5.0				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			OK			*/
/*			  POSIX			OK			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS NW17-161				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_SUPERVISORY_H
#define _GDS_SUPERVISORY_H

/* Header File List: */
#include <string>
#include <sstream>
#include "tconv.h"
#include "gmutex.hh"
#include "gdsnotify.hh"
#include "testenv.hh"

namespace diag {

   class diagStorage;
   class dataBroker;
   class testpointMgr;


/** @name Supervisory Task
   
    @memo Abstract objects representing supervisory tasks
    @author Written April 1999 by Daniel Sigg
    @version 0.1
 ************************************************************************/

//@{

/** Test supervisory
    This object implements a rudimentary diagnostics supervisory task.
    It is an abstract object which all supervisory task must inherit.
    It implements a name, a mutex, an error message stream, a pointer
    to a storage object, a command notification object, an abort and a
    pause variable.
   
    @memo Object for implementing a diagnostics supervisory task
    @author Written April 1999 by Daniel Sigg
    @version 0.1
 ************************************************************************/
   class supervisory {   
   public:
   
      /// Error message stream
      std::ostringstream	errmsg;
   
      /** Constructs a supervisory object.
          @param Name name of supervisory task
   	  @memo Default constructor
       ******************************************************************/
      explicit supervisory (const std::string& Name) :
      myname (Name), tpMgr (0), abort (0), pause (0) {
      }
   
      /** Destructs a supervisory object.
   	  @memo Destructor
       ******************************************************************/
      virtual ~supervisory ();
   
      /** A function which returns a new supervisory object of the same
          type. Must be overwritten by descendents.
          @memo New supervisory object.
          @return new supervisory task
       ******************************************************************/
      virtual supervisory* self () const = 0;
   
      /** Returns the name of the supervisory task.
          @memo Name of supervisory task.
          @return name of supervisory task
       ******************************************************************/
      virtual std::string name () const {
         return myname;
      }
   
      /** Initializes the supervisory task. Sets the storage pointer,
          the command notification object, the test point management 
          pointer, the abort and pause pointers, and calls the setup 
          function.
          @memo Initialization method.
          @param Storage diagnostics storage object
          @param Notify command notification object
          @param RTDDMgr real-time data distribution management object
          @param TPMgr test point management object
          @param Abort pointer to abort variable
          @param Pause pointer to pause variable
          @param rtMode Real-time mode if true (old data otherwise)
          @return true if successful
       ******************************************************************/
      virtual bool init (diagStorage& Storage, const cmdnotify& Notify,
                        dataBroker& databroker, testpointMgr& TPMgr, 
                        bool* Abort = 0, bool* Pause = 0, 
                        bool rtMode = true);
   
      /** Sets up the supervisory task. This function must be 
          overwritten by descendents.
          @memo Setup method.
          @return true if successful
       ******************************************************************/
      virtual bool setup () = 0;
   
      /** Runs the supervisory task. This function must be overwritten
          by descendents.
          @memo Run method.
          @return true if successfuls
       ******************************************************************/
      virtual bool run () = 0;
   
   protected:
      /// Mutex to protect supervisory object
      thread::recursivemutex	mux;
      /// Name of supervisory task
      std::string		myname;
      /// Real-time mode
      bool			RTmode;
      /// Pointer to diagnostic storage object
      diagStorage*		storage;
      /// Command notification object
      cmdnotify			notify;
      /// Pointer to data distribution management object
      dataBroker*		dataMgr;
      /// Pointer to test point management object
      testpointMgr*		tpMgr;
      /// Pointer to abort variable
      bool*			abort;
      /// Pointer to pause variable
      bool*			pause;
   };


/** Basic test supervisory
    This object implements a basic diagnostics supervisory task.
    It is an abstract object which implements default settings
    (Def), synchronization settings (Sync) and an environment (Env[]).
   
    @memo Object for implementing a diagnostics supervisory task
    @author Written April 1999 by Daniel Sigg
    @version 0.1
 ************************************************************************/
   class basic_supervisory : 
   public supervisory, protected testenvironment {
   public:
      /// Minimum setup time = 5 sec
      static const double 	setupTime;
   
      /** Constructs a basic_supervisory object.
          @memo Constructor.
       ******************************************************************/
      explicit basic_supervisory (const std::string& Name)
      : supervisory (Name) {
      }
   
      /** A function which reads the defaults settings from the Def
          data object and the synchronization parameters from the 
          Sync data object.
          @memo Setup function.
          @return true if successful
       ******************************************************************/
      virtual bool setup ();
   
   protected:
   
      /// allow aborting test?
      bool			allowCancel;
      /// is it allowed to apply a stimulus?
      bool			noStimulus;
      /// should we do the analysis?
      bool			noAnalysis;
      /// flag describing whether the original time traces are kept
      int			keepTraces;
      /// default setting for the site identifier
      char			siteDefault;
      /// site identifier override
      char			siteForce;
      /// default setting for the ifo identifier
      char			ifoDefault;
      /// ifo identifier override
      char			ifoForce;
   
      /// start time in GPS nsec
      tainsec_t			start;
      /// time to wait before starting the test in sec
      double			wait;
      /// rate of repeating tests in sec
      double			repeatrate;
      /// Slow down delay in sec
      double			slowDown;
      /// channel for trigger start of test
      std::string		waitForStart;
      /// channel for trigger start of measurement step
      std::string		waitAtEachStep;
      /// channel for signal end of measurement step
      std::string		signalEndOfStep;
      /// channel for signal end of test
      std::string		signalEnd;
   
      /** A function which reads the enviroment from the storage object
          into the environment list (uses the Env[] variable sets).
          @memo Read environment.
          @return true if successful
       ******************************************************************/
      virtual bool readEnvironment ();
   
      /** Set the measurement time in the diagnostics storage object.
          @memo Set the measurement time.
          @return true if successful
       ******************************************************************/
      virtual bool setMeasurementTime (tainsec_t mtime);
   };

  //@}
}
#endif // _GDS_SUPERVISORY_H
