
#include <time.h>
#include <iostream> // JCB
#include "diagtest.hh"
#include "diagdatum.hh"

static const int my_debug = 0 ;

namespace diag {
   using namespace std;
   using namespace thread;

int diagtest::instance_count = 0 ; // JCB

   double diagtest::measurementTime (double duration, double cycles, 
                     double f, bool roundup)
   {
      double		mTime;
   
      if (my_debug) cerr << "diagtest::measurementTime( duration = " << duration << ", cycles = " << cycles << ", f = " << f << ", roundup = " << (roundup ? "TRUE" : "FALSE") << ")" << endl ;

      if (f <= 0) {
         return -1;
      }
      if (duration > 0) {
         if (cycles > 0) {
            mTime = max (duration, cycles / f);
	    if (my_debug) cerr << "  mTime = max (duration, cycles/f)" << endl ;
         }
         else {
            mTime = duration;
	    if (my_debug) cerr << "  mTime = duration (because cycles = 0)" << endl ;
         }
      }
      else if (cycles > 0) {
         mTime = cycles / f;
	 if (my_debug) cerr << "  mTime = cycles/f (because duration <= 0)" << endl ;
      }
      else {
         return -1;
      }
      if (my_debug) cerr << "    mTime = " << mTime << endl ;

      if (roundup) {
         int nc = (int) (mTime * f + 0.999999);
         mTime = (double) nc / f;
	 if (my_debug) cerr << "  roundup is TRUE, mTime = ((int)(mTime * f + 0.999999)) / f" << endl ;
	 if (my_debug) cerr << "    mTime = " << mTime << endl ;
      }
      if (mTime < 1 / f) {
         mTime = 1 / f;
	 if (my_debug) cerr << "  mTime < 1/f, mTime = 1/f" << endl ;
	 if (my_debug) cerr << "    mTime = " << mTime << endl ;
      }
      if (my_debug) cerr << "diagtest::measurementTime() returns mTime of " << mTime << endl ;
      return mTime;
   }


   double  diagtest::adjustForSampling (double duration, double dt)
   {
      double		ns;
      double		retval ;
   
      if (my_debug) cerr << "diagtest::adjustForSampling( duration = " << duration << ", dt = " << dt << ")" << endl ;
      if (dt <= 0) {
         retval = -1;
      }
      else if (fabs (duration) < 1E-6) {
         retval = 0;
      }
      else if (modf (duration / dt, &ns) > 0.000001) {
         retval = dt * (ns + 1);
      }
      else {
         retval = duration;
      }
      if (my_debug) cerr << "diagtest::adjustForSampling() returns " << retval << endl ;
      return retval ;
   }


   tainsec_t  diagtest::fineAdjustForSampling (tainsec_t t, double dt)
   {
      int	rate = (int) (1.0/dt + 0.5);
      tainsec_t retval ;
   
      if (my_debug) cerr << "diagtest::fineAdjustForSampling( t = " << t << ", dt = " << dt << ")" << endl ;
      if (rate <= 1) {
         tainsec_t 	step = (tainsec_t) (dt + 0.5);
         tainsec_t	time = (t + _ONESEC / 2) / _ONESEC;
         retval = step * (time / step) * _ONESEC;
      }
      else {
         tainsec_t	time = t / _ONESEC;
         tainsec_t	rest = t % _ONESEC;
         tainsec_t	step = _ONESEC / rate;
         retval = (time * _ONESEC +
                _ONESEC * ((rest + step / 2) / step) / rate);
      }
      if (my_debug) cerr << "diagtest::fineAdjustForSampling() returns " << retval << endl ;
      return retval ;
   }



   diagtest::~diagtest () 
   {
      setEnvironmentExcitationManager (0);
      { // JCB
//	 char str[128] ;
//	 sprintf(str, "diagtest instance %d destroyed\n", myinstance) ;
//	 cerr << str ;
      }
   }


   bool diagtest::init (diagStorage& Storage, 
                     const cmdnotify& Notify,
                     dataBroker& databroker,
                     excitationManager& EnvExc,
                     excitationManager& TestExc,
                     int KeepTraces, bool rtMode)
   {
      semlock 		lockit (mux);
   
      storage = &Storage;
      notify = Notify;
      dataMgr = &databroker;
      setEnvironmentExcitationManager (&EnvExc);
      testExc = &TestExc;
      keepTraces = KeepTraces;
      RTmode = rtMode;
   
      return true;
   }


}
