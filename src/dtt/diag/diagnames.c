/* version $Id: diagnames.c 6312 2010-09-17 17:09:04Z james.batch@LIGO.ORG $ */
#include <ctype.h>

   int compareTestNames (const char* s1, const char* s2)
   {      
      int 	d;
      int	i = 0;
      int	j = 0;
   
      if ((s1 == 0) || (s2 == 0)) {
         return (s1 ==  s2) ? 0 : ((s1 != 0) ? s1[0] : -s2[0]);
      }
   
      while (s1[i] || s2[j]) {
         /* skip blanks */
         if ((s1[i] == ' ') || (s1[i] == '\t')) {
            i++;
            continue;
         }
         if ((s2[j] == ' ') || (s2[j] == '\t')) {
            j++;
            continue;
         }
      	 /* compare with ignoring case */
         if ((d = tolower (s1[i]) - tolower (s2[j])))
            return d;
         i++;
         j++;
      }
      return 0;
   }
