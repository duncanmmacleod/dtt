/* version $Id: sweptsine.hh 7690 2016-08-17 00:07:17Z james.batch@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: sweptsine.h						*/
/*                                                         		*/
/* Module Description: Diagnostics test of a swept sine 		*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 28Nov98  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: sweptsine.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C++ 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			OK			*/
/*			  POSIX			OK			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_SWEPTSINE_H
#define _GDS_SWEPTSINE_H

/* Header File List: */
#include <string>
#include <complex>
#include <memory>
#include <vector>
#include "testsync.hh"
#include "stdtest.hh"
#include "tconv.h"


namespace diag {


/** Swept sine
    This object implements the swept sine test.
   
    @memo Object for implementing a swept sine test
    @author Written June 1999 by Daniel Sigg
    @version 0.1
 ************************************************************************/
   class sweptsine : public stdtest {   
   public:
   
      /** Constructs a test object for measuring the swept sine response.
          @memo Default constructor.
          @return void
       ******************************************************************/
      sweptsine ();
   
      /** Destructs a test object for measuring the swept sine response.
          @memo Destructor.
          @return void
       ******************************************************************/
      virtual ~sweptsine ();
   
      /** Returns a new diagnostics test object describing a swept sine
          measurement.
          @memo Diagnostics test object creation function.
          @return new diagnotsics test object
       ******************************************************************/
      virtual diagtest* self () const;
   
      /** Sets up the test. This function will activate the excitation
   	  and start the data flow from the rtdd interface - after 
          calculating measurement times and setting up the 
          excitations signals and the measurement partitions.
          This function also returns the first synchronization point.
          @memo Setup method.
          @param errmsg error message stream
          @param starttime start time of test
          @param sync synchronization point
          @return true if successful
       ******************************************************************/
      virtual bool setup (std::ostringstream& errmsg, 
                        tainsec_t starttime,
                        syncpointer& sync);
   
      /** End of test. This function cleans up temporary storage.
          @memo Cleanup method.
          @param errmsg error message stream
          @return true if successful
       ******************************************************************/
      virtual bool end (std::ostringstream& errmsg);
   
   public:
      /// Sweep point type
      class sweeppoint {
      public:
         /** Constructs a sweep point.
             @memo Default constructor.
             @param Freq frequency
             @param Ampl amplitude
             @param Phas phase at start of measurement
             @return void
          ***************************************************************/
         explicit sweeppoint (double Freq = 0.0, double Ampl= 0.0, 
                           double Phase = 0.0) 
         : freq (Freq), ampl (Ampl), phase (Phase) {
         }
      
      	 /// frequency of sine at sweep point
         double freq;
      	 /// amplitude of sine at sweep point
         double ampl;
      	 /// phase of sine at start of measurement (t0 + tp)
         double phase;
      };
      /// sweep point array type
      typedef std::vector<sweeppoint> sweeppoints;

      friend bool operator<(const sweeppoint &a, const sweeppoint &b)
      {
        return a.freq < b.freq;
      }
   
   protected:
      /// temporary storage for intermediates results
      class sstmpresult {
      public:
      
         /** Constructs a temporary object to store swept sine detection
             coefficients.
             @memo Default constructor.
             @param Name name of channel
             @param Averages number of averages (coefficients)
             @return void
          ***************************************************************/
         explicit sstmpresult (const std::string& Name = "", 
                           int Averages = 0);
      
         /** Destructs the temporary object.
             @memo Destructor.
             @return void
          ***************************************************************/
         ~sstmpresult();
      
         /** Constructs a temporary object from an other one.
             @memo Copy constructor.
             @param tmp temporary object
             @return void
          ***************************************************************/
         sstmpresult (const sstmpresult& tmp);
      
         /** Copies a temporary object from an other one. Moves the 
             pointers rather than copy. It will set the pointers in
             the original object to zero.
             @memo Copy operator.
             @param tmp temporary object
             @return reference to object
          ***************************************************************/
         sstmpresult&  operator= (const sstmpresult& tmp);
      
         /** Allocates new storage. Deletes the old arrays first.
             @memo Allocate memory.
             @param Averages number of averages (coefficients)
             @return true if successful
          ***************************************************************/
         bool allocate (int Averages = 0);
      
         /** Returns true if object memory is allocated.
             @memo Valid method.
             @return true if memory is allocated
          ***************************************************************/
         bool valid () const;
      
         /// number of averages (coefficients) stored
         int		averages;
      	 /// channel name
         std::string	name;
      	 /** sine detection coeffcients. coeff[0] - frequency,
      	     coeff[1] - averaged sine amplitude, coeff[2] ... 
      	     coeff[1+pAverages] - individual sine amplitudes. */
         std::complex<double>*	coeff;
      };
   
   
      /// measurement time
      double		measTime[2];
      /// settling time
      double		settlingTime;
      /// harmonic order
      int		harmonicOrder;
      /// window type
      int		window;
      /// fft result?
      bool		fftResult;
      /// sweep type
      int		sweepType;
      /// sweep direction
      int		sweepDir;
      /// start frequency
      double		fStart;
      /// stop frequency
      double		fStop;
      /// Ramp down time
      double		rampDown ;
      /// Ramp up time
      double		rampUp ;
      /// number of points per sweep
      int		nSweep;
      /// number of averages per point
      int		pAverages;
      /// number of A channels
      int		AChannels;
      /// frequency points used by sweep
      sweeppoints	fPoints;
      /// frequency points read from file
      sweeppoints	fPointsFile;
   
      /// smallest excitation frequency
      double		fMin;
      /// highest excitation frequency
      double		fMax;
      /// highest frequency of interest
      double		fMaxMeas;
      /// lowest sampling frequency
      double		fMinSample;
      /// highest sampling frequency
      double		fMaxSample;
      /// total number of A channels (including stimuli)
      int		numA;
      /// measuerment time in sec
      double		mTime;
      /// time to add to mTime to set it to the next time grid
      double 		mTimeAdd;
      /// precursor time in sec
      double		pTime;
      /// settling time in sec
      double		sTime;
      /// ramp time in sec
      double		rTime;
      /// number of skipped measurement steps
      int		skipMeas;
   
      /// temporary storage for results
      sstmpresult*	sstmps;
      /// temporary storage length
      int		sstmpsSize;
   
      /** Read parameters. This function reads the parameters from the 
          storage object.
          @memo Read parameter method.
          @param errmsg error message stream
          @return true if successful
       ******************************************************************/
      virtual bool readParam (std::ostringstream& errmsg);
   
      /** Calcluate measurement time. This function clalculates start 
   	  time and duration of each measuremenmt period.
          @memo Calculate measurement time method.
          @param errmsg error message stream
          @param t0 start time
          @return true if successful
       ******************************************************************/
      virtual bool calcTimes (std::ostringstream& errmsg,
                        tainsec_t& t0);
   
      /** Calcluate a new measurement point. This function calculates 
          a new measurement interval, synchronization point and
          measurement partitions.
          @memo Calculate measurement point method.
          @param t0 start time (t = 0)
          @param t1 earliest time measurement can start
          @param i measurement point index
          @param measPoint measurement index
          @return true if successful
       ******************************************************************/
      virtual bool newMeasPoint (int i, int measPoint = 0);
   
      /** Calculates measurement parameters. This function determines 
          excitation signals, partitions for the rtdd and synchronization 
          points.
          @memo Add measurements method.
          @param errmsg error message stream
          @param t0 start time
          @param measPoint measurement point number
          @return true if successful
       ******************************************************************/
      virtual bool calcMeasurements (std::ostringstream& errmsg,
                        tainsec_t t0 = 0, int measPoint = 0);
   
      /** Analysis routine which performs the analysis. This function 
          must be overwritten by descendents.
          @memo Analysis method.
          @param id callback argument describing the sync event
          @param measnum measurement number
          @param notify if true upon return sends a notfication message
          @return true if successful
       ******************************************************************/
      virtual bool analyze (const callbackarg& id, int measnum, 
                        bool& notify);
   
      /** Calculates sine amplitude and phase coefficients.
          @memo Sine detection method
          @param resultnum number of result record
          @param measnum measurement number
          @param chnname channel name (including array indices)
          @param stim true if stimulus readback channel
          @param id callback argument describing the sync event
          @return true if successful
       ******************************************************************/
      bool sinedet (int resultnum, int measnum, std::string chnname, 
                   bool stim, const callbackarg& id); 
   
      /** Calculates the transfer function and the coherence.
          @memo Transfer function method
          @param resultnum number of result record
          @param measnum measurement number
          @param chnname channel name (including array indices)
          @param stim true if stimulus readback channel
          @param id callback argument describing the sync event
          @return true if successful
       ******************************************************************/
      bool transfn (int resultnum, int measnum, std::string chnname, 
                   bool stim, const callbackarg& id); 

      /** Callback for aborting the test. This function is called
          when the user aborts the test. It stops the measurement.
          This function can be overwritten by descendents.
          @memo Sync abort method.
          @param id callback argument describing the sync event
          @return true if successful
       ******************************************************************/
      virtual bool syncAbort (const callbackarg& id) ;
   
      /** Callback for pausing the test. This function is called
   	  when the user pauses the test.
          @memo Sync pause method.
          @param id callback argument describing the sync event
          @param sync next synchronization point, or 0 if continue
          @return true if successful
       ******************************************************************/
      virtual bool syncPause (const callbackarg& id, syncpointer& sync);
   
      /** Callback for resuming the test. This function is called
   	  when the user resumes the test. 
          @memo Sync abort method.
          @param id callback argument describing the sync event
          @param sync next synchronization point, or 0 if continue
          @return true if successful
       ******************************************************************/
      virtual bool syncResume (const callbackarg& id, syncpointer& sync);
   };

}
#endif // _GDS_SWEPTSINE_H
