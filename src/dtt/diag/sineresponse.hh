/* version $Id: sineresponse.hh 7690 2016-08-17 00:07:17Z james.batch@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: sineresponse.h						*/
/*                                                         		*/
/* Module Description: Diagnostics test of a sine response		*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 28Nov98  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: sineresponse.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C++ 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			OK			*/
/*			  POSIX			OK			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_SINERESPONSE_H
#define _GDS_SINERESPONSE_H

/* Header File List: */
#include <string>
#include <vector>
#include <complex>
#include "stdtest.hh"
#include "tconv.h"

namespace diag {


/** Sine Response
    This object implements the sine response test.
   
    @memo Object for implementing a sine response test
    @author Written November 1998 by Daniel Sigg
    @see Diagnostics test manual for used algorithms
    @version 0.1
 ************************************************************************/
   class sineresponse : public stdtest {  
   public:
   
      /** Constructs a test object for measuring the sine response.
          @memo Default constructor.
          @return void
       ******************************************************************/
      sineresponse ();
   
      /** Returns a new diagnostics test object describing a sine
          response measurement.
          @memo Diagnostics test object creation function.
          @return new diagnotsics test object
       ******************************************************************/
      virtual diagtest* self () const;
   
      /** End of test. This function cleans up temporary storage.
          @memo Cleanup method.
          @param errmsg error message stream
          @return true if successful
       ******************************************************************/
      virtual bool end (std::ostringstream& errmsg);
   
   protected:
   
      /// temporary storage for intermediate results
      class tmpresult {
      public:
      
         /// type desribin a list of channel names
         typedef std::vector<std::string> chnnames;
      
         /** Constructs a temporary object to store  sine detection
             coefficients.
             @memo Default constructor.
             @param T0 start time
             @param SizeA number of A channels
             @param SizeB number of B channels
             @param SizeF number of frequencies
             @return void
          ***************************************************************/
         explicit tmpresult (tainsec_t T0 = 0,
                           int SizeA = 0, int SizeB = 0, int SizeF = 0); 
      
         /** Destructs the temporary object.
             @memo Destructor.
             @return void
          ***************************************************************/
         ~tmpresult();
      
         /** Constructs a temporary object from another one.
             @memo Copy constructor.
             @param tmp temporary object
             @return void
          ***************************************************************/
         tmpresult (const tmpresult& tmp);
      
         /** Copies a temporary object from another one. Moves the 
             pointers rather than copy. It will set the pointers in
             the original object to zero.
             @memo Copy operator.
             @param tmp temporary object
             @return reference to object
          ***************************************************************/
         tmpresult&  operator= (const tmpresult& tmp);
      
         /** Allocates new storage. Deletes the old arrays first.
             @memo Allocate memory.
             @param SizeA number of A channels
             @param SizeB number of B channels
             @param SizeF number of frequencies
             @return true if successful
          ***************************************************************/
         bool allocate (int SizeA = 0, int SizeB = 0, int SizeF = 0);
      
         /** Returns true if object memory is allocated.
             @memo Valid method.
             @return true if memory is allocated
          ***************************************************************/
         bool valid () const;
      
         /// number of A channels
         int		sizeA;
         /// number of B channels
         int		sizeB;
         /// number of frequencies
         int		sizeF;
         /// start time: t = 0
         tainsec_t	t0;
         /// frequencies
         double*	f;
         /// coefficents table: (sizeA + sizeB) * sizeA
         std::complex<float>*	coeff;
         /// crosscorrelation
         std::complex<float>*	cross;
         /// power coefficients
         float*		pwr;
         /// list of channel names
         chnnames 	names;
      };
   
      /// Ramp down time
      double            rampDown ;
      /// Ramp up time
      double            rampUp ;
      /// measurement time
      double		measTime[2];
      /// settling time
      double		settlingTime;
      /// harmonic order
      int		harmonicOrder;
      /// window type
      int		window;
      /// fft result?
      bool		fftResult;
   
      /// smallest excitation frequency
      double		fMin;
      /// highest excitation frequency
      double		fMax;
      /// highest frequency of interest
      double		fMaxMeas;
      /// lowest sampling frequency
      double		fMinSample;
      /// highest sampling frequency
      double		fMaxSample;
      /// measuerment time in sec
      double		mTime;
      /// time to add to mTime to set it to the next time grid
      double 		mTimeAdd;
      /// precursor time in sec
      double		pTime;
      /// settling time in sec
      double		sTime;
      /// ramp time in sec
      double		rTime;
      /// number of skipped measurement steps
      int		skipMeas;
   
      /// temporary storage
      tmpresult		tmp;
   
      /** Read parameters. This function reads the parameters from the 
          storage object.
          @memo Read parameter method.
          @param errmsg error message stream
          @return true if successful
       ******************************************************************/
      virtual bool readParam (std::ostringstream& errmsg);
   
      /** Calcluate measurement time. This function calculates start 
   	  time and duration of each measuremenmt period.
          @memo Calculate measurement time method.
          @param errmsg error message stream
          @param t0 start time
          @return true if successful
       ******************************************************************/
      virtual bool calcTimes (std::ostringstream& errmsg,
                        tainsec_t& t0);
   
      /** Calcluate a new measurement point. This function calculates 
          a new measurement interval, synchronization point and
          measurement partitions.
          @memo Calculate measurement point method.
          @param t0 start time (t = 0)
          @param t1 earliest time measurement can start
          @param i measurement point index
          @param measPoint measurement index
          @return true if successful
       ******************************************************************/
      virtual bool newMeasPoint (int i, int measPoint = 0);
   
      /** Calculates measurement parameters. This function determines 
          excitation signals, partitions for the rtdd and synchronization 
          points.
          @memo Add measurements method.
          @param errmsg error message stream
          @param t0 start time
          @param measPoint measurement point number
          @return true if successful
       ******************************************************************/
      virtual bool calcMeasurements (std::ostringstream& errmsg,
                        tainsec_t t0 = 0, int measPoint = 0);
   
      /** Analysis routine which performs the analysis. This function 
          must be overwritten by descendents.
          @memo Analysis method.
          @param id callback argument describing the sync event
          @param measnum measurement number
          @param notify if true upon return sends a notfication message
          @return true if successful
       ******************************************************************/
      virtual bool analyze (const callbackarg& id, int measnum, 
                        bool& notify);
   
      /** Calculates sine amplitude and phase coefficients.
          @memo Sine detection method
          @param resultnum number of result record
          @param measnum measurement number
          @param chnname channel name (including array indices)
          @param stim true if stimulus readback channel
          @param id callback argument describing the sync event
          @return true if successful
       ******************************************************************/
      bool sinedet (int resultnum, int measnum, std::string chnname, 
                   bool stim, const callbackarg& id); 
   };

}
#endif // _GDS_SINERESPONSE_H
