/* version $Id: stdsuper.hh 8024 2018-04-18 18:04:26Z ed.maros@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: stdsuper.h						*/
/*                                                         		*/
/* Module Description: standard diagnostics supervisory task		*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 12Apr99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: testiter.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C++ 5.0				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			OK			*/
/*			  POSIX			OK			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS NW17-161				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_STDSUPER_H
#define _GDS_STDSUPER_H

/* Header File List: */
#include <memory>
#include <string>
#include "supervisory.hh"
#include "gdsstring.h"
#include "excitation.hh"
#include "testiter.hh"
#include "diagtest.hh"
#include "testsync.hh"
#include "diagnames.h"

namespace diag {


/** @name Standard Supervisory Task
   
    @memo Objects representing the standrad supervisory task
    @author Written April 1999 by Daniel Sigg
    @version 0.1
 ************************************************************************/

//@{

/** Standard supervisory
    This object implements a standard supervisory task.
   
    @memo Standard supervisory task
    @author Written April 1999 by Daniel Sigg
    @version 0.1
 ************************************************************************/
   class standardsupervisory : public basic_supervisory {   
   public:
   
      /** Constructs a standard supervisory object.
   	  @memo Default constructor
       ******************************************************************/
      explicit standardsupervisory () : 
      basic_supervisory (standardSupervisoryName) {
      }
   
      /** Destructs a standard supervisory object.
   	  @memo Destructor
       ******************************************************************/
      virtual ~standardsupervisory ();
   
      /** Sets up the supervisory task.
          @memo Setup method.
          @return true if successful
       ******************************************************************/
      virtual bool setup ();
   
      /** Runs the standard supervisory task.
          @memo Run method.
          @return true if successfuls
       ******************************************************************/
      virtual bool run ();
   
      /** A function which returns a new standard supervisory object.
          @memo New standard supervisory object.
          @return name of diagnotsics test class
       ******************************************************************/
      virtual supervisory* self () const;
   
   protected:
   
      /// synchronization event
      enum syncevent {
      /// synchronization point expired normally
      normal = 0,
      /// aborted by user
      aborted = 1,
      /// paused by user
      paused = 2,
      /// resumed by user
      resumed = 3,
      /// timed-out
      timeout = 4,
      /// disconnected from data source
      disconnected = 5
      };
      typedef enum syncevent syncevent;
      /// excitation manager for the environment
      excitationManager		envExc;
      /// excitation manager for the test iterator
      excitationManager		iterExc;
      /// excitation manager for the test environment
      excitationManager		testEnvExc;
      /// excitation manager for the test signals
      excitationManager		testExc;
      /// test iterator
      std::unique_ptr<testiterator> testiter;
      /// test
      std::unique_ptr<diagtest>	test;
   
      /** Runs the standard supervisory task in real time mode.
          @memo Run real-time method.
          @return true if successfuls
       ******************************************************************/
      virtual bool runRT ();
   
      /** Runs the standard supervisory task off-line.
          @memo Run off-line method.
          @return true if successfuls
       ******************************************************************/
      virtual bool runOL ();
   
      /** Waits the specified time. Periodically checks the abort
          condition. Returns false if aborted; true otherwise.
          @memo Wait method.
          @param duration time to wait
          @return false if aborted
       ******************************************************************/
      virtual bool timeWait (double duration) const;
   
      /** Waits for a trigger signal. Periodically checks an EPICS 
          channel for a trigger. Returns false if aborted; true otherwise.
          @memo Wait trigger method.
          @param channel name of trigger channel 
          @return false if aborted
       ******************************************************************/
      virtual bool triggerWait (const std::string& channel) const;
   
      /** Waits for the specified synchronization point. Periodically 
          checks the abort condition. Returns false if aborted; true 
          otherwise.
          @memo Wait sync method.
   	  @param sync synchronization point
          @param testPaused true if test is already paused 
          @return false if aborted
       ******************************************************************/
      virtual syncevent syncWait (const syncpoint& sync,
                        bool testPaused) const;
   
      /** Reads past data from the nds and waits for the specified 
          synchronization point. Periodically checks the abort condition. 
          Returns false if aborted; true otherwise.
          @memo Read sync method.
   	  @param sync synchronization point
   	  @param last Measurement stop time of last read
   	  @param lastlast Measurement stop time of read before last
          @param testPaused true if test is already paused 
          @return false if aborted
       ******************************************************************/
      virtual syncevent syncRead (const syncpoint& sync,
                        taisec_t& last, taisec_t& lastlast, bool testPaused) const;
   
      /** Waits for test to resume. Periodically checks the abort
          condition. Returns false if aborted; true otherwise.
          @memo Wait resume method.
          @return false if aborted
       ******************************************************************/
      virtual bool resumeWait () const;
   
      /** Sends a trigger signal. Sets and resets an EPICS to indicate a 
          trigger.
          @memo Wait trigger method.
          @return true if successful
       ******************************************************************/
      virtual bool signalSend (const std::string& channel) const;
   };

//@}
}
#endif // _GDS_STDSUPER_H
