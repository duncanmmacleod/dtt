
#include "udn.hh"
#include <string.h>
#include <strings.h>
#include <algorithm>

namespace dfm {
   using namespace std;
   using namespace fantom;



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// UDN class                                                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool UDN::operator< (const UDN& udn) const
   {
      if (!fValid && udn.fValid) {
         return false;
      }
      else if (fValid && !udn.fValid) {
         return true;
      }
      else {
         return strcasecmp (fName.c_str(), udn.fName.c_str()) < 0;
      }
   }

//______________________________________________________________________________
   bool UDN::operator== (const UDN& udn) const
   {
      if ((fValid != false) ^ (udn.fValid != false)) {
         return false;
      }
      else {
         return strcasecmp (fName.c_str(), udn.fName.c_str()) == 0;
      }
   }

//______________________________________________________________________________
   void UDN::check() 
   {
      if (fName.empty()) {
         fValid = false;
      }
      else {
         fValid = true; 
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// UDN Info class                                                       //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   chniter UDNInfo::findChn (const char* chn)
   {
      if (!chn) {
         return fChannels.end();
      }
      channelentry c (chn);
      chniter f = lower_bound (fChannels.begin(), fChannels.end(), c);
      if (f == fChannels.end()) {
         return f;
      }
      if (c == *f) {
         return f;
      }
      else {
         return fChannels.end();
      }
   }

//______________________________________________________________________________
   std::pair <chniter, bool> UDNInfo::insertChn (
                     const char* chn, int rate)
   {
      if (!chn) {
         return make_pair (fChannels.end(), false);
      }
      else {
         channelentry c (chn, rate);
         chniter f = lower_bound (fChannels.begin(), fChannels.end(), c);
         if ((f != fChannels.end()) && (c == *f)) {
            f->SetRate (rate);
            return make_pair (f, true);
         }
         else {
            chniter ins = fChannels.insert (f, c);
            return make_pair (ins, true);
         }
      }
   }

//______________________________________________________________________________
   void UDNInfo::eraseChn (const char* chn)
   {
      if (chn) {
         channelentry c (chn);
         chniter f = lower_bound (fChannels.begin(), fChannels.end(), c);
         if ((f != fChannels.end()) && (c == *f)) {
            fChannels.erase (f);
         }
      }
   }

//______________________________________________________________________________
   void UDNInfo::clearChn()
   {
      fChannels.clear();
   }

//______________________________________________________________________________
   UDNInfo::dsegiter UDNInfo::findDSeg (const Time& start)
   {
      return fDIntervals.find (start);
   }

//______________________________________________________________________________
   void UDNInfo::insertDSeg (const Time& start, const Interval& duration)
   {
      dsegiter f = findDSeg (start);
      if (f != endDSeg()) {
         f->second = duration;
      }
      else {
         dataseglist::value_type val (start, duration);
         fDIntervals.insert (val);
      }
   }

//______________________________________________________________________________
   void UDNInfo::eraseDSeg (const Time& start)
   {
      fDIntervals.erase (start);
   }

//______________________________________________________________________________
   void UDNInfo::clearDSeg()
   {
      fDIntervals.clear();
   }


}
