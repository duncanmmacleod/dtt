/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: dfmapi							*/
/*                                                         		*/
/* Module Description: DFM low level interface   			*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 9Jan01   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: dfmapi.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_FDFMAPI_H
#define _LIGO_FDFMAPI_H


#include "Time.hh"
#include "Interval.hh"
#include "dfmtype.hh"
#include "udn.hh"
#include <string>


namespace fantom {
   class framemux;
   class channelquerylist;
}

namespace dfm {


/** @name DFM low level API 
    This header defines support methods for accessung the
    data flow manager.
   
    @memo DFM low level API
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


/** Basic API to the Data Flow Manager
    
    @memo Basic DFM class.
 ************************************************************************/
   class dfmapi {
   public:
      /// Staging types
      enum stagingtype {
      /// data only (no staging)
      nostaging = 0,
      /// stage data automatically
      stagedata = 1,
      /// Staging only (no data)
      stagingonly = 2
      };
   
      /// Default constructor
      dfmapi () : fError (false), fOpen (false), fChnPreselFull (false) {
      }
      /// Destructor
      virtual ~dfmapi () {
      }
   
      /// Test if error
      bool operator! () const {
         return fError; }
      /// Test if open
      bool isOpen() const {
         return fOpen; }
   
      /// Test if multiple UDNs are supported
      virtual bool supportMultiUDN() const {
         return false; }
      /// Test if staging is supported
      virtual bool supportStaging() const {
         return false; }
      /// Test if input is supported
      virtual bool supportInput() const {
         return true; }
      /// Test if output is supported
      virtual bool supportOutput() const {
         return false; }
   
      /// Open connection
      virtual bool open (const std::string& addr, bool read = true) = 0;
      /// Close connection
      virtual void close() = 0;
      /// Checks username/password; uses last/default login if no arguments
      virtual bool login (const UDN& udn,
                        const char* uname = 0, const char* pword = 0) {
         return true; }
      /// Request a list of avaialble UDNs
      virtual bool requestUDNs (UDNList& udn) = 0;
      /// Request a list of avaialble UDNs (uses cache)
      virtual bool cachedUDNs (const char* server, UDNList& udn,
                        bool forcelookup = false);
      /// Request information asscociated with a UDN
      virtual bool requestUDNInfo (const UDN& udn, UDNInfo& info) = 0;
      /// Request information asscociated with a UDN (uses cache)
      virtual bool cachedUDNInfo (const UDN& udn, UDNInfo& info,
                        bool forcelookup = false);
      /// Creates a data input request (smart_input is added to mux)
      virtual bool requestData (fantom::framemux& mux,
                        const Time& start, const Interval& duration, 
                        const UDNList& udn, 
                        stagingtype staging = stagedata,
                        const Interval& keep = Interval (1800.),
                        fantom::channelquerylist* select = 0);
      /// Creates a data output request (smart_output is added to mux)
      virtual bool sendData (fantom::framemux& mux,
                        const UDN& udn, const char* format,
                        const fantom::channelquerylist* channels = 0);
      /// Abort data request
      virtual void abort() {
      };
   
      /// Clear IO caches
      static void ClearCache();
   
   protected:
      /// True if error
      bool		fError;
      /// Address
      std::string	fAddr;
      /// True if connection is open
      bool		fOpen;
      /** Use full algorithm for channel preselection? This should
          be set to true by a dfm which sends a request to a server
          so that only the required number of channels are requested.
          For a dfm that just looks at existing frame files this is
          not necessary and only costs time.
        */
      bool		fChnPreselFull;
   
      /// Preselect channels
      virtual fantom::channelquerylist preselectChannels (
                        const UDN& udn,
                        const fantom::channelquerylist& insel,
                        const fantom::channelquerylist* outsel);
   };


/** Create a data flow manager API
    
    @memo Create a DFM API.
 ************************************************************************/
   dfmapi* createDFMapi (dataservicetype type);

//@}

}

#endif // _LIGO_FDFMAPI_H



