/* version $Id: mmaptest.cc 6306 2010-09-17 16:54:02Z james.batch@LIGO.ORG $ */

#include <stdio.h>
#include <string.h>
#include <sys/mman.h>

   int main ()
   {
      FILE* 		f1;
      char*		maddr;
      char*		maddr2;
      char*		maddr3;
   
      printf ("step 1:\n");
      f1 = fopen ("dtest.txt", "rb+");
      if (f1 == 0) {
         printf ("open failed\n");
         return 1;
      }
      printf ("step 2:\n");
   
      int mlen = 16 * 1024;
      int offset = 0;
      for (int i = 0; i < 10; i++) {
         maddr = (char*) mmap ((caddr_t) 0, mlen, PROT_READ | PROT_WRITE, 
                              MAP_SHARED, f1->_file, offset);
         printf ("mmap %i = %x\n", i, (int) maddr);
      }
      if ((caddr_t) maddr == MAP_FAILED) {
         printf ("mapping 1 failed\n");
         return 1;
      }
      int mlen2 = 16 * 1024;
      int offset2 = 200;
      maddr2 = (char*) mmap ((caddr_t) maddr, mlen2 + offset2, 
                           PROT_READ | PROT_WRITE, 
                           MAP_SHARED, f1->_file, 0);
      if ((caddr_t) maddr2 == MAP_FAILED) {
         printf ("mapping 2 failed\n");
         return 1;
      }
      printf ("mmap 1 = %x\n", (int) maddr);
      printf ("mmap 2 = %x\n", (int) maddr2);
   
      strcpy (maddr, "This is test 3 of memory mapping of files\n");
      munmap ((caddr_t) maddr, mlen);
      strcpy (maddr2+offset2, "This is test 4 of memory mapping of files\n");
   
      fclose (f1);
      munmap ((caddr_t) maddr2, mlen2);
   
      printf ("successful\n");
      return 0;
   }
