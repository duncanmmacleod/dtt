/* version $Id: tpdaqd.c 6306 2010-09-17 16:54:02Z james.batch@LIGO.ORG $ */

#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#ifndef _TP_DAQD
#define _TP_DAQD
#endif
#include "testpoint.h"

#define TICK		3;
   const char* const channel = "H2:LSC-REF2_Q";

   int main (int argc, char* argv[]) 
   {
      char* 	reply;
   
      /* channel name is */
      printf ("channel = %s\n", channel);
   
      /* set test point manager address for node 0 */
      if (tpSetHostAddress (0, "10.1.0.99", 0, 0) != 0) {
         printf ("unable to initialize test point interface\n");
         return 1;
      }
      /* set test point manager address for node 1; LHO only */
      if (tpSetHostAddress (1, "10.1.0.99", 0, 0) != 0) {
         printf ("unable to initialize test point interface\n");
         return 1;
      }
   
      /* select a test point by name */
      if (tpRequestName (channel, -1, NULL, NULL) != 0) {
         printf ("unable to select test point\n");
         return 1;
      }
   
      /* wait a little bit */
      {
         struct timespec wait = {3, 0};
         nanosleep (&wait, NULL);
      }
   
      /* check test points setting */
      reply = tpCommand ("show *");
      if (reply != 0) {
         printf ("%s\n", reply);
         free (reply);
      }
      else {
         printf ("unable to query test point manager\n");
      }
   
      /* clear test point */
      if (tpClearName (channel) != 0) {
         printf ("unable to clear test point\n");
         return 1;
      }
   
      /* wait a little bit */
      {
         struct timespec wait = {3, 0};
         nanosleep (&wait, NULL);
      }
   
      /* check test points setting */
      reply = tpCommand ("show *");
      if (reply != 0) {
         printf ("%s\n", reply);
         free (reply);
      }
      else {
         printf ("unable to query test point manager\n");
      }
   
      return 0;
   }
