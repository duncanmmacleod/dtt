/* version $Id: schedtest.c 6344 2010-11-10 06:19:42Z john.zweizig@LIGO.ORG $ */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "gdsutil.h"
#include "gdsheartbeat.h"
#include "gdssched.h"
#include "gdssched_client.h"

   static int 	cond = 0;

   static int yell (schedulertask_t* info,
                   taisec_t time, int epoch, void* arg)
   {
      if (info->xdr_arg == NULL) {
         printf ("%2li: time = %li, epoch = %i\n", (long) arg, time, epoch);
      }
      else {
         printf ("%2li: time = %li, epoch = %i\n", *((long*) arg), time, epoch);
      }
      return cond;
   }

   static void yellAgain (taisec_t time, int epoch, void* arg)
   {
      printf ("this is the end of %2li (time = %li, epoch = %i)\n", 
             (long) arg, time, epoch);
   }


/* single shot no wait */
   static schedulertask_t* test1 (schedulertask_t* entry)
   {
      SET_TASKINFO_ZERO (entry);
      entry->flag = 0;
      entry->arg = (void*) 1;
      entry->func = yell;
      entry->freeResources = yellAgain;
      return entry;
   }

/* single shot, absolute start time 5s in future, no sync */
   schedulertask_t* test2 (schedulertask_t* entry)
   {
      test1 (entry);
      entry->flag = SCHED_WAIT;
      entry->synctype = SCHED_SYNC_NEXT;
      entry->waittype = SCHED_WAIT_STARTTIME;
      entry->waitval = TAInow() + 5000000000LL; 
      printf ("aboslute time at %f\n", entry->waitval / 1E9);
      entry->arg = (void*) 2;
      return entry;
   }

/* single shot, absolute start time 5s in future, sync 5th epoch */
   schedulertask_t* test3 (schedulertask_t* entry)
   {
      test2 (entry);
      entry->synctype = SCHED_SYNC_EPOCH;
      entry->syncval = 5;
      entry->arg = (void*) 3;
      return entry;
   }

/* single shot, absolute start time 5s in future, sync inv. epoch 2 */
   schedulertask_t* test4 (schedulertask_t* entry)
   {
      static int	tagnum = 4;
      char		tag[100];
   
      sprintf (tag, "TEST%i", tagnum);
      tagnum++;
      test2 (entry);
      entry->flag = SCHED_WAIT | SCHED_TIMETAG;
      entry->tagtype = SCHED_TAG_START;
      strcpy (entry->timetag, tag);
      entry->synctype = SCHED_SYNC_IEPOCH;
      entry->syncval = 2;
      entry->arg = (void*) 4;
      return entry;
   }

/* single shot, wait immediate, sync epoch 7 */
   schedulertask_t* test5 (schedulertask_t* entry)
   {
      test1 (entry);
      entry->flag = SCHED_WAIT;
      entry->waittype = SCHED_WAIT_IMMEDIATE;
      entry->synctype = SCHED_SYNC_EPOCH;
      entry->syncval = 7;
      entry->arg = (void*) 5;
      return entry;
   }

/* single shot, wait delay 3.3s, no sync */
   schedulertask_t* test6 (schedulertask_t* entry)
   {
      test5 (entry);
      entry->waittype = SCHED_WAIT_TIMEDELAY;
      entry->waitval = 3300000000LL;
      entry->synctype = SCHED_SYNC_NEXT;
      printf ("wait %f sec\n", entry->waitval / 1E9); 
      entry->arg = (void*) 6;
      return entry;
   }

/* single shot, wait delay 20 epochs, sync with next 1s */
   schedulertask_t* test7 (schedulertask_t* entry)
   {
      test5 (entry);
      entry->waittype = SCHED_WAIT_EPOCHDELAY;
      entry->waitval = 20;
      entry->synctype = SCHED_SYNC_IEPOCH;
      entry->syncval = 1;
      printf ("wait %lli epochs\n", entry->waitval); 
      entry->arg = (void*) 7;
      return entry;
   }


/* single shot, wait for tag test4, wait 3 epochs */
   schedulertask_t* test8 (schedulertask_t* entry)
   {
      test5 (entry);
      entry->waittype = SCHED_WAIT_TAGEPOCHDELAYED;
      strcpy (entry->waittag, "TEST4");
      entry->waitval = 3;
      entry->synctype = SCHED_SYNC_NEXT;
      printf ("wait %lli epochs\n", entry->waitval); 
      entry->arg = (void*) 8;
      return entry;
   }

/* single shot, wait for tag test4, wait 3 seconds, sync 3 */
   schedulertask_t* test9 (schedulertask_t* entry)
   {
      test5 (entry);
      entry->waittype = SCHED_WAIT_TAGDELAYED;
      strcpy (entry->waittag, "test4");
      entry->waitval = 3000000000LL;
      entry->synctype = SCHED_SYNC_EPOCH;
      entry->syncval = 3;
      printf ("wait %f s\n", entry->waitval / 1E9); 
      entry->arg = (void*) 9;
      return entry;
   }

/* single shot, same as test 3, but with a timeout */
   schedulertask_t* test10 (schedulertask_t* entry)
   {
      test3 (entry);
      entry->flag |= SCHED_TIMEOUT;
      entry->timeout = 3000000000LL;
      entry->arg = (void*) 10;
      return entry;
   }

/* repeat every 2nd epoch, no wait */
   schedulertask_t* test11 (schedulertask_t* entry)
   {
      test1 (entry);
      entry->flag = SCHED_REPEAT;
      entry->repeattype = SCHED_REPEAT_INFINITY;
      entry->repeatratetype = SCHED_REPEAT_EPOCH;
      entry->repeatrate = 2;
      entry->repeatsynctype = SCHED_SYNC_NEXT;
      entry->repeatsyncval = 2;
      entry->arg = (void*) 11;
      return entry;
   }

/* repeat every 2nd inverse epoch, sync. epoch 2, no wait */
   schedulertask_t* test12 (schedulertask_t* entry)
   {
      test1 (entry);
      entry->flag = SCHED_REPEAT;
      entry->repeattype = SCHED_REPEAT_INFINITY;
      entry->repeatratetype = SCHED_REPEAT_IEPOCH;
      entry->repeatrate = 2;
      entry->repeatsynctype = SCHED_SYNC_EPOCH;
      entry->repeatsyncval = 2;
      entry->arg = (void*) 12;
      return entry;
   }

/* repeat every 2nd inv. epoch, sync. inv. epoch 1, no wait */
   schedulertask_t* test13 (schedulertask_t* entry)
   {
      test1 (entry);
      entry->flag = SCHED_REPEAT;
      entry->repeattype = SCHED_REPEAT_INFINITY;
      entry->repeatratetype = SCHED_REPEAT_IEPOCH;
      entry->repeatrate = 2;
      entry->repeatsynctype = SCHED_SYNC_IEPOCH;
      entry->repeatsyncval = 1;
      entry->arg = (void*) 13;
      return entry;
   }


/* repeat 3 times, every 4th inv. epoch, sync. inv. epoch 3, 
   wait for time tag and 20 epoch delay, sync. epoch 15 */
   schedulertask_t* test14 (schedulertask_t* entry)
   {
      test1 (entry);
      entry->flag = SCHED_REPEAT | SCHED_WAIT;
      entry->waittype = SCHED_WAIT_TAGEPOCHDELAYED;
      strcpy (entry->waittag, "test4");
      entry->waitval = 20;
      entry->synctype = SCHED_SYNC_EPOCH;
      entry->syncval = 15;
      printf ("wait %lli epochs\n", entry->waitval); 
   
      entry->repeattype = SCHED_REPEAT_N;
      entry->repeatval = 3;
      entry->repeatratetype = SCHED_REPEAT_IEPOCH;
      entry->repeatrate = 2;
      entry->repeatsynctype = SCHED_SYNC_IEPOCH;
      entry->repeatsyncval = 1;
      entry->arg = (void*) 14;
      return entry;
   }


/* repeat every inv. epoch, sync. epoch 14, wait immediate, sync. 14,
   until condition */
   schedulertask_t* test15 (schedulertask_t* entry)
   {
      test1 (entry);
      entry->flag = SCHED_REPEAT | SCHED_WAIT;
      entry->waittype = SCHED_WAIT_IMMEDIATE;
      entry->synctype = SCHED_SYNC_EPOCH;
      entry->syncval = 14;
   
      entry->repeattype = SCHED_REPEAT_COND;
      entry->repeatratetype = SCHED_REPEAT_IEPOCH;
      entry->repeatrate = 1;
      entry->repeatsynctype = SCHED_SYNC_EPOCH;
      entry->repeatsyncval = 14;
      entry->arg = (void*) 15;
      return entry;
   }


/* xdr test */
#include "gdsrsched.h"
   schedulertask_t* test16 (schedulertask_t* entry)
   {
      static schedulertask_r	rec;
      static char		buf[100] = "this is a test";
   
      printf ("size of char buf %i\n", (int) sizeof (buf));
      test1 (entry);
      entry->arg = &rec;
      rec.arg.arg_len = 40;
      rec.arg.arg_val = buf;
      *((long*) entry->arg) = 16;
      entry->xdr_arg = xdr_schedulertask_r;
      entry->arg_sizeof = sizeof (schedulertask_r);
      return entry;
   }

/* schedule task */
   int schedule (scheduler_t* sd, 
                schedulertask_t* (*test) (schedulertask_t*))
   {
      int		status;
      schedulertask_t	entry;
      int		testnum;
   
      if (sd == NULL) {
         printf ("invalid scheduler\n");
         return -1;
      }
      test (&entry);
      entry.flag |= SCHED_ASYNC;
      if ((sd->scheduler_flags & SCHED_LOCAL_BOUND) == SCHED_REMOTE_BOUND) {
         entry.func = (schedfunc_t) (1);
      }
      status = scheduleTask (sd, &entry);
      if (entry.xdr_arg == NULL) {
         testnum = (int) entry.arg;
      }
      else {
         testnum = *((long*) entry.arg);
         free (entry.arg);
         entry.arg = NULL;
      }
      if (status < 0) {
         printf ("scheduling error in test %i: error %i\n", 
                testnum, status);
      }
      else {
         printf ("test %i scheduled\n", testnum);
      }
      return status;
   }


/* main program */
   void
   #ifdef OS_VXWORKS
   schedtest (void)
   #else
   main (int argc, char *argv[])
   #endif
   {
      int	i = 0;
      int	j;
      int	old;
      int	id;
      taisec_t	tai;
      int	epoch;
      /*int	status;*/
      scheduler_t* sd1 = NULL;
      scheduler_t* sd2 = NULL;
      scheduler_t* sd3 = NULL;
      /* scheduler_t* sd4 = NULL;*/
   
      /* schedulertask_t	entry1;*/
      schedulertask_t	entry2;
   
      printf ("time now: %f\n", TAInow ()/1E9);
      printf ("install heartbeat = %i\n", installHeartbeat (NULL));
   
      sd2 = createRemoteScheduler (0);
      if (sd2 == NULL) {
         printf ("Couldn't create remote scheduler\n");
      }
   
      sd1 = createBoundScheduler (sd2, "10.1.0.18", 0x31000000, 1);
      /* sd1 = createScheduler (0, NULL, NULL); */
      if (sd1 == NULL) {
         printf ("Couldn't create scheduler\n");
      }
      sd3 = createBoundScheduler (sd2, NULL, 0, 0);
      if (sd3 == NULL) {
         printf ("Couldn't create scheduler\n");
      }
   
      printf ("start scheduling tasks\n");
      schedule (sd1, test1);
      schedule (sd1, test16);
      schedule (sd1, test2);
      schedule (sd1, test3);
      for (i=0; i < 10; i++) schedule (sd3, test4);
      schedule (sd1, test5);
      schedule (sd1, test6);
      schedule (sd1, test7);
      schedule (sd1, test8);
      schedule (sd1, test9);
      schedule (sd1, test10);
      schedule (sd1, test16);
      schedule (sd1, test14);
   
      schedule (sd1, test4);
      id = schedule (sd1, test15);
   
      while (1) {
         syncWithHeartbeatEx (&tai, &epoch);
         i++;
         if (epoch % NUMBER_OF_EPOCHS == 0) {
            printf ("time = %li\n", tai);
         }
      
      	 /* remove a task */
         if (i == 32) {
            printf ("remove test 15\n");
            removeScheduledTask (sd1, id, SCHED_WAIT);
         }
      	 /* check list of scheduled tasks */
         if (i == 2 * 16 + 5) {
            j = -1;
            printf ("list of all tasks\n");
            do {
               old = j;
               j = getScheduledTask (sd1, old, &entry2);
               if (j != old) {
                  printf ("task %i is still scheduled\n", 
                         (int) entry2.arg);
               }
            } while (old < j);
         }
      
      	 /* wait for finishing */
         if (i % (5 * 16) == 0) {
            if (waitForSchedulerToFinish (sd1, 0) == 0) {
               printf ("scheduler finished\n");
               break;
            }
         }
      
      	 /* set condition true */
         if (i / NUMBER_OF_EPOCHS >= 6) {
            cond = 1;
         }
      }
   
      closeScheduler (sd2, 0);
   
      exit (EXIT_SUCCESS);
   }


