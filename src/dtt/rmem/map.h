/* Version $Id: map.h 6334 2010-10-25 17:42:55Z james.batch@LIGO.ORG $ */
#include "hardware.h"
#if defined(_ADVANCED_LIGO)
#include "map_v3.h"
#elif (RMEM_LAYOUT == 0)
#include "map_v1.h"
#elif (RMEM_LAYOUT == 1)
#include "map_v2.h"
#else
#error Bad reflective memory layout specified
#endif

