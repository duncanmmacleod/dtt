/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGDfmChannels						*/
/*                                                         		*/
/* Module Description: ROOT Dialogbox for DFM channel selection		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 9Jan01   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: dfmdlgroot.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_TLGDFMCHANNELS_H
#define _LIGO_TLGDFMCHANNELS_H

#include "TLGFrame.hh"
#include "dataacc.hh"
#include <TGFrame.h>
#include <TGLabel.h>
#include <TGTextEntry.h>
#include <TGButton.h>


namespace ligogui {
   class TLGNumericControlBox;
   class ChannelEntry;
   class TLGChannelCombobox;
}

namespace dfm {


/** @name Dfm channel selection 
    This header defines a ROOT dialogbox for the DFM channel selection.
   
    @memo Dfm channel selection
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


   const Int_t kMaxChn = 20;
   const Int_t kDfmChnActive = 100;
   const Int_t kDfmChn = 200;
   const Int_t kDfmChnRate = 300;
   const Int_t kDfmChnBack = 400;
   const Int_t kDfmChnForward = 401;
   const Int_t kDfmChnWildcard = 402;
   const Int_t kDfmChnMax = 403;
   const Int_t kDfmChnEq = 404;
   const Int_t kDfmChnMaxEqRate = 405;


/** Channel selection dialog box.
    @memo Channel selection dialog box.
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGDfmChnSelDlg : public ligogui::TLGTransientFrame {
   protected:
      /// Data server with list of available channels
      dataserver*	fDS;
      /// Currently selected entry
      selserverentry*	fEntry;
      /// Currently available channels
      const fantom::channellist& fChnavail;
      /// Currently available channels (for combobox)
      ligogui::ChannelEntry*	fAllChns;
      /// Number of currently available channels (for combobox)
      int		fAllChnNum;
      /// Currently selected channels
      fantom::channellist	fChncur;
      /// Return value
      Bool_t*		fOk;
      /// Channel list offset
      Int_t		fOffset;
   
      /// Layout hinst
      TGLayoutHints*	fL[10];
      /// Group frames: Channel list; wildcard; rate
      TGGroupFrame*	fG[3];
      /// Lines
      TGCompositeFrame*	fLine[(kMaxChn + 1)/2];
      /// frames
      TGCompositeFrame*	fF[4];
      /// Channels active checkbox
      TGButton*		fChnActive[kMaxChn];
      /// Channel label
      TGLabel*		fChnLabel[kMaxChn];
      /// UDN selection
      ligogui::TLGChannelCombobox*	fChn[kMaxChn];
      /// Channel sample rate selection
      ligogui::TLGNumericControlBox*	fRate[kMaxChn];
      /// Channels forward/backward buttons
      TGButton*		fChnFrwdBkwd[2];
      /// Wildcard entry field
      TGTextEntry*	fWildcard;
      /// Maximum rate checkbox
      TGButton*		fMaxRate;
      /// Equal rate checkbox
      TGButton*		fEqRate;
      /// Max/Eq rate numeric entry field
      ligogui::TLGNumericControlBox*	fMaxEqRate;
      /// Ok button
      TGButton*		fOkButton;
      /// Cancel button
      TGButton*		fCancelButton;
      /// File button
      TGButton*		fFileButton;
      /// All button
      TGButton*		fAllButton;
      /// Clear button
      TGButton*		fClearButton;
   
   public:
      /** Constructs a new channel selection dialog box.
          @memo Constructor.
          @param p Parent window
   	  @param main Main window
   	  @param ds Data server
   	  @param entry Selected entry (return)
   	  @param avail List of available channels
   	  @param ret True if successful, False if cancel
       ******************************************************************/
      TLGDfmChnSelDlg (const TGWindow* p, const TGWindow* main,
                      dataserver& ds, selserverentry& entry,
                      const fantom::channellist& avail, Bool_t& ret);
      /// Destructor
      virtual ~TLGDfmChnSelDlg ();
      /// Close window
      virtual void CloseWindow();
      /// Read values
      virtual Bool_t ReadValues();
      /// Set channel list offset
      virtual void SetChnOffset (Int_t ofs = 0);
      /// Read channel names from file
      virtual Bool_t ReadChnFromFile (const char* filename);
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, Long_t);
   };


//@}

}


#endif // _LIGO_TLGDFMCHANNELS_H
