#include <time.h>
#include "goption.hh"
#include <stdlib.h>
#include <iostream>
#include <TROOT.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TGMsgBox.h>


   using namespace std;
   using namespace gdsbase;

   TROOT root ("MAN.robot", "Manual tape change");


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// main								        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
extern "C"
   int main (int argc, char** argv)
   {
      option_string opt (argc, argv, "hd:u:l:f:");
      string device = "-";
      opt.getOpt ('d', device);
      string unload;
      if (opt.getOpt ('u', unload)) {
         unload = string (" remove tape ") + unload;
      }
      string load;
      if (opt.getOpt ('l', load)) {
         load = string (unload.empty() ? "" : " and") + " insert tape " + load;
      }
      string path = "-";
      opt.getOpt ('f', path);
      // help
      if (opt.help() || (unload.empty() && load.empty())) {
         cout << "usage: MAN.robot -d 'dev' -u 'unload' -l 'load' -f 'path'" << endl;
         cout << "this is a helper application for the tape control driver" << endl;
         exit (1);
      }
   
      // start application
      TApplication theApp ("MAN.robot", &argc, argv);
   
      // bring up dialog box
      char buf[1024];
      sprintf (buf,
              "Device: %s - %s\n"
              "Please%s%s.\n",
              device.c_str(), path.c_str(), unload.c_str(), load.c_str());
      int retval;
      new TGMsgBox (gClient->GetRoot(), 0, "Change Tape", 
                   buf, kMBIconExclamation, kMBOk | kMBCancel, &retval);
   
      cout << (int)(retval != kMBOk) << endl;
      exit (retval != kMBOk);
   }
