/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: TLGDfmUdn						*/
/*                                                         		*/
/* Module Description: ROOT Dialogbox for DFM UDN selection		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 9Jan01   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: dfmdlgroot.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_TLFDFMUDN_H
#define _LIGO_TLFDFMUDN_H


#include "TLGFrame.hh"
#include "dataacc.hh"
#include <TGFrame.h>
#include <TGComboBox.h>
#include <TGLabel.h>
#include <TGTextEntry.h>
#include <TGButton.h>

namespace ligogui {
   class TLGNumericControlBox;
}

namespace dfm {


/** @name Dfm UDN selection 
    This header defines a ROOT dialogbox for the DFM UDN selection.
   
    @memo Dfm UDN selection
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


   const Int_t kMaxUDN = 10;
   const Int_t kDfmUDNSel = 200;
   const Int_t kDfmUDNActive = 300;



/** UDN entry dialog box for directories.
    @memo UDN directory entry dialog box.
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGDfmUDNDirDlg : public ligogui::TLGTransientFrame {
   private:
      /// Source selection?
      Bool_t		fSourceSel;
      /// Return UDN
      UDN*		fUDN;
      /// Return value
      Bool_t*		fOk;
   
      /// Layout hinst
      TGLayoutHints*	fL[7];
      /// Frames
      TGCompositeFrame*	fF[7];
      /// Labels
      TGLabel*		fLabel[3];
      /// Name
      TGTextEntry*	fName;
      /// File button
      TGButton*		fFileButton;
      /// Enable button
      TGButton*		fEnableButton;
      /// Start buttons
      TGButton*		fStartButton[2];
      /// Start values
      ligogui::TLGNumericControlBox*	fStart[2];
      /// Stop buttons/Number of files
      TGButton*		fStopButton[2];
      /// Stop/Number of files values
      ligogui::TLGNumericControlBox*	fStop[2];
      /// Ok button
      TGButton*		fOkButton;
      /// Cancel button
      TGButton*		fCancelButton;
   
   public:
      /** Constructs a UDN directory entry dialog box.
          @memo Constructor.
          @param p Parent window
   	  @param main Main window
   	  @param UDN Universal data set name (in/out)
   	  @param sourcesel input UDN (true) or output UDN
   	  @param ret True if successful, False if cancel
       ******************************************************************/
      TLGDfmUDNDirDlg (const TGWindow *p, const TGWindow *main, 
                      UDN& udn, Bool_t sourcesel, Bool_t& ret);
      /// Destructor
      virtual ~TLGDfmUDNDirDlg ();
      /// Close window
      virtual void CloseWindow();
      /// Get UDN 
      virtual Bool_t GetValues (UDN& udn);
      /// Set UDN 
      virtual Bool_t SetValues (const UDN& udn);
      /// Choose file
      virtual Bool_t ChooseFile (UDN& udn);
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, Long_t);
   };


/** Add tape specification dialog box.
    @memo Add tape specification dialog box.
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGDfmUDNTapeDlg : public ligogui::TLGTransientFrame {
   private:
      /// Source selection?
      Bool_t		fSourceSel;
      /// Return UDN
      UDN*		fUDN;
      /// Return value
      Bool_t*		fOk;
   
      /// Layout hinst
      TGLayoutHints*	fL[9];
      /// Frames
      TGCompositeFrame*	fF[8];
      /// Labels
      TGLabel*		fLabel[1];
      /// Device name
      TGTextEntry*	fName;
      /// Parameter buttons
      TGButton*		fPrmButton[4];
      /// Filepos/archive
      ligogui::TLGNumericControlBox*	fPosArch;
      /// Number value
      ligogui::TLGNumericControlBox*	fFNum;
      /// File/dir spec
      TGTextEntry*	fFD;
      /// Robot spec
      TGTextEntry*	fRobot;
      /// Ok button
      TGButton*		fOkButton;
      /// Cancel button
      TGButton*		fCancelButton;
   
   public:
      /** Constructs a tape configuration dialog box.
          @memo Constructor.
          @param p Parent window
   	  @param main Main window
   	  @param udn Tape specification
   	  @param sourcesel input UDN (true) or output UDN
   	  @param ret True if successful, False if cancel
       ******************************************************************/
      TLGDfmUDNTapeDlg (const TGWindow *p, const TGWindow *main,
                       UDN& udn, Bool_t sourcesel, Bool_t& ret);
      /// Destructor
      virtual ~TLGDfmUDNTapeDlg ();
      /// Close window
      virtual void CloseWindow();
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, Long_t);
   };


/** Add shared memory partition dialog box.
    @memo Add shared memory partition dialog box.
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGDfmUDNSmDlg : public ligogui::TLGTransientFrame {
   private:
      /// Return UDN
      UDN*		fUDN;
      /// Return value
      Bool_t*		fOk;
   
      /// Layout hinst
      TGLayoutHints*	fL[7];
      /// Frames
      TGCompositeFrame*	fF[6];
      /// Labels
      TGLabel*		fLabel[3];
      /// Partition
      TGTextEntry*	fName;
      /// Offline button
      TGButton*		fOfflineButton;
      /// Size value
      ligogui::TLGNumericControlBox*	fSize;
      /// Number value
      ligogui::TLGNumericControlBox*	fNum;
      /// Ok button
      TGButton*		fOkButton;
      /// Cancel button
      TGButton*		fCancelButton;
   
   public:
      /** Constructs a shared memory partition configuration dialog box.
          @memo Constructor.
          @param p Parent window
   	  @param main Main window
   	  @param udn Shared memory partition name
   	  @param ret True if successful, False if cancel
       ******************************************************************/
      TLGDfmUDNSmDlg (const TGWindow *p, const TGWindow *main,
                     UDN& udn, Bool_t& ret);
      /// Destructor
      virtual ~TLGDfmUDNSmDlg ();
      /// Close window
      virtual void CloseWindow();
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, Long_t);
   };


/** Multiple UDN selection dialog box.
    @memo Multiple UDN selection dialog box.
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class TLGDfmUDNDlg : public ligogui::TLGTransientFrame {
   private:
      /// Source selection?
      Bool_t		fSourceSel;
      /// Data server with list of available UDNs
      dataserver*	fDS;
      /// Currently selected UDNs
      UDNList		fUDNcur;
      /// UDN return value
      UDNList*		fUDNret;
      /// server type
      dataservicetype	fServerType;
      /// Return value
      Bool_t*		fOk;
   
      /// Layout hinst
      TGLayoutHints*	fL[8];
      /// Selection group
      TGGroupFrame*	fSelGroup;
      /// Lines
      TGCompositeFrame*	fLine[(kMaxUDN + 1)/2];
      /// UDN active checkbox
      TGButton*		fUDNActive[kMaxUDN];
      /// UDN selection
      TGComboBox*	fUDN[kMaxUDN];
      /// Button frame
      TGCompositeFrame*	fBtnFrame;
      /// Ok button
      TGButton*		fOkButton;
      /// Cancel button
      TGButton*		fCancelButton;
      /// Add button
      TGButton*		fAddButton;
   
   public:
      /** Constructs a new UDN selection dialog box.
          @memo Constructor.
          @param p Parent window
   	  @param main Main window
   	  @param ds Data server
   	  @param udnsel Selected list of UDNs (return)
   	  @param ret True if successful, False if cancel
       ******************************************************************/
      TLGDfmUDNDlg (const TGWindow* p, const TGWindow* main,
                   dataserver& ds, Bool_t sourcesel, UDNList& udnsel, 
                   Bool_t& ret);
      /// Destructor
      virtual ~TLGDfmUDNDlg ();
      /// Close window
      virtual void CloseWindow();
      /// build selection 
      virtual void Build ();
      /// read selection
      virtual void GetData ();
   
      /// Add UDN
      static Bool_t addUDN (const TGWindow* p, dataserver& ds,
                        Bool_t sourcesel, UDN* newudn = 0);
      /// Add file UDN
      static Bool_t addFileUDN (const TGWindow* p, dataserver& ds,
                        Bool_t sourcesel, UDN* newudn = 0);
      /// Add tape UDN
      static Bool_t addTapeUDN (const TGWindow* p, dataserver& ds,
                        Bool_t sourcesel, UDN* newudn = 0);
      /// Add shared memory partition
      static Bool_t addSmUDN (const TGWindow* p, dataserver& ds,
                        Bool_t sourcesel, UDN* newudn = 0);
      /// Process messages
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, Long_t);
   };


//@}

}


#endif // _LIGO_TLFDFMUDN_H
