/* version $Id: TLGDfmChannels.cc 7574 2016-02-16 20:17:52Z james.batch@LIGO.ORG $ */
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGDfmChannels						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TLGDfmChannels.hh"
#include "TLGEntry.hh"
#include "TLGChannelBox.hh"
#include <TGMsgBox.h>
#include <TVirtualX.h>
// #include <iostream>
#include <fstream>
#include <algorithm>
#include <string.h>
#include <strings.h>

namespace dfm {
   using namespace std;
   using namespace ligogui;
   using namespace fantom;

   const int kMaxChnLength = 1024 * 1024;

   static const char* const gTextTypes[] = { 
   "Text files", "*.txt",
   "ASCII files", "*.asc",
   "All files", "*",
   0, 0 };



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGDfmChnSelDlg						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGDfmChnSelDlg::TLGDfmChnSelDlg (const TGWindow* pwin, 
                     const TGWindow* main, dataserver& ds, 
                     selserverentry& entry, 
                     const fantom::channellist& avail, Bool_t& ret)
   : TLGTransientFrame (pwin, main, 10, 10), fDS (&ds), fEntry (&entry),
   fChnavail (avail), fOk (&ret)
   {
      // list of channels for comboboxes
      int size = fChnavail.size();
      fAllChns = new (nothrow) ChannelEntry[size];
      fAllChnNum = 0;
      if (fAllChns) {
         for (const_chniter i = fChnavail.begin(); i != fChnavail.end(); ++i) {
            if (!i->IsDuplicate()) {
               fAllChns[fAllChnNum].SetName (i->Name());
               fAllChns[fAllChnNum].SetUDN (i->UDN());
               fAllChns[fAllChnNum].SetRate (i->Rate());
               fAllChnNum++;
            }
         }
      }
      // list of selected channels
      channellist wcard;
      for (const_chniter i = fEntry->channels().begin(); 
          i != fEntry->channels().end(); ++i) {
         if (i->IsWildcard()) {
            wcard.push_back (*i);
         }
         else {
            fChncur.push_back (*i);
         }
      }
      string wildcardsel;
      Channels2String (wcard, wildcardsel);
   
      // Layout hints
      fL[0] = new TGLayoutHints (kLHintsTop | kLHintsExpandX, 5, 5, 5, 5);
      fL[1] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 0, 0, 4, 0);
      fL[2] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 2, 2, 0, 0);
      fL[3] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 2, 2, 0, 0);
      fL[4] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 12, 2, 10, 0);
      fL[5] = new TGLayoutHints (kLHintsRight | kLHintsTop, 2, 12, 10, 0);
      fL[6] = new TGLayoutHints (kLHintsTop | kLHintsLeft | kLHintsExpandX,
                           25, 25, 2, 2);
      fL[7] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 25, 2, 0, 0);
      fL[8] = new TGLayoutHints (kLHintsLeft | kLHintsTop |
                           kLHintsExpandX, 0, 0, 4, 0);
      fL[9] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY |
                           kLHintsExpandX, 2, 2, 0, 0);
      // channel list
      fG[0] = new TGGroupFrame (this, "List");
      AddFrame (fG[0], fL[0]);
      Int_t chntype = kChannelTreeShowRate | kChannelTreeSeparateSlow;
      if (fAllChnNum > 1000) chntype |= kChannelTreeLevel3 ;
      if (fAllChnNum > 5000) chntype |= kChannelTreeLevel6 ;
      // Sort the channel list so the tree gets built correctly.
      ChannelTree::SortChannelList(fAllChns, fAllChnNum, chntype) ;
      for (int i = 0; i < (kMaxChn + 1) / 2; i++) {
         fLine[i] = new TGHorizontalFrame (fG[0], 10, 10);
         fG[0]->AddFrame (fLine[i], fL[1]);
         for (int j = i; j < kMaxChn; j += kMaxChn/2) {
            fChnLabel[j] = new TGLabel (fLine[i], "       ");
            fLine[i]->AddFrame (fChnLabel[j], j < kMaxChn/2 ? fL[2] : fL[7]);
            fChnActive[j] = new TGCheckButton(fLine[i], "", kDfmChnActive+j);
            fChnActive[j]->Associate (this);
            fLine[i]->AddFrame (fChnActive[j], fL[3]);

            fChn[j] = new TLGChannelCombobox (fLine[i], kDfmChn + j,
                                 fAllChns, fAllChnNum, kTRUE, chntype, kTRUE);
            fChn[j]->Associate (this);
            fChn[j]->SetPopupHeight (400);
            fChn[j]->Resize (450, 22);
            fLine[i]->AddFrame (fChn[j], fL[3]);

            fRate[j] = new TLGNumericControlBox (fLine[i], 0., 6, 
                                 kDfmChnRate, kNESReal, kNEANonNegative);;
            fRate[j]->Associate (this);
            fLine[i]->AddFrame (fRate[j], fL[3]);
         }
      }
      fF[1] = new TGHorizontalFrame (fG[0], 10, 10);
      fG[0]->AddFrame (fF[1], fL[8]);
      fChnFrwdBkwd[0] = new TGTextButton (fF[1], 
                           new TGHotString ("        <<        "), kDfmChnBack);
      fChnFrwdBkwd[0]->Associate (this);
      fF[1]->AddFrame (fChnFrwdBkwd[0], fL[4]);
      fChnFrwdBkwd[1] = new TGTextButton (fF[1], 
                           new TGHotString ("        >>        "), kDfmChnForward);
      fChnFrwdBkwd[1]->Associate (this);
      fF[1]->AddFrame (fChnFrwdBkwd[1], fL[5]);
      // wildcard selection
      fG[1] = new TGGroupFrame (this, "Wildcard");
      AddFrame (fG[1], fL[0]);
      fF[2] = new TGHorizontalFrame (fG[1], 10, 10);
      fG[1]->AddFrame (fF[2], fL[8]);
      fWildcard = new TLGTextEntry (fF[2], wildcardsel.c_str(), 
                           kDfmChnWildcard);
      fWildcard->Associate (this);
      fWildcard->SetMaxLength (kMaxChnLength);
      fF[2]->AddFrame (fWildcard, fL[9]);
      // Rate selection
      fG[2] = new TGGroupFrame (this, "Rate");
      AddFrame (fG[2], fL[0]);
      fF[3] = new TGHorizontalFrame (fG[2], 10, 10);
      fG[2]->AddFrame (fF[3], fL[8]);
      fMaxRate = new TGCheckButton (fF[3], "Maximum     ", kDfmChnMax);
      fMaxRate->Associate (this);
      fF[3]->AddFrame (fMaxRate, fL[2]);
      fEqRate = new TGCheckButton (fF[3], "All equal       ", kDfmChnEq);
      fEqRate->Associate (this);
      fF[3]->AddFrame (fEqRate, fL[2]);
      fMaxEqRate = new TLGNumericControlBox (fF[3], 1., 6, 
                           kDfmChnMaxEqRate, kNESReal, kNEANonNegative);;
      fMaxEqRate->Associate (this);
      fF[3]->AddFrame (fMaxEqRate, fL[3]);
      // button group
      fF[0] = new TGHorizontalFrame (this, 100, 20);
      AddFrame (fF[0], fL[0]);
      fOkButton = new TGTextButton (fF[0], 
                           new TGHotString ("&Ok"), 1);
      fOkButton->Associate (this);
      fF[0]->AddFrame (fOkButton, fL[6]);
      fFileButton = new TGTextButton (fF[0], 
                           new TGHotString ("&File..."), 2);
      fFileButton->Associate (this);
      fF[0]->AddFrame (fFileButton, fL[6]);
      fClearButton = new TGTextButton (fF[0], 
                           new TGHotString ("C&lear"), 3);
      fClearButton->Associate (this);
      fF[0]->AddFrame (fClearButton, fL[6]);
      fAllButton = new TGTextButton (fF[0], 
                           new TGHotString ("&All"), 4);
      fAllButton->Associate (this);
      fF[0]->AddFrame (fAllButton, fL[6]);
      fCancelButton = new TGTextButton (fF[0], 
                           new TGHotString ("&Cancel"), 0);
      fCancelButton->Associate (this);
      fF[0]->AddFrame (fCancelButton, fL[6]);
      // set values
      fOffset = -1;
   
      // set dialog box title
      SetWindowName ("Channel Selection");
      SetIconName ("Channel Selection");
      SetClassHints ("ChnSelectionDlg", "ChnSelectionDlg");
      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                              (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
                              (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                              ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
      // make the message box non-resizable
      SetWMSize(width, height);
      SetWMSizeHints(width, height, width, height, 0, 0);
      SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
                 kMWMDecorMinimize | kMWMDecorMenu,
                 kMWMFuncAll  | kMWMFuncResize    | kMWMFuncMaximize |
                 kMWMFuncMinimize,
                 kMWMInputModeless);
   
      MapWindow();
      SetChnOffset (0);
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   TLGDfmChnSelDlg::~TLGDfmChnSelDlg ()
   {
      delete fClearButton;
      delete fAllButton;
      delete fFileButton;
      delete fCancelButton;
      delete fOkButton;
      delete fMaxEqRate;
      delete fEqRate;
      delete fMaxRate;
      delete fWildcard;
      delete fChnFrwdBkwd[0];
      delete fChnFrwdBkwd[1];
      for (int i = 0; i < kMaxChn; i++) {
         delete fRate[i];
         delete fChn[i];
         delete fChnLabel[i];
         delete fChnActive[i];
      }
      for (int i = 0; i < (kMaxChn + 1)/2; i++) {
         delete fLine[i];
      }
      for (int i = 0; i < 4; i++) {
         delete fF[i];
      }
      for (int i = 0; i < 3; i++) {
         delete fG[i];
      }
      for (int i = 0; i < 10; i++) {
         delete fL[i];
      }
      delete [] fAllChns;
   }

//______________________________________________________________________________
   void TLGDfmChnSelDlg::CloseWindow()
   {
      if (fOk) *fOk = kFALSE;
      DeleteWindow();
   }

//______________________________________________________________________________
   Bool_t TLGDfmChnSelDlg::ReadValues ()
   {
      // get values
      SetChnOffset (-1);
      // remove inactive
      for (chniter i = fChncur.begin(); i != fChncur.end(); ) {
         if (i->Active()) {
            ++i;
         }
         else {
            fChncur.erase (i);
         }
      }
      // get rate limits
      bool max = (fMaxRate->GetState() == kButtonDown);
      bool eq = (fEqRate->GetState() == kButtonDown);
      Float_t maxeq = fMaxEqRate->GetNumber();
      // add wildcard channels to current selection
      channellist wcard;
      String2Channels (wcard, fWildcard->GetText());
      copy (wcard.begin(), wcard.end(), 
           back_inserter (fChncur));
      SortChannels (fChncur);
      // check rate limits
      if (max) {
         for (chniter i = fChncur.begin(); i != fChncur.end(); ++i) {
            if ((i->Rate() == 0) || (i->Rate() > maxeq)) {
               i->SetRate (maxeq);
            }
         } 
      }
      if (eq) {
         for (chniter i = fChncur.begin(); i != fChncur.end(); ++i) {
            i->SetRate (maxeq);
         } 
      }
      // set ret value
      fEntry->setChannels (fChncur);
      return kTRUE;
   }

//______________________________________________________________________________
   void TLGDfmChnSelDlg::SetChnOffset (Int_t ofs)
   {
      if (ofs == fOffset) {
         return;
      }
      // read old values
      if (fOffset >= 0) {
         for (int i = 0; i < kMaxChn; i++) {
            // active
            bool active = (fChnActive[i]->GetState() == kButtonDown);
            // selected name
            string name = fChn[i]->GetChannel();
            // selected rate
            float rate = fRate[i]->GetNumber();
            if (fOffset + i >= (int)fChncur.size()) {
               fChncur.resize (fOffset + i + 1);
            }
            channelentry chn (name.c_str(), rate);
            chn.SetActive (active);
            fChncur[fOffset + i] = chn;
         }
      }
      // don't write on negative offset
      if (ofs < 0) {
         return;
      }
      // write new values
      fOffset = ofs;
      for (int i = 0; i < kMaxChn; i++) {
         // label
         char chnnum[256];
         sprintf (chnnum, "%3i:", fOffset + i);
         fChnLabel[i]->SetText (new TGString (chnnum));
         // in selected list?
         bool active = false;
         string name = "";
         float rate = 0;
         if (fOffset + i < (int)fChncur.size()) {
            active = fChncur[fOffset + i].Active();
            name = fChncur[fOffset + i].Name();
            rate = fChncur[fOffset + i].Rate();
         }
         // active
         fChnActive[i]->SetState (active ? kButtonDown : kButtonUp);
         // selected name
         fChn[i]->SetChannel (name.c_str());
         // selected rate
         fRate[i]->SetNumber (rate);
      }
   }

//______________________________________________________________________________
   Bool_t TLGDfmChnSelDlg::ReadChnFromFile (const char* filename)
   {
      if (filename == 0) {
         return kFALSE;
      }
      ifstream inp (filename);
      if (!inp) {
         return kFALSE;
      }
      fChncur.clear();
      string wildcardsel;
      char buf[1024];
      int err = 0;
      while ((err < 10) && inp.getline (buf, 1023, '\n')) {
         // read line
         string line = buf;
         while (!line.empty() && isspace (line[0])) {
            line.erase (0, 1);
         }
         if (line.empty() || (line[0] == '#')) {
            continue;
         }
         // find channel name & rate
         string::size_type pos = line.find_first_of (" \t\n\f\r\v");
         string name;
         float rate = 0;
         if (pos == string::npos) {
            name = line;
         }
         else {
            name = line.substr (0, pos);
            line.erase (0, pos + 1);
            while (!line.empty() && isspace (line[0])) {
               line.erase (0, 1);
            }
            if (!line.empty() && (isdigit (line[0]) || (line[0] == '.'))) {
               rate = atof (line.c_str());
            }
         }
         // check if channel name is valid
         bool ok = true;
         for (string::iterator c = name.begin(); c != name.end(); ++c) {
            if (!isprint (*c)) {
               ok = false;;
               break;
            }
         }
         if (!ok) {
            err++;
            continue;
         }
         // add channel
         channelentry chn (name.c_str(), rate);
         const_chniter f = 
            lower_bound (fChnavail.begin(), fChnavail.end(), chn);
         if (f == fChnavail.end() ||
            (strcasecmp (f->Name(), chn.Name()) != 0)) {
            if (!wildcardsel.empty()) wildcardsel += " ";
            wildcardsel += name;
            if (rate) {
               sprintf (buf, " %g", rate);
               wildcardsel += buf;
            }
         }
         else {
            fChncur.push_back (chn);
         }
      }
      fWildcard->SetText (wildcardsel.c_str());
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGDfmChnSelDlg::ProcessMessage (Long_t msg, 
                     Long_t parm1, Long_t)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         switch (parm1) {
            // Cancel
            case 0:
               {
                  if (fOk) *fOk = kFALSE;
                  DeleteWindow();
                  break;
               }
            // Ok
            case 1:
               {
                  // get values
                  if (ReadValues()) {
                     if (fOk) *fOk = kTRUE;
                     DeleteWindow();
                  }
                  break;
               }
            // File
            case 2:
               {
                  // read channels from file
                  TGFileInfo	info;
                  info.fFilename = 0;
                  info.fIniDir = 0;
               #if ROOT_VERSION_CODE >= ROOT_VERSION(3,1,6)
                  info.fFileTypes = const_cast<const char**>(gTextTypes);
               #else
                  info.fFileTypes = const_cast<char**>(gTextTypes);
               #endif
	       #if 1
		  info.fFileTypeIdx = 4 ; // Point at all files.
		  {
		     // On return, info.fFilename will be filled in
		     // unless the user cancels the operation.
		     new TLGFileDialog(this, &info, kFDOpen) ;
		  }
		  if (info.fFilename)
	       #else
                  if (TLGFileDialog (this, info, kFDOpen, 0)) 
	       #endif
		  {
                     if (ReadChnFromFile (info.fFilename)) {
                        fOffset = -1;
                        SetChnOffset (0);
                     }
                     else {
                        Int_t retval;
                        char buf[1024];
                        sprintf (buf, "Unable to read %s\n", 
                                info.fFilename);
                        new TGMsgBox(fClient->GetRoot(), this, "Error", 
                                    buf, kMBIconStop, kMBOk, &retval);
                     }
                  }
               #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
                  delete [] info.fFilename;
               #endif
                  break;
               }
            // Clear
            case 3:
               {
                  // clear channel selection
                  fChncur.clear();
                  fOffset = -1;
                  SetChnOffset (0);
                  fWildcard->SetText ("");
                  break;
               }
            // All
            case 4:
               {
                  // Select all channls
                  fChncur.clear();
                  copy (fChnavail.begin(), fChnavail.end(), 
                       back_inserter (fChncur));
                  fOffset = -1;
                  SetChnOffset (0);
                  fWildcard->SetText ("");
                  break;
               }
            // Channel forward
            case kDfmChnBack:
               {
                  // set new channel list offset
                  Int_t newofs = fOffset - kMaxChn/2;
                  if (newofs < 0) newofs = 0;
                  SetChnOffset (newofs);
                  break;
               }
            // Channel backward
            case kDfmChnForward:
               {
                  // set new channel list offset
                  SetChnOffset (fOffset + kMaxChn/2);
                  break;
               }
         }
      }
      // Check buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_CHECKBUTTON)) {
         switch (parm1) {
            // Max rate
            case kDfmChnMax:
               {
                  // unset equal rate
                  if (fMaxRate->GetState() == kButtonDown) {
                     fEqRate->SetState (kButtonUp);
                  }
                  break;
               }
            // Equal rate
            case kDfmChnEq:
               {
                  // unset max rate
                  if (fEqRate->GetState() == kButtonDown) {
                     fMaxRate->SetState (kButtonUp);
                  }
                  break;
               }
         }
      }
      return kTRUE;
   }


}
