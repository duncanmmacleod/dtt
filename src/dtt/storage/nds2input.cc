/* -*- mode: c++; c-basic-offset: 4; -*- */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: nds2input						*/
/*                                                         		*/
/* Module Description: reads in channel data through the NDS2 interface	*/
/* implements decimation and zoom functions, partitions the data and	*/
/* stores it in a storage object					*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

//#define DEBUG

// Header File List:
#include "nds2input.hh"
#include <strings.h>
#include <signal.h>
#include <pthread.h>
#include <math.h>
#include <errno.h>
#include <algorithm>
#include <iostream>
#include "tconv.h"
#include "gdsprm.h"
#include "gdstask.h"
#include "map.h"
#include "fftype.hh"
#if defined (_CONFIG_DYNAMIC)
#include "confinfo.h" 
#endif

using namespace sends;
using namespace std;
using namespace thread;
using namespace framefast;

namespace diag {

    /*----------------------------------------------------------------------*/
    /*                                                         		    */
    /* Constants: PRM_FILE		  parameter file name		    */
    /*            PRM_SECTION	  section heading is channel name!	    */
    /*            PRM_SERVERNAME	  entry for server name		    */
    /*            PRM_SERVERPORT	  entry for server port		    */
    /*            DAQD_SERVER	  default server name for channel info	    */
    /*            DAQD_PORT		  default port for channel info	    */
    /*            __ONESEC		  one second (in nsec)		    */
    /*            _MIN_NDS_DELAY	  minimum delay allowed for NDS (s) */
    /*            _MAX_NDS_DELAY	  maximum delay allowed for NDS (s) */
    /*            _NDS_DELAY	  NDS delay for slow data (sec)		    */
    /*            taskNds2Name	  nds task priority			    */
    /*            taskNds2Priority	  nds task name			    */
    /*            taskCleanupName	  nds clenaup task priority	    */
    /*            taskCleanupPriority nds cleanup task name		    */
    /*            daqBufLen		  length of receiving socket buffer */
    /*            							    */
    /*----------------------------------------------------------------------*/
#define _CHNLIST_SIZE		200
#if !defined (_CONFIG_DYNAMIC)
#define PRM_FILE		gdsPathFile ("/param", "nds2.par")
#define PRM_SECTION		gdsSectionSite ("nds2")
#define PRM_SERVERNAME		"hostname"
#define PRM_SERVERPORT		"port"
#define DAQD_SERVER		"ldas-pcdev1.ligo.caltech.edu"
    //#define DAQD_PORT		31200
#endif
#define _MIN_NDS_DELAY		0.0
#define _MAX_NDS_DELAY		64.0 // 64 seconds
#define _NDS_DELAY		1

    static const int my_debug = 0 ;

    static const char	taskNds2Name[] = "tNDS2";
    static const int	taskNds2Priority = 0;
    static const char	taskCleanupName[] = "tNDS2cleanup";
    static const int	taskCleanupPriority = 20;
    static const int 	daqBufLen = 1024*1024;
    static const long	taskNds2OnlineTimeout = 64;
    static const long	taskNds2OfflineTimeout = 24 * 3600;  // 1 day!
    static const bool 	kNds2Debug = false;

    static const double __ONESEC = double(_ONESEC);


    /*----------------------------------------------------------------------*/
    /*                                                         		*/
    /* Class Name: nds2Manager						*/
    /*                                                         		*/
    /*----------------------------------------------------------------------*/
    int nds2Manager::ndstask (nds2Manager& NDS2Mgr) 
    {
	int		len;	// length of read buffer
	int		seqNum = -1;
	int		err;
	const timespec tick = {0, 1000000}; // 1ms
   
	// wait for data
	pthread_setcancelstate (PTHREAD_CANCEL_DISABLE, 0);
	while (1) {
	    // get the mutex
	    while (!NDS2Mgr.ndsmux.trylock()) {
		pthread_setcancelstate (PTHREAD_CANCEL_ENABLE, 0);
		nanosleep (&tick, 0);
		pthread_testcancel();
		pthread_setcancelstate (PTHREAD_CANCEL_DISABLE, 0);
	    }

	    // check if data is ready
	    // tainsec_t	t1 = TAInow();
	    err = NDS2Mgr.nds.WaitforData (true);
	    if (err < 0) {
		cerr << "NDS2 socket ERROR" << endl;
		NDS2Mgr.shut();
		NDS2Mgr.ndsmux.unlock();
		return -1;
	    }
	    else if (err == 0) {
		NDS2Mgr.ndsmux.unlock();
		pthread_setcancelstate (PTHREAD_CANCEL_ENABLE, 0);
		nanosleep (&tick, 0);
		pthread_testcancel();
		pthread_setcancelstate (PTHREAD_CANCEL_DISABLE, 0);
		continue;
	    }
	    // get data
	    err = 0;
	    cerr << "get data from nds2" << endl;
	    len = NDS2Mgr.nds.GetData();
	    cerr << "got data from nds2 " << len 
		 << " (>0 length, <0 error, -13 timeout)" << endl;
	    int seq_in = NDS2Mgr.nds.mRecvBuf.ref_header().SeqNum;
	    if (len == 0) {
		cerr << "Data block with length 0 encountered " << 
		    "****************************" << endl;
	    }
	    // check sequence number
	    else if (len > 0) {
		if (seqNum >= 0 && seq_in != seqNum + 1) err = 1;
		seqNum = seq_in; 
		cerr << "seq # = " << seqNum << endl;
	    }
	    if (err || (len < 0)) {
		cerr << "DATA RECEIVING ERROR " << len 
		     << " errno " << errno << endl;
		// exit (1);
	    }
      
	    // process reconfigure information
	    if (len > 0) {
		if (!NDS2Mgr.ndsdata (err)) {
		    len = -1;
		}
	    }
	    // end of data transmission encountered
	    else if (len <= 0) {
		cerr << "TRAILER TIME = " 
		     << NDS2Mgr.nds.mRecvBuf.ref_header().GPS 
		     << endl;
	    }

	    // quit if end of transfer is reached or on fatal error
	    if ((len < 0) || ((len == 0) && !NDS2Mgr.online_req)) {
		if ((len <= 0) && !NDS2Mgr.online_req) {
		    NDS2Mgr.dataCheckEnd();
		}
		NDS2Mgr.shut();
		NDS2Mgr.ndsmux.unlock();
		return -1;
	    }
	    NDS2Mgr.ndsmux.unlock();
	    // tainsec_t	t2 = TAInow();
	    // cerr << "TIME nds2task = " << (double)(t2-t1)/1E9 << endl;
	    pthread_setcancelstate (PTHREAD_CANCEL_ENABLE, 0);
	    pthread_testcancel();
	    pthread_setcancelstate (PTHREAD_CANCEL_DISABLE, 0);
	}
	return 0;
    }

    //==================================  Constructor
    nds2Manager::nds2Manager (gdsStorage* dat, testpointMgr* TPMgr, 
			      double Lazytime) 
	: dataBroker (dat, TPMgr, Lazytime), userNDS (false), 
	  RTmode (false), online_req(false), abort(false)
    {
	daqPort = 0;
    }

    //==================================  Destructor
    nds2Manager::~nds2Manager (void) {
    }

    class chnorder2 {
    public:
	chnorder2() {}
	bool operator() (const sends::DAQDChannel& c1, 
			 const sends::DAQDChannel& c2) const {
#if 0
	    return strcasecmp (c1.mName.c_str(), c2.mName.c_str()) < 0;
#else
	    bool rc = strcasecmp (c1.mName.c_str(), c2.mName.c_str()) < 0;
	    if (!rc) {
	       if (strcasecmp (c1.mName.c_str(), c2.mName.c_str()) == 0) {
		  // Compare the rates.
		  if (c1.mRate < c2.mRate) {
		     if (my_debug) cerr << "chnorder2() - " << c1.mName << " (" << c1.mRate << ") is less than " << c2.mName << " (" << c2.mRate << ")" << endl ;
		     rc = 1 ;
		  }
	       }
	    }
	    return rc ;
#endif
	}
    };

    bool nds2Manager::connect (const char* server, int port, bool usernds,
			       unsigned long epoch_start, unsigned long epoch_stop)
    {
	int		status;
   
	// get NDS2 parameters
	if (server == 0) {
	    // It's useless to get a configuration hoping that the nds2 server 
	    // responds to a UDP broadcast, it won't.  For aLIGO, even nds1
	    // servers don't respond to the UDP broadcast.  So get rid of this
	    // mess and return false if the server wasn't specified.
	    return false ;
	}
	else {
	    // user specified
	    daqServer = server;
	    daqPort = (port <= 0) ? DAQD_PORT : port;
	}
   
	// connect to NDS
	nds.setDebug (kNds2Debug);
	status = nds.open (daqServer, daqPort, daqBufLen);
	cerr << "NDS2 version = " << nds.Version() << endl;
	if (status != 0) {
	    return false;
	}
	// get channel list if user NDS
	userNDS = usernds;
	if (usernds) {
	    // Set the epoch if the start != stop.
	    if (epoch_start != epoch_stop) {
	       nds.SetEpoch(epoch_start, epoch_stop) ;
	    }

	    // The second argument to Available is a GPS time for the 
	    // channel list. 
	    // If we send the epoch start time as the GPS time to Available, we get the
	    // channel list at the time of the start of the epoch.  This won't be the
	    // complete list for the epoch, only the channels that existed at the start
	    // of the epoch.  Instead, pass a gps time of 0.  This forces the server to 
	    // give us a complete list of channels for the entire epoch.
	    // Also see readChnDAQServer() in Diag_Services/fantom/sendsio.cc
	    if (my_debug) cerr << "nds2Manager::connect() - get channel list" << endl ;
	    nds.Available (cRaw, 0, userChnList); // Call to DAQC_api::Available(), which NDS2Socket inherits.
	    nds.addAvailable(cRDS, 0, userChnList);

	    if (my_debug) cerr << "nds2Manager::connect() - sort channel list" << endl ;
	    sort (userChnList.begin(), userChnList.end(), chnorder2());
	    
	    if (my_debug) cerr << "nds2Manager::connect() - done" << endl ;
	}
   
	return true;
    }


    bool nds2Manager::set (tainsec_t start, tainsec_t* active)
    {
	semlock		lockit (mux);	// lock mutex */
	tainsec_t		chnactive;	// time when channel active
   
	// check if lazy clears have to be committed
	if ((cleartime > 0) && !areUsed()) {
	    mux.unlock();
	    if (!dataStop ()) {
		return false;
	    }
	    mux.lock();
	    channellist::iterator iter = channels.begin();
	    while (iter != channels.end()) {
		// if not used delete
		if (iter->inUseCount() <= 0) {
		    nds.RmChannel (iter->getChnName());
		    iter = channels.erase (iter);
		}
		else {
		    iter++;
		}
	    }
	}
   
	// set minimum active time
	if (active != 0) {
	    *active = start;
	}
	cleartime = 0;
	// check if already set
   
	if (!areSet()) {
	    // make sure nds is stopped
	    mux.unlock();
	    if (!dataStop ()) {
		return false;
	    }
	    mux.lock();
	    nds.RmChannel ("all");
	    // setup channels
	    for (channellist::iterator iter = channels.begin();
		 iter != channels.end(); iter++) {
		// add channel to list
		nds.AddChannel(iter->getChnName(), 
			       cOnline, 
			       iter->getDatarate());
		if (iter->isSet()) {
		    continue;
		}
		// activate channel
		if (!iter->subscribe (start, &chnactive)) {
		    // error
		    for (channellist::reverse_iterator iter2 (iter);
			 iter2 != channels.rend(); iter2++) {
			iter2->unsubscribe();
		    }
		    nds.RmChannel ("all");
		    return false;
		}
		if (active != 0) {
		    *active = max (chnactive, *active);
		}
	    }
	}
   
	// all set: start nds2
	if (!ndsStart ()) {
	    for (channellist::iterator iter = channels.begin();
		 iter != channels.end(); iter++) {
		iter->unsubscribe();
	    }     
	    return false;
	}
	// round active time to next second after adding max. filter delays
	if (active != 0) {
	    tainsec_t 	now = TAInow();
	    now = _ONESEC * ((now + _ONESEC - 1) / _ONESEC);
	    *active = max (now, *active);
	}
   
	return true;
    }


    bool nds2Manager::set (taisec_t start, taisec_t duration)
    {
	semlock		lockit (mux);	// lock mutex */
	cerr << "TIME STAMP BEFORE START = " << timeStamp() << endl;
   
	// make sure nds2 is stopped
	mux.unlock();
	if (!dataStop ()) {
	    return false;
	}
	mux.lock();
	nds.RmChannel ("all");
	cleartime = 0;
   
	// setup channels
	if (my_debug) cerr << "nds2Manager::set() - setup channels for NDS2" << endl;
	for (channellist::iterator iter = channels.begin();
	     iter != channels.end(); iter++) {
	    // add channel to list
	    //cerr << "Add channel: " << iter->getChnName() << " rate: " << iter->getDatarate() << endl;
	    nds.AddChannel (iter->getChnName(), cRaw, iter->getDatarate());
	    if (iter->isSet()) {
		continue;
	    }
	    // activate channel
	    if (!iter->subscribe (start, 0)) {
		// error
		for (channellist::reverse_iterator iter2 (iter);
		     iter2 != channels.rend(); iter2++) {
		    iter2->unsubscribe();
		}
		nds.RmChannel ("all");
		return false;
	    }
	}
   
	// all set: start nds2
	cerr << "start NDS2 @ " << start << ":" << duration << endl;
	if (!ndsStart (start, duration)) {
	    for (channellist::iterator iter = channels.begin();
		 iter != channels.end(); iter++) {
		iter->unsubscribe();
	    }     
	    return false;
	}
   
	cerr << "start NDS2 @ " << start << ":" << duration << " done" << endl;
	return true;
    }

    bool nds2Manager::channelInfo(const string& name, gdsChnInfo_t& info, int rate) const
    {
	if (my_debug) cerr << "nds2Manager::channelInfo(" << name << ", ..., " << rate << ")" << endl;
	if (!userNDS) {
	    cout << "Get channel info from channelHandler." << endl;
	    return channelHandler::channelInfo (name, info, rate);
	}
	else {
	    cout << "Get nds2 channel info for " << name << ", rate = " << rate << endl;
	    // find channel
	    DAQDChannel item;
	    item.mName =name;
	    if (rate > 0) {
	       item.mRate = rate ;
	    }
	    // The lower_bound function uses chnorder2() to find the
	    // first channel that matches the criteria that makes 
	    // chnorder2() happy. 
	    vector<DAQDChannel>::const_iterator chn =
		lower_bound (userChnList.begin(), userChnList.end(),
			     item, chnorder2());
	    memset (&info, 0, sizeof (gdsChnInfo_t));
	    if ((chn == userChnList.end()) ||
		(strcasecmp (item.mName.c_str(), chn->mName.c_str()) != 0)) {
		cout << "NDS2 has no channel info for " << name << endl;
		return false;
	    }
	    else {
		strncpy (info.chName, chn->mName.c_str(), 
			 sizeof (info.chName)-1);
		info.chName[sizeof(info.chName)-1] = 0;
		info.chGroup = int(chn->mChanType);
		info.dataRate = int(chn->mRate);
		info.bps = DAQDChannel::datatype_size(chn->mDatatype);
		info.dataType = chn->mDatatype;
		info.gain = chn->mGain;
		info.slope = chn->mSlope;
		info.offset = chn->mOffset;
		strncpy (info.unit, chn->mUnit.c_str(), sizeof (info.unit)-1);
		info.unit[sizeof(info.unit)-1] = 0;
		if (my_debug) cerr << "channelInfo() - found info, rate = " << info.dataRate << endl ;
		return true;
	    }
	}
    }

    void nds2Manager::shut(void) {
	TID = 0;
	nds.StopWriter ();
	nds.RmChannel ("all");
	nds.close();
    }

    bool nds2Manager::getTimes (taisec_t& start, taisec_t& duration)
    {
	start = 0;
	duration = 0;
	// get time segments
	return true; //(nds.Times (start, duration) == 0);
    }


    tainsec_t nds2Manager::timeoutValue (bool online) const
    {
	if (online) return tainsec_t(taskNds2OnlineTimeout)  * _ONESEC;
	else        return tainsec_t(taskNds2OfflineTimeout) * _ONESEC;
    }


    template <class T>
    inline void convertNDS2Data (float x[], T y[], int len)
    {
	if (littleendian()) {
	    T tmp;
	    for (int i = 0; i < len; i++) {
		tmp = y[i];
		swap (&tmp);
		x[i] = static_cast<float>(tmp);
	    }
	}
	else {
	    for (int i = 0; i < len; i++) {
		x[i] = static_cast<float>(y[i]);
	    }
	}
    }


    bool nds2Manager::ndsdata (int err)
    {
	semlock	lockit (mux);	// lock mutex 

	//---- get the start time (GPS), epoch number and duration of the
	//     data in the buffer.
	const DAQDRecHdr* head = &(nds.mRecvBuf.ref_header());
	taisec_t       time = taisec_t(head->GPS);
	//----------------- The epoch number gives the 1/16th slice number.
	//int	      epoch = (head->NSec + _EPOCH / 10) / _EPOCH;
	int           epoch = 0;
	tainsec_t  duration = tainsec_t(head->Secs) * _ONESEC;
	tainsec_t timestamp = tainsec_t(time) * _ONESEC 
	                    + tainsec_t(epoch)* _EPOCH;

#ifdef DEBUG 
	cerr << "time GPS = " << 	time << " slice = " << epoch 
	     << " duration sec = " << double(duration) / __ONESEC << endl;
#endif

	// check if we lost data
	if ((nexttimestamp != 0) && 
	    (timestamp > nexttimestamp + 1000)) {
	    cerr << "NDS2 RECEIVING ERROR: # of epochs lost = " <<
		(timestamp - (nexttimestamp - 1000)) / _EPOCH << endl;
	}
   
	// check NDS2 time
#ifdef GDS_ONLINE
	if (RTmode) {
	    double delay = (double) (TAInow() - timestamp) / __ONESEC;
	    double maxdelay = _MAX_NDS_DELAY + duration / __ONESEC;
	    if ((delay < _MIN_NDS_DELAY) || (delay > maxdelay)) {
		cerr << "TIMEOUT ERROR: NDS2 delay = " << delay << endl;
		//return false;
	    }
	}
#endif

	//------------------------------  Find longest channel data.
	int bf_size = 0;
	for (NDS2Socket::const_channel_iter iter = nds.chan_begin(); 
	     iter != nds.chan_end(); iter++) {
	    if (iter->mStatus < 0) continue;
	    int nWdCh = iter->mStatus / 
		        DAQDChannel::datatype_size(iter->mDatatype);
	    if (bf_size < nWdCh) bf_size = nWdCh;
	}

	//------------------------------  loop over channels
	float* fptr = new float[bf_size];
	for (NDS2Socket::const_channel_iter iter = nds.chan_begin(); 
	     iter != nds.chan_end(); iter++) {

	    string name = iter->mName;
	    // find daq channel and invoke callback
	    channellist::iterator chn = find (name);
	    if ((chn == channels.end()) || (*chn != name)) {
		continue;
	    }
	    int ndata = nds.GetChannelData(name, fptr, bf_size*sizeof(float));
	    cout << "fetch channel: " << name << " ndata: " << ndata
		 << " buffer size: " << bf_size << endl;

	    //--------------------------  invoke callback
	    chn->callback (time, epoch, fptr, ndata, err);
	}
	delete[] fptr;
   
#ifdef DEBUG 
	cerr << "nds2 callback done " 
	     << double((time*_ONESEC+epoch*_EPOCH) % (1000 * _ONESEC)) / 1E9
	     << " at " << double(TAInow() % (1000 * _ONESEC)) / 1E9 << endl;
	cerr << "time stamp = " << timeStamp() << endl;
#endif
   
	// set time of last successful NDS2 data transfer
	nexttimestamp = timestamp + duration;
	lasttime = TAInow();
	return true;
    }

    bool nds2Manager::ndsStart ()
    {
	// check if already running 
	if (TID != 0) {
	    return true;
	}
	// check if any channels are selected
	if (nds.chan_begin() == nds.chan_end()) {
	    return true;
	}
   
	// start net writer
	cerr << "nds2 start" << endl;
	abort = false;
	nds.setAbort (&abort);
	RTmode = true;

	//for (NDS2Socket::const_channel_iter iter = nds.chan_begin(); 
	//     iter != nds.chan_end(); iter++) {
	//    // check data rate
	//    if (iter->mRate < NUMBER_OF_EPOCHS) {
	//	fastUpdate = false;
	//	break;
	//    }
	//}
   
	// set last time
	nexttimestamp = 0;
	starttime = 0;
	stoptime = 0; // no end
	lasttime = TAInow();
	double stride = 1.0;

	// establish connection
	if (!nds.isOpen() && (nds.open (daqServer, daqPort) != 0)) {
	    nds.RmChannel ("all");    
	    return false;
	}

	online_req = true;
	if (nds.RequestOnlineData (stride, taskNds2OnlineTimeout) != 0) {
	    nds.RmChannel ("all");    
	    return false;
	}
	
	// create nds2 task
	int		attr;	// task create attribute
	attr = PTHREAD_CREATE_DETACHED;
	if (taskCreate (attr, taskNds2Priority, &TID, 
			taskNds2Name, (taskfunc_t) ndstask, 
			(taskarg_t) this) != 0) {
	    nds.StopWriter();     
	    nds.RmChannel ("all");    
	    return false;
	}
	cerr << "nds2 started" << endl;
   
	return true;
    }

    bool nds2Manager::ndsStart (taisec_t start, taisec_t duration)
    {
	// check if already running 
	if (TID != 0) {
	    return true;
	}
	// check if any channels are selected
	if (nds.chan_begin() == nds.chan_end()) {
	    return true;
	}
   
	// wait for data to become available
	tainsec_t tEnd = (start + duration + _NDS_DELAY) * _ONESEC;
	while (TAInow() < tEnd) {
	    timespec wait = {0, 250000000};
	    nanosleep (&wait, 0);
	}

	// set last time
	nexttimestamp = start * _ONESEC;
	starttime = start * _ONESEC;
	stoptime = (start + duration) * _ONESEC;
	lasttime = TAInow();

	// start net writer
	RTmode = false;
	if (my_debug) cerr << "nds2Manager::ndsStart() - nds2 start old data" << endl;
	abort = false;
	nds.setAbort (&abort);
	if (!nds.isOpen() && (nds.open (daqServer, daqPort) != 0)) {
	    nds.RmChannel ("all");
	    cerr << "nds2 error during open" << endl;
	    return false;
	}

	online_req = false;
	if (my_debug) cerr << "nds2Manager::ndsStart() - nds.RequestData(" << start << ", " << duration << ", ...)" << endl ;
	if (nds.RequestData (start, duration, taskNds2OfflineTimeout) != 0) {     
	    nds.RmChannel ("all");
	    cerr << "nds2 error during data request" << endl;
	    return false;
	}
   
	if (my_debug) cerr << "nds2Manager::ndsStart() - Create nds2 task thread" << endl ;
	// create nds2 task
	int		attr;	// task create attribute
	attr = PTHREAD_CREATE_DETACHED;
	if (taskCreate (attr, taskNds2Priority, &TID, 
			taskNds2Name, (taskfunc_t) ndstask, 
			(taskarg_t) this) != 0) {
	    nds.StopWriter();     
	    nds.RmChannel ("all");    
	    cerr << "nds2 error during task spawn" << endl;
	    return false;
	}
	cerr << "nds2 started" << endl;
   
	return true;
    }


    bool nds2Manager::dataStop ()
    {
	cerr << "kill nds2 task: get mutex" << endl;
	// get the mutex
	int n = 30;
	const timespec tick = {0, 100000000}; // 100ms
	abort = true;
	while ((n >= 0) && !ndsmux.trylock()) {
	    nanosleep (&tick, 0);
	    n--;
	    // send a signal to unblock select in daqsocket
	    if (n % 10 == 2) {
		taskID_t tid = TID;
		if (tid) pthread_kill (tid, SIGCONT);
	    }
	}
	if (n < 0) {
	    return false;
	}
	//ndsmux.lock();
	if (TID != 0) {
	    cerr << "kill nds2 task" << endl;
	    taskCancel (&TID);
	    cerr << "killed nds2 task" << endl;
	    shut();
	    cerr << "killed nds2" << endl;
	}
	ndsmux.unlock();
	return true;
    }

}
