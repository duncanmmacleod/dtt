/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: fchannel						*/
/*                                                         		*/
/* Module Description: channel classes for entries and list		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 4Oct00   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: fchannel.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_FCHANNEL_H
#define _LIGO_FCHANNEL_H

#include <string>
#include <vector>
#include <map>

namespace fantom {


/** @name Channel support classes
    This header defines the support classes to handle channels.
    It provides a channel entry record and a list of channel records
    for both a simple collection of channels and a set of channel
    queries.
   
    @memo Channel support
    @author Written October 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


/** Channel entry. This class stores name, rate and UDN. It also
    has flags for active, duplicate and wildcard.
    
    @memo Channel entry.
 ************************************************************************/
   class channelentry {
   protected:
      /// Active
      bool		fActive;
      /// Name
      std::string	fName;
      /// Rate
      float		fRate;
      /// name of associated UDN
      std::string	fUDN;
      /// name is a duplicate?
      bool		fIsDuplicate;
      /// name has a duplicate?
      bool		fHasDuplicate;
      /// wildcard?
      bool		fWildcard;
      /// DCUID number for source in online data
      int		fDCUID ;
   
   public:
      /// Default constructor
      explicit channelentry (const char* name = 0, float rate = 0, int dcuid=0);
      /// equal
      bool operator== (const channelentry& chn) const;
      /// order
      bool operator< (const channelentry& chn) const;
      /// active?
      bool Active() const {
         return fActive; }
     /// Set active?
      void SetActive (bool set = true) {
         fActive = set; }
      /// name
      const char* Name() const {
         return fName.c_str(); }
      /// Get rate
      float Rate() const {
         return fRate; }
      /// Set rate
      void SetRate (float r) {
         fRate = r; }
      /// Get UDN?
      const char* UDN() const {
         return fUDN.c_str(); }
      /// Set UDN?
      void SetUDN (const char* udn) {
         fUDN = udn; }
      /// check duplicate
      bool checkDuplicateName (channelentry& chn);
      /// check duplicate
      bool checkDuplicateNameRate (channelentry& chn);
      /// Is duplicate?
      bool IsDuplicate() const {
         return fIsDuplicate; }
      /// Has duplicate?
      bool HasDuplicate() const {
         return fHasDuplicate; }
      /// Is wildcard?
      bool IsWildcard() const {
         return fWildcard; }
      const int DCUID () const {
	 return fDCUID ;
      }
      void SetDCUID(const int dcuid) {
	 fDCUID =  dcuid ;
      }
   };

   /// Channel list
   typedef std::vector<channelentry> channellist;
   /// Channel iterator
   typedef channellist::iterator chniter;
   /// Const channel iterator
   typedef channellist::const_iterator const_chniter;

/** Translate a channel list into an ASCII string
    @param list channel list
    @param chns channel string (return)
    @param suppressUDN suppress UDN specification if true
    @return true if successful
    @memo Channel list to ASCII.
 ************************************************************************/
   bool Channels2String (const channellist& list, std::string& chns,
                     bool suppressUDN = false);

/** Translate an ASCII string to a channel list. Returned list is
    not sorted.
    @param list channel list (return)
    @param chns channel string
    @return true if successful
    @memo Channel list to ASCII.
 ************************************************************************/
   bool String2Channels (channellist& list, const char* chns);

/** Sort a channel list. This will also update duplicate information.
    The check_rate parameter allows the checking for duplicate channels to
    consider data rate if true.  If false, only the name will be considered
    when marking duplicates.
    @param list channel list
    @param check_rate flag to check data rate as well as name.
    @return true if successful
    @memo Sort channels.
 ************************************************************************/
   bool SortChannels (channellist& list, bool check_rate = false);

/** Find a channel. List must be sorted.
    @param list channel list
    @return channel iterator (end() if not found)
    @memo Find a channel.
 ************************************************************************/
   chniter FindChannel (channellist& list, const char* name, const float rate = 0);
/** Find a channel (const). List must be sorted.
    @param list channel list
    @return channel iterator (end() if not found)
    @memo Find a channel.
 ************************************************************************/
   const_chniter FindChannelConst (const channellist& list, 
                     const char* name, const float rate = 0);


/** Channel query object. This query object uses extended regular
    expressions to match the specified pattern against frame channel
    names. Channel names are matched case-insensitive.
    @memo Channel query object.
 ************************************************************************/
   class channelquery {
   public:
      /// Create new channel query object
      explicit channelquery (const char* pattern = "", float rate = 0);
      /// Constructor
      explicit channelquery (const channelentry& chn);
   
      /// Check if pattern equal to string
      bool operator== (const char* s) const;
      /// Regular expression match (s must be all upper case!)
      bool match (const char* s) const;
      /// Regular expression match (channel name must be all upper case!)
      bool match (const channelentry& chn) const {
         return match (chn.Name()); }
      /// Is a wildcard pattern?
      bool IsWildcard() const {
         return fIsWildcard; }
      /// Get pattern string
      const char* GetPattern() const {
         return fPattern.c_str(); }
      /// Get rate
      float Rate() const {
         return fRate; }
      /// Set rate
      void SetRate (float r) {
         fRate = r; }
   
      /// Returns the channel query object as a string
      std::string str (bool withrate = true) const;
   
   protected:
      /// is wildcard
      bool		fIsWildcard;
      /// Pattern
      std::string 	fPattern;
      /// Sampling rate
      float 		fRate;
   };


/** List of channel query patterns. A channel pattern is either
    a name with or without a wildcard. A channel query pattern
    is always converted to all upper case. Channel names without 
    wildcards are matched exactlly (strcmp) after converting the
    channel name under test to all upper case. If no exact match
    can be found, the fnmatch function is used search through the 
    wildcard patterns. Again, the channel name under test is first
    converted to all upper case.
    @memo Channel query list.
 ************************************************************************/
   class channelquerylist {
   public:
      /// Exact list
      typedef std::map <std::string, channelquery> exactlist;
      /// Wildcard list
      typedef std::vector <channelquery> wildcardlist;
   
      /// Constructor
      channelquerylist() {
      }
      /// Constructor
      channelquerylist (const channellist& list);
      /// Assignment
      channelquerylist& operator= (const channellist& list);
      /// Returns the list as a string
      std::string str (bool withrate = true) const;
   
   /** Returns the first channel query object which matches the specified
       name. Returns 0 if no match was found.
       @memo Channel name matching.
       @param name Channel name ot match
       @return channel query object
    *********************************************************************/
     const channelquery* findMatch (const std::string& name) const;

   /** Returns the first channel query object which matches the specified
       channel. Returns 0 if no match was found.
       @memo Channel entry matching.
       @param chn Channel entry to match
       @return Channel query object
    *********************************************************************/
      const channelquery* findMatch (const channelentry& chn) const {
         return findMatch (chn.Name()); }
      /// Add a pattern
      void add (const channelquery& q);
      /// Add another list
      void add (const channelquerylist& list);
   
      /// Is empty?
      bool empty() const {
         return fEList.empty() && fWList.empty(); }
      /// Clear list
      void clear() {
         fEList.clear(); fWList.clear(); }
   
   protected: 
      /// Exact list
      exactlist		fEList;
      /// Wildcard list
      wildcardlist	fWList;
   };


/** Creates a new channel query list from a configuration string. The
    returned list is owned by the caller and has to be freed with
    delete when no longer used. The error string should be at least
    80 characters long.
    @memo New channel list.
    @param config Configuration script
    @param error Optional error string (return)
    @return new channel query list
 ************************************************************************/
   channelquerylist* newChannelList (const char* config, char* error = 0);

/** Creates a new channel query list from a configuration file. The
    returned list is owned by the caller and has to be freed with
    delete when no longer used. The error string should be at least
    80 characters long.
    @memo New channel list from file.
    @param filename Name of file
    @param error Optional error string (return)
    @return new channel query list
 ************************************************************************/
   channelquerylist* newChannelListFromFile (const char* filename, 
                     char* error = 0);

/** Queries a channel. Returns true if the channel passes the filter.
    If no list if supplied (i.e., filter = 0), the query returns true.
    @param channel Channel under test
    @param filter List of channel queries
    @return true if channel passes filter, false otherwise
    @memo Filter channel list.
 ************************************************************************/
   bool QueryChannel (const channelentry& channel, 
                     const channelquerylist* filter);

/** Filter a channel list (in-place operation).
    If no list if supplied (i.e., filter = 0), all channels are returned.
    @param list channel list (input/output)
    @param filter List of channel queries
    @return true if successful
    @memo Filter channel list.
 ************************************************************************/
   bool FilterChannels (channellist& list, 
                     const channelquerylist* filter);

/** Filter a channel list. Channels from list1 which pass the filter
    are appended to list2.
    If no list if supplied (i.e., filter = 0), all channels are returned.
    @param list1 input channel list
    @param list2 output channel list
    @param filter List of channel queries
    @return true if successful
    @memo Filter channel list.
 ************************************************************************/
   bool FilterChannels (const channellist& list1, 
                     channellist& list2,
                     const channelquerylist* filter);

//@}

}

#endif // _LIGO_FCHANNEL_H

