#include "framedir.hh"
#include <stdio.h>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <string>
#include <iostream>


   using namespace std;

   const int kMAXPATHLEN = 2*1024;
   const int buflen = 4*1024;

   int main (int argc, char** argv)
   {
      FrameDir		fdir;
      bool 		suppress_head = false;
      bool		continuation = true;
      FrameDir::gps_t   beg = 0;
      FrameDir::gps_t   end = 0;
   
      // parse cmd line arguments
      int 		c;
      extern char*	optarg;
      extern int	optind;	
      int		errflag = 0;
      while ((c = getopt (argc, argv, "nlb:e:h")) != EOF) {
         switch (c) {
            case 'n':
               {
                  suppress_head = true;
                  break;
               }
            case 'l':
               {
                  continuation = false;
                  break;
               }
            case 'b':
               {
                  beg = atoi (optarg);
                  break;
               }
            case 'e':
               {
                  end = atoi (optarg);
                  break;
               }
            case 'h':
               {
                  errflag = 1;
                  break;
               }
         }
      }
      if (errflag || (argc <= 1)) {
         cout << "usage: udnls [-n] [-l] [-b 'start'] [-e 'stop'] 'frame directory(ies)'" << endl;
         cout << "       -n : suppresses udn head" << endl;
         cout << "       -l : long list (one file per line)" << endl;
         cout << "       -b 'start' : start time (GPS)" << endl;
         cout << "       -e 'stop'  : stop  time (GPS)" << endl;
         cout << "example: udnls ""Data*/*.gwf""" << endl;
         exit (0);
      }
      if ((beg > 0) && (end > 0) && (beg > end)) {
         cout << "error: start time must be smaller than stop time" << endl;
         exit (0);
      }
      // get current working dir
      static char cwd[kMAXPATHLEN];
      if (::getcwd (cwd, kMAXPATHLEN) == 0) {
         strcpy (cwd, "");
      }
      // add files
      for (int i = optind; i < argc; i++) {
         string f = argv[i];
         while (!f.empty() && isspace (f[0])) f.erase (0, 1);
         if (f.empty()) {
            continue;
         }
         if (f[0] != '/') {
            f = cwd + string ("/") + f;
         }
         string::size_type pos;
         while ((pos = f.find ("/./")) != string::npos) {
            f.erase (pos, 2);
         }
         while ((pos = f.find ("//")) != string::npos) {
            f.erase (pos, 1);
         }
         fdir.add (f.c_str(), true);
      }
      // write output
      fdir.write (cout, !continuation, beg, end, suppress_head);
      // if (continuation) {
         // for (FrameDir::series_iterator i = fdir.beginSeries();
             // i != fdir.endSeries(); ++i) {
            // if ((i->second.getEndGPS() <= beg) || 
               // ((end > 0) && (i->second.getStartGPS() >= end))) {
               // continue;
            // }
            // ffData::count_t m = 0;
            // int n = i->second.getNFiles();
            // if (i->second.getStartGPS() < beg) {
               // m = (double (beg - i->second.getStartGPS()) + 0.5) / i->second.getDt();
               // n -= m;
            // }
            // if ((end > 0) && (i->second.getEndGPS() > end)) {
               // int d = (double (end - i->second.getStartGPS()) - 0.5) / 
                  // i->second.getDt() + 1;
               // n -= (int)i->second.getNFiles() - d;
            // }
            // if (n == 0) {
               // continue;
            // }
            // if (!suppress_head) {
               // cout << "file://";
            // }
            // cout << i->second.getFile (m);
            // if (n > 1) {
               // cout << " -c " << n - 1;
            // }
            // cout << endl;
         // }
      // }
      // else {
         // FrameDir::file_iterator stop = (end == 0) ? fdir.end() : fdir.getLast (end);
         // for (FrameDir::file_iterator i = fdir.getStart (beg); i != stop; ++i) {
            // if (!suppress_head) {
               // cout << "file://";
            // }
            // cout << i->getFile() << endl;
         // }
      // }
   }

