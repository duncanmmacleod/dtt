add_library(fantom SHARED
        dirio.cc
        dmtio.cc
        fantom.cc
        fchannel.cc
        fmsgq.cc
        fname.cc
        framemux.cc
        funcio.cc
        inetio.cc
        larsio.cc
        ndsio.cc
        sendsio.cc
        robotctrl.cc
        smartio.cc
        syncio.cc
        tapeio.cc
        )

target_link_libraries(fantom
        lsmp
        frameio
        framefast
        gdsbase
        daqs
        sockutil
        pthread
        ${READLINE_LIBRARIES}
        )

set_target_properties(fantom
        PROPERTIES
        VERSION 0.0.0
        PUBLIC_HEADER fantom.hh
        PRIVATE_HEADER
            "fantomtype.hh;fchannel.hh;dirio.hh;dmtio.hh;fmsgq.hh;fname.hh;framemux.hh;funcio.hh;inetio.hh;iosupport.hh;larsio.hh;ndsio.hh;refcount.hh;robotctrl.hh;sendsio.hh;smartio.hh;syncio.hh;tapeio.hh;udnls.hh"
        )

INSTALL_LIB(fantom ${INCLUDE_INSTALL_DIR}/gds/fantom)

add_executable(fantom_executable
        fantommain.cc)

set_target_properties(fantom_executable PROPERTIES OUTPUT_NAME fantom)

set_target_properties(fantom_executable
        PROPERTIES
        OUTPUT_NAME "fantom")

target_link_libraries(fantom_executable
#        fantom
#        frameio
#        framefast
#        sockutil
#        cmd_lib
#        util_lib
        awg
        #${OS_SPECIFIC_LIBS}
        #Rframeio
        framecpp8
        ${ROOT_LIBRARIES}
        )

add_executable(udnls
        udnls.cc)

target_link_libraries(udnls
        frameio
        framefast
        )

install(TARGETS udnls fantom_executable
        DESTINATION bin)