/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: framemux						*/
/*                                                         		*/
/* Module Description: frame multiplexer				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 6Oct00   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: framemux.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_FRAMEMUX_H
#define _LIGO_FRAMEMUX_H


#include "Time.hh"
#include "Interval.hh"
#include "framefast/frametype.hh"
#include "smartio.hh"
#include <string>
#include <queue>
#include <map>
#include <cstring>


namespace framefast {
   struct toc_t;
};

namespace fantom {

/** @name Frame Multiplexer
    This header defines the frame multiplexer (multiple-input /
    multiple-output) class.
   
    @memo Frame Multiplexer
    @author Written October 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

   class channelquery;


   /// Queue sort order (by channel name)
   class queuesort {
   public:
      /// ignore case
      bool operator () (const std::string& s1, 
                       const std::string& s2) const {
         return strcmp (s1.c_str(), s2.c_str()) < 0; }
   };

   /// Output buffer
   class outputbuffer : public framefast::data_t {
   public:
      /// Start time
      Time		fTime;
      /// Duration
      Interval		fInterval;
      /// Array full?
      //bool		fFull;
      /// filled so far
      int		fSoFar;
      /// partially filled in last point
      int		fPartialSoFar;
      /// partial data
      framefast::data_t	fPartialData;
      /// missing data?
      bool		fMissing;
   
      /// Create a new buffer
      outputbuffer (const Time& t, const Interval& i) 
      : fTime (t), fInterval (i), /*fFull (false),*/ fSoFar (0), 
      fPartialSoFar (0), fMissing (false) {
      }
      /// next time
      Time nexttime() {
         return fTime + fInterval; }
   };

   /// Queue of output buffers (single channel)
   class outputqueue : public std::queue<outputbuffer> {
   public:
      /// Create a new output queue
      outputqueue (const channelquery& query, int len);
      /// Add data to a queue
      bool addData (const Time& time, const framefast::data_t& data,
                   const Interval& offset, const Interval& duration);
      /// Write data to frame
      bool writeData (const Time& time, framefast::framewriter* fr);
      /// time of oldest buffer; false = no buffer
      bool ready (Time& t) const;
   
   protected:
      /// Data rate
      float 		fRate;
      /// Frame length
      int		fLength;
   };

   /// Output queues for all channels
   class channelqueue : 
   public std::map<std::string, outputqueue, queuesort> {
   public:
      /// Constructor
      channelqueue (int len) : fLength (len) {
      }
      /// Time of oldest buffer; false = no buffer
      bool ready (Time& t) const;
      /// find/add a channel
      outputqueue* getChannel (const char* name, 
                        const channelquery* query = 0);
      /// remove unused channels
      void channelPurge();
      /// Get frame length
      int frameLength () const {
         return fLength; }
   
   protected:
      /// Frame output length (in sec)
      int		fLength;
   };

   /// list of channel queues
   typedef std::deque<channelqueue> queuelist;


   /// Frame multiplexer
   class framemux {
   public:
      /// Constructor
      framemux (smart_ilist& in, smart_olist& out, const bool* ctrlC = 0);
      /// Destructor
      ~framemux ();
   
      /// input list
      smart_ilist& inp() {
         return *fIn; }
      /// output list
      smart_olist& out() {
         return *fOut; }
   
      /// Return oldest input frame
      framefast::framereader* oldest();
      /// Process one frame across all inputs; return processed duration
      double process ();
      /// Read a frame from input; makes sure a frame is available
      //bool readInput (int inum);
       /// Flush the frame from the output; time=0 flush all
      bool flushOutput (const Time& before = Time (0));
   
      /// Last message
      const char* Message () const {
         return fMsg.c_str(); }
      /// get clock
      Time clock() const {
         return fClock; }
      /// set clock
      void setClock (const Time& t) {
         fClock = t; }
      /// get stop time
      Time stop() const {
         return fStop; }
      /// set stop time
      void setStop (const Time& t) {
         fStop = t; }
   
   public:
      /// list of cache hits
      typedef std::vector<bool> cachehits;
      /// list of output query channels
      typedef std::vector<channelquery> cachequery;
      /// cache index entry
      struct cacheindexentry {
         /// List offset
         int			fOffset;
         /// Channel name
         const char* 		fName;
         /// TOC category: ADC, proc., sim., etc.
         int			fTocCategory;
         /// TOC index
         int			fTocIndex;
      };
      /// cache index
      typedef std::vector<cacheindexentry> cacheindex;
      /// output cache record
      struct outputcache {
         /// Cache hit, i.e., channel is used
         cachehits		fHits;
         /// Channel queries
         cachequery		fQueries;
         /// Index
         cacheindex		fIndex;
      };
      /// list if hits for each output
      typedef std::map<int, outputcache> cacheline;
      /// list of channnel names in input
      typedef std::vector<std::string> cachevalues;
      /// input cache record
      struct inputcache {
         /// Cached input channel names
         cachevalues		fValues;
         /// Channel used by any one or more output
         cachehits		fUsed;
         /// Channel selected by input channel list
         cachehits		fSelect;
         /// Index
         cacheindex		fIndex;
         /// quick-test counter
      	 int			fQuickTest;
         /// Cached output channel hits
         cacheline		fHits;
      };
      /// list of input cache records
      typedef std::map<int, inputcache> cachelist;
   
   protected:
      /// Debug
      int		fDebug;
      /// Control C active
      const bool*	fCtrlC;
      /// clock (current time)
      Time		fClock;
      /// stop time
      Time		fStop;
      /// List of inputs
      smart_ilist*	fIn;
      /// List of outputs
      smart_olist*	fOut;
      /// Message
      std::string	fMsg;
      /// List of queues
      queuelist		fQueues;
      /// Cache for selected channel names
      cachelist	fCache;
   
      /// Read a frame
      bool readData (int innum, framefast::framereader* fr,
                    const Interval& offset, const Interval& duration);
   
      /// Invalidate cache
      void InvalidateCache();
      /// Invalidate input cache
      void InvalidateInputCache(int i);
      /// Get cached hits
      outputcache* UpdateCache (int i, int o, bool firsti,
                        const framefast::toc_t* toc, 
                        const channelquerylist* iq, 
                        const channelquerylist* oq);
   };



//@}

}


#endif // _LIGO_FRAMEMUX_H


