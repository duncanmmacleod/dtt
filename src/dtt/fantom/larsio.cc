#include "sockutil.h"
#include "larsio.hh"
#include "framefast/frametype.hh"
#include "framefast/framefast.hh"
#include "fname.hh"
#include "pipe_exec.hh"
#include "fdstream.hh"
#include <time.h>
#include <ctype.h>
#include <sys/time.h>
#include <stdio.h>
#include <cstdlib>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/utsname.h>
#include <fcntl.h>
#include <cstring>

#define _TIMEOUT 10


namespace fantom {
   using namespace std;
   using namespace framefast;



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// constants		                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   const char* const kDFMPIPE = "DFMPIPE";
   const char* const kDFMSTART = "DFMSTART";
   const char* const kLIGOTOOLS = "LIGOTOOLS";

   const char* const kDfmCommand = "dfmstart";
   const char* const kLdaspwCommand = "ldaspw";
   //const char* const kLarsCommand = "dfmpipe";
   //const char* const kLarsExitString = "exit";

   const char* const kLarsRequest = 
   "udn %s\n"
   "times %lu-%lu\n"
   "channels {%s}\n"
   "output url\n"
   "go";

   const char* const kLarsLoginTest = 
   "udn %s\n"
   "times 6000000000-6000000001\n"
   "output stdout\n"
   "command null\n"
   "go";

   const char* const kLarsInfoRequest = 
   "udn //*\n"
   "udntype frame\n"
   "output stdout\n"
   "go";

   const char* const kLarsChannelRequest = 
   "udn %s/channels\n"
   "output stdout\n"
   "go";

   const char* const kLarsTimesRequest = 
   "udn %s/times\n"
   "output stdout\n"
   "go";


   const bool kLarsWithRate = false;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// forwards		                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static int DfmConnect (char *errmsg);
   static int DfSocketReceiveLineT (int sock, char *buf, 
                     int buflen, int timeout);


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// utility functions                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static string trim (const char* p)
   {
      while (isspace (*p)) p++;
      string s = p;
      while ((s.size() > 0) && isspace (s[s.size()-1])) {
         s.erase (s.size() - 1);
      }
      return s;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// simple_lars_namerecord                                               //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class simple_lars_namerecord : public namerecord {
   public:
      typedef static_ref_counter<lars_support> refcount;
      typedef dynamic_ref_counter<lars_support> refcounter;
      // Constructor
      simple_lars_namerecord (lars_support* lars, int n,
                        refcount* refcnt = 0) 
      : namerecord ("lars://"), fLars (lars), fStreamN (n),
      fRefCount (refcnt) {
      }
      // Get next URL
      virtual bool getNextName (std::string& name) {
         name = fLars->getFrameUrl (fStreamN);
         return !name.empty(); }
   
   protected:
      // Pointer to LARS support
      lars_support*	fLars;
      // Stream number
      int		fStreamN;
      // reference counter
      refcounter	fRefCount;
   };



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// lars_support		                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   lars_support::lars_support (const char* udn, const char* conf) 
   : fType (FF), fOpen (false), fStreamNum (-1), fLars (0), fSock (-1),
   fRefCount (this)
   {
      setUDN (udn);
      setConf (conf);
   }

//______________________________________________________________________________
   lars_support::~lars_support()
   {
      close();
   }

//______________________________________________________________________________
   void lars_support::setUDN (const char* udn)
   {
      fUDN = udn ? trim (udn) : string ("");
   }

//______________________________________________________________________________
   void lars_support::setConf (const char* conf)
   {
      // nothing
   }

//______________________________________________________________________________
   bool lars_support::selectChannels (const channelquerylist* chns)
   {
      if (chns) {
         fChannels = *chns;
      }
      else {
         fChannels.clear();
      }
      return true;
   }

//______________________________________________________________________________
   int lars_support::getFrameStreamNum ()
   {
      // check if connection is open
      if (!fOpen) {
         // if not open connection
         if (!open()) {
            close();
            cerr << "Unable to open connection" << endl;
            return 0;
         }
         // and send request
         if (!request()) {
            close();
            cerr << "Unable to send request" << endl;
            return 0;
         }
      }
      // check if request has been sent
      return fStreamNum;
   }

//______________________________________________________________________________
   std::string lars_support::getFrameUrl (int N)
   {
      if ((N < 0) || (N >= getFrameStreamNum()) || !fLars || !*fLars) {
         return "";
      }
      char line[4*1024];
      // cerr << "Get lars URL " << N << endl;
      // Don't use getline since it skips empty lines!
      while (*fLars && fLars->get (line, sizeof (line))) {
         fLars->get(); // eol
         string s = trim (line);
         // cerr << "answer from lars: " << s << endl;
         if (s.find ("http://") == 0) {
            return s;
         }
         if (s.empty() || (s.find ("Error") == 0)) {
            close();
            return "";
         }
      }
      return "";
   }

//______________________________________________________________________________
   namerecord* lars_support::getNameRecord (int N, bool refcount)
   {
      if ((N < 0) || (N >= getFrameStreamNum())) {
         return 0;
      }
      namerecord* nr = new (nothrow) simple_lars_namerecord (this, N, 
                           refcount ? &fRefCount : 0);
      return nr;
   }

//______________________________________________________________________________
   bool lars_support::eof() const
   {
      return fLars == 0;
   }

//______________________________________________________________________________
   bool lars_support::login (const char* udn, const char* uname, 
                     const char* pword)
   {
      // get UDN  
      if (!udn) {
         return false;
      }
      string u = trim (udn);
      if (u.find ("lars://") == 0) {
         u.erase (0, 7);
      }
      // check if request would work
      lars_support lars (u.c_str());
      return lars.login (uname, pword);
   }

//______________________________________________________________________________
   bool lars_support::login (const char* uname, const char* pword)
   {
      // set new default username/password
      if (uname && pword) {
         if (!setlogin (uname, pword)) {
            return false;
         }
      }
   
      // check if request would work
      if (!open()) {
         return false;
      }
      // send off request
      char line[4*1024];
      sprintf (line, kLarsLoginTest, fUDN.c_str());
      *fLars << line << endl;
      //cerr << "Lars login request" << endl << line << endl <<
         //"End of Lars request" << endl;
      if (!*fLars) {
         close();
         return false;
      }
      // Don't use getline since it skips empty lines! (older Sun CC)
      fLars->get (line, sizeof (line));
      fLars->get(); // eol
      string s = trim (line);
      //cerr << "answer from lars: " << s << endl;
      for (string::iterator c = s.begin(); c != s.end(); ++c) {
         *c = tolower (*c); 
      }
      close();
      // check for error
      bool failed = (s.find ("error") == 0) &&
         ((s.find ("unknown user") != string::npos) || 
         (s.find ("incorrect password") != string::npos));
      return !failed;
   }

//______________________________________________________________________________
   bool lars_support::getInfo (const char* udn, frametype& utype,
                     channellist& chns, dataseglist& dsegs)
   {
      if (!udn) {
         return false;
      }
      string u = trim (udn);
      if (u.find ("lars://") == 0) {
         u.erase (0, 7);
      }
      lars_support lars (u.c_str());
      if (strstr (u.c_str(), "/min/") != 0) {
         utype = framefast::MTF;
      }
      else if (strstr (u.c_str(), "/sec/") != 0) {
         utype = framefast::STF;
      }
      else if (strstr (u.c_str(), "/raw/") != 0) {
         utype = framefast::MTF;
      }
      else {
         utype = framefast::NONE;
         return false;
      }
      if (!lars.getUDNchns (chns)) {
         return false;
      }
      return lars.getUDNtimes (dsegs);
   }

//______________________________________________________________________________
   bool lars_support::getInfo (frametype& utype,
                     channellist& chns, dataseglist& dsegs)
   {
      bool ret = getInfo (fUDN.c_str(), utype, chns, dsegs);
      if (ret) {
         fType = utype;
      }
      return ret;
   }

//______________________________________________________________________________
   bool lars_support::getUDNList (udnlist& udns)
   {
      lars_support lars;
      return lars.getUDNs (udns);
   }

//______________________________________________________________________________
   bool lars_support::setlogin (const char* uname, const char* pword)
   {
      // set new default username/password
      if (!uname || !pword) {
         return false;
      }
      cout << "LARS LOGIN: username = " << 
         (uname ? uname : "<blank>") <<
         "  password = " << (pword ? pword : "<blank>") << 
         "  (" << fUDN << ")" << endl;
   
      // first look for environment
      string ldaspwstart;
      const char* dfmstart = getenv (kDFMSTART);
      if (dfmstart) {
         int i = strlen (dfmstart) - 1;
         while ((i >= 0) && (dfmstart[i] != '/')) --i;
         if (i >= 0) {
            ldaspwstart.assign (dfmstart, 0, i + 1);
            ldaspwstart += kLdaspwCommand;
         }
      }
      pipe_exec* ldaspw = 0;
      if (!ldaspwstart.empty()) {
         ldaspw = new (nothrow) pipe_exec (ldaspwstart.c_str(), "w");
         if (!*ldaspw) {
            delete ldaspw; ldaspw = 0;
         }
      }
      // second look for dfmstart in path
      if (!ldaspw) {
         ldaspw = new (nothrow) pipe_exec (kLdaspwCommand, "w");
         if (!*ldaspw) {
            delete ldaspw; ldaspw = 0;
         }
      }
      // third look for LIGOTOOLS environement
      if (!ldaspw) {
         const char* ligotools = getenv (kLIGOTOOLS);
         if (ligotools) {
            char startcmd[1024];
            sprintf (startcmd, "%s/bin/%s", ligotools, kLdaspwCommand);
            ldaspw = new (nothrow) pipe_exec (startcmd, "w");
            if (!*ldaspw) {
               delete ldaspw; ldaspw = 0;
            }
         }
      }
      if (!ldaspw) {
         cerr << "Unable to locate ldaspw script" << endl;
         return false;
      }
      // finally! set user name & password
      *ldaspw << uname << endl;
      *ldaspw << pword << endl;
      *ldaspw << "y" << endl; // make it default
      bool ret = !!*ldaspw && (ldaspw->wait (5.0, 0) > 0);
      delete ldaspw;
      //cerr << "setlogin was a " << (ret ? "success" : "failure") << endl;
      return ret;
   }

//______________________________________________________________________________
   bool lars_support::getUDNs (udnlist& udns)
   {
      if (!open()) {
         return false;
      }
      // send off request
      char line[4*1024];
      sprintf (line, kLarsInfoRequest);
      *fLars << line << endl;
      cerr << "Lars UDNs request" << endl << line << endl <<
         "End of Lars request" << endl;
      if (!*fLars) {
         close();
         return false;
      }
      // Don't use getline since it skips empty lines!
      while (*fLars && fLars->get (line, sizeof (line))) {
         fLars->get(); // eol
         string s = trim (line);
         // test for valid UDN
         if (s.find ("//") == 0) {
            s = string ("lars://") + s;
            string::size_type pos = s.find (" ");
            if (pos != string::npos) {
               s.erase (pos);
            }
            udns.push_back (s);
         }
         // check for end of answer
         else if (s.empty()) {
            close();
            return true;
         }
         // check for error
         else if (s.find ("Error") == 0) {
            close();
            return false;
         }
         // all other lines ignored
      }
      close();
      return true;
   }

//______________________________________________________________________________
   bool lars_support::getUDNchns (channellist& chns)
   {
      if (!open()) {
         return false;
      }
      // send off request
      char line[4*1024];
      sprintf (line, kLarsChannelRequest, fUDN.c_str());
      *fLars << line << endl;
      cerr << "Lars chn request" << endl << line << endl <<
         "End of Lars request" << endl;
      if (!*fLars) {
         close();
         return false;
      }
      // Don't use getline since it skips empty lines!
      while (*fLars && fLars->get (line, sizeof (line))) {
         fLars->get(); // eol
         string s = trim (line);
         // check for end of answer
         if (s.empty()) {
            close();
            SortChannels (chns);
            return true;
         }
         // check for error
         else if (s.find ("Error") == 0) {
            close();
            return false;
         }
         // test for valid channel
         else if (s[0] != '#') {
            string::size_type pos = s.find (" ");
            string chn (s, 0, pos);
            float rate = 0;
            if (pos != string::npos) {
               rate = atof (s.c_str() + pos);
            }
            chns.push_back (channelentry (chn.c_str(), rate));
         }
         // all other lines ignored
      }
      close();
      SortChannels (chns);
      return true;
   }

//______________________________________________________________________________
   bool lars_support::getUDNtimes (dataseglist& dsegs)
   {
      if (!open()) {
         return false;
      }
      // send off request
      char line[4*1024];
      sprintf (line, kLarsTimesRequest, fUDN.c_str());
      *fLars << line << endl;
      cerr << "Lars time request" << endl << line << endl <<
         "End of Lars request" << endl;
      if (!*fLars) {
         close();
         return false;
      }
      // Don't use getline since it skips empty lines!
      while (*fLars && fLars->get (line, sizeof (line))) {
         fLars->get(); // eol
         string s = trim (line);
         // check for end of answer
         if (s.empty()) {
            close();
            return true;
         }
         // check for error
         else if (s.find ("Error") == 0) {
            close();
            return false;
         }
         // test for valid channel
         else if (s[0] != '#') {
            string::size_type pos = s.find (" ");
            if (pos == string::npos) {
               continue;
            }
            Time start (strtoul (s.c_str(), 0, 10), 0);
            Time stop (strtoul (s.c_str() + pos + 1, 0, 10), 0);
            if (stop > start) {
               dsegs.insert (dataseglist::value_type 
                            (start, stop - start));
            }
         }
         // all other lines ignored
      }
      close();
      return true;
   }

//______________________________________________________________________________
   bool lars_support::open ()
   {
      // close first
      if (fOpen) {
         close();
      }
      char errmsg[1024];
      fSock = DfmConnect (errmsg);
      if (fSock < 0) {
         return false;
      }
      fLars = new fd_iostream (fSock);
      fOpen = true;
      fStreamNum = -1;
      return true;
   #if 0
      // 0 - unknown; 1 - successful; -1 failed
      static int dfmpipeWhere = 0;
      static char dfmpipeCmd[2048] = "";
   
      // close first
      if (fOpen) {
         close();
      }
      // previous attemp failed
      if (dfmpipeWhere == -1) {
         return false;
      }
      // know how to do it
      else if (dfmpipeWhere == 1) {
         fLars = new (nothrow) pipe_exec (dfmpipeCmd);
         fOpen = (fLars != 0);
         fStreamNum = -1;
         return fOpen;
      }
      // try it
      else {
         dfmpipeWhere = 1;
         // first look for environment
         const char* dfmpipe = getenv (kDFMPIPE);
         if (dfmpipe) {
            strncpy (dfmpipeCmd, dfmpipe, sizeof (dfmpipeCmd) - 1);
            if (open ()) {
               return true;
            }
         }
         // second look for dfmpipe in path
         strncpy (dfmpipeCmd, kLarsCommand, sizeof (dfmpipeCmd) - 1);
         if (open ()) {
            return true;
         }
         // third look for LIGOTOOLS environement
         dfmpipe = getenv (kLIGOTOOLS);
         if (dfmpipe) {
            sprintf (dfmpipeCmd, "%s/bin/%s", dfmpipe, kLarsCommand);
            if (open ()) {
               return true;
            }
         }
         // all failed give up
         dfmpipeWhere = -1;
         cerr << "Couldn't find dfmpipe program" << endl;
         return false;
      }
   #endif
   }

//______________________________________________________________________________
   void lars_support::close()
   {
      if (!fOpen) {
         return;
      }
      if (fLars) {
         // *fLars << endl << kLarsExitString << endl;
         timespec wait = {0, 10000000};
         nanosleep (&wait, 0);
         delete fLars;
         fLars = 0;
         ::close (fSock);
         fSock = -1;
      }
      fStreamNum = -1;
      fOpen = false;
      // cerr << "lars close done" << endl;
   }

//______________________________________________________________________________
   bool lars_support::request ()
   {
      if (!fOpen || fUDN.empty()) {
         return false;
      }
      if ((fT0 == Time (0, 0)) || (fDt <= Interval(0.))) {
         return false;
      }
   
      // determine GPS time
      Time::ulong_t start = fT0.getS();
      Time::ulong_t duration  = fDt.GetS();
      while (Time (start + duration, 0) < fT0 + fDt) {
         duration++;
      }
      Time::ulong_t stop = start + duration;
      // channel list
      string channels;
      if (fChannels.empty()) {
         channels = "*";
      }
      else {
         channels = fChannels.str (kLarsWithRate);
      }
      // send off request
      char* buf = new char [16*1024 + channels.size()];
      sprintf (buf, kLarsRequest, fUDN.c_str(),
              start, stop, channels.c_str());
      *fLars << buf << endl;
      // cerr << "Lars request" << endl << buf << endl <<
      //"End of Lars request" << endl;
      delete [] buf;
      if (!*fLars) {
         close();
         return false;
      }
      char c = fLars->peek();
      if (!*fLars || (c == 'E')) {
         char buf[1001] = {0};
         fLars->getline (buf, 1000);
         cerr << "LARS Negativc REPLY " << buf << endl;
         close();
         return false;
      }
      // cerr << "LARS ONE CHAR ANSWER " << c << endl;
   
      fStreamNum = 1;
      return true;
   }

//______________________________________________________________________________
   static int DfmConnect (char *errmsg)
   {
      int sock = 0;
      int status, nitems, ntry;
      int pid, key, clientPort, replyPort, monitorPort, httpPort;
      int echokey;
      char keybuf[80];
      char host[80];
      char tmpstr[256], cmd[1024];
      char *cptr;
      char homedir[256], registerFile[256], filename[256];
      FILE *fptr;
      char ldasuser[24], encpw[80];
      struct utsname unamestruct;
   
   /*------ Beginning of code ------*/
   
   /*-- Figure out our home directory --*/
      cptr = (char *) getenv( "HOME" );
      if ( cptr == NULL ) {
         strcpy( errmsg, "Error: environment variable HOME is not set" );
         return -1;
      }
      strcpy( homedir, cptr );
   
   /*-- Figure out our hostname --*/
      status = uname( &unamestruct );
      if ( status < 0 ) {
         strcpy( errmsg, "Error: Unable to determine our hostname" );
         return -2;
      }
      strcpy( host, unamestruct.nodename );
   
   /*-- Determine the location of the register file --*/
      strcpy( registerFile, homedir );
      strcat( registerFile, "/.dfmRegister." );
      strcat( registerFile, host );
   
   /*-- Try to connect to the dfm.  First assume it is already running; if
    this fails, start it in the background and try again --*/
      for (ntry=1; ntry<=3; ntry++) {
      
      /*-- On the first retry, start the dfm in the background --*/
         if ( ntry == 2 ) {
         /*-- Start up a "daemonized" dfm process in the background --*/
         
            // first look for environment
            const char* dfmstart = getenv (kDFMSTART);
            status = -1;
            if (dfmstart) {
               status = ::system (dfmstart);
            }
            // second look for dfmstart in path
            if (status != 0) {
               status = ::system (kDfmCommand);
            }
            // third look for LIGOTOOLS environement
            if (status != 0) {
               dfmstart = getenv (kLIGOTOOLS);
               if (dfmstart) {
                  char startcmd[1024];
                  sprintf (startcmd, "%s/bin/%s", dfmstart, kDfmCommand);
                  status = ::system (startcmd);
               }
            }
            if (status != 0) {
               strcpy( errmsg, "Error: Unable to start dfm in the background" );
               return -3;
            }
         }
      
      /*-- If this is the second retry, then the first retry wasn't
      successful, so return with an error --*/
         if ( ntry == 3 ) {
            strcpy( errmsg, "Error: Unable to connect to dfm, even after trying to restart it" );
            return -4;
         }
      
      /*-- Open and read the register file --*/
         fptr = fopen( registerFile, "r" );
         if ( fptr == NULL ) { 
            continue; }
         nitems = fscanf( fptr, "%d %d %d %d %d %d", &pid, &key,
                        &clientPort, &replyPort, &monitorPort, &httpPort );
         if ( nitems < 6 ) { 
            continue; }
         fclose( fptr );
      
      /*-- Connect to the dfm's client socket --*/
         sock = ::socket (PF_INET, SOCK_STREAM, 0);
         if (sock == -1) {
            continue;
         }
         struct sockaddr_in name;		/* socket name */
         name.sin_family = AF_INET;
         name.sin_port = htons (clientPort);
         if (nslookup ("localhost", &name.sin_addr) < 0) {
            ::close (sock);
            continue;
         }
      /*-- connect to the web server --*/
	 // timeval timeout = {_TIMEOUT, 0};
	 wait_time timeout = _TIMEOUT;
         if (connectWithTimeout (sock, (struct sockaddr *) &name, 
                              sizeof (name), timeout) < 0) {
            ::close (sock);
            continue;
         }
      
      /*-- Ask the dfm to report the value of its key --*/
         char cmd[256];
         strcpy (cmd, "EchoIdentityKey\n");
         status = ::send (sock, cmd, strlen (cmd), 0);
         if (status == -1) { 
            close( sock ); 
            continue; }
      
         status = DfSocketReceiveLineT( sock, keybuf, sizeof(keybuf), 
                              _TIMEOUT);
         if ( status != 0 ) { close( sock ); 
            continue; }
      
      /*-- Check the value of the key --*/
         nitems = sscanf( keybuf, "%d", &echokey );
         if ( nitems < 1 ) { close(sock); 
            continue; }
         if ( echokey != key ) { close(sock); 
            continue; }
      
      /*-- If we get here, then the dfm is alive and well! --*/
         break;
      }
   
   /*-- Get the LDAS username and password out of the ~/.ldaspw file --*/
      sprintf( filename, "%s/.ldaspw", homedir );
      fptr = fopen( filename, "r" );
      if ( fptr == NULL ) {
         // strcpy( errmsg, "Error: You need to run the 'ldaspw' utility first" );
         // close(sock);
         // return -5;
         strcpy (ldasuser, "anonymous");
         strcpy (encpw, "47AE9D4868258BD71B9D8C9D14BE038B");
         nitems = 2;
      } 
      else {
         nitems = fscanf( fptr, "%s %s ", ldasuser, encpw );
      }
      if ( nitems < 2 ) {
         strcpy( errmsg, "Error: While reading username & password from ~/.ldaspw");
         fclose( fptr ); 
         close(sock);
         return -6;
      }
      fclose( fptr );
   
   /*-- Assemble the user info to send to the dfm --*/
      cptr = (char *) getenv( "USER" );
      if ( cptr != NULL ) {
         sprintf( cmd, "hostname %s logname %s\n", host, cptr );
      } 
      else {
         sprintf( cmd, "hostname %s logname unknown\n", host );
      }
   
      sprintf( tmpstr, "program fantom procid %d\n", int(getpid()) );
      strcat( cmd, tmpstr );
   
      sprintf( tmpstr, "name %s password %s\n", ldasuser, encpw );
      strcat( cmd, tmpstr );
   
   /*-- Send the user info to the dfm --*/
      status = ::send (sock, cmd, strlen (cmd), 0);
      if (status == -1) {
         strcpy( errmsg, "Error: While sending command to dfm" );
         close(sock);
         return -7;
      }
   
      return sock;
   }

//______________________________________________________________________________
   static int DfSocketReceiveLineT (int sock, char *buf, int buflen, 
                     int timeout)
   /* Receive a line (up to a newline) from a socket, subject to a timeout
   expressed in seconds.  Returns 0 if a line was successfully received,
   1 if the buffer size was reached, 2 if the receive operation timed out
   before receiving a complete line (or end-of-file), 3 if an error occurred
   while setting blocking mode.
   */
   {
      int flags;
      int nbytes, totbytes, left;
      int itime;
      char *cptr;
   
   /*-- Make sure the timeout is nonnegative --*/
      if ( timeout < 0 ) { timeout = 0; }
   
   /*-- Set the socket to non-blocking mode (unless it already is) --*/
      flags = fcntl( sock, F_GETFL );
      if ( (flags & O_NONBLOCK) == 0 ) {
         if ( fcntl( sock, F_SETFL, (flags|O_NONBLOCK) ) < 0 ) { 
            return 3; }
      }
   
   /*-- Get the reply into the buffer, one character at a time, until we
    encounter a newline --*/
      totbytes = 0;
      cptr = buf;
      left = buflen - 1;
   
      itime = 0;
      while ( itime <= timeout ) {
      /*printf( "-------- itime = %d\n", itime );*/
         while ( (nbytes = recv( sock, cptr, 1, 0 )) > 0 && *cptr != '\n') {
         /*printf( "-------- nbytes = %d\n", nbytes );*/
            totbytes += nbytes;
            cptr += nbytes;
            left -= nbytes;
         }
      /*-- If we got a newline, then we're done --*/
         if ( *cptr == '\n' ) { 
            break; }
      
      /*-- If we get here, then recv() returned an error.  See if it is a real
      error, i.e. not just because no data is currently available. --*/
      /*printf( "-------- nbytes = %d, errno=%d\n", nbytes, errno );*/
         if ( errno != EWOULDBLOCK ) { 
            break; }
      
      /*-- Sleep before trying to receive more data --*/
         sleep( 1 );
         itime++;
      }
   
   /*-- Remove the final newline, if any, and null-terminate the output --*/
      if ( *cptr == '\n' ) { *cptr = '\0'; }
      buf[totbytes] = '\0';
   
   /*-- Set the socket back to its original mode, if necessary --*/
      if ( (flags & O_NONBLOCK) == 0 ) {
         if ( fcntl(sock, F_SETFL, flags) < 0 ) { 
            return 3; }
      }
   
      if ( itime > timeout ) {
         return 2;  /* Timeout */
      } 
      else if ( totbytes >= buflen-1 ) {
         return 1;  /* Buffer full */
      } 
      else {
         return 0;  /* Success */
      }
   }
}
