/* version $Id: ldxprog.cc 6319 2010-09-17 18:06:52Z james.batch@LIGO.ORG $ */
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// ldxprog							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "ldxprog.hh"
#include "TLGProgressBar.hh"
#include <TTimer.h>
#include <TVirtualX.h>


namespace lidax {
   using namespace std;
   using namespace ligogui;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGLidaxProgress (progress window)				        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGLidaxProgress::TLGLidaxProgress (const TGWindow* p, 
                     const TGWindow* main, LidaxProgress& prog) 
   : TLGTransientFrame (p, main, 10, 10), fProg (&prog),
   fCompletion (0), fMsg (prog.GetMsg())
   {
      fL[0] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 15, 15, 20, 5);
      fL[1] = new TGLayoutHints (kLHintsLeft | kLHintsTop |
                           kLHintsExpandX, 15, 15, 0, 20);
      fL[2] = new TGLayoutHints (kLHintsLeft | kLHintsTop |
                           kLHintsExpandX, 120, 120, 0, 0);
      fL[3] = new TGLayoutHints (kLHintsLeft | kLHintsTop |
                           kLHintsExpandX, 0, 0, 0, 0);
      fL[4] = new TGLayoutHints (kLHintsLeft | kLHintsTop |
                           kLHintsExpandX, 15, 15, 0, 10);
      fBar = new TLGProgressBar (this, 350, 25, fCompletion);
      AddFrame (fBar, fL[0]);
      fF[0] = new TGHorizontalFrame (this, 10, 10);
      AddFrame (fF[0], fL[1]);
      fStatus = new TGLabel (fF[0], fMsg.c_str());
      fF[0]->AddFrame (fStatus, fL[3]);
      fF[1] = new TGHorizontalFrame (this, 10, 10);
      AddFrame (fF[1], fL[4]);
      fAbortButton = new TGTextButton (fF[1], new TGHotString 
                           ("   &Abort   "), 0);
      fAbortButton->Associate (this);
      fF[1]->AddFrame (fAbortButton, fL[2]);
   
      // set dialog box title
      SetWindowName ("LiDaX Progress");
      SetIconName ("LiDaX Progress");
      SetClassHints ("LiDaXProgDlg", "LiDaXProgDlg");
      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                              (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
                              (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                              ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
      // make the message box non-resizable
      SetWMSize (width, height);
      SetWMSizeHints (width, height, width, height, 0, 0);
      SetMWMHints (kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
                  kMWMDecorMenu,
                  kMWMFuncAll | kMWMFuncResize | kMWMFuncMaximize,
                  kMWMInputModeless);
   
      MapWindow();
      // create timer & arm
      fTimer = new TTimer (this, 1000, kTRUE);
      fTimer->TurnOn();
      fTimer->Reset();
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   TLGLidaxProgress::~TLGLidaxProgress () 
   {
      delete fTimer;
      delete fStatus;
      delete fBar;
      delete fAbortButton;
      delete fF[0];
      delete fF[1];
      for (int i = 0; i < 5; i++) {
         delete fL[i];
      }
   }

//______________________________________________________________________________
   Bool_t TLGLidaxProgress::HandleTimer (TTimer* timer)
   {
      if (fProg->IsCancel()) {
         fTimer->TurnOff();
         CloseWindow();
         return kFALSE;
      }
      else {
         float c = fProg->GetCompletion();
         string s = fProg->GetMsg();
         if ((c != fCompletion) || (s != fMsg)) {
            fCompletion = c;
            fMsg = s;
            fStatus->SetText (new TGString (s.c_str()));
            fClient->NeedRedraw (fBar);
            fClient->NeedRedraw (fStatus);
            DoRedraw();
         }
         fTimer->Reset();
         return kTRUE;
      }
   }

//______________________________________________________________________________
   void TLGLidaxProgress::CloseWindow()
   {
      DeleteWindow();
   }

//______________________________________________________________________________
   Bool_t TLGLidaxProgress::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         switch (parm1) {
            // cancel
            case 0:
               {
                  fProg->SetCancel();
                  DeleteWindow();
                  break;
               }
            // dismiss
            case 1:
               {
                  DeleteWindow();
                  break;
               }
         }
      }
      return kTRUE;
   }



}
