/* version $Id: ldxreport.cc 7582 2016-03-01 22:25:48Z john.zweizig@LIGO.ORG $ */
/* -*- mode: c++; c-basic-offset: 3; -*- */
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// lidax							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "ldxreport.hh"
#include "ldxprog.hh"
#include "XsilLidax.hh"
#include "fdstream.hh"
#include "pipe_exec.hh"
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>

namespace lidax {
   using namespace std;
   using namespace dfm;
   using namespace fantom;


   const char* const kSendMail = "/usr/lib/sendmail -t";

   const char* const htmlheader =
   "<!doctype html public \"-//w3c//dtd html 4.0 transitional//en\">\n"
   "<html>\n"
   "<head>\n"
   "   <meta http-equiv=\"Content-Type\" content=\"text/html; "
   "charset=iso-8859-1\">\n"
   "   <meta http-equiv=\"Pragma\" content=\"no-cache\">\n"
   "%s"
   "   <meta name=\"rating\" content=\"General\">\n"
   "   <meta name=\"objecttype\" content=\"Manual\">\n"
   "   <meta name=\"description\" content=\"LiDaX summary\">\n"
   "   <meta name=\"GENERATOR\" content=\"LiDaX/fantom\">\n"
   "   <title>LIGO data access summary</title>\n"
   "</head>\n"
   "<body text=\"#000000\" bgcolor=\"#FFFFFF\" link=\"#296E77\" "
   "vlink=\"#551A8B\" alink=\"#FF0000\">\n"
   "&nbsp;\n"
   "<table BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH=\"99%%\" "
   "BGCOLOR=\"#FFFFFF\" >\n"
   "<tr>\n"
   "<td ALIGN=LEFT VALIGN=TOP>\n"
   "<h2><font face=\"arial,helvetica\">"
   "LIGO Data Access Summary</font></h2>\n"
   "</tr>\n"
   "<tr>\n"
   "<td ALIGN=LEFT VALIGN=TOP>";
   const char* const htmlreload =
   "   <meta http-equiv=\"Refresh\" content=\"20\">\n";
   const char* const htmltable =
   "<p>\n"
   "<table BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH=\"99%%\" >\n"
   "<tr>\n"
   "<td VALIGN=TOP BGCOLOR=\"#006666\"><b>"
   "<font face=\"arial,helvetica\">"
   "<font color=\"#FFFFFF\"><font size=+1>\n"
   "%s\n"
   "</font></font></font></b></td>\n"
   "<td ALIGN=RIGHT VALIGN=TOP WIDTH=\"50%%\" BGCOLOR=\"#006666\">\n"
   "<b><font face=\"arial,helvetica\">"
   "<font color=\"#FFFF00\"><font size=+1>\n"
   "%s\n"
   "</font></font></font></b></td>\n"
   "</tr>\n"
   "<tr>\n"
   "<td VALIGN=TOP COLSPAN=\"2\" BGCOLOR=\"#CCEEEE\">\n"
   "%s\n"
   "</td>\n"
   "</tr>\n"
   "</table>";
   const char* const htmlfooter =
   "</td>\n"
   "</tr>\n"
   "</table>\n"
   "</body>\n"
   "</html>";



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// report							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   lidax_report::lidax_report (LidaxParam& param, LidaxProgress& prog,
                     int fd_std)
   : fParam (param), fProgress (prog), fStart (Now()), fInpbytes (0), 
   fOutbytes (0), fInFiles (0), fOutFiles (0), flogid (-1), log (0)
   {
      if (fParam.fLogOn) {
         flogid = fParam.fLog.empty() ? fd_std : 
	   ::open (fParam.fLog.c_str(), O_CREAT | O_WRONLY, 020);
         log = new fd_ostream (flogid);
         *log << "LIGO DATA ACCESS LOG" << endl;
         *log << writeDate (fStart) << endl;
         *log << endl;
         writeSetup (true, *log);
         writeSetup (false, *log);
         *log << endl;
      }
      if (param.fWebOn) {
         writeWeb ("init");
      }
   }

//______________________________________________________________________________
   lidax_report::~lidax_report()
   {
      update();
      fStop = Now();
      fDuration = fStop - fStart;
   
      // finish log
      if (fParam.fLogOn) {
         *log << endl;
         *log << (fProgress.IsCancel() ? "Aborted" : "Completed") 
            << " at " << writeDate (fStop) << " (" << fDuration.GetS() 
            << " sec)" << endl;
         *log << "Read   : " << setw (5) << fInFiles << " files, " 
            << setw (8) << writeBytes (fInpbytes) << setw (0) << endl;
         *log << "Written: " << setw (5) << fOutFiles << " files, " 
            << setw (8) << writeBytes (fOutbytes) << setw (0) << endl;
         *log << endl;
      }
      // finish web
      if (fParam.fWebOn) {
         writeWeb ("finished");
      }
      // send e-mail
      if (fParam.fEmailOn && !fParam.fEmail.empty()) {
         writeEmail ();
      }
      // cleanup
      if (flogid >= 0) ::close (flogid);
      if (log) delete log;
   }

//______________________________________________________________________________
   void lidax_report::update ()
   {
      fantom::fmsgqueue logq;
      if (fParam.fDacc->inlog (logq)) {
         fantom::fmsgqueue::fmsg msg; 
         while (logq.pop (msg)) {
            fInpbytes += (long long)msg.param(0);
            fInFiles++;
            if (fParam.fLogOn) {
               *log << "Input " << (int)msg.param(3) << " (" 
                  << writeDate (msg.time()) << ", " << (int)msg.param(0) 
                  << "): " << msg.msg() << endl;
            }
            if (fParam.fWebOn) {
               fInLast.push (msg);
               while (fInLast.size() > 10) {
                  fantom::fmsgqueue::fmsg temp;
                  fInLast.pop(temp);
               }
            }
         }
      }
      if (fParam.fDacc->outlog (logq)) {
         fantom::fmsgqueue::fmsg msg; 
         while (logq.pop (msg)) {
            fOutbytes += (long long)msg.param(0);
            fOutFiles++;
            if (fParam.fLogOn) {
               *log << "Output " << msg.param(3) << " (" 
                  << writeDate (msg.time()) << ", " << (int)msg.param(0) 
                  << "): " << msg.msg() << endl;
            }
            if (fParam.fWebOn) {
               fOutLast.push (msg);
               while (fOutLast.size() > 10) {
                  fantom::fmsgqueue::fmsg temp;
                  fOutLast.pop(temp);
               }
            }
         }
      }
      if (fParam.fWebOn) {
         writeWeb ("status");
      }
   }

//______________________________________________________________________________
   void lidax_report::writeSetup (bool inp, ostream& os, bool web)
   {
      // get server information
      const selservers& sel = 
         inp ? fParam.fDacc->sel() : fParam.fDacc->dest();
      selserverlist serv;
      if (sel.isMultiple()) {
         serv = sel.selectedM();
      }
      else {
         serv.push_back (sel.selectedS());
      }
      if (inp) {
         if (web) os << "<b>";
         os << "Request" << endl;
         if (web) os << "</b>";
         if (web) os << "<br>&nbsp;&nbsp;";
         // start time
         os << "  GPS time: " << sel.selectedTime().getS() << " ("
            << writeDate (sel.selectedTime()) << ")" << endl;
         // duration
         if (web) os << "<br>&nbsp;&nbsp;";
         os << "  Duration: " << (double)sel.selectedDuration() 
            << " sec" << endl;
      }
      // input/output channel number
      if (web) os << "<br><b>";
      os << (inp ? "Input" : "Output");
      if (web) os << "</b>";
      os << " (" << serv.size() << " channel" 
         << (serv.size() == 1 ? "" : "s") << ")" << endl;
      // list of channels
      int n = 0;
      for (const_selserveriter i = serv.begin(); 
          i != serv.end(); ++i, ++n) {
         // server name
         if (web) os << "<br>&nbsp;&nbsp;";
         os << "  " << setw (2) << n << setw (0) << " " <<
            (const char*)i->getName() << endl;
         // list of UDNs
         for (const_UDNiter j = i->getUDN().begin();
             j != i->getUDN().end(); ++j) {
            if (web) os << "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            os << "     UDN: " << (const char*)j->first << endl;
         }
         // output frame format
         if (!inp) {
            if (web) os << "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            os << "     Format: " << i->format() << endl;
         }
      }
   }

//______________________________________________________________________________
   void lidax_report::writeWeb (const char* wtype)
   {
      // check if to update
      bool finished = true;
      bool init = false;
      if (wtype && (strcmp (wtype, "init") == 0)) {
         fLastUpdate = Time (0, 0);
         init = true;
         finished = false;
      }
      else if (wtype && (strcmp (wtype, "finished") != 0)) {
         Interval last = Now() - fLastUpdate;
         if (last < Interval (10.0)) {
            return;
         }
         fLastUpdate = Now();
         finished = false;
      }
      // write web page
      ofstream web (fParam.fWeb.c_str());
      if (!web) {
         return;
      }
      char buf[128 * 1024];
      sprintf (buf, htmlheader, finished ? "" : htmlreload);
      web << buf << endl;
      if (init) {
         // nothing
      }
      else if (finished) {
         ostringstream out;
         out << "<b>Time</b>" << endl;
         out << "<br>&nbsp;&nbsp; Started at " << writeDate (fStart) 
            << endl;
         out << "<br>&nbsp;&nbsp; Finished at " << writeDate (fStop) 
            << " (" << fDuration.GetS() << " sec)" << endl;
         out << "<br>" << endl;
         out << "<b>Data</b>" << endl;
         out << "<br>&nbsp;&nbsp; Read   : " << setw (5) << fInFiles 
            << " files, " << setw (8) << writeBytes (fInpbytes) 
            << setw (0) << endl;
         out << "<br>&nbsp;&nbsp; Written: " << setw (5) << fOutFiles 
            << " files, " << setw (8) << writeBytes (fOutbytes) 
            << setw (0) << endl;
         sprintf (buf, htmltable, 
                 (fProgress.IsCancel() ? "Aborted" : "Completed"), 
                 writeDate (fStop).c_str(), out.str().c_str());
         web << buf << endl;
         ostringstream out2;
         writeSetup (true, out2, true);
         writeSetup (false, out2, true);
         sprintf (buf, htmltable, "Setup", 
                 "&nbsp;", out2.str().c_str());
         web << buf << endl;
      }
      // status update
      else {
         // Status summary
         Time current = Now();
         Interval duration = current - fStart;
         ostringstream out;
         out << "<b>Time</b>" << endl;
         out << "<br>&nbsp;&nbsp; Started at " << writeDate (fStart) 
            << endl;
         out << "<br>&nbsp;&nbsp; Currently at " << writeDate (current) 
            << " (" << duration.GetS() << " sec)" << endl;
         out << "<br>" << endl;
         out << "<b>Data</b>" << endl;
         out << "<br>&nbsp;&nbsp; Read   : " << setw (5) << fInFiles 
            << " files, " << setw (8) << writeBytes (fInpbytes) 
            << setw (0) << endl;
         out << "<br>&nbsp;&nbsp; Written: " << setw (5) << fOutFiles 
            << " files, " << setw (8) << writeBytes (fOutbytes) 
            << setw (0) << endl;
         sprintf (buf, htmltable, "Status", 
                 writeDate (current).c_str(), out.str().c_str());
         web << buf << endl;
            // Last few processed files
         ostringstream out2;
         fantom::fmsgqueue logq (fInLast);
         fantom::fmsgqueue::fmsg msg; 
         out2 << "<b>Input</b>" << endl;
         while (logq.pop (msg)) {
            out2 << "<br>&nbsp;&nbsp; " << (int)msg.param(3) << " (" 
               << writeDate (msg.time()) << ", " << (int)msg.param(0) 
               << "): " << msg.msg() << endl;
         }
         logq = fOutLast;
         out2 << "<br>" << endl;
         out2 << "<b>Output</b>" << "&nbsp;" << endl;
         while (logq.pop (msg)) {
            out2 << "<br>&nbsp;&nbsp; " << (int)msg.param(3) << " (" 
               << writeDate (msg.time()) << ", " << (int)msg.param(0) 
               << "): " << msg.msg() << endl;
         }
         sprintf (buf, htmltable, "Most recent", 
                 "&nbsp;", out2.str().c_str());
         web << buf << endl;
      }
      web << htmlfooter << endl;
   }

//______________________________________________________________________________
   void lidax_report::writeEmail ()
   {
      pipe_exec pe (kSendMail);
      if (!pe) {
         return;
      }
      pe << "Organization: LIGO / LIDAX" << endl;
      pe << "To: " << fParam.fEmail << endl;
      pe << "MIME-Version: 1.0" << endl;
      pe << "Subject: Lidax " << 
         (fProgress.IsCancel() ? "aborted" : "completed") << endl;
      pe << "Content-Type: text/plain; charset=us-ascii" << endl;
      pe << "Content-Transfer-Encoding: 7bit" << endl;
      pe << endl;
      pe << "LIGO DATA ACCESS SUMMARY" << endl;
      pe << writeDate (fStart) << endl;
      pe << endl;
      writeSetup (true, pe);
      writeSetup (false, pe);
      pe << endl;
      pe << (fProgress.IsCancel() ? "Aborted" : "Completed") 
         << " at " << writeDate (fStop) << " (" << fDuration.GetS() 
         << " sec)" << endl;
      pe << "Read   : " << setw (5) << fInFiles << " files, " 
         << setw (8) << writeBytes (fInpbytes) << setw (0) << endl;
      pe << "Written: " << setw (5) << fOutFiles << " files, " 
         << setw (8) << writeBytes (fOutbytes) << setw (0) << endl;
      pe << endl;
      pe.close();
      if (pe) pe.wait();
   }

//______________________________________________________________________________
   string lidax_report::writeDate (const Time& t) 
   {
      char tbuf[256];
      TimeStr (t, tbuf, "%Y, %M %d, %H:%N:%S");
      return tbuf;
   }

//______________________________________________________________________________
   string lidax_report::writeBytes (long long b) 
   {
      b /= 1024;
      char buf[256];
      if (b >= 20000 * 1024) {
         sprintf (buf, "%i GB", (int)(b / (1024 * 1024)));
         return buf;
      }
      else if (b >= 20000) {
         sprintf (buf, "%i MB", (int)(b / 1024));
         return buf;
      }
      else {
         sprintf (buf, "%i kB", (int)(b));
         return buf;
      }
   }



}
