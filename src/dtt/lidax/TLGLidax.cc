/* version $Id: TLGLidax.cc 7155 2014-08-23 02:55:50Z john.zweizig@LIGO.ORG $ */
//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGLidax							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TLGLidax.hh"
#include "TLGDfmSel.hh"
#include "TLGDfmMonitors.hh"
#include "ldxtype.hh"
#include "XsilLidax.hh"
#include "TLGEntry.hh"
#include <TSystem.h>
#include <TApplication.h>
#include <TGMsgBox.h>
#include <TVirtualX.h>
#include <fstream>


namespace lidax {
   using namespace std;
   using namespace ligogui;
   using namespace dfm;


   const char* const aboutmsg = 
   "LIGO))) Laser Interferometer Gravitational-wave Observatory\n"
   "LIGO Data Access (LiDaX)\n\n"
   "by Daniel Sigg et al., 2000 - 2019, copyright\n"
   "version " CDS_VERSION "\n\n"
   "http://www.ligo.caltech.edu\n"
   "http://git.ligo.org/cds/dtt";


   static const char* const gWebTypes[] = { 
   "Web files", "*.html",
   "All files", "*",
   0, 0 };

   static const char* const gLogTypes[] = { 
   "Log files", "*.log",
   "Text files", "*.txt",
   "All files", "*",
   0, 0 };

   const Long_t kX11WatchdogInterval = 120*1000; // 2 minutes



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGLidaxProgLayout						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class TLGLidaxProgLayout : public TGLayoutManager {
   public:
      TLGLidaxProgLayout (TGCompositeFrame* p, Int_t lines) 
      : fMain (p), fLines (lines) {
         fList = fMain->GetList(); }
      virtual void Layout ();
      virtual TGDimension GetDefaultSize() const {
         return TGDimension (545, 25 + 25 * fLines); }
   
   protected:
      TGCompositeFrame*	fMain;
      TList*		fList;
      Int_t 		fLines;
   };

//______________________________________________________________________________
   void TLGLidaxProgLayout::Layout ()
   {
      const Int_t x[] = {10, 100, 450};
      const Int_t w[] = {80, 330, 80};
      const Int_t y[] = {0, 25, 50, 75, 100};
      TGFrameElement* ptr;
      Int_t col = 0;
      Int_t row = 0;
      TIter next (fList);
      while ((ptr = (TGFrameElement*) next())) {
         Int_t ww = w[col];
         if (col == 0) {
            ww = ptr->fFrame->GetWidth();
         }
         ptr->fFrame->MoveResize (x[col], y[row]+20, ww, 22);
         if (++col >= 3) {
            row++; col = 0;
         }
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGLidaxDialog (main window)					        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGLidaxDialog::TLGLidaxDialog (const TGWindow* p, 
                     const TGWindow* main, LidaxParam& prm, Int_t& ret)
   : TGTransientFrame (p, main, 10, 10), fOk (&ret), fParam (&prm),
   fX11Watchdog (0) 
   {
      // layout hints
      fL[0] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsTop, 8, 8, 8, 8);
      fL[1] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsBottom, 10, 10, 0, 10);
      fL[2] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 8, 8, 8, 8);
      fL[3] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 0, 0, 0, 0);
      fL[4] = new TGLayoutHints (kLHintsLeft | kLHintsTop |
                           kLHintsExpandY, 0, 0, 0, 0);
      fL[5] = new TGLayoutHints (kLHintsRight | kLHintsTop, 3, 3, 3, 3);
      fL[6] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 0, 0, 0);
      fL[7] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsBottom, 10, 10, 0, 
                           fParam->fDMTEnable ? 132 : 218);
      fL[8] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsBottom, 10, 10, 0, 63);
      // toplevel frames
      fF[0] = new TGHorizontalFrame (this, 10, 10);
      AddFrame (fF[0], fL[3]);
      fF[1] = new TGVerticalFrame (fF[0], 10, 10);
      fF[0]->AddFrame (fF[1], fL[3]);
      fF[2] = new TGVerticalFrame (fF[0], 10, 10);
      fF[0]->AddFrame (fF[2], fL[4]);
      // Source (input selection)
      fSource = new TLGDfmSelection (fF[1], *fParam->fDacc, true, 0, 
                           kLidaxSource, true, true, true);
      fSource->Associate (this);
      fF[1]->AddFrame (fSource, fL[2]);
      // Destination (output selection)
      if (fParam->fOutEnable) {
         fDestination = new TLGDfmSelection (fF[1], *fParam->fDacc, false, 0, 
                              kLidaxDestination, true, true, false);
         fDestination->Associate (this);
         fF[1]->AddFrame (fDestination, fL[2]);
         // Couple them
         fSource->Couple (fDestination);
         fDestination->Couple (fSource);
         // Progress
         fProg =  new TGGroupFrame (fF[1], "Progress");
         fF[1]->AddFrame (fProg, fL[0]);
         fProg->SetLayoutManager (new TLGLidaxProgLayout (fProg, 4));
         fLogFileSel = new TGCheckButton (fProg, "Log file:", kLidaxLogSel);
         fLogFileSel->Associate (this);
         fProg->AddFrame (fLogFileSel, fL[0]);
         fLogFile = new TLGTextEntry (fProg, "lidax.log", kLidaxLogfile);
         fLogFile->Associate (this);
         fProg->AddFrame (fLogFile, fL[0]);
         fLogFileChoose = new TGTextButton (fProg, "Choose...", 
                              kLidaxLogfileChoose);
         fLogFileChoose->Associate (this);
         fProg->AddFrame (fLogFileChoose, fL[0]);
         fWebPageSel = new TGCheckButton (fProg, "Web page:", kLidaxWebSel);
         fWebPageSel->Associate (this);
         fProg->AddFrame (fWebPageSel, fL[0]);
         fWebPage = new TLGTextEntry (fProg, "lidax.html", kLidaxWebfile);
         fWebPage->Associate (this);
         fProg->AddFrame (fWebPage, fL[0]);
         fWebPageChoose = new TGTextButton (fProg, "Choose...", 
                              kLidaxWebfileChoose);
         fWebPageChoose->Associate (this);
         fProg->AddFrame (fWebPageChoose, fL[0]);
         fEmailSel = new TGCheckButton (fProg, "e-mail:", kLidaxEmailSel);
         fEmailSel->Associate (this);
         fProg->AddFrame (fEmailSel, fL[0]);
         fEmail = new TLGTextEntry (fProg, "user@ligo.caltech.edu", kLidaxEmail);
         fEmail->Associate (this);
         fProg->AddFrame (fEmail, fL[0]);
         fLabel[2] = new TGLabel (fProg, "");
         fProg->AddFrame (fLabel[2], fL[0]);
         fDialogSel = new TGCheckButton (fProg, "Dialog box", kLidaxDlgSel);
         fDialogSel->Associate (this);
         fProg->AddFrame (fDialogSel, fL[0]);
         fLabel[0] = new TGLabel (fProg, "");
         fProg->AddFrame (fLabel[0], fL[0]);
         fLabel[1] = new TGLabel (fProg, "");
         fProg->AddFrame (fLabel[1], fL[0]);
      }
      else {
         fDestination = 0;
         fProg = 0;
         fLogFileSel = 0;
         fLogFile = 0;
         fLogFileChoose = 0;
         fWebPageSel = 0;
         fWebPage = 0;
         fWebPageChoose = 0;
         fEmailSel = 0;
         fEmail = 0;
         fDialogSel = 0;
         fLabel[0] = 0;
         fLabel[1] = 0;
         fLabel[2] = 0;
      }
      // Buttons
      fAboutButton = new TGTextButton (fF[2], " ? ", kLidaxAbout);
      fAboutButton->Associate (this);
      fF[2]->AddFrame (fAboutButton, fL[5]);
      fCancelButton = new TGTextButton (fF[2], new TGHotString 
                           ("     &Cancel     "), kLidaxCancel);
      fCancelButton->Associate (this);
      fF[2]->AddFrame (fCancelButton, fL[1]);
      fOkButton = new TGTextButton (fF[2], new TGHotString 
                           ("     &Run...    "), kLidaxOk);
      fOkButton->Associate (this);
      fF[2]->AddFrame (fOkButton, fL[1]);
      if (fParam->fDMTEnable) {
         fMonitorButton = new TGTextButton (fF[2], new TGHotString 
                              ("     &Monitors...    "), kLidaxMonitors);
         fMonitorButton->Associate (this);
         fF[2]->AddFrame (fMonitorButton, fL[8]);
      }
      else {
         fMonitorButton = 0;
      }
      fRestoreButton = new TGTextButton (fF[2], new TGHotString 
                           ("     R&estore...    "), kLidaxRestore);
      fRestoreButton->Associate (this);
      fF[2]->AddFrame (fRestoreButton, fL[7]);
      fStoreButton = new TGTextButton (fF[2], new TGHotString 
                           ("     &Store...     "), kLidaxStore);
      fStoreButton->Associate (this);
      fF[2]->AddFrame (fStoreButton, fL[1]);
      fClearButton = new TGTextButton (fF[2], new TGHotString 
                           ("     C&lear     "), kLidaxClear);
      fClearButton->Associate (this);
      fF[2]->AddFrame (fClearButton, fL[1]);
      // set default
      SetValues ();
   
      // set dialog box title
      SetWindowName ("LIGO Data Access (LiDaX)");
      SetIconName ("LiDaX");
      SetClassHints ("LiDaXDlg", "LiDaXDlg");
      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                              (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
                              (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                              ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
      // make the message box non-resizable
      SetWMSize (width, height);
      SetWMSizeHints (width, height, width, height, 0, 0);
      SetMWMHints (kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
                  kMWMDecorMenu,
                  kMWMFuncAll | kMWMFuncResize | kMWMFuncMaximize,
                  kMWMInputModeless);
   
      MapWindow();
   
      // initialze X11 watchdog
      fX11Watchdog = new TTimer (this, kX11WatchdogInterval, kTRUE);
      fX11Watchdog->TurnOn();
   
      // wait for events
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   TLGLidaxDialog::~TLGLidaxDialog ()
   {
      delete fX11Watchdog;
      delete fClearButton;
      delete fStoreButton;
      delete fRestoreButton;
      delete fAboutButton;
      delete fOkButton;
      if (fMonitorButton) delete fMonitorButton;
      delete fCancelButton;
      delete fEmail;
      delete fEmailSel;
      delete fWebPageChoose;
      delete fWebPage;
      delete fWebPageSel;
      delete fLogFileChoose;
      delete fLogFile;
      delete fLogFileSel;
      delete fSource;
      delete fDestination;
      for (int i = 0; i < 3; i++) {
         delete fLabel[i];
      }
      delete fProg;
      for (int i = 0; i < 3; i++) {
         delete fF[i];
      }
      for (int i = 0; i < 9; i++) {
         delete fL[i];
      }
   }

//______________________________________________________________________________
   void TLGLidaxDialog::SetValues()
   {
      if (fParam->fOutEnable) {
         fDialogSel->SetState (fParam->fDlgProgress ? kButtonDown: kButtonUp);
         fLogFile->SetText (fParam->fLog.c_str());
         fLogFileSel->SetState (fParam->fLogOn ? kButtonDown : kButtonUp);
         fWebPage->SetText (fParam->fWeb.c_str());
         fWebPageSel->SetState (fParam->fWebOn ? kButtonDown : kButtonUp);
         fEmail->SetText (fParam->fEmail.c_str());
         fEmailSel->SetState (fParam->fEmailOn ? kButtonDown : kButtonUp);
      }
   }

//______________________________________________________________________________
   Bool_t TLGLidaxDialog::GetValues()
   {
      // read source
      if (!fSource->ReadData()) {
         return false;
      }
      if (!fParam->fOutEnable) {
         return true;
      }
      // read destination
      if (!fDestination->ReadData()) {
         return false;
      }
      // log file
      fParam->fLogOn = (fLogFileSel->GetState() == kButtonDown);
      fParam->fLog = fLogFile->GetText();
      if (fParam->fLogOn && !fParam->fLog.empty()) {
         ofstream out (fParam->fLog.c_str());
         bool ok = !out.fail();
         out.close();
         if (!ok) {
            int retval;
            new TGMsgBox (fClient->GetRoot(), this, "Error", 
                         "Unable to open log file", kMBIconStop, 
                         kMBOk, &retval);
            return false;
         }
      }
   
      // web page
      fParam->fWebOn = (fWebPageSel->GetState() == kButtonDown);
      fParam->fWeb = fWebPage->GetText();
      if (fParam->fWebOn) {
         ofstream out (fParam->fWeb.c_str());
         bool ok = !out.fail();
         out.close();
         if (!ok) {
            int retval;
            new TGMsgBox (fClient->GetRoot(), this, "Error", 
                         "Unable to open file for web page", 
                         kMBIconStop, kMBOk, &retval);
            return false;
         }
      }
   
      // email
      fParam->fEmailOn = (fEmailSel->GetState() == kButtonDown);
      fParam->fEmail = fEmail->GetText();
   
      // read progress
      fParam->fDlgProgress = (fDialogSel->GetState() == kButtonDown);
      return true;
   }

//______________________________________________________________________________
   void TLGLidaxDialog::CloseWindow()
   {
      if (fOk) *fOk = kLidaxCancel;
      DeleteWindow();
   }

//______________________________________________________________________________
   Bool_t TLGLidaxDialog::HandleTimer (TTimer* timer)
   {
      // quit, if X11 died
      if (timer == fX11Watchdog) {
         if (!gXDisplay) gApplication->Terminate(0);
         timer->Reset();
      }
   
      // return
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGLidaxDialog::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         switch (parm1) {
            // ok
            case kLidaxOk:
               {
                  if (GetValues()) {
                     // set return value and quit
                     if (fOk) *fOk = kLidaxOk;
                     DeleteWindow();
                  }
                  break;
               }
            // cancel
            case kLidaxCancel:
               {
                  // set return value and quit
                  if (fOk) *fOk = kLidaxCancel;
                  DeleteWindow();
                  break;
               }
            // select monitors
            case kLidaxMonitors:
               {
                  // display monitor selection dlg
                  Bool_t ret;
                  new TLGDfmMonitorDlg (fClient->GetRoot(), this,
                                       fDestination->GetSel(), 
                                       fParam->fMonitors, fParam->fDMTKill, ret);
                  break;
               }
            // clear
            case kLidaxClear:
               {
                  // clear selection
                  LidaxParam ldx (*fParam->fDacc);
                  *fParam = ldx;
                  SetValues ();
                  fSource->Build (-3);
                  fDestination->Build (-3);
                  break;
               }
            // store
            case kLidaxStore:
               {
                  // store selection to file
                  GetValues();
                  xsilStoreLidax (this, *fParam);
                  break;
               }
            // restore
            case kLidaxRestore:
               {
                  // restore selection to file
                  if (xsilRestoreLidax (this, *fParam)) {
                     SetValues ();
                     fSource->SetSel (fParam->fDacc->sel());
                     fDestination->SetSel (fParam->fDacc->dest());
                     fSource->Build (-2);
                     fDestination->Build (-2);
                  }
                  break;
               }
            // about
            case kLidaxAbout:
               {
                  int retval;
                  new TGMsgBox (fClient->GetRoot(), this, "About", 
                               aboutmsg, 0, kMBOk, &retval);
                  break;
               }
            // choose log file
            case kLidaxLogfileChoose:
               {
                  TGFileInfo	info;
                  info.fFilename = 0;
                  info.fIniDir = 0;
               #if ROOT_VERSION_CODE >= ROOT_VERSION(3,1,6)
                  info.fFileTypes = const_cast<const char**>(gLogTypes);
               #else
                  info.fFileTypes = const_cast<char**>(gLogTypes);
               #endif
	       #if 1
		  info.fFileTypeIdx = 0 ; // Point at .log
		  {
		     // If the user presses cancel or doesn't choose
		     // a file, info.fFilename will be null
		     new TLGFileDialog(this, &info, kFDSave) ;
		  }
		  if (info.fFilename)
	       #else
                  if (TLGFileDialog (this, info, kFDSave, ".log"))
	       #endif
		  {
                     fLogFile->SetText (info.fFilename);
                     fLogFileSel->SetState (kButtonDown);
                  }
               #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
                  delete [] info.fFilename;
               #endif
                  break;
               }
            // choose web page
            case kLidaxWebfileChoose:
               {
                  TGFileInfo	info;
                  info.fFilename = 0;
                  info.fIniDir = 0;
               #if ROOT_VERSION_CODE >= ROOT_VERSION(3,1,6)
                  info.fFileTypes = const_cast<const char**>(gWebTypes);
               #else
                  info.fFileTypes = const_cast<char**>(gWebTypes);
               #endif
	       #if 1
		  info.fFileTypeIdx = 0 ; // Point at .html
		  {
		     // If the user presses cancel or doesn't choose
		     // a file, info.fFilename will be null
		     new TLGFileDialog(this, &info, kFDSave) ;
		  }
		  if (info.fFilename)
	       #else
                  if (TLGFileDialog (this, info, kFDSave, ".html"))
	       #endif
		  {
                     fWebPage->SetText (info.fFilename);
                     fWebPageSel->SetState (kButtonDown);
                  }
               #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
                  delete [] info.fFilename;
               #endif
                  break;
               }
         }
      }
      return kTRUE;
   }


}
