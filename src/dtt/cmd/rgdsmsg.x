
/* GDS command message rpc interface */

/* fix include problems with VxWorks */
#ifdef RPC_HDR
%#define		_RPC_HDR
#endif
#ifdef RPC_XDR
%#define		_RPC_XDR
#endif
#ifdef RPC_SVC		
%#define		_RPC_SVC
#endif
#ifdef RPC_CLNT		
%#define		_RPC_CLNT
#endif
%#include "rpcinc.h"
%#include "rgdsmsgtype.h"


/* rpc interface */
program RGDSMSG {
   version RGDSMSGVERS {

      reply_r GDSMSGSEND (message_r msg) = 1;
      int GDSMSGCLOSE (void) = 2;
      int GDSMSGKEEPALIVE (void) = 3;
      breply_r GDSMSGDATA (bmessage_r msg) = 4;

   } = 1;
} = 0x40000000;

