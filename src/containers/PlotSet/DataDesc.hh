/* version $Id: DataDesc.hh 7747 2016-09-23 18:11:08Z james.batch@LIGO.ORG $ */
#ifndef _LIGO_DATADESC_H
#define _LIGO_DATADESC_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: DataDesc						*/
/*                                                         		*/
/* Module Description: Core include file for ligo gui libraray		*/
/*		       (doesn't require linking with ligogui)		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 29Oct99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGCore.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
#include <iosfwd>

/** @name DataDesc
    The include file DataDesc.hh exports basic routines and objects
    needed by plotting routines to describe the data. This library
    implements objects with inline methods only and can be used 
    without linking against 'libligogui.so'.

    BasicDataDescriptor: Abstract object which provides routines
    to access an X and Y data array with optional error bars.

    DataDescriptor: Object which implements access to data arrays.
    It stores pointers to float arrays. 

    DataCopy: Object which implements access to data arrays, but
    makes a copy of the data arrays provided in the constructor.

    BasicPlotDescriptor: Abstract object used to describe plot
    data. Its descendent (PlotDescriptor) implements a basic data
    descriptor, a grap type, an A channel name and an optional
    B channel name.

    HistDataDescriptor: Object which implements access to histograms.

    HistDataCopy: Object which owns the histogram data.
 
    HistDataRef: Reference to histogram data

    AttDataDescriptor: This attibute object can be used (through 
    multiple inheritence) by user data objects to make them
    'plot' aware. By inheriting from this attribute user data
    objects can be used directly in the plot routines.
   
    @brief Core library for plotting extensions
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{

/** @name Constants
    Constants for describing plot types.
   
    @brief Plot type constants
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{

   /// Time series
   const char* const kPTTimeSeries = "Time series";
   /// Frequency series
   const char* const kPTFrequencySeries = "Frequency series";
   /// Power spectrum
   const char* const kPTPowerSpectrum = "Power spectrum";
   /// Coherence
   const char* const kPTCoherence = "Coherence";
   /// Cross correlation
   const char* const kPTCrossCorrelation = "Cross power spectrum";
   /// Transfer function
   const char* const kPTTransferFunction = "Transfer function";
   /// Coherence function
   const char* const kPTCoherenceFunction = "Coherence function";
   /// Transfer coefficients
   const char* const kPTTransferCoefficients = "Transfer coefficients";
   /// Coherence coeffcients
   const char* const kPTCoherenceCoefficients = "Coherence coefficients";
   /// Harmonic coefficients
   const char* const kPTHarmonicCoefficients = "Harmonic coefficients";
   /// Intermodulation coefficients
   const char* const kPTIntermodulationCoefficients = 
   "Intermodulation coefficients";
   /// 1-d Histogram
   const char* const kPTHistogram1 = "1-D Histogram";
   /// XY plot
   const char* const kPTXY = "XY";

#ifndef __CINT__
   /// list of plot types
   const char* const kPTAll[] = {
   kPTTimeSeries, kPTFrequencySeries, kPTPowerSpectrum, kPTCoherence, 
   kPTCrossCorrelation, kPTTransferFunction, kPTCoherenceFunction, 
   kPTTransferCoefficients, kPTCoherenceCoefficients,
   kPTHarmonicCoefficients, kPTIntermodulationCoefficients, 
   kPTHistogram1, kPTXY, 0};
#endif

// :COMPILER: there is a strange bug instantinating templates from the
// standard CC library when the char constants are declared
// const char* const kPTTimeSeries = "..."; (sparcworks CC 5.0)

//@}


/** A basic data descriptor is an abstract object which is used to 
    specify data used by the plotting routines. Its main purpose
    is to provide a means to get pointers to an X and a Y data 
    array and their respective length. A basic data descriptor 
    distinguishes real and complex data arrays. It also has the
    capability to provide data for error bars. All descendents must
    override 'GetN' (length of data array), 'GetX' (pointer to X
    array) and 'GetY' (pointer to Y array).
   
    @brief Abstract object for describing plotting data
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class BasicDataDescriptor {
   protected:
      /// Data is complex
      bool		fComplexData;
      /// Dirty bit
      mutable bool	fDirty;
      /// Persistent bit
      bool		fPersistent;
      /// Calculated bit
      bool		fCalculated;
      /// Marked bit (temporary use for marking data descriptors)
      mutable bool	fMarked;
   
   public:
      /** Constructs a basic data descriptor. The convention for
          complex data arrays is to store real and imaginary part
          of each value in the array as two consecutive numbers.
          @brief Default constructor.
          @param cmplx if true Y array is complex
          @return void
       ******************************************************************/
      explicit BasicDataDescriptor (bool cmplx = false)
      : fComplexData (cmplx), fDirty(false), fPersistent (false),
      fCalculated (false), fMarked (false) {
      }
      /** Destructs a basic data descriptor.
          @brief Destructor.
          @return void
       ******************************************************************/
      virtual ~BasicDataDescriptor() {
      }

      virtual void DumpData(std::ostream& out) const;
   
      /** Returns true if Y data array is complex.
          @brief Is complex method.
          @return true if complex
       ******************************************************************/
      virtual bool IsComplex () const {
         return fComplexData; }

      /** Sets Y data array as either complex (true) or real (false).
          @brief Set complex method.
          @param cmplx true if complex
          @return void
       ******************************************************************/
      virtual void SetComplex (bool cmplx) {
         fComplexData = cmplx; }
   
      /** Is dirty method.
          @brief Is dirty.
          @return True if dirty
       ******************************************************************/
      virtual bool IsDirty () const {
         return fDirty; }
      /** Set the dirty flag.
          @brief Set dirty flag.
          @return void
       ******************************************************************/
      virtual void SetDirty (bool set = true) const {
         fDirty = set; }
      /** Is persistent method.
          @brief Is persistent.
          @return True if persistent
       ******************************************************************/
      virtual bool IsPersistent () const {
         return fPersistent; }
      /** Set the persistent flag.
          @brief Set persistent flag.
          @return void
       ******************************************************************/
      virtual void SetPersistent (bool set = true) {
         fPersistent = set; }
      /** Is calculated method.
          @brief Is calculated.
          @return True if calculated
       ******************************************************************/
      virtual bool IsCalculated () const {
         return fCalculated; }
      /** Set the calculated flag.
          @brief Set calculated flag.
          @return void
       ******************************************************************/
      virtual void SetCalculated (bool set = true) {
         fCalculated = set; }
      /** Is marked method.
          @brief Is marked.
          @return True if marked
       ******************************************************************/
      virtual bool IsMarked () const {
         return fMarked; }
      /** Set the marked flag.
          @brief Set marked flag.
          @return void
       ******************************************************************/
      virtual void SetMarked (bool set = true) const {
         fMarked = set; }
   
      /** Returns true if xy data and false if data can be represented
          as (x0, dx, y). A descriptor which returns false must also
          implement the function GetDX() to return the x spacing. The
          start value is assumed to be GetX()[0].
          @brief Is XY.
          @return true if xy
       ******************************************************************/
      virtual bool IsXY() const {
         return true; }
      /** Returns x spacing if (x0,dx,y) data.
          @brief x spacing.
          @return x spacing
       ******************************************************************/
      virtual float GetDX() const {
         return 1.; }
   
      /** Returns a the length of the data array.
          @brief Get N method.
          @return Length
       ******************************************************************/
      virtual int GetN() const {
         return 0; }
      /** Returns a pointer to the X data array.
          @brief Get X method.
          @return X array
       ******************************************************************/
      virtual float* GetX() const {
         return 0; }
      /** Returns a pointer to the Y data array.
          @brief Get Y method.
          @return Y array
       ******************************************************************/
      virtual float* GetY() const {
         return 0; }
      /** Returns a pointer to the X error bar data array.
          @brief Get error X method.
          @return X error array
       ******************************************************************/
      virtual float* GetEX() const {
         return 0; }
      /** Returns a pointer to the X high error bar data array when
          error bars are asymmetric.
          @brief Get error X high method.
          @return X high error array
       ******************************************************************/
      virtual float* GetEXhigh() const {
         return 0; }      
      /** Returns a pointer to the X error bar data array.
          @brief Get error X method.
          @return X error array
       ******************************************************************/
      virtual float* GetEY() const {
         return 0; }
      /** Returns a pointer to the Y high error bar data array when
          error bars are asymmetric.
          @brief Get error Y high method.
          @return Y high error array
       ******************************************************************/
      virtual float* GetEYhigh() const {
         return 0; }      
      /** Erases the data. This function can be used to reload or 
          recalculate the data values.
          @brief Erase data method.
          @return true if successful
       ******************************************************************/
      virtual bool EraseData() {
         return true; }
      /** Returns a pointer to the X bin edges data array (histogram).
          @brief Get X bin edges method.
          @return X bin edges array
       ******************************************************************/
      virtual double* GetXBinEdges() const { 
         return 0;}
      /** Returns a pointer to the bin content data array (histogram).
          @brief Get bin contents method.
          @return bin contents array
       ******************************************************************/
      virtual double* GetBinContents() const { 
         return 0;}
      /** Returns a pointer to the bin error data array (histogram).
          @brief Get bin errors method.
          @return bin error array
       ******************************************************************/
      virtual double* GetBinErrors() const { 
         return 0;}
      /** Returns a pointer to the character array for X-axis label (histogram).
          @brief Get X-axis label method.
          @return X-axis label
       ******************************************************************/
      virtual const char* GetXLabel() const { // hist (mito)
         return 0; }
      /** Returns a pointer to the character array for bin content axis label (histogram).
          @brief Get bin content axis label method.
          @return bin content axis label
       ******************************************************************/ 
      virtual const char* GetNLabel() const { // hist (mito)
         return 0; }
      /** Returns number of data entries to the histogram (histogram).
          @brief Get number of data entries method.
          @return number of data entries
       ******************************************************************/ 
      virtual int GetNEntries() const { // hist (mito) 
         return 0; }
      /** Returns a pointer to the statistics data array (histogram).
          @brief Get statistics array method.
          @return statistics array
      ******************************************************************/ 
      virtual double* GetStats() const { // hist (mito)
         return 0; }
   };



/** This data descriptor implements access to data owned by someone
    else. Only pointers to the data array are stored. The data
    must stay valid while this data descriptor is used by a plot
    routine. A user can supply either pointers to a length field,
    a X and a Y array, or a start X, a delta X and pointers to 
    a length field an a Y array. In the second case the object will
    allocate and intialize the X array.
   
    @brief Object for describing pointers to plotting data
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class DataDescriptor : public BasicDataDescriptor {
   private:
      /// Disable copy constructor
      DataDescriptor (const DataDescriptor&);
      /// Disable assignment operator
      DataDescriptor& operator= (const DataDescriptor&);
   
   protected:
      /// XY or just Y with x0 and dx
      bool		fXYData;
      /// dx distance
      float		fdX;
      /// first x value
      float		fX0;
      /// Data pointer (X)
      float*		fX;
      /// Data pointer (Y)
      float*		fY;
      /// Number of data points
      int*		fN;
   
   public:
      /** Constructs a data descriptor without data references. 
          @brief Default constructor.
          @return void
       ******************************************************************/
      DataDescriptor ()
      : fXYData (false), fdX (0.0), fX0 (0.0), fX (0), fY (0), fN (0) {
      }
      /** Constructs a data descriptor by providing pointer to an 
          x array, a y array and the array length.
          @brief Constructor.
          @param x Pointer to X array
          @param y Pointer to Y array
          @param n Reference to array length
          @param cmplx true if data is complex
          @return void
       ******************************************************************/
      DataDescriptor (float* x, float* y, int& n, 
                     bool cmplx = false)
      : fXYData (false), fdX (0.0), fX0 (0.0), fX (0), fY (0), fN (0) {
         SetData (x, y, n, cmplx); }
      /** Constructs a data descriptor by providing a start and delta
          for the x data, a pointer to a y array and the array length.
          @brief Constructor.
          @param x0 First X value
          @param dx X array spacing
          @param y Pointer to Y array
          @param n Reference to array length
          @param cmplx true if data is complex
          @return void
       ******************************************************************/
      DataDescriptor (float x0, float dx, float* y, 
                     int& n, bool cmplx = false)
      : fXYData (false), fdX (0.0), fX0 (0.0), fX (0), fY (0), fN (0) {
         SetData (x0, dx, y, n, cmplx);
      }
   
      /** Destructs the data descriptor.
          @brief Destructor.
          @return void
       ******************************************************************/
      virtual ~DataDescriptor() {      
         if (!fXYData) delete [] fX;
      }
   
      /** Sets a new data for descriptor by providing pointer to an 
          x array, a y array and the array length.
          @brief Set data method.
          @param x Pointer to X array
          @param y Pointer to Y array
          @param n Reference to array length
          @param cmplx true if data is complex
          @return void
       ******************************************************************/
      virtual bool SetData (float* x, float* y, int& n, 
                        bool cmplx = false);
      /** Sets a new data for descriptor by providing a start and delta
          for the x data, a pointer to a y array and the array length.
          @brief Set data method.
          @param x0 First X value
          @param dx X array spacing
          @param y Pointer to Y array
          @param n Reference to array length
          @param cmplx true if data is complex
          @return void
       ******************************************************************/
      virtual bool SetData (float x0, float dx, float* y, 
                        int& n, bool cmplx = false);
   
      /** Returns true if xy data.
          @brief Is XY.
          @return true if xy
       ******************************************************************/
      virtual bool IsXY() const {
         return fXYData; }
      /** Returns x spacing if xy data.
          @brief x spacing.
          @return x spacing
       ******************************************************************/
      virtual float GetDX() const {
         return fdX; }
      /** Returns a the length of the data array.
          @brief Get N method.
          @return Length
       ******************************************************************/
      virtual int GetN() const {
         return *fN; }
      /** Returns a reference to the length of the data array.
          @brief Get N ref method.
          @return Length
       ******************************************************************/
      virtual int& GetNRef() const {
         return *fN; }
      /** Returns a pointer to the X data array.
          @brief Get X method.
          @return X array
       ******************************************************************/
      virtual float* GetX() const {
         return fX; }
      /** Returns a pointer to the Y data array.
          @brief Get Y method.
          @return Y array
       ******************************************************************/
      virtual float* GetY() const {
         return fY; }
   };


/** An indirect data descriptor references another basic data
    descriptor.
   
    @brief Indirect data descritor
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class IndirectDataDescriptor : public BasicDataDescriptor {
   protected:
      /// Referenced data descriptor
      BasicDataDescriptor*	fDesc;
      /// Array offset
      int			fOfs;
      /// New array length
      int			fLen;
   
   public:
      /** Constructs a basic data descriptor. The convention for
          complex data arrays is to store real and imaginary part
          of each value in the array as two consecutive numbers.
          @brief Constructor.
          @param desc Data descriptor to reference
          @return void
       ******************************************************************/
      explicit IndirectDataDescriptor (BasicDataDescriptor& desc)
      : fDesc (&desc), fOfs (0), fLen (-1) {
      }
      /** Constructs a basic data descriptor. The convention for
          complex data arrays is to store real and imaginary part
          of each value in the array as two consecutive numbers.
          @brief Constructor.
          @param desc Data descriptor to reference
          @param offset Offset into the data array
          @param len New length of data array
          @return void
       ******************************************************************/
      explicit IndirectDataDescriptor (BasicDataDescriptor& desc,
                        int offset, int len)
      : fDesc (&desc), fOfs (offset < 0 ? 0 : offset), fLen (len) {
      }
   
      /** Returns true if Y data array is complex.
          @brief Is complex method.
          @return true if complex
       ******************************************************************/
      virtual bool IsComplex () const {
         return fDesc->IsComplex(); }
      /** Sets Y data array as either complex (true) or real (false).
          @brief Set complex method.
          @param cmplx true if complex
          @return void
       ******************************************************************/
      virtual void SetComplex (bool cmplx) {
         fDesc->SetComplex (cmplx); }
      /** Is dirty method.
          @brief Is dirty.
          @return True if dirty
       ******************************************************************/
      virtual bool IsDirty () const {
         return fDesc->IsDirty(); }
      /** Set the dirty flag.
          @brief Set dirty flag.
          @return void
       ******************************************************************/
      virtual void SetDirty (bool set = true) const {
         fDesc->SetDirty (set); }
      /** Is persistent method.
          @brief Is persistent.
          @return True if persistent
       ******************************************************************/
      virtual bool IsPersistent () const {
         return fDesc->IsPersistent(); }
      /** Set the persistent flag.
          @brief Set persistent flag.
          @return void
       ******************************************************************/
      virtual void SetPersistent (bool set = true) {
         fDesc->SetPersistent (set); }
      /** Is calculated method.
          @brief Is calculated.
          @return True if calculated
       ******************************************************************/
      virtual bool IsCalculated () const {
         return fDesc->IsCalculated(); }
      /** Set the calculated flag.
          @brief Set calculated flag.
          @return void
       ******************************************************************/
      virtual void SetCalculated (bool set = true) {
         fDesc->SetCalculated (set); }
      /** Is marked method.
          @brief Is marked.
          @return True if marked
       ******************************************************************/
      virtual bool IsMarked () const {
         return fDesc->IsMarked(); }
      /** Set the marked flag.
          @brief Set marked flag.
          @return void
       ******************************************************************/
      virtual void SetMarked (bool set = true) const {
         fDesc->SetMarked (set); }
   
      /** Returns true if xy data and false if data can be represented
          as (x0, dx, y). A descriptor which returns false must also
          implement the function GetDX() to return the x spacing. The
          start value is assumed to be GetX()[0].
          @brief Is XY.
          @return true if xy
       ******************************************************************/
      virtual bool IsXY() const {
         return fDesc->IsXY(); }
      /** Returns x spacing if (x0,dx,y) data.
          @brief x spacing.
          @return x spacing
       ******************************************************************/
      virtual float GetDX() const {
         return fDesc->GetDX(); }
   
      /** Returns a the length of the data array.
          @brief Get N method.
          @return Length
       ******************************************************************/
      virtual int GetN() const {
         return (fLen >= 0) ? fLen : fDesc->GetN(); }
      /** Returns a pointer to the X data array.
          @brief Get X method.
          @return X array
       ******************************************************************/
      virtual float* GetX() const {
         return (fLen >= 0) ? fDesc->GetX() + fOfs : fDesc->GetX(); }
      /** Returns a pointer to the Y data array.
          @brief Get Y method.
          @return Y array
       ******************************************************************/
      virtual float* GetY() const {
         return (fLen >= 0) ? fDesc->GetY() + fOfs : fDesc->GetY(); }
      /** Returns a pointer to the X error bar data array.
          @brief Get error X method.
          @return X error array
       ******************************************************************/
      virtual float* GetEX() const {
         return (fLen >= 0) ? fDesc->GetEX() + fOfs : fDesc->GetEX(); }
      /** Returns a pointer to the X high error bar data array when
          error bars are asymmetric.
          @brief Get error X high method.
          @return X high error array
       ******************************************************************/
      virtual float* GetEXhigh() const {
         return (fLen >= 0) ? fDesc->GetEXhigh() + fOfs : fDesc->GetEXhigh(); }
      /** Returns a pointer to the X error bar data array.
          @brief Get error X method.
          @return X error array
       ******************************************************************/
      virtual float* GetEY() const {
         return (fLen >= 0) ? fDesc->GetEY() + fOfs : fDesc->GetEY(); }
      /** Returns a pointer to the Y high error bar data array when
          error bars are asymmetric.
          @brief Get error Y high method.
          @return Y high error array
       ******************************************************************/
      virtual float* GetEYhigh() const {
         return (fLen >= 0) ? fDesc->GetEYhigh() + fOfs : fDesc->GetEYhigh(); }
      /** Erases the data. This function can be used to reload or 
          recalculate the data values.
          @brief Erase data method.
          @return true if successful
       ******************************************************************/
      virtual bool EraseData() {
         return fDesc->EraseData(); }
      /** Returns a pointer to the X bin edges data array (histogram).
          @brief Get X bin edges method.
          @return X bin edges array
       ******************************************************************/
      virtual double* GetXBinEdges() const { 
         return fDesc->GetXBinEdges();}
      /** Returns a pointer to the bin content data array (histogram).
          @brief Get bin contents method.
          @return bin contents array
       ******************************************************************/
      virtual double* GetBinContents() const { 
         return fDesc->GetBinContents();}
      /** Returns a pointer to the bin error data array (histogram).
          @brief Get bin errors method.
          @return bin error array
       ******************************************************************/
      virtual double* GetBinErrors() const { 
         return fDesc->GetBinErrors();}
      /** Returns a pointer to the character array for X-axis label (histogram).
          @brief Get X-axis label method.
          @return X-axis label
       ******************************************************************/
      virtual const char* GetXLabel() const { // hist (mito)
         return fDesc->GetXLabel(); }
      /** Returns a pointer to the character array for bin content axis label (histogram).
          @brief Get bin content axis label method.
          @return bin content axis label
       ******************************************************************/ 
      virtual const char* GetNLabel() const { // hist (mito)
         return fDesc->GetNLabel(); }
      /** Returns number of data entries to the histogram (histogram).
          @brief Get number of data entries method.
          @return number of data entries
       ******************************************************************/ 
      virtual int GetNEntries() const { // hist (mito) 
         return fDesc->GetNEntries(); }
      /** Returns a pointer to the statistics data array (histogram).
          @brief Get statistics array method.
          @return statistics array
      ******************************************************************/ 
      virtual double* GetStats() const { // hist (mito)
         return fDesc->GetStats(); }
   };



/** This data descriptor implements access to data by making a 
    copy of the data arrays specified in the constructor. The 
    original data can be reused. A user can supply either an X 
    and Y array, or a start X, a delta X and a Y array.
   
    @brief Object for storing plotting data
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class DataCopy : public DataDescriptor {
   private:
      /// Disable assignment operator
      DataCopy& operator= (const DataCopy&);
      /// Disable copy contructor
      DataCopy (const DataCopy&);
   
   protected:
      // Length of array
      int		fLen;
   
   public:
      /** Constructs a data descriptor without data. 
          @brief Default constructor.
          @return void
       ******************************************************************/
      DataCopy () : DataDescriptor () {
      }

      /** Constructs a data descriptor from another one. Optionally
          selects a subrange and bins the data
          @brief Copy Constructor.
          @param dat Basic data descriptor
          @param n1 Subrange start index
          @param len Subrange length (-1 for all)
          @param bin binning
          @return void
       ******************************************************************/
      DataCopy (const BasicDataDescriptor& dat, int n1 = 0, 
               int len = -1, int bin = 1);
   
      /** Constructs a data descriptor by providing pointer to an 
          x array, a y array and the array length. The data is copied 
          into local arrays.
          @brief Constructor.
          @param x Pointer to X array
          @param y Pointer to Y array
          @param n Reference to array length
          @param cmplx true if data is complex
          @return void
       ******************************************************************/
      DataCopy (float* x, float* y, int& n, 
               bool cmplx = false) 
      : DataDescriptor () {
         SetData (x, y, n, cmplx); }
      /** Constructs a data descriptor by providing a start and delta
          for the x data, a pointer to a y array and the array length.
          The data is copied into local arrays.
          @brief Constructor.
          @param x0 First X value
          @param dx X array spacing
          @param y Pointer to Y array
          @param n Reference to array length
          @param cmplx true if data is complex
          @return void
       ******************************************************************/
      DataCopy (float x0, float dx, float* y, 
               int& n, bool cmplx = false)
      : DataDescriptor () {
         SetData (x0, dx, y, n, cmplx); }
   
      /** Destructs the data descriptor.
          @brief Destructor.
          @return void
       ******************************************************************/
      virtual ~DataCopy() {
         if (fXYData) delete [] fX;
         delete [] fY;
      }
   
      /** Sets a new data for descriptor by providing pointer to an 
          x array, a y array and the array length. The old data is 
          freed and the new data is copied into local arrays.
          @brief Set data method.
          @param x Pointer to X array
          @param y Pointer to Y array
          @param n Reference to array length
          @param cmplx true if data is complex
          @return void
       ******************************************************************/
      virtual bool SetData (float* x, float* y, int& n, 
                        bool cmplx = false);
      /** Sets a new data for descriptor by providing a start and delta
          for the x data, a pointer to a y array and the array length.
          The old data is freed and the new data is copied into local 
          arrays.
          @brief Set data method.
          @param x0 First X value
          @param dx X array spacing
          @param y Pointer to Y array
          @param n Reference to array length
          @param cmplx true if data is complex
          @return void
       ******************************************************************/
      virtual bool SetData (float x0, float dx, float* y, 
                        int& n, bool cmplx = false);
   
      /** Checks if a XY data copy can be represented as (x0,dx,Y).
          @brief Unset XY method.
          @return true if changed
       ******************************************************************/
      virtual bool UnsetXY ();
   };


/** This data descriptor stores a data matrix---either m x n or
    (m + 1) x n (x-axis incluced). In general it is accessed
    through a DataRef descriptor. The supplied data matrix is
    adopted by the descriptor. It also keeps a reference count. 
    Other data descriptors can refer to
    the data of this descriptor by increasing the ref count and
    they have to decrease the ref count when they no longer use
    the data. When the reference count reaches zero, the object will
    automatically delete itslef. Since this object destructs itself
    it MUST NEVER be defined as a static object.
   
    @brief Object for storing referenced plotting data
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class DataRefCount : public DataDescriptor {
   private:
      /// Disable assignment operator
      DataRefCount& operator= (const DataRefCount&);
      /// Disable copy contructor
      DataRefCount (const DataRefCount&);
   
   protected:
      /// Reference count
      int		fRefCount;
      /// Number of rows
      int		fNN;
      /// Number of columns
      int		fM;
   
   public:
      /** Constructs a data descriptor without data. 
          @brief Default constructor.
          @return void
       ******************************************************************/
      DataRefCount () : DataDescriptor (), fRefCount (0), fNN (0), fM (0) {
      }

      /** Constructs a data descriptor by providing pointers to an 
          (m+1) x n matrix. The data is adopted by the object.
          The first row must be the x array.
          @brief Constructor.
          @param dat Pointer to (m+1) x n matrix
          @param n Reference to array length
          @param m Number of Y arrays
          @param cmplx true if data is complex
          @return void
       ******************************************************************/
      DataRefCount (float* dat, int n, int m = 1, 
                   bool cmplx = false) 
      : fRefCount (0), fNN (n), fM (m) {
         SetData (dat, dat + (cmplx ? 2 : 1) * fNN, fNN, cmplx);
      }

      /** Constructs a data descriptor by providing a start and delta
          for the x data, a pointer to a m x n matrix and the array 
          length. The data is adopted by the object.
          @brief Constructor.
          @param x0 First X value
          @param dx X array spacing
          @param dat Data array
          @param n Reference to array length
          @param m Number of Y arrays
          @param cmplx true if data is complex
          @return void
       ******************************************************************/
      DataRefCount (float x0, float dx, float* dat, 
                   int n, int m = 1, bool cmplx = false)
      : fRefCount (0), fNN (n), fM (m) {
         SetData (x0, dx, dat, fNN, cmplx);
      }

      /** Destructs the data descriptor.
          @brief Destructor.
          @return void
       ******************************************************************/
      virtual ~DataRefCount() {
         if (fXYData) {
            delete [] fX; fX = 0; }
         else { 
            delete [] fY; fY = 0; }
      }
      /** Get the number of M dimensions (number of y arrays contained
          by this object).
          @brief Get M method.
          @return M
       ******************************************************************/
      virtual int GetM () const {
         return fM; }
      /** Add a reference. This will increase the reference count.
          @brief Add reference.
          @return void
       ******************************************************************/
      virtual void AddRef () {
         fRefCount++; }
      /** Delete a reference. This will decrease the reference count.
          The object is deleted if the reference count reaches zero.
          @brief Add reference.
          @return void
       ******************************************************************/
      virtual void RemoveRef () {
         if (--fRefCount <= 0) delete this; }
      /** Returns a pointer to the Y data array with index 0.
          @brief Get Y method.
          @return Y array
       ******************************************************************/
      virtual float* GetY () const {
         return fY; }
      /** Returns a pointer to the Y data array with index i.
          @brief Get Y method.
          @return Y array
       ******************************************************************/
      virtual float* GetY (int i) const {
         return ((i < 0) || (i >= fM)) ? 0 : 
            fY + (IsComplex() ? 2 : 1) * i * (*fN); }
   };



/** This data descriptor implements access to data by reference
    to another data descriptor.
   
    @brief Object for storing references to plotting data
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class DataRef : public DataDescriptor {
   private:
      /// Disable assignment operator
      DataRef& operator= (const DataRef&);
      /// Disable copy contructor
      DataRef (const DataRef&);
   
   protected:
      /// Referenced data object
      DataRefCount*	fRef;
      ///  Index into y array
      int		fIndex;
   
   public:
      /** Constructs a data descriptor without data. 
          @brief Default constructor.
          @return void
       ******************************************************************/
      DataRef () : DataDescriptor (), fRef(0) {
      }

      /** Constructs a data descriptor by setting references to a
          data reference copy. 
          @brief Copy Constructor.
          @param ref Data descriptor of referenced object
          @param i index of Y array
          @return void
       ******************************************************************/
      DataRef (DataRefCount& ref, int i = 0) 
      : DataDescriptor (ref.GetX(), ref.GetY(i), ref.GetNRef(), 
                       ref.IsComplex()), fRef (&ref), fIndex (i) {
         fRef->AddRef(); }

      /** Destructs the data descriptor.
          @brief Destructor.
          @return void
       ******************************************************************/
      virtual ~DataRef() {
         if (fRef) fRef->RemoveRef(); }
      /** Get the referenced data object.
          @brief Get the referenced data object.
          @return referenced data
       ******************************************************************/
      const DataRefCount* GetRef() const {
         return fRef; }
      /** Is dirty method.
          @brief Is dirty.
          @return True if dirty
       ******************************************************************/
      virtual bool IsDirty () const {
         return fRef->IsDirty(); }
      /** Set the dirty flag.
          @brief Set dirty flag.
          @return void
       ******************************************************************/
      virtual void SetDirty (bool set = true) const {
         fRef->SetDirty (set); }
      /** Is persistent method.
          @brief Is persistent.
          @return True if persistent
       ******************************************************************/
      virtual bool IsPersistent () const {
         return fRef->IsPersistent(); }
      /** Set the persistent flag.
          @brief Set persistent flag.
          @return void
       ******************************************************************/
      virtual void SetPersistent (bool set = true) {
         fRef->SetPersistent (set); }
      /** Is calculated method.
          @brief Is calculated.
          @return True if calculated
       ******************************************************************/
      virtual bool IsCalculated () const {
         return fRef->IsCalculated(); }
      /** Set the calculated flag.
          @brief Set calculated flag.
          @return void
       ******************************************************************/
      virtual void SetCalculated (bool set = true) {
         fRef->SetCalculated (set); }
      /** Is marked method.
          @brief Is marked.
          @return True if marked
       ******************************************************************/
      virtual bool IsMarked () const {
         return fRef->IsMarked(); }
      /** Set the marked flag.
          @brief Set marked flag.
          @return void
       ******************************************************************/
      virtual void SetMarked (bool set = true) const {
         fRef->SetMarked(); }
      /** Returns true if xy data.
          @brief Is XY.
          @return true if xy
       ******************************************************************/
      virtual bool IsXY() const {
         return fRef->IsXY(); }
      /** Returns x spacing if xy data.
          @brief x spacing.
          @return x spacing
       ******************************************************************/
      virtual float GetDX() const {
         return fRef->GetDX (); }
   };



/** This data descriptor implements access to histogram data owned by someone
    else. Only pointers to the data array are stored. The data
    must stay valid while this data descriptor is used by a plot
    routine. 
   
    @brief Object for storing plotting data for a histogram
    @author Written July 2001 by Masahiro Ito
    @version 1.0
 ************************************************************************/
   class HistDataDescriptor : public BasicDataDescriptor {
   private:
      /// Disable copy constructor
      HistDataDescriptor (const HistDataDescriptor&);
      /// Dsiable assignment operator
      HistDataDescriptor& operator= (const HistDataDescriptor&);
   protected:
      /// false: fixed bin spacing, true: variable bin spacing
      bool 	fXYData;
      /// bin edges
      double* 	fXBins;
      /// bin contents
      double* 	fContents;
      /// bin errors
      double* 	fBinErrors;
      /// number of bins
      int* 	fN;
      /// x label
      const char* 	fXLabel;
      /// y label
      const char* 	fNLabel;
      /// number of data entries
      int* 	fNEntries;
      /// histogram statistics array (sumw,sumw2,sumwx,sumwx2)
      double* 	fStats;
   
   public:
      /** Constructs a histogram data descriptor without data. 
          @brief Default constructor.
          @return void
       ******************************************************************/
      HistDataDescriptor()
      : fXYData(false),fXBins(0),fContents(0),fBinErrors(0),fN(0),
      fXLabel(0),fNLabel(0),fNEntries(0),fStats(0) {
      }
      /** Constructs a histogram data descriptor by providing 
          pointer to the data array (w/o bin error) and parameters.
          @brief Constructor.
          @param edge Pointer to X bin edge array
          @param content Pointer to bin content array
          @param nbinx Number of bins
          @param xlabel X-axis label
          @param nlabel bin content axis label
          @param nent number of data entries to the histogram
          @param stat Pointer to the histogram statistics array
          @param xydata true: variable bin spacing, false: fixed bin spacing
          @return void
       ******************************************************************/
      HistDataDescriptor(double* edge, double* content,int& nbinx,
                        char* xlabel, char* nlabel, int& nent, 
                        double* stat, bool xydata) 
      : fXYData(false),fXBins(0),fContents(0),fBinErrors(0),fN(0),
      fXLabel(0),fNLabel(0),fNEntries(0),fStats(0) {
         SetData(edge,content,nbinx,xlabel,nlabel,nent,stat,xydata); }
      /** Constructs a histogram data descriptor by providing 
          pointer to the data array (w/ bin error) and parameters.
          @brief Constructor.
          @param edge Pointer to X bin edge array
          @param content Pointer to bin content array
          @param error Pointer to bin error array
          @param nbinx Number of bins
          @param xlabel X-axis label
          @param nlabel bin content axis label
          @param nent number of data entries to the histogram
          @param stat Pointer to the histogram statistics array
          @param xydata true: variable bin spacing, false: fixed bin spacing
          @return void
       ******************************************************************/
      HistDataDescriptor(double* edge, double* content, double* error, int& nbinx,
                        char* xlabel, char* nlabel, int& nent, 
                        double* stat,bool xydata) 
      : fXYData(false),fXBins(0),fContents(0),fBinErrors(0),fN(0),
      fXLabel(0),fNLabel(0),fNEntries(0),fStats(0) {
         SetData(edge,content,error,nbinx,xlabel,nlabel,nent,stat,xydata); }
      /** Destructs the histogram data descriptor.
          @brief Destructor.
          @return void
      ******************************************************************/
      virtual ~HistDataDescriptor(){
      }
      /** Returns a pointer to the bin edge array.
          @brief Get bin edge array method.
          @return bin edge array
      ******************************************************************/
      virtual double* GetXBinEdges() const { 
         return fXBins; }
      /** Returns a pointer to the bin content array.
          @brief Get bin content array method.
          @return bin content array
      ******************************************************************/
      virtual double* GetBinContents() const { 
         return fContents; }
      /** Returns a pointer to the bin error array.
          @brief Get bin error array method.
          @return bin error array
      ******************************************************************/
      virtual double* GetBinErrors() const { 
         return fBinErrors; }
      /** Returns the number of bins in the histogram.
          @brief Get number of bins method.
          @return number of bins
      ******************************************************************/
      virtual int GetN() const { 
         return *fN; }
      /** Sets a new data for descriptor (w/o bin error).
          @brief Set data method.
          @param edge Pointer to X-axis edge array
          @param content Pointer to content array
          @param nbinx number of bins
          @param xlabel X-axis label
          @param nlabel bin content axis label
          @param nent number of data entries to the histogram
          @param stat Pointet to the histogram statistics
          @param xydata true: variable bin spacing, false: fixed bin spacing
          @return void
       ******************************************************************/
      virtual bool SetData(double* edge,double* content,int& nbinx,
                        const char* xlabel, const char* nlabel, int& nent, 
                        double* stat,bool xydata);
      /** Sets a new data for descriptor (w/ bin error).
          @brief Set data method.
          @param edge Pointer to X-axis edge array
          @param content Pointer to content array
     	  @param error Pointer to bin error array
          @param nbinx number of bins
          @param xlabel X-axis label
          @param nlabel bin content axis label
          @param nent number of data entries to the histogram
          @param stat Pointet to the histogram statistics
          @param xydata true: variable bin spacing, false: fixed bin spacing
          @return void
       ******************************************************************/
      virtual bool SetData(double* edge,double* content,double* error,int& nbinx,
                        const char* xlabel, const char* nlabel, int& nent, 
                        double* stat,bool xydata);
      /** Returns X-axis label.
          @brief Get X-axis label method.
          @return X-axis label
      ******************************************************************/
      virtual const char* GetXLabel() const { 
         return fXLabel; }
      /** Returns bin content axis label.
          @brief Get bin content axis label method.
          @return bin content axis label
      ******************************************************************/ 
      virtual const char* GetNLabel() const { 
         return fNLabel; }
      /** Returns number of data entries to the histogram.
          @brief Get number of data entries method.
          @return number of data entries
      ******************************************************************/
      virtual int GetNEntries() const { 
         return *fNEntries; }
      /** Returns a pointer to histogram statistics array.
          @brief Get histogram statistics array method.
          @return bin histogram statistics array
      ******************************************************************/
      virtual double* GetStats() const { 
         return fStats; }
      /** Returns type of the histogram bin spacing.
          @brief Get histogram statistics array method.
          @return true: variable bin spacing, false: fixed bin spacing
      ******************************************************************/
      virtual bool IsXY() const { 
         return fXYData; }
   }; 


/** This data descriptor implements access to data by making a 
    copy of the data arrays specified in the constructor. The 
    original data can be reused. 
   
    @brief Object for storing plotting data
    @author Written July 2001 by Masahiro Ito
    @version 1.0
 ************************************************************************/
   class HistDataCopy : public HistDataDescriptor {
   private:
      /// Disable assignment operator
      HistDataCopy& operator= (const HistDataCopy&);
      /// Disable copy contructor
      HistDataCopy (const HistDataCopy&);
   protected:
      /// number of bins
      int fNBinx;
      /// number of data entries
      int fNEnt;
   public:
      /// constructor
      HistDataCopy() : HistDataDescriptor() {
      }
      /** Constructs a histogram data descriptor from another one.
          @brief Copy Constructor.
          @param dat Basic data descriptor
          @return void
       ******************************************************************/
      HistDataCopy(const BasicDataDescriptor& dat);
      /** Constructs a histogram data descriptor by providing 
          pointer to the data array (w/o bin error) and parameters.
          @brief Constructor.
          @param edge Pointer to X bin edge array
          @param content Pointer to bin content array
          @param nbinx Number of bins
          @param xlabel X-axis label
          @param nlabel bin content axis label
          @param nent number of data entries to the histogram
          @param stat Pointer to the histogram statistics array
          @param xydata true: variable bin spacing, false: fixed bin spacing
          @return void
      ******************************************************************/
      HistDataCopy(double* edge,double* content,int& nbinx,
                  const char* xlabel, const char* nlabel,
                  int& nent, double* stat, bool xydata)
      : HistDataDescriptor() 
      { SetData(edge,content,nbinx,xlabel,nlabel,nent,stat,xydata); }
      /** Constructs a histogram data descriptor by providing 
          pointer to the data array (w/ bin error) and parameters.
          @brief Constructor.
          @param edge Pointer to X bin edge array
          @param content Pointer to bin content array
          @param error Pointer to bin error array
          @param nbinx Number of bins
          @param xlabel X-axis label
          @param nlabel bin content axis label
          @param nent number of data entries to the histogram
          @param stat Pointer to the histogram statistics array
          @param xydata true: variable bin spacing, false: fixed bin spacing
          @return void
      ******************************************************************/
      HistDataCopy(double* edge,double* content,double* error,int& nbinx,
                  const char* xlabel, const char* nlabel,
                  int& nent, double* stat, bool xydata)
      : HistDataDescriptor() 
      { SetData(edge,content,error,nbinx,xlabel,nlabel,nent,stat,xydata); }
      /** Destructs the histogram data descriptor.
          @brief Destructor.
          @return void
       ******************************************************************/
      virtual ~HistDataCopy();
      /** Set a histogram data descriptor from another one.
          @brief Set data method.
          @param dat Basic data descriptor
          @return void
       ******************************************************************/
      virtual bool SetData(const BasicDataDescriptor& dat);
      /** Sets a new histogram data for descriptor by providing pointer to the
          data array and parameters. The old data is 
          freed and the new data is copied into local arrays.
          @brief Set data method.
          @param edge Pointer to X bin edge array
          @param content Pointer to bin content array
          @param nbinx Number of bins
          @param xlabel X-axis label
          @param nlabel bin content axis label
          @param nent number of data entries to the histogram
          @param stat Pointer to the histogram statistics array
          @param xydata true: variable bin spacing, false: fixed bin spacing
          @return void
      ******************************************************************/
      virtual bool SetData(double* edge,double* content,int& nbinx, 
                        const char* xlabel, const char* nlabel,
                        int& nent, double* stat, bool xydata);
      /** Sets a new histogram data for descriptor by providing pointer to the
          data array and parameters. The old data is 
          freed and the new data is copied into local arrays.
          @brief Set data method.
          @param edge Pointer to X bin edge array
          @param content Pointer to bin content array
          @param error Pointer to bin error array
          @param nbinx Number of bins
          @param xlabel X-axis label
          @param nlabel bin content axis label
          @param nent number of data entries to the histogram
          @param stat Pointer to the histogram statistics array
          @param xydata true: variable bin spacing, false: fixed bin spacing
          @return void
      ******************************************************************/
      virtual bool SetData(double* edge,double* content,double* error,int& nbinx, 
                        const char* xlabel, const char* nlabel,
                        int& nent, double* stat, bool xydata);
   };

/** This data descriptor stores a m x n data matrix.
    The supplied data matrix is adopted by the descriptor. 
   
    @brief Object for storing referenced plotting data
    @author Written July 2001 by Masahiro Ito
    @version 1.0
 ************************************************************************/
   class HistDataRef : public HistDataDescriptor {
   
   private:
     /// Disable assignment operator
      HistDataRef& operator= (const HistDataRef&);
     /// Dsiable assignment operator
      HistDataRef (const HistDataRef&);
   
   protected:
     /// number of bins
      int fNBinx;
     /// number of data entries
      int fNEnt;
   public:
     /** Constructs a histogram data descriptor without data. 
    @brief Default constructor.
    @return void
     ******************************************************************/
      HistDataRef () : HistDataDescriptor () {
      }
     /** Constructs a histogram data descriptor by providing pointers to an 
    m x n matrix (fixed bin spacing). 
    @brief Constructor.
    @param lowedge Lowest bin edge
    @param spacing Bin spacing
    @param dat Pointer to bin contens
    @param err Pointer to bin errors
    @param stat Stat data vector
    @param nbin number of bins
    @param xlabel X-axis label
    @param nlabel bin content axis label
    @param nent number of data entries to the histogram
    @return void
     ******************************************************************/
      HistDataRef (double lowedge, double spacing, 
                  double* dat, double* err, double* stat, int nbin,
                  const char* xlabel, const char* nlabel,
                  int& nent );
     /** Constructs a histogram data descriptor by providing pointers to an 
    m x n matrix (variable bin spacing). 
    @brief Constructor.
    @param bin Pointer to bin edges
    @param dat Pointer to bin contents
    @param err Pointer to bin errors
    @param stat Stat data vector
    @param nbin number of bins
    @param xlabel X-axis label
    @param nlabel bin content axis label
    @param nent number of data entries to the histogram
    @return void
     ******************************************************************/
      HistDataRef (double* bin, double* dat, double* err, 
                  double* stat, int nbin,
                  const char* xlabel, const char* nlabel,
                  int& nent );
      virtual ~HistDataRef ();
   };



/** This data descriptor implements arithmetic operations between
    two other data desciptors. Descendent must override the calc method.
   
    @brief Abstract object for describing transformed diagnostics data
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class OpDataDescriptor : public BasicDataDescriptor {
   private:
      /// Disable copy constructor
      OpDataDescriptor (const OpDataDescriptor&);
      /// Dsiable assignment operator
      OpDataDescriptor& operator= (const OpDataDescriptor&);
   
   protected:
      /// Calulate only when needed
      bool			fCalcOnNeed;
      /// First operand
      const BasicDataDescriptor* 	f1;
      /// Second operand
      const BasicDataDescriptor* 	f2;
      /// X array
      mutable float*		fX;
      /// Y array
      mutable float*		fY;
      /// array length
      mutable int		fN;
   
      /** Gets the data from the data descriptors.
          @brief Get data method.
          @return true if successful
       ******************************************************************/
      virtual bool GetData() const;
   
   public:
   
      /** Constructs an arithmetic data descriptor.
          @brief Constructor.
          @param d1 first data descriptor
          @param d2 second data descriptor
          @param complex Data are complex.
          @param calcOnNeed calulate only when needed
          @return void
       ******************************************************************/
      OpDataDescriptor (const BasicDataDescriptor* d1, 
                       const BasicDataDescriptor* d2, 
                       bool complex = false, bool calcOnNeed = true) 
      : BasicDataDescriptor (complex), fCalcOnNeed (calcOnNeed), 
      f1 (d1), f2 (d2), fX (0), fY (0), fN (0) {
         SetCalculated ();
         if (!calcOnNeed) GetData(); }
   
      /** Destructs a diagnostics data descriptor.
          @brief Destructor.
          @return void
       ******************************************************************/
      virtual ~OpDataDescriptor () {
         delete [] fX; delete [] fY; }
      /** Returns a the length of the data array.
          @brief Get N method.
          @return Length
       ******************************************************************/
      virtual int GetN() const {
         if (fX == 0) GetData();
         return fN; }
      /** Returns a pointer to the X data array.
          @brief Get X method.
          @return X array
       ******************************************************************/
      virtual float* GetX() const {
         if (fX == 0) GetData();
         return fX; }
      /** Returns a pointer to the Y data array.
          @brief Get Y method.
          @return Y array
       ******************************************************************/
      virtual float* GetY() const {
         if (fY == 0) GetData();
         return fY; }
      /** Calculation of new data.
          @brief Calculate arithmetic of basic data decriptors.
          @param x x array
          @param y output y array
          @param y1 first input y array
          @param y2 second input y array
          @return true if successful
       ******************************************************************/
      virtual bool Calc (float* x, float* y, const float* y1, 
                        const float* y2) const = 0;
   
      /** Erases the local data.
          @brief Erase data method.
          @return true if successful
       ******************************************************************/
      virtual bool EraseData();
   };



/** This data descriptor implements the identity operation.
   
    @brief Object for calculating the identity
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class IdentityDataDescriptor : public OpDataDescriptor {
   private:
      /// Disable copy constructor
      IdentityDataDescriptor (const IdentityDataDescriptor&);
      /// Disable assignment operator
      IdentityDataDescriptor& operator= (const IdentityDataDescriptor&);
   public:
      /** Constructs an arithmetic data descriptor.
          @brief Constructor.
          @param d data descriptor
          @param calcOnNeed calulate only when needed
          @return void
       ******************************************************************/
      IdentityDataDescriptor (const BasicDataDescriptor* d, 
                        bool calcOnNeed = true) 
      : OpDataDescriptor (d, 0, d->IsComplex(), calcOnNeed) {
      }
      /** Calculation of identity.
          @brief Calculate identity.
          @param x x array
          @param y output y array
          @param y1 first input y array
          @param y2 second input y array
          @return true if successful
       ******************************************************************/
      virtual bool Calc (float* x, float* y, const float* y1, 
                        const float* y2) const;
   };



/** This data descriptor implements d1 / |d2|^2. This is used to 
    calulate the transfer function from the cross power spectrum and
    the power spectrum.
   
    @brief Object for calculating the transfer function from the cross 
          power spectrum and the power spectrum
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class DivSqrDataDescriptor : public OpDataDescriptor {
   private:
      /// Disable copy constructor
      DivSqrDataDescriptor (const DivSqrDataDescriptor&);
      /// Disable assignment operator
      DivSqrDataDescriptor& operator= (const DivSqrDataDescriptor&);
   public:
      /** Constructs an arithmetic data descriptor.
          @brief Constructor.
          @param d1 first data descriptor
          @param d2 second data descriptor
          @param calcOnNeed calulate only when needed
          @return void
       ******************************************************************/
      DivSqrDataDescriptor (const BasicDataDescriptor* d1, 
                        const BasicDataDescriptor* d2, 
                        bool calcOnNeed = true) 
      : OpDataDescriptor (d1, d2, d1->IsComplex(), calcOnNeed) {
      
      }
      /** Calculation of division.
          @brief Calculate ratio of basic data decriptors.
          @param x x array
          @param y output y array
          @param y1 first input y array
          @param y2 second input y array
          @return true if successful
       ******************************************************************/
      virtual bool Calc (float* x, float* y, const float* y1, 
                        const float* y2) const;
   };



/** This data descriptor implements f_i = Sqrt (Sum_(i=j)^(N-1) A_j^2 dx). 
    This is used to the integrated rms of a power spectrum between the 
    current position and the higher end of the spectrum.
   
    @brief Object for calculating the "running" rms of a power spectum
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class FreqRMSDataDescriptor : public OpDataDescriptor {
   private:
      /// Disable copy constructor
      FreqRMSDataDescriptor (const FreqRMSDataDescriptor&);
      /// Disable assignment operator
      FreqRMSDataDescriptor& operator= (const FreqRMSDataDescriptor&);
   public:
      /** Constructs an arithmetic data descriptor.
          @brief Constructor.
          @param d1 first data descriptor
          @param calcOnNeed calulate only when needed
          @return void
       ******************************************************************/
      FreqRMSDataDescriptor (const BasicDataDescriptor* d1, 
                        bool calcOnNeed = true) 
      : OpDataDescriptor (d1, 0, false, calcOnNeed) {
      
      }
      /** Calculation of division.
          @brief Calculate ratio of basic data decriptors.
          @param x x array
          @param y output y array
          @param y1 first input y array
          @param y2 second input y array
          @return true if successful
       ******************************************************************/
      virtual bool Calc (float* x, float* y, const float* y1, 
                        const float* y2) const;
   };



/** An abstract object. Its descendent PlotDescriptor is used by 
    plotting routines to keep track of data descriptors, graph name
    channel names, parameter lists and calibration records.
   
    @brief Abstract object fo rdescribing plotting data
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class BasicPlotDescriptor {
   public:
      /** Constructs a basic plot descriptor. 
          @brief Default constructor.
          @return void
       ******************************************************************/
      BasicPlotDescriptor () {
      }
      /** Destructs the basic plot descriptor.
          @brief Destructor.
          @return void
       ******************************************************************/
      virtual ~BasicPlotDescriptor () {
      }
      /** Updates all plots with the current data described by this
          plot descriptor.
          @brief Update plot method.
          @return void
       ******************************************************************/
      virtual void UpdatePlot () const = 0;
   };



/** When inheriting from this data descriptor attribute object and
    overwriting the 'NewDataDescriptor' method a user data object 
    can be made 'plot aware'. This means that this object can then
    be used by plotting routines directly. To make it work the
    'NewDataDescriptor' method must return a valid data descriptor
    pointing to the data stored by the user object. 'Self aware' user 
    object can also override the 'GetGraphType', the 'GetAChannel'
    and optionally the 'GetBChannel' to provide the plotting routines
    with graph type and channel names. If not, they have to be 
    specified when invoking the plotting routine. This object will
    add some overhead to the user data object (copy constructor, 
    assignment operator, virtual methods, virtual destructor).
    The attribute also provides the method 'UpdatePlot' which can
    be used by the user object after its data has been changed
    to update the plots.

    Current limitations: Can only be used by one plot set at a time.
   
    @brief Attribute for making data objects 'plot aware'
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class AttDataDescriptor {
      /// PlotDescriptor is a friend class
      friend class PlotDescriptor;
   
   private:
      /// Pointer to plot descriptor which owns the data descriptor
      mutable BasicPlotDescriptor* fParent;
   
   public:
      /** Constructs a data descriptor attribute.
          @brief Default constructor.
          @return void
       ******************************************************************/
      AttDataDescriptor () : fParent (0) {
      };
      /** Destructs the data descriptor attribute.
          @brief Destructor.
          @return void
       ******************************************************************/
      virtual ~AttDataDescriptor () {
         delete fParent;}
      /** Constructs a data descriptor attribute from another one.
          @brief Copy constructor.
          @param att Data descriptor attribute
          @return void
       ******************************************************************/
      AttDataDescriptor (const AttDataDescriptor& att) {
         fParent = att.fParent; att.fParent = 0; }
      /** Assigns data descriptor attributes.
          @brief Assignment operator.
          @param att Data descriptor attribute
          @return *this
       ******************************************************************/
      AttDataDescriptor& operator= (const AttDataDescriptor& att) {
         delete fParent; fParent = att.fParent; 
         att.fParent = 0; 
         return *this; }
   
      /** Returns a data descriptor for the user data object.
          This method MUST be overwritten by derived classes.
          @brief New data descriptor method.
          @return Data descriptor
       ******************************************************************/
      virtual BasicDataDescriptor* NewDataDescriptor () const = 0;
      /** Returns the graph type of the user data object.
          This method might be overwritten by derived classes.
          @brief Get graph type method.
          @return Graph type string
       ******************************************************************/
      virtual const char* GetGraphType () const {
         return 0; }
      /** Returns the name of the A channel of the user data object.
          This method might be overwritten by derived classes.
          @brief Get A channel method.
          @return A channel string
       ******************************************************************/
      virtual const char* GetAChannel () const {
         return 0; }
      /** Returns the name of the B channel of the user data object.
          This method might be overwritten by derived classes.
          @brief Get B channel method.
          @return B channel string
       ******************************************************************/
      virtual const char* GetBChannel () const {
         return 0; }
      /** Returns the plot descriptor which owns the data descriptor.
          @brief Get parent plot descriptor.
          @return Parent plot descriptor
       ******************************************************************/
      const BasicPlotDescriptor* GetParentPlotDescriptor() const {
         return fParent; }
      /** Updates all plots which are using the data of the user
          object decribed by this data descriptor attribute.
          plot descriptor.
          @brief Update plot method.
          @return void
       ******************************************************************/
      virtual void UpdatePlot () const {
         if (fParent) fParent->UpdatePlot(); }
   };

//@}

#endif // _LIGO_DATADESC_H
