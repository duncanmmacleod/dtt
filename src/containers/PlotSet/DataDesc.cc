#include "DataDesc.hh"
#include <time.h>
#include <cstring>
#include <cmath>
#include <iostream>

   using namespace std;

   void BasicDataDescriptor::DumpData(std::ostream& out) const 
   {
       out << "DataDesc: complex: " << fComplexData << " dirty: " << fDirty
	   << " persistent: " << fPersistent << " calculated: " << fCalculated
	   << " marked: " << fMarked << endl;
       int N = GetN();
       out << "Number of data items: " << N << endl;
       const float* p = GetY();
       if (!p || !N) {
	 cout << "no data" << endl;
       } else {
	 for (int i=0; i<N; i += 8) {
	   out << i << " ";
	   for (int j=i; j<i+8 && j<N; ++j) out << " " << p[j]; 
	   out << endl;
	 }
       }
   }

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// DataDescriptor                                                       //
//                                                                      //
// Descriptor for simple data      		                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool DataDescriptor::SetData (float* x, float* y, int& n, 
                     bool cmplx)
   {
      if (!fXYData) delete [] fX;
      SetComplex (cmplx);
      fXYData = true;
      fX = x;
      fY = y;
      fN = &n;
      fdX = 1.0;
      return true;
   }

//______________________________________________________________________________
   bool DataDescriptor::SetData (float x0, float dx, float* y, 
                     int& n, bool cmplx)
   {
      if (!fXYData) delete [] fX;
      SetComplex (cmplx);
      fXYData = false;
      fY = y;
      fN = &n;
      fX = new float [*fN];
      for (int i = 0; i < *fN; i++) {
         fX[i] = x0 + (float) i * dx;
      }
      fdX = dx;
      return true;
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// DataCopy (inlines)                                                   //
//                                                                      //
// Descriptor for simple data (copies the data)	                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   DataCopy::DataCopy (const BasicDataDescriptor& dat, 
                     int n1, int len, int bin)
   {
      SetComplex (dat.IsComplex());
      if (bin < 1) bin = 1;
      if (len > 0) {
         if (n1 + len >= dat.GetN()) {
            fLen = dat.GetN() - n1;
         }
         else {
            fLen = len;
         }
      }
      else {
         fLen = dat.GetN() - n1;
      }
      fLen /= bin;
      if ((fLen <= 0) || (dat.GetX() == 0) || (dat.GetY() == 0)) {
         fLen = 0;
         fX = 0;
         fY = 0;
      } 
      else {
         fX = new float [fLen];
         fY = new float [(fComplexData ? 2 : 1) * fLen];
         if ((fX != 0) && (fY != 0)) {
            for (int i = 0; i < fLen; i++) {
               fX[i] = dat.GetX()[n1 + i * bin];
               if (!fComplexData) {
                  fY[i] = dat.GetY()[n1 + i * bin];
               }
               else {
                  fY[2*i] = dat.GetY()[2*(n1 + i * bin)];
                  fY[2*i+1] = dat.GetY()[2*(n1 + i * bin) + 1];
               }
               for (int j = 1; j < bin; j++) {
                  fX[i] += dat.GetX()[n1 + i * bin + j];
                  if (!fComplexData) {
                     fY[i] += dat.GetY()[n1 + i * bin + j];
                  }
                  else {
                     fY[2*i] += dat.GetY()[2*(n1 + i * bin + j)];
                     fY[2*i+1] += dat.GetY()[2*(n1 + i * bin + j) + 1];
                  }
               }
               fX[i] /= bin;
               fY[i] /= bin;
            }
         }
      }
      const DataDescriptor* dd = 
         dynamic_cast<const DataDescriptor*> (&dat);
      if (dd) {
         fXYData = dd->IsXY();
         fdX = dd->GetDX() * bin;
         fX0 = fX ? fX[0] : 0;
      }
      else {
         fXYData = true;
         fdX = 1;
         fX0 = 0;
         UnsetXY();
      }
      fN = &fLen;
   }

//______________________________________________________________________________
   bool DataCopy::SetData (float* x, float* y, int& n, 
                     bool cmplx)
   {
      delete [] fY;
      fLen = n;
      DataDescriptor::SetData (x, y, fLen, cmplx);
      fX = new float [fLen];
      if (fX != 0) {
         if (x != 0) {
            memcpy (fX, x, fLen * sizeof (float));
         }
         else {
            memset (fX, 0, fLen * sizeof (float));
         }
      }
      fY = new float [(cmplx ? 2 : 1) * fLen];
      if (fY != 0) {
         if (y != 0) {
            memcpy (fY, y, (cmplx ? 2 : 1) * fLen * sizeof (float));
         }
         else {
            memset (fY, 0, (cmplx ? 2 : 1) * fLen * sizeof (float));
         }
      }
      return true;
   }

//______________________________________________________________________________
   bool DataCopy::SetData (float x0, float dx, float* y, 
                     int& n, bool cmplx)
   {
      delete [] fY;
      fLen = n;
      DataDescriptor::SetData (x0, dx, y, fLen, cmplx);
      fY = new float [(cmplx ? 2 : 1) * fLen];
      if (fY != 0) {
         if (y != 0) {
            memcpy (fY, y, (cmplx ? 2 : 1) * fLen * sizeof (float));
         }
         else {
            memset (fY, 0, (cmplx ? 2 : 1) * fLen * sizeof (float));
         }
      }
      return true;
   }

//______________________________________________________________________________
   bool DataCopy::UnsetXY ()
   {
      if (!IsXY() || (fX == 0) || (fLen <= 1)) {
         return false;
      }
      // guess values
      fdX = (fX[fLen-1] - fX[0]) / (fLen - 1.);
      fX0 = fX[0];
      if (fdX == 0) {
         return false;
      }
      // now check values in between
      double tolerance = fabs (1E-3 * fdX);
      int checklen = (fLen > 1000) ? 1000 : fLen;
      // only check the first 1000 because of float precision!
      for (int i = 1; i < checklen; i++) {
         if (fabs (fX[i] - fX[i-1] - fdX) > tolerance) {
            return false;
         }
      }
      // all values are on grid -> unset XY
      fXYData = false;
      return true;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// HistDataDescriptor                                                   //
//                                                                      //
// Descriptor for Histogram		                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool HistDataDescriptor::SetData(double* edge,double* content,
                     int& nbinx,
                     const char* xlabel, const char* nlabel,
                     int& nent, double* stat,bool xydata)
   {
      return SetData(edge,content,0,nbinx,xlabel,nlabel,nent,stat,xydata);
   }

//______________________________________________________________________________
   bool HistDataDescriptor::SetData(double* edge,double* content,
                     double* error, int& nbinx,
                     const char* xlabel, const char* nlabel,
                     int& nent, double* stat,bool xydata)
   {
      fXBins = edge;
      fContents = content;
      if (error) fBinErrors = error;
      else fBinErrors = 0;
      fN = &nbinx;
      fXLabel = xlabel;
      fNLabel = nlabel;
      fNEntries = &nent;
      fStats = stat;
      fXYData = xydata;
   
      return true;
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// HistDataCopy                                                         //
//                                                                      //
// Descriptor for Histogram (copies the data)	                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   HistDataCopy::HistDataCopy(const BasicDataDescriptor& dat)
   {
      int nbin = dat.GetN();
      int nent = dat.GetNEntries();
      SetData(dat.GetXBinEdges(),dat.GetBinContents(),
             nbin,dat.GetXLabel(),dat.GetNLabel(),
             nent,dat.GetStats(),dat.IsXY());
   }

//______________________________________________________________________________
   HistDataCopy::~HistDataCopy()
   {
      if (fXBins) delete[] fXBins;
      if (fContents) delete[] fContents;
      if (fBinErrors) delete[] fBinErrors;
      if (fStats) delete[] fStats;
      if (fXLabel) delete[] fXLabel;
      if (fNLabel) delete[] fNLabel;
   }

//______________________________________________________________________________
   bool HistDataCopy::SetData(const BasicDataDescriptor& dat)
   {
      int nbin = dat.GetN();
      int nent = dat.GetNEntries();
      SetData(dat.GetXBinEdges(),dat.GetBinContents(),dat.GetBinErrors(),nbin,
             dat.GetXLabel(),dat.GetNLabel(),nent,dat.GetStats(),dat.IsXY());
      return true;
   }

//______________________________________________________________________________
   bool HistDataCopy::SetData(double* edge,double* content, int& nbinx,
                     const char* xlabel, const char* nlabel,
                     int& nent, double* stat,bool xydata)
   {
      return SetData(edge,content,0,nbinx,xlabel,nlabel,nent,stat,xydata);
   }

//______________________________________________________________________________
   bool HistDataCopy::SetData(double* edge,double* content,double* error,
                     int& nbinx,const char* xlabel,
                     const char* nlabel,int& nent, double* stat,bool xydata)
   {
      fNBinx = nbinx;
      fNEnt = nent;
      HistDataDescriptor::SetData(edge,content,error,fNBinx,"","",fNEnt,stat,xydata);
   
      fXBins = new double[nbinx+1];
      if (fXBins != 0) {
         if (edge != 0) {
            memcpy(fXBins,edge,(nbinx+1)*sizeof(double));
         }	
         else {
            memset(fXBins,0,(nbinx+1)*sizeof(double));
         }
      }
   
      fContents = new double[nbinx+2];
      if (fContents != 0) {
         if (content != 0) {
            memcpy(fContents,content,(nbinx+2)*sizeof(double));
         }	
         else {
            memset(fContents,0,(nbinx+2)*sizeof(double));
         }
      }
   
      if (error != 0) {
         fBinErrors = new double[nbinx+2];
         if (fBinErrors != 0) {
            memcpy(fBinErrors,error,(nbinx+2)*sizeof(double));
         }
      }
      else fBinErrors = 0;
   
      fStats = new double[4];
      if (fStats != 0) {
         if (stat != 0) memcpy(fStats,stat,4*sizeof(double));
         else memset(fStats,0,4*sizeof(double));
      }
   
      if(xlabel != 0) {
         fXLabel = new char[strlen (xlabel) + 1];
         strcpy ((char*)fXLabel, xlabel);
      }
      else fXLabel = 0;
   
      if(nlabel != 0) {
         fNLabel = new char[strlen (nlabel) + 1];
         strcpy ((char*)fNLabel, nlabel);
      }
      else fNLabel = 0;
   
      return true;
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// HistDataRef                                                          //
//                                                                      //
// Descriptor for Histogram (reference to data)	                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   HistDataRef::HistDataRef (double lowedge, double spacing, 
                     double* dat, double* err, double* stat, int nbin,
                     const char* xlabel, const char* nlabel,
                     int& nent ) {
      fNBinx = nbin;
      fNEnt = nent;
      double* xbins = new double[nbin + 1];
      for (int i = 0; i < nbin + 1; i++) xbins[i] = lowedge + spacing * i;
   
      char* xl = new char[strlen (xlabel) + 1];
      if (xl) strcpy (xl,xlabel);
      char* nl = new char[strlen (nlabel) + 1];
      if (nl) strcpy (nl,nlabel);
   
      if ( !err )
         SetData(xbins,dat,fNBinx,xl,nl,fNEnt,stat,false);
      else
         SetData(xbins,dat,err,fNBinx,xl,nl,fNEnt,stat,false);
   
   }

//______________________________________________________________________________
   HistDataRef::HistDataRef (double* bin, double* dat,  double* err, 
                     double* stat, int nbin,
                     const char* xlabel, const char* nlabel,
                     int& nent) {
      fNBinx = nbin;
      fNEnt = nent;
   
      char* xl = new char[strlen (xlabel) + 1];
      if (xl) strcpy (xl,xlabel);
      char* nl = new char[strlen (nlabel) + 1];
      if (nl) strcpy (nl,nlabel);
   
      if ( !err )
         SetData(bin, dat, fNBinx, xl, nl, fNEnt, stat, true);
      else
         SetData(bin, dat, err, fNBinx, xl, nl, fNEnt, stat, true);
   }

//______________________________________________________________________________
   HistDataRef::~HistDataRef () {
      if ( fXBins ) delete[] fXBins;
      if ( !fXYData && fContents ) delete[] fContents;
      if ( fStats ) delete[] fStats;
      if ( fXLabel ) delete[] fXLabel;
      if ( fNLabel ) delete[] fNLabel;
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// OpDataDescriptor                                                     //
//                                                                      //
// Descriptor for caluclated data		                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool OpDataDescriptor::GetData() const {
      if (f1 != 0) { 
         fN = f1->GetN();
         if ((f2 != 0) && (f2->GetN() != fN)) {
            fN = 0;
            return false;
         }
         delete [] fX; 
         fX = new float [fN];
         delete [] fY; 
         fY = new float [fN * (fComplexData ? 2 : 1)]; 
         if (fX) {
            memcpy (fX, f1->GetX(), fN * sizeof (float));
         }
         if ((fX != 0) && (fY != 0)) {
            return Calc (fX, fY, f1->GetY(), (f2 != 0) ? f2->GetY() : 0); 
         }
         else {
            fN = 0;
            return false;
         }
      }
      else { 
         return false; }
   }

//______________________________________________________________________________
   bool OpDataDescriptor::EraseData() {
      delete [] fX; 
      fX = 0; delete [] fY; 
      fY = 0; 
      fN = 0; 
      if (!fCalcOnNeed) {
         GetData(); 
      }
      return true;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// IdentityDataDescriptor                                               //
//                                                                      //
// Descriptor for caluclated data		                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool IdentityDataDescriptor::Calc (float* x, float* y, 
                     const float* y1, const float* y2) const
   {
      if ((x == 0) || (y == 0) || (y1 == 0) || (fN <= 1)) {
         return false;
      }
      memcpy (y, y1, (IsComplex() ? 2 : 1) * sizeof(float));
      return true;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// DivSqrDataDescriptor                                                 //
//                                                                      //
// Descriptor for caluclated data		                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool DivSqrDataDescriptor::Calc (float* x, float* y, 
                     const float* y1, const float* y2) const
   {
      if ((x == 0) || (y == 0) || (y1 == 0) || (y2 == 0)) {
         return false;
      }
      if (f1->IsComplex()) {
         for (int i = 0; i < fN; i++) {
            float div = (f2->IsComplex()) ? 
               y2[2*i]*y2[2*i]+y2[2*i+1]*y2[2*i+1] : y2[i]*y2[i];
            y[2*i] = y1[2*i] / div;
            y[2*i+1] = -y1[2*i+1] / div;
         }
      }
      else {
         for (int i = 0; i < fN; i++) {
            float div = (f2->IsComplex()) ? 
               y2[2*i]*y2[2*i]+y2[2*i+1]*y2[2*i+1] : y2[i]*y2[i];
            y[i] = y1[i] / div;
         }
      }
      return true;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// FreqRMSDataDescriptor                                                //
//                                                                      //
// Descriptor for caluclated data		                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool FreqRMSDataDescriptor::Calc (float* x, float* y, 
                     const float* y1, const float* y2) const
   {
      if ((x == 0) || (y == 0) || (y1 == 0) || (fN <= 1)) {
         return false;
      }
      double rms = 0.;
      for (int i = fN - 1; i >= 0; i--) {
         double val = f1->IsComplex() ? 
            y1[2*i]*y1[2*i] + y1[2*i+1]*y1[2*i+1] : y1[i]*y1[i];
         double dx = (i == fN - 1) ? 
            x[fN-1] - x[fN-2] : x[i+1] - x[i];
         //cout << "val="<<val<<" dx="<<dx<<endl;
         rms += val * dx;
         y[i] = sqrt (rms);
      }
      return true;
   }


