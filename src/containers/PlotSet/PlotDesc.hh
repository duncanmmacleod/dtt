#ifndef _LIGO_PLOTDESC_H
#define _LIGO_PLOTDESC_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: PlotDesc						*/
/*                                                         		*/
/* Module Description: Plot descriptor:					*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 29Oct99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: PlotDesc.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include "DataDesc.hh"
#include "ParamDesc.hh"
#include "Descriptor.hh"
#include "Table.hh"
#include <cstring>
#include <iosfwd>


   class PlotSet;


/** Describes the data, the graph type, the channel name(s), the
    calibration data and the associated parameters of a plot. 
    A plot descriptor owns a data descriptor and keeps a copy
    of the graph type and channel name strings. In general, a user
    will generate plot descriptors with the help of the plot set
    obejcts and use it as reference to the plot routines.
   
    @brief Plot descriptor
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class PlotDescriptor : public BasicPlotDescriptor {
   
      friend class PlotSet;
      friend class PlotListLink;
   
   private:
      /// Unique plot descriptor ID
      int		fID;
      /// Last used ID
      static int	fLastID;
      /// Pointer to original data object
      const AttDataDescriptor* fDataDescr;
      /// Pointer to plot set which owns this descriptor
      PlotSet*		fOwner;
   
      /// Disable copy constructor
      PlotDescriptor (const PlotDescriptor&);
      /// Disable assignment operator
      PlotDescriptor& operator= (const PlotDescriptor&);
   
   protected:
      /// Name of graph type
      std::string		fGraphType;
      /// True if the the plot type has A and B channel associated
      bool		fHasTwoChannels;
      /// Name of A channel
      std::string		fAChannel;
      /// Name of B channel (if exists)
      std::string		fBChannel;
   
      /// Data dexcriptor
      BasicDataDescriptor* fData;
      /// Parameter descriptor
      ParameterDescriptor fParam;
      /// Calibration descriptor
      calibration::Descriptor fCal;
      /// Whether to show the Disconnect message on screen
      unsigned int bufferF;
      bool fShowDisconnect;
      unsigned int bufferA;
   
   public:
   
      PlotDescriptor (BasicDataDescriptor* desc, const char* graphtype, 
                     const char* Achn, const char* Bchn = 0,
                     const ParameterDescriptor* prmd = 0,
                     const calibration::Descriptor* cald = 0);
      PlotDescriptor (const AttDataDescriptor& data, 
                     const char* graphtype, const char* Achn, 
                     const char* Bchn = 0);
   
      virtual ~PlotDescriptor();
   
      /// Clone a plot descriptor with optional new name
      virtual PlotDescriptor* Clone (const char* graphtype = 0, 
                        const char* Achn = 0, const char* Bchn = 0,
                        calibration::Table* caltable = 0) const;

      void DumpPlot(std::ostream& out) const;

      int GetID () const {
         return fID; }
      virtual const char* GetGraphType () const {
         return fGraphType.c_str(); }
      virtual void SetGraphType (const char* graphtype) {
         fGraphType = graphtype; }
   
      virtual const char* GetAChannel () const {
         return fAChannel.c_str(); }
      virtual void SetAChannel (const char* channel) {
         fAChannel = channel; }
      virtual const char* GetBChannel () const {
         return fHasTwoChannels ? fBChannel.c_str() : 0; }
      virtual void SetBChannel (const char* channel);
      /** Is dirty method.
          @brief Is dirty.
          @return True if dirty
       ******************************************************************/
      virtual bool IsDirty () const {
         return fData ? fData->IsDirty() : false; }
      /** Set the dirty flag.
          @brief Set dirty flag.
          @return void
       ******************************************************************/
      virtual void SetDirty (bool set = true) const {
         if (fData) fData->SetDirty (set); }
      /** Is persistent method.
          @brief Is persistent.
          @return True if persistent
       ******************************************************************/
      virtual bool IsPersistent () const {
         return fData ? fData->IsPersistent() : false; }
      /** Set the persistent flag.
          @brief Set persistent flag.
          @return void
       ******************************************************************/
      virtual void SetPersistent (bool set = true) {
         if (fData) fData->SetPersistent (set); }
      /** Is calculated method.
          @brief Is calculated.
          @return True if calculated
       ******************************************************************/
      virtual bool IsCalculated () const {
         return fData ? fData->IsCalculated() : false; }
      /** Set the calculated flag.
          @brief Set calculated flag.
          @return void
       ******************************************************************/
      virtual void SetCalculated (bool set = true) {
         if (fData) fData->SetCalculated (set); }
      /** Is marked method.
          @brief Is marked.
          @return True if marked
       ******************************************************************/
      virtual bool IsMarked () const {
         return fData ? fData->IsMarked() : false; }
      /** Set the marked flag.
          @brief Set marked flag.
          @return void
       ******************************************************************/
      virtual void SetMarked (bool set = true) const {
         if (fData) fData->SetMarked (set); }

      virtual void SetShowDisconnect(bool show)
      {
          fShowDisconnect = show;
      }

      virtual bool GetShowDisconnect() const
      {
          return fShowDisconnect;
      }

      virtual const BasicDataDescriptor* GetData () const {
         return fData; }
      virtual bool SetData (BasicDataDescriptor* desc);
      virtual bool SetData (const AttDataDescriptor& data);
      virtual bool SetData (float* x, float* y, int n, 
                        bool cmplx = false) {
         return SetData (new DataDescriptor (x, y, n, cmplx)); }
      virtual bool SetData (float x0, float dx, float* y, 
                        int n, bool cmplx = false) {
         return SetData (new DataDescriptor (x0, dx, y, n, cmplx)); }
      virtual ParameterDescriptor& Param() {
         return fParam; }
      virtual const ParameterDescriptor& Param() const {
         return fParam; }
      virtual calibration::Descriptor& Cal() {
         return fCal; }
      virtual const calibration::Descriptor& Cal() const {
         return fCal; }
   
      virtual bool IsComplex() const {
         return fData->IsComplex(); }
      virtual int GetN() const {
         return fData->GetN(); }
      virtual float* GetX() const {
         return fData->GetX(); }
      virtual float* GetY() const {
         return fData->GetY(); }
      virtual float* GetEX() const {
         return fData->GetEX(); }
      virtual float* GetEXhigh() const {
         return fData->GetEXhigh(); }
      virtual float* GetEY() const {
         return fData->GetEY(); }
      virtual float* GetEYhigh() const {
         return fData->GetEYhigh(); }
   
      virtual bool HasOwner() const {
         return (fOwner != 0); }
      virtual PlotSet* GetOwner() const {
         return fOwner; }
   
      virtual void UpdatePlot () const;
   };


#endif // _LIGO_PLOTDESC_H

