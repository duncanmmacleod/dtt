#include "PlotDesc.hh"
#include "PlotSet.hh"
#include "tconv.h"
#include "calutil.h"
#include <string.h>
#include <strings.h>
#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <iostream>


   using namespace std;
   using namespace calibration;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// PlotDescriptor                                                       //
//                                                                      //
// Descriptor for plot type, data and parameters                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   int PlotDescriptor::fLastID = 0;

//______________________________________________________________________________
   PlotDescriptor::PlotDescriptor (BasicDataDescriptor* desc, 
                     const char* graphtype, const char* Achn, 
                     const char* Bchn, const ParameterDescriptor* prmd,
                     const Descriptor* cald)
   : fID (fLastID++), fDataDescr (0), fOwner (0), fHasTwoChannels (false), 
   fData (0), fParam (prmd), fCal (cald), fShowDisconnect(false), bufferA(0xbaadf00d), bufferF(0xdeadbeef)
   {
      SetGraphType (graphtype);
      SetAChannel (Achn);
      SetBChannel (Bchn);
      SetData (desc);
   }

//______________________________________________________________________________
   PlotDescriptor::PlotDescriptor (const AttDataDescriptor& data,
                     const char* graphtype, const char* Achn, 
                     const char* Bchn)
   : fID (fLastID++), fDataDescr (0), fOwner(0), fHasTwoChannels (false), 
   fData (0),fParam (0), fCal (), fShowDisconnect(false), bufferA(0xbaadf00d), bufferF(0xdeadbeef)
   {
      if (graphtype != 0) {
         if (Achn == 0) { 
            return;
         }
      }
      else {
         if (((graphtype = data.GetGraphType()) == 0) || 
            ((Achn = data.GetAChannel()) == 0)) {
            return;
         }
         Bchn = data.GetBChannel();
      }
      SetGraphType (graphtype);
      SetAChannel (Achn);
      SetBChannel (Bchn);
      SetData (data);
   }

//______________________________________________________________________________
   PlotDescriptor::~PlotDescriptor()
   {
      if (fDataDescr) {
         fDataDescr->fParent = 0;
      }
      if (fOwner) {
         fOwner->Remove (this, false);
         fOwner = 0;
      }
      delete fData;
   }

//______________________________________________________________________________
   PlotDescriptor* PlotDescriptor::Clone (const char* graphtype, 
                     const char* Achn, const char* Bchn,
                     calibration::Table* caltable) const
   {
      DataCopy* data = (fData == 0) ? 0 : new DataCopy (*fData);
      PlotDescriptor* pd = new PlotDescriptor (data, 
                           graphtype ? graphtype : fGraphType.c_str(),
                           Achn ? Achn : fAChannel.c_str(),
                           Bchn && (strlen (Bchn) > 0) ? 
                           Bchn : (fHasTwoChannels ? fBChannel.c_str() : 0));
      if (pd != 0) {
         pd->Param() = fParam;
         pd->Cal().Clone (fCal, caltable);
      }
      return pd;
   }

//______________________________________________________________________________
void PlotDescriptor::DumpPlot(std::ostream& out) const
   {
      out << "PlotDesc: ID=" << fID << endl;
      out << "  type: " << fGraphType << " aChan: " << fAChannel;
      if (fHasTwoChannels) out << " bChan: " << fBChannel;
      else                 out << " no bChan";
      if (fData) fData->DumpData(out);
      else       out << "No data." << endl;
   }

//______________________________________________________________________________
   void PlotDescriptor::SetBChannel (const char* channel) 
   {
      if (channel != 0) {
         fBChannel = channel;
         fHasTwoChannels = true;
      }
      else {
         fBChannel = "";
         fHasTwoChannels = false;
      }
   }

//______________________________________________________________________________
   bool PlotDescriptor::SetData (const AttDataDescriptor& data) 
   {
      if (SetData (data.NewDataDescriptor())) {
         fDataDescr = &data;
         fDataDescr->fParent = this;
         return true;
      }
      else {
         return false;
      }
   }

//______________________________________________________________________________
   bool PlotDescriptor::SetData (BasicDataDescriptor* desc)
   {
      if (fData != 0) {
         if (fDataDescr) {
            fDataDescr->fParent = 0;
         }
         delete fData;
      }
      fData = desc;
      return true;
   }

//______________________________________________________________________________
   void PlotDescriptor::UpdatePlot () const
   {
      if (fOwner != 0) {
         fOwner->Update (this);
      }  
   }
