#!/bin/sh

if [ -d sysroot ] ; then
	echo "sysroot already exists, not creating"
else
	mkdir sysroot
fi

cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=sysroot -DNO_PYTHON_INSTALL=1 $1 && make -j4 && make install 
